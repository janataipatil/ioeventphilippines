//
//  ActivityPlanViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TitleBar.h"
#import "MenueBar.h"
#import "HelperClass.h"
#import "EventTypePlansSetupViewController.h"
#import "AddActivityPlanViewController.h"
#import "AddActivitiesInPlanViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "HelpViewController.h"

@interface ActivityPlanViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;
@property(strong,nonatomic)HelperClass *helper;
@property (nonatomic) Reachability* reachability;

@property (strong, nonatomic) IBOutlet UIView *ActivityPlanView;
@property (strong, nonatomic) IBOutlet UILabel *ActivityPlanLbl;
@property (strong, nonatomic) IBOutlet UILabel *ActivityLbl;
@property (strong, nonatomic) IBOutlet UITableView *ActivityPlanTableView;
@property (strong, nonatomic) IBOutlet UITableView *ActivitiesTableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *ActivityPlanSegmentControllerOutlet;
@property (strong, nonatomic) IBOutlet UISearchBar *ActivityPlanSearchOutlet;

- (IBAction)ActivityPlanSegmentControllerValueChanged:(id)sender;
- (IBAction)AddActivityPlanButtonAction:(id)sender;
- (IBAction)AddActivityButtonAction:(id)sender;
- (IBAction)Logintranslationbuttonaction:(id)sender;



@property (strong, nonatomic) IBOutlet UILabel *actplan_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actplan_desc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actplan_status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *act_desc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *act_type_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *act_priority_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *act_status_ulbl;



@end
