//
//  ActivityPlanViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ActivityPlanViewController.h"

@interface ActivityPlanViewController ()
{
    EventTypePlansSetupViewController *EventTypePlanSetupView;
    NSArray *ActivityPlansArr;
    NSArray *PlanActivitiesArr;
    NSString *TypeofActionStr;
    NSString *PlanId;
    NSString *ActivityPlanId;
    NSInteger selectedRow;
    AppDelegate *appdelegate;
    int RefreshTableview;
    HelpViewController *HelpView;
}

@end

@implementation ActivityPlanViewController
@synthesize TitleBarView,MenueBarView,helper,ActivitiesTableView,ActivityPlanTableView,ActivityPlanSegmentControllerOutlet,ActivityPlanView,ActivityPlanLbl,ActivityLbl,ActivityPlanSearchOutlet;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    [ActivityPlanSearchOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [ActivityPlanSearchOutlet valueForKey:@"_searchField"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    
    searchField.placeholder = [helper gettranslation:@"LBL_224"];

    
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = YES;
    TitleBarView.UsernameLbl.text = [helper getUsername];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
      [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    MenueBarView = [[MenueBar alloc]initWithFrame:CGRectMake(0, 20, 100, 748)];
    MenueBarView.ActivityPlanButton.backgroundColor = [UIColor colorWithRed:46.0/255.0f green:176.0/255.0f blue:238.0/255.0f alpha:1.0f];
    
    [MenueBarView.HomeButton addTarget:self action:@selector(HomeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivitiesButton addTarget:self action:@selector(ActivityButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.SpeakerButton addTarget:self action:@selector(SpeakerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.AttendeeButton addTarget:self action:@selector(AttendeeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.CalendarButton addTarget:self action:@selector(CalendarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
     MenueBarView.VersionNumber.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];

    
    [self.view addSubview:MenueBarView];
    [self.view addSubview:TitleBarView];
    [self langsetuptranslations];
    
    //Add swipe gesture recogniser
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeRightAction:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    [self refreshdata];
    
    
    
    
    
    

}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshTableview ==1)
    {
        ActivityPlanTableView.delegate = self;
        ActivityPlanTableView.dataSource = self;
        [ActivityPlanTableView reloadData];
        ActivitiesTableView.delegate = self;
        ActivitiesTableView.dataSource = self;
       [ActivitiesTableView reloadData];
    }
}
#pragma mark search bar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == ActivityPlanSearchOutlet)
    {
        [self refreshdata];
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
           // count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[c] %@) OR (SELF.activityPlandescription contains[c] %@) OR (SELF.status contains[c] %@)",searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[ActivityPlansArr filteredArrayUsingPredicate:predicate]];
            ActivityPlansArr = [filtereventarr copy];
        }
        [ActivityPlanTableView reloadData];
       
       
         ActivityPlanLbl.text = [NSString stringWithFormat:@"%@ (%lu)",[helper gettranslation:@"LBL_008"],(unsigned long)ActivityPlansArr.count];
       // [self getactivitydata];
    }
}
//-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
//{
//    [self getactivitydata];
//    return NO;
//}

#pragma mark menu button action


-(void)langsetuptranslations
{
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [MenueBarView.HomeButton setTitle:[helper gettranslation:@"LBL_143"] forState:UIControlStateNormal];
    [MenueBarView.ActivitiesButton setTitle:[helper gettranslation:@"LBL_003"] forState:UIControlStateNormal];
    [MenueBarView.CalendarButton setTitle:[helper gettranslation:@"LBL_060"] forState:UIControlStateNormal];
    [MenueBarView.SpeakerButton setTitle:[helper gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [MenueBarView.AttendeeButton setTitle:[helper gettranslation:@"LBL_054"] forState:UIControlStateNormal];
    [MenueBarView.ActivityPlanButton setTitle:[helper gettranslation:@"LBL_007"] forState:UIControlStateNormal];
    
    _actplan_ulbl.text = [helper gettranslation:@"LBL_007"];
    _actplan_desc_ulbl.text = [helper gettranslation:@"LBL_094"];
    _actplan_status_ulbl.text = [helper gettranslation:@"LBL_242"];
    
    _act_desc_ulbl.text = [helper gettranslation:@"LBL_094"];
    _act_type_ulbl.text = [helper gettranslation:@"LBL_266"];
    _act_priority_ulbl.text = [helper gettranslation:@"LBL_203"];
    _act_status_ulbl.text = [helper gettranslation:@"LBL_242"];
    
    
    [ActivityPlanSegmentControllerOutlet setTitle:[helper gettranslation:@"LBL_008"] forSegmentAtIndex:0];
    [ActivityPlanSegmentControllerOutlet setTitle:[helper gettranslation:@"LBL_288"] forSegmentAtIndex:1];
    
}


- (IBAction)HomeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindfromActivityPlanToHome" sender:self];
}
- (IBAction)CalendarButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"ActivityPlanToCalendar" sender:self];
}
- (IBAction)ActivityButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"ActivityPlanToActivies" sender:self];
}
- (IBAction)SpeakerButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"ActivityPlanToSpeaker" sender:self];
}
- (IBAction)AttendeeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"ActivityPlanToAttendee" sender:self];
}
-(void)getactivitiesdata
{
    PlanActivitiesArr = [[helper query_alldata:@"PlanActivities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",ActivityPlanId]];
    ActivityLbl.text = [NSString stringWithFormat:@"%@ (%lu)",[helper gettranslation:@"LBL_003"],(unsigned long)PlanActivitiesArr.count];
    [ActivitiesTableView reloadData];
    
   
}
-(void)getactivitydata
{
    if ([ActivityPlansArr count]>0)
    {
        ActivityPlanId = [ActivityPlansArr[0] valueForKey:@"planID"];
        [self getactivitiesdata];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self tableView:ActivityPlanTableView didSelectRowAtIndexPath:indexPath];
}
-(void)refreshdata
{
    [self getplansdata];
    [self getactivitydata];
}
-(void)getplansdata
{
    ActivityPlansArr = [helper query_alldata:@"ActivityPlans"];
    ActivityPlanLbl.text = [NSString stringWithFormat:@"%@ (%lu)",[helper gettranslation:@"LBL_008"],(unsigned long)ActivityPlansArr.count];
   [ActivityPlanTableView reloadData];
   
   
}

#pragma mark swipe method
-(IBAction)SwipeRightAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"ActivityPlanToAddActivityPlan"])
    {
        AddActivityPlanViewController *controller = segue.destinationViewController;
        controller.TypeofActionStr = TypeofActionStr;
        controller.PlanId = PlanId;
    }
    if ([[segue identifier] isEqualToString:@"ActivityPlanToAddActivitiesInPlan"])
    {
        AddActivitiesInPlanViewController *controller = segue.destinationViewController;
        controller.PlanId = ActivityPlanId;
        controller.TypeofActionStr = TypeofActionStr;
    }
    
}

#pragma segment controller method
- (IBAction)ActivityPlanSegmentControllerValueChanged:(id)sender
{
    switch (ActivityPlanSegmentControllerOutlet.selectedSegmentIndex)
    {
            case 0:
                [EventTypePlanSetupView removeFromParentViewController];
                [EventTypePlanSetupView.view removeFromSuperview];
                break;
            case 1:
                [self EventTypePlanSetup];
                break;
            default:
                break;
    }
}
-(void)EventTypePlanSetup
{
    
    EventTypePlanSetupView = [self.storyboard instantiateViewControllerWithIdentifier:@"EventTypePlansSetupView"];
    [EventTypePlanSetupView.view setFrame:CGRectMake(0, 0, ActivityPlanView.frame.size.width, ActivityPlanView.frame.size.height)];
    [self addChildViewController:EventTypePlanSetupView];
    [ActivityPlanView addSubview:EventTypePlanSetupView.view];
    [EventTypePlanSetupView didMoveToParentViewController:self];
    
    
}
- (IBAction)AddActivityPlanButtonAction:(id)sender
{
    TypeofActionStr = @"Add";
    [self performSegueWithIdentifier:@"ActivityPlanToAddActivityPlan" sender:self];
}
- (IBAction)AddActivityButtonAction:(id)sender
{
    TypeofActionStr = @"Add";
    [self performSegueWithIdentifier:@"ActivityPlanToAddActivitiesInPlan" sender:self];
}

#pragma mark tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == ActivityPlanTableView)
    {
       return [ActivityPlansArr count];
    }
    else if (tableView == ActivitiesTableView)
    {
        return [PlanActivitiesArr count];
    }
    else
      return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    if (tableView == ActivityPlanTableView)
    {
            NSString *cellIdentifier = @"ActivityPlancustomcell";
            customcell = [ActivityPlanTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            UIView *contentView = customcell.contentView;
            customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
            UILabel *ActivityPlanlbl = (UILabel *) [contentView viewWithTag:1];
            UILabel *ActivityPlanDescriptionlbl = (UILabel *) [contentView viewWithTag:2];
            UILabel *Statuslbl = (UILabel *) [contentView viewWithTag:3];
            UIButton *EditActivityPlansButton = (UIButton *)[contentView viewWithTag:4];
            [EditActivityPlansButton addTarget:self action:@selector(EditActivityPlanButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            ActivityPlanlbl.text = [ActivityPlansArr[indexPath.row] valueForKey:@"name"];
            ActivityPlanDescriptionlbl.text = [ActivityPlansArr[indexPath.row] valueForKey:@"activityPlandescription"];
            Statuslbl.text = [ActivityPlansArr[indexPath.row] valueForKey:@"status"];
    }
    else if (tableView == ActivitiesTableView)
    {
        NSString *cellIdentifier = @"PlanActivitiescustomcell";
        customcell = [ActivitiesTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        UILabel *Descriptionlbl = (UILabel *) [contentView viewWithTag:1];
        UILabel *Typelbl = (UILabel *) [contentView viewWithTag:2];
        UILabel *Durationlbl = (UILabel *) [contentView viewWithTag:3];
        UILabel *Prioritylbl = (UILabel *)[contentView viewWithTag:4];
        UILabel *Statuslbl = (UILabel *)[contentView viewWithTag:5];
        UIButton *EditActivitiesinPlansButton = (UIButton *)[contentView viewWithTag:6];
        [EditActivitiesinPlansButton addTarget:self action:@selector(EditActivitiesinPlansButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        Descriptionlbl.text = [PlanActivitiesArr[indexPath.row] valueForKey:@"activityDescription"];
        Typelbl.text = [PlanActivitiesArr[indexPath.row] valueForKey:@"type"];
        Durationlbl.text = [PlanActivitiesArr[indexPath.row] valueForKey:@"duration"];
        Prioritylbl.text = [PlanActivitiesArr[indexPath.row] valueForKey:@"priority"];
        Statuslbl.text = [PlanActivitiesArr[indexPath.row] valueForKey:@"status"];
    }
     customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
-(IBAction)EditActivitiesinPlansButtonAction:(id)sender
{
    TypeofActionStr = @"Edit";
    [self performSegueWithIdentifier:@"ActivityPlanToAddActivitiesInPlan" sender:self];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (tableView == ActivityPlanTableView)
    {
       // [self getplansdata];
        [ActivityPlanTableView reloadData];
       
        
        if ([ActivityPlansArr count]>0)
        {
            ActivityPlanId = [ActivityPlansArr[indexPath.row] valueForKey:@"planID"];
            [self getactivitiesdata];
            
        }
         //cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
       // [self tableView:ActivityPlanTableView didSelectRowAtIndexPath:indexPath];
    }
    selectedRow = indexPath.row;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == ActivityPlanTableView)
    {
        if (selectedRow == indexPath.row)
        {
            cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
        }
        else
        {
            cell.contentView.backgroundColor = [UIColor whiteColor] ;
        }
    }

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == ActivityPlanTableView)
    {
        [helper serverdelete:@"PLAN" entityid:[ActivityPlansArr[indexPath.row] valueForKey:@"planID"]];
        [helper delete_ActivityPlan:@"ActivityPlans" value:[ActivityPlansArr[indexPath.row] valueForKey:@"planID"]];
    }
    else if (tableView == ActivitiesTableView)
    {
        [helper serverdelete:@"PLAN_ACT" entityid:[PlanActivitiesArr[indexPath.row] valueForKey:@"iD"]];
        [self getplansdata];
        [self getactivitiesdata];
        [helper delete_ActivitiesinPlan:@"PlanActivities" planid:[PlanActivitiesArr[indexPath.row] valueForKey:@"planID"] activityid:[PlanActivitiesArr[indexPath.row] valueForKey:@"iD"]];
    }
    [self refreshdata];
}
#pragma mark unwind method
-(IBAction)unwindToActivityPlan:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
    [self getplansdata];
    [self getactivitiesdata];
}
#pragma mark edit activity plans
-(IBAction)EditActivityPlanButtonAction:(id)sender
{
    CGPoint textfieldPosition = [sender convertPoint:CGPointZero toView:ActivityPlanTableView];
    NSIndexPath *indexPath = [ActivityPlanTableView indexPathForRowAtPoint:textfieldPosition];
    TypeofActionStr = @"Edit";
    PlanId = [ActivityPlansArr[indexPath.row] valueForKey:@"planID"];
    [self performSegueWithIdentifier:@"ActivityPlanToAddActivityPlan" sender:self];
}

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}
-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}
@end
