//
//  ActivityView.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 14/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "SOCustomCell.h"
#import "Activity_detail.h"
#import "AppDelegate.h"
#import "updateactivity_ViewController.h"
#import "UpsertActivityView.h"
#import "EventDetails_ViewController.h"

@interface ActivityView : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UITableView *Activitylistview;
@property(strong,nonatomic) HelperClass *helper;
@property(strong,nonatomic) Activity_detail *actdetailview;
@property (strong, nonatomic) IBOutlet UIButton *AddActivityPlanButtonOutlet;
@property (strong, nonatomic) IBOutlet UISearchBar *activity_searchbar;
@property(strong,nonatomic)NSString *TypeOfActionStr;
- (IBAction)add_activity:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *AddActivityButtonOutlet;
- (IBAction)AddActivityPlanButtonAction:(id)sender;


-(void) refreshactivitylist;




@end
