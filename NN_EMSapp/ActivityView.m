//
//  ActivityView.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 14/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "ActivityView.h"


@interface ActivityView ()
{
    NSInteger ln_expandedRowIndex;
    NSArray *AssignedActivitylist;
    NSInteger count;
    AppDelegate *appdelegate;
    NSPredicate *eventidpredicate;
    NSString * sel_activityid;
    NSMutableArray *GroupedActivityArr;
    NSInteger SelectedSection;
    NSPredicate *ActivityPredicate;
    int RefreshTableview;
}

@end

@implementation ActivityView
@synthesize helper,Activitylistview,actdetailview,activity_searchbar,TypeOfActionStr,AddActivityButtonOutlet,AddActivityPlanButtonOutlet;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Activitylistview.delegate = self;
    Activitylistview.dataSource = self;
   
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    actdetailview = [[Activity_detail alloc]init];
    
    [activity_searchbar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.activity_searchbar valueForKey:@"_searchField"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    activity_searchbar.delegate = self;
    
    [self refreshdata];
    [self refreshactivitylist];
    
    if (![appdelegate.EventUserRoleArray containsObject:@"E_ACT_MNG"])
    {
        AddActivityButtonOutlet.hidden = YES;
        AddActivityPlanButtonOutlet.hidden = YES;
    }

    
    [self langsetuptranslations];
}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshTableview == 1)
    {
        Activitylistview.delegate = self;
        Activitylistview.dataSource = self;
        [Activitylistview reloadData];
    }
}
-(void)langsetuptranslations
{
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//---------------------Selection Functions start here
//------------------Table View Delegates
#pragma mark tableview delegate method
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SOCustomCell *customcell;
    NSString *cellidentifier = @"HeaderCustomcell";
    customcell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    
    UILabel *ActivityPlanNameLbl = [customcell viewWithTag:1];
    UIButton *Addactivity = [[UIButton alloc] initWithFrame:CGRectMake(837, 0, 45, 45)];
    Addactivity.tag = section;
    [customcell addSubview:Addactivity];
    
    
    UILabel *daysleft_ulbl = (UILabel *) [customcell viewWithTag:1901];
    UILabel *act_type_ulbl = (UILabel *) [customcell viewWithTag:1902];
    UILabel *actdescription_name = (UILabel *) [customcell viewWithTag:1903];
    UILabel *ownedby_ulbl = (UILabel *) [customcell viewWithTag:1904];
    UILabel *status_ulbl = (UILabel *) [customcell viewWithTag:1905];
    
    daysleft_ulbl.text = [helper gettranslation:@"LBL_076"];
    act_type_ulbl.text = [helper gettranslation:@"LBL_266"];
    actdescription_name.text = [helper gettranslation:@"LBL_005"];
    ownedby_ulbl.text = [helper gettranslation:@"LBL_187"];
    status_ulbl.text = [helper gettranslation:@"LBL_242"];
    
    
    ActivityPlanNameLbl.text = [GroupedActivityArr[section] valueForKey:@"PlanName"];
    
    [Addactivity addTarget:self action:@selector(addnewActivityClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIView * view = [[UIView alloc] init];
    [view addSubview:customcell];
    
    return view;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 84;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [GroupedActivityArr count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (SelectedSection == indexPath.section)
    {
        if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
        {
            return 157;
        }
        else
            return 40;
    }
    else
    return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *ActivityArr = [GroupedActivityArr[section] valueForKey:@"Activities"];
    
    if (SelectedSection == section)
    {
        return [ActivityArr count] + (ln_expandedRowIndex != -1 ? 1 : 0);
    }
    else
        return [ActivityArr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = [indexPath row];
    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
  
    //if (!ln_expandedCell)
  //  {
        SOCustomCell *customcell;
        UIImage *image;
        NSString *cellIdentifier = @"customcell";
        
        NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
       
        NSArray *ActivityArr = [GroupedActivityArr[indexPath.section] valueForKey:@"Activities"];
        if (indexPath.row >= ActivityArr.count)
        {
            [SelectedArray insertObject:ActivityArr[indexPath.row - 1] atIndex:0];
        }
        else
        {
            [SelectedArray insertObject:ActivityArr[indexPath.row] atIndex:0];
        }
        customcell = [self.Activitylistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

        
        UIView *contentView = customcell.contentView;
        
    
//        UIView *contentView = customcell.contentView;
        customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        
        
        UIImageView *duration = (UIImageView *) [contentView viewWithTag:1];
        UILabel *daysleft = (UILabel *) [contentView viewWithTag:2];
        UILabel *act_type = (UILabel *) [contentView viewWithTag:3];
        UILabel *event_name = (UILabel *) [contentView viewWithTag:4];
        UILabel *ownedby = (UILabel *) [contentView viewWithTag:5];
        UILabel *status = (UILabel *) [contentView viewWithTag:6];
        UILabel *priority = (UILabel *) [contentView viewWithTag:7];
        UIButton *edit_btn = (UIButton *) [contentView viewWithTag:8];
        
        NSString *canedit = [SelectedArray[0] valueForKey:@"canEdit"];
    if ([canedit isEqualToString:@"N"])
    {
        [edit_btn setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [edit_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        
    }
    
        [edit_btn addTarget:self action:@selector(editactivityClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [daysleft setText:[helper geteventduration:[SelectedArray[0] valueForKey:@"startdate"] eDate:[SelectedArray[0] valueForKey:@"enddate"]]];
        [act_type setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"type"]]];
        event_name.text = [SelectedArray[0] valueForKey:@"desc"];
        [ownedby setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"ownedby"]]];
        [status setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"status"]]];
        [priority setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"priority"]]];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        
        NSDate *sDate = [dateFormatter dateFromString:[SelectedArray[0] valueForKey:@"startdate"]];
        NSDate *eDate = [dateFormatter dateFromString:[SelectedArray[0] valueForKey:@"enddate"]];
        
        if ([sDate timeIntervalSinceNow] > 0.0)
        {
            image = [UIImage imageNamed:@"list_top.png"];
        }
        else if ([eDate timeIntervalSinceNow] > 0.0)
        {
            image = [UIImage imageNamed:@"list_equal.png"];
        }
        else
        {
            image = [UIImage imageNamed:@"list_bottom.png"];
        }
        [duration setImage:image];
        return customcell;
       
    //}
//    else
//    {
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
//        if (!cell)
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
//        [cell.contentView addSubview:actdetailview];
//        
//        NSArray *ActivityArr = [GroupedActivityArr[indexPath.section] valueForKey:@"Activities"];
//        
//        [self setupActdetails:[ActivityArr[indexPath.row-1] valueForKey:@"activityid"] backcolor:cell.backgroundColor];
//         cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
//        return cell;
//    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

   
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
    NSArray *ActivityArr = [GroupedActivityArr[indexPath.section] valueForKey:@"Activities"];
    appdelegate.canEdit=[ActivityArr[indexPath.row] valueForKey:@"canEdit"];
    
    NSLog(@"canEdit:%@",[ActivityArr[indexPath.row] valueForKey:@"canEdit"]);
    appdelegate.activityid = [ActivityArr[indexPath.row] valueForKey:@"activityid"];
    [self.parentViewController performSegueWithIdentifier:@"ActivityListtoEditUpsertActivity" sender:self];
    
    
}
/*- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSInteger row = [indexPath row];
    
   
    SelectedSection = [indexPath section];
    
    
    BOOL preventReopen = NO;
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
   [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:SelectedSection]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:SelectedSection]] withRowAnimation:UITableViewRowAnimationTop];
        cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
    }
    [tableView endUpdates];
    return nil;
}*/

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"desect called ");
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *ActivityArr = [GroupedActivityArr[indexPath.section] valueForKey:@"Activities"];
    if (indexPath.row < [ActivityArr count])
    {
        if ([[ActivityArr[indexPath.row] valueForKey:@"canDelete"] isEqualToString:@"Y"])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
    

}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *ActivityArr = [GroupedActivityArr[indexPath.section] valueForKey:@"Activities"];
    [helper serverdelete:@"ACTIVITY" entityid:[ActivityArr[indexPath.row] valueForKey:@"activityid"]];
    ActivityPredicate = [NSPredicate predicateWithFormat:@"activityid like %@",[ActivityArr[indexPath.row] valueForKey:@"activityid"]];
    [helper delete_predicate:@"Activities" predicate:ActivityPredicate];
    [self refreshdata];
    [self refreshactivitylist];
}
-(void) setupActdetails:(NSString *)act_id backcolor:(UIColor *)backcolor
{
    NSArray *actdata = [helper query_alldata:@"Activities"];
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"activityid like %@",act_id];
    actdata = [actdata filteredArrayUsingPredicate:aPredicate];
    
    if ([actdata count]>0)
    {
        [self.actdetailview.activity_desc setText:[actdata[0] valueForKey:@"desc"]];
        [self.actdetailview.activity_comment setText:[actdata[0] valueForKey:@"Comment"]];
        self.actdetailview.activity_comment.userInteractionEnabled = YES;
        [self.actdetailview.activity_commentdate setText:[actdata[0] valueForKey:@"activity_commentdate"]];
        [self.actdetailview.activity_commentby setText:[actdata[0] valueForKey:@"activity_commentby"]];
        [self.actdetailview.prioritylbl setText:[actdata[0] valueForKey:@"priority"]];
    
        
        self.actdetailview.actdesc_ulbl.text = [helper gettranslation:@"LBL_595"];
        self.actdetailview.actcomment_ulbl.text = [helper gettranslation:@"LBL_601"];
        self.actdetailview.actstime_ulbl.text = [helper gettranslation:@"LBL_240"];
        self.actdetailview.actetime_ulbl.text = [helper gettranslation:@"LBL_123"];
        self.actdetailview.actpri_ulbl.text = [helper gettranslation:@"LBL_203"];

        
        /*NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        NSDate *sdate = [dateFormatter dateFromString:[actdata[0] valueForKey:@"startdate"]];
        NSDate *edate = [dateFormatter dateFromString:[actdata[0] valueForKey:@"enddate"]];
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
    
        [self.actdetailview.activity_startdate setText:[dateFormatter stringFromDate:sdate]];
        [self.actdetailview.activity_enddate setText:[dateFormatter stringFromDate:edate]];*/
        
        [self.actdetailview.activity_startdate setText:[helper formatingdate:[actdata[0] valueForKey:@"startdate"] datetime:@"FormatDate"]];
        [self.actdetailview.activity_enddate setText: [helper formatingdate:[actdata[0] valueForKey:@"enddate"] datetime:@"FormatDate"]];
    
    [self.actdetailview.mainview setBackgroundColor:backcolor];
    }
    
}
#pragma mark search bar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == activity_searchbar)
    {
        [self refreshdata];
        if(searchText.length == 0)
        {
          [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.type contains[c] %@) OR (SELF.plan_name contains[c] %@) OR (SELF.ownedby contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.desc contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[AssignedActivitylist filteredArrayUsingPredicate:predicate]];
            AssignedActivitylist = [filtereventarr copy];
        }
        [self refreshactivitylist];
    }
}
#pragma mark edit activity method
-(IBAction)editactivityClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:Activitylistview];
    NSIndexPath *indexPath = [Activitylistview indexPathForRowAtPoint:buttonPosition];
    
    NSArray *ActivityArr = [GroupedActivityArr[indexPath.section] valueForKey:@"Activities"];
    if (indexPath != nil)
    {
       
        appdelegate.activityid = [ActivityArr[indexPath.row] valueForKey:@"activityid"];
        appdelegate.canEdit= [ActivityArr[indexPath.row] valueForKey:@"canEdit"];

        //NSLog(@"Activity id is %@",appdelegate.activityid);
        //NSLog(@"Activity id here is %@",[ActivityArr[indexPath.row] valueForKey:@"activityid"]);
    }
    
    [self.parentViewController performSegueWithIdentifier:@"ActivityListtoEditUpsertActivity" sender:self];
}

#pragma mark add new activity to plan method
-(IBAction)addnewActivityClicked:(id)sender
{
    UIButton *clickedButton = (UIButton*)sender;
    NSLog(@"GroupedActivityArr: %@", [GroupedActivityArr[clickedButton.tag] valueForKey:@"PlanName"]);

}





#pragma mark unwind method

-(IBAction)unwindtoEventActivitylist:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
    [self refreshdata];
    [ self refreshactivitylist ];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}

- (IBAction)add_activity:(id)sender
{
    NSLog(@"Added New Activity");
    appdelegate.activityid = @"";
    
    
    [self.parentViewController performSegueWithIdentifier:@"ActivityListtoUpsertActivity" sender:self];
   
}
-(void)refreshdata
{
    AssignedActivitylist = [helper query_alldata:@"Activities"];
    eventidpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:eventidpredicate];


}
- (IBAction)AddActivityPlanButtonAction:(id)sender
{
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToAddEventActivityPlan" sender:self];
}

-(void) refreshactivitylist
{
    SelectedSection = -1;
    ln_expandedRowIndex = -1;
    GroupedActivityArr = [[NSMutableArray alloc]init];
    NSArray *groups = [AssignedActivitylist valueForKeyPath:@"@distinctUnionOfObjects.plan_name"];
   
    for (NSString *groupId in groups)
    {
        NSMutableDictionary *entry = [NSMutableDictionary new];
        [entry setObject:groupId forKey:@"PlanName"];
        NSArray *groupNames = [AssignedActivitylist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"plan_name = %@", groupId]];
        [entry setObject:groupNames forKey:@"Activities"];
        [GroupedActivityArr addObject:entry];
        
    }
    
    // Add adhoc activity plan
    if ( ![groups containsObject: @"Ad-Hoc Activities"] )
    {
        NSMutableDictionary *adhoc_entry = [NSMutableDictionary new];
        [adhoc_entry setObject:@"Ad-Hoc Activities" forKey:@"PlanName"];
        [GroupedActivityArr addObject:adhoc_entry];
    }
    Activitylistview.delegate = self;
    Activitylistview.dataSource = self;
    [Activitylistview reloadData];
   
   
}

@end
