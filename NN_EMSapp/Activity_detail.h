//
//  Activity_detail.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface Activity_detail : UIView

@property (strong, nonatomic) IBOutlet UITextView *activity_desc;
@property (strong, nonatomic) IBOutlet UITextView *activity_comment;
@property (strong, nonatomic) IBOutlet UILabel *activity_commentdate;
@property (strong, nonatomic) IBOutlet UILabel *activity_commentby;
@property (strong, nonatomic) IBOutlet UILabel *activity_startdate;
@property (strong, nonatomic) IBOutlet UILabel *activity_enddate;
@property(strong,nonatomic)IBOutlet UILabel *prioritylbl;

@property(retain,nonatomic)IBOutlet UIButton *editButon;

@property (strong,nonatomic) IBOutlet UIView *mainview;


@property (strong, nonatomic) IBOutlet UILabel *actdesc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actcomment_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actstime_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actetime_ulbl;
@property(strong,nonatomic)IBOutlet UILabel *actpri_ulbl;




@end
