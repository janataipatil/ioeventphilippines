//
//  Activity_detail.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Activity_detail.h"

@implementation Activity_detail


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"Activity_detail" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    
    self.activity_desc = (UITextView *)[mainView viewWithTag:1];
    self.activity_comment = (UITextView *)[mainView viewWithTag:2];
    self.activity_commentdate = (UILabel *)[mainView viewWithTag:3];
    self.activity_commentby = (UILabel *)[mainView viewWithTag:4];
    self.mainview = (UIView *)[mainView viewWithTag:5];
    self.editButon = (UIButton *)[mainView viewWithTag:6];
    self.activity_startdate = (UILabel *)[mainView viewWithTag:7];
    self.activity_enddate = (UILabel *)[mainView viewWithTag:8];
    self.prioritylbl = (UILabel *)[mainView viewWithTag:9];
    
    
    _actdesc_ulbl = (UILabel *)[mainView viewWithTag:1901];
    _actcomment_ulbl = (UILabel *)[mainView viewWithTag:1905];
    _actstime_ulbl = (UILabel *)[mainView viewWithTag:1903];
    _actetime_ulbl = (UILabel *)[mainView viewWithTag:1904];
    _actpri_ulbl = (UILabel *)[mainView viewWithTag:1902];
    
    
    return self;
}

@end
