//
//  AddActivitiesInPlanViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 24/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddActivitiesInPlanViewController.h"

@interface AddActivitiesInPlanViewController ()
{
    NSString *TextfieldFlag;
    NSArray *LovArr;
    NSString *ActivityId;
    AppDelegate *appdelegate;
    NSString *transaction_type;
    NSArray *ActivityArr;
    
    NSUInteger stringlength;
}

@end

@implementation AddActivitiesInPlanViewController
@synthesize helper,TypeTextField,StatusTextField,DurationTextField,PriorityTextField,DescriptionTextField,LOVTableView,PlanId,TypeofActionStr,AddEditActivitylbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self langsetuptranslations];
    
    [StatusTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [TypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [PriorityTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    LOVTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableView.layer.borderWidth = 1;
    
    if ([TypeofActionStr isEqualToString:@"Add"])
    {
        ActivityId = [helper generate_rowId];
        transaction_type = @"Add Activities in plan";
//        LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
//        NSArray *StatusArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Active"]];
//        if ([StatusArr count]>0)
//        {
           // StatusTextField.text = [StatusArr[0] valueForKey:@"value"];
       // }
        StatusTextField.text = [helper getvaluefromlic:@"REC_STAT" lic:@"Active"];
        AddEditActivitylbl.text = @"Add Activity";
        
    }
    else if ([TypeofActionStr isEqualToString:@"Edit"])
    {
        AddEditActivitylbl.text = @"Edit Activity";
        transaction_type = @"Edit Activities in plan";
        [self setUpActivityView];
    }
   
 
    
}
-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancel_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    
    _attachlabel.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_011"]];
    
    
    _priority_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_203"]];
    PriorityTextField.placeholder=[helper gettranslation:@"LBL_757"];
    [self markasrequired:_priority_ulbl];
    _status_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_242"]];
    [self markasrequired:_status_ulbl];
    _type_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_266"]];
    [self markasrequired:_type_ulbl];
    TypeTextField.placeholder=[helper gettranslation:@"LBL_757"];
    _description_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_094"]];

    [self markasrequired:_description_ulbl];
}

-(void) markasrequired:(UILabel *) label
{
    int labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
}


-(void)setUpActivityView
{
    ActivityArr = [[helper query_alldata:@"PlanActivities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",PlanId]];
    if ([ActivityArr count]>0)
    {
        DescriptionTextField.text = [ActivityArr[0] valueForKey:@"activityDescription"];
        DurationTextField.text = [ActivityArr[0] valueForKey:@"duration"];
        StatusTextField.text = [ActivityArr[0] valueForKey:@"status"];
        TypeTextField.text = [ActivityArr[0] valueForKey:@"type"];
        PriorityTextField.text = [ActivityArr[0] valueForKey:@"priority"];
        ActivityId = [ActivityArr[0] valueForKey:@"iD"];
    }
}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if ([TextfieldFlag isEqualToString:@"TypeTextField"] )
        {
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
        }
        else if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if ([TextfieldFlag isEqualToString:@"PriorityTextField"])
        {
          LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];
        }
    }
    else
    {
        if ([TextfieldFlag isEqualToString:@"TypeTextField"] )
        {
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
        }
        else if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if ([TextfieldFlag isEqualToString:@"PriorityTextField"])
        {
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];
        }

        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[LovArr filteredArrayUsingPredicate:filterpredicate]];
        LovArr = [filteredpartArray copy];
        
    }
    [LOVTableView reloadData];
  
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)SaveButtonAction:(id)sender
{
    
    NSString *PriorityLIC = [helper getLic:@"PRIORITY" value:PriorityTextField.text];
    NSString *StatusLIC = [helper getLic:@"REC_STAT" value:StatusTextField.text];
    NSString *TypeLIC = [helper getLic:@"ACT_TYPE" value:TypeTextField.text];
    
    
    if([TypeofActionStr isEqualToString:@"Edit"])
    {
        if ([ActivityArr count]>0)
        {
            if ([DescriptionTextField.text isEqualToString:[ActivityArr[0] valueForKey:@"activityDescription"]?: @""] && [DurationTextField.text isEqualToString:[ActivityArr[0] valueForKey:@"duration"]?: @""] && [StatusTextField.text isEqualToString:[ActivityArr[0] valueForKey:@"status"]?: @""] && [TypeTextField.text isEqualToString:[ActivityArr[0] valueForKey:@"type"]?: @""] && [PriorityTextField.text isEqualToString:[ActivityArr[0] valueForKey:@"priority"]?: @""])
            {
                [self CancelButtonAction:self];
                return;
            }
            else
            {
              [helper delete_ActivitiesinPlan:@"PlanActivities" planid:PlanId activityid:ActivityId];
            }
        }
    }
    if ([DescriptionTextField.text length]>0 && [StatusTextField.text length]>0 && [TypeTextField.text length]>0 && [PriorityTextField.text length]>0)
    {
        NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
        [PayloadDict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
        [PayloadDict setValue:DescriptionTextField.text forKey:@"Description"];
        [PayloadDict setValue:DurationTextField.text forKey:@"Duration"];
        [PayloadDict setValue:ActivityId forKey:@"ID"];
        [PayloadDict setValue:PlanId forKey:@"PlanID"];
        [PayloadDict setValue:PriorityLIC forKey:@"PriorityLIC"];
        [PayloadDict setValue:StatusLIC forKey:@"StatusLIC"];
        [PayloadDict setValue:TypeLIC forKey:@"TypeLIC"];
        [PayloadDict setValue:PriorityTextField.text forKey:@"Priority"];
        [PayloadDict setValue:StatusTextField.text forKey:@"Status"];
        [PayloadDict setValue:TypeTextField.text forKey:@"Type"];
        NSMutableArray *PayloadArray = [[NSMutableArray alloc]init];
        [PayloadArray addObject:PayloadDict];
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArray options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetPlanAct" pageno:@"" pagecount:@"" lastpage:@""];
    
        appdelegate.senttransaction = @"Y";
        [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Add Activity in Plan"  Status:@"Open"];
        /*NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
           
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            NSLog(@"Json data is %@",jsonData);
        }*/
        [helper insertPlanActivities:PayloadArray];
        [self CancelButtonAction:self];
    }
    else
    {
        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] preferredStyle:UIAlertControllerStyleAlert];
        
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:takephotoalert animated:YES completion:nil];
        
    }
}

- (IBAction)CancelButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromAddActivityInPlanToAddActivity" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LovArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *Lovlbl = (UILabel *)[contentView viewWithTag:1];
    Lovlbl.text = [LovArr[indexPath.row] valueForKey:@"value"];
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([TextfieldFlag isEqualToString:@"StatusTextField"])
    {
        StatusTextField.text = [LovArr[indexPath.row] valueForKey:@"value"];
        [StatusTextField resignFirstResponder];
    }
    else if ([TextfieldFlag isEqualToString:@"TypeTextField"])
    {
        TypeTextField.text = [LovArr[indexPath.row] valueForKey:@"value"];
        [TypeTextField resignFirstResponder];
    }
    else if ([TextfieldFlag isEqualToString:@"PriorityTextField"])
    {
        PriorityTextField.text = [LovArr[indexPath.row] valueForKey:@"value"];
        [PriorityTextField resignFirstResponder];
    }
    LOVTableView.hidden = YES;
    
}
#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == TypeTextField)
    {
        TextfieldFlag = @"TypeTextField";
        LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
    }
    else if (textField == StatusTextField)
    {
        TextfieldFlag = @"StatusTextField";
        LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
      }
    else if (textField == PriorityTextField)
    {
        TextfieldFlag = @"PriorityTextField";
        LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];
    }
//    if (textField == DurationTextField )
//    {
//        [helper MoveViewUp:YES Height:140 ViewToBeMoved:self.view];
//    }
    if (textField == StatusTextField)
    {
       [helper MoveViewUp:YES Height:200 ViewToBeMoved:self.view];
    }
    if (textField == TypeTextField || textField == PriorityTextField)
    {
        [helper MoveViewUp:YES Height:235 ViewToBeMoved:self.view];
    }
    if (textField == StatusTextField || textField == TypeTextField || textField == PriorityTextField)
    {
        [LOVTableView reloadData];
       [self adjustHeightOfTableview:LOVTableView];
        LOVTableView.hidden = NO;
    }
    if (textField == DescriptionTextField)
    {
        stringlength = 100;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (textField == DurationTextField )
//    {
//        [helper MoveViewUp:NO Height:140 ViewToBeMoved:self.view];
//    }
    if (textField == StatusTextField)
    {
        [helper MoveViewUp:NO Height:200 ViewToBeMoved:self.view];
        if (![helper isdropdowntextCorrect:StatusTextField.text lovtype:@"REC_STAT"])
        {
            StatusTextField.text = @"";
        }
    }
    if (textField == TypeTextField || textField == PriorityTextField)
    {
        [helper MoveViewUp:NO Height:235 ViewToBeMoved:self.view];
    }
    if (textField == TypeTextField)
    {
        if (![helper isdropdowntextCorrect:TypeTextField.text lovtype:@"ACT_TYPE"])
        {
            TypeTextField.text = @"";
        }
    }
    if (textField == PriorityTextField)
    {
        if (![helper isdropdowntextCorrect:PriorityTextField.text lovtype:@"PRIORITY"])
        {
            PriorityTextField.text = @"";
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
   
        CGFloat height = LOVTableView.contentSize.height;
        CGFloat maxHeight = LOVTableView.superview.frame.size.height - LOVTableView.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableView.frame;
           
       
                if([TextfieldFlag isEqualToString:@"TypeTextField"])
                {
                    frame.origin.x = 227;
                    frame.origin.y = 467;
                }
                if([TextfieldFlag isEqualToString:@"StatusTextField"])
                {
                    frame.origin.x = 527;
                    frame.origin.y = 410;
                }
                if([TextfieldFlag  isEqualToString:@"PriorityTextField"])
                {
                    frame.origin.x = 227;
                    frame.origin.y = 410;
                }
            LOVTableView.frame = frame;
        }];
    
}
#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (LOVTableView.hidden == NO)
    {
        LOVTableView.hidden = YES;
        [StatusTextField resignFirstResponder];
        [TypeTextField resignFirstResponder];
        [PriorityTextField resignFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == DescriptionTextField)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}
@end
