//
//  AddActivityPlanViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 23/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"

@interface AddActivityPlanViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITableView *StatusTableview;
}
@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic)IBOutlet UITextField *PlanNameTextField;
@property(strong,nonatomic)IBOutlet UITextField *DescriptionTextField;
@property(strong,nonatomic)IBOutlet UITextField *StatusTextField;
@property(strong,nonatomic)NSString *TypeofActionStr;
@property(strong,nonatomic)NSString *PlanId;
@property (strong, nonatomic) IBOutlet UILabel *AddEditActivitylbl;

- (IBAction)SaveButtonAction:(id)sender;



- (IBAction)CancelButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *planname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *description_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancel_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *attachlabel;



@end
