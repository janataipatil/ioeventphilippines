//
//  AddActivityPlanViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 23/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddActivityPlanViewController.h"

@interface AddActivityPlanViewController ()
{
    NSArray *StatusLOVArr;
    NSString *PlanId;
    AppDelegate *appdelegate;
    NSString *transaction_type;
    NSArray *ActivityPlanArr;
    NSPredicate *RecordStatusPredicate;
    
    NSUInteger stringlength;
    
}

@end

@implementation AddActivityPlanViewController
@synthesize helper,StatusTextField,DescriptionTextField,PlanNameTextField,TypeofActionStr,AddEditActivitylbl,PlanId;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [StatusTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    RecordStatusPredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    if ([TypeofActionStr isEqualToString:@"Add"])
    {
        AddEditActivitylbl.text = [helper gettranslation:@"LBL_012"];
        PlanId = [helper generate_rowId];
        transaction_type = @"Add Activity Plan";
        //prepopulate the status value in form
      /*  NSArray *StatusArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Active"]];
        if ([StatusArr count]>0)
        {
            StatusTextField.text = [StatusArr[0] valueForKey:@"value"];
        }*/
        
        StatusTextField.text = [helper getvaluefromlic:@"REC_STAT" lic:@"Active"];

    }
    else if ([TypeofActionStr isEqualToString:@"Edit"])
    {
        AddEditActivitylbl.text = [helper gettranslation:@"LBL_105"];
        [self SetupActivityPlan];
    }
    StatusTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    StatusTableview.layer.borderWidth = 1;
    
    [self langsetuptranslations];
}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancel_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    
    _attachlabel.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_012"]];

    _planname_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_201"]];
    [self markasrequired:_planname_ulbl];
    
    _status_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_242"]];
    
    _description_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_094"]];
    
    
    
}

-(void) markasrequired:(UILabel *) label
{
    int labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark setupactivity plan
-(void)SetupActivityPlan
{
    ActivityPlanArr = [[helper query_alldata:@"ActivityPlans"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",PlanId]];
    if ([ActivityPlanArr count]>0)
    {
        PlanNameTextField.text = [ActivityPlanArr[0] valueForKey:@"name"];
        StatusTextField.text = [ActivityPlanArr[0] valueForKey:@"status"];
        DescriptionTextField.text = [ActivityPlanArr[0] valueForKey:@"activityPlandescription"];
        PlanId = [ActivityPlanArr[0] valueForKey:@"planID"];
    }
}
- (IBAction)SaveButtonAction:(id)sender
{
    NSString *status = [helper getLic:@"REC_STAT" value:StatusTextField.text];
    
    if([PlanNameTextField.text length] >0)
    {
         NSError *error;
        //plan name should not be nil
        if([TypeofActionStr isEqualToString:@"Edit"])
        {
            if ([ActivityPlanArr count]>0)
            {
                if ([PlanNameTextField.text isEqualToString:[ActivityPlanArr[0] valueForKey:@"name"]?: @""] && [DescriptionTextField.text isEqualToString:[ActivityPlanArr[0] valueForKey:@"activityPlandescription"]?: @""] && [StatusTextField.text isEqualToString:[ActivityPlanArr[0] valueForKey:@"status"]?: @""])
                {
                    [self CancelButtonAction:self];
                    return;
                }
                else
                {
                    [helper delete_ActivityPlan:@"ActivityPlans" value:PlanId];
                }
            }
        }
            
        NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
        [PayloadDict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
        [PayloadDict setValue:DescriptionTextField.text forKey:@"Description"];
        [PayloadDict setValue:PlanNameTextField.text forKey:@"Name"];
        [PayloadDict setValue:PlanId forKey:@"PlanID"];
        [PayloadDict setValue:status forKey:@"StatusLIC"];
        [PayloadDict setValue:StatusTextField.text forKey:@"Status"];
        NSMutableArray *PayloadArray = [[NSMutableArray alloc]init];
        [PayloadArray addObject:PayloadDict];
        NSString *jsonString;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArray options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetActPlan" pageno:@"" pagecount:@"" lastpage:@""];
        
        appdelegate.senttransaction = @"Y";
        [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Add ActivityPlan"  Status:@"Open"];
       /* NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"]  message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            NSLog(@"json data is %@",jsonData);
        
        }*/
        [helper insertActivityPlans:PayloadArray];
        [self CancelButtonAction:self];
    }
    else
    {
        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_491"] preferredStyle:UIAlertControllerStyleAlert];
        
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
                                   
        [self presentViewController:takephotoalert animated:YES completion:nil];
                                   

    }
        
}

- (IBAction)CancelButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromAddActivityPlanToAddActivity" sender:self];
     [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        StatusLOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:RecordStatusPredicate];
    }
    else
    {
        StatusLOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:RecordStatusPredicate];
        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[StatusLOVArr filteredArrayUsingPredicate:filterpredicate]];
        StatusLOVArr = [filteredpartArray copy];

    }
   [StatusTableview reloadData];
    
    
}
#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == PlanNameTextField)
    {
        stringlength = 50;
    }
    if (textField == DescriptionTextField)
    {
        stringlength = 100;
    }
    if (textField == StatusTextField)
    {
        StatusLOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:RecordStatusPredicate];
        [StatusTableview reloadData];
        StatusTableview.hidden = NO;

    }
    [helper MoveViewUp:YES Height:140 ViewToBeMoved:self.view];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [helper MoveViewUp:NO Height:140 ViewToBeMoved:self.view];
    if (textField == StatusTextField)
    {
        if (![helper isdropdowntextCorrect:StatusTextField.text lovtype:@"REC_STAT"])
        {
            StatusTextField.text = @"";
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [StatusLOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [StatusTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *statuslbl = (UILabel *)[contentView viewWithTag:1];
    statuslbl.text = [StatusLOVArr[indexPath.row] valueForKey:@"value"];
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    StatusTextField.text = [StatusLOVArr[indexPath.row] valueForKey:@"value"];
    StatusTableview.hidden = YES;
    [StatusTextField resignFirstResponder];
}

#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (StatusTableview.hidden == NO)
    {
        StatusTableview.hidden = YES;
        [StatusTextField resignFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == PlanNameTextField || textField == DescriptionTextField)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}

@end
