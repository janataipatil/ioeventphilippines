//
//  AddAttachment.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 03/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"

@interface AddAttachment : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentPickerDelegate,UITextFieldDelegate>
- (IBAction)CancelButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *AttachmentTypeTextfield;
@property (weak, nonatomic) IBOutlet UIImageView *groupDropDownImg;
@property (strong, nonatomic) IBOutlet UITableView *AttachmentTypeTableview;
- (IBAction)CameraButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *FileNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *DescriptionTextField;
@property (strong, nonatomic) IBOutlet UITextField *FileTypeTextfield;
@property (strong, nonatomic) IBOutlet UITextField *UploadDateTextfield;
@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic) UIImage *capturedlicensimg;
@property (strong, nonatomic) IBOutlet UITextField *FileSizeTextField;
//- (IBAction)IcloudButtonAction:(id)sender;
//- (IBAction)CancelButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *group_lbl;
@property (weak, nonatomic) IBOutlet UITextField *group_tv;

@property (strong, nonatomic)NSString *filename;
@property (strong, nonatomic)NSString *filesize;
@property (strong, nonatomic)NSString *filetype;
@property (strong, nonatomic)NSString *uploadDate;
@property (strong, nonatomic)NSString *speakerID;
@property (strong, nonatomic)NSString *attendeeID;

@property (strong, nonatomic)NSData  *filedata;
;


@property (strong, nonatomic) IBOutlet UISwitch *PublicSwitchButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *addAttachmentBtn;


- (IBAction)SaveButtonAction:(id)sender;

- (IBAction)PublicSwitchButtonAction:(id)sender;
@property (strong, nonatomic) NSString *senderView;

@property (strong, nonatomic) IBOutlet UILabel *attachmenttype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *filename_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *public_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *filetype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *filesize_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *description_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancel_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *attachlabel;
@property (weak, nonatomic) IBOutlet UIView *groupView_underline;






@end
