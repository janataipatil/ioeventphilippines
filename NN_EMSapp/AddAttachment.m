//
//  AddAttachment.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 03/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddAttachment.h"
#import <AssetsLibrary/AssetsLibrary.h>
@interface AddAttachment ()
{
    NSArray *AttachmentTypeArr;
    NSString *attachedimagename;
    NSString *PathExtension;
    NSMutableData *responsedata;
    dispatch_semaphore_t sem_att;
    NSData *filedata;
    NSString *StorageURL;
    NSString *RandomnId;
    AppDelegate *appedelegate;
    NSString *transaction_type;
    NSString *Public;
    NSUInteger count;
    int publicswitchtag;
    NSManagedObjectContext *context;
    SLKeyChainStore *keychain;

    NSUInteger stringlength;
    NSArray *groupArray;
     BOOL imageUploaded;
    NSString *selectedGroupID;
    NSString *textFieldFlag;
}

@end

@implementation AddAttachment
@synthesize AttachmentTypeTextfield,AttachmentTypeTableview,filedata,FileNameTextField,FileSizeTextField,FileTypeTextfield,DescriptionTextField,UploadDateTextfield,helper,PublicSwitchButtonOutlet,capturedlicensimg;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appedelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appedelegate managedObjectContext];

    PublicSwitchButtonOutlet.transform = CGAffineTransformMakeScale(0.60, 0.60);
    [PublicSwitchButtonOutlet setOn:NO animated:YES];
    Public = @"N";
    self.group_tv.delegate=self;
    if([_senderView isEqualToString:@"Speaker"])
    {
        [_addAttachmentBtn setHidden:YES];
        [PublicSwitchButtonOutlet setHidden:YES];
        [_public_ulbl setHidden:YES];
        [_group_tv setHidden:YES];
        [_group_lbl setHidden:YES];
        [_groupDropDownImg setHidden:YES];
        [_groupView_underline setHidden:YES];
        
        FileNameTextField.text=_filename;
        FileSizeTextField.text=_filesize;
        FileTypeTextfield.text=_filetype;
        
         AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPK_ATT_TYPE"]];
       // AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"SPEAKER"]];
    
        
    }
    else if([_senderView isEqualToString:@"Attendee"])
    {
        FileNameTextField.text=_filename;
        FileSizeTextField.text=_filesize;
        FileTypeTextfield.text=_filetype;
        
        [PublicSwitchButtonOutlet setHidden:YES];
        [_addAttachmentBtn setHidden:YES];
        [_public_ulbl setHidden:YES];
        [_group_tv setHidden:YES];
        [_group_lbl setHidden:YES];
        [_groupDropDownImg setHidden:YES];
        [_groupView_underline setHidden:YES];
        
         AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATTENDEE_ATT_TYPE"]];
       // AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"EVT_ATT"]];
        
        
    }
    else if([_senderView isEqualToString:@"Itenary"])
    {
        
        FileNameTextField.text=_filename;
        FileSizeTextField.text=_filesize;
        FileTypeTextfield.text=_filetype;
        
        [PublicSwitchButtonOutlet setHidden:YES];
        [_addAttachmentBtn setHidden:YES];
        [_public_ulbl setHidden:YES];
        [_group_tv setHidden:YES];
        [_group_lbl setHidden:YES];
        [_groupDropDownImg setHidden:YES];
        [_groupView_underline setHidden:YES];
        
        AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ITI_ATT_TYPE"]];
    }
    else
    {
        AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATT_TYPE"]];
        
        [_group_tv setHidden:NO];
        [_group_lbl setHidden:NO];
        [_groupDropDownImg setHidden:NO];
        [_groupView_underline setHidden:NO];
       // AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"EVENT"]];

    }
    [AttachmentTypeTextfield addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    keychain = [SLKeyChainStore keyChainStoreWithService:appedelegate.keychainName];


    [self langsetuptranslations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)langsetuptranslations
{
    
    appedelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancel_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _attachmenttype_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_324"]];
    [self markasrequired:_attachmenttype_ulbl];
    AttachmentTypeTextfield.placeholder=[helper gettranslation:@"LBL_757"];
    
    _public_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_604"]];
    _filename_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_402"]];
    _filetype_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_404"]];
    _filesize_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_403"]];
    _description_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_094"]];
    
    _attachlabel.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_296"]];
    _group_lbl.text=[helper gettranslation:@"LBL_923"];
}

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *Touch = [touches anyObject];
    [DescriptionTextField resignFirstResponder];
    if(AttachmentTypeTableview.hidden == NO)
    {
        AttachmentTypeTableview.hidden = YES;
        [AttachmentTypeTextfield resignFirstResponder];
    }
}
-(void)removeview
{
    if([_senderView isEqualToString:@"Speaker"] || [_senderView isEqualToString:@"Attendee"]||[_senderView isEqualToString:@"Itenary"])
    {
        [self performSegueWithIdentifier:@"BackToAttachmentListView" sender:self];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
    else
    {
        appedelegate.swipevar = @"AttachmentView";
        [self performSegueWithIdentifier:@"AddAttachmentToEventDetail" sender:self];
        [UIView animateWithDuration:0.3f animations:^{
            self.view.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    }
}
- (IBAction)CancelButtonAction:(id)sender
{
    [self removeview];

}
#pragma mark Tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [AttachmentTypeArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"socustomcell";
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *AttachmentType = (UILabel *) [contentView viewWithTag:1];
    
    if([textFieldFlag isEqualToString:@"Group"]){
        AttachmentType.text = [AttachmentTypeArr[indexPath.row] valueForKey:@"groupname"];
    }
    else
        AttachmentType.text = [AttachmentTypeArr[indexPath.row] valueForKey:@"value"];
    
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == AttachmentTypeTableview)
    {
        if([textFieldFlag isEqualToString:@"Group"])
        {
            _group_tv.text = [AttachmentTypeArr[indexPath.row] valueForKey:@"groupname"];
            selectedGroupID =[AttachmentTypeArr[indexPath.row] valueForKey:@"groupid"];
            [_group_tv resignFirstResponder];
        }
        else
        {
            AttachmentTypeTextfield.text = [AttachmentTypeArr[indexPath.row] valueForKey:@"value"];
        }
        AttachmentTypeTableview.hidden = YES;
        [AttachmentTypeTextfield resignFirstResponder];
    }
   
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == AttachmentTypeTextfield)
    {
       if([_senderView isEqualToString:@"Speaker"])
       {
           AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPK_ATT_TYPE"]];
       }
       else if([_senderView isEqualToString:@"Attendee"])
       {
           AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATTENDEE_ATT_TYPE"]];
       }
        else if ([_senderView isEqualToString:@"Itenary"])
        {
             AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ITI_ATT_TYPE"]];
        }
        else
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATT_TYPE"]];
        }
        textFieldFlag = @"Attachment";
        [AttachmentTypeTableview reloadData];
        
        AttachmentTypeTableview.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
     
        AttachmentTypeTableview.layer.borderWidth = 1;
        AttachmentTypeTableview.hidden = NO;
   
        [helper MoveViewUp:YES Height:110 ViewToBeMoved:self.view];
       
    }
    else if (textField == _group_tv)
    {
        AttachmentTypeArr = [[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appedelegate.eventid]];
        [_group_tv resignFirstResponder];
        textFieldFlag = @"Group";
        CGRect frame = AttachmentTypeTableview.frame;
        frame.origin.x = 8;
        frame.origin.y = 75;
        AttachmentTypeTableview.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
        
        AttachmentTypeTableview.layer.borderWidth = 1;
        AttachmentTypeTableview.hidden = NO;
        
        [helper MoveViewUp:YES Height:110 ViewToBeMoved:self.view];
        [AttachmentTypeTableview reloadData];
    }
    else if (textField == FileNameTextField)
    {
        stringlength = 100;
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
    }
    else if (textField == DescriptionTextField)
    {
        stringlength = 255;
         [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
    }
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == AttachmentTypeTextfield)
    {
        AttachmentTypeTableview.hidden = YES;
        [helper MoveViewUp:NO Height:110 ViewToBeMoved:self.view];
        
        
        if([_senderView isEqualToString:@"Speaker"])
        {
            if (![helper isdropdowntextCorrect:AttachmentTypeTextfield.text lovtype:@"SPK_ATT_TYPE"])
            {
                AttachmentTypeTextfield.text = @"";
            }
        }
        else if ([_senderView isEqualToString:@"Attendee"])
        {
            if (![helper isdropdowntextCorrect:AttachmentTypeTextfield.text lovtype:@"ATTENDEE_ATT_TYPE"])
            {
                AttachmentTypeTextfield.text = @"";
            }
        }
        else if ([_senderView isEqualToString:@"Itenary"])
        {
         
            if (![helper isdropdowntextCorrect:AttachmentTypeTextfield.text lovtype:@"ITI_ATT_TYPE"])
            {
                AttachmentTypeTextfield.text = @"";
            }
        }
        else
        {
            if(![helper isdropdowntextCorrect:AttachmentTypeTextfield.text lovtype:@"ATT_TYPE"])
            {
                AttachmentTypeTextfield.text = @"";
            }
        }
    }
    else if (textField == DescriptionTextField || textField == FileNameTextField)
    {
        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.view];
    }
    else if (textField == _group_tv)
    {
        [helper MoveViewUp:NO Height:110 ViewToBeMoved:self.view];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark search method
-(void)textFieldDidChange :(UITextField *)textField
{
    NSString *searchText;
    searchText = textField.text;
    
    if(searchText.length == 0)
    {
        if([_senderView isEqualToString:@"Speaker"])
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPK_ATT_TYPE"]];
          //  AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"SPEAKER"]];
        }
        else if([_senderView isEqualToString:@"Attendee"])
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATTENDEE_ATT_TYPE"]];
            //AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"EVT_ATT"]];
            
        }
        else if ([_senderView isEqualToString:@"Itenary"])
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ITI_ATT_TYPE"]];
        }
        else
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATT_TYPE"]];
            // AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"EVENT"]];
        }
    }
    else
    {
        if([_senderView isEqualToString:@"Speaker"])
        {
             AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPK_ATT_TYPE"]];
           // AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"SPEAKER"]];
        }
        else if([_senderView isEqualToString:@"Attendee"])
        {
             AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATTENDEE_ATT_TYPE"]];
           // AttachmentTypeArr=[AttachmentTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"EVT_ATT"]];
            
        }
        else if ([_senderView isEqualToString:@"Itenary"])
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ITI_ATT_TYPE"]];
        }
        else
        {
            AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATT_TYPE"]];
            // AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",@"EVENT"]];
        }
       // AttachmentTypeArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ATT_TYPE"]];
        NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.value contains[c] %@)",searchText];
        filteredArray = [NSMutableArray arrayWithArray:[AttachmentTypeArr filteredArrayUsingPredicate:predicate]];
        AttachmentTypeArr = [filteredArray copy];
    }
    [AttachmentTypeTableview reloadData];
   
    
}
#pragma mark Camera method
- (IBAction)CameraButtonAction:(id)sender
{
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus == NotReachable)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        }
        else
        {
            if (([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusNotDetermined) || ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusAuthorized))
            {
                ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
                [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                    if (*stop) {
                                    UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_578"] message:[helper gettranslation:@"LBL_504"] preferredStyle:UIAlertControllerStyleAlert];
                                    [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_632"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                               {
                                                                   if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                                                                   {
                                                                       UIImagePickerController *impicker = [[UIImagePickerController alloc]init];
                                                                       impicker.delegate = self;
                                                                       impicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                                                       impicker.allowsEditing = NO;
                                                                       [self presentViewController:impicker animated:YES completion:NULL];
                        
                                                                   }
                        
                        
                                                               }]];
                        
                                    [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_334"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                               {
                                                                   if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
                                                                   {
                                                                       UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
                                                                       imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                       imagepicker.allowsEditing = YES;
                                                                       imagepicker.delegate = self;
                                                                       [self presentViewController:imagepicker animated:YES completion:NULL];
                                                                   }
                        
                                                               }]];
                                   /* [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_631"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                               {
                                                                   UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.content"] inMode:UIDocumentPickerModeImport];
                                                                   documentPicker.delegate = self;
                                                                   documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
                                                                   [self presentViewController:documentPicker animated:YES completion:nil];
                                                               }]];*/
                        
                                    [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil]];
                                    [self presentViewController:takephotoalert animated:YES completion:nil];
                    }
                    *stop = TRUE;
                } failureBlock:^(NSError *error) {
                    // INSERT CODE TO PERFORM WHEN USER TAPS DONT ALLOW, eg. :
//                    self.imagePickerController dismissViewControllerAnimated:YES completion:nil];
                }];
            }
            else
            {
                [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_935"] action:[helper gettranslation:@"LBL_462"]];
            }
        }
}
    
/*- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        NSError *error;
        filedata = [NSData dataWithContentsOfURL:url options:0 error:&error];
        NSString  *resumefilename = [url lastPathComponent];
        resumefilename =  [resumefilename stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        NSURL *resume_url = [NSURL URLWithString: resumefilename];
        FileTypeTextfield.text = [resume_url pathExtension];
        NSString *cvname = resumefilename;
        if ([resumefilename length]>0)
        {
            cvname = [NSString stringWithFormat:@"%@%@",[[cvname substringToIndex:1]uppercaseString],[[cvname substringFromIndex:1]lowercaseString]];
            FileNameTextField.text =cvname ;
        }
        FileSizeTextField.text = [NSByteCountFormatter stringFromByteCount:filedata.length countStyle:NSByteCountFormatterCountStyleFile];
    }
}*/

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)resizeimage
{
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 500000;
    
    filedata = UIImageJPEGRepresentation(capturedlicensimg, compression);
    
    while ([filedata length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        filedata = UIImageJPEGRepresentation(capturedlicensimg, compression);
    }
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    if (picker.sourceType ==  UIImagePickerControllerSourceTypeCamera)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"yyyyMMdd"];
        NSString *date = [dateformatter stringFromDate:[NSDate date]];
        [dateformatter setDateFormat:@"HHmmss"];
        NSString *time = [dateformatter stringFromDate:[NSDate date]];
        FileNameTextField.text = [NSString stringWithFormat:@"Image_%@_%@.JPG",date,time];
        FileTypeTextfield.text = @"JPG";

    }
    [self resizeimage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
        {
            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
            attachedimagename = [imageRep filename];
           // NSLog(@"Attachment image name is %@",attachedimagename);
            PathExtension = [attachedimagename pathExtension];
          //  NSLog(@"Attachment path extension %@",PathExtension);
            FileNameTextField.text = attachedimagename;
            FileTypeTextfield.text = PathExtension;
        }

        
      };
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    //filedata = UIImageJPEGRepresentation(capturedlicensimg, 1); //1 it represents the quality of the image.
    
    //NSLog(@"Size of Image(bytes):%ld",[imgData length]);
    NSLog(@"image size %@",[NSByteCountFormatter stringFromByteCount:filedata.length countStyle:NSByteCountFormatterCountStyleFile]);
    

        FileSizeTextField.text = [NSByteCountFormatter stringFromByteCount:filedata.length countStyle:NSByteCountFormatterCountStyleFile];
    
}
//#pragma mark Icloud method
//- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
//{
//    if (controller.documentPickerMode == UIDocumentPickerModeImport)
//    {
//        
//      //  NSString *filebase64string;
//        NSError *error;
//        filedata = [NSData dataWithContentsOfURL:url options:0 error:&error];
//       // filebase64string= [filedata base64EncodedStringWithOptions:kNilOptions];
//        attachedimagename = [url lastPathComponent];
//        PathExtension = [attachedimagename pathExtension];
//        FileNameTextField.text = attachedimagename;
//        FileTypeTextfield.text = PathExtension;
//        FileSizeTextField.text = [NSByteCountFormatter stringFromByteCount:filedata.length countStyle:NSByteCountFormatterCountStyleFile];
//
//        
//    }
//}
#pragma mark upload attachmment method
-(void) upload_attachmentdata:(NSString *) filename base64:(NSString *)base64
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[NSString stringWithFormat:@"%@.%@",[NSString stringWithString:RandomnId],@"PNG"] forKey:@"FileName"];
    [dict setValue:base64 forKey:@"Base64"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageContainer" entityname:@"S_Config_Table"] forKey:@"Container"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageAccount" entityname:@"S_Config_Table"] forKey:@"Account"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageKey" entityname:@"S_Config_Table"] forKey:@"Key"];

    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSData *postData = [jsonStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
  
    if([[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"] length]>0)
    {
        [request setURL:[NSURL URLWithString:[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]]];
        [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
        [request setValue:[keychain stringForKey:@"SLToken"] forHTTPHeaderField:@"X-ZUMO-AUTH"];
        [request setHTTPMethod:@"PUT"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
        if(conn)
        {
            responsedata = [NSMutableData data];
        }
        else
        {
            NSLog(@"Connection could not be made in upload attachment request");
        }
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responsedata setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responsedata appendData:data];
    
}
// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [helper removeWaitCursor];
    imageUploaded=NO;
    NSString *err = [NSString stringWithFormat:@"%@%@",[error description],[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:err preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
   dispatch_semaphore_signal(sem_att);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    NSString *jsonArray = [NSJSONSerialization JSONObjectWithData:responsedata options:NSJSONReadingAllowFragments error:&error];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[jsonArray dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    StorageURL = [json valueForKey:@"FileURI"];
    imageUploaded=YES;
    
       dispatch_semaphore_signal(sem_att);
}
-(void)InsertLocationUrl
{
    //location url
    if ([AttachmentTypeTextfield.text isEqualToString:@"Gallery"])
    {
        NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Galleryimages"];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",FileNameTextField.text]]; //Add the file name
        if (filedata)
        {
            [filedata writeToFile:filePath atomically:YES];
            [helper update_imageurl:filePath entityname:@"Event_Gallery" entityid:RandomnId];
        }
    }

}
#pragma mark Save method

- (IBAction)SaveButtonAction:(id)sender
{
    NSArray  *Attachmenttypelist;
    [FileNameTextField resignFirstResponder];
    [FileSizeTextField resignFirstResponder];
    [FileTypeTextfield resignFirstResponder];
    [DescriptionTextField resignFirstResponder];
    
    if ([[helper getLic:@"ATT_TYPE" value:AttachmentTypeTextfield.text] isEqualToString:@"Map"])
    {
        Attachmenttypelist  = [[helper query_alldata:@"Event_Attachments"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@ AND attachmenttype like %@",appedelegate.eventid,[helper getvaluefromlic:@"ATT_TYPE" lic:@"Map"]]];
    }
    else if ([[helper getLic:@"ATT_TYPE" value:AttachmentTypeTextfield.text] isEqualToString:@"FAQ"])
    {
        Attachmenttypelist = [[helper query_alldata:@"Event_Attachments"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@ AND attachmenttype like %@",appedelegate.eventid,[helper getvaluefromlic:@"ATT_TYPE" lic:@"FAQ"]]];
    }
    else if ([[helper getLic:@"ATT_TYPE" value:AttachmentTypeTextfield.text] isEqualToString:@"EVT_BG"])
    {
       Attachmenttypelist = [[helper query_alldata:@"Event_Attachments"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@ AND attachmenttype like %@",appedelegate.eventid,[helper getvaluefromlic:@"ATT_TYPE" lic:@"EVT_BG"]]];
    }
    
       if([AttachmentTypeTextfield.text length]>0 && (capturedlicensimg != nil) && [FileSizeTextField.text length]>0)
        {
           if ([Attachmenttypelist count]>0)
           {
        
               [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@ %@ %@",[helper gettranslation:@"LBL_800"],AttachmentTypeTextfield.text,[helper gettranslation:@"LBL_801"]] action:[helper gettranslation:@"LBL_462"]];
           }
           else
           {
               transaction_type = @"Add Attachment";
               NSError *error;
               NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
               [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
               NSString *UploadDate = [dateFormatter stringFromDate:[NSDate date]];
               RandomnId = [helper generate_rowId];
               NSData *jsonData;
               [helper showWaitCursor:[helper gettranslation:@"LBL_536"]];
               sem_att = dispatch_semaphore_create(0);
               
               [self upload_attachmentdata:attachedimagename base64:[filedata base64EncodedStringWithOptions:kNilOptions]];
               
               while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
               [helper removeWaitCursor];
               if(imageUploaded)
               {
                   [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
                   NSString *jsonString;
                   NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                   
                   [dict setValue:RandomnId forKey:@"ID"];
               
                   if([_senderView isEqualToString:@"Speaker"])
                   {
                       [dict setValue:_speakerID forKey:@"EntityID"];
                   }
                   else if ([_senderView isEqualToString:@"Attendee"])
                   {
                       [dict setValue:_attendeeID forKey:@"EntityID"];
                   }
                   else if ([_senderView isEqualToString:@"Itenary"])
                   { 
                       [self SendItinearyData];
                       [dict setValue:appedelegate.itnearyid forKey:@"EntityID"];
                   }
                   else
                   {
                       [dict setValue:appedelegate.eventid forKey:@"EntityID"];
                       [dict setValue:selectedGroupID forKey:@"GroupID"];
                   }
                   
               
                   [dict setValue:FileNameTextField.text forKey:@"FileName"];
                   [dict setValue: FileTypeTextfield.text forKey:@"FileType"];
                   [dict setValue:FileSizeTextField.text forKey:@"FileSize"];
                   [dict setValue:UploadDate forKey:@"UploadDate"];
                   [dict setValue:StorageURL forKey:@"StorageID"];
                   [dict setValue:DescriptionTextField.text forKey:@"Description"];
                   [dict setValue:AttachmentTypeTextfield.text forKey:@"Type"];
               
                   if([_senderView isEqualToString:@"Speaker"])
                   {
                       [dict setValue:[helper getLic:@"SPK_ATT_TYPE" value:AttachmentTypeTextfield.text] forKey:@"TypeLIC"];

                   }
                   else if ([_senderView isEqualToString:@"Attendee"])
                   {
                       [dict setValue:[helper getLic:@"ATTENDEE_ATT_TYPE" value:AttachmentTypeTextfield.text] forKey:@"TypeLIC"];
                   }
                   else if ([_senderView isEqualToString:@"Itenary"])
                   {
                       [dict setValue:[helper getLic:@"ITI_ATT_TYPE" value:AttachmentTypeTextfield.text] forKey:@"TypeLIC"];
                   }
                   else
                   {
                       [dict setValue:[helper getLic:@"ATT_TYPE" value:AttachmentTypeTextfield.text] forKey:@"TypeLIC"];
                   }
               
               
                   [dict setValue:[helper stringtobool:Public] forKey:@"Public"];
                   [dict setValue:@"0" forKey:@"CommentCount"];
                   [dict setValue:@"0" forKey:@"LikeCount"];
                   NSMutableArray *Arr = [[NSMutableArray alloc]init];
                   [Arr addObject:dict];
                   jsonData = [NSJSONSerialization dataWithJSONObject:Arr options:0 error:&error];
                   if (jsonData)
                   {
                       jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                   }
                   NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
                   NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
                   NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
                   if(base64String)
                   {
                       NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetAttachment" pageno:@"" pagecount:@"" lastpage:@""];
                       appedelegate.senttransaction = @"Y";
                      
                       
                       NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
                       
                       if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
                       {
                           [helper removeWaitCursor];
                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
                           
                           [alert show];
                       }
                   
                       if([_senderView isEqualToString:@"Speaker"])
                       {
                           [helper insertspeakerattachmentdata:Arr];
                       }
                       else if ([_senderView isEqualToString:@"Attendee"])
                       {
                           [helper addEventAttendeeAttachment:Arr];
                       
                       }
                       else if ([_senderView isEqualToString:@"Itenary"])
                       {
                           [helper InsertItenaryAttachment:Arr eventid:appedelegate.eventid];
                       }
                       else
                       {
                          
                           NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                           NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_Attachments"    inManagedObjectContext:context];
                           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((SELF.attachmenttypelic  contains[c] %@) AND (SELF.eventid  contains[c] %@))",@"Map",appedelegate.eventid];
                           NSError *error;
                           [fetchRequest setEntity:entity];
                           [fetchRequest setPredicate:predicate];
                           NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                           if([fetchedObjects count]>0)
                           {
                               for (NSManagedObject *object in fetchedObjects)
                               {
                                   [object setValue:@"Attachment" forKey:@"attachmenttypelic"];
                                   [object setValue:[helper getvaluefromlic:@"ATT_TYPE" lic:@"Attachment"] forKey:@"attachmenttypelic"];
                               }
                               [context save:&error];
                           }
                           [helper insertEventAttachment:Arr EventId:appedelegate.eventid];
                       }
                            [helper removeWaitCursor];
                            [self removeview];
                   }
                   }
               }
       }
    else if([AttachmentTypeTextfield.text length]==0)
    {
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"]  message:@"Please enter the required field" delegate:self  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        //[alert show];
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];

    }
    else if (capturedlicensimg == nil)
    {
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please add file" delegate:self  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        //[alert show];
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_499"] action:[helper gettranslation:@"LBL_462"]];
    }
   
}
#pragma mark switch method
- (IBAction)PublicSwitchButtonAction:(id)sender
{
    if (publicswitchtag == 0)
    {
        Public = @"Y";
        publicswitchtag ++;
       // PublicSwitchButtonOutlet.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
        PublicSwitchButtonOutlet.onTintColor = [helper DarkBlueColour];
        
    }
    else if (publicswitchtag == 1)
    {
        publicswitchtag --;
        Public = @"N";
        
    }
    NSLog(@"Public button value is %@",Public);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == FileNameTextField || textField == DescriptionTextField)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}

-(void)SendItinearyData
{
 
    NSArray *SelectedItinearyData = [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"itineraryID like %@",appedelegate.itnearyid]];
    
    
    NSMutableDictionary *ReqDict = [[NSMutableDictionary alloc]init];
    if([SelectedItinearyData count]>0)
    {
        if([[SelectedItinearyData[0] valueForKey:@"dbrec"]isEqualToString:@"N"])
        {
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"eventid"] forKey:@"EventID"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"id"] forKey:@"ID"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"itineraryID"] forKey:@"ItineraryID"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"affiliateID"] forKey:@"AffiliateID"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"salutationLIC"]forKey:@"SalutationLIC"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"salutation"] forKey:@"Salutation"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"fullname"] forKey:@"Name"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"partyID"] forKey:@"PartyID"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"partyTypeLIC"] forKey:@"PartyTypeLIC"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"entityID"] forKey:@"EntityID"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"entityTypeLIC"] forKey:@"EntityTypeLIC"];
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"authorizationTypeLIC"] forKey:@"AuthorizationTypeLIC"];
    
    
    
    
            NSString *jsonString;
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ReqDict options:0 error:&error];
            if (! jsonData)
            {
                NSLog(@"Got an error: %@", error);
            }
            else
            {
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
            NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
            NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
            NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
            NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetItinerary" pageno:@"" pagecount:@"" lastpage:@""];
    
            NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
            if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
            {
                [helper removeWaitCursor];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
                [alert show];
            }
        }
    }
        
}


@end
