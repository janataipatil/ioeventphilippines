//
//  AddAttendee.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/17/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SOCustomCell.h"
#import "SLKeyChainStore.h"


@class HelperClass;
@interface AddAttendee : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIDocumentPickerDelegate>
{
    
    IBOutlet UITableView *LOVTableview;
    //IBOutlet UITableView *salutation_tableview;
}
@property (strong, nonatomic) IBOutlet UILabel *AddEditAttendeeLbl;
@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic)NSString *attendeeid;

@property (strong, nonatomic) IBOutlet UITextField *salutation_tv;
@property (strong, nonatomic) IBOutlet UITextField *firstname_tv;
@property (strong, nonatomic) IBOutlet UITextField *lastname_tv;

@property (strong, nonatomic) IBOutlet UITextField *status_tv;
@property (strong, nonatomic) IBOutlet UITextField *email_tv;
@property (strong, nonatomic) IBOutlet UITextField *contactno_tv;

@property (strong, nonatomic) IBOutlet UITextField *speciality_tv;
@property (strong, nonatomic) IBOutlet UITextField *integrationsrc_tv;
@property (strong, nonatomic) IBOutlet UITextField *targetclass_tv;
@property (strong, nonatomic) IBOutlet UITextField *owner_tv;
@property (strong, nonatomic) IBOutlet UITextField *InstitutionTextField;
@property (strong, nonatomic) IBOutlet UIView *UserViewOutlet;
@property (strong, nonatomic) IBOutlet UITextField *SubTargetClassTextField;

@property (strong, nonatomic) IBOutlet UIWebView *LoadImageWebview;
- (IBAction)addimage_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (strong, nonatomic) IBOutlet UIView *parentview;
@property (strong, nonatomic) IBOutlet UISearchBar *UserSearchBarOutlet;
@property (strong, nonatomic) IBOutlet UITableView *UserTableView;

@property (strong, nonatomic) IBOutlet UIButton *SaveButttonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *CancelButtonOutlet;

- (IBAction)save_btn:(id)sender;
- (IBAction)cancel_btn:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *titlelabel_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *salutation_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *first_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *lastname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *emailaddress_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *contactno_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *integrationsrc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *institution_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *speciality_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *targetclass_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *subtargetclass_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *positions_ulbl;

@property (strong, nonatomic) NSArray *attendeelist;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *rowid;
@property (strong, nonatomic) IBOutlet UILabel *rowid_tv;


@end
