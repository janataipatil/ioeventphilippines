//
//  AddAttendee.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/17/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddAttendee.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface AddAttendee ()
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSArray *Eventlovlist;
    SLKeyChainStore *keychain;
    NSString *generatedrowid;
    NSArray *attendeelist;
    NSData *imageData;
    NSString *attachedimagename;
    BOOL imageselcted;
    NSMutableData *responsedata;
    dispatch_semaphore_t sem_att;
    NSString *uploadflag;
    NSString *imagestorageurl;
    NSArray *LOVArr;
    NSString *TextfieldFlag;
    IBOutlet UIButton *addimage_btn;
    NSString *transaction_type;
    NSArray *OwnerListArr;
    NSUInteger count;
    NSMutableArray  *selectedAttendeeArr;
    NSArray *AttendeePositionArr;
   // NSArray *AttendeeList;
    NSUInteger stringlength;
    NSString *PickImage;

}
@end

@implementation AddAttendee
@synthesize attendeeid,helper,profileimage,salutation_tv,status_tv,speciality_tv,targetclass_tv,integrationsrc_tv,owner_tv,firstname_tv,lastname_tv,email_tv,contactno_tv,LoadImageWebview,AddEditAttendeeLbl,UserTableView,UserViewOutlet,UserSearchBarOutlet,SaveButttonOutlet,CancelButtonOutlet,SubTargetClassTextField,InstitutionTextField,attendeelist;

- (void)viewDidLoad
{
    [super viewDidLoad];
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
  
    
    //AttendeeList=[[helper query_alldata:@"Attendee"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@"] ];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
     selectedAttendeeArr = [[NSMutableArray alloc]init];
    if(attendeeid.length > 0)
    {
       // addimage_btn.hidden = YES;
        [self setupattendeeview];
        generatedrowid = attendeeid;
        transaction_type = @"Edit Attendee";
        AddEditAttendeeLbl.text = @"Edit Attendee";
        _titlelabel_ulbl.text=[helper gettranslation:@"LBL_108"];
        [integrationsrc_tv setUserInteractionEnabled:NO];
    }
    else
    {
        generatedrowid = [helper generate_rowId];
        transaction_type = @"Add Attendee";
        AddEditAttendeeLbl.text = @"Add Attendee";
        NSString *posid=[helper getPositionID];
        [selectedAttendeeArr addObject:posid];
        _titlelabel_ulbl.text=[helper gettranslation:@"LBL_016"];
        _rowid_tv.text=generatedrowid;
        [integrationsrc_tv setUserInteractionEnabled:NO];
    
        
    }

     LOVTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
     LOVTableview.layer.borderWidth = 1;
    
    [salutation_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [status_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [targetclass_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [speciality_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [integrationsrc_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [owner_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //prepopulate the status value in form
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
//    dispatch_async(queue, ^{
         NSArray *StatusArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Active"]];
    
        NSArray *source=[[helper query_alldata:@"Event_LOV"]
                         filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"EMS"]];
//        dispatch_sync(dispatch_get_main_queue(), ^{

            if ([StatusArr count]>0)
            {
                status_tv.text = [StatusArr[0] valueForKey:@"value"];
            }
            if([source count]>0)
            {
                integrationsrc_tv.text=[source[0] valueForKey:@"value"];
                
            }
//        });
//    });
//    NSArray *StatusArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Active"]];
   
    
    
    
    
   
    [UserSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [UserSearchBarOutlet valueForKey:@"_searchField"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
   searchField.backgroundColor = [UIColor colorWithRed:220/255.0 green:228/255.0 blue:240/255.0 alpha:1.0f];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
   [UserTableView setEditing:YES];
    
//    AttendeePositionArr = [helper query_alldata:@"Position_List"];
//
//    NSArray *FilteredAttendeeArr = [[helper query_alldata:@"AttendeePositions"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeID like %@",attendeeid]];
//    for (int i =0; i<[FilteredAttendeeArr count]; i++)
//    {
//        [selectedAttendeeArr addObject:[FilteredAttendeeArr[i] valueForKey:@"positionID"]];
//    }
//
//    NSMutableArray *idArr = [[NSMutableArray alloc]init];
//    for (int i =0; i<[AttendeePositionArr count]; i++)
//    {
//        if([AttendeePositionArr[i] valueForKey:@"positionID"])
//        [idArr addObject:[AttendeePositionArr[i] valueForKey:@"positionID"]];
//
//    }
//
//    NSMutableArray *selectedUserArr = [[NSMutableArray alloc]init];
//
//    for (int i =0; i<[idArr count]; i++)
//    {
//        NSString *PositionId = [idArr objectAtIndex:i];
//        NSString *YesNoStr;
//        if ([selectedAttendeeArr containsObject:PositionId])
//        {
//            YesNoStr = @"Y";
//        }
//        else
//        {
//            YesNoStr = @"N";
//        }
//        NSArray *Arr = [AttendeePositionArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"positionID like %@",PositionId]];
//        if ([Arr count]>0)
//        {
//            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
//            [Dict setValue:[Arr[0] valueForKey:@"positionName"] forKey:@"value"];
//            [Dict setValue:[Arr[0] valueForKey:@"positionID"] forKey:@"lic"];
//            [Dict setValue:YesNoStr forKey:@"selecteduser"];
//            [selectedUserArr addObject:Dict];
//        }
//    }
//    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"selecteduser" ascending:NO];
//    AttendeePositionArr=[selectedUserArr sortedArrayUsingDescriptors:@[sort]];
//    NSMutableArray *PositionidArr = [[NSMutableArray alloc]init];
//
//    for (int i =0; i<[AttendeePositionArr count]; i++)
//    {
//        [PositionidArr addObject:[AttendeePositionArr[i] valueForKey:@"lic"]];
//    }
//    for (int i =0; i<[selectedAttendeeArr count]; i++)
//    {
//        NSString *PositionId = [selectedAttendeeArr objectAtIndex:i];
//        if ([PositionidArr containsObject:PositionId])
//        {
//            NSUInteger index = [PositionidArr indexOfObject:PositionId];
//            NSInteger Attendeecount = [PositionidArr count];
//            if ((index > 0) || ((index == 0) && (index< Attendeecount)) )
//            {
//                [UserTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
//            }
//
//
//        }
//    }//for loop end
    
    
    if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] ||(![appdelegate.UserRolesArray containsObject:@"ATT_R"] && ![appdelegate.UserRolesArray containsObject:@"ATT_W"]))
    {
        addimage_btn.userInteractionEnabled = NO;
        salutation_tv.userInteractionEnabled = NO;
        firstname_tv.userInteractionEnabled = NO;
        lastname_tv.userInteractionEnabled = NO;
        status_tv.userInteractionEnabled = NO;
        email_tv.userInteractionEnabled = NO;
        contactno_tv.userInteractionEnabled = NO;
        speciality_tv.userInteractionEnabled = NO;
        integrationsrc_tv.userInteractionEnabled = NO;
        targetclass_tv.userInteractionEnabled = NO;
        SaveButttonOutlet.userInteractionEnabled = NO;
        SubTargetClassTextField.userInteractionEnabled = NO;
        UserTableView.userInteractionEnabled = NO;
    }
    if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] && [appdelegate.UserRolesArray containsObject:@"ATT_W"])
    {
        addimage_btn.userInteractionEnabled = YES;
        salutation_tv.userInteractionEnabled = YES;
        firstname_tv.userInteractionEnabled = YES;
        lastname_tv.userInteractionEnabled = YES;
        status_tv.userInteractionEnabled = YES;
        email_tv.userInteractionEnabled = YES;
        contactno_tv.userInteractionEnabled = YES;
        speciality_tv.userInteractionEnabled = YES;
        integrationsrc_tv.userInteractionEnabled = YES;
        targetclass_tv.userInteractionEnabled = YES;
        SaveButttonOutlet.userInteractionEnabled = YES;
        SubTargetClassTextField.userInteractionEnabled = YES;
        UserTableView.userInteractionEnabled = YES;
    }

    [self langsetuptranslations];
}

-(void)viewWillAppear:(BOOL)animated
{

    [self reloadPositionList];
}

-(void)viewDidAppear:(BOOL)animated{
    [helper removeWaitCursor];
}
-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
   // [_titlelabel_ulbl setText:[helper gettranslation:@"LBL_016"]];
    
    //_salutation_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_218"]];
     _salutation_ulbl.text = [NSString stringWithFormat:@"%@ ",[helper gettranslation:@"LBL_218"]];
    salutation_tv.placeholder=[helper gettranslation:@"LBL_757"];
    [self markasrequired:_salutation_ulbl];
    
    _first_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_141"]];
    [self markasrequired:_first_ulbl];
    
    _rowid.text=[helper gettranslation:@"LBL_825"];
    
    _lastname_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_153"]];
    [self markasrequired:_lastname_ulbl];
    
   // _speciality_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_238"]];
     _speciality_ulbl.text = [NSString stringWithFormat:@"%@ ",[helper gettranslation:@"LBL_238"]];
    speciality_tv.placeholder=[helper gettranslation:@"LBL_757"];
    [self markasrequired:_speciality_ulbl];
    
    _targetclass_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_255"]];
    targetclass_tv.placeholder=[helper gettranslation:@"LBL_757"];
  //  [self markasrequired:_targetclass_ulbl];
    
    
    _subtargetclass_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_254"]];
    SubTargetClassTextField.placeholder=[helper gettranslation:@"LBL_757"];
  //  [self markasrequired:_subtargetclass_ulbl];
    
    _status_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_242"]];
    [self markasrequired:_status_ulbl];
    
    _contactno_ulbl.text = [helper gettranslation:@"LBL_070"];
    
    _emailaddress_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_119"]];
   // [self markasrequired:_emailaddress_ulbl];
    
    _integrationsrc_ulbl.text = [helper gettranslation:@"LBL_148"];
    integrationsrc_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
    _institution_ulbl.text = [helper gettranslation:@"LBL_147"];
    InstitutionTextField.placeholder=[helper gettranslation:@"LBL_757"];
    
    _positions_ulbl.text = [helper gettranslation:@"LBL_915"];
    
}

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupattendeeview
{
//    attendeelist = [helper query_alldata:@"Attendee"];
//    attendeelist = [attendeelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",attendeeid]];
    if ([attendeelist count]>0)
    {
        salutation_tv.text = [attendeelist[0] valueForKey:@"salutation"];
        firstname_tv.text = [attendeelist[0] valueForKey:@"firstname"];
        lastname_tv.text = [attendeelist[0] valueForKey:@"lastname"];
        _rowid_tv.text = [attendeelist[0] valueForKey:@"id"];
        status_tv.text = [attendeelist[0] valueForKey:@"status"];
        email_tv.text = [attendeelist[0] valueForKey:@"emailaddress"];
        contactno_tv.text = [attendeelist[0] valueForKey:@"contactnumber"];
    
        speciality_tv.text = [attendeelist[0] valueForKey:@"speciality"];
        integrationsrc_tv.text = [attendeelist[0] valueForKey:@"integrationsource"];
        targetclass_tv.text = [attendeelist[0] valueForKey:@"targetclass"];
        SubTargetClassTextField.text = [attendeelist[0] valueForKey:@"subTargetClass"];
        imagestorageurl = [attendeelist[0] valueForKey:@"imageid"];
        InstitutionTextField.text = [attendeelist[0] valueForKey:@"institution"];
        if (imagestorageurl)
        {
           
            dispatch_queue_t queue = dispatch_queue_create(0, 0);
            dispatch_async(queue,
                           ^{
                               UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:[attendeelist[0] valueForKey:@"imageid"]]]];
                               if(image)
                               {
                                   dispatch_async(dispatch_get_main_queue(),
                                                  ^{
                                                      self.profileimage.image = image;
                                                  });
                               }
                           });
                           
                               
                   }
        else
        {
            self.profileimage.image = [UIImage imageNamed:@"User.png"];
        }
      }
}


-(void)removepopup
{
    
    [self performSegueWithIdentifier:@"cancel_btn" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
    
}
- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
    
}



- (IBAction)save_btn:(id)sender
{
    if ((firstname_tv.text.length > 0)&&(lastname_tv.text.length > 0)&&(status_tv.text.length > 0)) 
    {
        attendeelist = [helper query_alldata:@"Attendee"];
        attendeelist = [attendeelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",attendeeid]];
        
        
        
        if (attendeelist.count > 0)
        {
            //edit records
            if ([salutation_tv.text isEqualToString:[attendeelist[0] valueForKey:@"salutation"]?: @""] && [firstname_tv.text isEqualToString:[attendeelist[0] valueForKey:@"firstname"]?: @""] && [lastname_tv.text isEqualToString:[attendeelist[0] valueForKey:@"lastname"]?: @""] && [status_tv.text isEqualToString:[attendeelist[0] valueForKey:@"status"]?: @""] && [email_tv.text isEqualToString:[attendeelist[0] valueForKey:@"emailaddress"]?: @""] && [contactno_tv.text isEqualToString:[attendeelist[0] valueForKey:@"contactnumber"]?: @""]  && [speciality_tv.text isEqualToString:[attendeelist[0] valueForKey:@"speciality"]?: @""]  && [integrationsrc_tv.text isEqualToString:[attendeelist[0] valueForKey:@"integrationsource"]?: @""]   && [targetclass_tv.text isEqualToString:[attendeelist[0] valueForKey:@"targetclass"]?: @""] && [SubTargetClassTextField.text isEqualToString:[attendeelist[0] valueForKey:@"subTargetClass"]?: @""] && [InstitutionTextField.text isEqualToString:[attendeelist[0] valueForKey:@"institution"]?: @""] && [imagestorageurl isEqualToString:[attendeelist[0] valueForKey:@"imageid"]?:@""] && [PickImage isEqualToString:@"YES"])
            {
                [self removepopup];
            }
            else
            {
                [self savedata:attendeelist];
            }
        }
        else
        {
            //crreate records
            [self savedata:attendeelist];
        }

    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
    }
}



-(void)savedata:(NSArray *) attendeedata
{
    if([uploadflag isEqualToString:@"imageupload"])
    {
        
        [helper showWaitCursor:[helper gettranslation:@"LBL_538"]];
        sem_att = dispatch_semaphore_create(0);
        
        [self upload_attachmentdata:attachedimagename base64:[imageData base64EncodedStringWithOptions:kNilOptions]];
        while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
        [helper removeWaitCursor];
    }
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *gmtZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [formatter setTimeZone:gmtZone];
    [formatter setDateFormat:@"yyyyMMddhhmmssSSS"];
    
    [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    
    if (attendeeid)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",attendeeid];
        [helper delete_predicate:@"Attendee" predicate:predicate];
    }
    
    
    /*NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Attendee" inManagedObjectContext:context];
    NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
    
    [newrecord setValue:generatedrowid forKey:@"id"];
    [newrecord setValue:salutation_tv.text  forKey:@"salutation"];
    [newrecord setValue:salutation_tv.text  forKey:@"salutationlic"];
    [newrecord setValue:firstname_tv.text  forKey:@"firstname"];
    [newrecord setValue:lastname_tv.text  forKey:@"lastname"];
    
    [newrecord setValue:status_tv.text  forKey:@"status"];
    [newrecord setValue:status_tv.text  forKey:@"statuslic"];
    [newrecord setValue:email_tv.text  forKey:@"emailaddress"];
    [newrecord setValue:contactno_tv.text  forKey:@"contactnumber"];
    
    [newrecord setValue:speciality_tv.text  forKey:@"speciality"];
    [newrecord setValue:speciality_tv.text  forKey:@"specialitylic"];
    [newrecord setValue:integrationsrc_tv.text  forKey:@"integrationsourcelic"];
    [newrecord setValue:integrationsrc_tv.text  forKey:@"integrationsource"];
    [newrecord setValue:targetclass_tv.text  forKey:@"targetclasslic"];
    [newrecord setValue:targetclass_tv.text  forKey:@"targetclass"];
    [newrecord setValue:owner_tv.text  forKey:@"ownedby"];
    
    [newrecord setValue:[formatter stringFromDate:[NSDate date]]  forKey:@"updated"];
    [newrecord setValue:[keychain stringForKey:@"username"]  forKey:@"updatedby"];
    [newrecord setValue:imagestorageurl  forKey:@"imageid"];
    
    
    if (attendeedata.count>0)
    {
        [newrecord setValue:[attendeedata[0] valueForKey:@"affiliateid"] forKey:@"affiliateid"];
        [newrecord setValue:[attendeedata[0] valueForKey:@"integrationid"] forKey:@"integrationid"];
        [newrecord setValue:[attendeedata[0] valueForKey:@"passcode"] forKey:@"passcode"];
        [newrecord setValue:[attendeedata[0] valueForKey:@"created"] forKey:@"created"];
        [newrecord setValue:[attendeedata[0] valueForKey:@"createdby"] forKey:@"createdby"];
    }
    else
    {
        [newrecord setValue:[formatter stringFromDate:[NSDate date]] forKey:@"created"];
        [newrecord setValue:[keychain stringForKey:@"username"] forKey:@"createdby"];
    }
    
    NSError *error;
    [context save:&error];*/
    
    
    //sending record to the server
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *AttendeeDict = [[NSMutableDictionary alloc]init];
    [AttendeeDict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
    [AttendeeDict setValue:generatedrowid forKey:@"AttendeeID"];
    [AttendeeDict setValue:[helper getLic:@"CONTACT_SAL" value:salutation_tv.text] forKey:@"SalutationLIC"];
    [AttendeeDict setValue:salutation_tv.text forKey:@"Salutation"];
    [AttendeeDict setValue:firstname_tv.text forKey:@"FirstName"];
    [AttendeeDict setValue:lastname_tv.text forKey:@"LastName"];
    [AttendeeDict setValue:[helper getLic:@"SPECIALITY" value:speciality_tv.text] forKey:@"SpecialityLIC"];
    [AttendeeDict setValue:speciality_tv.text forKey:@"Speciality"];
    [AttendeeDict setValue:owner_tv.text forKey:@"OwnedBy"];
    [AttendeeDict setValue:[helper getLic:@"REC_STAT" value:status_tv.text] forKey:@"StatusLIC"];
    [AttendeeDict setValue:status_tv.text  forKey:@"Status"];
    [AttendeeDict setValue:imagestorageurl forKey:@"ImageIcon"];
    [AttendeeDict setValue:email_tv.text forKey:@"EmailAddress"];
    [AttendeeDict setValue:contactno_tv.text forKey:@"ContactNumber"];
    [AttendeeDict setValue:integrationsrc_tv.text forKey:@"IntegrationSource"];
    [AttendeeDict setValue:[helper getLic:@"INT_SRC" value:integrationsrc_tv.text] forKey:@"IntegrationSourceLIC"];
    [AttendeeDict setValue:targetclass_tv.text forKey:@"TargetClass"];
    [AttendeeDict setValue:[helper getLic:@"TRGT_CLASS" value:targetclass_tv.text]  forKey:@"TargetClassLIC"];
    [AttendeeDict setValue:InstitutionTextField.text forKey:@"Institution"];
    [AttendeeDict setValue:[helper getLic:@"SUB_TRGT_CLASS" value:SubTargetClassTextField.text] forKey:@"SubTargetClassLIC"];
    [AttendeeDict setValue:SubTargetClassTextField.text forKey:@"SubTargetClass"];
    NSMutableArray *PositionArr = [[NSMutableArray alloc]init];
    for (int i =0; i<[selectedAttendeeArr count]; i++)
    {
        NSArray *PositionDetailArr = [[helper query_alldata:@"AttendeePositions"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.positionID like %@) AND (SELF.attendeeID like %@)",[selectedAttendeeArr objectAtIndex:i],attendeeid]];
        if ([PositionDetailArr count]>0)
        {
            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
            [Dict setValue:[PositionDetailArr[0] valueForKey:@"iD"] forKey:@"ID"];
            [Dict setValue:[PositionDetailArr[0] valueForKey:@"attendeeID"] forKey:@"AttendeeID"];
            [Dict setValue:[PositionDetailArr[0] valueForKey:@"positionID"] forKey:@"PositionID"];
            [Dict setValue:[PositionDetailArr[0] valueForKey:@"positionName"] forKey:@"PositionName"];
            [PositionArr addObject:Dict];
        }
        else
        {
            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
            [Dict setValue:[helper generate_rowId] forKey:@"ID"];
            [Dict setValue:generatedrowid forKey:@"AttendeeID"];
            [Dict setValue:[selectedAttendeeArr objectAtIndex:i] forKey:@"PositionID"];
            NSArray *FilterAttendeePositionArr = [AttendeePositionArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",[selectedAttendeeArr objectAtIndex:i]]];
           // NSLog(@"Position name %@",[FilterAttendeePositionArr[0] valueForKey:@"value"]);
            
            if ([FilterAttendeePositionArr count]>0)
            {
                [Dict setValue:[FilterAttendeePositionArr[0] valueForKey:@"value"] forKey:@"PositionName"];
            }
           
            [PositionArr addObject:Dict];
        }
        
    }
                                           
    [JsonDict setValue:AttendeeDict forKey:@"Attendee"];
    [JsonDict setValue:PositionArr forKey:@"AttendeePos"];
    
    NSMutableArray *AttendeeArr = [[NSMutableArray alloc]init];
    [AttendeeArr addObject:AttendeeDict];
    //NSMutableArray *PositionArr = [[NSMutableArray alloc]init];
    
//    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
//    [JsonArray addObject:AttendeeDict];
    
    
    
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetAttendeeH" pageno:@"" pagecount:@"" lastpage:@""];
    
    appdelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Add Attendee"  Status:@"Open"];
    
    /*NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
     
     if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
     {
         [helper removeWaitCursor];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"]  message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
     
         [alert show];
         //[helper insertAttendeeList:AttendeeArr];
         //[helper insertAttendeePostion:PositionArr];
     }
     else
     {
         //NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
         //NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
         //NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
         [helper insertAttendeeList:AttendeeArr];
         [helper insertAttendeePostion:PositionArr];
    }*/
    [helper insertAttendeeList:AttendeeArr];
    [helper insertAttendeePostion:PositionArr];
    [helper removeWaitCursor];
    [self removepopup];
}

- (IBAction)selectphotobtnaction:(id)sender
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_513"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_578"] message:[helper gettranslation:@"LBL_504"] preferredStyle:UIAlertControllerStyleAlert];
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_632"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                                       {
                                           UIImagePickerController *impicker = [[UIImagePickerController alloc]init];
                                           impicker.delegate = self;
                                           impicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                           impicker.allowsEditing = NO;
                                           [self presentViewController:impicker animated:YES completion:NULL];
                                           
                                       }
                                       
                                       
                                   }]];
        
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_334"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
                                       {
                                           UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
                                           imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                           imagepicker.allowsEditing = YES;
                                           imagepicker.delegate = self;
                                           [self presentViewController:imagepicker animated:YES completion:NULL];
                                       }
                                       
                                   }]];
        /*[takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_631"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       sem_att = dispatch_semaphore_create(0);
                                       uploadflag = @"RemovePhoto";
                                       [self upload_attachmentdata:@"" base64:@""];
                                       self.profileimage.image = [UIImage imageNamed:@""];
                                       while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
                                       
                                   }]];*/

        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:takephotoalert animated:YES completion:nil];
    }
}




#pragma mark camera delegate method
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *capturedlicensimg;
    if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    if (picker.sourceType ==  UIImagePickerControllerSourceTypeCamera)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerEditedImage];
    }
    
    PickImage = @"YES";
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSURL* localUrl = (NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL];
    NSLog(@"url is %@",localUrl);
    //profileimage.image = capturedlicensimg;
   // [addimage_btn setImage:capturedlicensimg forState:UIControlStateNormal];
    uploadflag = @"imageupload";
    self.profileimage.image = capturedlicensimg;
    
    [self resizeimage];
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        attachedimagename = [imageRep filename];
        imageselcted = 1;
    };
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    NSLog(@"Attachment image name is %@",attachedimagename);
    
}

-(void)resizeimage
{
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250000;
    
    imageData = UIImageJPEGRepresentation(profileimage.image, compression);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(profileimage.image, compression);
    }
}

#pragma mark upload attachmment method
-(void) upload_attachmentdata:(NSString *) filename base64:(NSString *)base64
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[NSString stringWithFormat:@"%@.%@",[NSString stringWithString:[helper generate_rowId]],[filename pathExtension]] forKey:@"FileName"];
    [dict setValue:base64 forKey:@"Base64"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageContainer" entityname:@"S_Config_Table"] forKey:@"Container"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageAccount" entityname:@"S_Config_Table"] forKey:@"Account"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageKey" entityname:@"S_Config_Table"] forKey:@"Key"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSData *postData = [jsonStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSLog(@"storage uri %@",[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]);
    [request setURL:[NSURL URLWithString:[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]]];
    [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
    [request setValue:[keychain stringForKey:@"SLToken"] forHTTPHeaderField:@"X-ZUMO-AUTH"];
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn)
    {
        responsedata = [NSMutableData data];
    }
    else
    {
        
        // [helper printloginconsole:@"2" logtext:@"Connection could not be made in upload attachment request"];
        NSLog(@"Connection could not be made in upload attachment request");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responsedata setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responsedata appendData:data];
    
}
// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
    dispatch_semaphore_signal(sem_att);
    
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    NSString *jsonArray = [NSJSONSerialization JSONObjectWithData:responsedata options:NSJSONReadingAllowFragments error:&error];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[jsonArray dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    
    if([uploadflag isEqualToString:@"imageupload"])
    {
        imagestorageurl = [json valueForKey:@"FileURI"];
        
    }
    else if ([uploadflag isEqualToString:@"RemovePhoto"])
    {
        imagestorageurl = @"No Image";
    }
    dispatch_semaphore_signal(sem_att);
}


- (IBAction)addimage_btn:(id)sender
{
    [self selectphotobtnaction:self];
}
#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == salutation_tv)
    {
         TextfieldFlag = @"SalutationTextField";
         LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
    }
    else if (textField == status_tv)
    {
        TextfieldFlag = @"StatusTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
      
    }
    else if (textField == targetclass_tv)
    {
        TextfieldFlag = @"TargetClassTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"TRGT_CLASS"]];
    }
    else if (textField == speciality_tv)
    {
        TextfieldFlag = @"SpecialityTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
    }
    else if (textField == integrationsrc_tv)
    {
        TextfieldFlag = @"IntegrationSourceTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"INT_SRC"]];
        NSLog(@"lic :%@",[LOVArr[0] valueForKey:@"value"]);
    }
    else if (textField == SubTargetClassTextField)
    {
        TextfieldFlag = @"SubtargetClassTextField";
        NSString *targetclasslicvalue =[helper getLic:@"TRGT_CLASS" value:targetclass_tv.text] ;
       
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@ && parlov like %@ && parlic like %@",@"SUB_TRGT_CLASS",@"TRGT_CLASS",targetclasslicvalue]];
    }

    if (textField == email_tv || textField == contactno_tv || textField == firstname_tv || textField == lastname_tv)
    {
        stringlength = 50;
    }
    
    if (textField == email_tv || textField == contactno_tv || textField == status_tv || textField == salutation_tv)
    {
          [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
    }
    
    if ( textField == speciality_tv || textField == integrationsrc_tv || textField == InstitutionTextField )
    {
        [helper MoveViewUp:YES Height:240 ViewToBeMoved:self.view];
    }
    if (textField == SubTargetClassTextField || textField == targetclass_tv)
    {
         [helper MoveViewUp:YES Height:300 ViewToBeMoved:self.view];
    }
    if (textField == targetclass_tv || textField == speciality_tv || textField == integrationsrc_tv ||  textField == salutation_tv || textField == status_tv || textField == SubTargetClassTextField)
    {
        [LOVTableview reloadData];
        [self adjustHeightOfTableview:LOVTableview];
        LOVTableview.hidden = NO;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == salutation_tv || textField == status_tv || textField == email_tv || textField == contactno_tv )
    {
        [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
    }
    if (textField == InstitutionTextField || textField == speciality_tv || textField == integrationsrc_tv)
    {
        [helper MoveViewUp:NO Height:240 ViewToBeMoved:self.view];
    }
    if (textField == SubTargetClassTextField || textField == targetclass_tv)
    {
        [helper MoveViewUp:NO Height:300 ViewToBeMoved:self.view];
    }
    if (textField == salutation_tv)
    {
        if (![helper isdropdowntextCorrect:salutation_tv.text lovtype:@"CONTACT_SAL"])
        {
            salutation_tv.text = @"";
        }
    }
    if (textField == status_tv)
    {
        if (![helper isdropdowntextCorrect:status_tv.text lovtype:@"REC_STAT"])
        {
            status_tv.text = @"";
        }
    }
    if (textField == speciality_tv)
    {
        if (![helper isdropdowntextCorrect:speciality_tv.text lovtype:@"SPECIALITY"])
        {
            speciality_tv.text = @"";
        }
    }
    if (textField == integrationsrc_tv)
    {
        if (![helper isdropdowntextCorrect:integrationsrc_tv.text lovtype:@"INT_SRC"])
        {
            integrationsrc_tv.text = @"";
        }
    }
    if (textField == targetclass_tv)
    {
        if (![helper isdropdowntextCorrect:targetclass_tv.text lovtype:@"TRGT_CLASS"])
        {
            targetclass_tv.text = @"";
        }
    }
    if (textField == SubTargetClassTextField)
    {
        if (![helper isdropdowntextCorrect:SubTargetClassTextField.text lovtype:@"SUB_TRGT_CLASS"])
        {
            SubTargetClassTextField.text = @"";
        }
    }
    if(textField == contactno_tv)
    {
        if(![helper NumberValidate:contactno_tv.text])
        {
            if(![contactno_tv.text isEqualToString:@""])
            {
                [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_413"] action:[helper gettranslation:@"LBL_462"]];
                contactno_tv.text=@"";
            }
           
        }
    }
    if(textField == email_tv)
    {
        if(![self NSStringIsValidEmail:email_tv.text])
        {
            if(![email_tv.text isEqualToString:@""])
            {
                [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_414"] action:[helper gettranslation:@"LBL_462"]];
                email_tv.text=@"";
                
            }
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == salutation_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
        }
        else if (theTextField == status_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (theTextField == targetclass_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"TRGT_CLASS"]];
        }
        else if (theTextField == speciality_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (theTextField == integrationsrc_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"INT_SRC"]];
        }
      
    }
    else
    {
        if (theTextField == salutation_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
        }
        else if (theTextField == status_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (theTextField == targetclass_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"TRGT_CLASS"]];
        }
        else if (theTextField == speciality_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (theTextField == integrationsrc_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"INT_SRC"]];
        }
        if (theTextField == salutation_tv || theTextField == status_tv || theTextField == targetclass_tv || theTextField == speciality_tv || theTextField == integrationsrc_tv)
        {
            NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
            LOVArr = [filteredpartArray copy];
        }
    }
   [LOVTableview reloadData];
   
   
}
#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == LOVTableview)
    {
        CGFloat height = LOVTableview.contentSize.height;
        CGFloat maxHeight = LOVTableview.superview.frame.size.height - LOVTableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableview.frame;
       
                if([TextfieldFlag isEqualToString:@"SalutationTextField"])
                {
                    frame.origin.x = 310;
                    frame.origin.y = 185;
                    frame.size.width = 130;
                
                }
                if([TextfieldFlag isEqualToString:@"StatusTextField"])
                {
                    frame.origin.x = 310;
                    frame.origin.y = 258;
                    frame.size.width = 130;
                  
                }
                if([TextfieldFlag  isEqualToString:@"TargetClassTextField"])
                {
                    frame.origin.x = 310;
                    frame.origin.y = 383;
                    frame.size.width = 130;
                 
                }
                if([TextfieldFlag isEqualToString:@"SpecialityTextField"])
                {
                    frame.origin.x = 310;
                    frame.origin.y = 326;
                    frame.size.width = 130;
                 
                }
                if([TextfieldFlag isEqualToString:@"IntegrationSourceTextField"])
                {
                    frame.origin.x = 466;
                    frame.origin.y = 326;
                    frame.size.width = 175;
                }
                if([TextfieldFlag isEqualToString:@"SubtargetClassTextField"])
                {
                    frame.origin.x = 467;
                    frame.origin.y = 383;
                    frame.size.width = 175;
                }
            
            LOVTableview.frame = frame;
            
        }];

        }
}
#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == LOVTableview)
    {
        return [LOVArr count];
    }
    else if (tableView == UserTableView)
    {
        return [AttendeePositionArr count];
    }
    else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    if (tableView == LOVTableview)
    {
        customcell = [LOVTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
        UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
        CGRect lovtypenameframe = lovtypename.frame;
        CGRect sepratorviewframe = sepratorview.frame;
        if ([TextfieldFlag isEqualToString:@"IntegrationSourceTextField"] || [TextfieldFlag isEqualToString:@"SubtargetClassTextField"] )
        {
            lovtypenameframe.size.width = 175;
            sepratorviewframe.size.width = 175;
        }
        if ([TextfieldFlag isEqualToString:@"SalutationTextField"] || [TextfieldFlag isEqualToString:@"StatusTextField"] || [TextfieldFlag isEqualToString:@"SpecialityTextField"] || [TextfieldFlag isEqualToString:@"TargetClassTextField"] )
        {
            lovtypenameframe.size.width = 130;
            sepratorviewframe.size.width = 130;
        }
        lovtypename.frame = lovtypenameframe;
        sepratorview.frame = sepratorviewframe;
        lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        
    }
    else if (tableView == UserTableView)
    {
        customcell = [UserTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        UILabel *PositionName = (UILabel *)[contentView viewWithTag:1];
        PositionName.text = [AttendeePositionArr[indexPath.row] valueForKey:@"value"];
        //customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
        customcell.tintColor = [helper DarkBlueColour];
    }
    return customcell;
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (tableView == LOVTableview)
    {
        if ([TextfieldFlag isEqualToString:@"SalutationTextField"])
        {
            salutation_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [salutation_tv resignFirstResponder];
        }
        if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            status_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [status_tv resignFirstResponder];
        }
        if ([TextfieldFlag isEqualToString:@"TargetClassTextField"])
        {
            targetclass_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [targetclass_tv resignFirstResponder];
        }
        if ([TextfieldFlag isEqualToString:@"SpecialityTextField"])
        {
            speciality_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [speciality_tv resignFirstResponder];
        }
        if ([TextfieldFlag isEqualToString:@"IntegrationSourceTextField"])
        {
            integrationsrc_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [integrationsrc_tv resignFirstResponder];
        }
        if ([TextfieldFlag isEqualToString:@"SubtargetClassTextField"])
        {
            SubTargetClassTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [SubTargetClassTextField resignFirstResponder];
        }
        LOVTableview.hidden = YES;
    }
    else if (tableView == UserTableView)
    {
        [selectedAttendeeArr addObject:[AttendeePositionArr[indexPath.row] valueForKey:@"lic"]];
    }
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == UserTableView)
    {
        for (int i=0;i<[selectedAttendeeArr count]; i++)
        {
            NSString *item = [selectedAttendeeArr objectAtIndex:i];
            if ([item isEqualToString:[AttendeePositionArr[indexPath.row] valueForKey:@"lic"]])
            {
                [helper delete_predicate:@"AttendeePositions" predicate:[NSPredicate predicateWithFormat:@"positionID like %@",[selectedAttendeeArr objectAtIndex:i]]];
                [selectedAttendeeArr removeObject:item];
              
                
                i--;
            }
        }
    }
    
}

#pragma mark touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != LOVTableview)
    {
        LOVTableview.hidden = YES;
        [salutation_tv resignFirstResponder];
        [status_tv resignFirstResponder];
        [firstname_tv resignFirstResponder];
        [lastname_tv resignFirstResponder];
        [email_tv resignFirstResponder];
        [contactno_tv resignFirstResponder];
        [targetclass_tv resignFirstResponder];
        [speciality_tv resignFirstResponder];
        [integrationsrc_tv resignFirstResponder];
        [SubTargetClassTextField resignFirstResponder];
    }
}
#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if(searchBar == UserSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == UserSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == UserSearchBarOutlet)
    {
        [self reloadPositionList];
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.value contains[c] %@)",searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[AttendeePositionArr filteredArrayUsingPredicate:predicate]];
            AttendeePositionArr = [filtereventarr copy];
        }
        
      
      
       
        NSMutableArray *PositionidArr = [[NSMutableArray alloc]init];
        for (int i =0; i<[AttendeePositionArr count]; i++)
        {
            NSString *Positionid = [AttendeePositionArr[i] valueForKey:@"lic"];
            [PositionidArr addObject:Positionid];
        }
        
        for (int i =0; i<[selectedAttendeeArr count]; i++)
        {
            NSString *PositionId = [selectedAttendeeArr objectAtIndex:i];
            if ([PositionidArr containsObject:PositionId])
            {
                NSUInteger index = [PositionidArr indexOfObject:PositionId];
                NSInteger Positioncount = [PositionidArr count];
                if ((index > 0) || ((index == 0) && (index< Positioncount)) )
                {
                    [UserTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }//for loop end
        [UserTableView reloadData];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == email_tv || textField == contactno_tv || textField == firstname_tv || textField == lastname_tv)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}
-(void)reloadPositionList{
   AttendeePositionArr = [helper query_alldata:@"Position_List"];
    NSArray *FilteredAttendeeArr = [[helper query_alldata:@"AttendeePositions"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeID like %@",attendeeid]];
    for (int i =0; i<[FilteredAttendeeArr count]; i++)
    {
        [selectedAttendeeArr addObject:[FilteredAttendeeArr[i] valueForKey:@"positionID"]];
    }
    
    NSMutableArray *idArr = [[NSMutableArray alloc]init];
    for (int i =0; i<[AttendeePositionArr count]; i++)
    {
        if([AttendeePositionArr[i] valueForKey:@"positionID"])
            [idArr addObject:[AttendeePositionArr[i] valueForKey:@"positionID"]];
        
    }
    
    NSMutableArray *selectedUserArr = [[NSMutableArray alloc]init];
    
    for (int i =0; i<[idArr count]; i++)
    {
        NSString *PositionId = [idArr objectAtIndex:i];
        NSString *YesNoStr;
        if ([selectedAttendeeArr containsObject:PositionId])
        {
            YesNoStr = @"Y";
        }
        else
        {
            YesNoStr = @"N";
        }
        NSArray *Arr = [AttendeePositionArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"positionID like %@",PositionId]];
        if ([Arr count]>0)
        {
            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
            [Dict setValue:[Arr[0] valueForKey:@"positionName"] forKey:@"value"];
            [Dict setValue:[Arr[0] valueForKey:@"positionID"] forKey:@"lic"];
            [Dict setValue:YesNoStr forKey:@"selecteduser"];
            [selectedUserArr addObject:Dict];
        }
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"selecteduser" ascending:NO];
    AttendeePositionArr=[selectedUserArr sortedArrayUsingDescriptors:@[sort]];
    NSMutableArray *PositionidArr = [[NSMutableArray alloc]init];
    
    for (int i =0; i<[AttendeePositionArr count]; i++)
    {
        [PositionidArr addObject:[AttendeePositionArr[i] valueForKey:@"lic"]];
    }
    for (int i =0; i<[selectedAttendeeArr count]; i++)
    {
        NSString *PositionId = [selectedAttendeeArr objectAtIndex:i];
        if ([PositionidArr containsObject:PositionId])
        {
            NSUInteger index = [PositionidArr indexOfObject:PositionId];
            NSInteger Attendeecount = [PositionidArr count];
            if ((index > 0) || ((index == 0) && (index< Attendeecount)) )
            {
                [UserTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            
        }
    }
}

@end
