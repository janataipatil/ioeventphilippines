//
//  AddAttendeeView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/23/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "Reachability.h"


@interface AddAttendeeView : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *allAttendee_tbl;

@property(strong,nonatomic) HelperClass *helper;
@property (nonatomic) Reachability* reachability;
@property (weak, nonatomic) IBOutlet UIButton *groupBtn;
- (IBAction)grpBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UISearchBar *attendee_searchbar;

@property (nonatomic, readonly) NSMutableArray* AddedAttendee;


@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *title_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *salutation_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *fname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *lname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *speciality_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *targetcls_ulbl;
@property (weak, nonatomic) IBOutlet UILabel *group_lbl;
@property (weak, nonatomic) IBOutlet UITextField *group_Tv;
@property (weak, nonatomic) IBOutlet UITableView *groupTableView;








@end
