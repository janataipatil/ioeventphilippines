  //
//  AddAttendeeView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/23/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "AddAttendeeView.h"

@interface AddAttendeeView ()
{
    NSMutableArray *allAttendeeslist;
    NSInteger count;
    AppDelegate *appdelegate;
    dispatch_queue_t squeue;
    NSMutableArray *ListofAttendee;
    NSMutableArray *SelectedAttendee;
    NSArray *groupArray;
    NSArray *MailingList;
    NSArray *AttendeeList;
    NSString *selectdeGroupID;
    NSString *selectedGroupName;
    NSMutableArray *SearchgrpArray;
}

@end

@implementation AddAttendeeView
@synthesize allAttendee_tbl,helper,attendee_searchbar,AddedAttendee,reachability;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    squeue = dispatch_queue_create("iwiz.serial.Queue", NULL);
    _group_Tv.delegate=self;
    [_group_Tv setUserInteractionEnabled: NO];
    [allAttendee_tbl setEditing:YES animated:YES];
    [_groupTableView setHidden:YES];
    
    _groupTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    _groupTableView.layer.borderWidth = 1;
    
    groupArray =[[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    _group_Tv.placeholder = [helper gettranslation:@"LBL_932"];
    _group_lbl.text =[helper gettranslation:@"LBL_923"];
    
    if([appdelegate.AttendeeAddType isEqualToString:@"AddEventAttendee"])
    {
        allAttendeeslist = [NSMutableArray arrayWithArray:[[helper query_alldata:@"Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"statuslic like %@",@"Active"]]];
        
    }
    
    if ([appdelegate.AttendeeAddType isEqualToString:@"MailingList"])
    {
        SearchgrpArray= [[NSMutableArray alloc]init];
        [SearchgrpArray addObjectsFromArray:groupArray];
        
        NSMutableDictionary *grpDict = [[NSMutableDictionary alloc]init];
        [grpDict setValue:@"" forKey:@"eventid"];
        [grpDict setValue:@"" forKey:@"groupid"];
        [grpDict setValue:@"ALL" forKey:@"groupname"];
        [SearchgrpArray addObject: grpDict];
        
        groupArray = [SearchgrpArray mutableCopy];
        
        [self ReloadMailingList];
    }
    
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [attendee_searchbar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.attendee_searchbar valueForKey:@"_searchField"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
    SelectedAttendee = [[NSMutableArray alloc] init];
    
    
    NSLog(@"allAttendeeslist count : %lu",(unsigned long)allAttendeeslist.count);
    NSArray *geteventattendee = [helper query_predicate:@"Event_Attendee" name:@"eventid" value:appdelegate.eventid];
    
    if([appdelegate.AttendeeAddType isEqualToString:@"AddEventAttendee"])
    {
        for (int i=0;i<[geteventattendee count]; i++)
        {
            NSString *attendeeid = [[geteventattendee objectAtIndex:i] valueForKey:@"attendeeid"];
            for (int j=0;j<[allAttendeeslist count]; j++)
            {
                NSString *masterattendeeid = [[allAttendeeslist objectAtIndex:j] valueForKey:@"id"];
            
                if ([masterattendeeid isEqualToString:attendeeid])
                {
                    [allAttendeeslist removeObjectAtIndex:j];
                    j--;
                }
            }
        }
    }
    else
    {
        
        
        
    }
    
    ListofAttendee = [allAttendeeslist mutableCopy];
    [self langsetuptranslations];
    NSLog(@"allAttendeeslist count : %lu",(unsigned long)allAttendeeslist.count);
    NSLog(@"Add attendee array is %@",appdelegate.EventUserRoleArray);
}

-(void)langsetuptranslations
{
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    

    _salutation_ulbl.text = [helper gettranslation:@"LBL_218"];
    
    _fname_ulbl.text = [helper gettranslation:@"LBL_141"];
    _lname_ulbl.text = [helper gettranslation:@"LBL_153"];
    _speciality_ulbl.text = [helper gettranslation:@"LBL_238"];
    _targetcls_ulbl.text = [helper gettranslation:@"LBL_255"];
    _status_ulbl.text = [helper gettranslation:@"LBL_242"];
    
    
    _title_ulbl.text = [helper gettranslation:@"LBL_016"];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _groupTableView)
    {
        _groupTableView.hidden = YES;
        [attendee_searchbar resignFirstResponder];
        [_group_Tv resignFirstResponder];
        
    }
    [[self attendee_searchbar] endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _groupTableView)
    {
        return  groupArray.count;
    }
    else
    return [allAttendeeslist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";

    if(tableView == allAttendee_tbl)
    {
        
        customcell = [self.allAttendee_tbl dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        UILabel *salutation = (UILabel *) [contentView viewWithTag:2];
        UILabel *firstname = (UILabel *) [contentView viewWithTag:3];
        UILabel *lastname = (UILabel *) [contentView viewWithTag:4];
        UILabel *speciality = (UILabel *) [contentView viewWithTag:5];
        UILabel *status = (UILabel *) [contentView viewWithTag:6];
        UILabel *targetclass = (UILabel *) [contentView viewWithTag:7];
    
        [salutation setText:[allAttendeeslist[indexPath.row] valueForKey:@"salutation"]];
        [firstname setText:[NSString stringWithFormat:@"%@",[allAttendeeslist[indexPath.row] valueForKey:@"firstname"]]];
        [lastname setText:[NSString stringWithFormat:@"%@",[allAttendeeslist[indexPath.row] valueForKey:@"lastname"]]];
        [targetclass setText:[allAttendeeslist[indexPath.row] valueForKey:@"targetclass"]];
        [status setText:[allAttendeeslist[indexPath.row] valueForKey:@"status"]];
        [speciality setText:[allAttendeeslist[indexPath.row] valueForKey:@"speciality"]];
        customcell.tintColor = [helper DarkBlueColour];
    }
    else if(tableView == _groupTableView)
    {
        customcell = [self.groupTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UILabel *groupName = (UILabel *) [customcell.contentView viewWithTag:1];
       
        if([[groupArray[indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"])
        {
            groupName.text = [helper gettranslation:@"LBL_037"];
        }
        else
            groupName.text = [groupArray[indexPath.row] valueForKey:@"groupname"];
    }
    return customcell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == allAttendee_tbl)
    {
        NSString *Attendeeid = [allAttendeeslist[indexPath.row] valueForKey:@"id"];
        if (![SelectedAttendee containsObject:Attendeeid])
        {
            [SelectedAttendee addObject:[allAttendeeslist[indexPath.row] valueForKey:@"id"]];
        }
    }
    else
    {
        selectdeGroupID = [groupArray[indexPath.row] valueForKey:@"groupid"];
        _group_Tv.text = [groupArray[indexPath.row] valueForKey:@"groupname"];
        [_groupTableView setHidden:YES];
        [_group_Tv resignFirstResponder];
       
        if([appdelegate.AttendeeAddType isEqualToString:@"MailingList"]){
       
            if([[groupArray[indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"]){
                [self ReloadMailingList];
                [self.allAttendee_tbl reloadData];
            }
            else
            {
                [self ReloadMailingList];
                
                NSArray *tempAttendeList = [allAttendeeslist mutableCopy];
                NSArray *temp= [tempAttendeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID LIKE %@",[groupArray[indexPath.row] valueForKey:@"groupid"]]];
                [allAttendeeslist removeAllObjects];
                [allAttendeeslist addObjectsFromArray:temp];
                [self.allAttendee_tbl reloadData];
            }
            
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[SelectedAttendee count]; i++)
    {
        NSString *item = [SelectedAttendee objectAtIndex:i];
        if ([item isEqualToString:[allAttendeeslist[indexPath.row] valueForKey:@"id"]])
        {
            [SelectedAttendee removeObject:item];
            i--;
        }
    }
}



#pragma mark edit activity method
-(IBAction)savebtnClicked:(UIButton *)sender
{
    if (SelectedAttendee.count<=0)
    {
        [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_616"] withDuaration:1.0];
    }
    else if ([appdelegate.AttendeeAddType isEqualToString:@"MailingList"])
    {

        NSString *rowid;
        [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
        
        NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
        
        for (int i=0;i<[SelectedAttendee count]; i++)
        {
            NSLog(@"SelectedAttendee %@",[SelectedAttendee objectAtIndex:i]);
            
            NSArray *attendeedetail = [allAttendeeslist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",[SelectedAttendee objectAtIndex:i]]];
            if ([attendeedetail count]>0)
            {
                
                rowid = [NSString stringWithFormat:@"EVENTATT-%@",[helper generate_rowId]];
                NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];

                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"attendeeid"]?: @"" forKey:@"AttendeeID"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"id"]?: @"" forKey:@"EventAttendeeID"];
                [eventattendee_dict setValue:appdelegate.eventid forKey:@"EventID"];
                [eventattendee_dict setValue:rowid forKey:@"ID"];
                [eventattendee_dict setValue:appdelegate.templatename forKey:@"EventMailID"];
                
                
                [EventAttendee_ar addObject:eventattendee_dict];
            }
        }
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventMailingList" pageno:@"" pagecount:@"" lastpage:@""];
        
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSMutableArray *JsonArray = [[NSMutableArray alloc] init];
            
            [helper InsertEventMailingList:EventAttendee_ar eventid:appdelegate.eventid];
            [self removepopup];
            [helper removeWaitCursor];
        }
    }
    else
    {
        NSString *rowid;
        [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];

        NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
    
        for (int i=0;i<[SelectedAttendee count]; i++)
        {
            NSLog(@"SelectedAttendee %@",[SelectedAttendee objectAtIndex:i]);
        
            NSArray *attendeedetail = [allAttendeeslist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",[SelectedAttendee objectAtIndex:i]]];
            if ([attendeedetail count]>0)
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lovtype = %@ AND lic = %@", @"E_ATT_STAT", @"Proposed"];
                NSString *getstatus = [[[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:predicate] valueForKey:@"value"] objectAtIndex:0];
                rowid = [NSString stringWithFormat:@"EVENTATT-%@",[helper generate_rowId]];
                NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];
        
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"affiliateid" ]?: @"" forKey:@"AffiliateID"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"id"]?: @"" forKey:@"AttendeeID"];
                
                [eventattendee_dict setValue:selectdeGroupID forKey:@"GroupID"];
                [eventattendee_dict setValue:appdelegate.eventid forKey:@"EventID"];
                [eventattendee_dict setValue:rowid forKey:@"ID"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"ownedby"]?: @"" forKey:@"Owner"];
                [eventattendee_dict setValue:getstatus forKey:@"Status"];
                [eventattendee_dict setValue:@"Proposed" forKey:@"StatusLIC"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"firstname"]?: @"" forKey:@"FirstName"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"lastname"]?: @"" forKey:@"LastName"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"salutation"]?: @"" forKey:@"Salutation"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"salutationlic"] ?: @"" forKey:@"SalutationLIC"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"speciality"]?: @"" forKey:@"Speciality"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"specialitylic"]?: @"" forKey:@"SpecialityLIC"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"targetclass"]?: @"" forKey:@"TargetClass"];
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"targetclasslic"]?: @"" forKey:@"TargetClassLIC"];
                
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"subTargetClass"]?: @"" forKey:@"SubTargetClass"];
               
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"emailaddress"]?: @"" forKey:@"EmailAddress"];
                
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"integrationsource"]?: @"" forKey:@"IntegrationSource"];
                
                [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"contactnumber"]?: @"" forKey:@"ContactNumber"];
                [eventattendee_dict setValue:[helper stringtobool:@"Y"] forKey:@"CanEdit"];
                [eventattendee_dict setValue:[helper stringtobool:@"Y"] forKey:@"CanDelete"];
                
//                if([appdelegate.EventUserRoleArray containsObject:@"D_ADM"] || [appdelegate.EventUserRoleArray containsObject:@"E_ADM"])
//                {
//                     [eventattendee_dict setValue:[helper stringtobool:@"Y"] forKey:@"ApprovalFlag"];
//                }
//                else
                //{
                    [eventattendee_dict setValue:[helper stringtobool:@"N"] forKey:@"ApprovalFlag"];
                //}
                [EventAttendee_ar addObject:eventattendee_dict];
            }
        }
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventAttendee" pageno:@"" pagecount:@"" lastpage:@""];
        
        appdelegate.senttransaction = @"Y";
        [helper insert_transaction_local:msgbody entity_id:rowid type:@"Add Event Attendee" entityname:@"EventAttendee" Status:@"Open"];
        [helper addEventAttendee:appdelegate.eventid EventAttendeedata:EventAttendee_ar];
        [self removepopup];
        [helper removeWaitCursor];
    }
}

-(IBAction)cancelbtnClicked:(UIButton *)sender
{
    [self removepopup];
}


#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == attendee_searchbar)
    {
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
//    if(searchBar == attendee_searchbar)
//    {
//        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.parentViewController.view];
//        [attendee_searchbar resignFirstResponder];
//    }
   
    [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.parentViewController.view];
    [attendee_searchbar endEditing:YES];
        return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == attendee_searchbar)
    {
         allAttendeeslist = [ListofAttendee mutableCopy];
        
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.targetclass contains[c] %@) OR (SELF.speciality contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[allAttendeeslist filteredArrayUsingPredicate:predicate]];
            allAttendeeslist = [filtereventarr copy];
        }
        [allAttendee_tbl reloadData];
      
       
        NSMutableArray *AttendeeidArr = [[NSMutableArray alloc]init];
        for (int i =0; i<[allAttendeeslist count]; i++)
        {
            NSString *Attendeeid = [allAttendeeslist[i] valueForKey:@"id"];
            [AttendeeidArr addObject:Attendeeid];
        }
        
        for (int i =0; i<[SelectedAttendee count]; i++)
        {
            NSString *AttendeeId = [SelectedAttendee objectAtIndex:i];
            if ([AttendeeidArr containsObject:AttendeeId])
            {
                NSUInteger index = [AttendeeidArr indexOfObject:AttendeeId];
                NSInteger Attendeecount = [AttendeeidArr count];
                if ((index > 0) || ((index == 0) && (index< Attendeecount)) )
                {
                    [allAttendee_tbl selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
            
            
        }

    }
}

-(void)removepopup
{
    
    if([appdelegate.AttendeeAddType isEqualToString:@"MailingList"])
    {
        [self performSegueWithIdentifier:@"BackToMessageView" sender:self];
        [UIView animateWithDuration:0.3f animations:^{
            self.view.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    }
    else
    {
        appdelegate.swipevar =@"attendeeview";
        [self performSegueWithIdentifier:@"UnwindtoEventDetails" sender:self];
        [UIView animateWithDuration:0.3f animations:^{
            self.view.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if(textField == _group_Tv)
    {

        [_groupTableView setHidden:NO];
        [_groupTableView reloadData];
        [attendee_searchbar endEditing:YES];
        [_group_Tv endEditing:YES];
        [_group_Tv resignFirstResponder];
        
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    if(textField == _group_Tv)
//    {
////        [_group_Tv resignFirstResponder];
//        [_groupTableView setHidden:YES];
//        [_groupTableView reloadData];
//        
//        [_group_Tv endEditing:YES];
//        [_group_Tv resignFirstResponder];
//    }
}

-(void)ReloadMailingList{
    MailingList = [[helper query_alldata:@"Event_MailingList"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND (eventmailid like %@)",appdelegate.eventid,appdelegate.templatename]];
    
    allAttendeeslist =[NSMutableArray arrayWithArray:[[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND NOT(statuslic like % @) AND NOT(statuslic like %@)",appdelegate.eventid,@"Rejected",@"Proposed"]]];
    
    for (int i=0; i<MailingList.count; i++)
    {
        AttendeeList = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND NOT(statuslic like % @) AND NOT(statuslic like %@)",appdelegate.eventid,@"Rejected",@"Proposed"]];
        
        NSArray *tempAttendee = [AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[MailingList[i] valueForKey:@"attendeeid"]]];
        
        if([tempAttendee count]>0)
            [allAttendeeslist removeObject:tempAttendee[0]];
    }
}
- (IBAction)grpBtn:(id)sender {
    [_groupTableView setHidden:NO];
    [_groupTableView reloadData];
    [attendee_searchbar endEditing:YES];
}
@end
