//
//  AddEventActivityPlanViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 06/03/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "ToastView.h"
@interface AddEventActivityPlanViewController : UIViewController

@property(strong,nonatomic)HelperClass *Helper;
@property (strong, nonatomic) IBOutlet UISearchBar *ActivityPlanSearchBarOutlet;


@property (strong, nonatomic) IBOutlet UITableView *ActivityPlanTableView;

- (IBAction)CancelButtonAction:(id)sender;

- (IBAction)SaveButtonAction:(id)sender;



@property (strong, nonatomic) IBOutlet UILabel *title_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *planname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *description_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancel_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *createdbylabel;


@end
