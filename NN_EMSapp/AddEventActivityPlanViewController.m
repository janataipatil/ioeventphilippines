//
//  AddEventActivityPlanViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 06/03/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddEventActivityPlanViewController.h"

@interface AddEventActivityPlanViewController ()
{
    NSMutableArray *EventActivityPlanArr;
    NSMutableArray *CopyOfEventActivityPlanArr;
    NSMutableArray *SelectedActivityPlansArr;
    AppDelegate *appdelegate;

}

@end

@implementation AddEventActivityPlanViewController
@synthesize ActivityPlanTableView,ActivityPlanSearchBarOutlet,Helper;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [ActivityPlanSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [ActivityPlanSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [Helper gettranslation:@"LBL_224"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [Helper DarkBlueColour];
    EventActivityPlanArr = [NSMutableArray arrayWithArray:[[Helper query_alldata:@"ActivityPlans"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"statusLIC like %@",@"Active"]]];
    SelectedActivityPlansArr = [[NSMutableArray alloc]init];
    
    [ActivityPlanTableView setEditing:YES animated:YES];
    NSArray *ActivityPlanArr = [[Helper query_alldata:@"Activities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    for (int i = 0; i<[ActivityPlanArr count]; i++)
    {
        NSString *PlanId = [ActivityPlanArr[i] valueForKey:@"planid"];
        for (int j = 0; j<[EventActivityPlanArr count]; j++)
        {
            NSString *MasterPlanId = [EventActivityPlanArr[j] valueForKey:@"planID"];
            if ([MasterPlanId isEqualToString:PlanId])
            {
                [EventActivityPlanArr removeObjectAtIndex:j];
                j--;
            }
        }
    }
    //keep original copy for search
    CopyOfEventActivityPlanArr = [EventActivityPlanArr mutableCopy];
    
    [self langsetuptranslations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)langsetuptranslations
{
    appdelegate.translations = [Helper query_alldata:@"Lang_translations"];
    
    [_cancel_ulbl setTitle:[Helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[Helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _title_ulbl.text = [Helper gettranslation:@"LBL_012"];
    _planname_ulbl.text = [Helper gettranslation:@"LBL_201"];
    _description_ulbl.text = [Helper gettranslation:@"LBL_094"];
    _createdbylabel.text = [Helper gettranslation:@"LBL_350"];
    
}

- (IBAction)CancelButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromAddEventActivityPlanToActivities" sender:self];
    
        [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}

- (IBAction)SaveButtonAction:(id)sender
{
    if (SelectedActivityPlansArr.count<=0)
    {
        [ToastView showToastInParentView:self.view withText:[Helper gettranslation:@"LBL_617"] withDuaration:1.0];
    }
    else
    {
       
        NSMutableArray *ActivitiesArr = [[NSMutableArray alloc]init];
        NSMutableArray *CodeFieldsArr = [[NSMutableArray alloc]init];
        
        
        
        for (int i = 0; i < [SelectedActivityPlansArr count]; i++)
        {
            NSMutableDictionary *ActivityDict;
             NSLog(@"plan id is %@",[Helper query_alldata:@"PlanActivities"]);
             NSArray *ActivityArr = [[Helper query_alldata:@"PlanActivities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",[SelectedActivityPlansArr objectAtIndex:i]]];
            
            if ([SelectedActivityPlansArr count] == 1)
            {
                NSArray *ActivityArr = [[Helper query_alldata:@"PlanActivities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",[SelectedActivityPlansArr objectAtIndex:i]]];
                if ([ActivityArr count] == 0)
                {
                    // [ToastView showToastInParentView:self.view withText: withDuaration:1.0];
                    [Helper showalert:[Helper gettranslation:@"LBL_590"] message:[Helper gettranslation:@"LBL_708"] action:[Helper gettranslation:@"LBL_462"]];
                }
            }
            
            
            for (int i =0; i<[ActivityArr count];i++)
            {
                NSString *ActivityId = [Helper generate_rowId];
                ActivityDict = [[NSMutableDictionary alloc]init];
                NSMutableDictionary *newActivity = [[NSMutableDictionary alloc] init];
                [newActivity setObject:ActivityId forKey:@"ActivityID"];
                [newActivity setObject: [Helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
                [newActivity setObject:[ActivityArr[i] valueForKey:@"activityDescription"]?:@"" forKey:@"Description"];
                [newActivity setObject:appdelegate.eventid forKey:@"EventID"];
                [newActivity setObject:[Helper GetPlanName:[ActivityArr[i] valueForKey:@"planID"]] forKey:@"PlanName"];
                [newActivity setObject:[Helper stringtobool:@"Y"] forKey:@"CanDelete"];
                [newActivity setObject:[Helper stringtobool:@"Y"] forKey:@"CanEdit"];
                [newActivity setObject:[ActivityArr[i] valueForKey:@"planID"] forKey:@"PlanID"];
                [newActivity setObject:[ActivityArr[i] valueForKey:@"priorityLIC"]?:@"" forKey:@"PriorityLIC"];
                [newActivity setObject:[ActivityArr[i] valueForKey:@"priority"]?:@"" forKey:@"Priority"];
                
//                [newActivity setObject:[ActivityArr[i] valueForKey:@"statusLIC"] forKey:@"StatusLIC"];
//                [newActivity setObject:[ActivityArr[i] valueForKey:@"status"] forKey:@"Status"];
                
                
                [newActivity setObject:@"Planned" forKey:@"StatusLIC"];
                [newActivity setObject:[Helper getvaluefromlic:@"ACT_STATUS" lic:@"Planned"] forKey:@"Status"];
                
                
                [newActivity setObject:[ActivityArr[i] valueForKey:@"typeLIC"]?:@"" forKey:@"TypeLIC"];
                [newActivity setObject:[ActivityArr[i] valueForKey:@"type"] ?:@""forKey:@"Type"];
                [newActivity setObject:[ActivityArr[i] valueForKey:@"iD"]?:@""  forKey:@"PlanActivityID"];
               
                [ActivitiesArr addObject:newActivity];
                //[ActivityDict setObject:newActivity forKey:@"Activities"];
                [Helper update_activitydata:[ActivityArr[i] valueForKey:@"iD"] inputdata:newActivity source:@"Child"];
                //create activity code fields
                NSError *error;
                NSManagedObjectContext *context = [appdelegate managedObjectContext];
//                NSArray *MasterCodefieldsArr = [[Helper query_alldata:@"Act_Config_Table"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activity_typelic like %@",[ActivityArr[i] valueForKey:@"type"]]];
                
                
                NSArray *MasterCodefieldsArr = [[Helper query_alldata:@"Act_Config_Table"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activity_typelic like %@",[Helper getLic:@"ACT_TYPE" value:[ActivityArr[i] valueForKey:@"type"]]]];
                
                for (NSDictionary *record in  MasterCodefieldsArr)
                {
                    //check for dynamic feild are present already
//                    NSArray *DynamicCodeFieldsArr = [[Helper query_alldata:@"Act_dynamicdetails"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.activityid contains[c] %@) AND (SELF.fieldNameLIC contains[c] %@)",[ActivityArr[i] valueForKey:@"iD"],[record valueForKey:@"fieldnamelic"]]];
//                    if ([DynamicCodeFieldsArr count] == 0)
//                    {
                        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Act_dynamicdetails" inManagedObjectContext:context];
                        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                        [newrecord setValue:ActivityId forKey:@"activityid"];
                        [newrecord setValue:appdelegate.eventid forKey:@"eventid"];
                        [newrecord setValue:[record valueForKey:@"fieldname"] forKey:@"fieldName"];
                        [newrecord setValue:[record valueForKey:@"fieldnamelic"] forKey:@"fieldNameLIC"];
                        [newrecord setValue:[record valueForKey:@"fieldtype"] forKey:@"fieldType"];
                        [newrecord setValue:[record valueForKey:@"fieldtypelic"] forKey:@"fieldTypeLic"];
                        [newrecord setValue:@"" forKey:@"fieldValue"];
                        [newrecord setValue:[Helper generate_rowId] forKey:@"id"];
                        [newrecord setValue:[record valueForKey:@"picklist_type"] forKey:@"picklistType"];
                        [newrecord setValue:[record valueForKey:@"required"] forKey:@"required"];
                        [newrecord setValue:[record valueForKey:@"sequence"] forKey:@"sequence"];
                        [context save:&error];
                    //}
                }
           // NSMutableArray *ActivityCodeFieldArr = [[NSMutableArray alloc]init];
            NSLog(@"Activity code fields %@",[Helper query_alldata:@"Act_dynamicdetails"]);
            NSArray *CreatedCodefieldsArr = [[Helper query_alldata:@"Act_dynamicdetails"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"((SELF.eventid contains[c] %@) AND (SELF.activityid contains[c] %@))",appdelegate.eventid,ActivityId]];
            for (NSDictionary *record in CreatedCodefieldsArr)
            {
                NSMutableDictionary *CodeFieldDict = [[NSMutableDictionary alloc]init];
                [CodeFieldDict setObject:[record valueForKey:@"id"] forKey:@"ID"];
                [CodeFieldDict setObject:[record valueForKey:@"eventid"] forKey:@"EventID"];
                [CodeFieldDict setObject:[record valueForKey:@"activityid"] forKey:@"ActivityID"];
                [CodeFieldDict setObject:[record valueForKey:@"fieldNameLIC"] forKey:@"FieldNameLIC"];
                [CodeFieldDict setObject:[record valueForKey:@"fieldTypeLic"] forKey:@"FieldTypeLIC"];
                [CodeFieldDict setObject:[record valueForKey:@"fieldValue"]?:@"" forKey:@"FieldValue"];
                [CodeFieldDict setObject:[record valueForKey:@"picklistType"]?:@"" forKey:@"PickListType"];
                [CodeFieldDict setObject:[Helper stringtobool:[record valueForKey:@"required"]] forKey:@"Required"];
                [CodeFieldDict setObject:[record valueForKey:@"sequence"] forKey:@"Sequence"];
                //[ActivityCodeFieldArr addObject:CodeFieldDict];
                 [CodeFieldsArr addObject:CodeFieldDict];
            }
           // [CodeFieldsArr addObject:ActivityCodeFieldArr];
                //[ActivityDict setObject:ActivityCodeFieldArr forKey:@"ActCodeFields"];
            //[PayloadArr addObject:ActivityDict];
           
         }
        NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
        [PayloadDict setValue:CodeFieldsArr forKey:@"ActCodeFields"];
        [PayloadDict setValue:ActivitiesArr forKey:@"Activities"];
       //  NSMutableArray *PayloadArr =[[NSMutableArray alloc]init];
        //[PayloadArr addObject:PayloadDict];
        NSError *error;
        NSString *jsonString;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadDict options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [Helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [Helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [Helper create_trans_msgbody:base64String txType:@"EMSSetActivitiesH" pageno:@"" pagecount:@"" lastpage:@""];
        
        appdelegate.senttransaction = @"Y";
        
        [Helper insert_transaction_local:msgbody entity_id:[Helper generate_rowId] type:@"Add Activities" entityname:@"Add Activities"  Status:@"Open"];
        

    }
        [Helper removeWaitCursor];
        [self CancelButtonAction:self];

    }
}
#pragma mark tablevie delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [EventActivityPlanArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *PlanNamelbl = (UILabel *)[contentView viewWithTag:1];
    PlanNamelbl.text = [EventActivityPlanArr[indexPath.row] valueForKey:@"name"];
    UILabel *PlanDescriptionlbl = (UILabel *)[contentView viewWithTag:2];
    PlanDescriptionlbl.text = [EventActivityPlanArr[indexPath.row] valueForKey:@"activityPlandescription"];
    UILabel *CreatedBylbl = (UILabel *)[contentView viewWithTag:3];
    CreatedBylbl.text = [EventActivityPlanArr[indexPath.row] valueForKey:@"createdBy"];
    //customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    customcell.tintColor = [Helper DarkBlueColour];
    return customcell;
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SelectedActivityPlansArr addObject:[EventActivityPlanArr[indexPath.row] valueForKey:@"planID"]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[SelectedActivityPlansArr count]; i++)
    {
        NSString *item = [SelectedActivityPlansArr objectAtIndex:i];
        if ([item isEqualToString:[EventActivityPlanArr[indexPath.row] valueForKey:@"planID"]])
        {
            [SelectedActivityPlansArr removeObject:item];
            i--;
        }
    }
}

#pragma mark search bar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == ActivityPlanSearchBarOutlet)
    {
        EventActivityPlanArr = [CopyOfEventActivityPlanArr mutableCopy];
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[c] %@) OR (SELF.activityPlandescription contains[c] %@) OR (SELF.createdBy contains[c] %@)",searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[EventActivityPlanArr filteredArrayUsingPredicate:predicate]];
            EventActivityPlanArr = [filtereventarr copy];
        }
       [ActivityPlanTableView reloadData];
        
       
    }
}

#pragma mark unwind method
-(void)unwindtoEventType:(UIStoryboardSegue *)segue
{
    
}
@end
