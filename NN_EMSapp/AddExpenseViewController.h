//
//  AddExpenseViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 02/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "HSDatePickerViewController.h"
#import "AppDelegate.h"
#import "CommentView.h"
#import "NotesViewController.h"

@interface AddExpenseViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *ExpenseHeaderlbl;
@property (strong, nonatomic) IBOutlet UITextField *ExpenseTypeTextField;
@property (strong, nonatomic) IBOutlet UITextField *PaymentTypeTextField;
@property (strong, nonatomic) IBOutlet UITextField *NotesTextField;
@property (strong, nonatomic) IBOutlet UITextField *AmountTextField;
@property (strong, nonatomic) IBOutlet UITextField *ExpenseCurrencyTextField;

@property (strong, nonatomic) IBOutlet UILabel *totalAmount_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *expenseCurrency_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *conversionRate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *expenseStatus;

@property (strong, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) IBOutlet UILabel *expenseStatus_ulbl;

@property (strong, nonatomic) IBOutlet UITableView *ExpenseHistoryTableView;

@property (strong, nonatomic) IBOutlet UITextField *ConversionRateTextField;

@property (strong, nonatomic) IBOutlet UITableView *LOVTableView;
@property (strong, nonatomic) IBOutlet UISwitch *ReceiptSwitchOutlet;
@property (strong, nonatomic) IBOutlet UIButton *ExpenseDateButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitBtn:(id)sender;

@property(strong,nonatomic)HelperClass *helper;
- (IBAction)SaveButtonAction:(id)sender;
- (IBAction)CancelButtonAction:(id)sender;
//- (IBAction)ReceiptValueChangedAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *attachReceiptLBL;

- (IBAction)ExpenseDateButtonAction:(id)sender;
- (IBAction)ReceiptValueChangedAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *attachReceiptBtn;
- (IBAction)attachReceiptBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *expenseApprovals_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *expensetype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *expensedate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *amount_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *receipts_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *notes_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *paymenttype_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancel_ulbl;

@end
