//
//  AddExpenseViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 02/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddExpenseViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface AddExpenseViewController ()<HSDatePickerViewControllerDelegate>
{
    NSString *TextfieldFlag;
    NSArray *LOVArr;
    NSString *attachedimagename;
    NSString *PathExtension;
    NSPredicate *ExpenseTypePredicate;
    NSPredicate *PaymentTypePredicate;
    NSPredicate *ExpenseIDPredicate;
    NSPredicate *CurrencyPredicate;
    
    AppDelegate *appDelegate;
    NSString *StorageURL;
    NSString *ExpenseID;
    NSString *ReceiptFlag;
    NSMutableData *responsedata;
     dispatch_semaphore_t sem_att;
    NSString *OwnedBy;
    NSArray *ExpenseArr;
    NSString *transaction_type;
    NSString *CanEdit;
    NSData *filedata;
    NSString *CanDelete;
    UIImage *capturedlicensimg;
    NSUInteger stringlength;
    SLKeyChainStore *keychain;
    NSString *receiptUploadFlag;
    NSString *currencySymbol;
    NSString *expenseApprovalComment;
    NSArray *expenseApprovalArray;
    float totalAmt;
    NSString *txType;
    NSString *rowid;
    NSManagedObjectContext *context;
    
    BOOL rmvReciept;
    NSString *semp;
    dispatch_semaphore_t sem_event,sem_test;
    
    
    NSString *ExpenseStatusType;
    float ConversionRate;
}
@property (nonatomic, strong) NSDate *selectedDate;

@end

@implementation AddExpenseViewController
@synthesize ExpenseHeaderlbl,ExpenseTypeTextField,PaymentTypeTextField,NotesTextField,AmountTextField,LOVTableView,ReceiptSwitchOutlet,helper,ExpenseDateButtonOutlet;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    context=[appDelegate managedObjectContext];

    
    appDelegate.translations = [helper query_alldata:@"Lang_translations"];
    ReceiptSwitchOutlet.transform = CGAffineTransformMakeScale(0.60, 0.60);
  //  ReceiptSwitchOutlet.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    
    ReceiptSwitchOutlet.onTintColor = [helper DarkBlueColour];
    LOVTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableView.layer.borderWidth = 1;
    [ExpenseTypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [PaymentTypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    ExpenseTypePredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"EXP_TYPE"];
    PaymentTypePredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"PMT_TYPE"];
    
    CurrencyPredicate=[NSPredicate predicateWithFormat:@"lovtype like %@",@"CC_CODE"];
    
    ExpenseIDPredicate = [NSPredicate predicateWithFormat:@"id like %@",appDelegate.ExpenseID];
    
    NSString *currencyCode=[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"]?:@"";
    
    if([currencyCode isEqualToString:@"No Matching Data Found"])
        currencyCode =@"";
    
    // NSString *currencyCode = @"EUR";
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:currencyCode];
    currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:currencyCode]];

    rmvReciept=NO;
    _attachReceiptLBL.text=[helper gettranslation:@"LBL_813"];
    
    receiptUploadFlag=@"NO";    //This flags keeps a track if  receipt is modified or not.
    
     keychain = [SLKeyChainStore keyChainStoreWithService:appDelegate.keychainName];
    
    
    [self langsetuptranslations];
    
    _submitBtn.layer.masksToBounds=YES;
    _submitBtn.layer.cornerRadius=5.0;
    
    _expenseStatus.text=[helper getvaluefromlic:@"EXP_STATUS" lic:@"New"];
    ExpenseTypeTextField.placeholder=[helper gettranslation:@"LBL_757"];
    PaymentTypeTextField.placeholder=[helper gettranslation:@"LBL_757"];
    
    if ([appDelegate.ExpenseID length]>0)
    {
        [self setupExpenseView];
        
        if([CanEdit isEqualToString:@"N"])
        ExpenseHeaderlbl.text=[helper gettranslation:@"LBL_815"];
        else
        ExpenseHeaderlbl.text = [helper gettranslation:@"LBL_110"];
        
        transaction_type = @"Edit Expense";
       
    }
    else
    {
        
        ExpenseHeaderlbl.text = [helper gettranslation:@"LBL_019"];
        ExpenseID = [helper generate_rowId];
       
        _ExpenseCurrencyTextField.text=currencyCode;
        _ConversionRateTextField.text=@"1";
        
        [_expenseStatus setTextColor:[helper getstatuscolor:@"New" entityname:@"Expense"]];
        [_submitBtn setHidden:YES];
       SLKeyChainStore *Keychain = [SLKeyChainStore keyChainStoreWithService:appDelegate.keychainName];
    
//        NSPredicate *UserNamepredicate = [NSPredicate predicateWithFormat:@"userid like %@",[Keychain stringForKey:@"username"]];
        NSPredicate *UserNamepredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[Keychain stringForKey:@"username"]];
        
        NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:UserNamepredicate];
        if ([userarr count]>0)
        {
            OwnedBy = [userarr[0] valueForKey:@"userid"];
        }
        transaction_type = @"Add Expense";
        ReceiptFlag = @"N";
        [ReceiptSwitchOutlet setOn:NO];
        CanEdit = @"Y";
        CanDelete = @"Y";

    }
}
-(void)setupExpenseView
{
    
    expenseApprovalArray=[[helper query_alldata:@"Expense_Approvals"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"expenseID like %@",appDelegate.ExpenseID]];
    
    
    ExpenseArr = [[helper query_alldata:@"Event_Expense"]filteredArrayUsingPredicate:ExpenseIDPredicate];
    if ([ExpenseArr count]>0)
    {
        ExpenseID = [ExpenseArr[0] valueForKey:@"id"];
        
        ExpenseTypeTextField.text = [helper getvaluefromlic:@"EXP_TYPE" lic:[ExpenseArr[0] valueForKey:@"expenseTypeLIC"]];
        PaymentTypeTextField.text = [helper getvaluefromlic:@"PMT_TYPE" lic:[ExpenseArr[0] valueForKey:@"paymentTypeLIC"]];
        
        
        _totalAmount.text=[NSString stringWithFormat:@"%@ %@",currencySymbol,[ExpenseArr[0] valueForKey:@"amount"]];
        
        AmountTextField.text=[ExpenseArr[0] valueForKey:@"expenseAmount"];
        _ConversionRateTextField.text=[ExpenseArr[0] valueForKey:@"conversionRate"];
        
        _ExpenseCurrencyTextField.text= [helper getvaluefromlic:@"CC_CODE" lic:[ExpenseArr[0] valueForKey:@"ExpenseCurrencyLIC"]];

        _expenseStatus.text=[ExpenseArr[0] valueForKey:@"status"];
        
        
        
        [_expenseStatus setTextColor:[helper getstatuscolor:[ExpenseArr[0] valueForKey:@"statuslic"] entityname:@"Expense"]];
        
//        if([[ExpenseArr[0] valueForKey:@"status"] isEqualToString:@"New"] ||[[ExpenseArr[0] valueForKey:@"status"] isEqualToString:@"Rejected"] )
//        {
//            [_submitBtn setHidden:NO];
//        }
//        else
//        {
            [_submitBtn setHidden:YES];
       // }
        
        [ExpenseDateButtonOutlet setTitle:[helper formatingdate:[ExpenseArr[0] valueForKey:@"ExpenseDate"] datetime:@"datetime"] forState:UIControlStateNormal];
        
        AmountTextField.text = [ExpenseArr[0] valueForKey:@"expenseAmount"];
        NotesTextField.text = [ExpenseArr[0] valueForKey:@"purpose"];
    
        
        if([[ExpenseArr[0] valueForKey:@"reciepturl"]length]>0)
        {
            _attachReceiptLBL.text=[ExpenseArr[0] valueForKey:@"filename"];
        }
        else
            _attachReceiptLBL.text=[helper gettranslation:@"LBL_813"];
        
        
        OwnedBy = [ExpenseArr[0] valueForKey:@"ownedBy"];
        CanEdit = [ExpenseArr[0] valueForKey:@"canEdit"];
        CanDelete = [ExpenseArr[0] valueForKey:@"canDelete"];
        if ([[ExpenseArr[0] valueForKey:@"recieptsFlag"] isEqualToString:@"Y"])
        {
            ReceiptFlag = @"Y";
            [ReceiptSwitchOutlet setOn:YES];
        }
        else  if ([[ExpenseArr[0] valueForKey:@"recieptsFlag"] isEqualToString:@"N"])
        {
            ReceiptFlag = @"N";
            [ReceiptSwitchOutlet setOn:NO];
        }
        if ([CanEdit isEqualToString:@"N"] ||[[ExpenseArr[0] valueForKey:@"statuslic"] isEqualToString:@"Submitted"] ||[[ExpenseArr[0] valueForKey:@"statuslic"] isEqualToString:@"Approved"])
        {
            ExpenseTypeTextField.userInteractionEnabled = NO;
            PaymentTypeTextField.userInteractionEnabled = NO;
            ExpenseDateButtonOutlet.userInteractionEnabled = NO;
            AmountTextField.userInteractionEnabled = NO;
            NotesTextField.userInteractionEnabled = NO;
            
            [_ExpenseCurrencyTextField setUserInteractionEnabled:NO];
            [_ConversionRateTextField  setUserInteractionEnabled:NO];
            
            [_attachReceiptBtn setUserInteractionEnabled:NO];
            ReceiptSwitchOutlet.userInteractionEnabled = NO;
            [_savebtn_ulbl setHidden:YES];
            ExpenseHeaderlbl.text=[helper gettranslation:@"LBL_815"];
            
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)langsetuptranslations
{
    appDelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancel_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _expensetype_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_266"]];
    [self markasrequired:_expensetype_ulbl];
    
    _expensedate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_074"]];
    [self markasrequired:_expensedate_ulbl];
    
    _paymenttype_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_195"]];
    [self markasrequired:_paymenttype_ulbl];

    _notes_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_855"]];
    
    _amount_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_041"]];
    [self markasrequired:_amount_ulbl];
    
    _expenseCurrency_ulbl.text=[NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_073"]];
    [self markasrequired:_expenseCurrency_ulbl];
    
    _conversionRate_ulbl.text=[NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_840"]];
    [self markasrequired:_conversionRate_ulbl];

    _totalAmount_ulbl.text=[NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_841"]];

    _expenseApprovals_ulbl.text=[NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_848"]];

    _receipts_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_210"]];
    
    
    [_submitBtn setTitle:[helper gettranslation:@"LBL_248"] forState:UIControlStateNormal];

}

-(void) markasrequired:(UILabel *) label
{
    int labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
}

- (IBAction)SaveButtonAction:(id)sender
{
    
    txType=@"SaveExpenseData";
    
    if([appDelegate.ExpenseID length]>0)
    {
        if ([ExpenseArr count]>0)
        {
            if ([ExpenseTypeTextField.text isEqualToString:[ExpenseArr[0] valueForKey:@"expensetype"]?: @""] && [PaymentTypeTextField.text isEqualToString:[ExpenseArr[0] valueForKey:@"paymenttype"]?: @""] && [ExpenseDateButtonOutlet.titleLabel.text isEqualToString:[helper formatingdate:[ExpenseArr[0] valueForKey:@"expenseDate"] datetime:@"datetime"]?: @""] && [AmountTextField.text isEqualToString:[ExpenseArr[0] valueForKey:@"expenseAmount"]?: @""] && [NotesTextField.text isEqualToString:[ExpenseArr[0] valueForKey:@"purpose"]?: @""] && [ReceiptFlag isEqualToString:[ExpenseArr[0] valueForKey:@"recieptsFlag"]?:@""] &&[receiptUploadFlag isEqualToString:@"NO"] && [_ExpenseCurrencyTextField.text isEqualToString:[ExpenseArr[0] valueForKey:@"expenseCurrency"]?:@""]  && [_ConversionRateTextField.text isEqualToString:[ExpenseArr[0] valueForKey:@"conversionRate"]?:@""])
                {
                    [self CancelButtonAction:self];
                    return;
                }
            else
                {
                    [helper delete_predicate:@"Event_Expense" predicate:ExpenseIDPredicate];
                    [self saveExpenseData];
                }
        }
    }
    else
    [self saveExpenseData];
    
    
    }


-(void)saveExpenseData
{
    if ([ExpenseTypeTextField.text length]>0 && [PaymentTypeTextField.text length]>0 && [ExpenseDateButtonOutlet.titleLabel.text length]>0 && [AmountTextField.text length]>0 && [_ConversionRateTextField.text length]>0 && [_ExpenseCurrencyTextField.text length]>0)
    {
        [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
        if(![StorageURL isEqualToString:@" "])
        {
            sem_att = dispatch_semaphore_create(0);
            [self upload_attachmentdata:attachedimagename base64:[filedata base64EncodedStringWithOptions:kNilOptions]];
            while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
        }
        
        NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
        
        if([txType isEqualToString:@"SaveExpenseData"])
        {
            [PayloadDict setValue:appDelegate.eventid forKey:@"EventID"];
            [PayloadDict setValue:[helper converingformateddatetooriginaldate:ExpenseDateButtonOutlet.titleLabel.text datetype:@"ExpenseDate"] forKey:@"ExpenseDate"];
            [PayloadDict setValue:ExpenseTypeTextField.text forKey:@"ExpenseType"];
            [PayloadDict setValue:[helper getLic:@"EXP_TYPE" value:ExpenseTypeTextField.text] forKey:@"ExpenseTypeLIC"];
            [PayloadDict setValue:ExpenseID forKey:@"ID"];
            [PayloadDict setValue:OwnedBy forKey:@"OwnedBy"];
            [PayloadDict setValue:[helper stringtobool:ReceiptFlag] forKey:@"RecieptsFlag"];
        
            [PayloadDict setValue:[helper getLic:@"CC_CODE" value:_ExpenseCurrencyTextField.text] forKey:@"ExpenseCurrencyLIC"];
            
            [PayloadDict setValue:AmountTextField.text forKey:@"ExpenseAmount"];
            
            float amt=[AmountTextField.text floatValue];
        
            
            
            _totalAmount.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];

            if([_ConversionRateTextField.text length]>0)
            {
                [PayloadDict setValue:_ConversionRateTextField.text forKey:@"ConversionRate"];
                ConversionRate=[_ConversionRateTextField.text floatValue];
            }
            else
            {
                [PayloadDict setValue:@"1" forKey:@"ConversionRate"];
                ConversionRate=1;
            }
            
            totalAmt=amt*ConversionRate;
            
            _totalAmount.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
            [PayloadDict setValue:[helper getvaluefromlic:@"EXP_STATUS" lic:_expenseStatus.text] forKey:@"Status"];
            [PayloadDict setValue:[helper getLic:@"EXP_STATUS" value:_expenseStatus.text] forKey:@"StatusLIC"];
            
            [PayloadDict setValue:[NSString stringWithFormat:@"%.2f",totalAmt] forKey:@"Amount"];
            
            [PayloadDict setValue:[helper getLic:@"PMT_TYPE" value:PaymentTypeTextField.text]  forKey:@"PaymentTypeLIC"];
            [PayloadDict setValue:PaymentTypeTextField.text forKey:@"PaymentType"];
            [PayloadDict setValue:NotesTextField.text forKey:@"Purpose"];
            [PayloadDict setValue:[helper stringtobool:CanDelete] forKey:@"CanDelete"];
            [PayloadDict setValue:[helper stringtobool:CanEdit] forKey:@"CanEdit"];
            [PayloadDict setValue:StorageURL forKey:@"RecieptURL"];
            [PayloadDict setValue:attachedimagename forKey:@"RecieptName"];
            
        }
        else
        {
            [PayloadDict setValue:ExpenseID forKey:@"ID"];
            [PayloadDict setValue:appDelegate.eventid forKey:@"EventID"];
            [PayloadDict setValue:[helper getLic:@"EXP_STATUS" value:@"Submitted"] forKey:@"StatusLIC"];
        }
        
        NSMutableArray *PayloadArray = [[NSMutableArray alloc]init];
        [PayloadArray addObject:PayloadDict];
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArray options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventExpense" pageno:@"" pagecount:@"" lastpage:@""];
      //  appDelegate.senttransaction = @"Y";
       // [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Event Expense"  Status:@"Open"];
        
        
        
        
        
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
         
         if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
         {
             [helper removeWaitCursor];

         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
         
         [alert show];
         }
         else
         {
         NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
         NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
         
         }
        if([txType isEqualToString:@"SaveExpenseData"])
            [helper insertEventExpense:PayloadArray];
        
        [helper removeWaitCursor];

        
        [self CancelButtonAction:self];
        
    }
    else
    {
        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] preferredStyle:UIAlertControllerStyleAlert];
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:takephotoalert animated:YES completion:nil];
    }

}


- (IBAction)CancelButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"UnwindFromAddExpenseToExpense" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

//- (IBAction)ReceiptValueChangedAction:(id)sender
//{
//    
//}

- (IBAction)ExpenseDateButtonAction:(id)sender
{
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    if (self.selectedDate) {
        hsdpvc.date = self.selectedDate;
    }
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (IBAction)ReceiptValueChangedAction:(id)sender
{
    if([ReceiptFlag isEqualToString:@"Y"])
    {
        ReceiptFlag = @"N";
        [ReceiptSwitchOutlet setOn:NO];
    }
    else if ([ReceiptFlag isEqualToString:@"N"])
    {
        ReceiptFlag = @"Y";
        [ReceiptSwitchOutlet setOn:YES];
    }
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _ExpenseHistoryTableView)
    {
        return [expenseApprovalArray count];
    }
    else
    return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    if(tableView == _ExpenseHistoryTableView)
    {
        cellIdentifier =@"expenseApprovals";
        customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
        UILabel *stepName=(UILabel *)[contentView viewWithTag:1];
        UILabel *Approver=(UILabel *)[contentView viewWithTag:2];
        UILabel *ParentStepName=(UILabel *)[contentView viewWithTag:3];
        UILabel *Status=(UILabel *)[contentView viewWithTag:4];
        UITextField *Comment=(UITextField *)[contentView viewWithTag:5];
        UIButton *CmntBtn=(UIButton *)[contentView viewWithTag:10];
      
        UIButton *AcceptBtn=(UIButton *)[contentView viewWithTag:6];
        UIButton *RejectBtn=(UIButton *)[contentView viewWithTag:7];

        stepName.text=[expenseApprovalArray[indexPath.row] valueForKey:@"ApprovalFor"];
        ParentStepName.text=[expenseApprovalArray[indexPath.row] valueForKey:@"ParentApprovalName"];
        Approver.text=[expenseApprovalArray[indexPath.row] valueForKey:@"approverName"];
        Status.text=[expenseApprovalArray[indexPath.row] valueForKey:@"statusLIC"];
        
        [Status setTextColor:[helper getstatuscolor:[expenseApprovalArray[indexPath.row] valueForKey:@"statusLIC"] entityname:@"Expense"]];
        Comment.text=[expenseApprovalArray[indexPath.row] valueForKey:@"comment"];
        
        
        [AcceptBtn addTarget:self action:@selector(ApproveExpense:) forControlEvents:UIControlEventTouchUpInside];
        [RejectBtn addTarget:self action:@selector(RejectExpense:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if([[expenseApprovalArray[indexPath.row] valueForKey:@"approvalFlag"] isEqualToString:@"N"] || [[expenseApprovalArray[indexPath.row] valueForKey:@"status"] isEqualToString:@"Approved"] || [[expenseApprovalArray[indexPath.row] valueForKey:@"status"] isEqualToString:@"Rejected"])
        {
            [AcceptBtn setHidden:YES];
            [RejectBtn setHidden:YES];
            
        }
        else
        {
            [AcceptBtn setHidden:NO];
            [RejectBtn setHidden:NO];
        }
        
        [CmntBtn addTarget:self action:@selector(TextFieldActive:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if(indexPath.row % 2 == 0)
        {
            contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
        }
        else
        {
            contentView.backgroundColor = [UIColor clearColor];
        }

        
        
        return customcell;
    }
    else
    {
        cellIdentifier = @"customcell";
    
        customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        UILabel *Typlbl = (UILabel *)[contentView viewWithTag:1];
    
        Typlbl.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    
    
        customcell.selectionStyle = UITableViewCellSelectionStyleNone;
        return customcell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([TextfieldFlag isEqualToString:@"ExpenseTypeTextField"])
    {
        ExpenseTypeTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [ExpenseTypeTextField resignFirstResponder];
    }
    else if ([TextfieldFlag isEqualToString:@"PaymentTypeTextField"])
    {
        PaymentTypeTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [PaymentTypeTextField resignFirstResponder];
    }
    else if ([TextfieldFlag isEqualToString:@"ExpenseCurrencyTextField"])
    {
        _ExpenseCurrencyTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_ExpenseCurrencyTextField resignFirstResponder];
    }
    
    
    LOVTableView.hidden = YES;
}

-(IBAction)TextFieldActive:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_ExpenseHistoryTableView];
    NSIndexPath *indexPath = [_ExpenseHistoryTableView indexPathForRowAtPoint:buttonPosition];
    
    expenseApprovalComment=[expenseApprovalArray[indexPath.row] valueForKey:@"comment"];
    
    [self performSegueWithIdentifier:@"previewComment" sender:self];
    
    [sender resignFirstResponder];
}
#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == ExpenseTypeTextField)
    {
        TextfieldFlag = @"ExpenseTypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:ExpenseTypePredicate];
    }
    else if (textField == PaymentTypeTextField)
    {
        TextfieldFlag = @"PaymentTypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:PaymentTypePredicate];
    }
    else if(textField == _ExpenseCurrencyTextField)
    {
        TextfieldFlag = @"ExpenseCurrencyTextField";
        LOVArr=[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:CurrencyPredicate];
    }
    if (textField == ExpenseTypeTextField || textField == PaymentTypeTextField || textField == _ExpenseCurrencyTextField)
    {
       [LOVTableView reloadData];
       [self adjustHeightOfTableview:LOVTableView];
        LOVTableView.hidden = NO;
    }
    
    if(textField == _ConversionRateTextField)
    {
        _ConversionRateTextField.keyboardType = UIKeyboardTypeNumberPad;
    }
    if (textField == PaymentTypeTextField)
    {
        [helper MoveViewUp:YES Height:10 ViewToBeMoved:self.view];
    }
//    if(textField ==AmountTextField)
//    {
//        if (AmountTextField.text>0)
//        {
//            AmountTextField.text = [helper CurrencytoFloat:AmountTextField.text];
//        }
//    }
    if (textField == NotesTextField)
    {
        [helper MoveViewUp:YES Height:70 ViewToBeMoved:self.view];
        stringlength = 100;
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == PaymentTypeTextField)
    {
        [helper MoveViewUp:NO Height:10 ViewToBeMoved:self.view];
        if (![helper isdropdowntextCorrect:PaymentTypeTextField.text lovtype:@"PMT_TYPE"])
        {
            PaymentTypeTextField.text = @"";
        }
    }
    if(textField == NotesTextField)
    {
       [helper MoveViewUp:NO Height:70 ViewToBeMoved:self.view];
    }
    
    if(textField==_ConversionRateTextField)
    {
        if(![helper CostValidate:_ConversionRateTextField.text])
        {
            _ConversionRateTextField.text=@"";
        }
        else
        {
            float amt=[AmountTextField.text floatValue];
           
            if([_ConversionRateTextField.text length]>0)
            {
                ConversionRate=[_ConversionRateTextField.text floatValue];
            }
            else
                ConversionRate=1.0;

             totalAmt=amt*ConversionRate;
            
            _totalAmount.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
        }
    }
    
    if (textField == ExpenseTypeTextField)
    {
        if (![helper isdropdowntextCorrect:ExpenseTypeTextField.text lovtype:@"EXP_TYPE"])
        {
            ExpenseTypeTextField.text = @"";
        }
        
    }
    
    if(textField == _ExpenseCurrencyTextField)
    {
        if(![helper isdropdowntextCorrect:_ExpenseCurrencyTextField.text lovtype:@"CC_CODE"])
        {
            _ExpenseCurrencyTextField.text=@"";
        }
            
    }
    
    if(textField ==AmountTextField)
    {
        if(![helper CostValidate:AmountTextField.text]||AmountTextField.text.length>8)
        {
            //[helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_765"]action:[helper gettranslation:@"LBL_462"]];
            AmountTextField.text=@"";
        }
        else
        {
            
            float amt=[AmountTextField.text floatValue];
            
            
            
            if([_ConversionRateTextField.text length]>0)
            {
                ConversionRate=[_ConversionRateTextField.text floatValue];
            }
            else
                ConversionRate=1;
            
            
             totalAmt=amt*ConversionRate;
            
            _totalAmount.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
            
        }

    }
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == ExpenseTypeTextField )
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:ExpenseTypePredicate];
        }
        else if (theTextField == PaymentTypeTextField)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:PaymentTypePredicate];
        }
        else if (theTextField == _ExpenseCurrencyTextField)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:CurrencyPredicate];
        }
    }
    else
    {
        if (theTextField == ExpenseTypeTextField )
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:ExpenseTypePredicate];
        }
        else if (theTextField == PaymentTypeTextField )
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:PaymentTypePredicate];
        }
        else if (theTextField == _ExpenseCurrencyTextField)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:CurrencyPredicate];
        }
        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
        LOVArr = [filteredpartArray copy];
        
    }
    [LOVTableView reloadData];
}

#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    
    CGFloat height = LOVTableView.contentSize.height;
    CGFloat maxHeight = LOVTableView.superview.frame.size.height - LOVTableView.frame.origin.y;
    if (height > maxHeight)
        height = maxHeight;
    [UIView animateWithDuration:0.0 animations:^{
        CGRect frame = LOVTableView.frame;
        
        
        if([TextfieldFlag isEqualToString:@"ExpenseTypeTextField"])
        {
            frame.origin.x = 102;
            frame.origin.y = 173;
        }
        if([TextfieldFlag isEqualToString:@"PaymentTypeTextField"])
        {
            frame.origin.x = 680;
            frame.origin.y = 177;
        }
        if([TextfieldFlag isEqualToString:@"ExpenseCurrencyTextField"])
        {
            frame.origin.x = 102;
            frame.origin.y = 246;
        }
        
        LOVTableView.frame = frame;
    }];
}
#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != LOVTableView)
    {
        if (LOVTableView.hidden == NO)
        {
            LOVTableView.hidden = YES;
            [ExpenseTypeTextField resignFirstResponder];
            [PaymentTypeTextField resignFirstResponder];
            [_ExpenseCurrencyTextField resignFirstResponder];
        }
    }
}
#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
   // [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
    [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
    
    [ExpenseDateButtonOutlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    self.selectedDate = date;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == NotesTextField)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
    
    
    
}
- (IBAction)attachReceiptBtn:(id)sender {
    
    sem_event=dispatch_semaphore_create(0);
    sem_test=dispatch_semaphore_create(0);
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_578"] message:[helper gettranslation:@"LBL_504"] preferredStyle:UIAlertControllerStyleAlert];
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_632"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                                       {
                                           UIImagePickerController *impicker = [[UIImagePickerController alloc]init];
                                           impicker.delegate = self;
                                           impicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                           impicker.allowsEditing = NO;
                                           [self presentViewController:impicker animated:YES completion:NULL];
                                           dispatch_semaphore_signal(sem_test);
                                           
                                           
                                           
                                       }
                                       
                                       
                                   }]];
        
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_334"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
                                       {
                                           UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
                                           imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                           imagepicker.allowsEditing = YES;
                                           imagepicker.delegate = self;
                                           [self presentViewController:imagepicker animated:YES completion:NULL];
                                           dispatch_semaphore_signal(sem_test);
                                                                                  }
                                       
                                   }]];
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_523"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       rmvReciept=YES;
                                       dispatch_semaphore_signal(sem_test);
                                       
                                    }]];

        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil]];
        
        

        [self presentViewController:takephotoalert animated:YES completion:nil];
        
while (dispatch_semaphore_wait(sem_test , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
        
        if(rmvReciept)
        {
            
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           semp = @"Yes";
                                           rmvReciept=NO;
                                           dispatch_semaphore_signal(sem_event);
                                           
                                       }];
            [alert addAction:okaction];
            
            
            UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                           {
                                               semp = @"No";
                                               rmvReciept=NO;
                                               dispatch_semaphore_signal(sem_event);
                                           }];
            
            [alert addAction:cancelaction];
            [self presentViewController:alert animated:NO completion:nil];
            
            
            while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
            
            if ([semp isEqualToString:@"Yes"])
            {
                StorageURL=@"||BLANK__VALUE||";
                receiptUploadFlag=@"YES";
                _attachReceiptLBL.text=[helper gettranslation:@"LBL_813"];
                //[_attachReceiptBtn setTitle:[helper gettranslation:@"LBL_813"] forState:UIControlStateNormal];
                
            }
        }

        
    }

    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
        [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)resizeimage
{
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 500000;
    
    filedata = UIImageJPEGRepresentation(capturedlicensimg, compression);
    
    while ([filedata length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        filedata = UIImageJPEGRepresentation(capturedlicensimg, compression);
    }
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerOriginalImage];
        receiptUploadFlag=@"YES";
    }
    else if (picker.sourceType ==  UIImagePickerControllerSourceTypeCamera)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"yyyyMMdd"];
        NSString *date = [dateformatter stringFromDate:[NSDate date]];
        [dateformatter setDateFormat:@"HHmmss"];
        NSString *time = [dateformatter stringFromDate:[NSDate date]];
                receiptUploadFlag=@"YES";
        
      //  [_attachReceiptBtn setTitle:[NSString stringWithFormat:@"Image_%@_%@.JPG",date,time] forState:UIControlStateNormal];
        _attachReceiptLBL.text=[NSString stringWithFormat:@"Image_%@_%@.JPG",date,time];
    }
 

    
    [self resizeimage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
        {
            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
            attachedimagename = [imageRep filename];
            // NSLog(@"Attachment image name is %@",attachedimagename);
            PathExtension = [attachedimagename pathExtension];
            //  NSLog(@"Attachment path extension %@",PathExtension);
//            [_attachReceiptBtn setTitle:attachedimagename forState:UIControlStateNormal];
            _attachReceiptLBL.text=attachedimagename;

        }
        
        
    };
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    
}



//if(rmvReciept)
//{
//    
//    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                               {
//                                   semp = @"Yes";
//                                   rmvReciept=NO;
//                                   dispatch_semaphore_signal(sem_event);
//                                   
//                               }];
//    [alert addAction:okaction];
//    
//    
//    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
//                                   {
//                                       semp = @"No";
//                                       rmvReciept=NO;
//                                       dispatch_semaphore_signal(sem_event);
//                                   }];
//    
//    [alert addAction:cancelaction];
//    [self.parentViewController presentViewController:alert animated:NO completion:nil];
//    
//    
//    while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
//    
//    if ([semp isEqualToString:@"Yes"])
//    {
//        StorageURL=@"||BLANK__VALUE||";
//        receiptUploadFlag=@"YES";
//        _attachReceiptLBL.text=[helper gettranslation:@"LBL_813"];
//        //[_attachReceiptBtn setTitle:[helper gettranslation:@"LBL_813"] forState:UIControlStateNormal];
//        
//    }
//}

-(void) upload_attachmentdata:(NSString *) filename base64:(NSString *)base64
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[NSString stringWithFormat:@"%@%@",[helper generate_rowId],attachedimagename] forKey:@"FileName"];
    [dict setValue:base64 forKey:@"Base64"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageContainer" entityname:@"S_Config_Table"] forKey:@"Container"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageAccount" entityname:@"S_Config_Table"] forKey:@"Account"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageKey" entityname:@"S_Config_Table"] forKey:@"Key"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSData *postData = [jsonStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setURL:[NSURL URLWithString:[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]]];
    
    [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
    [request setValue:[keychain stringForKey:@"SLToken"] forHTTPHeaderField:@"X-ZUMO-AUTH"];
    
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn)
    {
        responsedata = [NSMutableData data];
    }
    else
    {
        
        // [helper printloginconsole:@"2" logtext:@"Connection could not be made in upload attachment request"];
        NSLog(@"Connection could not be made in upload attachment request");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responsedata setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responsedata appendData:data];
    
}
// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    dispatch_semaphore_signal(sem_att);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    NSString *jsonArray = [NSJSONSerialization JSONObjectWithData:responsedata options:NSJSONReadingAllowFragments error:&error];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[jsonArray dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    StorageURL = [json valueForKey:@"FileURI"];
    
    dispatch_semaphore_signal(sem_att);
}

- (IBAction)submitBtn:(id)sender {
    
    txType=@"ChangeStatus";
    [self saveExpenseData];
    
    
}


-(IBAction)ApproveExpense:(id)sender
{
    ExpenseStatusType=@"Approve";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_ExpenseHistoryTableView];
    NSIndexPath *indexPath = [_ExpenseHistoryTableView indexPathForRowAtPoint:buttonPosition];
    
    
  rowid=[expenseApprovalArray[indexPath.row] valueForKey:@"id"];
    
    [self performSegueWithIdentifier:@"commentView" sender:self];
    
   // [self changeExpenseApprovalStatus: Comment:Cmnt.text StatusLIC:[helper getLic:@"EXP_APR_STATUS" value:@"Approved"]];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"commentView"])
    {
        if([ExpenseStatusType isEqualToString:@"Approve"])
        {
            CommentView *cmntView=segue.destinationViewController;
            cmntView.ID=rowid;
            cmntView.statuslic=[helper getLic:@"EXP_APR_STATUS" value:@"Approved"];
            cmntView.expenseID=appDelegate.ExpenseID;
            cmntView.txType=ExpenseStatusType;
            cmntView.transaction_type=transaction_type;
            
        }
        else
        {
            CommentView *cmntView=segue.destinationViewController;
            cmntView.ID=rowid;
            cmntView.statuslic=[helper getLic:@"EXP_APR_STATUS" value:@"Rejected"];
            cmntView.expenseID=appDelegate.ExpenseID;
            cmntView.txType=ExpenseStatusType;
            cmntView.transaction_type=transaction_type;
        }
    }
    if([[segue identifier] isEqualToString:@"previewComment"])
    {
        NotesViewController *notes=segue.destinationViewController;
        notes.cmnt=expenseApprovalComment;
        notes.Type=@"ExpenseApprovals";
    }
    
    
    
}
-(IBAction)RejectExpense:(id)sender
{
    
    ExpenseStatusType=@"Reject";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_ExpenseHistoryTableView];
    NSIndexPath *indexPath = [_ExpenseHistoryTableView indexPathForRowAtPoint:buttonPosition];
    
    
    rowid=[expenseApprovalArray[indexPath.row] valueForKey:@"id"];
   
    [self performSegueWithIdentifier:@"commentView" sender:self];
    
  //  [self changeExpenseApprovalStatus:id Comment:Cmnt.text StatusLIC:[helper getLic:@"EXP_APR_STATUS" value:@"Rejected"]];
    
}

-(void)changeExpenseApprovalStatus:(NSString *)ID Comment:(NSString *)Comment StatusLIC:(NSString *)StatusLIC
{
    [helper showWaitCursor:@"LBL_812"];
    
    NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
    
    [PayloadDict setValue:StatusLIC forKey:@"StatusLIC"];
    [PayloadDict setValue:Comment forKey:@"comment"];
    [PayloadDict setValue:appDelegate.ExpenseID forKey:@"expenseID"];
    [PayloadDict setValue:ExpenseID forKey:@"ID"];
    
    NSMutableArray *PayloadArray = [[NSMutableArray alloc]init];
    [PayloadArray addObject:PayloadDict];
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArray options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventExpense" pageno:@"" pagecount:@"" lastpage:@""];
    appDelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Event Expense"  Status:@"Open"];
    
    [helper removeWaitCursor];
    
}
-(IBAction)unwindToAddExpense:(UIStoryboardSegue *)segue
{
    [self setupExpenseView];
    [_ExpenseHistoryTableView reloadData];
    
    
}
@end
