//
//  AddNewGroupViewController.h
//  NN_EMSapp
//
//  Created by Gaurav Kumar on 13/03/18.
//  Copyright © 2018 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewGroupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)saveBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)CancelBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *GroupTableView;
@property (weak, nonatomic) IBOutlet UITextField *groupTextField;
@property (weak, nonatomic) IBOutlet UIButton *addNewGroupBtn;
- (IBAction)addNewGroupBtn:(id)sender;
@end
