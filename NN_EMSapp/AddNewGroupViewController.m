//
//  AddNewGroupViewController.m
//  NN_EMSapp
//
//  Created by Gaurav Kumar on 13/03/18.
//  Copyright © 2018 iWizards. All rights reserved.
//

#import "AddNewGroupViewController.h"
#import "AppDelegate.h"
#import "HelperClass.h"

@interface AddNewGroupViewController ()<UITextFieldDelegate>
{
    NSMutableArray *GroupArray;
    NSMutableArray *FinalGroupArray;
    AppDelegate *appDelegate;
    HelperClass *helper;
    int lastIndex;
    BOOL modified;
    BOOL Deleted;
}

@end

@implementation AddNewGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    GroupArray = [[NSMutableArray alloc]init];
    FinalGroupArray = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    appDelegate.swipevar=@"Group";
    modified = NO;
    Deleted = NO;
    [GroupArray addObjectsFromArray:[[helper query_alldata:@"Event_Group"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appDelegate.eventid]]];
    _GroupTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    _GroupTableView.layer.borderWidth = 1;
   
//    _addNewGroupBtn.alpha = 0.4;
//    _addNewGroupBtn.enabled = NO;
    
    //Translations
    _groupTextField.placeholder=[helper gettranslation:@"LBL_918"];
    [_saveBtn setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    [_cancelBtn setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    _titleLbl.text = [helper gettranslation:@"LBL_918"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return GroupArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[_GroupTableView dequeueReusableCellWithIdentifier:@"groupName"];
    
    UITextField *groupName = (UITextField *)[cell.contentView viewWithTag:1];
    groupName.text = [[GroupArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];

    UIButton *deleteButton = (UIButton*)[cell.contentView viewWithTag:2];
    [deleteButton addTarget:self action:@selector(DeleteGroup:) forControlEvents:UIControlEventTouchUpInside];
    groupName.delegate = self;
    lastIndex = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return  cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveBtn:(id)sender {
    [_groupTextField resignFirstResponder];
    if(modified){
        if(GroupArray.count>0){
            for(int i=0 ; i<=lastIndex;i++){
        
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                UITableViewCell *cell = [_GroupTableView cellForRowAtIndexPath:indexPath];
                UITextField *txtField = (UITextField *)[cell viewWithTag:1];
        
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setValue:txtField.text forKey:@"GroupName"];
        
                NSString *id = [[GroupArray objectAtIndex:indexPath.row] valueForKey:@"groupid"];
                if(id.length>0)
                    [dict setValue:id forKey:@"ID"];
                else
                    [dict setValue:[helper generate_rowId] forKey:@"ID"];
                [dict setValue:appDelegate.eventid forKey:@"EventID"];
                [FinalGroupArray addObject:dict];
            }
            if(FinalGroupArray.count>0){
        
                [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
                NSArray *GrpArr = [FinalGroupArray mutableCopy];
                NSString *jsonString;
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:GrpArr options:0 error:&error];
                if (! jsonData)
                {
                    NSLog(@"Got an error: %@", error);
                }
                else
                {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                }
                NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
                NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
                NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
                NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventGroup" pageno:@"" pagecount:@"" lastpage:@""];
                NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
                if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
                {
                    [helper removeWaitCursor];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
                    [alert show];
                }
                else
                {
                    NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
                    NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                    [helper deletewitheventid:@"Event_Group" value:appDelegate.eventid];
                    [helper InsertGroupDataForEventId:GrpArr sender:@"dsfdsf"];
                    [helper removeWaitCursor];
                    [self performSegueWithIdentifier:@"BackToEventDetail" sender:self];
                    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
                        [self.view removeFromSuperview];
                        [self removeFromParentViewController];
                        }];
                }
            }
        }
    }
    
    if(Deleted)
    {
        [self performSegueWithIdentifier:@"BackToEventDetail" sender:self];
        [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    }
}
- (IBAction)CancelBtn:(id)sender {
    appDelegate.swipevar=@"Group";
    [self performSegueWithIdentifier:@"BackToEventDetail" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
- (IBAction)addNewGroupBtn:(id)sender {
    if([_groupTextField.text length]>0)
    {
        BOOL groupAlreadyPresent = NO;
        for(int i=0;i<GroupArray.count;i++)
        {
           if([_groupTextField.text isEqualToString:[GroupArray[i] valueForKey:@"groupname"]])
           {
               groupAlreadyPresent = YES;
           }
                
        }
        
        if(!groupAlreadyPresent)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setValue:_groupTextField.text forKey:@"groupname"];
            [dict setValue:[helper generate_rowId] forKey:@"groupid"];
            [GroupArray addObject:dict];
            modified = YES;
        }
        else
        {
            [helper showalert:[helper gettranslation:@"LBL_590"]  message:[helper gettranslation:@"LBL_934"] action:[helper gettranslation:@"LBL_462"] ];
        }
    }
    _groupTextField.text=@"";
    [_GroupTableView reloadData];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    textField.borderStyle = UITextBorderStyleNone;
    [helper MoveViewUp:NO Height:230 ViewToBeMoved:self.view];
    textField.layer.shadowRadius = 0.0;
    textField.layer.shadowColor = [[UIColor clearColor] CGColor];
    textField.layer.shadowOffset = CGSizeMake(0.0,0.0);
    textField.layer.shadowOpacity = 100.0;


}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    modified = YES;
    [helper MoveViewUp:YES Height:230 ViewToBeMoved:self.view];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.layer.masksToBounds = false;
    textField.layer.shadowRadius = 3.0;
    textField.layer.shadowColor = [[UIColor blackColor] CGColor];
    textField.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    textField.layer.shadowOpacity = 1.0;
    
    
}

-(void)DeleteGroup:(UIButton *)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_GroupTableView];
    NSIndexPath *indexPath = [_GroupTableView indexPathForRowAtPoint:buttonPosition];
   
    if([helper CheckIfGroupIdCanBeDeleted:[[GroupArray valueForKey:@"groupid"]objectAtIndex:indexPath.row]])
    {
        NSMutableArray *finalArray = [[NSMutableArray alloc]init];
         [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:[[GroupArray valueForKey:@"groupname"]objectAtIndex:indexPath.row] forKey:@"GroupName"];
        [dict setValue:[[GroupArray valueForKey:@"groupid"]objectAtIndex:indexPath.row] forKey:@"ID"];
        [dict setValue:[[GroupArray valueForKey:@"eventid"]objectAtIndex:indexPath.row] forKey:@"EventID"];
        [dict  setValue:[helper stringtobool:@"Y"] forKey:@"CanDelete"];
        [finalArray addObject:dict];
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:finalArray options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventGroup" pageno:@"" pagecount:@"" lastpage:@""];


        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];

        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
    
            [alert show];
        }
        else
        {
            [helper removeWaitCursor];
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            [helper deletewithGroupID:[[GroupArray valueForKey:@"groupid"]objectAtIndex:indexPath.row]];
            [GroupArray removeObjectAtIndex:indexPath.row];
            [self.GroupTableView reloadData];
            Deleted = YES;
            
        }
    }
    else{
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_927"] action:[helper gettranslation:@"LBL_462"]];
    }
}


-(void)RefreshData{
    GroupArray = [[NSMutableArray alloc]init];
     [GroupArray addObjectsFromArray:[[helper query_alldata:@"Event_Group"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appDelegate.eventid]]];
}
@end
