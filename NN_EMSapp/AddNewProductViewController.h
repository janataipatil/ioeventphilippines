//
//  AddNewProductViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 08/11/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"


@interface AddNewProductViewController : UIViewController
- (IBAction)saveBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UILabel *titleLBL;
- (IBAction)cancelBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *selectProductlLBL;
@property (strong, nonatomic) IBOutlet UITextField *selectProduct_tv;
- (IBAction)weightgeSlider:(id)sender;
@property (strong, nonatomic) IBOutlet UISlider *weightageSlider;
@property (strong, nonatomic) IBOutlet UITextField *weightageTextfield;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIView *DropdownTableview;
@property (strong, nonatomic) IBOutlet UITableView *dropdownTblview;
@property (strong, nonatomic) IBOutlet UILabel *Value_LBL;
@property (strong, nonatomic) NSString *senderView;
@end
