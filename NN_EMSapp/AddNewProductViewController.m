//
//  AddNewProductViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 08/11/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddNewProductViewController.h"

@interface AddNewProductViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSArray *productlov;
    float remainingValue;
    NSArray *ProductTypes;
    NSString *rowid;
    float currentTotalValue;
}
@end

@implementation AddNewProductViewController

- (void)viewDidLoad {
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    _weightageSlider.maximumValue = 100.0;
    _weightageSlider.minimumValue = 0.0;
    

        ProductTypes = [[helper query_alldata:@"Product"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.itnearyid]];

        NSPredicate *LOVPredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"PRODUCT"];
 
        productlov = [helper predicateSearch:@"Event_LOV" name:@" " predicate:LOVPredicate];
   
    
        NSArray *EventProduct = [[helper query_alldata:@"Product"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eid like %@",appdelegate.eventid]];
    
            if(EventProduct.count>0)
            {
                for(int i=0;i<EventProduct.count;i++)
                {
                    productlov = [productlov filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT(lic like %@)",[EventProduct[i] valueForKey:@"lic"]]];
                    if(![[EventProduct [i] valueForKey:@"id"]isEqualToString:appdelegate.itnearyid])
                    {
                        remainingValue = remainingValue + [[EventProduct[i] valueForKey:@"weight"] integerValue];
                    }
                }
                currentTotalValue = remainingValue ;
            
                remainingValue = 100 - remainingValue;
            
                _weightageSlider.value = remainingValue;
                _weightageTextfield.text =[NSString stringWithFormat:@"%d",(int)floor(remainingValue)];
            }
            else
            {
                remainingValue = 0.0;
                _weightageSlider.value = 100;
                _weightageTextfield.text =@"100";
            }
    
    
    
    
            appdelegate.swipevar =@"Product";
    
            [self.DropdownTableview setHidden:YES];
            [self.dropdownTblview setHidden:YES];
    
    
            if([ProductTypes count]>0)
            {
                _weightageTextfield.text = [NSString stringWithFormat:@"%d",[[ProductTypes[0] valueForKey:@"weight"] integerValue]];
            
                NSString *SelectedProduct = [ProductTypes[0] valueForKey:@"product"];
            
                _selectProduct_tv.text =SelectedProduct;
        
                float val = [[ProductTypes[0] valueForKey:@"weight"]floatValue];
        
                [self.weightageSlider setValue:val animated:YES];
        
                rowid = [ProductTypes[0] valueForKey:@"id"];
            }
            else
            {
                rowid = [helper generate_rowId];
       
                // _weightageSlider.value = 100;
            }
    

  
        [self setupTranslation];
   
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupTranslation
{
    if(ProductTypes.count>0)
    {
        _titleLBL.text = [helper gettranslation:@"LBL_897"];
    }
    else
    {
        _titleLBL.text = [helper gettranslation:@"LBL_896"];
    }
    _selectProductlLBL.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_204"]];
    _Value_LBL.text = [helper gettranslation:@"LBL_895"];
    [helper markasrequired:_selectProductlLBL];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productlov count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *cellIdentifier;
    cellIdentifier = @"ProductTypes";
    
    UITableViewCell *customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIView *contentView = customcell.contentView;
    
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
    
    
    lovtypename.text = [productlov[indexPath.row] valueForKey:@"value"];
    return customcell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
     _selectProduct_tv.text = [productlov[indexPath.row] valueForKey:@"value"];
    [_selectProduct_tv resignFirstResponder];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveBtn:(id)sender
{
    @autoreleasepool
    {
        [_selectProduct_tv resignFirstResponder];
    
        NSString *productlic = [helper getLic:@"PRODUCT" value:_selectProduct_tv.text];
    
        float number = [_weightageSlider value];
        int value = (int)floor(number);
    
        if(currentTotalValue + value <=100)
        {
            if([_weightageTextfield.text integerValue]>=0 && [_selectProduct_tv.text length]>0)
            {
                [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
                NSMutableArray *ReqArray =[[NSMutableArray alloc]init];
    
                NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
   
                [JsonDict setValue:rowid forKey:@"ID"];
                [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
                [JsonDict setValue:productlic forKey:@"ProductLIC"];
                [JsonDict setValue:_selectProduct_tv.text forKey:@"Product"];
                [JsonDict setValue:[NSString stringWithFormat:@"%d",value] forKey:@"Weightage"];
                [JsonDict setValue:[helper stringtobool:@"N"] forKey:@"Deleted"];
    
                [ReqArray addObject:JsonDict];
    
                NSString *jsonString;
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ReqArray options:0 error:&error];
                if (! jsonData)
                {
                    NSLog(@"Got an error: %@", error);
                }
                else
                {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                }
                NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
                NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
                NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
                NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventProduct" pageno:@"" pagecount:@"" lastpage:@""];
    
                NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx"  HTTPMethod:@"POST" parameters:nil headers:nil];
     
                if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_590"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
     
                    [alert show];
                }
                else
                {
                    NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
                    NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                    NSLog(@"Json data is %@",jsonData);
         
                    if([appdelegate.itnearyid  length]>0)
                    {
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",appdelegate.itnearyid];
                        [helper delete_predicate:@"Product" predicate:predicate];
             
                        [helper insertEventProduct:ReqArray];
             
                    }
                    else
                        [helper insertEventProduct:ReqArray];
                }
    
                [helper removeWaitCursor];
                [self RemovePopup];
            }
            else
            {
                if([_selectProduct_tv.text length]>0)
                    [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_899"] action:[helper gettranslation:@"LBL_462"]];
                else
                    [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
            }
        }
        else
        {
            [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_899"] action:[helper gettranslation:@"LBL_462"]];
        }
    }
}
- (IBAction)cancelBtn:(id)sender {
    [self RemovePopup];
}
- (IBAction)weightgeSlider:(id)sender {
    int SliderValue = [_weightageSlider value];
    _weightageTextfield.text = [NSString stringWithFormat:@"%d",SliderValue];
}
-(void)RemovePopup
{
    [self performSegueWithIdentifier:@"BackToEventDetail" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    _dropdownTblview.hidden = YES;
    [_selectProduct_tv resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _selectProduct_tv)
    {
        [helper MoveViewUp:YES Height:180 ViewToBeMoved:self.view];
        
        [self.DropdownTableview setHidden:NO];
        [self.dropdownTblview setHidden:NO];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == _selectProduct_tv)
    {
        [helper MoveViewUp:NO Height:180 ViewToBeMoved:self.view];
        if (![helper isdropdowntextCorrect:_selectProduct_tv.text lovtype:@"PRODUCT"])
        {
            _selectProduct_tv.text = @"";
        }
        
        [self.DropdownTableview setHidden:YES];
        [self.dropdownTblview setHidden:YES];
    }
    else if (textField == _weightageTextfield)
    {
        [_weightageSlider setValue:[_weightageTextfield.text integerValue] animated:NO];
    }
    [textField resignFirstResponder];
}


@end
