//
//  AddSession.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/31/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SOCustomCell.h"
#import "SLKeyChainStore.h"
#import "HSDatePickerViewController.h"

@interface AddSession : UIViewController<UITextFieldDelegate>

{
    
    IBOutlet UITableView *LOVTableview;
    //IBOutlet UITableView *salutation_tableview;
}
@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic)NSString *attendeeid;


@property (strong, nonatomic) IBOutlet UITextField *title_tv;
@property (strong, nonatomic) IBOutlet UITextField *sessiontype_tv;
@property (strong, nonatomic) IBOutlet UITextField *starttime_tv;
@property (strong, nonatomic) IBOutlet UITextField *endtime_tv;
@property (strong, nonatomic) IBOutlet UITextField *location_tv;
@property (strong, nonatomic) IBOutlet UITextField *description_tv;
@property (weak, nonatomic) IBOutlet UILabel *group_lbl;
@property (weak, nonatomic) IBOutlet UITextField *group_tv;


@property(strong,nonatomic)NSString *senderview;
@property(strong,nonatomic)NSString *cancelsegue;
@property (strong, nonatomic) IBOutlet UILabel *viewtitle_lbl;

- (IBAction)save_btn:(id)sender;
- (IBAction)cancel_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *dropdown_view;
@property (strong, nonatomic) IBOutlet UILabel *dropdown_lbl;


@property (strong, nonatomic) IBOutlet UILabel *title_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *description_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *location_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *starttime_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *endtime_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *sessiontype_ubl;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;

@end
