//
//  AddSession.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/31/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddSession.h"

@interface AddSession ()<HSDatePickerViewControllerDelegate>
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSArray *Eventsessionlist;
    NSArray *Eventlovlist;
    SLKeyChainStore *keychain;
    NSString *generatedrowid;
    NSArray *LOVArr;
    NSString *transaction_type;
    NSDateFormatter *dateFormatter;
    NSString *TextfieldFlag;
    NSUInteger count;
    HSDatePickerViewController *hsdpvc;
    NSDate *eventstartdate;
    NSDate *eventenddate;
    NSString *CanEdit;
    NSString *CanDelete;
    NSArray *groupArray;
    NSString *startEndDate;
    NSUInteger stringlength;
    dispatch_semaphore_t sem_session;
    NSString *selectedGroupID;
}
@property (nonatomic, strong) NSDate *selectedDate;

@end

@implementation AddSession
@synthesize helper,senderview,cancelsegue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
   keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    _dropdown_view.hidden = YES;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    NSArray *EventData = [[helper query_alldata:@"Event"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if ([EventData count]>0)
    {
        eventstartdate = [dateFormatter dateFromString:[EventData[0] valueForKey:@"startdate"]];
        eventenddate = [dateFormatter dateFromString:[EventData[0] valueForKey:@"enddate"]];
       
        groupArray = [[helper query_alldata:@"Event_Group"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
        NSString *maxdate = [EventData[0] valueForKey:@"enddate"];
        hsdpvc.maxDate = [dateFormatter dateFromString:maxdate];
    
        hsdpvc.minDate = [dateFormatter dateFromString:[EventData[0] valueForKey:@"startdate"]];
        hsdpvc.date = eventstartdate;
    }
    
    dateFormatter = [[NSDateFormatter alloc] init];
    if (appdelegate.sessionid.length >0)
    {
        senderview = @"Editsession";
    }
    else
    {
        senderview = @"Addsession";
    }
    
    
    if([senderview isEqualToString:@"Addsession"])
    {
        generatedrowid = [helper generate_rowId];
        transaction_type = @"Add New Session";
        _viewtitle_lbl.text = [helper gettranslation:@"LBL_025"];
        CanEdit = @"Y";
        CanDelete = @"Y";
    }
    else if([senderview isEqualToString:@"Editsession"])
    {
        [self setupsessionview];
        generatedrowid = appdelegate.sessionid;
        transaction_type = @"Edit Session";
        _viewtitle_lbl.text =[helper gettranslation:@"LBL_113"];
    }
    
    // Do any additional setup after loading the view.
    LOVTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableview.layer.borderWidth = 1;
    
   // [_title_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_sessiontype_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
   // [_starttime_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //[_location_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
   // [_endtime_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //[_description_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self langsetuptranslations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)langsetuptranslations
{
    
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    _sessiontype_tv.placeholder = [helper gettranslation:@"LBL_757"];
    
    _title_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_261"]] ;
    [self markasrequired:_title_ulbl];
    
    _sessiontype_ubl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_230"]] ;
    [self markasrequired:_sessiontype_ubl];
    
    _description_ulbl.text = [helper gettranslation:@"LBL_094"] ;
    _location_ulbl.text = [helper gettranslation:@"LBL_161"] ;
    
    _starttime_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_240"]] ;
    [self markasrequired:_starttime_ulbl];
    
    _endtime_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_123"]] ;
    [self markasrequired:_endtime_ulbl];
    
    _group_lbl.text =[helper gettranslation:@"LBL_923"];
    
    
}

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}


-(void)setupsessionview
{
    Eventsessionlist = [helper query_alldata:@"Event_Session"];
    Eventsessionlist = [Eventsessionlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.sessionid]];
    if ([Eventsessionlist count]>0)
    {
        _title_tv.text = [Eventsessionlist[0] valueForKey:@"title"];
        _sessiontype_tv.text = [helper getvaluefromlic:@"SSN_TYPE" lic:[Eventsessionlist[0] valueForKey:@"typelic"]];
        _location_tv.text = [Eventsessionlist[0] valueForKey:@"location"];
        _description_tv.text = [Eventsessionlist[0] valueForKey:@"session_desc"];
        _group_tv.text = [helper getGroupNameforGroupID:[Eventsessionlist[0] valueForKey:@"groupID"]];
        selectedGroupID = [Eventsessionlist[0] valueForKey:@"groupID"];
        _starttime_tv.text = [helper formatingdate:[Eventsessionlist[0] valueForKey:@"startdate"] datetime:@"FormatDate"];
        _endtime_tv.text = [helper formatingdate:[Eventsessionlist[0] valueForKey:@"enddate"] datetime:@"FormatDate"];
        CanEdit = [Eventsessionlist[0] valueForKey:@"canEdit"];
        CanDelete = [Eventsessionlist[0] valueForKey:@"canDelete"];
    }

}


- (IBAction)save_btn:(id)sender
{
    sem_session = dispatch_semaphore_create(0);
    if ((_title_tv.text.length > 0)&&(_sessiontype_tv.text.length > 0)&&(_starttime_tv.text.length > 0)&&(_endtime_tv.text.length > 0))
    {
        
        if([senderview isEqualToString:@"Addsession"])
        {
            [self savedata:Eventsessionlist];
        }
        else if([senderview isEqualToString:@"Editsession"])
        {
            
           /* NSDate *sdate = [dateFormatter dateFromString:_starttime_tv.text];
            NSDate *edate = [dateFormatter dateFromString:_endtime_tv.text];
            
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];*/
            
            
            
            
            //edit records
            //            && [_eventloc_tv.text isEqualToString:[eventvenue[0] valueForKey:@"location"]?: @""]
            if ([_title_tv.text isEqualToString:[Eventsessionlist[0] valueForKey:@"title"]?: @""] && [selectedGroupID isEqualToString:[Eventsessionlist[0] valueForKey:@"groupID"]?: @""]  && [_sessiontype_tv.text isEqualToString:[Eventsessionlist[0] valueForKey:@"typelic"]?: @""] && [_location_tv.text isEqualToString:[Eventsessionlist[0] valueForKey:@"location"]?: @""] && [_description_tv.text isEqualToString:[Eventsessionlist[0] valueForKey:@"session_desc"]?: @""]   && [[helper converingformateddatetooriginaldate:_starttime_tv.text datetype:@"ServerDate"] isEqualToString:[Eventsessionlist[0] valueForKey:@"startdate"]?: @""] && [[helper converingformateddatetooriginaldate:_endtime_tv.text datetype:@"ServerDate"] isEqualToString:[Eventsessionlist[0] valueForKey:@"enddate"]?: @""] )
            {
                
                [self removepopup];
            }
            else
            {
                [self savedata:Eventsessionlist];
            }
        }
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
    }
}

-(void)savedata:(NSArray *) Eventsessiondata
{
    
    NSError *error;

    [helper showWaitCursor:[helper gettranslation:@"LBL_613"]];
    sem_session = dispatch_semaphore_create(0);
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:generatedrowid forKey:@"ID"];
    [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
    [JsonDict setValue:_title_tv.text forKey:@"Title"];
    [JsonDict setValue:_description_tv.text forKey:@"Description"];
    [JsonDict setValue:_location_tv.text forKey:@"Location"];
    [JsonDict setValue:selectedGroupID forKey:@"GroupID"];
    [JsonDict setValue:[helper converingformateddatetooriginaldate:_starttime_tv.text datetype:@"ServerDate"] forKey:@"StartDate"];
    [JsonDict setValue:[helper converingformateddatetooriginaldate:_endtime_tv.text datetype:@"ServerDate"] forKey:@"EndDate"];
    [JsonDict setValue:[helper getLic:@"SSN_TYPE" value:_sessiontype_tv.text] forKey:@"TypeLIC"];
    [JsonDict setValue:[helper stringtobool:CanEdit] forKey:@"CanEdit"];
    [JsonDict setValue:[helper stringtobool:CanDelete] forKey:@"CanDelete"];
    
    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArray options:0 error:&error];
    if (! JsonArray)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventSession" pageno:@"" pagecount:@"" lastpage:@""];
    
    appdelegate.senttransaction = @"Y";
    
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Add Event Sessions"  Status:@"Open"];
    
    if ([senderview isEqualToString:@"Editsession"])
    {
        [helper delete_predicate:@"Event_Session" predicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.sessionid]];
    }
    
    [helper insertEventSession:appdelegate.eventid SessionArr:JsonArray];
    dispatch_semaphore_signal(sem_session);
    
    [helper removeWaitCursor];
    
    while (dispatch_semaphore_wait(sem_session, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    [self removepopup];
    
    
}
- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}

-(void)removepopup
{
    
    [self performSegueWithIdentifier:@"unwindtoeventschedule" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished)
    {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=====-=-=-=-=-=-=-=-=-=-=-=-=-==-==-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=÷÷


#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _dropdown_view.hidden = YES;
    
    if (textField == _title_tv)
    {
        stringlength = 50;
    }
    else if (textField == _location_tv)
    {
        stringlength = 50;
    }
    else if (textField == _sessiontype_tv)
    {
        TextfieldFlag = @"STypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SSN_TYPE"]];
            [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
        _dropdown_lbl.text =[helper gettranslation:@"LBL_230"];
        [LOVTableview reloadData];
        [self adjustHeightOfTableview:LOVTableview];
        LOVTableview.hidden = NO;
        _dropdown_view.hidden = NO;
    }
    else if (textField == _group_tv)
    {
        TextfieldFlag = @"groupTextField";
        [_group_tv resignFirstResponder];
        LOVArr = [[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
        _dropdown_lbl.text =[helper gettranslation:@"LBL_923"];
        [LOVTableview reloadData];
        [self adjustHeightOfTableview:LOVTableview];
        LOVTableview.hidden = NO;
        _dropdown_view.hidden = NO;
    }

    else if (textField == _starttime_tv) {}
    else if (textField == _endtime_tv) {}
    else if (textField == _description_tv)
    {
        stringlength = 100;
    }

    
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
        if (textField == _sessiontype_tv)
        {
            [helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
            if(![helper isdropdowntextCorrect:_sessiontype_tv.text lovtype:@"SSN_TYPE"])
            {
                _sessiontype_tv.text = @"";
            }
        }
    if (textField == _group_tv)
    {
        [helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == _sessiontype_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SSN_TYPE"]];
        }
    }
    else
    {
        if (theTextField == _sessiontype_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SSN_TYPE"]];
            NSMutableArray *filteredArray = [[NSMutableArray alloc] init];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
            filteredArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
            LOVArr = [filteredArray copy];
        }

    }
    [LOVTableview reloadData];
    
    
}


#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == LOVTableview)
    {
        CGFloat height = LOVTableview.contentSize.height;
        CGFloat maxHeight = LOVTableview.superview.frame.size.height - LOVTableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableview.frame;
            
            if([TextfieldFlag isEqualToString:@"STypeTextField"])
            {
                frame.origin.x = _sessiontype_tv.frame.origin.x;
                frame.origin.y = _sessiontype_tv.frame.origin.y +30;
                
            }
            if([TextfieldFlag isEqualToString:@"groupTextField"])
            {
                frame.origin.x = 3;
                frame.origin.y = 30;
                
            }
            _dropdown_view.frame= frame;
            
        }];
 
        
    }
}
#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([TextfieldFlag isEqualToString:@"groupTextField"])
    {
        return  [groupArray count];
    }
    else
        return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [LOVTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
    //    CGRect lovtypenameframe = lovtypename.frame;
    //    CGRect sepratorviewframe = sepratorview.frame;
    //
    //    lovtypename.frame = lovtypenameframe;
    //    sepratorview.frame = sepratorviewframe;
    
    if([TextfieldFlag isEqualToString:@"STypeTextField"])
    {
        lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    }
    else if([TextfieldFlag isEqualToString:@"groupTextField"])
    {
        lovtypename.text = [groupArray[indexPath.row] valueForKey:@"groupname"];
    }

    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if([TextfieldFlag isEqualToString:@"STypeTextField"])
    {
        _sessiontype_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_sessiontype_tv resignFirstResponder];
        
    }
    if([TextfieldFlag isEqualToString:@"groupTextField"])
    {
        _group_tv.text = [groupArray[indexPath.row] valueForKey:@"groupname"];
        selectedGroupID = [groupArray[indexPath.row] valueForKey:@"groupid"];
    }
    LOVTableview.hidden = YES;
    _dropdown_view.hidden = YES;
}
#pragma mark touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != LOVTableview)
    {
        LOVTableview.hidden = YES;
        _dropdown_view.hidden = YES;
        [_sessiontype_tv resignFirstResponder];
    }
}
- (IBAction)startdate_btn:(id)sender
{
    startEndDate = @"StartDate";
    
    hsdpvc=[[HSDatePickerViewController alloc]init];
    hsdpvc.delegate = self;
    
    hsdpvc.date = eventstartdate;
    hsdpvc.minDate = eventstartdate;
    hsdpvc.maxDate = eventenddate;
    

    [self presentViewController:hsdpvc animated:YES completion:nil];
    
}

- (IBAction)enddate_btn:(id)sender
{
    
    startEndDate = @"EndDate";
    startEndDate = @"EndDate";
    
    hsdpvc=[[HSDatePickerViewController alloc]init];
    NSString *sessionStartDate = _starttime_tv.text;
    hsdpvc.delegate = self;
    
    if(sessionStartDate.length>0)
    {
        NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
        [dateformat setTimeZone:[NSTimeZone systemTimeZone]];
        [dateformat setDateFormat:@"dd MMM yyyy HH:mm"];
        
        NSDate *actStartDate=[dateformat dateFromString:sessionStartDate];
        hsdpvc.date = actStartDate;
        
        hsdpvc.minDate = actStartDate;
        hsdpvc.maxDate = eventenddate;
        
    }
    else
    {
        hsdpvc.minDate=[NSDate date];
    }
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
    
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
    if ([startEndDate isEqualToString:@"StartDate"])
    {
        [_starttime_tv setText:[dateFormatter1 stringFromDate:date]];//[helper FormatCalendarDate:date DateTime:@"Time"]];
    }
    else if ([startEndDate isEqualToString:@"EndDate"])
    {
        [_endtime_tv setText:[dateFormatter1 stringFromDate:date]];//[helper FormatCalendarDate:date DateTime:@"Time"]];
    }
    self.selectedDate = date;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == _title_tv || textField == _location_tv  || textField == _description_tv)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}
@end
