//
//  AddTemplateViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 31/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"

@interface AddTemplateViewController : UIViewController

- (IBAction)SaveButtonAction:(id)sender;
- (IBAction)CancelButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *TemplateTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *TemplateSearchBarOutlet;
@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UILabel *tittleheader;

@property (strong, nonatomic) IBOutlet UILabel *templatename_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *templatedesc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *templatetype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *templatelstsync_ulbl;


@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;

@end
