//
//  AddTemplateViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 31/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddTemplateViewController.h"

@interface AddTemplateViewController ()
{
    NSMutableArray *TemplateArr;
    NSMutableArray *SelectedTemplateArr;
    NSMutableArray *CopyofTemplateArr;
    NSUInteger count;
    AppDelegate *appdelegate;
}

@end

@implementation AddTemplateViewController
@synthesize helper,TemplateTableView,TemplateSearchBarOutlet;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    TemplateArr = [NSMutableArray arrayWithArray:[helper query_alldata:@"Template"]];
    SelectedTemplateArr = [[NSMutableArray alloc]init];
    [TemplateSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [TemplateSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    
    //for checkbox
    [TemplateTableView setEditing:YES animated:YES];
    NSArray *EventTemplateArr = [[helper query_alldata:@"Event_Mail"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    for (int i = 0; i < [EventTemplateArr count]; i++)
    {
        NSString *TemplateID = [EventTemplateArr[i] valueForKey:@"templateID"];
        for (int j= 0; j<[TemplateArr count]; j++)
        {
            NSString *MasterTemplateID = [TemplateArr[j] valueForKey:@"templateID"];
            if ([MasterTemplateID isEqualToString:TemplateID])
            {
                [TemplateArr removeObjectAtIndex:j];
                j--;
            }
            
        }
    }
    //keep original copy for search
    CopyofTemplateArr = [TemplateArr mutableCopy];
    [self langsetuptranslations];

}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _tittleheader.text = [helper gettranslation:@"LBL_028"];
    _templatename_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_258"]];
    _templatedesc_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_094"]];
    _templatetype_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_266"]];
    _templatelstsync_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_156"]];

}

-(void) markasrequired:(UILabel *) label
{
    int labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [TemplateArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *TemplateNamelbl = (UILabel *)[contentView viewWithTag:1];
    UILabel *TemplateDescriptionlbl = (UILabel *)[contentView viewWithTag:2];
    UILabel *Typelbl = (UILabel *)[contentView viewWithTag:3];
    UILabel *LastSentlbl = (UILabel *)[contentView viewWithTag:4];
    
    TemplateNamelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateName"];
    TemplateDescriptionlbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateDesc"];
    LastSentlbl.text = [helper formatingdate:[TemplateArr[indexPath.row] valueForKey:@"lastSentDate"] datetime:@"date"];
    Typelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateType"];
    //customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    customcell.tintColor = [helper DarkBlueColour];
    return customcell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
- (IBAction)SaveButtonAction:(id)sender
{
    if([SelectedTemplateArr count]<= 0)
    {
        [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_620"] withDuaration:1.0];
    }
    else
    {
        NSMutableArray *PayloadArr = [[NSMutableArray alloc]init];
        for (int i=0;i<[SelectedTemplateArr count]; i++)
        {
            NSArray *TemplateDetailArr = [TemplateArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"templateID like %@",[SelectedTemplateArr objectAtIndex:i]]];
            NSMutableDictionary *ServerDict = [[NSMutableDictionary alloc]init];
            if ([TemplateDetailArr count]>0)
            {
                NSString *generatedRowId = [helper generate_rowId];
                [ServerDict setValue:generatedRowId forKey:@"ID"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateID"] forKey:@"TemplateID"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateName"] forKey:@"TemplateName"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateDesc"] forKey:@"Comment"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateDesc"] forKey:@"Description"];
                [ServerDict setValue:appdelegate.eventid forKey:@"EventID"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateType"] forKey:@"TemplateType"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templatecategory"] forKey:@"Category"];
                [ServerDict setValue:[helper getLic:@"TEMP_CAT" value:[TemplateDetailArr[0] valueForKey:@"templatecategory"]] forKey:@"CategoryLIC"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templatesubcategory"] forKey:@"SubCategory"];
                
            }
            [PayloadArr addObject:ServerDict];
        }
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArr options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AesPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventMail" pageno:@"" pagecount:@"" lastpage:@""];
        
        /* NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
         
         if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
         {
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
         
         [alert show];
         }
         else
         {
             NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
             NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
             NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
             if (jsonData)
             {
                 //[helper insertEventTeamMember:appdelegate.eventid TeamArr:DatabaseArr];
                 [self CancelButtonAction:self];
             }
         }*/
        
        appdelegate.senttransaction = @"Y";
        [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Add Template" entityname:@"AddTemplate" Status:@"Open"];
        [helper insertEventEmail:PayloadArr];
        [self CancelButtonAction:self];
        

    }
}

- (IBAction)CancelButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromAddToEventTemplate" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SelectedTemplateArr addObject:[TemplateArr[indexPath.row] valueForKey:@"templateID"]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[SelectedTemplateArr count]; i++)
    {
        NSString *item = [SelectedTemplateArr objectAtIndex:i];
        if ([item isEqualToString:[TemplateArr[indexPath.row] valueForKey:@"templateID"]])
        {
            [SelectedTemplateArr removeObject:item];
            i--;
        }
    }
}

#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if(searchBar == TemplateSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == TemplateSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == TemplateSearchBarOutlet)
    {
        TemplateArr = [CopyofTemplateArr mutableCopy];
        if(searchText.length == 0)
        {
           [searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0.1];
        }
        else
        {
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.templateName contains[c] %@) OR (SELF.templateDesc contains[c] %@) OR (SELF.templateType contains[c] %@) ",searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[TemplateArr filteredArrayUsingPredicate:predicate]];
            
            TemplateArr = [filtereventarr copy];
        }
       [TemplateTableView reloadData];
       NSMutableArray *TemplateidArr = [[NSMutableArray alloc]init];
        for (int i =0; i<[TemplateArr count]; i++)
        {
            NSString *Templateid = [TemplateArr[i] valueForKey:@"templateID"];
            [TemplateidArr addObject:Templateid];
        }
        
        for (int i =0; i<[SelectedTemplateArr count]; i++)
        {
            NSString *TemplateId = [SelectedTemplateArr objectAtIndex:i];
            if ([TemplateidArr containsObject:TemplateId])
            {
                NSUInteger index = [TemplateidArr indexOfObject:TemplateId];
                NSInteger Templatecount = [TemplateidArr count];
                if ((index > 0) || ((index == 0) && (index< Templatecount)) )
                {
                    [TemplateTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }//for loop end

    }
}


@end
