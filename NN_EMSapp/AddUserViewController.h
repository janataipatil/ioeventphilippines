//
//  AddUserViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 29/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"

@interface AddUserViewController : UIViewController

- (IBAction)SaveButtonAction:(id)sender;
- (IBAction)CancelButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UISearchBar *UserSearchBarOutlet;
@property (strong, nonatomic) IBOutlet UITableView *UserTableView;
@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UILabel *firstname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *lastname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *userid_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *usertype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;

@property (strong, nonatomic) IBOutlet UISegmentedControl *usrPositionSegmentControl;

- (IBAction)usrPositionSegmentControl:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *headertitle_ulbl;


@end
