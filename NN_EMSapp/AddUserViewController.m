//
//  AddUserViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 29/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddUserViewController.h"

@interface AddUserViewController ()
{
    NSMutableArray *UserArr;
    NSMutableArray *PositionArr;
    NSMutableArray *CopyofUserArr;
    NSMutableArray *SelectedUserArr;
    AppDelegate *appdelegate;
    NSInteger count;
    NSPredicate *eventpredicate;
    NSArray *EventTeamArr;
    
    NSString *SelectionType;
    
    
}

@end

@implementation AddUserViewController
@synthesize UserSearchBarOutlet,UserTableView,helper;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];

    [UserSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [UserSearchBarOutlet valueForKey:@"_searchField"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
    UserArr=[[NSMutableArray alloc]init];
    PositionArr=[[NSMutableArray alloc]init];
    
    UserArr = [[helper query_alldata:@"User"]mutableCopy];
    PositionArr=[[helper query_alldata:@"Position_List"]mutableCopy];
    
    eventpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    
    SelectedUserArr = [[NSMutableArray alloc]init];
    
    SelectionType=@"User";
    //for checkbox
    [UserTableView setEditing:YES animated:YES];
    
    @autoreleasepool {
        
   EventTeamArr = [[helper query_alldata:@"Event_Team"] filteredArrayUsingPredicate:eventpredicate];
    
    for (int i = 0; i < [EventTeamArr count]; i++)
    {
        NSString *userid = [EventTeamArr[i] valueForKey:@"teamid"];
        for (int j= 0; j<[UserArr count]; j++)
        {
            NSString *Masteruserid = [UserArr[j] valueForKey:@"userid"];
            if ([Masteruserid isEqualToString:userid])
            {
                [UserArr removeObjectAtIndex:j];
                j--;
            }
            
        }
    }
    //keep original copy for search
    CopyofUserArr = [UserArr mutableCopy];

    [self languagesetuptranslations];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)languagesetuptranslations
{

        
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    if([SelectionType isEqualToString:@"User"])
    {
        _firstname_ulbl.text = [helper gettranslation:@"LBL_141"];
        _lastname_ulbl.text = [helper gettranslation:@"LBL_153"];
        _userid_ulbl.text = [helper gettranslation:@"LBL_267"];
        _usertype_ulbl.text = [helper gettranslation:@"LBL_271"];
        _headertitle_ulbl.text = [helper gettranslation:@"LBL_029"];
        _status_ulbl.text = [helper gettranslation:@"LBL_242"];
    }
    else
    {
        _firstname_ulbl.text = [helper gettranslation:@"LBL_827"];
        _lastname_ulbl.text = [helper gettranslation:@"LBL_828"];
        _userid_ulbl.text = [helper gettranslation:@"LBL_467"];
        
        _usertype_ulbl.text = [helper gettranslation:@"LBL_148"];
        
        _headertitle_ulbl.text = [helper gettranslation:@"LBL_029"];
        _status_ulbl.text = [helper gettranslation:@"LBL_242"];

    }
    
    
}


- (IBAction)SaveButtonAction:(id)sender
{
    if([SelectedUserArr count]<= 0)
    {
       [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_618"] withDuaration:1.0];
    }
    else
    {
     
        NSMutableArray *PayloadArr = [[NSMutableArray alloc]init];
        NSMutableArray *DatabaseArr = [[NSMutableArray alloc]init];
        
        
        for (int i=0;i<[SelectedUserArr count]; i++)
        {
            NSMutableDictionary *ServerDict = [[NSMutableDictionary alloc]init];
            NSMutableDictionary *DatabaseDict = [[NSMutableDictionary alloc]init];

            if([[SelectedUserArr[i] valueForKey:@"Type"]isEqualToString:@"User"])
            {
                
                NSArray *UserDetailArr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[SelectedUserArr[i] valueForKey:@"ID"]]];

                if ([UserDetailArr count]>0)
                {
                    NSString *generatedRowId = [helper generate_rowId];
                    [ServerDict setValue:appdelegate.eventid forKey:@"EventID"];
                    [ServerDict setValue:[UserDetailArr[0] valueForKey:@"statuslic"] forKey:@"StatusLIC"];
                    [ServerDict setValue:[UserDetailArr[0] valueForKey:@"userid"] forKey:@"TeamID"];
                    [ServerDict setValue:[helper getLic:@"APPROVER_TYPE" value:@"User"] forKey:@"TypeLIC"];
                    [ServerDict setValue:generatedRowId forKey:@"ID"];
               
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"userid"] forKey:@"TeamID"];
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"userid"] forKey:@"Username"];
                    
                    [DatabaseDict setValue:[helper getLic:@"APPROVER_TYPE" value:@"User"] forKey:@"TypeLIC"];
                    [DatabaseDict setValue:[helper getvaluefromlic:@"APPROVER_TYPE" lic:@"User"] forKey:@"Type"];
                   
                    [DatabaseDict setValue:generatedRowId forKey:@"ID"];
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"status"] forKey:@"Status"];
                    
                    [DatabaseDict setValue:[NSString stringWithFormat:@"%@ %@",[UserDetailArr[0] valueForKey:@"firstname"],[UserDetailArr[0] valueForKey:@"lastname"]]forKey:@"TeamName"];
                    

                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"eMSPosition"] forKey:@"EMSPosition"];
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"eMSPositionLIC"] forKey:@"EMSPositionLIC"];
                
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"statuslic"] forKey:@"StatusLIC"];
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"emailaddress"] forKey:@"Email"];
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"emsuserrolelic"] forKey:@"RoleLIC"];
                    [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"emsuserrole"] forKey:@"Role"];
                    
                    [DatabaseDict setValue:[helper stringtobool:@"Y"] forKey:@"CanDelete"];
                    [DatabaseDict setValue:[helper stringtobool:@"Y"] forKey:@"CanEdit"];

                }
                //[DatabaseArr addObject:DatabaseDict];
                //[PayloadArr addObject:ServerDict];
            }
            else
            {
                    //NSMutableDictionary *ServerDict = [[NSMutableDictionary alloc]init];
                    //NSMutableDictionary *DatabaseDict = [[NSMutableDictionary alloc]init];
                    
                    NSArray *UserDetailArr = [[helper query_alldata:@"Position_List"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"positionID CONTAINS[c] %@",[SelectedUserArr[i] valueForKey:@"ID"]]];
                    
                    if ([UserDetailArr count]>0)
                    {
                        NSString *generatedRowId = [helper generate_rowId];
                        [ServerDict setValue:appdelegate.eventid forKey:@"EventID"];
                        [ServerDict setValue:[UserDetailArr[0] valueForKey:@"statuslic"] forKey:@"StatusLIC"];
                        [ServerDict setValue:[UserDetailArr[0] valueForKey:@"positionID"] forKey:@"TeamID"];
                        [ServerDict setValue:[helper getLic:@"APPROVER_TYPE" value:@"Position"] forKey:@"TypeLIC"];
                        [ServerDict setValue:generatedRowId forKey:@"ID"];
                        
                        [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"positionID"] forKey:@"TeamID"];
                        [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"positionName"] forKey:@"TeamName"];
                        [DatabaseDict setValue:generatedRowId forKey:@"ID"];


                        [DatabaseDict setValue:[helper getLic:@"APPROVER_TYPE" value:@"Position"] forKey:@"TypeLIC"];
                        [DatabaseDict setValue:[helper getvaluefromlic:@"APPROVER_TYPE" lic:@"Position"] forKey:@"Type"];
                        [DatabaseDict setValue:[UserDetailArr[0] valueForKey:@"statuslic"] forKey:@"StatusLIC"];
                        [DatabaseDict setValue:[helper stringtobool:@"Y"] forKey:@"CanDelete"];
                        [DatabaseDict setValue:[helper stringtobool:@"Y"] forKey:@"CanEdit"];
                        
                    }
                
             }
            
            [DatabaseArr addObject:DatabaseDict];
            [PayloadArr addObject:ServerDict];

        }


        
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArr options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AesPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventTeam" pageno:@"" pagecount:@"" lastpage:@""];
        
        [helper insertEventTeamMember:appdelegate.eventid TeamArr:DatabaseArr];

        
       /* NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            if (jsonData)
            {
                [helper insertEventTeamMember:appdelegate.eventid TeamArr:DatabaseArr];
                [self CancelButtonAction:self];
            }
            
            
            
            
        }*/
        
        
         appdelegate.senttransaction = @"Y";
         [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Add Activity Plans to Event Type" entityname:@"EventTypeActivityPlans" Status:@"Open"];

        [self removepopup];

    }
        
    
}

- (IBAction)CancelButtonAction:(id)sender
{
//    [self performSegueWithIdentifier:@"unwindFromUserToEventTeam" sender:self];
//    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
//        [self.view removeFromSuperview];
//        [self removeFromParentViewController];
//    }];

    [self removepopup];
    
//    double delayInSeconds = 1.0;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
//                   {
//                       // do something
//                       [self CancelButtonAction:self];
//                       [self removepopup];
//                   });
//    
    
}

-(void)removepopup
{
    appdelegate.swipevar = @"Team";
    [self performSegueWithIdentifier:@"unwindFromUserToEventTeam" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}




#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([SelectionType isEqualToString:@"User"])
        return [UserArr count];
    else
        return [PositionArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *FirstNamelbl = (UILabel *)[contentView viewWithTag:1];
    UILabel *LastNamelbl = (UILabel *)[contentView viewWithTag:2];
    UILabel *UserIDlbl = (UILabel *)[contentView viewWithTag:3];
    UILabel *UserTypelbl = (UILabel *)[contentView viewWithTag:4];
  
    UILabel *Statuslbl = (UILabel *)[contentView viewWithTag:6];
    
    if([SelectionType isEqualToString:@"User"])
    {
        FirstNamelbl.text = [UserArr[indexPath.row] valueForKey:@"firstname"];
        LastNamelbl.text = [UserArr[indexPath.row] valueForKey:@"lastname"];
        UserIDlbl.text = [UserArr[indexPath.row] valueForKey:@"userid"];
        UserTypelbl.text = [UserArr[indexPath.row] valueForKey:@"usertype"];
        Statuslbl.text = [UserArr[indexPath.row] valueForKey:@"status"];
        
        if([SelectedUserArr count]>0)
        {
            for(int i=0;i<SelectedUserArr.count;i++)
            {
                if([[SelectedUserArr[i] valueForKey:@"Type"] isEqualToString:@"User"])
                {
                    if([[SelectedUserArr[i] valueForKey:@"ID"] isEqualToString:[UserArr[indexPath.row] valueForKey:@"userid"]])
                    {
                        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                    }
                }
            }
        }
    }
    else
    {
        FirstNamelbl.text = [PositionArr[indexPath.row] valueForKey:@"positionName"];
        LastNamelbl.text = [PositionArr[indexPath.row] valueForKey:@"positionType"];
        UserIDlbl.text = [PositionArr[indexPath.row] valueForKey:@"parentPositionName"];
        UserTypelbl.text = [PositionArr[indexPath.row] valueForKey:@"integrationSource"];
        Statuslbl.text= [PositionArr[indexPath.row] valueForKey:@"status"];

        [UserTypelbl setHidden:YES];
        if([SelectedUserArr count]>0)
        {
            for(int i=0;i<SelectedUserArr.count;i++)
            {
                if([[SelectedUserArr[i] valueForKey:@"Type"] isEqualToString:@"Position"])
                {
                    if([[SelectedUserArr[i] valueForKey:@"ID"] isEqualToString:[PositionArr[indexPath.row] valueForKey:@"positionID"]])
                    {
                        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                    }
                }
            }
        }

    }
   // customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    customcell.tintColor = [helper DarkBlueColour];
    return customcell;
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([SelectionType isEqualToString:@"User"])
    {
        
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[UserArr[indexPath.row] valueForKey:@"userid"] forKey:@"ID"];
        [dict setValue:@"User" forKey:@"Type"];
        [SelectedUserArr addObject:dict];
        
    }
    else
    {
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[PositionArr[indexPath.row] valueForKey:@"positionID"] forKey:@"ID"];
        [dict setValue:@"Position" forKey:@"Type"];
        [SelectedUserArr addObject:dict];
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([SelectionType isEqualToString:@"User"])
    {
        for (int i=0;i<[SelectedUserArr count]; i++)
        {
            NSDictionary *item = [SelectedUserArr objectAtIndex:i];
            if ([[item valueForKey:@"ID"]isEqualToString:[UserArr[indexPath.row] valueForKey:@"userid"]])
            {
                [SelectedUserArr removeObjectAtIndex:i];
                i--;
            }
        }
    }
    else
    {
        for (int i=0;i<[SelectedUserArr count]; i++)
        {
            NSDictionary *item = [SelectedUserArr objectAtIndex:i];
            if ([[item valueForKey:@"ID"] isEqualToString:[PositionArr[indexPath.row] valueForKey:@"positionID"]])
            {
                [SelectedUserArr removeObjectAtIndex:i];
                i--;
            }
        }

    }
}
#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if(searchBar == UserSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == UserSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([SelectionType isEqualToString:@"User"])
    {
        if(searchBar == UserSearchBarOutlet)
        {
            UserArr = CopyofUserArr;
            if(searchText.length == 0)
            {
                [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
            }
            else
            {
                count = searchText.length;
                NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.userid contains[c] %@) OR (SELF.usertype contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
                filtereventarr = [NSMutableArray arrayWithArray:[UserArr filteredArrayUsingPredicate:predicate]];
            
                UserArr = [filtereventarr copy];
            }
            [UserTableView reloadData];
            NSMutableArray *UseridArr = [[NSMutableArray alloc]init];
            for (int i =0; i<[UserArr count]; i++)
            {
                NSString *Userid = [UserArr[i] valueForKey:@"userid"];
                [UseridArr addObject:Userid];
            }
        
            for (int i =0; i<[SelectedUserArr count]; i++)
            {
                NSString *UserId = [SelectedUserArr objectAtIndex:i];
                if ([UseridArr containsObject:UserId])
                {
                    NSUInteger index = [UseridArr indexOfObject:UserId];
                    NSInteger Usercount = [UseridArr count];
                    if ((index > 0) || ((index == 0) && (index< Usercount)) )
                    {
                        [UserTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                    }
                }
            }//for loop end
        }
    }
    else
    {
        if(searchBar == UserSearchBarOutlet)
        {
            PositionArr = CopyofUserArr;
            if(searchText.length == 0)
            {
                [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
            }
            else
            {
                count = searchText.length;
                NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.positionName contains[c] %@) OR (SELF.parentPositionName contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.positionType contains[c] %@) OR (SELF.integrationSource contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
                filtereventarr = [NSMutableArray arrayWithArray:[PositionArr filteredArrayUsingPredicate:predicate]];
                
                PositionArr = [filtereventarr copy];
            }
            [UserTableView reloadData];

        }
   }
}


- (IBAction)usrPositionSegmentControl:(id)sender {
    
    
    NSInteger index=_usrPositionSegmentControl.selectedSegmentIndex;
    
    
    switch (index)
    {
            
        case 0:
            
            @autoreleasepool {
                
                                    for (int i = 0; i < [EventTeamArr count]; i++)
                                    {
                                        NSString *userid = [EventTeamArr[i] valueForKey:@"teamid"];
                                        for (int j= 0; j<[UserArr count]; j++)
                                        {
                                            NSString *Masteruserid = [UserArr[j] valueForKey:@"userid"];
                                            if ([Masteruserid isEqualToString:userid])
                                            {
                                                [UserArr removeObjectAtIndex:j];
                                                j--;
                                            }
                                        }
                                    }
                
            
                                    SelectionType=@"User";
                                    //keep original copy for search
                
                                    CopyofUserArr = [UserArr mutableCopy];
                                     [self languagesetuptranslations];
                                    UserSearchBarOutlet.text=@"";
                                    [UserTableView reloadData];
            
            }
            break;
            
        case 1:
            
            @autoreleasepool {
        
            
                                    for (int i = 0; i < [EventTeamArr count]; i++)
                                    {
                                            NSString *userid = [EventTeamArr[i] valueForKey:@"teamid"];
                                            for (int j= 0; j<[PositionArr count]; j++)
                                            {
                                                    NSString *Masteruserid = [PositionArr[j] valueForKey:@"positionID"];
                                                    if ([Masteruserid isEqualToString:userid])
                                                    {
                                                            [PositionArr removeObjectAtIndex:j];
                                                            j--;
                                                    }
                    
                                            }
                                    }
            
                                    SelectionType=@"Position";
                                    CopyofUserArr = [PositionArr mutableCopy];
                                    UserSearchBarOutlet.text=@"";
                                    [self languagesetuptranslations];
                                    [UserTableView reloadData];
            }
                break;
            
        default:
            break;
    }
    }

@end
