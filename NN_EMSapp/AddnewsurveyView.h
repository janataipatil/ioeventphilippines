//
//  AddnewsurveyView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/8/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "HSDatePickerViewController.h"

@interface AddnewsurveyView : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    
    IBOutlet UITableView *LOVTableview;
}

@property (strong, nonatomic) IBOutlet UITextField *Templatename_tv;
@property (strong, nonatomic) IBOutlet UITextField *Templatedesc_tv;

@property(strong,nonatomic)NSString *surveyid;


@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic)NSString *TypeofActionStr;

@property(strong,nonatomic)NSString *senderview;
@property(strong,nonatomic)NSString *cancelsegue;
@property (strong, nonatomic) IBOutlet UILabel *viewtitle_lbl;

@property (strong, nonatomic) IBOutlet UIButton *StartDateButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *EndDateButtonOutlet;
- (IBAction)save_btn:(id)sender;

- (IBAction)cancel_btn:(id)sender;
- (IBAction)startdate_btn:(id)sender;
- (IBAction)enddate_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *dropdown_view;
@property (strong, nonatomic) IBOutlet UILabel *dropdown_lbl;

@property (strong, nonatomic) IBOutlet UILabel *desc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *surveyname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *sdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *edate_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;


@end
