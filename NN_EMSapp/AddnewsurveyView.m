//
//  AddnewsurveyView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/8/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AddnewsurveyView.h"

@interface AddnewsurveyView ()<HSDatePickerViewControllerDelegate>

{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    SLKeyChainStore *keychain;
    NSString *generatedrowid;
    
    NSString *affiliateid;
    NSString *TextfieldFlag;
    NSArray *LOVArr;
    NSString *startEndDate;
    NSString *transaction_type;
    NSDateFormatter *dateFormatter;
    
    
    NSArray * Surveylisttemplate;
    
    NSString *surveytemplateid;
    NSMutableArray *SurveyNameArr;
    
    NSArray *surveylist;
    NSInteger count;
    NSPredicate *EventidPredicate;
    
    NSDate *sdate;
    NSDate *edate;
    
}

@property (nonatomic, strong) NSDate *selectedDate;

@end

@implementation AddnewsurveyView
@synthesize helper,senderview,cancelsegue,StartDateButtonOutlet,EndDateButtonOutlet;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    Surveylisttemplate = [helper query_alldata:@"Survey_templates"];
appdelegate.translations = [helper query_alldata:@"Lang_translations"];

    SurveyNameArr = [[NSMutableArray alloc]init];
    for (int i = 0; i<[Surveylisttemplate count]; i++)
    {
        [SurveyNameArr addObject:[Surveylisttemplate[i] valueForKey:@"name"]];
    }
    
   keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
 
   
    _dropdown_view.hidden = YES;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    
    if (appdelegate.surveyid.length >0)
    {
        senderview = @"Editsurvey";
    }
    else
    {
        senderview = @"Addsurvey";
    }
    
    if([senderview isEqualToString:@"Addsurvey"])
    {
        generatedrowid = [helper generate_rowId];
        transaction_type = @"Add New Survey";
     _viewtitle_lbl.text = [helper gettranslation:@"LBL_022"];
    }
    else if([senderview isEqualToString:@"Editsurvey"])
    {
        generatedrowid = appdelegate.surveyid;
        transaction_type = @"Edit Survey";
        _viewtitle_lbl.text = [helper gettranslation:@"LBL_773"];
        [self setupattendeeview];
    }
    
    [self langsetuptranslations];
    
    LOVTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableview.layer.borderWidth = 1;
    
    [_Templatename_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _Templatename_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
}


-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];

    _desc_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_094"]];
    [self markasrequired:_desc_ulbl];
    
    _sdate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_240"]];
    [self markasrequired:_sdate_ulbl];
    
    _edate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_123"]];
    [self markasrequired:_edate_ulbl];
    
    _surveyname_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_258"]];
    [self markasrequired:_surveyname_ulbl];
    
}
-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupattendeeview
{
    surveylist = [helper query_alldata:@"Event_Survey"];
    surveylist = [surveylist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.surveyid]];
    if ([surveylist count]>0)
    {
        _Templatename_tv.text = [surveylist[0] valueForKey:@"title"];
        _Templatedesc_tv.text = [surveylist[0] valueForKey:@"survey_desc"];
        [StartDateButtonOutlet setTitle:[helper formatingdate:[surveylist[0] valueForKey:@"survey_startdate"] datetime:@"FormatDate"] forState:UIControlStateNormal];
        [EndDateButtonOutlet setTitle:[helper formatingdate:[surveylist[0] valueForKey:@"survey_enddate"] datetime:@"FormatDate"] forState:UIControlStateNormal];
        
        _viewtitle_lbl.text = [surveylist[0] valueForKey:@"title"];
       
    }
    if (![appdelegate.EventUserRoleArray containsObject:@"E_SRV_MNG"])
    {
        _Templatename_tv.userInteractionEnabled = NO;
        StartDateButtonOutlet.userInteractionEnabled = NO;
        EndDateButtonOutlet.userInteractionEnabled = NO;
    }
    
    
}

- (IBAction)save_btn:(id)sender
{
    int flag = 0;
    sdate = [dateFormatter dateFromString:StartDateButtonOutlet.titleLabel.text];
    edate = [dateFormatter dateFromString:EndDateButtonOutlet.titleLabel.text];
        if ((_Templatename_tv.text.length > 0)&&(StartDateButtonOutlet.titleLabel.text.length > 0)&&(EndDateButtonOutlet.titleLabel.text.length > 0))
    {
        NSMutableArray *TemplateNameArr = [[NSMutableArray alloc]init];
        NSMutableArray *StartTimeArr = [[NSMutableArray alloc]init];
        NSMutableArray *EndTimeArr = [[NSMutableArray alloc]init];
        NSArray *SurveyArr = [helper query_alldata:@"Event_Survey"];
        for (int i = 0;i<[SurveyArr count];i++)
        {
            [TemplateNameArr addObject:[SurveyArr[i] valueForKey:@"title"]];
            [StartTimeArr addObject:[SurveyArr[i] valueForKey:@"survey_startdate"]];
            [EndTimeArr addObject:[SurveyArr[i] valueForKey:@"survey_enddate"]];
        }
        double StartTime = [[helper converingformateddatetooriginaldate:StartDateButtonOutlet.titleLabel.text datetype:@"ServerDate"] doubleValue];
        double EndTime = [[helper converingformateddatetooriginaldate:EndDateButtonOutlet.titleLabel.text datetype:@"ServerDate"] doubleValue];
        NSDateFormatter *startdateFormatter = [[NSDateFormatter alloc] init];
        [startdateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [startdateFormatter setDateFormat:@"yyyyMMddHHmmss"];
      //  UILabel *mylabel = [[UILabel alloc]init];
        if ([TemplateNameArr containsObject:_Templatename_tv.text]  )
        {
            for (int i =0; i<[StartTimeArr count]; i++)
            {
                // NSLog(@"int is %d",i);
                if ((StartTime <= [[StartTimeArr objectAtIndex:i] doubleValue]) && (EndTime <= [[StartTimeArr objectAtIndex:i]doubleValue]))
                {
                    
                  //  [helper showalert:[helper gettranslation:@"LBL_590"] message:@"Survey overlaps with the existing survey" action:[helper gettranslation:@"LBL_462"]];
                    
                    //[self CreateSurvey];
                    flag = 1;
                   break;
                   // break;
                    
                
                }
                else if ((StartTime >= [[EndTimeArr objectAtIndex:i] doubleValue]) && (EndTime >= [[EndTimeArr objectAtIndex:i]doubleValue]))
                {
                    //[helper showalert:[helper gettranslation:@"LBL_590"] message:@"Survey overlaps with the existing survey" action:[helper gettranslation:@"LBL_462"]];
                    //return;
                    
                   // [self CreateSurvey];
                    flag = 1;
                    break;
                    

                }
                else
                {
                    // NSLog(@"int is %d",i);
                   // [helper showalert:[helper gettranslation:@"LBL_590"] message:@"Survey overlaps with the existing survey" action:[helper gettranslation:@"LBL_462"]];
                    flag = 0;
                    // break;
                }
               
            }
            
                if (flag == 0)
                {
                    [helper showalert:[helper gettranslation:@"LBL_590"] message:@"Survey overlaps with the existing survey" action:[helper gettranslation:@"LBL_462"]];
                    
                }
                else
                {
                    [self CreateSurvey];
                //flag = 0;
                }

            
        }
        else
        {
            [self CreateSurvey];
        }
        
        
   
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
    }
}
-(void)CreateSurvey
{
    if([senderview isEqualToString:@"Addsurvey"])
    {
        [self savedata:surveylist];
    }
    else if([senderview isEqualToString:@"Editsurvey"])
    {
        //edit records
        //            && [_eventloc_tv.text isEqualToString:[eventvenue[0] valueForKey:@"location"]?: @""]
        if ([_Templatename_tv.text isEqualToString:[surveylist[0] valueForKey:@"title"]?: @""]  && [_Templatedesc_tv.text isEqualToString:[surveylist[0] valueForKey:@"survey_desc"]?: @""] &&[[dateFormatter stringFromDate:sdate] isEqualToString:[surveylist[0] valueForKey:@"survey_startdate"]?: @""] && [[dateFormatter stringFromDate:edate] isEqualToString:[surveylist[0] valueForKey:@"survey_enddate"]?: @""] )
        {
            
            [self removepopup];
        }
        else
        {
            [self savedata:surveylist];
            
        }
    }

}

-(void)savedata:(NSArray *) Surveylistdata
{
    NSError *error;
   /* [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:[[NSTimeZone localTimeZone] abbreviation]]];
    NSDate *sdate = [dateFormatter dateFromString:StartDateButtonOutlet.titleLabel.text];
    NSDate *edate = [dateFormatter dateFromString:EndDateButtonOutlet.titleLabel.text];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];*/
    
    
    
    [helper showWaitCursor:[helper gettranslation:@"LBL_612"]];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    //    Eventlist = [helper query_alldata:@"Event"];
    //    Eventlist = [Eventlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    //[JsonDict setValue:[dateFormatter stringFromDate:sdate] forKey:@"StartDate"];
    //[JsonDict setValue:[dateFormatter stringFromDate:edate] forKey:@"EndDate"];
   
    [JsonDict setValue:generatedrowid forKey:@"ID"];
    [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
    [JsonDict setValue:affiliateid forKey:@"AffiliateID"];
    [JsonDict setValue:surveytemplateid forKey:@"SurveyTemplateID"];
    [JsonDict setValue:_Templatename_tv.text forKey:@"Title"];
    [JsonDict setValue:_Templatedesc_tv.text forKey:@"Description"];
    
   
    
    [JsonDict setValue:[helper converingformateddatetooriginaldate:StartDateButtonOutlet.titleLabel.text datetype:@"ServerDate"] forKey:@"StartDate"];
    [JsonDict setValue:[helper converingformateddatetooriginaldate:EndDateButtonOutlet.titleLabel.text datetype:@"ServerDate"] forKey:@"EndDate"];
    
    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArray options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventSurvey" pageno:@"" pagecount:@"" lastpage:@""];
    

    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        [self getsurveydetails];
    }
    [helper removeWaitCursor];
    [self removepopup];
}

- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}

- (IBAction)startdate_btn:(id)sender
{
    startEndDate = @"StartDate";
   HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    NSString *activityEndDate=[self.EndDateButtonOutlet.titleLabel text];
    
    hsdpvc.delegate = self;
   
    hsdpvc.date = [NSDate date];
    hsdpvc.minDate = [NSDate date];
    if(activityEndDate.length>0)
    {
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
        
        NSDate *actEndDate=[dateFormatter1 dateFromString:activityEndDate];
        
        hsdpvc.maxDate=actEndDate;
        
        
    }
 
//    }
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (IBAction)enddate_btn:(id)sender
{
    startEndDate = @"EndDate";
    
   HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    
    NSString *activityStartDate=[self.StartDateButtonOutlet.titleLabel text];
    hsdpvc.delegate = self;
    
    if(activityStartDate.length>0)
    {
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
        
        NSDate *actStartDate=[dateFormatter1 dateFromString:activityStartDate];
        hsdpvc.date = actStartDate;
        hsdpvc.minDate = actStartDate;
        
    }
    else
        hsdpvc.minDate=[NSDate date];

    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (IBAction)location_btn:(id)sender
{
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
   
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
    
    if ([startEndDate isEqualToString:@"StartDate"])
    {
    
        
       // [StartDateButtonOutlet setTitle:[dateFormater stringFromDate:date] forState:UIControlStateNormal];
        [StartDateButtonOutlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    }
    else if ([startEndDate isEqualToString:@"EndDate"])
    {
       // [EndDateButtonOutlet setTitle:[dateFormater stringFromDate:date] forState:UIControlStateNormal];
        [EndDateButtonOutlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    }
    
    self.selectedDate = date;
}


-(void)removepopup
{
    
    [self performSegueWithIdentifier:@"unwindtoSurveylist" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=====-=-=-=-=-=-=-=-=-=-=-=-=-==-==-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=÷÷


#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{

    if (textField ==_Templatename_tv)
        //&& )
    {
        if([senderview isEqualToString:@"Addsurvey"])
        {
            [_Templatename_tv setUserInteractionEnabled:YES];
            TextfieldFlag = @"SurveyTextField";
            LOVArr = [Surveylisttemplate mutableCopy];
          //  [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_258"];
            [LOVTableview reloadData];
            LOVTableview.hidden = NO;
            _dropdown_view.hidden = NO;
        }
        else
            [_Templatename_tv setUserInteractionEnabled:NO];
    }
    

}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _Templatename_tv)
    {
        //[helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
        if(![SurveyNameArr containsObject:_Templatename_tv.text])
        {
            _Templatename_tv.text = @"";
        }
    }
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == _Templatename_tv)
        {
            [Surveylisttemplate mutableCopy];
        }
    }
    else
    {
        if (theTextField == _Templatename_tv)
        {
             [Surveylisttemplate mutableCopy];
            
        }
        
        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(name contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
        LOVArr = [filteredpartArray copy];
        
    }
    [LOVTableview reloadData];
    
    
}



#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [LOVTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
   // UIView *sepratorview = (UIView *)[contentView viewWithTag:2];

    lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"name"];
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    
    if([TextfieldFlag isEqualToString:@"SurveyTextField"])
    {
        _Templatename_tv.text = [LOVArr[indexPath.row] valueForKey:@"name"];
        _Templatedesc_tv.text = [LOVArr[indexPath.row] valueForKey:@"templatedescription"];
        surveytemplateid = [LOVArr[indexPath.row] valueForKey:@"id"];
        [_Templatename_tv resignFirstResponder];
    }
    LOVTableview.hidden = YES;
    _dropdown_view.hidden = YES;
}
#pragma mark touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != LOVTableview)
    {
        LOVTableview.hidden = YES;
        _dropdown_view.hidden = YES;
        [_Templatename_tv resignFirstResponder];
        [_Templatedesc_tv resignFirstResponder];
    }
}


-(void) getsurveydetails
{
    NSError *error;
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
    [JsonDict setValue:@"SURVEY" forKey:@"Entity"];
    
    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSGetEventDetails" pageno:@"" pagecount:@"" lastpage:@""];
    
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSArray *EventSurvey = [jsonData valueForKey:@"EventSurvey"];
        if (EventSurvey.count >0)
        {
            [helper delete_alldata:@"Event_Survey"];
            [helper insertEventSurvey:appdelegate.eventid SurveyArr:EventSurvey];
        }
        
        NSArray *EventSurveyQn = [jsonData valueForKey:@"EventSurveyQn"];
        if (EventSurveyQn.count >0)
        {
            [helper delete_alldata:@"Event_SurveyQuestion"];
            [helper insertEventSurveyQn:appdelegate.eventid SurveyQnArr:EventSurveyQn];
        }
    }
   
}


@end
