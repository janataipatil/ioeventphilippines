//
//  AdvanceSearchView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/27/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "Reachability.h"


@interface AdvanceSearchView : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UITableView *LOVTableview;
}

@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic) Reachability* reachability;
@property (strong, nonatomic) IBOutlet UITextField *firstname_tv;
@property (strong, nonatomic) IBOutlet UITextField *lastname_tv;
@property (strong, nonatomic) IBOutlet UITextField *emailaddress_tv;
@property (strong, nonatomic) IBOutlet UITextField *status_tv;
@property (strong, nonatomic) IBOutlet UITextField *integrationsource_tv;
@property (strong, nonatomic) IBOutlet UITextField *institution_tv;

@property (strong, nonatomic) IBOutlet UITextField *speciality_tv;
@property (strong, nonatomic) IBOutlet UITextField *targetclass_tv;
@property (strong, nonatomic) IBOutlet UITextField *subtargetclass_tv;

@property (strong, nonatomic) IBOutlet UIButton *cancel_btn;
- (IBAction)cancel_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *search_btn;
- (IBAction)search_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *dropdown_view;
@property (strong, nonatomic) IBOutlet UILabel *dropdown_lbl;

@property (strong, nonatomic) IBOutlet NSString * cancelSegue;
@property (strong, nonatomic) IBOutlet NSString * senderview;
@property (strong, nonatomic) IBOutlet UITextField *idtf;

@property (strong, nonatomic) IBOutlet UILabel *idlbl;

@property (strong, nonatomic) IBOutlet UILabel *titlelabel_ulbl;

//@property (strong, nonatomic) IBOutlet UILabel *salutation_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *first_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *lastname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *emailaddress_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *integrationsrc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *institution_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *speciality_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *targetclass_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *subtargetclass_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *searchbtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;




@end
