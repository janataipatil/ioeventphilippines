//
//  AdvanceSearchView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/27/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AdvanceSearchView.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "SOCustomCell.h"

@interface AdvanceSearchView ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSPredicate *advsrc_predicate;
    NSArray *Eventlovlist;
    NSArray *LOVArr;
    NSString *TextfieldFlag;
}
@end

@implementation AdvanceSearchView
@synthesize helper;

- (void)viewDidLoad
{
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    
    _dropdown_view.hidden = YES;
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    
    // Do any additional setup after loading the view.
    LOVTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableview.layer.borderWidth = 1;
    
    [_status_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_integrationsource_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_speciality_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_targetclass_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_subtargetclass_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self langsetuptranslations];
}

-(void)langsetuptranslations
{
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_searchbtn_ulbl setTitle:[helper gettranslation:@"LBL_224"] forState:UIControlStateNormal];
    
//    _salutation_ulbl.text = [helper gettranslation:@"LBL_218"];
    _first_ulbl.text = [helper gettranslation:@"LBL_141"];
    _lastname_ulbl.text = [helper gettranslation:@"LBL_153"];
    
    _speciality_ulbl.text = [helper gettranslation:@"LBL_238"];
    _speciality_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
    _targetclass_ulbl.text = [helper gettranslation:@"LBL_255"];
    _targetclass_tv.placeholder=[helper gettranslation:@"LBL_757"];
    _status_ulbl.text = [helper gettranslation:@"LBL_242"];
    _status_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
    _emailaddress_ulbl.text = [helper gettranslation:@"LBL_119"];
    _integrationsrc_ulbl.text = [helper gettranslation:@"LBL_148"];
    _integrationsource_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
    _institution_ulbl.text = [helper gettranslation:@"LBL_147"];
    _subtargetclass_ulbl.text = [helper gettranslation:@"LBL_254"];
    _subtargetclass_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
    _titlelabel_ulbl.text = [helper gettranslation:@"LBL_309"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}
- (IBAction)search_btn:(id)sender
{
    
    NSString *predicatestr;
    
    if ( _firstname_tv.text.length > 0 || _lastname_tv.text.length > 0 || _emailaddress_tv.text.length > 0 || _status_tv.text.length > 0 || _integrationsource_tv.text.length > 0 || _institution_tv.text.length > 0 || _speciality_tv.text.length > 0 || _targetclass_tv.text.length > 0 || _subtargetclass_tv.text.length > 0 || _idtf.text.length > 0)
    {
        
        if (_firstname_tv.text.length > 0)
        {
            predicatestr = [NSString stringWithFormat:@"(SELF.firstname contains[c] \"%@\")",_firstname_tv.text];
        }
        if (_lastname_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.lastname contains[c] \"%@\")",predicatestr,_lastname_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.lastname contains[c] \"%@\")",_lastname_tv.text];
            }

        }
        if (_emailaddress_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.emailaddress contains[c] \"%@\")",predicatestr,_emailaddress_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.emailaddress contains[c] \"%@\")",_emailaddress_tv.text];
            }
        }
        if (_status_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.status contains[c] \"%@\")",predicatestr,_status_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.status contains[c] \"%@\")",_status_tv.text];
            }
        }
        if (_integrationsource_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.integrationsource contains[c] \"%@\")",predicatestr,_integrationsource_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.integrationsource contains[c] \"%@\")",_integrationsource_tv.text];
            }
        }
        if (_institution_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.institution contains[c] \"%@\")",predicatestr,_institution_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.institution contains[c] \"%@\")",_institution_tv.text];
            }
        }
        if (_speciality_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.speciality contains[c] \"%@\")",predicatestr,_speciality_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.speciality contains[c] \"%@\")",_speciality_tv.text];
            }
        }
        if (_targetclass_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.targetclass contains[c] \"%@\")",predicatestr,_targetclass_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.targetclass contains[c] \"%@\")",_targetclass_tv.text];
            }
        }
        if (_subtargetclass_tv.text.length > 0)
        {
            if (predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.subTargetClass contains[c] \"%@\")",predicatestr,_subtargetclass_tv.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.subTargetClass contains[c] \"%@\")",_subtargetclass_tv.text];
            }
        }
        if(_idtf.text.length > 0)
        {
            if(predicatestr.length > 0)
            {
                predicatestr = [NSString stringWithFormat:@"%@ AND (SELF.id contains[c] \"%@\")",predicatestr,_idtf.text];
            }
            else
            {
                predicatestr = [NSString stringWithFormat:@"(SELF.id contains[c] \"%@\")",_idtf.text];

            }
        }
        
        
        
        advsrc_predicate = [NSPredicate predicateWithFormat:predicatestr];
        
        appdelegate.advsrcpredicate = advsrc_predicate;
    }
    else
    {
        appdelegate.advsrcpredicate = nil;
    }
    
    [self removepopup];
}

-(void)removepopup
{
    
    [self performSegueWithIdentifier:_cancelSegue sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=====-=-=-=-=-=-=-=-=-=-=-=-=-==-==-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=÷÷


#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == _status_tv)
    {
        TextfieldFlag = @"StatusTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_242"];
    }
    else if (textField == _targetclass_tv)
    {
        TextfieldFlag = @"TargetClassTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"TRGT_CLASS"]];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_255"];
    }
    else if (textField == _speciality_tv)
    {
        TextfieldFlag = @"SpecialityTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_238"];
        
    }
    else if (textField == _integrationsource_tv)
    {
        TextfieldFlag = @"IntegrationSourceTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"INT_SRC"]];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_148"];
    }
    else if (textField == _subtargetclass_tv)
    {
        TextfieldFlag = @"SubtargetClassTextField";
       // LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SUB_TRGT_CLASS"]];
       // NSString *targetclasslicvalue =[helper getLic:@"TRGT_CLASS" value:_targetclass_tv.text] ;
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SUB_TRGT_CLASS"]];

        
        _dropdown_lbl.text = [helper gettranslation:@"LBL_254"];
    }
    
    
    if ( textField == _status_tv || textField == _targetclass_tv || textField == _speciality_tv || textField == _integrationsource_tv || textField == _subtargetclass_tv)
    {
        [LOVTableview reloadData];
        [self adjustHeightOfTableview:LOVTableview];
        LOVTableview.hidden = NO;
        _dropdown_view.hidden = NO;
    }
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == _status_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (theTextField == _targetclass_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"TRGT_CLASS"]];
        }
        else if (theTextField == _speciality_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (theTextField == _integrationsource_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"INT_SRC"]];
        }
        else if (theTextField == _subtargetclass_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SUB_TRGT_CLASS"]];
        }
    }
    else
    {
        if (theTextField == _status_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (theTextField == _targetclass_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"TRGT_CLASS"]];
        }
        else if (theTextField == _speciality_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (theTextField == _integrationsource_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"INT_SRC"]];
        }
        else if (theTextField == _subtargetclass_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SUB_TRGT_CLASS"]];
        }

        
        if ( theTextField == _status_tv || theTextField == _targetclass_tv || theTextField == _speciality_tv || theTextField == _integrationsource_tv || theTextField == _subtargetclass_tv)
        {
            NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
            LOVArr = [filteredpartArray copy];
        }
    }
    [LOVTableview reloadData];
}


#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == LOVTableview)
    {
        CGFloat height = LOVTableview.contentSize.height;
        CGFloat maxHeight = LOVTableview.superview.frame.size.height - LOVTableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableview.frame;
            
            if ([TextfieldFlag isEqualToString:@"StatusTextField"])
            {
               // frame.size.height = 100;
                frame.origin.x = 189;
                frame.origin.y = 66;
            }
            if ([TextfieldFlag isEqualToString:@"TargetClassTextField"])
            {
                frame.origin.x = 398;
                frame.origin.y = 125;
            }
            if ([TextfieldFlag isEqualToString:@"SpecialityTextField"])
            {
                frame.origin.x = 187;
                frame.origin.y = 125;
            }
            if ([TextfieldFlag isEqualToString:@"IntegrationSourceTextField"])
            {
                frame.origin.x = 398;
                frame.origin.y = 66;
            }
            if ([TextfieldFlag isEqualToString:@"SubtargetClassTextField"])
            {
                frame.origin.x = 603;
                frame.origin.y = 125;
            }
    
            _dropdown_view.frame= frame;
//            LOVTableview.frame = frame;
        }];
        
    }
}
#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [LOVTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
    
    lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([TextfieldFlag isEqualToString:@"StatusTextField"])
    {
        _status_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_status_tv resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"TargetClassTextField"])
    {
        _targetclass_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_targetclass_tv resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"SpecialityTextField"])
    {
        _speciality_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_speciality_tv resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"IntegrationSourceTextField"])
    {
        _integrationsource_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_integrationsource_tv resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"SubtargetClassTextField"])
    {
        _subtargetclass_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_subtargetclass_tv resignFirstResponder];
    }
    LOVTableview.hidden = YES;
    _dropdown_view.hidden = YES;
}

#pragma mark touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != LOVTableview)
    {
        LOVTableview.hidden = YES;
        _dropdown_view.hidden = YES;
        [_status_tv resignFirstResponder];
        [_targetclass_tv resignFirstResponder];
        [_speciality_tv resignFirstResponder];
        [_integrationsource_tv resignFirstResponder];
        [_subtargetclass_tv resignFirstResponder];
    }
}

/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == _eventname_tv || textField == _eventdesc_tv)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}
*/


@end
