//
//  AllAttendeesVC.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 12/16/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "Attendee_detail.h"
#import "TitleBar.h"
#import "MenueBar.h"
#import "ToastView.h"
#import "HelpViewController.h"

@class HelperClass;

@interface AllAttendeesVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic) Reachability* reachability;
@property(strong,nonatomic) Attendee_detail *attendetailview;
@property (strong, nonatomic) IBOutlet UITableView *Attendeelistview;
@property (strong, nonatomic) IBOutlet UISearchBar *AttendeeSearchBar;
@property (strong, nonatomic) IBOutlet UILabel *attendee_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;
@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;
@property (strong, nonatomic) IBOutlet UIButton *AddAttendeeButtonOutlet;

- (IBAction)advancesearch_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *downloadProgress;

- (IBAction)addattendee_btn:(id)sender;
- (IBAction)import_attendee_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *adv_screachbtn;
- (IBAction)adv_screachbt:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *progressStatus;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

-(void)refreshProgressbar:(float)resourceNumber totalResources:(float)totalResources lastpage:(NSString *)lastpage;
-(void)checkProgressUpdate:(NSTimer *)timer;
@property (strong, nonatomic) IBOutlet UILabel *salutation;
@property (strong, nonatomic) IBOutlet UILabel *firstname;
@property (strong, nonatomic) IBOutlet UILabel *lastname;
@property (strong, nonatomic) IBOutlet UILabel *speciality;
@property (strong, nonatomic) IBOutlet UILabel *targetclass;
@property (strong, nonatomic) IBOutlet UILabel *status;

- (IBAction)salutationBtn:(id)sender;
- (IBAction)firstNameBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *lastNameBtn;
- (IBAction)specialityBtn:(id)sender;
- (IBAction)targetClassBtn:(id)sender;
- (IBAction)statusBtn:(id)sender;
- (IBAction)lastNameBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *firstNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *salutationImg;
@property (strong, nonatomic) IBOutlet UIImageView *lastNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *specialityImg;
@property (strong, nonatomic) IBOutlet UIImageView *statusImg;
@property (strong, nonatomic) IBOutlet UIImageView *targetClassImg;

@end
