//
//  AllAttendeesVC.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 12/16/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "AllAttendeesVC.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "SOCustomCell.h"
#import "AddAttendee.h"
#import "AdvanceSearchView.h"

@interface AllAttendeesVC ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSString *senderview;
    NSString *download;
    NSArray *attdata;
    BOOL salutationFlag;
    BOOL firstNameFlag;
    BOOL lastNameFlag;
    BOOL statusFlag;
    BOOL targetClassFlag;
    BOOL specialityFlag;
    NSArray *AttendeePositions;
    NSArray *MasterAttendeeDetail;
    
}


@end

@implementation AllAttendeesVC
{
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    
    SLKeyChainStore *keychain;
    MSClient *client;
    NSArray *attendeelist;
    
    NSString *appID;
    NSString *custID;
    NSString *AESPrivateKey;
    NSInteger ln_expandedRowIndex;
    
    NSString *sel_attendeeid;
    ToastView *showToast;
    UIButton *del_btn;
    
    NSInteger count;
    int RefreshTableview;
    HelpViewController *HelpView;
    
}

@synthesize helper,TitleBarView,MenueBarView,Attendeelistview,AttendeeSearchBar,attendee_lbl,attendetailview,AddAttendeeButtonOutlet,progressStatus,progressIndicator;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
    AESPrivateKey =[helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    AttendeePositions = [helper query_alldata:@"AttendeePositions"];
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = YES;
    TitleBarView.UsernameLbl.text = [helper getUsername];
    showToast=[[ToastView alloc]init];
    
    salutationFlag=YES;
    firstNameFlag=YES;
    lastNameFlag=YES;
    statusFlag=YES;
    targetClassFlag=YES;
    specialityFlag=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    
    NSString *lastpageStatus=[[NSUserDefaults standardUserDefaults]valueForKey:@"lastPageStatus"];
    NSString *page=[[NSUserDefaults standardUserDefaults]valueForKey:@"pageno"];
    
    
    
    NSLog(@"user Default:%@ %@",lastpageStatus,page);
    
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
    [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    MenueBarView = [[MenueBar alloc]initWithFrame:CGRectMake(0, 20, 100, 748)];
    MenueBarView.AttendeeButton.backgroundColor = [helper LightBlueColour];
    [MenueBarView.CalendarButton addTarget:self action:@selector(CalendarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivitiesButton addTarget:self action:@selector(ActivityButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.SpeakerButton addTarget:self action:@selector(SpeakerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.HomeButton addTarget:self action:@selector(HomeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivityPlanButton addTarget:self action:@selector(ActivityPlanButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    MenueBarView.VersionNumber.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
    
    _adv_screachbtn.hidden = YES;
    
    
    [self.view addSubview:MenueBarView];
    [self.view addSubview:TitleBarView];
    
    
    [self langsetuptranslations];
    
    attendetailview = [[Attendee_detail alloc] init];
   
    ln_expandedRowIndex = -1;
    
    //    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    
    if ([userarr count]>0)
    {
        _Username_lbl.text = [NSString stringWithFormat:@"%@ %@",[userarr[0] valueForKey:@"firstname"],[userarr[0] valueForKey:@"lastname"]];
    }
    
    attendeelist = [helper query_alldata:@"Attendee"];
    attdata = attendeelist;
    
    AttendeeSearchBar.delegate = self;
    [AttendeeSearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchfield = [AttendeeSearchBar valueForKey:@"_searchField"];
    searchfield.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
    searchfield.placeholder = [helper gettranslation:@"LBL_224"];
    searchfield.backgroundColor = [UIColor whiteColor];
    
    
    //Add swipe gesture recogniser
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeRightAction:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    if (![appdelegate.UserRolesArray containsObject:@"ACT_ADM"])
    {
        MenueBarView.ActivityPlanButton.hidden = YES;
    }
    //Role back access
    if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] || (![appdelegate.UserRolesArray containsObject:@"ATT_R"] && ![appdelegate.UserRolesArray containsObject:@"ATT_W"]) )
    {
        AddAttendeeButtonOutlet.hidden = YES;
    }
    if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] && [appdelegate.UserRolesArray containsObject:@"ATT_W"])
    {
        AddAttendeeButtonOutlet.hidden = NO;
    }
    
    _downloadProgress.text=[helper gettranslation:@"LBL_628"];
    
    [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self selector:@selector(checkProgressUpdate:) userInfo: nil repeats:YES];
    
    
    if([appdelegate.downloadInProgress isEqualToString:@"NO"] && [lastpageStatus isEqualToString:@"N"])//Start background thread if download is not completed
    {
        appdelegate.lstpage=lastpageStatus;
        appdelegate.pageno=page;
        [helper dispatchAttendeeThread];
        [ToastView showToastInParentView:self.view withText:@"Download Started" withDuaration:1.0];
    }
    
    [progressIndicator startAnimating];
    
    if([lastpageStatus isEqualToString:@"Y"])
    {
        appdelegate.lstpage=lastpageStatus;
        [progressIndicator setHidden:YES];
        [_downloadProgress setHidden:YES];
    }
    
}
-(void)viewDidAppear:(BOOL)animated
{   
    if (RefreshTableview == 1)
    {
        Attendeelistview.delegate = self;
        Attendeelistview.dataSource = self;
        [Attendeelistview reloadData];
    }
    MasterAttendeeDetail = [helper query_alldata:@"Attendee"];
}
-(void)refreshProgressbar:(float)resourceNumber totalResources:(float)totalResources lastpage:(NSString *)lastpage
{
    if([lastpage isEqualToString:@"N"])
    {
        appdelegate.downloadProgress=[NSString stringWithFormat:@"%f Completed",(resourceNumber/totalResources)*100];
    }
}
-(void)langsetuptranslations
{
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [MenueBarView.HomeButton setTitle:[helper gettranslation:@"LBL_143"] forState:UIControlStateNormal];
    [MenueBarView.ActivitiesButton setTitle:[helper gettranslation:@"LBL_003"] forState:UIControlStateNormal];
    [MenueBarView.CalendarButton setTitle:[helper gettranslation:@"LBL_060"] forState:UIControlStateNormal];
    [MenueBarView.SpeakerButton setTitle:[helper gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [MenueBarView.AttendeeButton setTitle:[helper gettranslation:@"LBL_054"] forState:UIControlStateNormal];
    [MenueBarView.ActivityPlanButton setTitle:[helper gettranslation:@"LBL_007"] forState:UIControlStateNormal];
    
    
    
    _salutation.text = [helper gettranslation:@"LBL_218"];
    _firstname.text = [helper gettranslation:@"LBL_141"];
    _lastname.text = [helper gettranslation:@"LBL_153"];
    _speciality.text = [helper gettranslation:@"LBL_238"];
    _targetclass.text = [helper gettranslation:@"LBL_255"];
    _status.text = [helper gettranslation:@"LBL_242"];
    
}


#pragma mark swipe method
-(IBAction)SwipeRightAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark tableview method
//------------------Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 117; // 60+77
    }
    
    return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

   return [attendeelist count] + (ln_expandedRowIndex != -1 ? 1 : 0);
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = [indexPath row];
    NSInteger dataIndex = [self dataIndexForRowIndex:row];
    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
   
    if (!ln_expandedCell)
    {
        SOCustomCell *customcell;
        
        NSString *cellIdentifier = @"customcell";
        
        NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
        if(attendeelist.count>0)
        {
            [SelectedArray insertObject:attendeelist[dataIndex] atIndex:0];
        
        customcell = [self.Attendeelistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
        customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        
        UILabel *salutation = (UILabel *) [contentView viewWithTag:1];
        UILabel *firstname = (UILabel *) [contentView viewWithTag:2];
        UILabel *lastname = (UILabel *) [contentView viewWithTag:3];
        UILabel *status = (UILabel *) [contentView viewWithTag:4];
        UILabel *ownedby = (UILabel *) [contentView viewWithTag:5];
        
        UILabel *speciality = (UILabel *) [contentView viewWithTag:6];
        UIButton *editing_btn = (UIButton *) [contentView viewWithTag:7];
        
        
        
        [editing_btn addTarget:self action:@selector(editactivityClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSLog(@"indexPath.row = %ld",(long)indexPath.row);
        
        [salutation setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"salutation"]?:@""]];
        [firstname setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"firstname"]]];
        [lastname setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"lastname"]]];
        [ownedby setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"targetclass"]?:@""]];
        [status setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"status"]]];
        [speciality setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"speciality"]?:@""]];
        
        [customcell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] || (![appdelegate.UserRolesArray containsObject:@"ATT_R"] && ![appdelegate.UserRolesArray containsObject:@"ATT_W"]))
        {
            [editing_btn setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
        }
        if ([appdelegate.UserRolesArray containsObject:@"ATT_W"])
        {
            [editing_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] && [appdelegate.UserRolesArray containsObject:@"ATT_W"])
        {
            [editing_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        int count=indexPath.row;
//        NSLog(@"count:%d",count);
        }
        return customcell;
            
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
      
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        [cell.contentView addSubview:attendetailview];
        
        [self setupAttendeedetails:[attendeelist[dataIndex] valueForKey:@"id"] backcolor:cell.backgroundColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//     ln_expandedRowIndex=-1;
//    [Attendeelistview reloadData];
//}

//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    
//  //  ln_expandedRowIndex=0;
//    [Attendeelistview reloadData];
//}



#pragma mark edit activity method

-(void) setupAttendeedetails:(NSString *)attend_id backcolor:(UIColor *)backcolor
{
    
    [self.attendetailview.emailaddress_ulbl setText:[helper gettranslation:@"LBL_119"]];
    [self.attendetailview.contactno_ulbl setText:[helper gettranslation:@"LBL_070"]];
    [self.attendetailview.institution_ulbl setText:[helper gettranslation:@"LBL_147"]];
    [self.attendetailview.subtargetclass_ulbl setText:[helper gettranslation:@"LBL_254"]];
    [self.attendetailview.integrationsrc_ulbl setText:[helper gettranslation:@"LBL_148"]];
    [self.attendetailview.assignedtopostn_ulbl setText:[helper gettranslation:@"LBL_321"]];
    [self.attendetailview.id_ulbl setText:[helper gettranslation:@"LBL_825"]];
    
    
    
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"id like %@",attend_id];
    NSPredicate *postnPredicate = [NSPredicate predicateWithFormat:@"attendeeID like %@",attend_id];
    attdata = [MasterAttendeeDetail filteredArrayUsingPredicate:aPredicate];
    if ([attdata count]>0)
    {
        [self.attendetailview.attendee_emailaddress setText:[attdata[0] valueForKey:@"emailaddress"]];
        [self.attendetailview.attendee_integrationsource setText:[attdata[0] valueForKey:@"integrationsource"]];
        [self.attendetailview.attendee_contactno setText:[attdata[0] valueForKey:@"contactnumber"]];
        
        
        
        //        NSArray *position = [helper query_alldata:@"AttendeePositions"];
        
        NSArray *attendeepostn = [AttendeePositions filteredArrayUsingPredicate:postnPredicate];
        NSString *postnstring;
        
        for (int i=0; i<attendeepostn.count; i++)
        {
            
            if (postnstring.length >0)
            {
                postnstring = [NSString stringWithFormat:@"%@ | %@",postnstring,[attendeepostn[i] valueForKey:@"positionName"]];
            }
            else
            {
                postnstring = [NSString stringWithFormat:@"%@ ",[attendeepostn[i] valueForKey:@"positionName"]];
            }
        }
        
        
        //        [self.attendetailview.attendee_targetclass setText:[attdata[0] valueForKey:@"targetclasslic"]];
        
        
        [self.attendetailview.attendee_targetclass setText:postnstring];
        
        
        
        if ([attdata[0] valueForKey:@"imageid"])
        {
            [self.attendetailview.attendee_profileimg setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[attdata[0] valueForKey:@"imageid"]]]]];
        }
        else
        {
            [self.attendetailview.attendee_profileimg setImage:[UIImage imageNamed: @"User.png"]];
        }
        
        self.attendetailview.Institution.text = [attdata[0] valueForKey:@"institution"];
        self.attendetailview.SubTargetClass.text = [attdata[0] valueForKey:@"subTargetClass"];
        self.attendetailview.Id.text=[attdata[0] valueForKey:@"id"];
        
    }
}


//---------------------Selection Functions start here

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    if (indexPath.row % 2)
//    {
//        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
//    }
//    else
//    {
//        cell.contentView.backgroundColor = [UIColor whiteColor] ;
//    }
    
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSInteger row = [indexPath row];
    BOOL preventReopen = NO;

    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
//        cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
        
    }
    [tableView endUpdates];
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    //    cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
}




-(IBAction)editactivityClicked:(UIButton *)sender
{
    if([appdelegate.lstpage isEqualToString:@"Y"])
    {
        [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:Attendeelistview];
        NSIndexPath *indexPath = [Attendeelistview indexPathForRowAtPoint:buttonPosition];
        if (indexPath != nil)
        {
            appdelegate.attendyid = [attendeelist[indexPath.row] valueForKey:@"id"];
        }
        
        NSLog(@"attendyid : %@",appdelegate.attendyid);
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self performSegueWithIdentifier:@"MasterAttendeetoeditattendee" sender:self];
        });
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@! %@",[helper gettranslation:@"LBL_808"],[helper gettranslation:@"LBL_628"]] action:[helper gettranslation:@"LBL_462"]];
    }
}


- (IBAction)addattendee_btn:(id)sender
{
    if([appdelegate.lstpage isEqualToString:@"Y"])
    {
        senderview = @"addattendee";
        appdelegate.attendyid = @"";
        [self performSegueWithIdentifier:@"MasterAttendeetoeditattendee" sender:self];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@! %@",[helper gettranslation:@"LBL_808"],[helper gettranslation:@"LBL_628"]] action:[helper gettranslation:@"LBL_462"]];
    }
}

- (IBAction)import_attendee_btn:(id)sender
{
    
    senderview = @"advancesearch";
    appdelegate.attendyid = @"";
    [self performSegueWithIdentifier:@"Attendeelist_to_AdvanceSearch" sender:self];
}

#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == AttendeeSearchBar)
    {
        if(count > searchText.length)
        {
            attendeelist = MasterAttendeeDetail;
            
        }
        if(searchText.length == 0)
        {
            attendeelist = MasterAttendeeDetail;
            
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            attendeelist = MasterAttendeeDetail;
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.ownedby contains[c] %@) OR (SELF.speciality contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[attendeelist filteredArrayUsingPredicate:predicate]];
            attendeelist = [filtereventarr copy];
        }
        ln_expandedRowIndex=-1;
        [Attendeelistview reloadData];
    }
}
-(void) refreshAttendylistview
{
    attendeelist = [helper query_alldata:@"Attendee"];

    [Attendeelistview reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"MasterAttendeetoeditattendee"])
    {
        AddAttendee *controller = segue.destinationViewController;
        controller.attendeeid = appdelegate.attendyid;
        NSPredicate *postnPredicate = [NSPredicate predicateWithFormat:@"id like %@",appdelegate.attendyid];
        NSArray *attendeedata = [attendeelist filteredArrayUsingPredicate:postnPredicate];
        controller.attendeelist = attendeedata;
        //controller.activity_id = sel_activityid;
        //controller.cancelSegue = @"unwindToEventActivitiesView";
    }
    else if ([[segue identifier] isEqualToString:@"EventSpeakerToDisplayAttachment"])
    {
    }
    else if ([[segue identifier] isEqualToString:@"Attendeelist_to_AdvanceSearch"])
    {
        AdvanceSearchView *controller = segue.destinationViewController;
        controller.cancelSegue = @"unwindToMasterAttendee";
        //        controller.activity_id = appdelegate.activityid;
        // NSLog(@"cancel segue value %@",controller.cancelSegue);
    }
    
}
#pragma mark unwind method
- (IBAction)unwindToMasterAttendee:(UIStoryboardSegue *)segue;
{
    RefreshTableview = 1;
    if ([senderview isEqualToString:@"advancesearch"])
    {
        NSPredicate *attendeepredicate = appdelegate.advsrcpredicate;
        
        if (attendeepredicate)
        {
            attendeelist = [helper query_alldata:@"Attendee"];
            _adv_screachbtn.hidden = NO;
            attendeelist = [attendeelist filteredArrayUsingPredicate:appdelegate.advsrcpredicate];
            Attendeelistview.delegate = self;
            Attendeelistview.dataSource = self;
            ln_expandedRowIndex=-1;
            [Attendeelistview reloadData];
        }
        else
        {
            NSLog(@"No Filter associated");
        }
        
    }
    else
    {
        ln_expandedRowIndex = -1;
        attendeelist = [helper query_alldata:@"Attendee"];
        Attendeelistview.delegate = self;
        Attendeelistview.dataSource = self;
        [Attendeelistview reloadData];
        
    }
    
    
}
#pragma mark menu button action
-(IBAction)HomeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"AllAttendeesToHome" sender:self];
}
-(IBAction)ActivityButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"AllAttendeesToActivities" sender:self];
}
- (IBAction)SpeakerButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"AllAttendeesToSpeaker" sender:self];
}
-(IBAction)ActivityPlanButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"AllAttendeesToActivityPlan" sender:self];
}
- (IBAction)CalendarButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"AllAttendeesToCalendar" sender:self];
}

- (IBAction)adv_screachbt:(id)sender
{
    _adv_screachbtn.hidden = YES;
    
    ln_expandedRowIndex = -1;
    appdelegate.advsrcpredicate = nil;
    attendeelist = [helper query_alldata:@"Attendee"];
    [Attendeelistview reloadData];

}

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}

-(void)checkProgressUpdate:(NSTimer *)timer
{
    
    
    if([appdelegate.lstpage isEqualToString:@"Y"])
    {
        [progressIndicator setHidden:YES];
        [_downloadProgress setHidden:YES];
        //        attendeelist=[helper query_alldata:@"Attendee"];
        //        [Attendeelistview reloadData];
        
    }
    else if([appdelegate.lstpage isEqualToString:@"N"] && [appdelegate.downloadInProgress isEqualToString:@"NO"])
    {
        //  attendeelist=[helper query_alldata:@"Attendee"];
        NSLog(@"AttendeeListCount:%lu",(unsigned long)attendeelist.count);
      
        // _downloadProgress.text=[NSString stringWithFormat:@"%u Completed",page/attendeelist.count];
        // [Attendeelistview reloadData];
        
    }
    
    
}
-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}
- (IBAction)salutationBtn:(id)sender {
    salutationFlag=!salutationFlag;
    if(salutationFlag)
    {
        attendeelist=[helper sortData:attendeelist colname:@"salutation" type:@"abc" Ascending:NO];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_salutationImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        attendeelist=[helper sortData:attendeelist colname:@"salutation" type:@"abc" Ascending:YES];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_salutationImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
}

- (IBAction)firstNameBtn:(id)sender {
    firstNameFlag=!firstNameFlag;
    if(firstNameFlag)
    {
        attendeelist=[helper sortData:attendeelist colname:@"firstname" type:@"abc" Ascending:NO];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_firstNameImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        attendeelist=[helper sortData:attendeelist colname:@"firstname" type:@"abc" Ascending:YES];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_firstNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    
}
- (IBAction)specialityBtn:(id)sender {
    specialityFlag=!specialityFlag;
    if(specialityFlag)
    {
        attendeelist=[helper sortData:attendeelist colname:@"speciality" type:@"abc" Ascending:NO];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_specialityImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
        
    }
    else
    {
        attendeelist=[helper sortData:attendeelist colname:@"speciality" type:@"abc" Ascending:YES];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_specialityImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
}

- (IBAction)targetClassBtn:(id)sender {
    targetClassFlag=!targetClassFlag;
    if(targetClassFlag)
    {
        attendeelist=[helper sortData:attendeelist colname:@"targetclass" type:@"abc" Ascending:NO];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_targetClassImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    else
    {
        attendeelist=[helper sortData:attendeelist colname:@"targetclass" type:@"abc" Ascending:YES];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_targetClassImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    
}

- (IBAction)statusBtn:(id)sender {
    statusFlag=!statusFlag;
    if(statusFlag)
    {
        attendeelist=[helper sortData:attendeelist colname:@"status" type:@"abc" Ascending:NO];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
        
    }
    else
    {
        attendeelist=[helper sortData:attendeelist colname:@"status" type:@"abc" Ascending:YES];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
}

- (IBAction)lastNameBtn:(id)sender {
    lastNameFlag=!lastNameFlag;
    if(lastNameFlag)
    {
        attendeelist=[helper sortData:attendeelist colname:@"lastname" type:@"abc" Ascending:NO];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_lastNameImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    else
    {
        attendeelist=[helper sortData:attendeelist colname:@"lastname" type:@"abc" Ascending:YES];
        ln_expandedRowIndex=-1;
        [self.Attendeelistview reloadData];
        [_lastNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
}

- (NSInteger)dataIndexForRowIndex:(NSInteger)row
{
    if (ln_expandedRowIndex != -1 && ln_expandedRowIndex <= row)
    {
        if (ln_expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}
@end
