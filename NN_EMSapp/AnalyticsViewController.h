//
//  AnalyticsViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/30/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ShinobiCharts/ShinobiCharts.h>
#import "AppDelegate.h"
#import "HelperClass.h"
#import "ToastView.h"

@class HelperClass;
@interface AnalyticsViewController : UIViewController<SChartDelegate,SChartDatasource>

@property (strong, nonatomic) IBOutlet UILabel *tittlebar_lbl;
- (IBAction)Graphtoggle_segment:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *EventAnalyticsTitleLBL;

@property (nonatomic,strong) HelperClass *Helper1;

@property (strong, nonatomic) IBOutlet UISegmentedControl *Graphtoggle_segment;
@property (strong, nonatomic) IBOutlet UIView *shinobigraph_view;
- (IBAction)cancel_btn:(id)sender;

@property (strong, nonatomic) NSString *event_id;

@end
