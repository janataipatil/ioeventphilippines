//
//  AnalyticsViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/30/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "AnalyticsViewController.h"

@interface AnalyticsViewController ()//<SChartDatasource,SChartDelegate>
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;

}
@end

@implementation AnalyticsViewController
{
    NSArray *grid_data;
    NSMutableData *NSM_data;
    
    NSArray *TableData;
    NSArray *EventData;
    
    NSArray *piedata_activities;
    NSArray *piedata_attendees;
    NSArray *piedata_emailcomm;
    
    NSDictionary *columndata;
    NSMutableArray* Column[1];
    
    NSMutableArray *colorAssignmentArray;  // sidu: This Array holds color of pie-charts
    NSMutableArray *selectColorAssignmentArray;//This Array holds the selection color of pie-charts
    
    NSMutableDictionary *pieactdata_percent;
    NSMutableDictionary *pieactdata_values;
    
    NSMutableDictionary *pieattdata_percent;
    NSMutableDictionary *pieattdata_values;
    
    NSMutableDictionary *pieecomdata_percent;
    NSMutableDictionary *pieecomdata_values;
    
    
    NSMutableDictionary *piedata_percent;
    NSMutableDictionary *piedata_values;
    
//    NSDictionary *columndata;
    
    ShinobiChart* columnchart;
    ShinobiChart* piechart;
    ShinobiChart* piechart_activities;
    ShinobiChart* piechart_attendees;
    ShinobiChart* piechart_emailcomm;
    
    UILabel *selectedPieLabel;
    NSPredicate *act_predicate;
    NSPredicate *att_predicate;
    NSPredicate *ecom_predicate;
    
    UIColor *Bluecol, *sBluecol;
    UIColor *Redcol, *sRedcol;
    UIColor *Greencol, *sGreencol;
    UIColor *Purplecol, *sPurplecol;
    UIColor *Orangecol, *sOrangecol;
    UIColor *Greycol, *sGreycol;
    UIColor *Mustardcol, *sMustardcol;
    UIColor *DarkGreencol, *sDarkGreencol;
    
    NSString *chart_title;
   
}
@synthesize Helper1,Graphtoggle_segment;

- (void)viewDidLoad
{
    [super viewDidLoad];
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    Helper1 = [[HelperClass alloc] init];
    
    //Colors that are used in Pie-charts are defined here.
    
    
    Bluecol = [UIColor colorWithRed:40/255.0 green:95/255.0 blue:143/255.0 alpha:1];
    Redcol = [UIColor colorWithRed:200/255.0 green:48/255.0 blue:44/255.0 alpha:1];
    Greencol = [UIColor colorWithRed:56/255.0 green:165/255.0 blue:56/255.0 alpha:1];
    Purplecol = [UIColor colorWithRed:83/255.0 green:141/255.0 blue:184/255.0 alpha:1];
    Orangecol = [UIColor colorWithRed:255/255.0 green:102/255.0 blue:0/255.0 alpha:1];
    Greycol = [UIColor colorWithRed:107/255.0 green:107/255.0 blue:107/255.0 alpha:1];
    Mustardcol = [UIColor colorWithRed:229/255.0 green:184/255.0 blue:0/255.0 alpha:1];
    DarkGreencol = [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:1];
    
    sBluecol = [UIColor colorWithRed:40/255.0 green:95/255.0 blue:143/255.0 alpha:0.8];
    sRedcol = [UIColor colorWithRed:200/255.0 green:48/255.0 blue:44/255.0 alpha:0.8];
    sGreencol = [UIColor colorWithRed:56/255.0 green:165/255.0 blue:56/255.0 alpha:0.8];
    sPurplecol = [UIColor colorWithRed:83/255.0 green:141/255.0 blue:184/255.0 alpha:0.8];
    sOrangecol = [UIColor colorWithRed:255/255.0 green:102/255.0 blue:0/255.0 alpha:0.8];
    sGreycol = [UIColor colorWithRed:107/255.0 green:107/255.0 blue:107/255.0 alpha:0.8];
    sMustardcol = [UIColor colorWithRed:229/255.0 green:184/255.0 blue:0/255.0 alpha:0.8];
    sDarkGreencol = [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:0.8];

    
    TableData = [Helper1 query_alldata:@"Event_Analytics"];
    TableData = [TableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.eventid like %@",_event_id]];
    
    
    _EventAnalyticsTitleLBL.text=[Helper1 gettranslation:@"LBL_129"];
    
    //eventid
    appdelegate.translations = [Helper1 query_alldata:@"Lang_translations"];
    
    EventData = [[Helper1 query_alldata:@"Event"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.eventid like %@",_event_id]];
    
    act_predicate = [NSPredicate predicateWithFormat:@"self.entityname like 'Activity'"];
    att_predicate = [NSPredicate predicateWithFormat:@"self.entityname like 'Attendee'"];
    ecom_predicate = [NSPredicate predicateWithFormat:@"self.entityname like 'EmailComm'"];

    //    NSPredicate *budget_predicate = [NSPredicate predicateWithFormat:@"self.EntityName like Activity"];
    
    piedata_percent = [[NSMutableDictionary alloc] init];
    piedata_values = [[NSMutableDictionary alloc] init];
    
    [self fetch_columnchartdata];
//    
//    [self fetch_piechartdata:@"Activity"];
//    [self setPieChart];

    [Graphtoggle_segment setSelectedSegmentIndex:0];
    [Graphtoggle_segment sendActionsForControlEvents:UIControlEventValueChanged];
    
    //Trasnlation for segment controller
    [Graphtoggle_segment setTitle:[Helper1 gettranslation:@"LBL_003"] forSegmentAtIndex:0];
    [Graphtoggle_segment setTitle:[Helper1 gettranslation:@"LBL_056"] forSegmentAtIndex:1];
    [Graphtoggle_segment setTitle:[Helper1 gettranslation:@"LBL_818"] forSegmentAtIndex:2];
    [Graphtoggle_segment setTitle:[Helper1 gettranslation:@"LBL_817"] forSegmentAtIndex:3];

    
    [self setPieChart];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)Graphtoggle_segment:(id)sender
{
    switch (self.Graphtoggle_segment.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"segment index is %ld",(long)self.Graphtoggle_segment.selectedSegmentIndex);
            [self fetch_piechartdata:@"Activity"];
            [columnchart removeFromSuperview];
            [self setPieChart];
            
            selectedPieLabel.hidden = YES;
            
//            [piechart reloadData];
//            [piechart redrawChart];
            
           // [self setPieChart];
            
            break;
            
        case 1:
            NSLog(@"segment index is %ld",(long)self.Graphtoggle_segment.selectedSegmentIndex);
            
            [self fetch_piechartdata:@"Attendee"];
            selectedPieLabel.hidden = YES;
            [columnchart removeFromSuperview];
            
//            [piechart reloadData];
//            [piechart redrawChart];
            
            [self setPieChart];
            
            break;
            
        case 2:
            NSLog(@"segment index is %ld",(long)self.Graphtoggle_segment.selectedSegmentIndex);
            
            
            [self fetch_piechartdata:@"EmailComm"];
            selectedPieLabel.hidden = YES;
            [columnchart removeFromSuperview];
                [self setPieChart];
//            [piechart reloadData];
//            [piechart redrawChart];
            
            
            break;
            
        case 3:
            NSLog(@"segment index is %ld",(long)self.Graphtoggle_segment.selectedSegmentIndex);
            [piechart removeFromSuperview];
            
            [self fetch_columnchartdata];
            [self setColumnChart];
            
            
            break;
            
        default:
            break;
    }

    
}

-(void) setPieChart
{
    [piechart removeFromSuperview];
    
    CGFloat margin_pie = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? 0.0 : 5.0;
    piechart = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.shinobigraph_view.bounds, margin_pie, margin_pie)];
    piechart.autoresizingMask =  ~UIViewAutoresizingNone;
    
    piechart.datasource = self;
    piechart.delegate = self;
    
    piechart.legend.hidden = NO;
    piechart.legend.placement = SChartLegendPlacementInsidePlotArea;
    piechart.legend.position = SChartLegendPositionTopRight;
    piechart.legend.style.font = [UIFont fontWithName:@"HelveticaNeue" size:14.f];
    piechart.legend.style.fontColor = [UIColor blackColor];
    
    
    selectedPieLabel = [[UILabel alloc] initWithFrame:CGRectMake(310, 165, 120, 50)];
    selectedPieLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.f];
    selectedPieLabel.textColor = [UIColor colorWithRed:44/255.0 green:83/255.0 blue:153/255.0 alpha:1];
    selectedPieLabel.textAlignment =  NSTextAlignmentCenter;
    selectedPieLabel.backgroundColor = [UIColor clearColor];
    selectedPieLabel.text = @"";
    selectedPieLabel.numberOfLines = 2;
    selectedPieLabel.adjustsFontSizeToFitWidth = NO;
    
    [self.shinobigraph_view  addSubview:piechart];
    [piechart addSubview:selectedPieLabel];
    
}
-(void)fetch_piechartdata:(NSString *)entityname
{
    [piedata_percent removeAllObjects];
    [piedata_values removeAllObjects];
    
    
    if ([entityname isEqualToString:@"Activity"])
    {
        
        colorAssignmentArray=[[NSMutableArray alloc]init];
        selectColorAssignmentArray=[[NSMutableArray alloc]init];

        
        piedata_activities = [TableData filteredArrayUsingPredicate:act_predicate];

       /// piedata_activities = [EventData filteredArrayUsingPredicate:act_predicate];
        for (int i=0; i< piedata_activities.count; i++)
        {
            
            NSString *Label = [piedata_activities[i] valueForKey:@"label"];
            
            int value = [[piedata_activities[i] valueForKey:@"value"] intValue];
            
            [piedata_percent setValue:[NSNumber numberWithFloat:value] forKey:Label];
            [piedata_values setValue:[NSNumber numberWithFloat:value]  forKey:Label];
            
           
        }
    }
    else if ([entityname isEqualToString:@"Attendee"])
    {
        colorAssignmentArray=[[NSMutableArray alloc]init];
        selectColorAssignmentArray=[[NSMutableArray alloc]init];
        
        piedata_attendees = [TableData filteredArrayUsingPredicate:att_predicate];
         //piedata_attendees = [EventData filteredArrayUsingPredicate:att_predicate];
        for (int i=0; i< piedata_attendees.count; i++)
        {
            NSString *Label = [piedata_attendees[i] valueForKey:@"label"];
            int value = [[piedata_attendees[i] valueForKey:@"value"] intValue];
            
            
            [piedata_percent setValue:[NSNumber numberWithFloat:value]  forKey:Label];
            [piedata_values setValue:[NSNumber numberWithFloat:value]  forKey:Label];
        
        }
    }
    else if ([entityname isEqualToString:@"EmailComm"])
    {
        colorAssignmentArray=[[NSMutableArray alloc]init];
        selectColorAssignmentArray=[[NSMutableArray alloc]init];

        
        piedata_emailcomm = [TableData filteredArrayUsingPredicate:ecom_predicate];
        
        //piedata_emailcomm = [EventData filteredArrayUsingPredicate:ecom_predicate];
        
        for (int i=0; i< piedata_emailcomm.count; i++)
        {
            
            NSString *Label = [piedata_emailcomm[i] valueForKey:@"label"];
            int value = [[piedata_emailcomm[i] valueForKey:@"value"] intValue];
            
            [piedata_percent setObject:[NSNumber numberWithFloat:value]  forKey:Label];
            [piedata_values setObject:[NSNumber numberWithFloat:value]  forKey:Label];
            
            
        }
    }

}

-(void) setColumnChart
{
    CGFloat margin = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? 0.0 : 5.0;
    columnchart = [[ShinobiChart alloc] initWithFrame:CGRectInset(_shinobigraph_view.bounds, margin, margin)];
    
    
    columnchart.autoresizingMask =  ~UIViewAutoresizingNone;
    columnchart.datasource = self;
    
    // add a pair of axes
    SChartCategoryAxis *ColumnxAxis = [[SChartCategoryAxis alloc] init];
    
    columnchart.xAxis = ColumnxAxis;
    ColumnxAxis.style.titleStyle.textColor = [UIColor blackColor];
    ColumnxAxis.style.titleStyle.font =[UIFont fontWithName:@"HelveticaNeue-Medium"size:11.f];
    ColumnxAxis.style.lineColor = [UIColor blackColor];
    
    SChartAxis *ColumnyAxis = [[SChartNumberAxis alloc] init];
    
    columnchart.yAxis = ColumnyAxis;
    ColumnyAxis.style.lineColor = [UIColor blackColor];
    
    
    ColumnyAxis.style.majorTickStyle.tickLabelOrientation = TickLabelOrientationVertical;
    
    ColumnyAxis.style.titleStyle.textColor = [UIColor blackColor];
    ColumnyAxis.style.titleStyle.font =[UIFont fontWithName:@"HelveticaNeue-Bold"size:11.f];
    
    
    columnchart.backgroundColor = [UIColor whiteColor];
    columnchart.plotAreaBackgroundColor = [UIColor clearColor];
    columnchart.gestureDoubleTapEnabled = YES;
    columnchart.gestureDoubleTapResetsZoom = YES;
    columnchart.yAxis.enableGesturePanning = YES;
    columnchart.yAxis.enableGestureZooming = YES;
    columnchart.xAxis.enableGesturePanning = YES;
    columnchart.xAxis.enableGestureZooming = YES;
    
    NSLocale *theLocale = [NSLocale currentLocale];
    NSString *currencySymbol = [theLocale objectForKey:NSLocaleCurrencySymbol];
    
    
    columnchart.yAxis.title =[NSString stringWithFormat:@"%@ %@",[Helper1 gettranslation:@"LBL_820"],currencySymbol];
    
    columnchart.xAxis.style.majorTickStyle.labelColor = [UIColor blackColor];
    
    columnchart.yAxis.style.majorTickStyle.labelColor = [UIColor clearColor];
    
    columnchart.xAxis.style.majorTickStyle.labelFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];
    
    // add to the view
    [_shinobigraph_view addSubview:columnchart];
    
    

}

-(void)fetch_columnchartdata
{
    
    Column[0] = [[NSMutableArray alloc]init];
    columndata = [[NSMutableDictionary alloc]init];
    
    int actualcost = [[EventData[0] valueForKey:@"actualcost"] integerValue];
    int approvedcost = [[EventData[0] valueForKey:@"approvedcost"] integerValue];
    int remaining_budget = [[EventData[0] valueForKey:@"approvedcost"] integerValue] - [[EventData[0] valueForKey:@"actualcost"] integerValue];
    
    
    //columndata = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:actualcost], @"Actual Cost",[NSNumber numberWithInteger:approvedcost], @"Approved Cost", [NSNumber numberWithInteger:remaining_budget], @"Remaining Budget",nil];
    
    columndata = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:actualcost],[NSString stringWithFormat:@"2%@",[Helper1 gettranslation:@"LBL_400"]],[NSNumber numberWithInteger:approvedcost],[NSString stringWithFormat:@"1%@",[Helper1 gettranslation:@"LBL_819"]], [NSNumber numberWithInteger:remaining_budget],[NSString stringWithFormat:@"3%@",[Helper1 gettranslation:@"LBL_211"]],nil];
    
    
    NSArray *colsortedKeys = [[NSArray alloc]init];
    //colsortedKeys = [columndata allKeys];
    
    colsortedKeys = [[columndata allKeys] sortedArrayUsingSelector: @selector(compare:)];
    
    for (NSString *key in colsortedKeys)
    {
        SChartDataPoint* dataPoint = [SChartDataPoint new];
      //  dataPoint.xValue = key;
         dataPoint.xValue = [key substringFromIndex:1];
        dataPoint.yValue = [columndata valueForKey:key];
        
        [Column[0] addObject:dataPoint];
    }

    
}


-(NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart
{
    if (chart == piechart)
    {
        return 1;
    }
    else if (chart == columnchart)
    {
        return 1;
    }
    return 0;
}
-(NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    if (chart == piechart)
    {
        return piedata_percent.allKeys.count;
    }
    else if (chart == columnchart)
    {
       return Column[seriesIndex].count;
    }
    
    return 0;
}
-(id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex
{
    if (chart == piechart)
    {
        SChartRadialDataPoint *datapoint = [[SChartRadialDataPoint alloc] init];
        NSString* key = piedata_percent.allKeys[dataIndex];
        datapoint.name = key;
        datapoint.value = piedata_percent[key];
        
        return datapoint;
    }
    else if (chart == columnchart)
    {
        SChartDataPoint *datapoint = [[SChartDataPoint alloc] init];
        datapoint.xValue = [Column[seriesIndex][dataIndex] xValue] ;
        datapoint.yValue = [Column[seriesIndex][dataIndex] yValue] ;
        
        return datapoint;
    }
    return nil;
}

-(SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index
{
    if (chart == piechart)
    {
        SChartDonutSeries *pieSeries = [[SChartDonutSeries alloc] init];
        
        //pieSeries.style.chartEffect = SChartRadialChartEffectRoundedLight;
        pieSeries.style.dataPointLabelStyle.textColor = [UIColor blackColor];
        pieSeries.style.dataPointLabelStyle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];
        

        colorAssignmentArray = [[NSMutableArray alloc] init];
        selectColorAssignmentArray = [[NSMutableArray alloc] init];
        
        NSArray *allKeys = piedata_values.allKeys;
        
        for(int i=0;i<allKeys.count;i++)
        {
            
            NSString* Label = [allKeys objectAtIndex:i];
            
            NSString *englishLabel=[self getLICValue:Label];
            
            if([englishLabel isEqualToString:@"Completed"])
            {
                [colorAssignmentArray addObject:Bluecol];
                [selectColorAssignmentArray addObject:sBluecol];
            }
            else if ([englishLabel isEqualToString:@"Assigned"])
            {
                [colorAssignmentArray addObject:Greencol];
                [selectColorAssignmentArray addObject:sGreencol];
            }
            else if ([englishLabel isEqualToString:@"InProgress"])
            {
                [colorAssignmentArray addObject:Orangecol];
                [selectColorAssignmentArray addObject:sOrangecol];
            }
            else if ([englishLabel isEqualToString:@"Planned"])
            {
                [colorAssignmentArray addObject:Mustardcol];
                [selectColorAssignmentArray addObject:sMustardcol];
            }
            else if([englishLabel isEqualToString:@"Accepted"])
            {
                [colorAssignmentArray addObject:Bluecol];
                [selectColorAssignmentArray addObject:sBluecol];
            }
            else if ([englishLabel isEqualToString:@"Declined"])
            {
                [colorAssignmentArray addObject:Redcol];
                [selectColorAssignmentArray addObject:sRedcol];
            }
            else if ([englishLabel isEqualToString:@"Invited"])
            {
                [colorAssignmentArray addObject:DarkGreencol];
                [selectColorAssignmentArray addObject:sDarkGreencol];
            }
            else if ([englishLabel isEqualToString:@"Proposed"])
            {
                [colorAssignmentArray addObject:Orangecol];
                [selectColorAssignmentArray addObject:sOrangecol];
            }
            else if ([englishLabel isEqualToString:@"Approved"])
            {
                [colorAssignmentArray addObject:Greencol];
                [selectColorAssignmentArray addObject:sGreencol];
            }
            else if ([englishLabel isEqualToString:@"Tentative"])
            {
                [colorAssignmentArray addObject:Purplecol];
                [selectColorAssignmentArray addObject:sPurplecol];
            }
            else if([englishLabel isEqualToString:@"EMAIL_QUEUED"])
            {
                [colorAssignmentArray addObject:Orangecol];
                [selectColorAssignmentArray addObject:sOrangecol];
            }
            else if ([englishLabel isEqualToString:@"EMAIL_DELIVERED"])
            {
                [colorAssignmentArray addObject:Mustardcol];
                [selectColorAssignmentArray addObject:sMustardcol];
            }
            else if ([englishLabel isEqualToString:@"EMAIL_OPENED"])
            {
                [colorAssignmentArray addObject:Greencol];
                [selectColorAssignmentArray addObject:sGreencol];
            }
            else if([englishLabel isEqualToString:@"Sent"])
            {
                [colorAssignmentArray addObject:Purplecol];
                [selectColorAssignmentArray addObject:sPurplecol];
            }
            else if ([englishLabel isEqualToString:@"EMAIL_FAILED"])
            {
                [colorAssignmentArray addObject:Redcol];
                [selectColorAssignmentArray addObject:sRedcol];
            }
            else if ([englishLabel isEqualToString:@"EMAIL_SPAM"])
            {
                [colorAssignmentArray addObject:Greycol];
                [selectColorAssignmentArray addObject:sGreycol];
            }
            
            
        }
        
        
        //pieSeries.selectedStyle.chartEffect = SChartRadialChartEffectRounded;
        
        pieSeries.selectedStyle.protrusion = 15.0f;
        pieSeries.selectionAnimation.duration = @1.5;
        pieSeries.selectedPosition = @0.0;
        
        pieSeries.style.labelFontColor = [UIColor blackColor];
        pieSeries.selectedStyle.labelFontColor = [UIColor blackColor];

        pieSeries.animationEnabled = YES;
        
        //Assign Color to the Pie-charts.

        [[pieSeries style] setFlavourColors:colorAssignmentArray];
        
        [[pieSeries selectedStyle]setFlavourColors:selectColorAssignmentArray];

        
        
        SChartAnimation *animation = [SChartAnimation growAnimation];
        animation.duration = @3;
        pieSeries.entryAnimation = animation;
        
        
        return pieSeries;
        
    }
    else if (chart == columnchart)
    {
        
        SChartColumnSeries *columnSeries = [[SChartColumnSeries alloc] init];
        
        
        columnSeries.selectionMode = SChartSelectionPoint;
        columnSeries.selectedStyle.showAreaWithGradient = NO;
        columnSeries.style.showAreaWithGradient = NO;
        
        columnSeries.style.areaColor =[UIColor colorWithRed:72/255.0 green:133/255.0 blue:237/255.0 alpha:1];
        columnSeries.selectedStyle.areaColor = [UIColor colorWithRed:72/255.0 green:133/255.0 blue:237/255.0 alpha:0.8];
        
        
        
        columnSeries.animationEnabled = YES;
        SChartAnimation *animation = [SChartAnimation growVerticalAnimation];
        animation.duration = @5;
        columnSeries.entryAnimation = animation;
        
        columnSeries.style.dataPointLabelStyle.showLabels = YES;
        
        //Position labels
        columnSeries.style.dataPointLabelStyle.offsetFromDataPoint = CGPointMake(0, 8);
        
        columnSeries.style.dataPointLabelStyle.textColor = [UIColor colorWithRed:51/255.0f green:76/255.0f blue:104/255.0f alpha:1] ;;
        columnSeries.style.dataPointLabelStyle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13.f];
        
        return columnSeries;
}
    return nil;
}

- (void)sChart:(ShinobiChart *)chart toggledSelectionForRadialPoint:(SChartRadialDataPoint *)dataPoint inSeries:(SChartRadialSeries *)series atPixelCoordinate:(CGPoint)pixelPoint
{
    
    NSLog(@"Time period Clicked: %@", dataPoint.name);
    NSString *dvalue = [piedata_values valueForKey:dataPoint.name];
    NSLog(@"Time period Clicked: %@", dvalue);
    selectedPieLabel.text = [NSString stringWithFormat:@"%@\n%@",dataPoint.name,dvalue];
}


- (IBAction)cancel_btn:(id)sender
{
    [self performSegueWithIdentifier:@"backtoHomescreen" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

-(NSString*)getLICValue:(NSString*)licstr
{
    NSString *str;
    NSArray *arr = [Helper1 query_alldata:@"Event_Analytics"];
    arr=[arr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.eventid like %@",_event_id]];
    arr=[arr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"label like %@",licstr]];
    
    str=[arr[0] valueForKey:@"labellic"];
    return str;
    
    
}

@end
