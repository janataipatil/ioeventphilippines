//
//  AnswerVC.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/16/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"

@interface AnswerVC : UIViewController

@property (nonatomic,strong) HelperClass *helper;

@property (strong, nonatomic) IBOutlet UITextView *answer;
@property (strong, nonatomic) IBOutlet UILabel *questionby;
@property (strong, nonatomic) IBOutlet UILabel *questionfor;
@property (strong, nonatomic) IBOutlet UILabel *answerby;

@property(retain,nonatomic)IBOutlet UIButton *saveButon;
@property(retain,nonatomic)IBOutlet UIButton *cancelButon;

- (IBAction)saveButon:(id)sender;
- (IBAction)cancelButon:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *answer_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *questionby_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *questionfor_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *answerby_ulbl;

@end
