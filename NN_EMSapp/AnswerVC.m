//
//  AnswerVC.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/16/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AnswerVC.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "SessionQ&AView.h"

@interface AnswerVC ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSPredicate *eventpredicate,*answeredpredicate,*pendingPredicate,*sessionpredicate;
    NSString *editflg;
    SLKeyChainStore *keychain;
    NSString *recordid;
    NSString *status;
    NSString *statuslic;
    NSArray *answerdata;
    UIViewController *currentTopVC;
    NSString *Starteditting;
    
    NSString *semp;
//    dispatch_semaphore_t sem_event;
}

@end

@implementation AnswerVC

@synthesize helper;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.id like %@",appdelegate.questionid];
    answerdata = [[helper query_alldata:@"Event_SessionQnA"] filteredArrayUsingPredicate:predicate];
    
    NSArray *sessionarray = [[helper query_alldata:@"Event_Session"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.id like %@",appdelegate.sessionid]] ;
    
    if ([sessionarray count]>0)
    {
        editflg = [sessionarray[0] valueForKey:@"canEdit"];
        NSString *forname =  [answerdata[0] valueForKey:@"question_forname"];
        [_answer setText:[answerdata[0] valueForKey:@"answer_text"]];
        [_questionby setText:[answerdata[0] valueForKey:@"question_byname"]];
        [_questionfor setText:forname];
        [_answerby setText:[answerdata[0] valueForKey:@"answer_by"]];
        statuslic = [answerdata[0] valueForKey:@"status"];
        recordid = [answerdata[0] valueForKey:@"id"];
    }
    

    
    Starteditting = @"NO";
    
    statuslic = @"";
//    recordid = @"";

    _answer_ulbl.text = [helper gettranslation:@"LBL_313"];
    _questionby_ulbl.text = [helper gettranslation:@"LBL_674"];
    _questionfor_ulbl.text = [helper gettranslation:@"LBL_516"];
    _answerby_ulbl.text = [helper gettranslation:@"LBL_043"];
    
    
    
    
    self.saveButon.hidden = YES;
    self.cancelButon.hidden = YES;
    NSLog(@"user role array %@",appdelegate.EventUserRoleArray);
    if (![appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
    {
        self.answer.userInteractionEnabled = NO;
    }
    else
    {
        self.answer.backgroundColor = [UIColor colorWithRed:245.0/255.0f green:245.0/255.0f blue:245.0/255.0f alpha:1.0f];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)saveButon:(id)sender
{
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TeamTableview];
//    NSIndexPath *indexPath = [TeamTableview indexPathForRowAtPoint:buttonPosition];
//    appdelegate.userid = [Teamlist[indexPath.row] valueForKey:@"userid"];
//    
//    
//    [self syncresponsewithserver:indexPath callfor:@"admindelegate"];
    [self saveQnAanswer];
    
}

- (IBAction)cancelButon:(id)sender
{
    if ([answerdata count]>0)
    {
        [_answer setText:[answerdata[0] valueForKey:@"answer_text"]];
    }
}


-(void) syncresponsewithserver:(NSIndexPath *) indexPath callfor:(NSString *)callfor
{
}
    
-(void) saveQnAanswer
{

    NSError *error;
//    [helper showWaitCursor:@"Updating Answer..."];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
    [JsonDict setValue:recordid forKey:@"ID"];
    [JsonDict setValue:_answer.text forKey:@"AnsText"];
    
    
    if (_answerby.text.length > 0)
    {
        [JsonDict setValue:_answerby.text forKey:@"AnsBy"];
    }
    else
    {
        [JsonDict setValue:[keychain stringForKey:@"username"] forKey:@"AnsBy"];
    }
    
    
    if (_answer.text.length > 0)
    {
        statuslic = @"Answered";
        status = [helper getvaluefromlic:@"SESS_QNA_STAT" lic:statuslic];
    }
    else
    {
        statuslic = @"Unanswered";
        status = [helper getvaluefromlic:@"SESS_QNA_STAT" lic:statuslic];
        
    }
    
    [JsonDict setValue:[keychain stringForKey:@"username"] forKey:@"AnsBy"];
    [JsonDict setValue:status forKey:@"Status"];
    [JsonDict setValue:statuslic forKey:@"StatusLIC"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    [JsonDict setValue:[dateFormatter stringFromDate:[NSDate date] ] forKey:@"AnsDate"];
    [JsonDict setValue:statuslic forKey:@"StatusLIC"];
    
    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArray options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetSessionQnA" pageno:@"" pagecount:@"" lastpage:@""];
    
    appdelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Set QnA Answer" entityname:@"Event SessionQnA"  Status:@"Open"];
    
//    NSManagedObjectContext  *context = [appdelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_SessionQnA" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.id like %@",recordid];
    
    //        NSError *error;
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if([fetchedObjects count]>0)
    {
        for (NSManagedObject *object in fetchedObjects)
        {
            
            [object setValue:[JsonDict valueForKey:@"AnsText"] forKey:@"answer_text"];
            [object setValue:[JsonDict valueForKey:@"AnsBy"] forKey:@"answer_by"];
            [object setValue:[JsonDict valueForKey:@"StatusLIC"] forKey:@"statuslic"];
            [object setValue:[JsonDict valueForKey:@"Status"] forKey:@"status"];
            [object setValue:[JsonDict valueForKey:@"AnsText"] forKey:@"answer_date"];
        }
        
        [context save:&error];
        
    }
    //        [helper removeWaitCursor];
    //    }
    
//    Teamlist = [helper query_alldata:@"Event_Team"];
//    Teamlist = [Teamlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
//    [TeamTableview reloadData];
    [_answer resignFirstResponder];
    
    self.saveButon.hidden = YES;
    self.cancelButon.hidden = YES;
    
    
    SessionQ_AView *parentvc = [[SessionQ_AView alloc]initWithNibName:@"SessionQ&AView" bundle:nil];
    [parentvc Refreshview];
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    
    self.saveButon.hidden = NO;
    self.cancelButon.hidden = NO;
}
-(void)textViewDidChange:(UITextView *)textView
{
    /*YOUR CODE HERE*/
}
-(void)textViewDidChangeSelection:(UITextView *)textView
{
    /*YOUR CODE HERE*/
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    /*YOUR CODE HERE*/
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{

    [self performSegueWithIdentifier:@"unwindToSessionQnAView" sender:self];
    
    appdelegate.sem = dispatch_semaphore_create(0);
    
    if ([_answer.text isEqualToString:[answerdata[0] valueForKey:@"answer_text"]])
    {
        dispatch_semaphore_signal(appdelegate.sem);
    }
    
    else
    {
        currentTopVC = [self currentTopViewController];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_584"] message:[helper gettranslation:@"LBL_586"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       self.saveButon.hidden = YES;
                                       self.cancelButon.hidden = YES;
                                       dispatch_semaphore_signal(appdelegate.sem);
                                       
                                   }];
        [alert addAction:okaction];
        
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           [textView becomeFirstResponder];
                                           [self textViewShouldBeginEditing:textView];
                                           dispatch_semaphore_signal(appdelegate.sem);
                                       }];
        
        [alert addAction:cancelaction];
        [currentTopVC presentViewController:alert animated:NO completion:nil];
    }
    
//    while (dispatch_semaphore_wait(appdelegate.sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    
    NSLog(@"Changes Saved");
    
    
    
}
- (UIViewController *)currentTopViewController
{
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

@end
