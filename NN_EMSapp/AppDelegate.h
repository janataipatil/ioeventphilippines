//
//  AppDelegate.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 8/30/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SLKeyChainStore.h"
#import <MicrosoftAzureMobile/MicrosoftAzureMobile.h>
#import <WindowsAzureMessaging/WindowsAzureMessaging.h>
#import "Reachability.h"
#import "HomeViewController.h"

#import <UserNotifications/UserNotifications.h>

@import GoogleMaps;
@import GooglePlaces;
@import UserNotifications;

@class HomeViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (weak, nonatomic) HomeViewController *homeviewController;

@property(strong,nonatomic)NSString *attendyid;
@property (strong,nonatomic) NSString *uniquetags;
@property (strong,nonatomic) NSString *Push_received;
@property (strong,nonatomic) NSString *EventApprovals;
@property (strong,nonatomic) NSString *logFilePath;
@property (strong,nonatomic) NSString *keychainName;



- (void)saveContext;
-(void) Register_pushnotification;
//-(void) disconnect_pushnotification;

- (NSURL *)applicationDocumentsDirectory;
@property (strong, nonatomic) MSClient *client;
@property(strong,nonatomic)NSString *eventid;
@property(strong,nonatomic)NSString *speakerid;
@property(strong,nonatomic)NSString *AttachmentType;
@property(strong,nonatomic)NSString *EventListViewType;
@property(strong,nonatomic)NSString *EventListFilter;
@property(strong,nonatomic) NSString *AttendeeAddType;
@property(strong,nonatomic)NSString *speakerListLastPage;
@property(strong,nonatomic)NSString *speakerListPageNo;
@property(strong,nonatomic)NSString *speakerListPageCount;
@property(strong,nonatomic)NSString *speakerDownloadInProgress;
@property(strong,nonatomic)NSPredicate *eventAdvanceSearch;
@property(strong,nonatomic)NSString *activityid;
@property(strong,nonatomic)NSString *planid;
@property(strong,nonatomic)NSString *approvalid;
@property(strong,nonatomic)NSString *sessionid;
@property(strong,nonatomic)NSString *surveyqnid;
@property(strong,nonatomic)NSString *surveyid;
@property(strong,nonatomic)NSString *questionid;
@property(strong,nonatomic)NSString *templatename;
@property(strong,nonatomic)NSString *downloadProgress;
@property(strong,nonatomic)NSString *lstpage;
@property(strong,nonatomic)NSString *pagecount;
@property(strong,nonatomic)NSString *pageno;
@property(strong,nonatomic) NSString *logout;
@property(strong,nonatomic)NSString *downloadInProgress;
@property(strong,nonatomic)NSString *navpurpose;
@property(strong,nonatomic)NSString *cancelclicked;
@property(strong,nonatomic)NSString *attendeeName;
//@property(strong,nonatomic)NSString *;

@property(strong,nonatomic)NSString *canEdit;
@property(strong,nonatomic)NSString *itinearySearch;
@property(strong,nonatomic)NSPredicate *advsrcpredicate;

@property(nonatomic,retain) NSArray *translations;
@property(nonatomic,retain) NSArray *backupTranslations;
@property(nonatomic,retain) NSString *itnearyid;
@property(nonatomic,retain) NSArray *ItnearyData;

@property(nonatomic,retain) NSPredicate *ItiniearySearch;
@property int selectedItineraraySegment;
@property(strong,nonatomic)NSString *swipevar,*filepath;
@property(strong,nonatomic)UIActivityIndicatorView *activityView;
@property(strong,nonatomic)UIView *loadingView;
@property(strong,nonatomic)NSString *currentscreen;
@property(strong,nonatomic)NSString *CurrencyCode;
@property(strong,nonatomic)NSString *eventStatus;
@property(strong,nonatomic)NSString *downloadinggalleryimages;
@property(strong,nonatomic)NSString *EventType,*EventSubType;
@property(assign)NSUInteger badgevalue;
@property(strong,nonatomic)NSString *PerformDeltaSync;
@property(strong,nonatomic)NSString *userid;
@property(strong,nonatomic)dispatch_semaphore_t sem;
@property(strong,nonatomic)NSString *senttransaction;
@property(strong,nonatomic)NSString *fdrstr;
@property(strong,nonatomic)NSString *ExpenseID;
@property(strong,nonatomic)NSArray *UserRolesArray;
@property(strong,nonatomic)NSArray *EventUserRoleArray;
@property(strong,nonatomic)NSString *eventTempStartDate;
@property(strong,nonatomic)NSString *eventTempEndDate;
@property(strong,nonatomic)NSString *EAlstpage;
@property(strong,nonatomic)NSString *EApageno;
@property(strong,nonatomic)NSString *EApagecount;
@property(strong,nonatomic)NSString *EAdownloadInProgress;
@property(strong,nonatomic)NSData *Devicetoken;
@property(strong,nonatomic)NSMutableData *ResponseData;
@property(strong,nonatomic)NSString *ProcessStatus;
@property(strong,nonatomic) NSString *DataDownloaded;

@property(strong,nonatomic)NSString *ProcessALERT;
@property(strong,nonatomic)NSString *ProcessBUTTON;
@end

