//
//  AppDelegate.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 8/30/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "AppDelegate.h"
#import "HelperClass.h"
#import "SignalR.h"
#import "SRConnection.h"
#import "EncryptedStore.h"

@interface AppDelegate ()
{
    SLKeyChainStore *keychain;
    HelperClass *helper;
    NSString *acktoken;
    dispatch_semaphore_t sem_att;
    NSString *languageCode;
}


@property (nonatomic, strong) SRHubConnection *hubConnection;
@property (nonatomic, strong) SRHubProxy *chat;

@end

@implementation AppDelegate

@synthesize uniquetags,eventid,speakerid,activityid,attendyid,swipevar,filepath,activityView,loadingView,currentscreen,sem,downloadinggalleryimages,PerformDeltaSync,senttransaction,fdrstr,EventType,EventSubType,planid,userid,approvalid,sessionid,ExpenseID,surveyid,surveyqnid,UserRolesArray,questionid,EventUserRoleArray,translations,ResponseData,cancelclicked,ProcessStatus,DataDownloaded,ProcessALERT,ProcessBUTTON,keychainName;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    // Override point for customization after application launch.
    
  //  keychainName = @"iWizards.Silverline-Client-QA";
    keychainName = @"iWizards.Silverline-Client-preprod";
    NSArray *logPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [logPath objectAtIndex:0];
    NSString *fileName =[NSString stringWithFormat:@"%@.log",@"IOEvents"];
    _logFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    //Set file path to appdelegate variable
    
    NSString *userdefaultstr = [[NSUserDefaults standardUserDefaults] valueForKey:@"version"];
    NSString *bundleversionstr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
       [[NSUserDefaults standardUserDefaults]setObject:bundleversionstr forKey:@"AppVersion"];
       [[NSUserDefaults standardUserDefaults]synchronize];
    
   keychain = [SLKeyChainStore keyChainStoreWithService:keychainName];
  
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"path here %@",[paths objectAtIndex:0]);
        translations = [[NSMutableArray alloc]init];
    
    [GMSServices provideAPIKey:@"AIzaSyDAghu1WHSYglNPBeFqx_i1seVV5MGyUdM"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyDAghu1WHSYglNPBeFqx_i1seVV5MGyUdM"];
//    [GMSPlacesClient provideAPIKey:@"AIzaSyDAghu1WHSYglNPBeFqx_i1seVV5MGyUdM"];
    self.itinearySearch = @"NO";
    self.downloadInProgress=@"NO";
    self.EAdownloadInProgress=@"NO";
    self.speakerDownloadInProgress=@"NO";
    self.selectedItineraraySegment=0;
    
    if ([userdefaultstr isEqualToString:bundleversionstr])
    {
        uniquetags = [keychain stringForKey:@"app_uniquetag"];
        NSString *checkappreset = [[NSUserDefaults standardUserDefaults]valueForKey:@"appreset"];
        if ([checkappreset isEqualToString:@"N"])
        {
//            [self setlaunchview];
        }
    }
    else
    {
        //NSString *Bundlename = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
        NSFileManager *localFileManager = [[NSFileManager alloc] init];
        NSString * rootDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
       // NSString *DatabasePath = [NSString stringWithFormat:@"%@/%@",rootDir,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]];
        NSURL *rootURL = [NSURL fileURLWithPath:rootDir isDirectory:YES];
        // NSURL *storeURL = [rootURL URLByAppendingPathComponent:@"IFSProjectApprovals.sqlite"];
        NSURL *storeURL = [rootURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]]];
        [localFileManager removeItemAtURL:storeURL error:NULL];
        uniquetags = [self generate_utag];
        keychain[@"app_uniquetag"] = uniquetags;
    }
    helper = [[HelperClass alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkForReachability) name:kReachabilityChangedNotification object:nil];
    
    _Push_received = @"NO";
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"SignalR_reconnect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self selector:@selector(onTick:) userInfo: nil repeats:YES];
    
    NSString *username=[keychain stringForKey:@"username"];
    NSString *password=[keychain stringForKey:@"password"];
    
    self.chat = [self.hubConnection createHubProxy:@"SL_Hub"];
    [self.chat on:@"slPUSH" perform:self selector:@selector(MessageReceived:)];

    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    /*if ([keychain stringForKey:@"username"] == nil && [keychain stringForKey:@"password"] == nil)
    {
         [self GetLoginLabelTranslations];
    }*/
    NSLog(@"userdefault value is %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"LanguageCode"]);
    NSString *languagecode = [helper getlanguageCode];
    
    if (![[[NSUserDefaults standardUserDefaults]valueForKey:@"LanguageCode"] isEqualToString:[helper getlanguageCode]])
    {
         [self GetLoginLabelTranslations];
    }
    
    
   
    /*
    if([username length]>0 && [password length]>0)
    {
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        
        if(![languageCode isEqualToString:[helper query_data:@"abc" inputdata:@"langCode" entityname:@"S_Config_Table"]])
            {
                [self GetLoginLabelTranslations];
            }
    }
    else
    {
        [self GetLoginLabelTranslations];
    }
    */
    
    
    self.lstpage=[[NSUserDefaults standardUserDefaults]valueForKey:@"lastPageStatus"];
    self.EAlstpage=[[NSUserDefaults standardUserDefaults]valueForKey:@"EAlastPageStatus"];
    
    return YES;
}
-(void)GetLoginLabelTranslations
{

    sem_att = dispatch_semaphore_create(0);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    

    //NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    //languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    languageCode = [helper getlanguageCode];
    
   // keychain[@"languageCode"]=languageCode;
    [helper insert_data:@"langCode" valuedata:languageCode entityname:@"S_Config_Table"];
    
   // NSString *Url = [NSString stringWithFormat:@"https://ioeventqa.azurewebsites.net/api/EMS_LoginLabels?LangCode=%@",languageCode];
  
//     NSString *Url = [NSString stringWithFormat:@"%@/api/EMS_LoginLabels?LangCode=%@",[helper geturl:@"UAT"],languageCode];
//     NSString *Url = [NSString stringWithFormat:@"%@/api/EMS_LoginLabels?LangCode=%@",[helper geturl:@"QA"],languageCode];
   // NSString *Url = [NSString stringWithFormat:@"%@/api/EMS_LoginLabels?LangCode=%@",[helper geturl:@"PREPROD"],languageCode];
   NSString *Url = [NSString stringWithFormat:@"%@/api/EMS_LoginLabels?LangCode=%@",[helper geturl:@"PROD"],languageCode];
  //  NSString *Url = [NSString stringWithFormat:@"%@/api/EMS_LoginLabels?LangCode=%@",[helper geturl:@"DEV"],languageCode];
    
    
    [request setURL:[NSURL URLWithString:Url]];
    [request setHTTPMethod:@"GET"];
    //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
   // [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn)
    {
        NSLog(@"Connection Successful");
    } else
    {
        NSLog(@"Connection could not be made");
    }
    
    while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
   // [helper removeWaitCursor];
    NSString *errormsg = [NSString stringWithFormat:@"%@",error];
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:errormsg preferredStyle:UIAlertControllerStyleAlert];
//    
//    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
//    
//    [self.window presentViewController:alert animated:true completion:nil];
    
    //[helper showalert:@"Alert" message:errormsg action:@"OK"];
    NSLog(@"Failed to get the login labels data %@",errormsg);
     dispatch_semaphore_signal(sem_att);
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    ResponseData = [NSMutableData dataWithData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *Error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:ResponseData options:kNilOptions error:&Error];
    if (jsonArray)
    {
//        [helper delete_alldata:@"Lang_translations"];
        [helper insertLanguageTranslatedLabels:jsonArray];
        //NSArray *Arr = [helper query_alldata:@"Lang_translations"];
        dispatch_semaphore_signal(sem_att);
        [[NSUserDefaults standardUserDefaults]setObject:languageCode forKey:@"LanguageCode"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else
    {
        NSLog(@"Did not get response for login translations API");
         dispatch_semaphore_signal(sem_att);
    }
    
}
- (void)applicationWillResignActive:(UIApplication *)application {

    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    [self.hubConnection stop:@1];
    
   [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    UIApplication *app = [UIApplication sharedApplication];
    UIBackgroundTaskIdentifier bgTask = 0;
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [self Register_pushnotification];
    
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.

    [self saveContext];
   
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.iWizards.NN_EMSapp" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"NN_EMSapp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    // Following Statement will be used to stop default function of WAL functionality of ios
    NSDictionary *options = @{NSSQLitePragmasOption:@{@"journal_mode":@"DELETE"}};
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"NN_EMSapp.sqlite"];
    NSError *error = nil;
    
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
   // NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    //encryted_core_data statement
    NSPersistentStoreCoordinator *coordinator = [EncryptedStore makeStore:[self managedObjectModel] passcode:uniquetags];
    
    if (!coordinator) {
        return nil;
    }
//    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
#pragma mark custom method
-(void) setlaunchview
{
    //replace and push rootview manually
    
    
    
//    if ( IDIOM == IPAD )
//    {
//        /* do something specifically for iPad. */
//        storyboard = [UIStoryboard storyboardWithName:@"Main_ipad" bundle:nil];
//    }
//    else
//    {
//        /* do something specifically for iPhone or iPod touch. */
//        storyboard = [UIStoryboard storyboardWithName:@"Main_iphone" bundle:nil];
//    }
    
    
    //replace and push rootview manually
    
//    LoginViewController  *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
//    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:lvc];
//    navigationController.navigationBarHidden = YES;
//    self.window.rootViewController =nil;
//    self.window.rootViewController = navigationController;
//    [self.window makeKeyAndVisible];
    
    
}
-(NSString *) generate_utag
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *generatestr = [NSMutableString stringWithCapacity:10];
    for (NSUInteger i= 0U; i<10; i++)
    {
        u_int32_t random = arc4random() % [letters length];
        unichar character = [letters characterAtIndex:random];
        [generatestr appendFormat:@"%C",character];
    }
  
    
    return generatestr;
    
}

-(void) checkForReachability
{
    helper = [[HelperClass alloc]init];
    [helper Reachabilitychanged];
    

}


// ------- ------- Custom Methods for Push Notificsations --------- -----------

-(void) Register_pushnotification
{
    
    keychain  = [SLKeyChainStore keyChainStoreWithService:keychainName];
    
    helper = [[HelperClass alloc]init];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"SignalR_reconnect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *custid = [helper query_data:@"name" inputdata:@"customerid" entityname:@"S_Config_Table"];
    NSString *pushuniquetags = [keychain stringForKey:@"app_uniquetag"];
    NSString *apptoken = [keychain stringForKey:@"SLToken"];
    NSString *appid = @"EMS_M_100";

   // self.hubConnection = [SRHubConnection connectionWithURL:@"https://nneventdev.azurewebsites.net"];  //Dev Signal R Conn
//    self.hubConnection = [SRHubConnection connectionWithURL:@"https://nneventqa.azurewebsites.net"]; //QA Signal R Conn
//    self.hubConnection = [SRHubConnection connectionWithURL:@"https://nneventstg.azurewebsites.net"]; //Stage Signal R Conn
    
   // self.hubConnection = [SRHubConnection connectionWithURL:@"https://ioeventuat.azurewebsites.net"]; //UAT Signal R Conn
//    self.hubConnection = [SRHubConnection connectionWithURL:[helper geturl:@"UAT"]];
//    self.hubConnection = [SRHubConnection connectionWithURL:[helper geturl:@"QA"]];
  // self.hubConnection = [SRHubConnection connectionWithURL:[helper geturl:@"PREPROD"]];
   self.hubConnection = [SRHubConnection connectionWithURL:[helper geturl:@"PROD"]];
   // self.hubConnection = [SRHubConnection connectionWithURL:[helper geturl:@"DEV"]];
   // self.hubConnection = [SRHubConnection connectionWithURL:@"https://ioeventqa.azurewebsites.net"];
    
    [self.hubConnection addValue:pushuniquetags forHTTPHeaderField:@"TAG"];
    [self.hubConnection addValue:appid forHTTPHeaderField:@"APPID"];
    [self.hubConnection addValue:apptoken forHTTPHeaderField:@"X-ZUMO-AUTH"];
    [self.hubConnection addValue:[keychain stringForKey:@"username"] forHTTPHeaderField:@"USERNAME"];
    [self.hubConnection addValue:custid forHTTPHeaderField:@"CUSTOMERID"];
    
    self.chat = [self.hubConnection createHubProxy:@"SL_Hub"];
    
    [self.chat on:@"slPUSH" perform:self selector:@selector(MessageReceived:)];
    
    [self.hubConnection start];
    

    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
     {
         if( !error )
         {
             [[UIApplication sharedApplication] registerForRemoteNotifications];  // required to get the app to do anything at all about push notifications
             NSLog( @"Push registration success." );
         }
         else
         {
             NSLog( @"---------- Push registration FAILED  ----------" );
             NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
             NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
         }  
     }];

//    UNNotificationSetting *settings = [UNNotificationSetting settingsForTypes:UIUserNotificationTypeSound |  UIUserNotificationTypeAlert | UIUserNotificationTypeBadge categories:nil];
//    
//    
//    [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
//        settings.alertStyle == UNAlertStyleNone
//    }]
//    
//    
//    UNUserNotificationCenter.currentNotificationCenter().getNotificationSettingsWithCompletionHandler{ (mySettings) in  mySettings.alertStyle == .None }
//    
//    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//    [[UIApplication sharedApplication] registerForRemoteNotifications];
//    
    [[NSUserDefaults standardUserDefaults]setObject:@"Y" forKey:@"Pushregister"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"SignalR_reconnect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken
{
    keychain  = [SLKeyChainStore keyChainStoreWithService:keychainName];
  
    helper = [[HelperClass alloc]init];
    
    //QA Signal R Conn
//    SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://nnemsdev.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=r1RN4Uvgsc3IdVjaG8HUdQoyEJePFAEIZBzjK8qa8nE=" notificationHubPath:@"nnemsdev_app"];
    
    
   // UAT Signal R Conn
   // SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://ioeventnphub.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=X3HRmisgwhpMvF8ge71ZA8s5OuiPMOgXfpajbMoYn6s=" notificationHubPath:@"ioevent_app"];
    
    
    
   // PREPROD Signal R Conn
  //  SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://ioeventshubstaging.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=Jzrf7huIoi12u+C6geiF1eY6brMeLqLzbwwKwThz3oc=" notificationHubPath:@"ioevents_app"];
    
    
    //PROD Signal R Conn
    SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://ioeventshubprod.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=YFVptRJzbRKcmCG2t3zcdRgPZYJx2sQOdc6DCRdxrcM=" notificationHubPath:@"ioevents_app"];
    
    
    
    
    
    NSString *uniquetag = [keychain stringForKey:@"app_uniquetag"];
    NSArray *atag = @[uniquetag];
    _Devicetoken = deviceToken;

    NSSet *tag = [NSSet setWithArray:atag];
//    NSLog(@"tag is:%@",tag);
    [hub registerNativeWithDeviceToken:deviceToken tags:tag completion:^(NSError* error)
     
     {
         if (error != nil)
         {
             NSString *errormsg = [NSString stringWithFormat:@"%@",error];
             [self MessageBox:@"Push Registration" message:errormsg];
         }
         else
         {
             [self MessageBox:@"Push Registration" message:@"Silverline Push Message Registration successful"];
         }
     }];
}

-(void)MessageBox:(NSString *)title message:(NSString *)messageText
{
    
    helper = [[HelperClass alloc] init];
    
//    [helper Insert_PushNotify:messageText message_sender:@"Application" message_entity:@"APP" message_type:title message_id:[helper generate_rowId] meassage_read:@"N"];
    [self.homeviewController updatenotificationbadge];
    
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UNNotificationRequest *)notification
{
    NSLog(@"---checkpoint 1");
    NSString *alertmessage;
    NSPredicate *npredicate = [NSPredicate predicateWithFormat:@"self.meassage_read like %@",@"N"];
    NSArray *pushdata = [[[helper query_alldata:@"Push_Notification_Table"] filteredArrayUsingPredicate:npredicate] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.message_type like %@",@"Delta"]];
    
    if (pushdata.count == 0)
    {
        alertmessage = [NSString stringWithFormat:@"%lu Unread Messages",(unsigned long)[[[helper query_alldata:@"Push_Notification_Table"] filteredArrayUsingPredicate:npredicate] count]];
    }
    else if (pushdata.count == 1)
    {
        alertmessage = @"New update available on server. \n Please perform delta sync";
    }
    else if (pushdata.count > 1)
    {
        alertmessage = @"Multiple updates available on server. \n Please perform delta sync";
    }
    
    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_461"] message:alertmessage delegate:nil cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil, nil];
    [notificationAlert show];
    
    // NSLog(@"didReceiveLocalNotification");
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
//{
//    NSString *stringURl = /* some URL */;
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{

    
//    [self Register_pushnotification];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
//    ^{
//        NSString *pushtype = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"Type"]];
//        NSLog(@"pushtype: %@",pushtype);
//        
//        helper = [[HelperClass alloc]init];
//        
//        if ([pushtype isEqualToString:@"INFO"])
//        {
//            _Push_received = @"YES";
//            acktoken = [userInfo objectForKey:@"PushToken"];
//            /*
//             [self send_appack:[NSString stringWithFormat:@"%@",[userInfo objectForKey:@"PushToken"]] log_mgs:nil];
//             
//             dispatch_async(dispatch_get_main_queue(),
//             ^{
//             [helper getpushmessage];
//             });
//             */
//        }
//
//        
//    dispatch_sync(dispatch_get_main_queue(), ^(void)
//        {
//            //update ui on main thread
//        });
//    });
    
    
    
    
    NSString *pushtype = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"Type"]];
    NSLog(@"pushtype: %@",pushtype);
    
    helper = [[HelperClass alloc]init];

    if ([pushtype isEqualToString:@"INFO"])
    {
        _Push_received = @"YES";
        acktoken = [userInfo objectForKey:@"PushToken"];
        /*
        [self send_appack:[NSString stringWithFormat:@"%@",[userInfo objectForKey:@"PushToken"]] log_mgs:nil];
        
        dispatch_async(dispatch_get_main_queue(),
        ^{
            [helper getpushmessage];
        });
         */
    }
    
    
    
}

// Handle any failure to register.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSString *errormsg = [NSString stringWithFormat:@"Push Register error: %@",error];
    [self MessageBox:@"Push Registration Error" message:errormsg];
}


- (void)MessageReceived:(NSString *)message
{
    [helper printloginconsole:@"6" logtext:[NSString stringWithFormat:@"Push message %@",message]];
    NSData *resultData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
    
    
//    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
//    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
//    {
//        //Do checking here.
//        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
//    }
//    else
//    {
//        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    }
    
    
    [helper printloginconsole:@"6" logtext:[NSString stringWithFormat:@"\n jsonData push response: %@",jsonData]];
    
    NSString *pushtype = [NSString stringWithFormat:@"%@",[jsonData objectForKey:@"Type"]];
    
    if ([pushtype isEqualToString:@"INFO"])
    {
        _Push_received = @"YES";
        acktoken = [jsonData objectForKey:@"PushToken"];
        /*
        [self send_appack:[NSString stringWithFormat:@"%@",[jsonData objectForKey:@"PushToken"]] log_mgs:nil];
        
        dispatch_async(dispatch_get_main_queue(),
        ^{
            [helper getpushmessage];
        });
         */
    }

}


-(void)onTick:(NSTimer *)timer
{
    if ([_Push_received isEqualToString:@"YES"])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
        ^{
            
            NSArray *openreqs = [[helper query_alldata:@"Transaction_Queue"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"transaction_status like 'Open'"] ];
            if (openreqs.count>0)
            {
                [helper send_transaction_server];
            }
            
            
            [helper send_appack:acktoken log_mgs:nil];
            [helper getpushmessage];
            
                      dispatch_async(dispatch_get_main_queue(),
           ^{
               [self.homeviewController updatenotificationbadge];
           });
       });
    
    }
    
    NSString *SignalR_reconnect = [[NSUserDefaults standardUserDefaults]valueForKey:@"SignalR_reconnect"];
    if([SignalR_reconnect isEqualToString:@"YES"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
        ^{
            [self Register_pushnotification];
            
        });
        
    }
    
  
    if ([senttransaction isEqualToString:@"Y"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                          
                               
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus remoteHostStatus = [networkReachability currentReachabilityStatus];
        if(remoteHostStatus == NotReachable)
        {
            NSLog(@"No internet connection ");
        }
        else
        {
           
            [helper send_transaction_server];
            senttransaction = @"N";
        }
                           
        });
    }
    
}


@end
