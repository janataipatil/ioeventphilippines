//
//  Approval.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 15/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ShinobiGrids/ShinobiDataGrid.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"

@interface Approval : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic)HelperClass *helper;

- (IBAction)AddTeamButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UISearchBar *ApprovalSearchBarOutlet;
@property (strong, nonatomic) IBOutlet UIButton *AddApprovalButtonOutlet;

@property (strong, nonatomic) IBOutlet UITableView *ApprovalTableview;


@property(strong,nonatomic) IBOutlet UILabel *stepname_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *assignee_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *prapproval_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *status_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *comment_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *budgetappr_ulbl;


@end
