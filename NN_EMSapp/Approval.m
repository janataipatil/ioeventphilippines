//
//  Approval.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 15/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Approval.h"

@interface Approval ()<SDataGridDataSourceHelperDelegate>
{
    ShinobiDataGrid *shinobigrid;
    NSArray *approvallist;
    AppDelegate *appdelegate;
    NSInteger count;
    NSArray *Userarray;
    SLKeyChainStore *keychain;
    NSPredicate *EventidPredicate;
    int RefreshTableview;
}
@property (nonatomic, strong) SDataGridDataSourceHelper *datasourceHelper;
@end

@implementation Approval
@synthesize helper,ApprovalSearchBarOutlet,ApprovalTableview,AddApprovalButtonOutlet;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    approvallist = [helper query_alldata:@"Event_Approvals"];
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    EventidPredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    approvallist = [approvallist filteredArrayUsingPredicate:EventidPredicate];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [self refreshdata];
    [ApprovalSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [ApprovalSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    Userarray  = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    if ([[helper EventCanEditFlag:@"canEdit"] isEqualToString:@"N"] || [appdelegate.eventStatus isEqualToString:@"Approved"] || [appdelegate.eventStatus isEqualToString:@"Started"])
    {
        AddApprovalButtonOutlet.hidden = YES;
    }
    
    [self langsetuptranslations];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshTableview == 1)
    {
        ApprovalTableview.delegate = self;
        ApprovalTableview.dataSource = self;
        [ApprovalTableview reloadData];
    }
}

-(void)langsetuptranslations
{
    _stepname_ulbl.text = [helper gettranslation:@"LBL_245"];
    _assignee_ulbl.text = [helper gettranslation:@"LBL_050"];
    
    _prapproval_ulbl.text = [helper gettranslation:@"LBL_188"];
    _budgetappr_ulbl.text = [helper gettranslation:@"LBL_059"];
    
    _status_ulbl.text = [helper gettranslation:@"LBL_242"];
    _comment_ulbl.text = [helper gettranslation:@"LBL_066"];
    
}

-(void)refreshdata
{
    approvallist = [helper query_alldata:@"Event_Approvals"];
    approvallist = [approvallist filteredArrayUsingPredicate:EventidPredicate];
    ApprovalTableview.delegate = self;
    ApprovalTableview.dataSource = self;
    [ApprovalTableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)AddTeamButtonAction:(id)sender
{
    NSString *networkstatus = [helper checkinternetconnection];
    
    if ([networkstatus isEqualToString:@"Y"])
    {
        appdelegate.approvalid = @"";
        [self.parentViewController performSegueWithIdentifier:@"EventDetailToupsertapprovals" sender:self];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_441"] message:[helper gettranslation:@"LBL_630"] action:[helper gettranslation:@"LBL_462"]];
    }
    
    
}
#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == ApprovalSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == ApprovalSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == ApprovalSearchBarOutlet)
    {
        approvallist = [helper query_alldata:@"Event_Approvals"];
        approvallist = [approvallist filteredArrayUsingPredicate:EventidPredicate];
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.approvalfor contains[c] %@) OR (SELF.comment contains[c] %@) OR (SELF.parentapprovalname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.approverid contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[approvallist filteredArrayUsingPredicate:predicate]];
            approvallist = [filtereventarr copy];
        }
       [ApprovalTableview reloadData];
       }
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [approvallist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UIButton *BudgetApproverButton = (UIButton *)[contentView viewWithTag:1];
    UILabel *stepname_lbl = (UILabel *)[contentView viewWithTag:2];
    UILabel *assignee_lbl = (UILabel *)[contentView viewWithTag:3];
    UILabel *PRapproval_lbl = (UILabel *)[contentView viewWithTag:4];
    UILabel *status_lbl = (UILabel *)[contentView viewWithTag:5];
    UILabel *comment_lbl = (UILabel *)[contentView viewWithTag:6];
    UIButton *EditButtonOutlet = (UIButton *)[contentView viewWithTag:7];
    
    NSString *canedit = [approvallist[indexPath.row] valueForKey:@"canEdit"];
    NSString *canappr = [approvallist[indexPath.row] valueForKey:@"approvalflag"];
    
   // EditButtonOutlet.hidden = YES;
    
//    if ([canedit isEqualToString:@"Y"]||[canappr isEqualToString:@"Y"])
//    {
//        EditButtonOutlet.hidden = NO;
//    }
    
    
    //Condition to check whether it is read only or Editable
    
    if([[approvallist[indexPath.row] valueForKey:@"statuslic"] isEqualToString:@"Approved"])
    {
        [EditButtonOutlet setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
    }
    else
    {
        if ([canedit isEqualToString:@"Y"]||[canappr isEqualToString:@"Y"])
               {
                    [EditButtonOutlet setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
                }
        else
        {
            [EditButtonOutlet setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
        }
    }
    
    
    
    if ([[approvallist[indexPath.row] valueForKey:@"budgetapprover"] isEqualToString:@"Y"])
    {
        [BudgetApproverButton setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
    }
    else if ([[approvallist[indexPath.row] valueForKey:@"budgetapprover"] isEqualToString:@"N"])
    {
        [BudgetApproverButton setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
    }
   
    
    [EditButtonOutlet addTarget:self action:@selector(EditButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    stepname_lbl.text = [approvallist[indexPath.row] valueForKey:@"approvalfor"];
    assignee_lbl.text = [approvallist[indexPath.row] valueForKey:@"approverName"];
    PRapproval_lbl.text = [approvallist[indexPath.row] valueForKey:@"parentapprovalname"];
    status_lbl.text = [approvallist[indexPath.row] valueForKey:@"status"];
    comment_lbl.text = [approvallist[indexPath.row] valueForKey:@"comment"];
    
    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *candel = [approvallist[indexPath.row] valueForKey:@"canDelete"];
   NSString *status= [approvallist[indexPath.row] valueForKey:@"statuslic"];
    
    if ([candel isEqualToString:@"N"] || [status isEqualToString:@"Approved"])
    {
        return NO;
    }
    else
    {
        return YES;
    }
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == ApprovalTableview)
    {
        
        NSPredicate *userpred = [NSPredicate predicateWithFormat:@"id like %@",[approvallist[indexPath.row] valueForKey:@"id"]];
        [helper serverdelete:@"E_APPROVALS" entityid:[approvallist[indexPath.row] valueForKey:@"id"]];
        NSLog(@"APPROVALS id is %@",[approvallist[indexPath.row] valueForKey:@"id"]);
        [helper delete_predicate:@"Event_Approvals" predicate:userpred];
        [self refreshdata];
    }
}
-(IBAction)EditButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:ApprovalTableview];
    NSIndexPath *indexPath = [ApprovalTableview indexPathForRowAtPoint:buttonPosition];
    
    NSString *networkstatus = [helper checkinternetconnection];
   
    if(![[approvallist[indexPath.row] valueForKey:@"statuslic"] isEqualToString:@"Approved"])
    {
        if ([networkstatus isEqualToString:@"Y"])
        {
            CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:ApprovalTableview];
            NSIndexPath *indexPath = [ApprovalTableview indexPathForRowAtPoint:buttonPosition];
            appdelegate.approvalid = [approvallist[indexPath.row] valueForKey:@"id"];
            [self.parentViewController performSegueWithIdentifier:@"EventDetailToupsertapprovals" sender:self];
        }
        else
        {
            [helper showalert:[helper gettranslation:@"LBL_441"] message:[helper gettranslation:@"LBL_630"] action:[helper gettranslation:@"LBL_462"]];
        }
    }
    else
    {
        appdelegate.approvalid = [approvallist[indexPath.row] valueForKey:@"id"];
        [self.parentViewController performSegueWithIdentifier:@"EventDetailToupsertapprovals" sender:self];
    }
    
}
#pragma mark unwind method

-(IBAction)unwindtoApprovallist:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
   [self refreshdata];
}




@end
