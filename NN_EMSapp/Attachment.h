//
//  Attachment.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 15/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"


@interface Attachment : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UISearchBar *attachmentSearchBarOutlet;

@property (strong, nonatomic) IBOutlet UITableView *attachmentTableview;

@property (strong, nonatomic) IBOutlet UIButton *AddAttachmentButtonOutlet;
- (IBAction)AddAttachmentButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *filename_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *filetype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *filesize_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *attachmentype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *uploadedate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *description_ulbl;
@property (weak, nonatomic) IBOutlet UILabel *groupSearch_lbl;
@property (weak, nonatomic) IBOutlet UITextField *groupSearch_tv;
@property (weak, nonatomic) IBOutlet UITableView *groupTableView;

@end
