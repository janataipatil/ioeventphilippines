//
//  Attachment.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 15/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Attachment.h"

@interface Attachment ()
{
    NSArray *attachmentlist;
    AppDelegate *appdelegate;
    NSInteger count;
    NSArray *Userarray;
    SLKeyChainStore *keychain;
    NSPredicate *EventidPredicate;
    NSString *attachmentfile_url;
    NSString *filedownloadloc;
    NSString *locfilename;
    int RefreshTableview;
    NSArray *GroupArray;
    NSMutableArray *SearchgrpArray;
}
@end

@implementation Attachment

@synthesize helper, attachmentTableview,attachmentSearchBarOutlet,AddAttachmentButtonOutlet;

- (void)viewDidLoad
{
    [super viewDidLoad];
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    attachmentlist = [helper query_alldata:@"Event_Attachments"];
    attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    _groupSearch_tv.delegate=self;
    _groupTableView.delegate=self;
    _groupTableView.dataSource=self;
    
    GroupArray = [[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
   _groupSearch_tv.placeholder=[helper gettranslation:@"LBL_932"];
    SearchgrpArray= [[NSMutableArray alloc]init];
    [SearchgrpArray addObjectsFromArray:GroupArray];
    
    NSMutableDictionary *grpDict = [[NSMutableDictionary alloc]init];
    [grpDict setValue:@"" forKey:@"eventid"];
    [grpDict setValue:@"" forKey:@"groupid"];
    [grpDict setValue:@"ALL" forKey:@"groupname"];
    [SearchgrpArray addObject: grpDict];
    
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [attachmentSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [attachmentSearchBarOutlet valueForKey:@"_searchField"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
   searchField.textColor = [helper DarkBlueColour];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    
    
     [self refreshdata];
    
    if (![appdelegate.EventUserRoleArray containsObject:@"E_ATCH_MNG"])
    {
        AddAttachmentButtonOutlet.hidden = YES;
    }
    
    [self langsetuptranslations];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshTableview == 1)
    {
        attachmentTableview.delegate = self;
        attachmentTableview.dataSource = self;
        [attachmentTableview reloadData];
      }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _groupTableView)
    {
        _groupTableView.hidden = YES;
        [_groupSearch_tv resignFirstResponder];
    }
}
-(void)langsetuptranslations
{

        _filename_ulbl.text = [helper gettranslation:@"LBL_402"];
        _filetype_ulbl.text = [helper gettranslation:@"LBL_404"];
        _filesize_ulbl.text = [helper gettranslation:@"LBL_923"];
        _groupSearch_lbl.text=[helper gettranslation:@"LBL_923"];
        _attachmentype_ulbl.text = [helper gettranslation:@"LBL_324"];
        _uploadedate_ulbl.text = [helper gettranslation:@"LBL_577"];
       _description_ulbl.text = [helper gettranslation:@"LBL_094"];
    
}

-(void)refreshdata
{
    attachmentlist = [helper query_alldata:@"Event_Attachments"];
    attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    attachmentTableview.delegate = self;
    attachmentTableview.dataSource = self;
    [attachmentTableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == attachmentSearchBarOutlet)
    {
        if(count > searchText.length)
        {
            attachmentlist = [helper query_alldata:@"Event_Attachments"];
            attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        }
        if(searchText.length == 0)
        {
            attachmentlist = [helper query_alldata:@"Event_Attachments"];
            attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            attachmentlist = [helper query_alldata:@"Event_Attachments"];
            attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.filename contains[c] %@) OR (SELF.filetype contains[c] %@) OR (SELF.filesize contains[c] %@) OR (SELF.attachmenttype contains[c] %@) OR (SELF.desc contains[c] %@) OR (SELF.uploaddate contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[attachmentlist filteredArrayUsingPredicate:predicate]];
            attachmentlist = [filtereventarr copy];
        }
        [attachmentTableview reloadData];
    }
}

#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _groupTableView){
        return GroupArray.count;
    }else
    return [attachmentlist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    if(tableView == _groupTableView){

        customcell = [self.groupTableView dequeueReusableCellWithIdentifier:@"socustomcell" forIndexPath:indexPath];
        UILabel *groupName = (UILabel *)[customcell.contentView viewWithTag:1];
        
        if([[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"]){
            groupName.text = [helper gettranslation:@"LBL_037"];
        }
        else
            groupName.text = [[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
    }
    else{
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    UILabel *filename = (UILabel *)[contentView viewWithTag:1];
    UILabel *filetype = (UILabel *)[contentView viewWithTag:2];
    UILabel *filesize = (UILabel *)[contentView viewWithTag:3];
    UILabel *attachmenttype = (UILabel *)[contentView viewWithTag:4];
    UILabel *uploaddate = (UILabel *)[contentView viewWithTag:5];
    UILabel *desc = (UILabel *)[contentView viewWithTag:6];

    NSString *formateddate = [helper formatingdate:[attachmentlist[indexPath.row] valueForKey:@"uploaddate"] datetime:@"FormatDate"];
    

    filename.text = [attachmentlist[indexPath.row] valueForKey:@"filename"];

    filetype.text = [NSString stringWithFormat:@"%@, %@",[attachmentlist[indexPath.row] valueForKey:@"filetype"],[attachmentlist[indexPath.row] valueForKey:@"filesize"]];
   
    
   // filetype.text = [attachmentlist[indexPath.row] valueForKey:@"filetype"];
    
    NSArray *tempGroupArray = [GroupArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupid like %@",[attachmentlist[indexPath.row] valueForKey:@"groupID"]]];
    
    if(tempGroupArray.count>0)
    {
        filesize.text = [tempGroupArray[0] valueForKey:@"groupname"];
    }
        else
            filesize.text = @"";
    
    attachmenttype.text = [attachmentlist[indexPath.row] valueForKey:@"attachmenttype"];
    uploaddate.text = formateddate;//[attachmentlist[indexPath.row] valueForKey:@"uploaddate"];
    desc.text = [attachmentlist[indexPath.row] valueForKey:@"desc"];
    
    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return customcell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([appdelegate.EventUserRoleArray containsObject:@"E_ATCH_MNG"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *attachrecid = [attachmentlist[indexPath.row] valueForKey:@"id"];
    NSPredicate *attachpred = [NSPredicate predicateWithFormat:@"id like %@",attachrecid];
    
    [helper serverdelete:@"E_ATT" entityid:attachrecid];
    [helper delete_predicate:@"Event_Attachments" predicate:attachpred];
    
    [self refreshdata];
}

#pragma mark unwind method
-(IBAction)unwindToAttachment:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
    [self refreshdata];
}

- (IBAction)AddAttachmentButtonAction:(id)sender
{
    [self.parentViewController performSegueWithIdentifier:@"AtttachmentToAddAttachment" sender:self];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _groupTableView){
        if([[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"])
        {
            [self refreshdata];
            _groupSearch_tv.text =[helper gettranslation:@"LBL_037"];
        }
        else{
            attachmentlist = [helper query_alldata:@"Event_Attachments"];
            attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            attachmentlist = [attachmentlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupid"]]];
            _groupSearch_tv.text =[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
            [self.attachmentTableview reloadData];
        }
        [_groupTableView setHidden:YES];
        [_groupSearch_tv resignFirstResponder];
    }
    else
    {
    [helper showWaitCursor:[helper gettranslation:@"LBL_374"]];
    
    NSString * filename = [NSString stringWithFormat:@"%@.%@", [attachmentlist[indexPath.row] valueForKey:@"filename"], [attachmentlist[indexPath.row] valueForKey:@"filetype"]];
    attachmentfile_url = [attachmentlist[indexPath.row] valueForKey:@"storageid"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^{
        @autoreleasepool {
        
        locfilename = [filename stringByReplacingOccurrencesOfString:@" " withString:@""];
        filedownloadloc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:locfilename];
                       
        NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:attachmentfile_url]];
        
        if ( curlData )
        {
            [curlData writeToFile:filedownloadloc atomically:YES];
        }
        else
        {
            NSLog(@"Unable to download Attachment: %@",attachmentfile_url);
        }
                        }
        dispatch_async(dispatch_get_main_queue(),
        ^{
            [helper removeWaitCursor];
            [self performSegueWithIdentifier:@"AttachmentToWebview" sender:self];
        });
    });
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    if([[segue identifier] isEqualToString:@"AttachmentToWebview"])
    {
        DisplayAttachment *controller = segue.destinationViewController;
        controller.webviewurl = filedownloadloc;
       // controller.fromview = @"Eventattachment";
        controller.documentname = locfilename;
        controller.cancelsegue = @"DisplayAttachmentsToList";
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _groupSearch_tv){
        [_groupSearch_tv resignFirstResponder];
        [_groupTableView setHidden:NO];
        [_groupTableView reloadData];
    }
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == attachmentSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == attachmentSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}

@end
