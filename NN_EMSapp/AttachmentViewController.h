//
//  AttachmentViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 04/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"
#import "AddAttachment.h"
#import "DisplayAttachment.h"

@interface AttachmentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *titleHeader_lbl;

- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic) IBOutlet UITableView *AttachmetTableView;

@property (strong, nonatomic) IBOutlet UILabel *fileName_lbl;

@property (strong, nonatomic) IBOutlet UILabel *fileType_lbl;

@property (strong, nonatomic) IBOutlet UILabel *fileSize_lbl;

@property (strong, nonatomic) IBOutlet UILabel *attachmentType_lbl;

@property (strong, nonatomic) IBOutlet UILabel *description_lbl;

@property (strong, nonatomic) IBOutlet UILabel *uploadDate_lbl;

@property (strong, nonatomic) IBOutlet UIButton *addAttachment;

@property (strong, nonatomic)NSString *senderView;
@property (strong, nonatomic)NSString *speakerID;
@property (strong, nonatomic)NSString *attendeeID;
@property (strong, nonatomic)NSString *attendeeName;
@property (strong, nonatomic)NSString *speakerName;

- (IBAction)addAttachment:(id)sender;

@end
