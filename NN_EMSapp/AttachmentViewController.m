//
//  AttachmentViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 04/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AttachmentViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface AttachmentViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    dispatch_semaphore_t sem_att;
    NSArray *AttachmentArray;
    UIImage *capturedlicensimg;
    
    NSData *filedata;
    NSString *filename;
    NSString *filesize;
    NSString *fileType;
    NSString *attachmentType;
    NSString *attachmentName;
    NSString *URL;
    NSString *semp;
    dispatch_semaphore_t sem_event;
    BOOL isDataSaved;
    
}

@end

@implementation AttachmentViewController

- (void)viewDidLoad {
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper=[[HelperClass alloc]init];
    
    
    self.AttachmetTableView.dataSource=self;
    self.AttachmetTableView.delegate=self;
    
    [_backBtn setTitle:[helper gettranslation:@"LBL_342"] forState:UIControlStateNormal];
    isDataSaved = false;
    
    [self RefreshData];
    
    if([_senderView isEqualToString:@"Speaker"])
    {
        if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] || (![appdelegate.UserRolesArray containsObject:@"SPK_R"] && ![appdelegate.UserRolesArray containsObject:@"SPK_W"]))
        {
            [_addAttachment setHidden:YES];
        }
        if ([appdelegate.UserRolesArray containsObject:@"SPK_W"])
        {
            [_addAttachment setHidden:NO];
        }
        if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] && [appdelegate.UserRolesArray containsObject:@"SPK_W"])
        {
            [_addAttachment setHidden:NO];
        }
    }
    else if([_senderView isEqualToString:@"Attendee"])
    {
        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] || (![appdelegate.UserRolesArray containsObject:@"ATT_R"] && ![appdelegate.UserRolesArray containsObject:@"ATT_W"]))
        {
            [_addAttachment setHidden:YES];        }
        if ([appdelegate.UserRolesArray containsObject:@"ATT_W"])
        {
            
            [_addAttachment setHidden:NO];
            
        }
        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] && [appdelegate.UserRolesArray containsObject:@"ATT_W"])
        {
            
            [_addAttachment setHidden:NO];
            
        }
        
    }
    // else if ([_senderView isEqualToString:@"Itineary"])
    
    
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma tableview delegate functions

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [AttachmentArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *identifier=@"AttachmentCell";
    UITableViewCell *Cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    UILabel *filename=(UILabel *)[Cell viewWithTag:1];
    UILabel *fileType=(UILabel *)[Cell viewWithTag:2];
    UILabel *fileSize=(UILabel *)[Cell viewWithTag:3];
    UILabel *attachmentType=(UILabel *)[Cell viewWithTag:4];
    UILabel *description=(UILabel *)[Cell viewWithTag:5];
    UILabel *uploadDate=(UILabel *)[Cell viewWithTag:6];
    
    
    filename.text=[AttachmentArray[indexPath.row] valueForKey:@"filename"];
    if([_senderView isEqualToString:@"Itenary"])
        fileType.text=[AttachmentArray[indexPath.row] valueForKey:@"filetype"];
    else
        fileType.text=[AttachmentArray[indexPath.row] valueForKey:@"fileType"];
    
    if([_senderView isEqualToString:@"Itenary"])
        fileSize.text=[AttachmentArray[indexPath.row] valueForKey:@"filesize"];
    else
        fileSize.text=[AttachmentArray[indexPath.row] valueForKey:@"fileSize"];
    
    
    attachmentType.text=[AttachmentArray[indexPath.row] valueForKey:@"type"];
    description.text=[AttachmentArray[indexPath.row] valueForKey:@"desc"];
    
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
    
    
    NSDate *date=[dateformatter dateFromString:[AttachmentArray[indexPath.row] valueForKey:@"uploaddate"]];
    
    [dateformatter setDateFormat:@"dd MMM yyyy"];
    
    uploadDate.text=[dateformatter stringFromDate:date];
    
    
    return Cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if([_senderView isEqualToString:@"Speaker"])
    {
        if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] || (![appdelegate.UserRolesArray containsObject:@"SPK_R"] && ![appdelegate.UserRolesArray containsObject:@"SPK_W"]))
        {
            return NO;
        }
        else if ([appdelegate.UserRolesArray containsObject:@"SPK_W"])
        {
            return YES;
        }
        else if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] && [appdelegate.UserRolesArray containsObject:@"SPK_W"])
        {
            return YES;
        }
        else
            return NO;
    }
    else if ([_senderView isEqualToString:@"Attendee"])
    {
        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] || (![appdelegate.UserRolesArray containsObject:@"ATT_R"] && ![appdelegate.UserRolesArray containsObject:@"ATT_W"]))
        {
            return NO;
        }
        else if ([appdelegate.UserRolesArray containsObject:@"ATT_W"])
        {
            
            return YES;
            
        }
        else if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] && [appdelegate.UserRolesArray containsObject:@"ATT_W"])
        {
            
            return YES;
        }
        else
        {
            return NO;
        }
        
    }
    else if([_senderView isEqualToString:@"Itenary"])
    {
        return YES;
    }
    else
        return NO;
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    if([_senderView isEqualToString:@"Itenary"])
        URL=[AttachmentArray[indexPath.row] valueForKey:@"storageid"];
    else
        URL=[AttachmentArray[indexPath.row] valueForKey:@"storageID"];
    
    attachmentName=[AttachmentArray[indexPath.row] valueForKey:@"filename"];
    if([URL length]>0)
        [self performSegueWithIdentifier:@"showAttachment" sender:self];
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   semp = @"Yes";
                                   [self deleteActionPerformed:indexPath];
                               }];
    [alert addAction:okaction];
    
    
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       semp = @"No";
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    [alert addAction:cancelaction];
    [self presentViewController:alert animated:NO completion:nil];
    
    
    //    while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
    //
    //    if ([semp isEqualToString:@"Yes"])
    //    {
    //
    //        NSString *attachrecid = [AttachmentArray[indexPath.row] valueForKey:@"id"];
    //        NSPredicate *attachpred = [NSPredicate predicateWithFormat:@"id like %@",attachrecid];
    //
    //        [helper serverdelete:@"ENTITY_ATT" entityid:attachrecid];
    //
    //        if([_senderView isEqualToString:@"Speaker"])
    //        {
    //            [helper delete_predicate:@"SpeakerAttachment" predicate:attachpred];
    //        }
    //        else if ([_senderView isEqualToString:@"Itenary"])
    //        {
    //            [helper delete_predicate:@"ItinearyAttachment" predicate:attachpred];
    //        }
    //        else
    //        {
    //            [helper delete_predicate:@"AttendeeAttachment" predicate:attachpred];
    //        }
    //
    //
    //        [self RefreshData];
    //        [self.AttachmetTableView reloadData];
    //    }
}

-(void)deleteActionPerformed:(NSIndexPath *)indexTag {
    isDataSaved = true;
    [helper showWaitCursor:[helper gettranslation:@"LBL_734"]];
    
    NSString *attachrecid = [AttachmentArray[indexTag.row] valueForKey:@"id"];
    NSPredicate *attachpred = [NSPredicate predicateWithFormat:@"id like %@",attachrecid];
    dispatch_queue_t queue=dispatch_queue_create(0,0);
    dispatch_async(queue, ^{
        // Perform long running process
        [helper serverdelete:@"ENTITY_ATT" entityid:attachrecid];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            if([_senderView isEqualToString:@"Speaker"])
            {
                [helper delete_predicate:@"SpeakerAttachment" predicate:attachpred];
            }
            else if ([_senderView isEqualToString:@"Itenary"])
            {
                [helper delete_predicate:@"ItinearyAttachment" predicate:attachpred];
            }
            else
            {
                [helper delete_predicate:@"AttendeeAttachment" predicate:attachpred];
            }
            [self RefreshData];
            
        });
    });
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backBtn:(id)sender {
    
    if([_senderView isEqualToString:@"Speaker"])
        [self performSegueWithIdentifier:@"BackToEventSpeakerDetails" sender:self];
    else
        [self performSegueWithIdentifier:@"BackToEventAttendees" sender:self];
    
    [self.view removeFromSuperview];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    if (picker.sourceType ==  UIImagePickerControllerSourceTypeCamera)
    {
        capturedlicensimg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"yyyyMMdd"];
        NSString *date = [dateformatter stringFromDate:[NSDate date]];
        [dateformatter setDateFormat:@"HHmmss"];
        NSString *time = [dateformatter stringFromDate:[NSDate date]];
        filename = [NSString stringWithFormat:@"Image_%@_%@.JPG",date,time];
        fileType = @"JPG";
        
    }
    [self resizeimage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
        {
            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
            filename = [imageRep filename];
            // NSLog(@"Attachment image name is %@",attachedimagename);
            fileType = [filename pathExtension];
            //  NSLog(@"Attachment path extension %@",PathExtension);
            
            [self performSegueWithIdentifier:@"uploadAttachmentDialog" sender:self];
        }
        
        
    };
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    //filedata = UIImageJPEGRepresentation(capturedlicensimg, 1); //1 it represents the quality of the image.
    
    //NSLog(@"Size of Image(bytes):%ld",[imgData length]);
    // NSLog(@"image size %@",[NSByteCountFormatter stringFromByteCount:filedata.length countStyle:NSByteCountFormatterCountStyleFile]);
    
    
    filesize = [NSByteCountFormatter stringFromByteCount:filedata.length countStyle:NSByteCountFormatterCountStyleFile];
    
    
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera)
        [self performSegueWithIdentifier:@"uploadAttachmentDialog" sender:self];
}
-(void)resizeimage
{
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 500000;
    
    filedata = UIImageJPEGRepresentation(capturedlicensimg, compression);
    
    while ([filedata length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        filedata = UIImageJPEGRepresentation(capturedlicensimg, compression);
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"uploadAttachmentDialog"])
    {
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
        
        AddAttachment *controller=segue.destinationViewController;
        
        controller.senderView=_senderView;
        controller.filesize=filesize;
        controller.filetype=fileType;
        controller.filename=filename;
        controller.filedata=filedata;
        controller.capturedlicensimg = capturedlicensimg;
        
        if([_senderView isEqualToString:@"Speaker"])
            controller.speakerID=_speakerID;
        else
            controller.attendeeID=appdelegate.attendyid;
        
        controller.uploadDate=[dateformatter stringFromDate:[NSDate date]];
    }
    
    if([[segue identifier] isEqualToString:@"showAttachment"])
    {
        DisplayAttachment *controller=segue.destinationViewController;
        if([_senderView isEqualToString:@"Speaker"])
            controller.type=@"Speaker";
        else if ([_senderView isEqualToString:@"Attendee"])
            controller.type=@"Attendee";
        else
            controller.type=@"Itenary";
        
        controller.webviewurl=URL;
        controller.documentname=attachmentName;
        //        NSInteger *dfgs = [UIApplication sharedApplication].applicationIconBadgeNumber;
        //        UIImageView *ima = [[UIImageView alloc]init];
        //        ima.image.badge
        
    }
    
    
    
}
- (IBAction)addAttachment:(id)sender {
    
//    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
//    if (networkStatus == NotReachable)
//    {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:true completion:nil];
//    }
//    else
//    {
//        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_578"] message:[helper gettranslation:@"LBL_504"] preferredStyle:UIAlertControllerStyleAlert];
//        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_632"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                   {
//                                       if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
//                                       {
//                                           UIImagePickerController *impicker = [[UIImagePickerController alloc]init];
//                                           impicker.delegate = self;
//                                           impicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
//                                           impicker.allowsEditing = NO;
//                                           [self presentViewController:impicker animated:YES completion:NULL];
//
//                                       }
//
//
//                                   }]];
//
//        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_334"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                   {
//                                       if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
//                                       {
//                                           UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
//                                           imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
//                                           imagepicker.allowsEditing = YES;
//                                           imagepicker.delegate = self;
//                                           [self presentViewController:imagepicker animated:YES completion:NULL];
//                                       }
//
//                                   }]];
//
//        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:takephotoalert animated:YES completion:nil];
//    }
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        if (([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusNotDetermined) || ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusAuthorized))
        {
            ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
            [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                if (*stop) {
                    UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_578"] message:[helper gettranslation:@"LBL_504"] preferredStyle:UIAlertControllerStyleAlert];
                    [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_632"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                               {
                                                   if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                                                   {
                                                       UIImagePickerController *impicker = [[UIImagePickerController alloc]init];
                                                       impicker.delegate = self;
                                                       impicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                                       impicker.allowsEditing = NO;
                                                       [self presentViewController:impicker animated:YES completion:NULL];
                                                       
                                                   }
                                                   
                                                   
                                               }]];
                    
                    [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_334"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                               {
                                                   if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
                                                   {
                                                       UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
                                                       imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                       imagepicker.allowsEditing = YES;
                                                       imagepicker.delegate = self;
                                                       [self presentViewController:imagepicker animated:YES completion:NULL];
                                                   }
                                                   
                                               }]];
                    /* [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_631"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                     {
                     UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.content"] inMode:UIDocumentPickerModeImport];
                     documentPicker.delegate = self;
                     documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
                     [self presentViewController:documentPicker animated:YES completion:nil];
                     }]];*/
                    
                    [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:takephotoalert animated:YES completion:nil];
                }
                *stop = TRUE;
            } failureBlock:^(NSError *error) {
                
            }];
        }
        else
        {
            [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_935"] action:[helper gettranslation:@"LBL_462"]];
        }
    }
}


-(void)RefreshData
{
    if([_senderView isEqualToString:@"Speaker"])
    {
        
        AttachmentArray=[[helper query_alldata:@"SpeakerAttachment"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"entityid like %@",_speakerID]];
        _titleHeader_lbl.text=_speakerName;
    }
    else if ([_senderView isEqualToString:@"Attendee"])
    {
        AttachmentArray=[[helper query_alldata:@"AttendeeAttachment"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"entityid like %@",appdelegate.attendyid]];
        _titleHeader_lbl.text=appdelegate.attendeeName;
    }
    else if ([_senderView isEqualToString:@"Itenary"])
    {
        AttachmentArray=[[helper query_alldata:@"ItinearyAttachment"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"entityid like %@",appdelegate.itnearyid]];
        _titleHeader_lbl.text=appdelegate.attendeeName;
        
    }
    if (isDataSaved) {
        isDataSaved = false;
        [helper removeWaitCursor];
        [self.AttachmetTableView reloadData];
        
    }
    
}


-(IBAction)unwindtoAttachmentList:(UIStoryboardSegue *)segue
{
    
    if([_senderView isEqualToString:@"Speaker"])
    {
        
        AttachmentArray=[[helper query_alldata:@"SpeakerAttachment"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"entityid like %@",_speakerID]];
        
    }
    else if ([_senderView isEqualToString:@"Attendee"])
    {
        AttachmentArray=[[helper query_alldata:@"AttendeeAttachment"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"entityid like %@",appdelegate.attendyid]];
        
    }
    else
    {
        AttachmentArray=[[helper query_alldata:@"ItinearyAttachment"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(entityid like %@)",appdelegate.itnearyid]];
    }
    
    [_AttachmetTableView reloadData];
}
@end

