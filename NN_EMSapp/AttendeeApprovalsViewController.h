//
//  AttendeeApprovalsViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 07/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "EventAttendeePopoverViewController.h"
#import "EventAttendee_Detail.h"

@interface AttendeeApprovalsViewController : UIViewController<UIPopoverPresentationControllerDelegate,UITableViewDelegate,UITableViewDataSource>


@property (strong, nonatomic) IBOutlet UITableView *ApprovalTableView;
@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UISearchBar *AttendeeApproverSearchBarOutlet;
@property(strong,nonatomic) EventAttendee_Detail  *attendetailview;
- (IBAction)SelectAllButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *SelectAllButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *AcceptButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *RejectButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *advancesrc_btn;

- (IBAction)AcceptButtonAction:(id)sender;
- (IBAction)RejectButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *salutation;
@property (strong, nonatomic) IBOutlet UILabel *firstname;
@property (strong, nonatomic) IBOutlet UILabel *lastname;
@property (strong, nonatomic) IBOutlet UILabel *speciality;
@property (strong, nonatomic) IBOutlet UILabel *targetclass;
@property (strong, nonatomic) IBOutlet UILabel *status;

@property (weak, nonatomic) IBOutlet UITextField *groupSearchtv;

@property (weak, nonatomic) IBOutlet UILabel *groupSearchlbl;
@property (weak, nonatomic) IBOutlet UITableView *groupTableView;
//- (IBAction)addattendeeClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *AddAttendeeButtonOutlet;
- (IBAction)groupsortbtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *groupsortimage;

- (IBAction)clearsrc_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *clearsrc_btn;

- (IBAction)advancesrc_btn:(id)sender;
- (IBAction)salutationBtn:(id)sender;
- (IBAction)firstNameBtn:(id)sender;
- (IBAction)lastNameBtn:(id)sender;
- (IBAction)statusBtn:(id)sender;
- (IBAction)targetClassBtn:(id)sender;
- (IBAction)specialityBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *salutationImg;
@property (strong, nonatomic) IBOutlet UIImageView *firstNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *lastNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *statusImg;
@property (strong, nonatomic) IBOutlet UIImageView *specialityImg;
@property (strong, nonatomic) IBOutlet UIImageView *targetClassImg;




@end
