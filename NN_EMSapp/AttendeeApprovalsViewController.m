//
//  AttendeeApprovalsViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 07/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "AttendeeApprovalsViewController.h"
#import "AdvanceSearchView.h"

@interface AttendeeApprovalsViewController ()
{
    NSArray *AttendeeApprovalArr;
    AppDelegate *appdelegate;
    int SelectAllButtonPressCount;
    NSUInteger count;
    NSPredicate *ApprovalPredicate;
    NSMutableArray *SelectedAttendee;
    NSMutableArray *SearchgrpArray;
    NSString *senderview;
    NSString *AttendeeID;
    BOOL salutationFlag;
    BOOL firstNameFlag;
    BOOL lastNameFlag;
    BOOL statusFlag;
    BOOL targetClassFlag;
    BOOL specialityFlag;
    CGPoint location;
    
    BOOL RowSelected;
    BOOL selectAllFlag;
    NSIndexPath *m_currentIndexPath;
    
    NSInteger ln_expandedRowIndex;
    NSArray *groupArray;
    NSArray *MasterLOV;
    BOOL groupFlag;
}

@end

@implementation AttendeeApprovalsViewController
@synthesize helper,ApprovalTableView,SelectAllButtonOutlet,AttendeeApproverSearchBarOutlet,AcceptButtonOutlet,RejectButtonOutlet;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self RefreshData];
    
    _groupSearchtv.placeholder=[helper gettranslation:@"LBL_932"];
    
    groupArray = [[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    SearchgrpArray= [[NSMutableArray alloc]init];
    [SearchgrpArray addObjectsFromArray:groupArray];
    _groupTableView.delegate=self;
    _groupTableView.dataSource=self;
    NSMutableDictionary *grpDict = [[NSMutableDictionary alloc]init];
    [grpDict setValue:@"" forKey:@"eventid"];
    [grpDict setValue:@"" forKey:@"groupid"];
    [grpDict setValue:@"ALL" forKey:@"groupname"];
    [SearchgrpArray addObject: grpDict];
    
    
     groupFlag=YES;
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    MasterLOV =  [helper query_alldata:@"Event_LOV"];
    [_advancesrc_btn setHidden:YES];
    
    RowSelected=NO;
    _attendetailview = [[EventAttendee_Detail alloc] init];
    [AttendeeApproverSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchfield = [AttendeeApproverSearchBarOutlet valueForKey:@"_searchField"];
    searchfield.placeholder = [helper gettranslation:@"LBL_224"];
    searchfield.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:242.0/255.0f green:246.0/255.0f blue:250.0f/255.0f alpha:1.0f];
    NSLog(@"Event user role array is %@",appdelegate.EventUserRoleArray);
    if (![appdelegate.EventUserRoleArray containsObject:@"E_ATT_MNG"])
    {
        AcceptButtonOutlet.hidden = YES;
        RejectButtonOutlet.hidden = YES;
    }
    
    ln_expandedRowIndex = -1;
    SelectAllButtonPressCount=0;
    selectAllFlag=NO;
    if(AttendeeApprovalArr.count==0)
        [AcceptButtonOutlet setUserInteractionEnabled:NO];
    else
        [AcceptButtonOutlet setUserInteractionEnabled:YES];
    
    _clearsrc_btn.hidden = YES;
    [self langsetuptranslations];
    
    salutationFlag=YES;
    firstNameFlag=YES;
    lastNameFlag=YES;
    statusFlag=YES;
    targetClassFlag=YES;
    specialityFlag=YES;
    

}

-(void)langsetuptranslations
{
    _salutation.text = [helper gettranslation:@"LBL_218"];
    _firstname.text = [helper gettranslation:@"LBL_141"];
    _lastname.text = [helper gettranslation:@"LBL_153"];
    _speciality.text = [helper gettranslation:@"LBL_238"];
    _targetclass.text = [helper gettranslation:@"LBL_255"];
    _status.text = [helper gettranslation:@"LBL_242"];
    
}

-(IBAction)uploadAttachment:(id)sender
{
    // AttachmentViewController *attach=[[AttachmentViewController alloc]init];
    // attach.senderView=@"Attendee";
    appdelegate.attendeeName=[NSString stringWithFormat:@"%@ %@",[AttendeeApprovalArr[m_currentIndexPath.row] valueForKey:@"firstname"],[AttendeeApprovalArr[m_currentIndexPath.row] valueForKey:@"lastname"]];
    // attach.attendeeID=[attendylist[m_currentIndexPath.row] valueForKey:@"attendeeid"];
    
    appdelegate.attendyid=[AttendeeApprovalArr[m_currentIndexPath.row] valueForKey:@"id"];
    
    [self.parentViewController.parentViewController performSegueWithIdentifier:@"showAttendeeAttachment" sender:self];
    
}

-(void)RefreshData
{
    AttendeeApprovalArr = [helper query_alldata:@"Event_Attendee"];
    NSLog(@"Approval flag is %@",[AttendeeApprovalArr valueForKey:@"approvalFlag"]);
    
    ApprovalPredicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.approvalFlag like %@)",appdelegate.eventid,@"Y"];
    AttendeeApprovalArr = [AttendeeApprovalArr filteredArrayUsingPredicate:ApprovalPredicate];
    ln_expandedRowIndex=-1;
    [ApprovalTableView reloadData];
  

    
    SelectedAttendee = [[NSMutableArray alloc] init];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark tableview method

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 127; // 60+77
    }
    
    return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _groupTableView){
        return SearchgrpArray.count;
    }else
    return [AttendeeApprovalArr count] + (ln_expandedRowIndex != -1 ? 1 : 0);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(tableView == _groupTableView){
        SOCustomCell *customcell;
        customcell = [self.groupTableView dequeueReusableCellWithIdentifier:@"socustomcell" forIndexPath:indexPath];
        UILabel *groupName = (UILabel *)[customcell.contentView viewWithTag:1];
        
        if([[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"]){
            groupName.text = [helper gettranslation:@"LBL_037"];
        }
        else
            groupName.text = [[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
        return customcell;
    }else{
    
    NSInteger row = [indexPath row];
    NSInteger dataIndex = [self dataIndexForRowIndex:row];

    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
    
    
    if (!ln_expandedCell)
    {
        SOCustomCell *customcell;
        NSString *cellIdentifier = @"customcell";
        customcell = [ApprovalTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
        UIView *contentView = customcell.contentView;
    
        NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
       // if (indexPath.row >= AttendeeApprovalArr.count)
        //{
          //  [SelectedArray insertObject:AttendeeApprovalArr[indexPath.row - 1] atIndex:0];
        //}
        //else
       // {
            [SelectedArray insertObject:AttendeeApprovalArr[dataIndex] atIndex:0];
        //}

        
         NSArray *tempGroupArray =[groupArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupid like %@",[SelectedArray[0] valueForKey:@"groupID"]]];
    
        // customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        UILabel *salutation = (UILabel *) [contentView viewWithTag:20];
        UILabel *firstname = (UILabel *) [contentView viewWithTag:30];
        UILabel *lastname = (UILabel *) [contentView viewWithTag:4];
        UILabel *status = (UILabel *) [contentView viewWithTag:5];
        UILabel *ownedby = (UILabel *) [contentView viewWithTag:6];
        UILabel *speciality = (UILabel *) [contentView viewWithTag:7];
        UIButton *checkbox = (UIButton *)[contentView viewWithTag:100];
        UILabel *group = (UILabel *)[contentView viewWithTag:8];
    
        [salutation setText:[SelectedArray[0] valueForKey:@"salutation"]];
        [firstname setText:[SelectedArray[0] valueForKey:@"firstname"]];
        [lastname setText:[SelectedArray[0] valueForKey:@"lastname"]];
        [ownedby setText:[SelectedArray[0] valueForKey:@"targetclass"]];
        [status setText:[SelectedArray[0] valueForKey:@"status"]];
        [speciality setText:[SelectedArray[0] valueForKey:@"speciality"]];
        //customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    
        if(tempGroupArray.count > 0)
        {
            [group setText:[tempGroupArray[0] valueForKey:@"groupname"]];
        }
        else
            group.text=@"";

        
        [checkbox addTarget:self action:@selector(checkboxAction:) forControlEvents:UIControlEventTouchUpInside];
    
        
        if(SelectAllButtonPressCount==0 && !selectAllFlag)
        {
            [checkbox setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
        }
        else
        {
            [checkbox setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];

        }
        
        if([SelectedAttendee containsObject:[SelectedArray[0] valueForKey:@"attendeeid"]])
        {
            [checkbox setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        }
        else{
            [checkbox setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
        }
        
        customcell.tintColor = [helper DarkBlueColour];
    
        NSLog(@"attendy id is %@",[SelectedArray[0] valueForKey:@"attendeeid"]);
        NSLog(@"id is %@",[SelectedArray[0] valueForKey:@"id"]);
    
        //  [customcell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return customcell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        
        UIButton *attachButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [attachButton setImage:[UIImage imageNamed:@"attach.png"] forState:UIControlStateNormal];
        attachButton.frame=CGRectMake(826, 0,48,44);
        
        [attachButton addTarget:self action:@selector(uploadAttachment:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.contentView addSubview:_attendetailview];
        [cell.contentView addSubview:attachButton];
        [self setupAttendeedetails:[AttendeeApprovalArr[indexPath.row-1] valueForKey:@"id"] backcolor:cell.backgroundColor];
      // AttendeeID=[attendylist[indexPath.row-1] valueForKey:@"attendeeid"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
        return cell;

    }
    }
}

-(IBAction)checkboxAction:(id)sender{
   
    ln_expandedRowIndex =-1;
    [ApprovalTableView reloadData];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:ApprovalTableView];
    NSIndexPath *indexPath = [ApprovalTableView indexPathForRowAtPoint:buttonPosition];
    
    
    
    if([SelectedAttendee containsObject:[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"]])
    {
        RowSelected = YES;
    }
    else{
        RowSelected = NO;
    }
    NSLog(@"index:%ld",(long)indexPath.row);

    RowSelected = !RowSelected;
        if(RowSelected)
        {
            
            AttendeeID=[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"];
            [SelectedAttendee addObject:[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"]];
            [sender setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        }
    else
    {
            for (int i=0;i<[SelectedAttendee count]; i++)
            {
                NSString *item = [SelectedAttendee objectAtIndex:i];
                if ([item isEqualToString:[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"]])
                {
                    [SelectedAttendee removeObject:item];
                    i--;
                }
            }
        [sender setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];

    }
    
    
    
  
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}

//-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//   return 0;
//}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    m_currentIndexPath=indexPath;
//    AttendeeID=[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"];
//    [SelectedAttendee addObject:[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"]];
    
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    if (indexPath.row % 2)
//    {
//        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
//    }
//    else
//    {
//        cell.contentView.backgroundColor = [UIColor whiteColor] ;
//    }
    
    if(tableView == _groupTableView){
        if([[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"])
        {
            ln_expandedRowIndex=-1;
            [self RefreshData];
            _groupSearchtv.text =[helper gettranslation:@"LBL_037"];
            [ApprovalTableView reloadData];
        }
        else{
            AttendeeApprovalArr = [helper query_alldata:@"Event_Attendee"];
            ApprovalPredicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.approvalFlag like %@)",appdelegate.eventid,@"Y"];
           
             AttendeeApprovalArr = [AttendeeApprovalArr filteredArrayUsingPredicate:ApprovalPredicate];
          
            AttendeeApprovalArr = [AttendeeApprovalArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupid"]]];
            
            _groupSearchtv.text =[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
            ln_expandedRowIndex=-1;
            [ApprovalTableView reloadData];
        }
        [_groupTableView setHidden:YES];
        [_groupSearchtv resignFirstResponder];
    }
    
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _groupSearchtv){
        [_groupSearchtv resignFirstResponder];
        [_groupTableView setHidden:NO];
        [_groupTableView reloadData];
    }
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == ApprovalTableView){
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSInteger row = [indexPath row];
  
    BOOL preventReopen = NO;
    
//    if (indexPath.row % 2)
//    {
//        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
//    }
//    else
//    {
//        cell.contentView.backgroundColor = [UIColor whiteColor] ;
//    }
    
    
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
      //  cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
        
        m_currentIndexPath=[NSIndexPath indexPathForRow:row inSection:0];
    }
    [tableView endUpdates];
    
    return nil;
    }else
        return indexPath;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    for (int i=0;i<[SelectedAttendee count]; i++)
//    {
//        NSString *item = [SelectedAttendee objectAtIndex:i];
//        if ([item isEqualToString:[AttendeeApprovalArr[indexPath.row] valueForKey:@"attendeeid"]])
//        {
//            [SelectedAttendee removeObject:item];
//            i--;
//        }
//    }
    
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    if (indexPath.row % 2)
//    {
//        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
//    }
//    else
//    {
//        cell.contentView.backgroundColor = [UIColor whiteColor] ;
//    }

}

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    ln_expandedRowIndex=-1;
//    [ApprovalTableView reloadData];
//}

#pragma mark search bar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == AttendeeApproverSearchBarOutlet)
    {
        if(count > searchText.length)
        {
            AttendeeApprovalArr = [helper query_alldata:@"Event_Attendee"];
            AttendeeApprovalArr = [AttendeeApprovalArr filteredArrayUsingPredicate:ApprovalPredicate];
        }
        if(searchText.length == 0)
        {
            AttendeeApprovalArr = [helper query_alldata:@"Event_Attendee"];
            AttendeeApprovalArr = [AttendeeApprovalArr filteredArrayUsingPredicate:ApprovalPredicate];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            AttendeeApprovalArr = [helper query_alldata:@"Event_Attendee"];
            AttendeeApprovalArr = [AttendeeApprovalArr filteredArrayUsingPredicate:ApprovalPredicate];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.targetclass contains[c] %@) OR (SELF.speciality contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[AttendeeApprovalArr filteredArrayUsingPredicate:predicate]];
            AttendeeApprovalArr = [filtereventarr copy];
        }
         ln_expandedRowIndex=-1;
        [ApprovalTableView reloadData];
        
       
    }
}

- (IBAction)SelectAllButtonAction:(id)sender
{
    if (SelectAllButtonPressCount == 0)
    {
        SelectAllButtonPressCount++;
        [SelectAllButtonOutlet setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        [SelectedAttendee removeAllObjects];
        for (int i =0; i <[AttendeeApprovalArr count]; i++)
        {
                         [SelectedAttendee addObject:[AttendeeApprovalArr[i] valueForKey:@"attendeeid"]];
        }
        selectAllFlag = YES;
    }
    else if (SelectAllButtonPressCount == 1)
    {
        SelectAllButtonPressCount --;
         [SelectAllButtonOutlet setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];

        [SelectedAttendee removeAllObjects];
        selectAllFlag = NO;
    }
    [self.ApprovalTableView reloadData];
}
-(void)SendRequestToServer:(NSString *)status
{
    NSString *rowid;
   // [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    
    //        NSMutableDictionary *EventAttendee_dict = [[NSMutableDictionary alloc]init];
    NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
    
    for (int i=0;i<[SelectedAttendee count]; i++)
    {
        NSLog(@"SelectedAttendee %@",[SelectedAttendee objectAtIndex:i]);
        
        NSArray*attendeedetail = [AttendeeApprovalArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[SelectedAttendee objectAtIndex:i]]];
        
       // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lovtype = %@ AND lic = %@", @"E_ATT_STAT", @"Proposed"];
       // NSString *getstatus = [[[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:predicate] valueForKey:@"value"] objectAtIndex:0];
        rowid = [NSString stringWithFormat:@"EVENTATT-%@",[helper generate_rowId]];
        
        NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];
        
        [eventattendee_dict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"attendeeid"]?: @"" forKey:@"AttendeeID"];
        [eventattendee_dict setValue:appdelegate.eventid forKey:@"EventID"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"id"]?: @""  forKey:@"ID"];
//        [eventattendee_dict setValue:[[attendeedetail valueForKey:@"owner"] objectAtIndex:0] forKey:@"Owner"];
        [eventattendee_dict setValue:[self getvaluefromlic:@"E_ATT_STAT" lic:status] forKey:@"Status"];
        [eventattendee_dict setValue:status forKey:@"StatusLIC"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"firstname"]?: @"" forKey:@"FirstName"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"lastname"] ?: @"" forKey:@"LastName"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"salutation"] ?: @"" forKey:@"Salutation"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"salutationlic"]?: @"" forKey:@"SalutationLIC"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"speciality"]?: @"" forKey:@"Speciality"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"specialitylic"]?: @"" forKey:@"SpecialityLIC"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"targetclass"]?: @"" forKey:@"TargetClass"];
        [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"targetclasslic"]?: @"" forKey:@"TargetClassLIC"];
        NSString  *ApprovalFlag;
        if ([status isEqualToString:@"Approved"])
        {
            ApprovalFlag = @"false";
        }
        else if ([status isEqualToString:@"Rejected"])
        {
            ApprovalFlag = @"true";
        }
        [eventattendee_dict setValue:ApprovalFlag forKey:@"ApprovalFlag"];
        [EventAttendee_ar addObject:eventattendee_dict];
        
        
    }
    
    
    
    NSString *jsonString;
    NSError *error;
    // for (int i =0 ; i < [EventAttendee_ar count]; i++)
    //{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar options:0 error:&error];
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventAttendee" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
         NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        if (jsonData)
        {
            
           // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
           
            for(int i = 0;i<[SelectedAttendee count] ;i++)
            {
                NSManagedObjectContext  *context = [appdelegate managedObjectContext];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_Attendee" inManagedObjectContext:context];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.attendeeid like %@)",appdelegate.eventid,SelectedAttendee[i]];
                
              
                NSError *error;
                [fetchRequest setEntity:entity];
                [fetchRequest setPredicate:predicate];
                NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                NSString *ApprovalFlag;
                if ([status isEqualToString:@"Approved"])
                {
                    ApprovalFlag = [helper booltostring:0];
                }
                else if ([status isEqualToString:@"Rejected"])
                {
                    ApprovalFlag = [helper booltostring:1];
                }

                if([fetchedObjects count]>0)
                {
                    for (NSManagedObject *object in fetchedObjects)
                    {
                        [object setValue:status forKey:@"statuslic"];
                        [object setValue:[self getvaluefromlic:@"E_ATT_STAT" lic:status] forKey:@"status"];
                        [object setValue:ApprovalFlag forKey:@"approvalFlag"];
                    }
                    [context save:&error];
                }
            }
        }//jsondata end
        
        [self RefreshData];
        
        
    }
    
    
  
   // [helper removeWaitCursor];
}
- (IBAction)AcceptButtonAction:(id)sender
{
    if([SelectedAttendee count]>0)
    {
        [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
        [self SendRequestToServer:@"Approved"];
        [SelectAllButtonOutlet setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
        [SelectedAttendee removeAllObjects];
        [helper removeWaitCursor];
    }
}

- (IBAction)RejectButtonAction:(id)sender
{
    if([SelectedAttendee count]>0)
    {
        [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
        [self SendRequestToServer:@"Rejected"];
        [SelectAllButtonOutlet setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];[SelectedAttendee removeAllObjects];
        SelectAllButtonPressCount=0;
        [helper removeWaitCursor];
    }
}
- (IBAction)groupsortbtn:(id)sender {
    groupFlag=!groupFlag;
    if(groupFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"groupName" type:@"abc" Ascending:NO];
        [ApprovalTableView reloadData];
        [_groupsortimage setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"groupName" type:@"abc" Ascending:YES];
        [ApprovalTableView reloadData];
        [_groupsortimage setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
}

- (IBAction)clearsrc_btn:(id)sender
{
    _clearsrc_btn.hidden =  YES;
    
    [self RefreshData];
    
}
- (IBAction)advancesrc_btn:(id)sender
{
    senderview = @"advancesearch";
    appdelegate.attendyid = @"";
    [self.parentViewController.parentViewController performSegueWithIdentifier:@"Attendeeappr_to_AdvanceSearch" sender:self];
}

- (IBAction)salutationBtn:(id)sender {
    salutationFlag=!salutationFlag;
    if(salutationFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"salutation" type:@"abc" Ascending:NO];
        [self.ApprovalTableView reloadData];
        [_salutationImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"salutation" type:@"abc" Ascending:YES];
        [self.ApprovalTableView reloadData];
        [_salutationImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }

    
}

- (IBAction)firstNameBtn:(id)sender {
    firstNameFlag=!firstNameFlag;
    if(firstNameFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"firstname" type:@"abc" Ascending:NO];
        [self.ApprovalTableView reloadData];
        [_firstNameImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"firstname" type:@"abc" Ascending:YES];
        [self.ApprovalTableView reloadData];
        [_firstNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }

}

- (IBAction)lastNameBtn:(id)sender {
    lastNameFlag=!lastNameFlag;
    if(lastNameFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"lastname" type:@"abc" Ascending:NO];
        [self.ApprovalTableView reloadData];
        [_lastNameImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"lastname" type:@"abc" Ascending:YES];
        [self.ApprovalTableView reloadData];
        [_lastNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }

}

- (IBAction)statusBtn:(id)sender {
    statusFlag=!statusFlag;
    if(statusFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"status" type:@"abc" Ascending:NO];
        [self.ApprovalTableView reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

        
        
    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"status" type:@"abc" Ascending:YES];
        [self.ApprovalTableView reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }

}

- (IBAction)targetClassBtn:(id)sender {
    targetClassFlag=!targetClassFlag;
    if(targetClassFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"targetclass" type:@"abc" Ascending:NO];
        [self.ApprovalTableView reloadData];
        [_targetClassImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"targetclass" type:@"abc" Ascending:YES];
        [self.ApprovalTableView reloadData];
        [_targetClassImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }

}

- (IBAction)specialityBtn:(id)sender {
    specialityFlag=!specialityFlag;
    if(specialityFlag)
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"speciality" type:@"abc" Ascending:NO];
        [self.ApprovalTableView reloadData];
        [_specialityImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        AttendeeApprovalArr=[helper sortData:AttendeeApprovalArr colname:@"speciality" type:@"abc" Ascending:YES];
        [self.ApprovalTableView reloadData];
        [_specialityImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    EventAttendeePopoverViewController *eventAttendee=(EventAttendeePopoverViewController *)segue.destinationViewController;
    eventAttendee.attendeeid=AttendeeID;
    
    if ([[segue identifier] isEqualToString:@"showAttendeeAttachment"])
    {
        AttachmentViewController *Controller=segue.destinationViewController;
        Controller.senderView=@"Attendee";
        Controller.attendeeName=[NSString stringWithFormat:@"%@ %@",[AttendeeApprovalArr[m_currentIndexPath.row] valueForKey:@"firstName"],[AttendeeApprovalArr[m_currentIndexPath.row] valueForKey:@"lastname"]];
        Controller.attendeeID=[AttendeeApprovalArr[m_currentIndexPath.row] valueForKey:@"attendeeid"];
    }
    
//    //It provides anchor pointing to the table view cell
//    CGRect selectedCellRect = [self.ApprovalTableView rectForRowAtIndexPath:m_currentIndexPath];
//    //Change the Width of selectedCellRect in  order to change the x axis of popoverView
//    selectedCellRect.size.width = selectedCellRect.size.width*2;
//    eventAttendee.popoverPresentationController.sourceRect = selectedCellRect;
//    //Frame for the PopOver
//    eventAttendee.preferredContentSize = CGSizeMake(364,329);
//    eventAttendee.popoverPresentationController.delegate=self;

}

-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    [self RefreshData];
    [ApprovalTableView reloadData];
}


#pragma mark unwind method
- (IBAction)unwindToAttendeeApprovals:(UIStoryboardSegue *)segue;
{
    if ([senderview isEqualToString:@"advancesearch"])
    {
        NSPredicate *attendeepredicate = appdelegate.advsrcpredicate;
        
        if (attendeepredicate)
        {
            _clearsrc_btn.hidden = NO;
            AttendeeApprovalArr = [helper query_alldata:@"Event_Attendee"];
            ApprovalPredicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.approvalFlag like %@)",appdelegate.eventid,@"Y"];
            AttendeeApprovalArr = [[AttendeeApprovalArr filteredArrayUsingPredicate:ApprovalPredicate] filteredArrayUsingPredicate:attendeepredicate];
            [ApprovalTableView reloadData];
           }
        else
        {
            NSLog(@"No Filter associated");
        }
        
    }
    
}
-(void) setupAttendeedetails:(NSString *)attend_id backcolor:(UIColor *)backcolor
{
    
    [self.attendetailview.emailaddress_ulbl setText:[helper gettranslation:@"LBL_119"]];
    [self.attendetailview.contactno_ulbl setText:[helper gettranslation:@"LBL_070"]];
    [self.attendetailview.subtargetclass_ulbl setText:[helper gettranslation:@"LBL_254"]];
    [self.attendetailview.integrationsrc_ulbl setText:[helper gettranslation:@"LBL_148"]];
    [self.attendetailview.Attendee_status_ulbl setText:[helper gettranslation:@"LBL_242"]];
    
    
    NSArray *attdata = [helper query_alldata:@"Event_Attendee"];
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"id like %@",attend_id];
    attdata = [attdata filteredArrayUsingPredicate:aPredicate];
    if ([attdata count]>0)
    {
        [self.attendetailview.attendee_emailaddress setText:[attdata[0] valueForKey:@"emailaddress"]];
        [self.attendetailview.attendee_integrationsource setText:[attdata[0] valueForKey:@"integrationsource"]];
        [self.attendetailview.attendee_contactno setText:[attdata[0] valueForKey:@"contactnumber"]];
        self.attendetailview.SubTargetClass.text = [attdata[0] valueForKey:@"subTargetClass"];
        self.attendetailview.attendee_Status.text=[attdata[0] valueForKey:@"status"];
  
        
    }
}

- (NSInteger)dataIndexForRowIndex:(NSInteger)row
{
    if (ln_expandedRowIndex != -1 && ln_expandedRowIndex <= row)
    {
        if (ln_expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}

//- (IBAction)addattendeeClicked:(id)sender {
//    if([appdelegate.EAlstpage isEqualToString:@"Y"])
//    {
//        _groupSearchtv.text=@"";
//        appdelegate.AttendeeAddType=@"AddEventAttendee";
//        [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddattendees" sender:self];
//    }
//    else
//    {
//        [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@! %@",[helper gettranslation:@"LBL_808"],[helper gettranslation:@"LBL_628"]] action:[helper gettranslation:@"LBL_462"]];
//    }
//
//}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _groupTableView)
    {
        _groupTableView.hidden = YES;
        [_groupSearchtv resignFirstResponder];
    }
}

-(NSString *)getvaluefromlic:(NSString *)lovtype lic:(NSString *)lic
{
    NSString *value;
    NSArray *LovArr = [MasterLOV filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.lovtype like %@) AND (SELF.lic like %@)",lovtype,lic]];
    
    
    if ([LovArr count]>0)
    {
        value = [LovArr[0] valueForKey:@"value"];
    }
    
    return value;
    
}
@end
