//
//  Attendee_detail.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/16/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Attendee_detail : UIView

@property (strong, nonatomic) IBOutlet UILabel *attendee_emailaddress;
@property (strong, nonatomic) IBOutlet UILabel *attendee_integrationsource;
@property (strong, nonatomic) IBOutlet UILabel *attendee_contactno;
@property (strong, nonatomic) IBOutlet UILabel *attendee_targetclass;
@property (strong,nonatomic) IBOutlet UIImageView *attendee_profileimg;
@property(retain,nonatomic)IBOutlet UIButton *editButon;
@property(strong,nonatomic)IBOutlet UILabel *Institution;
@property(strong,nonatomic)IBOutlet UILabel *SubTargetClass;
@property(strong,nonatomic)IBOutlet UILabel *Id;

@property (strong, nonatomic) IBOutlet UILabel *emailaddress_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *contactno_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *institution_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *subtargetclass_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *integrationsrc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *assignedtopostn_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *id_ulbl;




@property (strong,nonatomic) IBOutlet UIView *mainview;

@end
