//
//  Attendee_detail.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/16/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "Attendee_detail.h"

@implementation Attendee_detail

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"Attendee_detail" owner:self options:nil];
    
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    
    self.attendee_emailaddress = (UILabel *)[mainView viewWithTag:1];
    self.attendee_integrationsource = (UILabel *)[mainView viewWithTag:2];
    self.attendee_contactno = (UILabel *)[mainView viewWithTag:3];
    self.attendee_targetclass = (UILabel *)[mainView viewWithTag:4];
    self.Institution = (UILabel *)[mainView viewWithTag:5];
    self.SubTargetClass = (UILabel *)[mainView viewWithTag:6];
    self.Id=(UILabel *)[mainView viewWithTag:7];
    
    self.attendee_profileimg  = (UIImageView *)[mainView viewWithTag:9];
    self.emailaddress_ulbl = (UILabel *)[mainView viewWithTag:1901];
    self.contactno_ulbl = (UILabel *)[mainView viewWithTag:1902];
    self.institution_ulbl = (UILabel *)[mainView viewWithTag:1903];
    self.subtargetclass_ulbl = (UILabel *)[mainView viewWithTag:1904];
    self.integrationsrc_ulbl = (UILabel *)[mainView viewWithTag:1905];
    self.assignedtopostn_ulbl = (UILabel *)[mainView viewWithTag:1906];
    self.id_ulbl=(UILabel *)[mainView viewWithTag:1907];
    
    
    
    return self;
}


@end
