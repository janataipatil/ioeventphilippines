//
//  AttendeesView.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 14/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "EventAttendeePopoverViewController.h"
#import "AttendeeApprovalsViewController.h"
#import "EventAttendee_Detail.h"
#import "AttachmentViewController.h"


@interface AttendeesView : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIPopoverPresentationControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *Attendylistview;
@property(strong,nonatomic) HelperClass *helper;
@property(strong,nonatomic) EventAttendee_Detail  *attendetailview;
@property (strong, nonatomic) IBOutlet UISearchBar *attendy_searchbar;
@property (strong, nonatomic) IBOutlet UIButton *AddAttendeeButtonOutlet;
@property (strong, nonatomic) IBOutlet UISegmentedControl *AttendeeSegmentControllerOutlet;
@property (strong, nonatomic) IBOutlet UIView *SepratorView;

-(void) refreshAttendylistview;
- (IBAction)AttendeeSegmentControllerValueChangedAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *salutation;
@property (strong, nonatomic) IBOutlet UILabel *firstname;
@property (strong, nonatomic) IBOutlet UILabel *lastname;
@property (strong, nonatomic) IBOutlet UILabel *speciality;
@property (strong, nonatomic) IBOutlet UILabel *targetclass;
@property (strong, nonatomic) IBOutlet UILabel *status;

@property (strong, nonatomic) IBOutlet UILabel *downloadProgress;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;


@property (strong, nonatomic) IBOutlet UIButton *specialityBtn;

- (IBAction)specialityBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *targetClassBtn;
- (IBAction)targetClassBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *statusBtn;
- (IBAction)statusBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *lastNameBtn;
- (IBAction)lastNameBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *firstNameBtn;
- (IBAction)firstNameBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *salutationBtn;
- (IBAction)salutationBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *salutationImg;
@property (strong, nonatomic) IBOutlet UIImageView *firstNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *lastNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *statusImg;
@property (strong, nonatomic) IBOutlet UIImageView *targetClassImg;
@property (strong, nonatomic) IBOutlet UIImageView *specialityImg;
@property (weak, nonatomic) IBOutlet UIButton *groupSortBtn;
- (IBAction)groupSortBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *groupSortImg;
@property (weak, nonatomic) IBOutlet UILabel *grouplblHeader;
@property (weak, nonatomic) IBOutlet UITextField *groupSearchtv;

@property (weak, nonatomic) IBOutlet UILabel *groupSearchlbl;
@property (weak, nonatomic) IBOutlet UITableView *groupTableView;
- (IBAction)addattendeebtnClicked:(id)sender;

@end
