//
//  AttendeesView.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 14/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "AttendeesView.h"

@interface AttendeesView ()
{
    NSArray *attendylist;
    NSInteger count;
    AppDelegate *appdelegate;
    NSPredicate *eventidpredicate;
    NSPredicate *ApprovalPredicate;
    NSString *SelectedSegment;
    AttendeeApprovalsViewController *AttendeeApprovalView;
    int RefreshTableview;
    BOOL isDataSaved;
    NSString *AttendeeID;
    BOOL salutationFlag;
    BOOL firstNameFlag;
    BOOL lastNameFlag;
    BOOL statusFlag;
    BOOL targetClassFlag;
    BOOL specialityFlag;
    BOOL groupFlag;
    
    NSString *AttendeeStatusSelectedSegment;
    NSString *semp;
    
    dispatch_semaphore_t sem_event;
    NSArray *groupArray;
   NSIndexPath *m_currentIndexPath;
    NSMutableArray *SearchgrpArray;
    NSInteger ln_expandedRowIndex;

    
    
}

@end

@implementation AttendeesView
@synthesize Attendylistview,helper,attendy_searchbar,AddAttendeeButtonOutlet,AttendeeSegmentControllerOutlet,SepratorView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Attendylistview.delegate = self;
    //Attendylistview.dataSource = self;
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    groupArray = [[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    SearchgrpArray= [[NSMutableArray alloc]init];
    [SearchgrpArray addObjectsFromArray:groupArray];
    _groupTableView.delegate=self;
    _groupTableView.dataSource=self;
    NSMutableDictionary *grpDict = [[NSMutableDictionary alloc]init];
    [grpDict setValue:@"" forKey:@"eventid"];
    [grpDict setValue:@"" forKey:@"groupid"];
    [grpDict setValue:@"ALL" forKey:@"groupname"];
    [SearchgrpArray addObject: grpDict];
    _groupSearchtv.placeholder=[helper gettranslation:@"LBL_932"];
    isDataSaved=false;
    [self RefreshData];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    attendy_searchbar.delegate = self;
    [attendy_searchbar setBackgroundImage:[UIImage new]];
    UITextField *searchfield = [attendy_searchbar valueForKey:@"_searchField"];
    searchfield.placeholder = [helper gettranslation:@"LBL_224"];
    searchfield.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:242.0/255.0f green:246.0/255.0f blue:250.0f/255.0f alpha:1.0f];
    
    salutationFlag=YES;
    firstNameFlag=YES;
    lastNameFlag=YES;
    statusFlag=YES;
    targetClassFlag=YES;
    specialityFlag=YES;
    groupFlag=YES;
    
    SelectedSegment = @"All Attendees";
    NSArray *test=appdelegate.EventUserRoleArray;
    
    if (![appdelegate.EventUserRoleArray containsObject:@"E_ATT_MNG"])
    {
        AddAttendeeButtonOutlet.hidden = YES;
    }
    ln_expandedRowIndex = -1;
    NSString *lastpageStatus=[[NSUserDefaults standardUserDefaults]valueForKey:@"EAlastPageStatus"];
    NSString *page=[[NSUserDefaults standardUserDefaults]valueForKey:@"EApageno"];
    
    appdelegate.EAlstpage=lastpageStatus;
    appdelegate.EApageno=page;
    

    
    _attendetailview = [[EventAttendee_Detail alloc] init];
    _downloadProgress.text=[helper gettranslation:@"LBL_628"];
    
    if([appdelegate.EAdownloadInProgress isEqualToString:@"NO"] && [lastpageStatus isEqualToString:@"N"])
    {
        [helper dispatchEventAttendeeThread:appdelegate.eventid];
        [ToastView showToastInParentView:self.view withText:@"Download Started" withDuaration:1.0];
    }

    [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self selector:@selector(checkProgressUpdate:) userInfo: nil repeats:YES];
    
    if([lastpageStatus isEqualToString:@"Y"])
    {
        appdelegate.EAlstpage=lastpageStatus;
        [_progressIndicator setHidden:YES];
        [_downloadProgress setHidden:YES];
    }
    
    [self langsetuptranslations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)langsetuptranslations
{
    
    [AttendeeSegmentControllerOutlet setTitle:[helper gettranslation:@"LBL_039"] forSegmentAtIndex:0];
    [AttendeeSegmentControllerOutlet setTitle:[helper gettranslation:@"LBL_172"] forSegmentAtIndex:1];
    
    
    _salutation.text = [helper gettranslation:@"LBL_218"];
    _firstname.text = [helper gettranslation:@"LBL_141"];
    _lastname.text = [helper gettranslation:@"LBL_153"];
    _speciality.text = [helper gettranslation:@"LBL_238"];
    _targetclass.text = [helper gettranslation:@"LBL_255"];
    _status.text = [helper gettranslation:@"LBL_242"];
    _grouplblHeader.text = [helper gettranslation:@"LBL_923"];
    _groupSearchlbl.text =[helper gettranslation:@"LBL_923"];
    
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _groupTableView)
    {
        _groupTableView.hidden = YES;
        [_groupSearchtv resignFirstResponder];
    }
}

#pragma mark tableview method

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 127; // 60+77
    }
    
    return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _groupTableView){
        return SearchgrpArray.count;
    }else
    return [attendylist count] + (ln_expandedRowIndex != -1 ? 1 : 0);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _groupTableView){
        SOCustomCell *customcell;
        customcell = [self.groupTableView dequeueReusableCellWithIdentifier:@"socustomcell" forIndexPath:indexPath];
        UILabel *groupName = (UILabel *)[customcell.contentView viewWithTag:1];
        
        if([[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"]){
            groupName.text = [helper gettranslation:@"LBL_037"];
        }
        else
            groupName.text = [[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
        return customcell;
    }
    else
    {
        NSInteger row = [indexPath row];
         NSInteger dataIndex = [self dataIndexForRowIndex:row];
    
        BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
        if (!ln_expandedCell)
        {
    
            SOCustomCell *customcell;
            NSString *cellIdentifier = @"customcell";
            customcell = [Attendylistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            
            NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
           
            [SelectedArray insertObject:attendylist[dataIndex] atIndex:0];
            //GROUP CHANGE
            NSArray *tempGroupArray =[groupArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupid like %@",[SelectedArray[0] valueForKey:@"groupID"]]];
            
            UIView *contentView = customcell.contentView;
            UILabel *salutation = (UILabel *) [contentView viewWithTag:20];
            UILabel *firstname = (UILabel *) [contentView viewWithTag:30];
            UILabel *lastname = (UILabel *) [contentView viewWithTag:4];
            UILabel *status = (UILabel *) [contentView viewWithTag:5];
            UILabel *ownedby = (UILabel *) [contentView viewWithTag:6];
            UILabel *speciality = (UILabel *) [contentView viewWithTag:7];
            UILabel *group = (UILabel *)[contentView viewWithTag:8];
            
            if(tempGroupArray.count > 0)
            {
                [group setText:[tempGroupArray[0] valueForKey:@"groupname"]];
            }
                else
                    group.text=@"";

//            if ([[SelectedArray[0] valueForKey:@"statuslic"] isEqualToString:@"Proposed"] || [[SelectedArray[0] valueForKey:@"statuslic"] isEqualToString:@"Rejected"] ||[[SelectedArray[0] valueForKey:@"statuslic"] isEqualToString:@"Invited"] ||[[SelectedArray[0] valueForKey:@"statuslic"] isEqualToString:@"Approved"]  )
//            {
//               // SignatureButton.hidden = YES;
//            }
//            else
//            {
//                //SignatureButton.hidden = NO;
//            }
            NSString *saltn=[SelectedArray[0] valueForKey:@"salutation"];
    
            if(saltn)
                [salutation setText:[SelectedArray[0] valueForKey:@"salutation"]];
            else
                [salutation setText:@" "];
    
            [firstname setText:[SelectedArray[0] valueForKey:@"firstname"]];
            [lastname setText:[SelectedArray[0] valueForKey:@"lastname"]];
    
            NSString *Spec=[SelectedArray[0] valueForKey:@"speciality"];
    
            [ownedby setText:[SelectedArray[0] valueForKey:@"targetclass"]];
            
            [status setText:[SelectedArray[0] valueForKey:@"status"]];
    
            if(Spec)
            {
                [speciality setText:Spec];
            }
            else
            {
                [speciality setText:@" "];
            }
            NSLog(@"attendy id is %@",[SelectedArray[0] valueForKey:@"attendeeid"]);
            NSLog(@"id is %@",[SelectedArray[0] valueForKey:@"id"]);
    
            [customcell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return customcell;
        }
    else
    {
       
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        
        UIButton *attachButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [attachButton setImage:[UIImage imageNamed:@"attach.png"] forState:UIControlStateNormal];
        attachButton.frame=CGRectMake(826, 0,48,44);
        
        UIButton *signBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [signBtn setImage:[UIImage imageNamed:@"sign.png"] forState:UIControlStateNormal];
        signBtn.frame=CGRectMake(839, 95,29,29);
        
        UIButton *inviteButton=[UIButton buttonWithType:UIButtonTypeCustom];
        inviteButton.layer.cornerRadius=5;
        inviteButton.clipsToBounds= YES;
        [inviteButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
        
        [inviteButton setTitle:[helper gettranslation:@"LBL_851"] forState:UIControlStateNormal];
        
        [inviteButton setBackgroundColor:[UIColor whiteColor]];
        [inviteButton setTitleColor:[UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:101.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        inviteButton.layer.borderWidth=1.0f;
        inviteButton.layer.borderColor=[UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:101.0/255.0 alpha:1.0].CGColor;
        
        inviteButton.frame =CGRectMake(372, 90, 114, 30);
      //  inviteButton.backgroundColor=[UIColor redColor];
        
        
        [attachButton addTarget:self action:@selector(uploadAttachment:) forControlEvents:UIControlEventTouchUpInside];
        [inviteButton addTarget:self action:@selector(inviteAttendee:) forControlEvents:UIControlEventTouchUpInside];
        [signBtn addTarget:self action:@selector(editactivityClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Proposed"] || [[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Rejected"] ||[[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Invited"] ||[[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Approved"]  )
        {
             signBtn.hidden = YES;
        }
        else
        {
             signBtn.hidden = NO;
        }
       
        UIButton *deleteButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setImage:[UIImage imageNamed:@"DeleteSchedule.png"] forState:UIControlStateNormal];
        deleteButton.frame = CGRectMake(826, 48,48,44);
        deleteButton.tag=50;
        
        UISegmentedControl *AttendeeStatus = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[helper gettranslation:@"LBL_588"], [helper gettranslation:@"LBL_593"],[helper gettranslation:@"LBL_602"],nil]];
        AttendeeStatus.frame = CGRectMake(372, 90, 415, 28);
        AttendeeStatus.selectedSegmentIndex = 0;
        AttendeeStatus.tintColor = [UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:101.0/255.0 alpha:1.0];
        AttendeeStatus.backgroundColor = [UIColor clearColor];
        AttendeeStatus.tag = 7;
        
        [AttendeeStatus addTarget:self action:@selector(segmentclicked:) forControlEvents: UIControlEventValueChanged];

        [cell.contentView addSubview:_attendetailview];
        [cell.contentView addSubview:attachButton];
        [cell.contentView addSubview:signBtn];

        if ([[attendylist[indexPath.row-1] valueForKey:@"canDelete"] isEqualToString:@"N"])
        {
            [deleteButton setHidden:YES];
        }
        else
        {
            [deleteButton setHidden:NO];
            
        }

        if([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Proposed"] || [[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Rejected"] || [[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Signed"] ||[[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"RSVP"])
        {
            [AttendeeStatus setHidden:YES];
            [self.attendetailview.attendee_Status setHidden:NO];
            [self.attendetailview.attendee_Status setText:[attendylist[indexPath.row-1] valueForKey:@"status"]];
        }
        else if ([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Approved"])
        {
            [AttendeeStatus setHidden:YES];
            [self.attendetailview.attendee_Status setHidden:YES];
            [cell.contentView addSubview:inviteButton];
        }
        else if([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Tentative"])
        {
            [self.attendetailview.attendee_Status setHidden:YES];
            AttendeeStatus.selectedSegmentIndex = 2;
            [cell.contentView addSubview:AttendeeStatus];
        }
        else if([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Accepted"])
        {
            [self.attendetailview.attendee_Status setHidden:YES];
            AttendeeStatus.selectedSegmentIndex = 0;
            [cell.contentView addSubview:AttendeeStatus];
        }
        else if([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Declined"])
        {
            [self.attendetailview.attendee_Status setHidden:YES];
            AttendeeStatus.selectedSegmentIndex = 1;
            [cell.contentView addSubview:AttendeeStatus];
        }
        else if ([[attendylist[indexPath.row-1] valueForKey:@"statuslic"] isEqualToString:@"Invited"])
        {
            [self.attendetailview.attendee_Status setHidden:YES];
            AttendeeStatus.selectedSegmentIndex = UISegmentedControlNoSegment;
            [cell.contentView addSubview:AttendeeStatus];
        }
        else
        {
            [self.attendetailview.attendee_Status setHidden:YES];
            AttendeeStatus.selectedSegmentIndex =UISegmentedControlNoSegment;
            [cell.contentView addSubview:AttendeeStatus];
        }

        
        AttendeeStatusSelectedSegment=[NSString stringWithFormat:@"%ld", (long)AttendeeStatus.selectedSegmentIndex];
        
        
        [deleteButton addTarget:self action:@selector(DeleteAttendee:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[attendylist[indexPath.row-1] valueForKey:@"canDelete"] isEqualToString:@"Y"])
        {
                 [cell.contentView addSubview:deleteButton];
        }
       
        
        [self setupAttendeedetails:[attendylist[indexPath.row-1] valueForKey:@"id"] backcolor:cell.backgroundColor];
        AttendeeID=[attendylist[indexPath.row-1] valueForKey:@"attendeeid"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
        return cell;

    }
}
}

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    ln_expandedRowIndex=-1;
//    [Attendylistview reloadData];
//}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    

}
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([[attendylist[indexPath.row] valueForKey:@"canDelete"] isEqualToString:@"N"])
//    {
//        return NO;
//    }
//    else
//    {
//        return YES;
//    }
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
   
    if(tableView == _groupTableView){
        if([[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"])
        {
            ln_expandedRowIndex=-1;
            [self RefreshData];
            _groupSearchtv.text =[helper gettranslation:@"LBL_037"];
            [Attendylistview reloadData];
        }
        else{
            attendylist = [helper query_alldata:@"Event_Attendee"];
            eventidpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
            attendylist = [attendylist filteredArrayUsingPredicate:eventidpredicate];
            attendylist = [attendylist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupid"]]];
            _groupSearchtv.text =[[SearchgrpArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
            ln_expandedRowIndex=-1;
            [Attendylistview reloadData];
        }
        [_groupTableView setHidden:YES];
        [_groupSearchtv resignFirstResponder];
    }else
    {
        AttendeeID=[attendylist[indexPath.row]valueForKey:@"attendeeid"];
    }

}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == Attendylistview){
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSInteger row = [indexPath row];

    NSLog(@"index:%lu",m_currentIndexPath.row);

    BOOL preventReopen = NO;
    
    
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        

        
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
       // cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
        m_currentIndexPath=[NSIndexPath indexPathForRow:row inSection:0];
    }
    [tableView endUpdates];
    return nil;
    }
    else
        return indexPath;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    //    cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
}




-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    [self refreshAttendylistview];

}
-(IBAction)DeleteAttendee:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action)
                              {
                                  semp = @"Yes";
                                  [self deleteAttendee];
                              }];
    [alert addAction:okaction];
    
    
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action)
                                  {
                                      semp = @"No";
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                  }];
    
    [alert addAction:cancelaction];
    [self presentViewController:alert animated:NO completion:nil];

}

-(void)deleteAttendee {
    isDataSaved = true;
    [helper showWaitCursor:[helper gettranslation:@"LBL_734"]];
    dispatch_queue_t queue=dispatch_queue_create(0,0);
    dispatch_async(queue, ^{
        // Perform long running process
        [helper serverdelete:@"E_ATTENDEE" entityid:[attendylist[m_currentIndexPath.row] valueForKey:@"id"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            NSPredicate *AttendeeIdPredicate = [NSPredicate predicateWithFormat:@"id like %@",[attendylist[m_currentIndexPath.row] valueForKey:@"id"]];
            [helper delete_predicate:@"Event_Attendee" predicate:AttendeeIdPredicate];
            ln_expandedRowIndex=-1;
            
            
            [self refreshAttendylistview];
            
        });
    });
}

-(IBAction)uploadAttachment:(id)sender
{
   // AttachmentViewController *attach=[[AttachmentViewController alloc]init];
   // attach.senderView=@"Attendee";
  appdelegate.attendeeName=[NSString stringWithFormat:@"%@ %@",[attendylist[m_currentIndexPath.row] valueForKey:@"firstname"],[attendylist[m_currentIndexPath.row] valueForKey:@"lastname"]];
   // attach.attendeeID=[attendylist[m_currentIndexPath.row] valueForKey:@"attendeappeid"];
    appdelegate.AttachmentType=@"Attendee";
    appdelegate.attendyid=[attendylist[m_currentIndexPath.row] valueForKey:@"id"];
   
    [self.parentViewController performSegueWithIdentifier:@"showAttendeeAttachment" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAttendeeAttachment"])
    {
        AttachmentViewController *Controller=segue.destinationViewController;
        Controller.senderView=@"Attendee";
        Controller.attendeeName=appdelegate.attendeeName;
        Controller.attendeeID=appdelegate.attendyid;
    }
}

#pragma mark edit activity method
-(IBAction)editactivityClicked:(UIButton *)sender
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_630"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
    
        
        appdelegate.attendyid = [attendylist[m_currentIndexPath.row] valueForKey:@"id"];
        
        appdelegate.swipevar = @"Sign";
        [self.parentViewController performSegueWithIdentifier:@"EventDetailToSign" sender:self];
    }
}


/*- (IBAction)attendeeClicked:(id)sender
{
    if([appdelegate.EAlstpage isEqualToString:@"Y"])
    {
        _groupSearchtv.text=@"";
         appdelegate.AttendeeAddType=@"AddEventAttendee";
        [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddattendees" sender:self];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@! %@",[helper gettranslation:@"LBL_808"],[helper gettranslation:@"LBL_628"]] action:[helper gettranslation:@"LBL_462"]];
    }
}*/

#pragma mark search bar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == attendy_searchbar)
    {
        if(count > searchText.length)
        {
            attendylist = [helper query_alldata:@"Event_Attendee"];
            attendylist = [attendylist filteredArrayUsingPredicate:eventidpredicate];
        }
        if(searchText.length == 0)
        {
            attendylist = [helper query_alldata:@"Event_Attendee"];
            attendylist = [attendylist filteredArrayUsingPredicate:eventidpredicate];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            attendylist = [helper query_alldata:@"Event_Attendee"];
            attendylist = [attendylist filteredArrayUsingPredicate:eventidpredicate];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.targetclass contains[c] %@) OR (SELF.speciality contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[attendylist filteredArrayUsingPredicate:predicate]];
            attendylist = [filtereventarr copy];
        }
        ln_expandedRowIndex=-1;
       [Attendylistview reloadData];
       
       
       
    }
}
-(void)RefreshData
{
    NSArray *list = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    
    for (int i=0; i<list.count; i++) {
         [helper UpdateGroupForAttendee:[list[i] valueForKey:@"id"] groupName:[helper getGroupNameforGroupID:[list[i] valueForKey:@"groupID"]]];
    }
    
    attendylist = [helper query_alldata:@"Event_Attendee"];
    eventidpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    attendylist = [attendylist filteredArrayUsingPredicate:eventidpredicate];
    
    [Attendylistview reloadData];
    
    if (isDataSaved) {
        isDataSaved = false;
        [helper removeWaitCursor];
    }
}
- (IBAction)AttendeeSegmentControllerValueChangedAction:(id)sender
{
    switch (AttendeeSegmentControllerOutlet.selectedSegmentIndex)
    {
        case 0:
            if (![appdelegate.EventUserRoleArray containsObject:@"E_ATT_MNG"])
            {
                AddAttendeeButtonOutlet.hidden = YES;
            }
            else
            {
                AddAttendeeButtonOutlet.hidden = NO;
            }
            attendy_searchbar.hidden = NO;
            SepratorView.hidden = NO;
            [AttendeeApprovalView.view removeFromSuperview];
            [self RefreshData];
            break;
        case 1:
            AddAttendeeButtonOutlet.hidden = YES;
            attendy_searchbar.hidden = YES;
            SepratorView.hidden = YES;
            _groupSearchtv.text=@"";
            [self SetAttendeeApprovalView];
            break;
        default:
            break;
    }

}
-(void)SetAttendeeApprovalView
{
    AttendeeApprovalView = [self.storyboard instantiateViewControllerWithIdentifier:@"AttendeeApprovalView"];
    [AttendeeApprovalView.view setFrame:CGRectMake(0, 36, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:AttendeeApprovalView];
    [self.view addSubview:AttendeeApprovalView.view];
    [AttendeeApprovalView didMoveToParentViewController:self];
}
-(void) refreshAttendylistview
{
    attendylist = [helper query_alldata:@"Event_Attendee"];
    attendylist = [attendylist filteredArrayUsingPredicate:eventidpredicate];
    ln_expandedRowIndex=-1;
    [Attendylistview reloadData];
    if (isDataSaved) {
        isDataSaved = false;
        [helper removeWaitCursor];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
}
- (void)reloadTable
{
    
    [Attendylistview reloadData];
}
-(IBAction)unwindtoEventAttendylist:(UIStoryboardSegue *)segue
{
    //RefreshTableview = 1;
    [self refreshAttendylistview];
}
//
//- (void)dismissViewControllerAnimated:(BOOL)flag
//                           completion:(void (^)(void))completion
//{
//
//}
-(void)checkProgressUpdate:(NSTimer *)timer
{
    
    
    if([appdelegate.EAlstpage isEqualToString:@"Y"])
    {
        [_progressIndicator setHidden:YES];
        [_downloadProgress setHidden:YES];
       // attendylist=[helper query_alldata:@"Event_Attendee"];
        //[Attendylistview reloadData];
    }
    else if([appdelegate.EAlstpage isEqualToString:@"N"] && [appdelegate.downloadInProgress isEqualToString:@"NO"])
    {
        //attendylist=[helper query_alldata:@"Event_Attendee"];
     //   NSLog(@"EAAttendeeListCount:%d",attendylist.count);
        
        // _downloadProgress.text=[NSString stringWithFormat:@"%u Completed",page/attendeelist.count];
       // [Attendylistview reloadData];
        
    }
    
    
}
- (IBAction)specialityBtn:(id)sender {
    
    specialityFlag=!specialityFlag;
    if(specialityFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"speciality" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_specialityImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"speciality" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_specialityImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }

}
- (IBAction)targetClassBtn:(id)sender {
    targetClassFlag=!targetClassFlag;
    if(targetClassFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"targetclass" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_targetClassImg setImage:[UIImage imageNamed:@"descending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"targetclass" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_targetClassImg setImage:[UIImage imageNamed:@"ascending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
}
- (IBAction)statusBtn:(id)sender {
    statusFlag=!statusFlag;
    if(statusFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"status" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"descending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"status" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"ascending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}
- (IBAction)lastNameBtn:(id)sender {
    lastNameFlag=!lastNameFlag;
    if(lastNameFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"lastname" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_lastNameImg setImage:[UIImage imageNamed:@"descending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"lastname" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_lastNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}
- (IBAction)firstNameBtn:(id)sender {
    firstNameFlag=!firstNameFlag;
    if(firstNameFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"firstname" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_firstNameImg setImage:[UIImage imageNamed:@"descending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"firstname" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_firstNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}
- (IBAction)salutationBtn:(id)sender {
    salutationFlag=!salutationFlag;
    if(salutationFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"salutation" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_salutationImg setImage:[UIImage imageNamed:@"descending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"salutation" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_salutationImg setImage:[UIImage imageNamed:@"ascending.png"]];
         [_groupSortImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}

-(void) setupAttendeedetails:(NSString *)attend_id backcolor:(UIColor *)backcolor
{
    
    [self.attendetailview.emailaddress_ulbl setText:[helper gettranslation:@"LBL_119"]];
    [self.attendetailview.contactno_ulbl setText:[helper gettranslation:@"LBL_070"]];
    [self.attendetailview.subtargetclass_ulbl setText:[helper gettranslation:@"LBL_254"]];
    [self.attendetailview.integrationsrc_ulbl setText:[helper gettranslation:@"LBL_148"]];
    [self.attendetailview.Attendee_status_ulbl setText:[helper gettranslation:@"LBL_242"]];
    
//    [self.attendetailview.eventAttendeeSegmentController setTitle:[helper gettranslation:@"LBL_588"] forSegmentAtIndex:0];
//    [self.attendetailview.eventAttendeeSegmentController setTitle:[helper gettranslation:@"LBL_593"] forSegmentAtIndex:1];
//    [self.attendetailview.eventAttendeeSegmentController setTitle:[helper gettranslation:@"LBL_602"] forSegmentAtIndex:2];
    
    NSArray *attdata = [helper query_alldata:@"Event_Attendee"];
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"id like %@",attend_id];
    attdata = [attdata filteredArrayUsingPredicate:aPredicate];
    if ([attdata count]>0)
    {
        [self.attendetailview.attendee_emailaddress setText:[attdata[0] valueForKey:@"emailaddress"]];
        [self.attendetailview.attendee_integrationsource setText:[attdata[0] valueForKey:@"integrationsource"]];
        [self.attendetailview.attendee_contactno setText:[attdata[0] valueForKey:@"contactnumber"]];
        


        self.attendetailview.SubTargetClass.text = [attdata[0] valueForKey:@"subTargetClass"];
     
    }
}

- (IBAction)inviteAttendee:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action)
                              {
                                  semp = @"Yes";
                                  [self saveData];
                              }];
    [alert addAction:okaction];
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action)
                                  {
                                      semp = @"No";
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                  }];
    [alert addAction:cancelaction];
    [self presentViewController:alert animated:NO completion:nil];
}

-(void)saveData {
    isDataSaved = true;
    [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
    dispatch_queue_t queue=dispatch_queue_create(0,0);
    dispatch_async(queue, ^{
        // Perform long running process
        [self SendRequestToServer:@"Invited"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [self RefreshData];
        });
    });
}
- (void)segmentclicked:(UISegmentedControl *)segment
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //    NSInteger i = [segment tag];
    if(segment.selectedSegmentIndex == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action)
                                  {
                                      semp = @"Yes";
                                      [self saveDataFromSegementClicked:@"Accepted"];
                                      
                                  }];
        [alert addAction:okaction];
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action)
                                      {
                                          semp = @"No";
                                          segment.selectedSegmentIndex=[AttendeeStatusSelectedSegment intValue];
                                          
                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                      }];
        [alert addAction:cancelaction];
        [self presentViewController:alert animated:NO completion:nil];
    }
    else if(segment.selectedSegmentIndex == 1)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action)
                                  {
                                      semp = @"Yes";
                                      [self saveDataFromSegementClicked:@"Declined"];
                                  }];
        [alert addAction:okaction];
        
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action)
                                      {
                                          semp = @"No";
                                          segment.selectedSegmentIndex=[AttendeeStatusSelectedSegment intValue];
                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                      }];
        
        [alert addAction:cancelaction];
        [self presentViewController:alert animated:NO completion:nil];
    }
    else if (segment.selectedSegmentIndex == 2)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action)
                                  {
                                      semp = @"Yes";
                                      [self saveDataFromSegementClicked:@"Tentative"];
                                  }];
        [alert addAction:okaction];
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action)
                                      {
                                          semp = @"No";
                                          segment.selectedSegmentIndex=[AttendeeStatusSelectedSegment intValue];
                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                      }];
        
        [alert addAction:cancelaction];
        [self presentViewController:alert animated:NO completion:nil];
    }
}

-(void)saveDataFromSegementClicked:(NSString *)sendValue {
    isDataSaved = true;
    [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
    dispatch_queue_t queue=dispatch_queue_create(0,0);
    dispatch_async(queue, ^{
        // Perform long running process
        [self SendRequestToServer:sendValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            ln_expandedRowIndex=-1;
            [self RefreshData];
            //            [self.Attendylistview reloadData];
            [self tableView:Attendylistview willSelectRowAtIndexPath:m_currentIndexPath];
        });
    });
}



-(void)SendRequestToServer:(NSString *)status
{
    @autoreleasepool
    {
        NSString *rowid;
    
        NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
    
        rowid = [NSString stringWithFormat:@"EVENTATT-%@",[helper generate_rowId]];
    
        NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];
    
        NSArray *AttendeeList = [helper query_alldata:@"Event_Attendee"];
        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"attendeeid like %@",AttendeeID];
        AttendeeList = [AttendeeList filteredArrayUsingPredicate:aPredicate];
        NSString *Attendee_Status = [helper getvaluefromlic:@"E_ATT_STAT" lic:status];
    
        [eventattendee_dict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"attendeeid"]?: @"" forKey:@"AttendeeID"];
        [eventattendee_dict setValue:appdelegate.eventid forKey:@"EventID"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"id"]?: @""  forKey:@"ID"];
    
        [eventattendee_dict setValue:Attendee_Status forKey:@"Status"];
        [eventattendee_dict setValue:status forKey:@"StatusLIC"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"firstname"]?: @"" forKey:@"FirstName"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"lastname"] ?: @"" forKey:@"LastName"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"salutation"] ?: @"" forKey:@"Salutation"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"salutationlic"]?: @"" forKey:@"SalutationLIC"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"speciality"]?: @"" forKey:@"Speciality"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"specialitylic"]?: @"" forKey:@"SpecialityLIC"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"targetclass"]?: @"" forKey:@"TargetClass"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"targetclasslic"]?: @"" forKey:@"TargetClassLIC"];
        NSString  *ApprovalFlag;
        if ([status isEqualToString:@"Approved"])
        {
            ApprovalFlag = @"false";
        }
        else if ([status isEqualToString:@"Rejected"])
        {
            ApprovalFlag = @"true";
        }
        [eventattendee_dict setValue:ApprovalFlag forKey:@"ApprovalFlag"];
        [EventAttendee_ar addObject:eventattendee_dict];
    
    
        NSString *jsonString;
        NSError *error;
    
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar options:0 error:&error];
        if (! jsonData)
        {
            // NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventAttendee" pageno:@"" pagecount:@"" lastpage:@""];
    
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
            [alert show];
        }
        else
        {
            NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            if (jsonData)
            {
            
                NSManagedObjectContext  *context = [appdelegate managedObjectContext];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_Attendee" inManagedObjectContext:context];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.attendeeid like %@)",appdelegate.eventid,AttendeeID];
            
            
                NSError *error;
                [fetchRequest setEntity:entity];
                [fetchRequest setPredicate:predicate];
                NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                NSString *ApprovalFlag;
                if ([status isEqualToString:@"Approved"])
                {
                    ApprovalFlag = [helper booltostring:0];
                }
                else  if ([status isEqualToString:@"Rejected"])
            {
                ApprovalFlag = [helper booltostring:1];
            }
            
            if([fetchedObjects count]>0)
            {
                for (NSManagedObject *object in fetchedObjects)
                {
                    [object setValue:status forKey:@"statuslic"];
                    [object setValue:[helper getvaluefromlic:@"E_ATT_STAT" lic:status] forKey:@"status"];
                    [object setValue:ApprovalFlag forKey:@"approvalFlag"];
                }
                [context save:&error];
            }
        }
    }
}
}
- (NSInteger)dataIndexForRowIndex:(NSInteger)row
{
    if (ln_expandedRowIndex != -1 && ln_expandedRowIndex <= row)
    {
        if (ln_expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}

- (IBAction)groupSortBtn:(id)sender {
    groupFlag=!groupFlag;
    if(groupFlag)
    {
        attendylist=[helper sortData:attendylist colname:@"groupName" type:@"abc" Ascending:NO];
        [self.Attendylistview reloadData];
        [_groupSortImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    else
    {
        attendylist=[helper sortData:attendylist colname:@"groupName" type:@"abc" Ascending:YES];
        [self.Attendylistview reloadData];
        [_groupSortImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_firstNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_specialityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_targetClassImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_lastNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_salutationImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _groupSearchtv){
        [_groupSearchtv resignFirstResponder];
        [_groupTableView setHidden:NO];
        [_groupTableView reloadData];
    }
}

- (IBAction)addattendeebtnClicked:(id)sender {
    if([appdelegate.EAlstpage isEqualToString:@"Y"])
    {
        _groupSearchtv.text=@"";
        appdelegate.AttendeeAddType=@"AddEventAttendee";
        [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddattendees" sender:self];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@! %@",[helper gettranslation:@"LBL_808"],[helper gettranslation:@"LBL_628"]] action:[helper gettranslation:@"LBL_462"]];
    }
}
@end
