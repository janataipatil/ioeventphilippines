//
//  CommentView.h
//  NN_EMSapp
//
//  Created by iWizards XI on 18/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "AppDelegate.h"
#import "HelperClass.h"

@interface CommentView : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *headerTitle;
@property (strong, nonatomic) IBOutlet UIView *MainView;

@property (strong, nonatomic) IBOutlet UILabel *writeComment_lbl;

@property (strong, nonatomic) IBOutlet UITextView *cmntTextView;

@property (strong, nonatomic) IBOutlet UILabel *message;

@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *approveBtn;
- (IBAction)approveBtn:(id)sender;

@property(strong,nonatomic)NSString *ID;
@property(strong,nonatomic)NSString *expenseID;
@property(strong,nonatomic)NSString *Comment;
@property(strong,nonatomic)NSString *statuslic;
@property(strong,nonatomic)NSString *transaction_type;
@property(strong,nonatomic)NSString *txType;

@end
