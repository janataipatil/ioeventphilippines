//
//  CommentView.m
//  NN_EMSapp
//
//  Created by iWizards XI on 18/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "CommentView.h"

@interface CommentView ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
      NSManagedObjectContext *context;
}

@end

@implementation CommentView

- (void)viewDidLoad {
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context=[appdelegate managedObjectContext];

    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.MainView.bounds];
    self.MainView.layer.masksToBounds = NO;
    self.MainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.MainView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.MainView.layer.shadowOpacity = 0.3f;
    self.MainView.layer.shadowPath = shadowPath.CGPath;

    
    
    if([_txType isEqualToString:@"Approve"])
    {
        [_approveBtn setTitle:[helper gettranslation:@"LBL_853"] forState:UIControlStateNormal];
    }
    else
    {
        [_approveBtn setTitle:[helper gettranslation:@"LBL_521"]  forState:UIControlStateNormal];
    }
    
  //  _cmntTextView.layer.masksToBounds=YES;
    _cmntTextView.layer.borderWidth=1.0;
    _cmntTextView.layer.cornerRadius=3.0;
    _cmntTextView.layer.borderColor=[[UIColor colorWithRed:227/255.0 green:229/255.0 blue:229/255.0 alpha:1.0] CGColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)setupLangTranslations
{
    _writeComment_lbl.text=[helper gettranslation:@"LBL_282"];
    _message.text=[helper gettranslation:@"LBL_740"];
    [_cancelBtn setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_approveBtn setTitle:[helper gettranslation:@"LBL_853"] forState:UIControlStateNormal];
    
    _message.text=[helper gettranslation:@"LBL_740"];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)changeExpenseApprovalStatus:(NSString *)ID Comment:(NSString *)Comment StatusLIC:(NSString *)StatusLIC expenseID:(NSString *)expenseid
{
   
    NSString *process_result = @"Success";
    NSString *AESPrivateKey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
    
    [PayloadDict setValue:StatusLIC forKey:@"StatusLIC"];
    [PayloadDict setValue:Comment forKey:@"Comment"];
    [PayloadDict setValue:expenseid forKey:@"ExpenseID"];
    [PayloadDict setValue:ID forKey:@"ID"];
    
    NSMutableArray *PayloadArray = [[NSMutableArray alloc]init];
    [PayloadArray addObject:PayloadDict];
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArray options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetExpenseApproval" pageno:@"" pagecount:@"" lastpage:@""];
    NSMutableDictionary *TRANXAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        
        NSString *authresponse =  [helper decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        

    }
   
    
    
 
    
}





- (IBAction)cancelBtn:(id)sender {
    
    [self performSegueWithIdentifier:@"BackToAddExpense" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

    
    }
- (IBAction)approveBtn:(id)sender
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
    
    [self changeExpenseApprovalStatus:_ID Comment:_cmntTextView.text StatusLIC:_statuslic expenseID:_expenseID];
  
        [self DownloadExpenseOnExpenseSubmition];
        [self DownloadApprovalsOnExpenseSubmition];
        [helper removeWaitCursor];

        [self performSegueWithIdentifier:@"BackToAddExpense" sender:self];
        [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    
    
    
    
    }
-(void)DownloadExpenseOnExpenseSubmition
{
    NSString *process_result = @"Success";
    NSString *AESPrivateKey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [helper create_trans_payload:@"EXPENSE" fltervalue:appdelegate.eventid];
    NSDictionary *EVENTLOV_msgbody = [helper create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetEventDetails" pageno:@"" pagecount:@"" lastpage:@""];
    
    
    
    NSMutableDictionary *TRANXAPI_response = [helper invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        
        NSString *authresponse =  [helper decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [helper deletewitheventid:@"Event_Expense" value:appdelegate.eventid];
        
        NSArray *EventExpenseArr = [jsonData valueForKey:@"EventExpense"];
        [helper insertEventExpense:EventExpenseArr];
    }
}
-(void)DownloadApprovalsOnExpenseSubmition
{
    NSString *process_result = @"Success";
    NSString *AESPrivateKey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [helper create_trans_payload:@"EXP_AFTER_APPROVALS" fltervalue:appdelegate.eventid];
    NSDictionary *EVENTLOV_msgbody = [helper create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetEventDetails" pageno:@"" pagecount:@"" lastpage:@""];
    
   
    
    NSMutableDictionary *TRANXAPI_response = [helper invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        
        NSString *authresponse =  [helper decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSArray *EventExpenseApprovalArr=[jsonData valueForKey:@"ExpenseApproval"];
        
        [helper deletewitheventid:@"Expense_Approvals" value:appdelegate.eventid];
        
        if(EventExpenseApprovalArr.count>0)
            
            [helper insertEventExpenseApproval:EventExpenseApprovalArr];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
     [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.view];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

@end
