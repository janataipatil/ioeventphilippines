//
//  CreateSpeakerViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/13/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SOCustomCell.h"
#import "SLKeyChainStore.h"
#import "EventSpeakerViewController.h"



@class HelperClass;
@interface CreateSpeakerViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIDocumentPickerDelegate>
{
    
    IBOutlet UITableView *salutation_tableview;
}
@property (strong, nonatomic) IBOutlet UIView *PersonalDetailView;


@property (strong, nonatomic) IBOutlet UITextField *firstname_tv;
@property (strong, nonatomic) IBOutlet UITextField *lastname_tv;
@property (strong, nonatomic) IBOutlet UITextField *speciality_tv;
@property (strong, nonatomic) IBOutlet UITextField *designation_tv;
@property (strong, nonatomic) IBOutlet UITextField *email_tv;
@property (strong, nonatomic) IBOutlet UITextField *contactno_tv;
@property (strong, nonatomic) IBOutlet UITextField *company_tv;
@property (strong, nonatomic) IBOutlet UITextField *salutation_textfield;
@property (strong, nonatomic) IBOutlet UISwitch *internalbtn_switch;

@property (strong, nonatomic) IBOutlet UITextField *departnment_tv;

@property (strong, nonatomic) IBOutlet UITextField *addressline1_tv;
@property (strong, nonatomic) IBOutlet UITextField *addressline2_tv;
//@property (strong, nonatomic) IBOutlet UITextField *city_tv;
//@property (strong, nonatomic) IBOutlet UITextField *state_tv;
@property (strong, nonatomic) IBOutlet UITextField *city_tv;
@property (strong, nonatomic) IBOutlet UITextField *state_tv;
@property (strong, nonatomic) IBOutlet UITextField *currency_tv;
@property (strong, nonatomic) IBOutlet UITextField *costperhour_tv;

@property (strong, nonatomic) IBOutlet UITextField *country_tv;
@property (strong, nonatomic) IBOutlet UILabel *resume_title_lbl;

@property (strong, nonatomic) IBOutlet UITextField *zipcode_tv;
@property (strong, nonatomic) IBOutlet UILabel *change_imgLBL;
@property (strong, nonatomic) IBOutlet UILabel *currency_lbl;
@property (strong, nonatomic) IBOutlet UILabel *BillingDetails_lbl;
@property (strong, nonatomic) IBOutlet UILabel *conversionDetail_lbl;
@property (strong, nonatomic) IBOutlet UITextField *conversionDetail_tv;

@property (strong, nonatomic) IBOutlet UITextField *passportnumber_tv;
@property (strong, nonatomic) IBOutlet UILabel *totalAmt_lbl;
@property (strong, nonatomic) IBOutlet UILabel *totalAmt_tv;


//@property (strong, nonatomic) IBOutlet UITextField *validfrom_tv;
//@property (strong, nonatomic) IBOutlet UITextField *validtill_tv;
@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UITextView *executivesumery_textview;
@property(strong,nonatomic)NSString *speakerid;

@property (strong, nonatomic) IBOutlet UIButton *SalutationButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *SpecialityButtonOutlet;

@property (strong, nonatomic) IBOutlet UIView *pickerview;
- (IBAction)fileattachment_btn_action:(id)sender;
- (IBAction)city_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *CountryButtonOutlet;

- (IBAction)internalflagvaluechanged:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *StateButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *StatusButtonOutlet;
@property (strong, nonatomic) IBOutlet UITextField *status_tv;
@property (strong, nonatomic) IBOutlet UIButton *CityButtonOutlet;

@property (strong, nonatomic) IBOutlet UIButton *valid_till_btn_outlet;

- (IBAction)salutationbtnaction:(id)sender;
- (IBAction)status_btn:(id)sender;
- (IBAction)speciality_btn:(id)sender;
- (IBAction)valid_from_btn:(id)sender;

- (IBAction)valid_till_btn:(id)sender;

- (IBAction)state_btn:(id)sender;
//@property (strong, nonatomic) IBOutlet UIButton *resume_btn_outlet;

- (IBAction)country_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *SaveButtonOutlet;

//@property (strong, nonatomic) IBOutlet UILabel *resumeupload_lbl;
@property (strong, nonatomic) IBOutlet UIButton *valid_from_btn_outlet;

@property (strong, nonatomic) IBOutlet UIButton *CameraButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *ResumeButtonOutlet;

- (IBAction)save_btn:(id)sender;
- (IBAction)cancel_btn:(id)sender;
- (IBAction)dismiss_btn:(id)sender;
- (IBAction)selectphotobtnaction:(id)sender;
-(void)setupspeakerview;
@property (strong, nonatomic) IBOutlet UIImageView *faceimage;



@property (strong, nonatomic) IBOutlet UILabel *titleview_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *changeimgbtn_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *resume_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *personalinfo_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *salutation_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *fname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *lname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *speciality_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *company_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *department_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *designation_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *emailaddr_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *contactno_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *internal_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *passportdtl_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *passportno_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *validfrm_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *validtill_ulbl;


@property (strong, nonatomic) IBOutlet UILabel *Address_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrline1_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrline2_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrcity_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrzipcode_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrstate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrcountry_ulbl;
@property (strong, nonatomic) IBOutlet UIView *DropDownView;
@property (strong, nonatomic) IBOutlet UILabel *Dropdownlbl;

@property (strong, nonatomic) IBOutlet UILabel *exesummary_ulbl;

@end
