//
//  CreateSpeakerViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/13/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "CreateSpeakerViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "HSDatePickerViewController.h"

@interface CreateSpeakerViewController ()<HSDatePickerViewControllerDelegate>
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSString *selecteddate;
    NSString *getdate;
    NSArray *Eventlovlist;
    NSDateFormatter *formatter;
    NSArray *speakerlist;
    NSString *validfromoriginalvalue;
    NSString *validtilloriginalvalue;
    dispatch_semaphore_t sem_att;
    NSData *imageData;
    NSString *attachedimagename;
    NSMutableData *responsedata;
    NSString *imagestorageurl;
    NSString *salutationlic,*specialitylic,*statuslic,*citylic,*statelic,*countrylic;
//    NSString *salutationlic,*specialitylic,*statuslic,*citylic,*statelic,*countrylic;
    float ConversionRate;
    NSString *AESPrivateKey;
    NSString *filebase64string;
    NSString *resumefilename;
    NSString *filepathflag,*imagepathflag;
    NSString *filestorageurl;
    SLKeyChainStore *keychain;
    NSString *dropdownflag;
    NSString *internal;
    NSUInteger count;
    dispatch_queue_t squeue;
    BOOL imageselcted,resumeselected;
    NSString *uploadflag;
    NSString *pathextension ;
    NSData *resumedata;
    NSString *transaction_type;
    HSDatePickerViewController *hsdpvc;
    NSString *startEndDate;
    NSUInteger stringlength;
    NSString *currencylic;
    float totalAmt;
    
    NSString *currencySymbol;
    NSString *StorageContainer;
    NSString *StorageAccount;
    NSString *StorageKey;
    
    
}

@property (nonatomic, strong) NSDate *selectedDate;

@end

@implementation CreateSpeakerViewController

@synthesize firstname_tv,lastname_tv,speciality_tv,email_tv,contactno_tv,faceimage,helper,designation_tv,departnment_tv,company_tv,addressline1_tv,addressline2_tv,city_tv,state_tv,country_tv,zipcode_tv,passportnumber_tv,executivesumery_textview,pickerview,salutation_textfield,speakerid,internalbtn_switch,costperhour_tv,status_tv,resume_title_lbl,valid_from_btn_outlet,valid_till_btn_outlet,SaveButtonOutlet,CameraButtonOutlet,ResumeButtonOutlet,CountryButtonOutlet,CityButtonOutlet,StateButtonOutlet,StatusButtonOutlet,SalutationButtonOutlet,SpecialityButtonOutlet,Dropdownlbl,DropDownView,PersonalDetailView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
   // [validfrom_tv addTarget:self action:@selector(validfromdatedate) forControlEvents:UIControlEventEditingDidBegin];
    //[validfrom_tv addTarget:self action:@selector(validfromdatedate) forControlEvents:UIControlEventEditingChanged];
    
   // [validtill_tv addTarget:self action:@selector(validtodatedate) forControlEvents:UIControlEventEditingDidBegin];
   // [validtill_tv addTarget:self action:@selector(validtodatedate) forControlEvents:UIControlEventEditingChanged];
    
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    
  
    StorageContainer = [helper query_data:@"name" inputdata:@"StorageContainer" entityname:@"S_Config_Table"];
    StorageAccount = [helper query_data:@"name" inputdata:@"StorageAccount" entityname:@"S_Config_Table"];
   StorageKey = [helper query_data:@"name" inputdata:@"StorageKey" entityname:@"S_Config_Table"];
    
    NSString *currencyCode=[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"];
    
//    NSString *currencyCode = @"EUR";
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:currencyCode];
    currencySymbol = [NSString stringWithFormat:@"%@",currencyCode];
    
    NSLog(@"%@ -> %@", currencyCode, [[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode]);
    appdelegate.CurrencyCode = currencyCode;
    hsdpvc=[[HSDatePickerViewController alloc]init];
    
    salutation_tableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    salutation_tableview.layer.borderWidth = 1;
   
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setDateFormat: @"dd MMM yyyy"];
    internalbtn_switch.transform = CGAffineTransformMakeScale(0.60, 0.60);
    [self langsetuptranslations];
    if(speakerid)
    {
        [self setupspeakerview];
        _change_imgLBL.text=[helper gettranslation:@"LBL_063"];
       // [_changeimgbtn_ulbl setTitle:[helper gettranslation:@"LBL_063"] forState:UIControlStateNormal];
        transaction_type = @"Edit Speaker";
    }
    else
    {
        transaction_type = @"Create New Speaker";
        speakerid = [helper generate_rowId];
       // internalbtn_switch.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
        internalbtn_switch.onTintColor = [helper DarkBlueColour];
        _conversionDetail_tv.text=@"1";
        _currency_tv.text=currencyCode;
        
        internal = @"Y";
        _change_imgLBL.text=[helper gettranslation:@"LBL_578"];
        //[_changeimgbtn_ulbl setTitle:[helper gettranslation:@"LBL_578"] forState:UIControlStateNormal];
        status_tv.text=[helper getvaluefromlic:@"REC_STAT" lic:@"Active"];
    }
    squeue = dispatch_queue_create("iwiz.serial.Queue", NULL);
    [salutation_textfield addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [status_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [speciality_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [city_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [state_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [country_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] || (![appdelegate.UserRolesArray containsObject:@"SPK_R"] && ![appdelegate.UserRolesArray containsObject:@"SPK_W"]) )
    {
        SaveButtonOutlet.userInteractionEnabled = NO;
        salutation_textfield.userInteractionEnabled = NO;
        internalbtn_switch.userInteractionEnabled = NO;
        costperhour_tv.userInteractionEnabled = NO;
        status_tv.userInteractionEnabled = NO;
        firstname_tv.userInteractionEnabled = NO;
        lastname_tv.userInteractionEnabled = NO;
        speciality_tv.userInteractionEnabled = NO;
        company_tv.userInteractionEnabled = NO;
        designation_tv.userInteractionEnabled = NO;
        departnment_tv.userInteractionEnabled = NO;
        CameraButtonOutlet.userInteractionEnabled = NO;
        ResumeButtonOutlet.userInteractionEnabled = NO;
        email_tv.userInteractionEnabled = NO;
        contactno_tv.userInteractionEnabled = NO;
        passportnumber_tv.userInteractionEnabled = NO;
        valid_from_btn_outlet.userInteractionEnabled = NO;
        valid_till_btn_outlet.userInteractionEnabled = NO;
        addressline1_tv.userInteractionEnabled = NO;
        city_tv.userInteractionEnabled = NO;
        state_tv.userInteractionEnabled = NO;
        addressline2_tv.userInteractionEnabled = NO;
        zipcode_tv.userInteractionEnabled = NO;
        country_tv.userInteractionEnabled = NO;
        executivesumery_textview.userInteractionEnabled = NO;
        CountryButtonOutlet.userInteractionEnabled = NO;
        StateButtonOutlet.userInteractionEnabled = NO;
        CityButtonOutlet.userInteractionEnabled = NO;
        StatusButtonOutlet.userInteractionEnabled = NO;
        SalutationButtonOutlet.userInteractionEnabled = NO;
        SpecialityButtonOutlet.userInteractionEnabled = NO;
    }
    if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] && [appdelegate.UserRolesArray containsObject:@"SPK_W"])
    {
        SaveButtonOutlet.userInteractionEnabled = YES;
        salutation_textfield.userInteractionEnabled = YES;
        internalbtn_switch.userInteractionEnabled = YES;
        costperhour_tv.userInteractionEnabled = YES;
        status_tv.userInteractionEnabled = YES;
        firstname_tv.userInteractionEnabled = YES;
        lastname_tv.userInteractionEnabled = YES;
        speciality_tv.userInteractionEnabled = YES;
        company_tv.userInteractionEnabled = YES;
        designation_tv.userInteractionEnabled = YES;
        departnment_tv.userInteractionEnabled = YES;
        CameraButtonOutlet.userInteractionEnabled = YES;
        ResumeButtonOutlet.userInteractionEnabled = YES;
        email_tv.userInteractionEnabled = YES;
        contactno_tv.userInteractionEnabled = YES;
        passportnumber_tv.userInteractionEnabled = YES;
        valid_from_btn_outlet.userInteractionEnabled = YES;
        valid_till_btn_outlet.userInteractionEnabled = YES;
        addressline1_tv.userInteractionEnabled = YES;
        city_tv.userInteractionEnabled = YES;
        state_tv.userInteractionEnabled = YES;
        addressline2_tv.userInteractionEnabled = YES;
        zipcode_tv.userInteractionEnabled = YES;
        country_tv.userInteractionEnabled = YES;
        executivesumery_textview.userInteractionEnabled = YES;
        CountryButtonOutlet.userInteractionEnabled = YES;
        StateButtonOutlet.userInteractionEnabled = YES;
        CityButtonOutlet.userInteractionEnabled = YES;
        StatusButtonOutlet.userInteractionEnabled = YES;
        SalutationButtonOutlet.userInteractionEnabled = YES;
        SpecialityButtonOutlet.userInteractionEnabled = YES;
    }
    
    //CGPoint point =  [salutation_textfield.superview convertPoint:salutation_textfield.frame.origin toView:pickerview];
    // CGPoint point = [salutation_textfield convertPoint:CGPointZero toView:PersonalDetailView];
    
    //NSLog(@"cgpoint is %ld",point);
}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _titleview_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_118"]];
    

    _change_imgLBL.text=[helper gettranslation:@"LBL_063"];
   // [_changeimgbtn_ulbl setTitle:[helper gettranslation:@"LBL_063"] forState:UIControlStateNormal];
     resume_title_lbl.text = [helper gettranslation:@"LBL_213"];
    
    _personalinfo_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_198"]];
    
    _salutation_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_218"]];
    [self markasrequired:_salutation_ulbl];
    
    NSLog(@"%@",[helper gettranslation:@"LBL_141"]);
    _fname_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_141"]];
    [self markasrequired:_fname_ulbl];
    
    _lname_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_153"]];
    [self markasrequired:_lname_ulbl];
    
    _speciality_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_238"]];
   // [self markasrequired:_speciality_ulbl];
    
    _company_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_067"]];
    [self markasrequired:_company_ulbl];
    
    _department_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_092"]];
    [self markasrequired:_department_ulbl];
    
    _designation_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_095"]];
  //  [self markasrequired:_designation_ulbl];
    
    //_costperhour_lbl.text = [NSString stringWithFormat:@"%@ ",[helper gettranslation:@"LBL_071"]];
   // [self markasrequired:_costperhour_lbl];
    
    _emailaddr_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_119"]];
    [self markasrequired:_emailaddr_ulbl];
    
    _contactno_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_070"]];
    [self markasrequired:_contactno_ulbl];
    
    _status_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_242"]];
    [self markasrequired:_status_ulbl];
    
    _internal_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_149"]];
    
    _passportdtl_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_193"]];
    _passportno_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_194"]];
    _validfrm_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_274"]];
    _validtill_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_275"]];
    
    
    _Address_ulbl.text = [helper gettranslation:@"LBL_030"];
    _addrline1_ulbl.text = [helper gettranslation:@"LBL_031"];
    _addrline2_ulbl.text = [helper gettranslation:@"LBL_032"];
    _addrcity_ulbl.text = [helper gettranslation:@"LBL_064"];
    _addrzipcode_ulbl.text = [helper gettranslation:@"LBL_283"];
    _addrstate_ulbl.text = [helper gettranslation:@"LBL_241"];
    _addrcountry_ulbl.text = [helper gettranslation:@"LBL_072"];
    
    _exesummary_ulbl.text = [helper gettranslation:@"LBL_134"];
    
    salutation_textfield.placeholder = [helper gettranslation:@"LBL_757"];
    speciality_tv.placeholder = [helper gettranslation:@"LBL_757"];
    city_tv.placeholder = [helper gettranslation:@"LBL_757"];;
    state_tv.placeholder =[helper gettranslation:@"LBL_757"];
    country_tv.placeholder = [helper gettranslation:@"LBL_757"];
    
}

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}


-(void)setupspeakerview
{
  
    speakerlist = [helper query_alldata:@"Speaker"];
    speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",speakerid]];
    if([speakerlist count]>0)
    {
      
        firstname_tv.text = [speakerlist[0] valueForKey:@"firstname"];
        lastname_tv.text = [speakerlist[0] valueForKey:@"lastname"];
        speciality_tv.text = [speakerlist[0] valueForKey:@"speciality"];
        email_tv.text  = [speakerlist[0] valueForKey:@"emailaddress"];
        contactno_tv.text= [speakerlist[0] valueForKey:@"contactno"];
        designation_tv.text = [speakerlist[0] valueForKey:@"designation"];
        departnment_tv.text = [speakerlist[0] valueForKey:@"department"];
        company_tv.text = [speakerlist[0] valueForKey:@"company"];
        
        _currency_tv.text = [helper getvaluefromlic:@"CC_CODE" lic:[speakerlist[0] valueForKey:@"currencylic"]];
        _totalAmt_tv.text = [NSString stringWithFormat:@"%@ %@",currencySymbol,[speakerlist[0] valueForKey:@"totalAmount"]];
        costperhour_tv.text = [speakerlist[0] valueForKey:@"chargeperhour"];
        _conversionDetail_tv.text = [speakerlist[0] valueForKey:@"conversionRate"];
        
        addressline1_tv.text= [speakerlist[0] valueForKey:@"streetaddress1"];
        addressline2_tv.text = [speakerlist[0] valueForKey:@"streetaddress2"];
        city_tv.text = [speakerlist[0] valueForKey:@"city"];
        state_tv.text = [speakerlist[0] valueForKey:@"state"];
        
        country_tv.text = [speakerlist[0] valueForKey:@"country"];
        zipcode_tv.text = [speakerlist[0] valueForKey:@"zipcode"];
        passportnumber_tv.text = [speakerlist[0] valueForKey:@"passportnumber"];
        [valid_from_btn_outlet setTitle:[helper formatingdate:[speakerlist[0] valueForKey:@"passportvalidfrom"] datetime:@"Passportdate"] forState:UIControlStateNormal];
        [valid_till_btn_outlet setTitle:[helper formatingdate:[speakerlist[0] valueForKey:@"passportvalidto"] datetime:@"Passportdate"] forState:UIControlStateNormal];
        executivesumery_textview.text = [speakerlist[0] valueForKey:@"summary"];
        salutation_textfield.text = [speakerlist[0] valueForKey:@"salutation"];
        imagestorageurl = [speakerlist[0] valueForKey:@"imageicon"];
        filestorageurl = [speakerlist[0] valueForKey:@"cvid"];
        
        if (filestorageurl)
        {
            resumefilename = [speakerlist[0] valueForKey:@"cvFileName"];
            NSURL *resume_url = [NSURL URLWithString: filestorageurl];
            pathextension = [resume_url pathExtension];
        }

        NSString *cvname = [speakerlist[0] valueForKey:@"cvFileName"];
        if ([cvname length]>0)
        {
            cvname = [NSString stringWithFormat:@"%@%@",[[cvname substringToIndex:1]uppercaseString],[[cvname substringFromIndex:1]lowercaseString]];
            resume_title_lbl.text =cvname ;
            
            
        }
        
            NSLog(@"File storage url is %@",filestorageurl);
      
        //costperhour_tv.text = [helper stringtocurrency:[speakerlist[0] valueForKey:@"chargeperhour"]];
        costperhour_tv.text = [speakerlist[0] valueForKey:@"chargeperhour"];
        status_tv.text = [speakerlist[0] valueForKey:@"status"];
        if([[speakerlist[0] valueForKey:@"internalFlag"] isEqualToString:@"Y"])
        {
            //internalbtn_switch.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
            internalbtn_switch.onTintColor = [helper DarkBlueColour];
            internal = @"Y";
            [internalbtn_switch setOn:YES];
            
        }
        else
        {
           
            [internalbtn_switch setOn:NO];
            internal = @"N";
        }
        
        
         NSLog(@"internal flag in createspeaker class %@",[speakerlist[0] valueForKey:@"internalFlag"]);
                          
        if([speakerlist[0] valueForKey:@"location_url"])
        {
            UIImage *speakerprofilepic = [UIImage imageWithContentsOfFile:[speakerlist[0] valueForKey:@"location_url"]];
            if(speakerprofilepic)
            {
                faceimage.image = speakerprofilepic;
            }
            else
            {
               faceimage.image = [UIImage imageNamed:@"User.png"];
            }
         
        }
        else
        {
            faceimage.image = [UIImage imageNamed:@"User.png"];
        
        }
    }
}
-(void)viewWillAppear:(BOOL)animated
{
//    UIView *firstname_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    firstname_tv.leftView = firstname_tvpaddingview;
//    firstname_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *lastname_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    lastname_tv.leftView = lastname_tvpaddingview;
//    lastname_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *speciality_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    speciality_tv.leftView = speciality_tvpaddingview;
//    speciality_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView * departnment_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    departnment_tv.leftView =  departnment_tvpaddingview;
//    departnment_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *designation_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    designation_tv.leftView = designation_tvpaddingview;
//    designation_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *company_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    company_tv.leftView = company_tvpaddingview;
//    company_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView * email_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    email_tv.leftView =  email_tvpaddingview;
//    email_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *contactno_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    contactno_tv.leftView = contactno_tvpaddingview;
//    contactno_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *addressline1_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    addressline1_tv.leftView = addressline1_tvpaddingview;
//    addressline1_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *addressline2_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    addressline2_tv.leftView = addressline2_tvpaddingview;
//    addressline2_tv.leftViewMode = UITextFieldViewModeAlways;
//
//    UIView *city_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    city_tv.leftView = city_tvpaddingview;
//    city_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *state_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    state_tv.leftView = state_tvpaddingview;
//    state_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *country_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    country_tv.leftView = country_tvpaddingview;
//    country_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *zipcode_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    zipcode_tv.leftView = zipcode_tvpaddingview;
//    zipcode_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *passportnumber_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    passportnumber_tv.leftView = passportnumber_tvpaddingview;
//    passportnumber_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//    UIView *costperhourpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    costperhour_tv.leftView = costperhourpaddingview;
//    costperhour_tv.leftViewMode = UITextFieldViewModeAlways;
//    
//   /* UIView *validfrom_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    valid_from_btn_outlet.leftView = validfrom_tvpaddingview;
//    valid_from_btn_outlet.leftViewMode = UITextFieldViewModeAlways;*/
//    
//   /* UIView *validtill_tvpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    validtill_tv.leftView = validtill_tvpaddingview;
//    validtill_tv.leftViewMode = UITextFieldViewModeAlways;*/
//    
//    UIView * salutation_textfieldpaddingview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
//    salutation_textfield.leftView =  salutation_textfieldpaddingview;
//    salutation_textfield.leftViewMode = UITextFieldViewModeAlways;
    


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma doucment picker method
- (IBAction)fileattachment_btn_action:(id)sender
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
       /* UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.content"]
        inMode:UIDocumentPickerModeImport];
        documentPicker.delegate = self;
        documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:documentPicker animated:YES completion:nil];*/
    }
}
- (IBAction)internalflagvaluechanged:(id)sender
{
    if([internal isEqualToString:@"Y"])
    {
        internal = @"N";
        [internalbtn_switch setOn:NO];
    }
    else if ([internal isEqualToString:@"N"])
    {
        internal = @"Y";
        //internalbtn_switch.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
        internalbtn_switch.onTintColor = [helper DarkBlueColour];
    }
    NSLog(@"internal value is %@",internal);
}
/*- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
            resumeselected = 1;
            NSError *error;
            NSData *image_Data = [NSData dataWithContentsOfURL:url options:0 error:&error];
            resumedata = [NSData dataWithContentsOfURL:url options:0 error:&error];
            filebase64string= [image_Data base64EncodedStringWithOptions:kNilOptions];
            resumefilename = [url lastPathComponent];
            //replace whitespace to underscore in filename
            resumefilename =  [resumefilename stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            NSURL *resume_url = [NSURL URLWithString: resumefilename];
            pathextension = [resume_url pathExtension];
            NSString *cvname = resumefilename;
            if ([resumefilename length]>0)
            {
                cvname = [NSString stringWithFormat:@"%@%@",[[cvname substringToIndex:1]uppercaseString],[[cvname substringFromIndex:1]lowercaseString]];
                resume_title_lbl.text =cvname ;
            }
    }
}*/
#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == salutation_tableview)
    {
        
        CGFloat height = salutation_tableview.contentSize.height;
        CGFloat maxHeight = salutation_tableview.superview.frame.size.height - salutation_tableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = salutation_tableview.frame;
            CGRect WidthFrame = CGRectMake(0, 24, 117, 130);
            CGRect labelFrame = CGRectMake(7, 0, 100, 24);
            if([dropdownflag isEqualToString:@"salutationdropdown"])
            {
               
                frame.origin.x = 338;
                frame.origin.y = 0;
                frame.size.width = 117;
                WidthFrame.size.width = 117;
                labelFrame.size.width = 110;
            }
            if([dropdownflag isEqualToString:@"statusdropdown"])
            {
                frame.origin.x = 735;
                frame.origin.y = 0;
                frame.size.width = 117;
                WidthFrame.size.width = 117;
                labelFrame.size.width = 108;
            }
            if([dropdownflag  isEqualToString:@"specialitydropdown"])
            {
                frame.origin.x = 338;
                frame.origin.y = 73;
                frame.size.width = 237;
                WidthFrame.size.width = 237;
                labelFrame.size.width = 229;
            }
            if([dropdownflag isEqualToString:@"currencydropdown"])
            {
                frame.origin.x = 165;
                frame.origin.y = 412;
                frame.size.width = 130;
                WidthFrame.size.width = 130;
                labelFrame.size.width = 123;
            }
            if([dropdownflag isEqualToString:@"countrydropdown"])
            {
                frame.origin.x = 439;
                frame.origin.y = 400;
                frame.size.width = 190;
                WidthFrame.size.width = 190;
                labelFrame.size.width = 183;
            }
            if([dropdownflag isEqualToString:@"statedropdown"])
            {
                frame.origin.x = 664;
                frame.origin.y = 400;
                frame.size.width = 190;
                WidthFrame.size.width = 190;
                labelFrame.size.width = 183;
            }
            if([dropdownflag isEqualToString:@"citydropdown"])
            {
                frame.origin.x = 664;
                //frame.origin.y = 468;
                frame.origin.y = 431;
                frame.size.width = 190;
                WidthFrame.size.width = 190;
                labelFrame.size.width = 183;
            }
            
           // frame.size.height = height+15;
            DropDownView.frame = frame;
            salutation_tableview.frame =  WidthFrame;
            Dropdownlbl.frame = labelFrame;
        }];
    }

}
- (IBAction)salutationbtnaction:(id)sender
{
    salutation_tableview.hidden = YES;
    dropdownflag = @"salutationdropdown";
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    salutation_tableview.hidden = NO;
}

- (IBAction)status_btn:(id)sender
{
    salutation_tableview.hidden = YES;
    dropdownflag = @"statusdropdown";
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    salutation_tableview.hidden = NO;
}

- (IBAction)speciality_btn:(id)sender
{
    salutation_tableview.hidden = YES;
    dropdownflag = @"specialitydropdown";
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    salutation_tableview.hidden = NO;
}

- (IBAction)valid_from_btn:(id)sender
{
    startEndDate = @"fromdate";
    hsdpvc=[[HSDatePickerViewController alloc]init];
    hsdpvc.delegate = self;
    
    if([passportnumber_tv.text length]>0)
    {
        
        NSString *eventEndDate=[self.valid_till_btn_outlet.titleLabel text];
        
        hsdpvc.delegate = self;
        hsdpvc.minDate=[NSDate date];
        if(eventEndDate.length>0)
        {
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
            
            NSDate *evntEndDate=[dateFormatter1 dateFromString:eventEndDate];
            
            hsdpvc.maxDate=evntEndDate;
            
            
        }

        [self presentViewController:hsdpvc animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_610"] delegate:self  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        [alert show];

    }

    
}

- (IBAction)valid_till_btn:(id)sender
{
    startEndDate = @"todate";
    hsdpvc=[[HSDatePickerViewController alloc]init];
    hsdpvc.delegate=self;
    
    if([passportnumber_tv.text length]>0)
    {
        startEndDate = @"todate";
        NSString *activityStartDate=[self.valid_from_btn_outlet.titleLabel text];
        if(activityStartDate.length>0)
        {
            NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
            [dateformat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateformat setDateFormat:@"dd MMM yyyy HH:mm"];
            
            NSDate *actStartDate=[dateformat dateFromString:activityStartDate];
            hsdpvc.date = actStartDate;
            
            hsdpvc.minDate = actStartDate;
            
        }
        [self presentViewController:hsdpvc animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_610"] delegate:self  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        [alert show];
        
    }
    
    
}
- (IBAction)city_btn:(id)sender
{
    salutation_tableview.hidden = YES;
    dropdownflag = @"citydropdown";
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    salutation_tableview.hidden = NO;
}


- (IBAction)state_btn:(id)sender
{
    salutation_tableview.hidden = YES;
    dropdownflag = @"statedropdown";
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    salutation_tableview.hidden = NO;
}

- (IBAction)country_btn:(id)sender
{
    salutation_tableview.hidden = YES;
    dropdownflag = @"countrydropdown";
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
    Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"]];
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    salutation_tableview.hidden = NO;
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [Eventlovlist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"socustomcell";
    customcell = [salutation_tableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    //customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
   
//    customcell.textLabel.text = [Eventlovlist[indexPath.row] valueForKey:@"Value"];
//    customcell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12.f];
//    customcell.textLabel.textAlignment = NSTextAlignmentCenter;
//    customcell.textLabel.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
//   
    
    
    UIView *ContentView = customcell.contentView;
    UILabel *Valuelbl = (UILabel *)[ContentView viewWithTag:1];
    if ([dropdownflag isEqualToString:@"salutationdropdown"])
    {
        CGRect Frame = Valuelbl.frame;
        Frame.size.width = 117;
        Valuelbl.frame =  Frame;
    }
    else if ([dropdownflag isEqualToString:@"statusdropdown"])
    {
        CGRect Frame = Valuelbl.frame;
        Frame.size.width = 117;
        Valuelbl.frame =  Frame;
    }
    else if ([dropdownflag isEqualToString:@"specialitydropdown"])
    {
        CGRect Frame = Valuelbl.frame;
        Frame.size.width = 237;
        Valuelbl.frame =  Frame;
    }
    
   
    Valuelbl.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
    return customcell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([dropdownflag isEqualToString:@"salutationdropdown"])
    {
        salutation_textfield.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        salutationlic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        [salutation_textfield resignFirstResponder];
    }
    if([dropdownflag isEqualToString:@"statusdropdown"])
    {
        status_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        statuslic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        [status_tv resignFirstResponder];

    }
    if([dropdownflag isEqualToString:@"currencydropdown"])
    {
        _currency_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        currencylic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        [_currency_tv resignFirstResponder];
        
    }
    if([dropdownflag isEqualToString:@"specialitydropdown"])
    {
        speciality_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        specialitylic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        [speciality_tv resignFirstResponder];

    }
    if([dropdownflag isEqualToString:@"citydropdown"])
    {
        city_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        citylic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        [city_tv resignFirstResponder];

    }
    if([dropdownflag isEqualToString:@"statedropdown"])
    {
        state_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        city_tv.text=@"";
        statelic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        [state_tv resignFirstResponder];

    }
//    if([dropdownflag isEqualToString:@""])
//    {
//       _currency_tv=[Eventlovlist[indexPath.row] valueForKey:@"value"];
//        currencylic=[Eventlovlist[indexPath.row] valueForKey:@"lic"];
//        [_currency_tv resignFirstResponder];
//    }
    
    if([dropdownflag isEqualToString:@"countrydropdown"])
    {
        NSArray *State = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
        State = [State filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"COUNTRY" value:country_tv.text]]];
        State = [State filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"value like %@",state_tv.text]];
        
        country_tv.text =[Eventlovlist[indexPath.row] valueForKey:@"value"];
        countrylic = [Eventlovlist[indexPath.row] valueForKey:@"lic"];
        
        
        
        if(State.count >0)
        {
            NSArray *City = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
            City = [City filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"STATE" value:state_tv.text]]];
            City = [City filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"value like %@",city_tv.text]];
            
            if([City count]>0)
            {
                state_tv.text=@"";
                city_tv.text=@"";
            }
            else
            {
                state_tv.text=@"";
            }
        }
        
        
        [country_tv resignFirstResponder];

    }
    DropDownView.hidden = YES;
    
}
-(void)resizeimage
{
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250000;
    
    imageData = UIImageJPEGRepresentation(faceimage.image, compression);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(faceimage.image, compression);
    }
    //write the imagedata to local path
    
    
    
}
#pragma mark upload attachmment method
-(void) upload_attachmentdata:(NSString *) filename base64:(NSString *)base64
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];

    [dict setValue:[NSString stringWithFormat:@"%@.%@",[NSString stringWithString:[helper generate_rowId]],[filename pathExtension]] forKey:@"FileName"];
    [dict setValue:base64 forKey:@"Base64"];
    [dict setValue:StorageContainer forKey:@"Container"];
    [dict setValue:StorageAccount forKey:@"Account"];
    [dict setValue:StorageKey forKey:@"Key"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSData *postData = [jsonStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSLog(@"storage uri %@",[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]);
    [request setURL:[NSURL URLWithString:[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]]];
    [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
    [request setValue:[keychain stringForKey:@"SLToken"] forHTTPHeaderField:@"X-ZUMO-AUTH"];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn)
    {
        responsedata = [NSMutableData data];
    }
    else
    {
        
       // [helper printloginconsole:@"2" logtext:@"Connection could not be made in upload attachment request"];
        NSLog(@"Connection could not be made in upload attachment request");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responsedata setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responsedata appendData:data];
    
}
// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
    dispatch_semaphore_signal(sem_att);
    
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    NSString *jsonArray = [NSJSONSerialization JSONObjectWithData:responsedata options:NSJSONReadingAllowFragments error:&error];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[jsonArray dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    
    if([uploadflag isEqualToString:@"imageupload"])
    {
        imagestorageurl = [json valueForKey:@"FileURI"];
        
    }
    else if ([uploadflag isEqualToString:@"RemovePhoto"])
    {
        imagestorageurl = @"";
    }
    if ([uploadflag isEqualToString:@"fileupload"])
    {
        filestorageurl = [json valueForKey:@"FileURI"];
    }
    
    
     dispatch_semaphore_signal(sem_att);
}

#pragma mark checking value changed
-(NSDictionary *)checktheupdatedvalues
{
    
    
        NSMutableDictionary *updatedict = [[NSMutableDictionary alloc]init];
    
        if([speakerlist count]>0)
        {
            if([firstname_tv.text length]>0 || [[speakerlist[0] valueForKey:@"firstname"] length]>0)
            {
                if(![firstname_tv.text isEqualToString:[speakerlist[0] valueForKey:@"firstname"]])
                {
                    [updatedict setValue:firstname_tv.text forKey:@"FirstName"];
                }
            }
            if([status_tv.text length]>0 || [[speakerlist[0] valueForKey:@"status"] length]>0)
            {
                if(![status_tv.text isEqualToString:[speakerlist[0] valueForKey:@"status"]])
                {
                    [updatedict setValue:[helper getLic:@"REC_STAT" value:status_tv.text] forKey:@"StatusLIC"];
                    [updatedict setValue:status_tv.text forKey:@"Status"];
                }
            }
            if([lastname_tv.text length] >0 || [[speakerlist[0] valueForKey:@"lastname"] length]>0 )
            {
                if(![lastname_tv.text isEqualToString:[speakerlist[0] valueForKey:@"lastname"]])
                {
                    [updatedict setValue:lastname_tv.text forKey:@"LastName"];
                }
            }
            if([salutation_textfield.text length] > 0 || [[speakerlist[0] valueForKey:@"salutation"] length]>0 )
            {
                if(![salutation_textfield.text isEqualToString:[speakerlist[0] valueForKey:@"salutation"]])
                {
                [updatedict setValue:[helper getLic:@"CONTACT_SAL" value:salutation_textfield.text] forKey:@"SalutationLIC"];
                [updatedict setValue:salutation_textfield.text forKey:@"Salutation"];
                }
            }
          
            if(![internal isEqualToString:[speakerlist[0] valueForKey:@"internalFlag"]])
            {
                [updatedict setValue:internal forKey:@"InternalFlag"];
            }
            if([costperhour_tv.text length] >0 || [[speakerlist[0] valueForKey:@"chargeperhour"] length]>0)
            {
                if(![costperhour_tv.text isEqualToString:[speakerlist[0] valueForKey:@"chargeperhour"]])
                {
                    [updatedict setValue:costperhour_tv.text forKey:@"ChargePerHour"];
                }
            }
            
            if([_conversionDetail_tv.text length]>0 || [[speakerlist[0] valueForKey:@"conversionRate"] length]>0)
            {
                if(![_conversionDetail_tv.text isEqualToString:[speakerlist[0] valueForKey:@"conversionRate"]])
                {
                    [updatedict setValue:_conversionDetail_tv.text forKey:@"ConversionRate"];
                }
            }
            
            if([speciality_tv.text length]>0 || [[speakerlist[0] valueForKey:@"speciality"] length]>0)
            {
                    if(![speciality_tv.text isEqualToString:[speakerlist[0] valueForKey:@"speciality"]])
                    {
                        [updatedict setValue:speciality_tv.text forKey:@"Speciality"];
                    }
            }
            if([company_tv.text length]>0 || [[speakerlist[0] valueForKey:@"company"] length]>0 )
            {
                if(![company_tv.text isEqualToString:[speakerlist[0] valueForKey:@"company"]])
                {
                    [updatedict setValue:company_tv.text forKey:@"Company"];
                }
            }
            if([designation_tv.text length]>0 || [[speakerlist[0] valueForKey:@"designation"] length]>0)
            {
                if(![designation_tv.text isEqualToString:[speakerlist[0] valueForKey:@"designation"]])
                {
                    [updatedict setValue:designation_tv.text forKey:@"Designation"];
                }
            }
            if([departnment_tv.text length]>0 || [[speakerlist[0] valueForKey:@"department"] length]>0)
            {
                if(![departnment_tv.text isEqualToString:[speakerlist[0] valueForKey:@"department"]])
                {
                    [updatedict setValue:departnment_tv.text forKey:@"Department"];
                }
            }
            if([email_tv.text length]>0 || [[speakerlist[0] valueForKey:@"emailaddress"] length]>0)
            {
                if(![email_tv.text isEqualToString:[speakerlist[0] valueForKey:@"emailaddress"]])
                {
                    [updatedict setValue:email_tv.text forKey:@"EmailAddress"];
                }
            }
            if([contactno_tv.text length] >0 || [[speakerlist[0] valueForKey:@"contactno"] length]>0)
            {
                if(![contactno_tv.text isEqualToString:[speakerlist[0] valueForKey:@"contactno"]])
                {
                    [updatedict setValue:contactno_tv.text forKey:@"ContactNo"];
                }
            }
            if([passportnumber_tv.text length]>0 || [[speakerlist[0] valueForKey:@"passportnumber"] length]>0)
            {
                if(![passportnumber_tv.text isEqualToString:[speakerlist[0] valueForKey:@"passportnumber"]])
                {
                    [updatedict setValue:passportnumber_tv.text forKey:@"PassportNumber"];
                }
            }
            if([valid_from_btn_outlet.titleLabel.text length] >0 || [[helper formatingdate:[speakerlist[0] valueForKey:@"passportvalidfrom"] datetime:@"Passportdate"] length]>0 )
            {
                    if(![valid_from_btn_outlet.titleLabel.text isEqualToString:[helper formatingdate:[speakerlist[0] valueForKey:@"passportvalidfrom"] datetime:@"Passportdate"]])
                    {
                        [updatedict setValue:[helper converingformateddatetooriginaldate:valid_from_btn_outlet.titleLabel.text datetype:@"PassportFormTillDate"] forKey:@"PassportValidFrom"];
                    }
            }
            if([valid_till_btn_outlet.titleLabel.text length]>0 || [[helper formatingdate:[speakerlist[0] valueForKey:@"passportvalidto"] datetime:@"Passportdate"] length]>0)
            {
                if(![valid_till_btn_outlet.titleLabel.text isEqualToString:[helper formatingdate:[speakerlist[0] valueForKey:@"passportvalidto"] datetime:@"Passportdate"]])
                {
                [updatedict setValue:[helper converingformateddatetooriginaldate:valid_till_btn_outlet.titleLabel.text datetype:@"PassportFormTillDate"] forKey:@"PassportValidTo"];
                }
            }
            if([addressline1_tv.text length]>0 || [[speakerlist[0] valueForKey:@"streetaddress1"] length]>0 )
            {
                if(![addressline1_tv.text isEqualToString:[speakerlist[0] valueForKey:@"streetaddress1"]])
                {
                    [updatedict setValue:addressline1_tv.text forKey:@"StreetAddress1"];
                }
            }
            if([addressline2_tv.text length]>0 || [[speakerlist[0] valueForKey:@"streetaddress2"] length]>0)
            {
                if(![addressline2_tv.text isEqualToString:[speakerlist[0] valueForKey:@"streetaddress2"]])
                {
                    [updatedict setValue:addressline2_tv.text forKey:@"StreetAddress2"];
                }
            }
            if([city_tv.text length]>0 || [[speakerlist[0] valueForKey:@"city"] length]>0 )
            {
                if(![city_tv.text isEqualToString:[speakerlist[0] valueForKey:@"city"]])
                {
                    [updatedict setValue:[helper getLic:@"CITY" value:city_tv.text] forKey:@"CityLIC"];
                    [updatedict setValue:city_tv.text forKey:@"City"];
                }
            }
            if([zipcode_tv.text length]>0 || [[speakerlist[0] valueForKey:@"zipcode"] length]>0)
            {
                if(![zipcode_tv.text isEqualToString:[speakerlist[0] valueForKey:@"zipcode"]])
                {
                    [updatedict setValue:zipcode_tv.text forKey:@"ZipCode"];
                }
            }
            if([state_tv.text length]>0 || [[speakerlist[0] valueForKey:@"state"] length]>0 )
            {
                if(![state_tv.text isEqualToString:[speakerlist[0] valueForKey:@"state"]])
                {
                    [updatedict setValue:[helper getLic:@"STATE" value:state_tv.text] forKey:@"StateLIC"];
                    [updatedict setValue:state_tv.text forKey:@"State"];
                }
            }
            if([country_tv.text length] >0 || [[speakerlist[0] valueForKey:@"country"] length]>0)
            {
                if(![country_tv.text isEqualToString:[speakerlist[0] valueForKey:@"country"]])
                {
                    [updatedict setValue:[helper getLic:@"COUNTRY" value:country_tv.text] forKey:@"CountryLIC"];
                    [updatedict setValue:country_tv.text forKey:@"Country"];
                }
            }
            if([executivesumery_textview.text length]>0 || [[speakerlist[0] valueForKey:@"summary"] length]>0)
            {
                if(![executivesumery_textview.text isEqualToString:[speakerlist[0] valueForKey:@"summary"]])
                {
                    [updatedict setValue:executivesumery_textview.text forKey:@"Summary"];
                }
            }
            if([imagestorageurl length]>0 || [[speakerlist[0] valueForKey:@"imageicon"] length]>0)
           {
               if(![imagestorageurl isEqualToString:[speakerlist[0] valueForKey:@"imageicon"]])
               {
                    [updatedict setValue:imagestorageurl forKey:@"ImageIcon"];
               }
           }
           if([filestorageurl length]>0 || [[speakerlist[0] valueForKey:@"cvid"] length]>0)
            {
                if(![filestorageurl isEqualToString:[speakerlist[0] valueForKey:@"cvid"]])
                {
                    [updatedict setValue:filestorageurl forKey:@"CVID"];
                }
            }
            if([currencylic length]>0 || [[speakerlist[0] valueForKey:@"currencylic"] length]>0)
            {
                if(![currencylic isEqualToString:[speakerlist[0] valueForKey:@"currencylic"]])
                {
                    [updatedict setValue:currencylic forKey:@"ExpenseCurrencyLIC"];
                }
            }
            
            
        }
    return updatedict;
}
-(NSMutableDictionary *)createdictionary
{
    specialitylic =[helper getLic:@"SPECIALITY" value:country_tv.text];
    statuslic=[helper getLic:@"REC_STAT" value:status_tv.text];
    statelic=[helper getLic:@"STATE" value:state_tv.text];
    citylic=[helper getLic:@"CITY" value:city_tv.text];
    countrylic=[helper getLic:@"COUNTRY" value:country_tv.text];
    currencylic=[helper getLic:@"CC_CODE" value:_currency_tv.text];
    salutationlic=[helper getLic:@"CONTACT_SAL" value:salutation_textfield.text];
    
    
    NSMutableDictionary *speaker_dict = [[NSMutableDictionary alloc]init];
    [speaker_dict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
    [speaker_dict setValue:[helper stringtobool:internal] forKey:@"InternalFlag"];
    if(imagestorageurl)
    {
        [speaker_dict setValue:imagestorageurl forKey:@"ImageIcon"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"ImageIcon"];
    }
    if(filestorageurl)
    {
        [speaker_dict setValue:filestorageurl forKey:@"CVID"];
        [speaker_dict setValue:resumefilename forKey:@"CVFileName"];
        [speaker_dict setValue:pathextension forKey:@"CVFileType"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"CVID"];
        [speaker_dict setValue:@"" forKey:@"CVFileName"];
        [speaker_dict setValue:@"" forKey:@"CVFileType"];
    }
//    if(speakerid)
//    {
//        [speaker_dict setValue:speakerid forKey:@"SpeakerID"];
//    }
//    else
//    {
        [speaker_dict setValue:speakerid forKey:@"SpeakerID"];
    //}
    if ([email_tv.text length]>0)
    {
         [speaker_dict setValue:email_tv.text forKey:@"EmailAddress"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"EmailAddress"];
    }
    if ([contactno_tv.text length]>0)
    {
        [speaker_dict setValue:contactno_tv.text forKey:@"ContactNo"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"ContactNo"];
    }
//    if ([costperhour_tv.text length]>0)
//    {
//        [speaker_dict setValue:[helper CurrencytoFloat:costperhour_tv.text] forKey:@"ChargePerHour"];
//    }
//    else
//    {
//        [speaker_dict setValue:@"" forKey:@"ChargePerHour"];
//    }
    if ([departnment_tv.text length]>0)
    {
        [speaker_dict setValue:departnment_tv.text forKey:@"Department"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"Department"];
    }
    if ([firstname_tv.text length]>0)
    {
         [speaker_dict setValue:firstname_tv.text forKey:@"FirstName"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"FirstName"];
    }
    if ([lastname_tv.text length]>0)
    {
        [speaker_dict setValue:lastname_tv.text forKey:@"LastName"];
    }
    else
    {
        [speaker_dict setValue:lastname_tv.text forKey:@"LastName"];
    }
    if ([addressline1_tv.text length]>0)
    {
        [speaker_dict setValue:addressline1_tv.text forKey:@"StreetAddress1"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"StreetAddress1"];
    }
    if ([addressline2_tv.text length]>0)
    {
         [speaker_dict setValue:addressline2_tv.text forKey:@"StreetAddress2"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"StreetAddress2"];
    }
    if ([zipcode_tv.text length]>0)
    {
        [speaker_dict setValue:zipcode_tv.text forKey:@"ZipCode"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"ZipCode"];
    }
    if ([city_tv.text length]>0)
    {
        [speaker_dict setValue:citylic forKey:@"CityLIC"];
        [speaker_dict setValue:city_tv.text forKey:@"City"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"CityLIC"];
        [speaker_dict setValue:@"" forKey:@"City"];
    }
    if ([country_tv.text length]>0)
    {
        [speaker_dict setValue:countrylic  forKey:@"CountryLIC"];
        [speaker_dict setValue:country_tv.text forKey:@"Country"];
    }
    else
    {
        [speaker_dict setValue:@""  forKey:@"CountryLIC"];
        [speaker_dict setValue:@"" forKey:@"Country"];
    }
    if ([state_tv.text length]>0)
    {
        [speaker_dict setValue:statelic  forKey:@"StateLIC"];
        [speaker_dict setValue:state_tv.text forKey:@"State"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"StateLIC"];
        [speaker_dict setValue:@"" forKey:@"State"];
    }
    if ([salutation_textfield.text length]>0)
    {
        [speaker_dict setValue:salutationlic forKey:@"SalutationLIC"];
        [speaker_dict setValue:salutation_textfield.text forKey:@"Salutation"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"SalutationLIC"];
        [speaker_dict setValue:@"" forKey:@"Salutation"];
    }
    if([_currency_tv.text length]>0)
    {
        [speaker_dict setValue:currencylic forKey:@"ExpenseCurrencyLIC"];
    }
    else
    {
         [speaker_dict setValue:@"" forKey:@"ExpenseCurrencyLIC"];
    }
    if([costperhour_tv.text length]>0)
    {
        [speaker_dict setValue:costperhour_tv.text forKey:@"ChargePerHour"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"ChargePerHour"];
    }
    if([_conversionDetail_tv.text length]>0)
    {
        [speaker_dict setValue:_conversionDetail_tv.text forKey:@"ConversionRate"];
        ConversionRate =[_conversionDetail_tv.text floatValue];
    }
    else
    {
        [speaker_dict setValue:@"1" forKey:@"ConversionRate"];
        ConversionRate =1;
    }
    if([_conversionDetail_tv.text length]>0 && [costperhour_tv.text length]>0)
    {
        float totalamt=[costperhour_tv .text floatValue]*ConversionRate;
        [speaker_dict setValue:[NSString stringWithFormat:@"%.2f",totalamt] forKey:@"TotalAmount"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"TotalAmount"];
    }
    if ([status_tv.text length]>0)
    {
        [speaker_dict setValue:statuslic forKey:@"StatusLIC"];
        [speaker_dict setValue:status_tv.text forKey:@"Status"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"StatusLIC"];
        [speaker_dict setValue:@"" forKey:@"Status"];
    }
    if ([speciality_tv.text length]>0)
    {
         [speaker_dict setValue:specialitylic forKey:@"Speciality"];
         [speaker_dict setValue:speciality_tv.text forKey:@"SpecialityLIC"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"Speciality"];
        [speaker_dict setValue:@"" forKey:@"SpecialityLIC"];
    }
    if ([company_tv.text length]>0)
    {
       [speaker_dict setValue:company_tv.text forKey:@"Company"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"Company"];
    }
    if ([designation_tv.text length]>0)
    {
         [speaker_dict setValue:designation_tv.text forKey:@"Designation"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"Designation"];
    }
    if ([executivesumery_textview.text length]>0)
    {
         [speaker_dict setValue:executivesumery_textview.text forKey:@"Summary"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"Summary"];
    }
    if ([passportnumber_tv.text length]>0)
    {
        [speaker_dict setValue:passportnumber_tv.text forKey:@"PassportNumber"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"PassportNumber"];
    }
    if ([valid_from_btn_outlet.titleLabel.text length]>0)
    {
        [speaker_dict setValue:[helper converingformateddatetooriginaldate:valid_from_btn_outlet.titleLabel.text datetype:@"PassportFormTillDate"] forKey:@"PassportValidFrom"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"PassportValidFrom"];
    }
    if ([valid_till_btn_outlet.titleLabel.text length]>0)
    {
         [speaker_dict setValue:[helper converingformateddatetooriginaldate:valid_till_btn_outlet.titleLabel.text datetype:@"PassportFormTillDate"]  forKey:@"PassportValidTo"];
    }
    else
    {
        [speaker_dict setValue:@"" forKey:@"PassportValidTo"];
    }
    
    return  speaker_dict;
}
-(BOOL)validateemail:(NSString *)emailstr
{
    if ([emailstr length]>0)
    {
        if ([emailstr rangeOfString:@"@"].location == NSNotFound)
        {
            //[helper showalert:[helper gettranslation:@"LBL_590"] message:@"Please enter correct email id" action:[helper gettranslation:@"LBL_462"]];
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
        return YES;
}
- (IBAction)save_btn:(id)sender
{
    @try {
        
    NSData *jsonData;
    NSError *error;
    NSMutableArray *speakerarr = [[NSMutableArray alloc]init];
    [salutation_textfield resignFirstResponder];
    [costperhour_tv resignFirstResponder];
    [status_tv resignFirstResponder];
    [firstname_tv resignFirstResponder];
    [lastname_tv resignFirstResponder];
    [speciality_tv resignFirstResponder];
    [company_tv resignFirstResponder];
    [designation_tv resignFirstResponder];
    [departnment_tv resignFirstResponder];
    [email_tv resignFirstResponder];
    [contactno_tv resignFirstResponder];
    [passportnumber_tv resignFirstResponder];
    [addressline1_tv resignFirstResponder];
    [addressline2_tv resignFirstResponder];
    [city_tv resignFirstResponder];
    [state_tv resignFirstResponder];
    [zipcode_tv resignFirstResponder];
    [country_tv resignFirstResponder];
    [executivesumery_textview resignFirstResponder];
        
        if ([salutation_textfield.text length]>0 && [status_tv.text length]>0 && [firstname_tv.text length] >0 && [lastname_tv.text length ]>0 && [company_tv.text length] >0 && [email_tv.text length]>0 && [contactno_tv.text length]>0 && [departnment_tv.text length]>0 )
        {
            if(imageselcted)
            {
                [helper showWaitCursor:[helper gettranslation:@"LBL_538"]];
                sem_att = dispatch_semaphore_create(0);
                uploadflag = @"imageupload";
                [self upload_attachmentdata:attachedimagename base64:[imageData base64EncodedStringWithOptions:kNilOptions]];
                while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
                [helper removeWaitCursor];
            }
            if(resumeselected)
            {
                [helper showWaitCursor:[helper gettranslation:@"LBL_614"]];
                sem_att = dispatch_semaphore_create(0);
                uploadflag = @"fileupload";
                [self upload_attachmentdata:resumefilename base64:filebase64string];
                while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
                [helper removeWaitCursor];
            }
            NSDictionary *checkupdate_dict = [[NSMutableDictionary alloc]init];
            
            if ([transaction_type isEqualToString:@"Edit Speaker"])
            {
                checkupdate_dict = [self checktheupdatedvalues];
                if ([checkupdate_dict allKeys].count == 0)
                {
                    [self removepopup];
                    return;
                }
            }
            checkupdate_dict = [self createdictionary];
            [speakerarr addObject:checkupdate_dict];
            jsonData = [NSJSONSerialization dataWithJSONObject:checkupdate_dict options:0 error:&error];
            [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
            NSString *jsonString;
            if (jsonData)
            {
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
            NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
            NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
            NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
            NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetSpeaker" pageno:@"" pagecount:@"" lastpage:@""];
            appdelegate.senttransaction = @"Y";
            [helper insert_transaction_local:msgbody entity_id:speakerid type:transaction_type entityname:@"Speaker"  Status:@"Open"];
            if ([transaction_type isEqualToString:@"Edit Speaker"])
            {
                [helper delete_speaker:@"Speaker" value:speakerid];
            }
            [helper insertspeakerdata:speakerarr];
            if (imagestorageurl)
            {
                //cant add this code up because database doesnot contain the speaker id
                [self InsertLocationUrl:@"Image"];
            }
            if (filestorageurl)
            {
                [self InsertLocationUrl:@"Resume"];
            }
            [helper removeWaitCursor];
            [self removepopup];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
    }
    }
    @catch (NSException *exception)
    {
        [helper showalert:exception.name message:exception.description action:[helper gettranslation:@"LBL_462"]];
        NSLog(@"Exception name %@ and description %@",exception.name,exception.description);
        
    }
    @finally
    {
        
    }
}
-(void)InsertLocationUrl:ImageResumeFile
{
    //location url
    @try
    {
        NSString *documentsPath;
        NSString *filePath;
        if ([ImageResumeFile isEqualToString:@"Image"])
        {
            documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Speaker"];
            filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",attachedimagename]]; //Add the file name
            if (imageData)
            {
                [imageData writeToFile:filePath atomically:YES];
                [helper update_imageurl:filePath entityname:@"Speaker" entityid:speakerid];
            }
        }
        else if ([ImageResumeFile isEqualToString:@"Resume"])
        {
            documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Resume"];
            filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",resumefilename]]; //Add the file name
            if (resumedata)
            {
                [resumedata writeToFile:filePath atomically:YES];
                [helper update_resumeurl:filePath entityname:@"Speaker" entityid:speakerid];
            }
        }
    }
    @catch (NSException *exception)
    {
        [helper showalert:exception.name message:exception.description action:[helper gettranslation:@"LBL_462"]];
        NSLog(@"Exception name %@ and description %@",exception.name,exception.description);
        
    }
    @finally
    {
        
    }

    
}


-(void)removepopup
{
   
    [self performSegueWithIdentifier:@"Cancelbutton" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
 
}
- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];

}

- (IBAction)dismiss_btn:(id)sender
{
    [self removepopup];
}

- (IBAction)selectphotobtnaction:(id)sender
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[helper gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertController *takephotoalert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_578"] message:[helper gettranslation:@"LBL_504"] preferredStyle:UIAlertControllerStyleAlert];
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_632"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                                   {
                                       UIImagePickerController *impicker = [[UIImagePickerController alloc]init];
                                       impicker.delegate = self;
                                       impicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                       impicker.allowsEditing = NO;
                                       [self presentViewController:impicker animated:YES completion:NULL];
                                       
                                   }
                                   
                                   
                               }]];
    
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_334"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
                                   {
                                       UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
                                       imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                       imagepicker.allowsEditing = YES;
                                       imagepicker.delegate = self;
                                       [self presentViewController:imagepicker animated:YES completion:NULL];
                                   }
                                   
                               }]];
        
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_523"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       sem_att = dispatch_semaphore_create(0);
                                       uploadflag = @"RemovePhoto";
                                       [self upload_attachmentdata:@"" base64:@""];
                                       faceimage.image = [UIImage imageNamed:@""];
                                        while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
                                       
                                   }]];
        [takephotoalert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:takephotoalert animated:YES completion:nil];
    }
}
#pragma mark camera delegate method
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
   
        UIImage *capturedlicensimg;
        if(picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum)
        {
            capturedlicensimg = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        if (picker.sourceType ==  UIImagePickerControllerSourceTypeCamera)
        {
            capturedlicensimg = [info objectForKey:UIImagePickerControllerEditedImage];
        }
    
    
    
        [picker dismissViewControllerAnimated:YES completion:NULL];
        NSURL* localUrl = (NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL];
        NSLog(@"url is %@",localUrl);
        faceimage.image = capturedlicensimg;
        [self resizeimage];
        NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
        {
            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
            attachedimagename = [imageRep filename];
            imageselcted = 1;
        };
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
        [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
        NSLog(@"Attachment image name is %@",attachedimagename);
    
    
    
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == salutation_textfield)
    {
        dropdownflag = @"salutationdropdown";
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
        Dropdownlbl.text = [helper gettranslation:@"LBL_218"];
        
    }
    else if (textField == speciality_tv)
    {
        dropdownflag = @"specialitydropdown";
     
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        Dropdownlbl.text = [helper gettranslation:@"LBL_238"];
    }
    else if (textField == status_tv)
    {
        dropdownflag = @"statusdropdown";
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        Dropdownlbl.text = [helper gettranslation:@"LBL_242"];
    }
    else if (textField == _currency_tv)
    {
        dropdownflag = @"currencydropdown";
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CC_CODE"]];
        Dropdownlbl.text = [helper gettranslation:@"LBL_073"];
    }
    else if (textField == _conversionDetail_tv)
    {
        _conversionDetail_tv.keyboardType = UIKeyboardTypeNumberPad;
        if(![helper CostValidate:_conversionDetail_tv.text])
        {
            _conversionDetail_tv.text=@"";
        }
        else
        {
            float amt=[costperhour_tv.text floatValue];
            
            if([_conversionDetail_tv.text length]>0)
            {
                ConversionRate=[_conversionDetail_tv.text floatValue];
            }
            else
                ConversionRate=1.0;
            
            totalAmt=amt*ConversionRate;
            
            _totalAmt_tv.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
        }
    }
    else if (textField == city_tv)
    {
        dropdownflag = @"citydropdown";
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
        Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"STATE" value:state_tv.text]]];
        
        if(Eventlovlist.count==0)
        {
          Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
          Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
        }
        Dropdownlbl.text = [helper gettranslation:@"LBL_064"];
    }
    else if (textField == state_tv)
    {
        dropdownflag = @"statedropdown";
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
//        if([country_tv.text length]=0)
        Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"COUNTRY" value:country_tv.text]]];
        
        if(Eventlovlist.count ==0)
        {
             Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
             Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
        }
        
        Dropdownlbl.text = [helper gettranslation:@"LBL_241"];
    }
    else if (textField == country_tv)
    {
        dropdownflag = @"countrydropdown";
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"]];
        Dropdownlbl.text = [helper gettranslation:@"LBL_072"];
    }
    if (textField == salutation_textfield || textField == speciality_tv || textField == status_tv || textField == city_tv || textField == state_tv || textField == country_tv || textField == _currency_tv)
    {
        DropDownView.hidden = NO;
        [salutation_tableview reloadData];
        [self adjustHeightOfTableview:salutation_tableview];
       // salutation_tableview.hidden = NO;

    }
    
    if(textField == firstname_tv || textField == lastname_tv || textField == designation_tv || textField == departnment_tv || textField == email_tv || textField == contactno_tv || textField == passportnumber_tv )
    {
        stringlength = 50;
    }
    
    if(textField == company_tv || textField == addressline1_tv || textField == addressline2_tv )
    {
        stringlength = 100;
    }
   /* if(textField == costperhour_tv )
    {
        stringlength = 8;
        if(textField ==costperhour_tv)
        {
            if (costperhour_tv.text>0)
            {
                costperhour_tv.text = [helper CurrencytoFloat:costperhour_tv.text];
            }
        }
    }*/
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    if(textView == executivesumery_textview)
    {
        stringlength = 1500;
    }
}

#pragma mark textfeild delegate method
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == addressline1_tv || textField == addressline2_tv   || textField == zipcode_tv || textField == passportnumber_tv )
    {
        [helper MoveViewUp:YES Height:250 ViewToBeMoved:self.parentViewController.view];
    }
    if(textField == salutation_textfield)
    {
        //[helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];
        [self salutationbtnaction:nil];
    }
    else if (textField == status_tv)
    {
      // [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];
        [self status_btn:nil];
    }
    else if (textField == speciality_tv)
    {
       // [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.parentViewController.view];
        [self speciality_btn:nil];
    }
    else if (textField == _currency_tv)
    {
        [textField resignFirstResponder];
         [helper MoveViewUp:YES Height:250 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == _conversionDetail_tv)
    {
        [helper MoveViewUp:YES Height:180 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == costperhour_tv)
    {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        [helper MoveViewUp:YES Height:250 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == city_tv )
    {
        [helper MoveViewUp:YES Height:250 ViewToBeMoved:self.parentViewController.view];
        [self city_btn:nil];
    }
    else if (textField == state_tv)
    {
        [helper MoveViewUp:YES Height:250 ViewToBeMoved:self.parentViewController.view];
        [self state_btn:nil];
    }
    else if (textField == country_tv)
    {
        [helper MoveViewUp:YES Height:250 ViewToBeMoved:self.parentViewController.view];
        [self country_btn:nil];
    }
    
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
     if(textField == addressline1_tv || textField == addressline2_tv || textField == zipcode_tv || textField == passportnumber_tv)
     {
        [helper MoveViewUp:NO Height:250 ViewToBeMoved:self.parentViewController.view];
     }
    if(textField == city_tv || textField == state_tv)
    {
         [helper MoveViewUp:NO Height:250 ViewToBeMoved:self.parentViewController.view];
         DropDownView.hidden = YES;
    }
    else if (textField == country_tv || textField == _currency_tv)
    {
        [helper MoveViewUp:NO Height:250 ViewToBeMoved:self.parentViewController.view];
        float amt=[costperhour_tv.text floatValue];
        
        if([_conversionDetail_tv.text length]>0)
        {
            ConversionRate=[_conversionDetail_tv.text floatValue];
        }
        else
            ConversionRate=1.0;
        
        totalAmt=amt*ConversionRate;
        
        _totalAmt_tv.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
        DropDownView.hidden = YES;
        
        
    }
   
    if(textField == speciality_tv)
    {
        // [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.parentViewController.view];
        if (![helper isdropdowntextCorrect:speciality_tv.text lovtype:@"SPECIALITY"])
        {
            speciality_tv.text = @"";
        }
    }
    if (textField == salutation_textfield)
    {
        if (![helper isdropdowntextCorrect:salutation_textfield.text lovtype:@"CONTACT_SAL"])
        {
            salutation_textfield.text = @"";
        }
    }
    if (textField == status_tv)
    {
        if (![helper isdropdowntextCorrect:status_tv.text lovtype:@"REC_STAT"])
        {
            status_tv.text = @"";
        }
    }
    else if (textField == _conversionDetail_tv)
    {
        [helper MoveViewUp:NO Height:180 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == costperhour_tv)
    {
        [helper MoveViewUp:NO Height:250 ViewToBeMoved:self.parentViewController.view];
    }
   /* if (textField == city_tv)
    {
        if (![helper isdropdowntextCorrect:city_tv.text lovtype:@"CITY"])
        {
            city_tv.text = @"";
        }
    }*/
    /*if (textField == state_tv)
    {
        if (![helper isdropdowntextCorrect:state_tv.text lovtype:@"STATE"])
        {
            state_tv.text = @"";
        }
    }*/
    /*if (textField == country_tv)
    {
        if (![helper isdropdowntextCorrect:country_tv.text lovtype:@"COUNTRY"])
        {
            country_tv.text = @"";
        }
    }*/
    
    
    if(textField==contactno_tv)
    {
        if(![helper NumberValidate:contactno_tv.text])
        {
            if(![contactno_tv.text isEqualToString:@""])
            {
                [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_413"] action:[helper gettranslation:@"LBL_462"]];
                contactno_tv.text=@"";
            }
        }
    }
    if(textField==email_tv)
    {
        if(![self NSStringIsValidEmail:email_tv.text])
        {
            if(![email_tv.text isEqualToString:@""])
            {
                [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_414"] action:[helper gettranslation:@"LBL_462"]];
                email_tv.text=@"";
                
            }
        }
    }
    [textField resignFirstResponder];
        return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if(textField == _conversionDetail_tv)
    {
        if(![helper CostValidate:_conversionDetail_tv.text])
        {
            _conversionDetail_tv.text=@"";
        }
        else
        {
            float amt=[costperhour_tv.text floatValue];
            
            if([_conversionDetail_tv.text length]>0)
            {
                ConversionRate=[_conversionDetail_tv.text floatValue];
            }
            else
                ConversionRate=1.0;
            
            totalAmt=amt*ConversionRate;
            
            _totalAmt_tv.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
        }
    }
    
    if(textField ==costperhour_tv)
    {
        if(![helper CostValidate:costperhour_tv.text]||costperhour_tv.text.length>8)
        {
            //[helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_765"]action:[helper gettranslation:@"LBL_462"]];
            _totalAmt_tv.text=@"";
            costperhour_tv.text=@"";
        }
        else
        {
            
            float amt=[costperhour_tv.text floatValue];
            
            
            
            if([_conversionDetail_tv.text length]>0)
            {
                ConversionRate=[_conversionDetail_tv.text floatValue];
            }
            else
                ConversionRate=1;
            
            
            totalAmt=amt*ConversionRate;
            
            _totalAmt_tv.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
        }
        
    }
    
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == costperhour_tv)
    {
        if(![helper CostValidate:_totalAmt_tv.text]||_totalAmt_tv.text.length>8)
        {
            //[helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_765"]action:[helper gettranslation:@"LBL_462"]];
            _totalAmt_tv.text=@"";
        }
        else
        {
            
            float amt=[costperhour_tv.text floatValue];
            
            
            
            if([_conversionDetail_tv.text length]>0)
            {
                ConversionRate=[_conversionDetail_tv.text floatValue];
            }
            else
                ConversionRate=1;
            
            
            totalAmt=amt*ConversionRate;
            
            _totalAmt_tv.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
        }
    }
    if(textField == _conversionDetail_tv)
    {
        if(![helper CostValidate:_conversionDetail_tv.text])
        {
            _conversionDetail_tv.text=@"";
        }
        else
        {
            float amt=[costperhour_tv.text floatValue];
            
            if([_conversionDetail_tv.text length]>0)
            {
                ConversionRate=[_conversionDetail_tv.text floatValue];
            }
            else
                ConversionRate=1.0;
            
            totalAmt=amt*ConversionRate;
            
            _totalAmt_tv.text=[NSString stringWithFormat:@"%@ %.2f",currencySymbol,totalAmt];
            
        }
    }
    
    return NO;
}
#pragma mark textview delegate method
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [helper MoveViewUp:YES Height:320 ViewToBeMoved:self.parentViewController.view];
    
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [helper MoveViewUp:NO Height:320 ViewToBeMoved:self.parentViewController.view];
    
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



-(void)textFieldDidChange :(UITextField *)textField
{
    NSString *searchText;
    if(textField == salutation_textfield)
    {
        searchText = salutation_textfield.text;
    }
    else if (textField == status_tv)
    {
        searchText = status_tv.text;
    }
    else if (textField == speciality_tv)
    {
        searchText = speciality_tv.text;
    }
    else if (textField == city_tv)
    {
        searchText = city_tv.text;
    }
    else if (textField == state_tv)
    {
        searchText = state_tv.text;
    }
    else if(textField == country_tv)
    {
        searchText = country_tv.text;
    }
    if (count > searchText.length)
    {
        Eventlovlist = nil;
        Eventlovlist = [helper query_alldata:@"Event_LOV"];
        if(textField == salutation_textfield)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
        }
        else if (textField == status_tv)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (textField == speciality_tv)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (textField == city_tv)
        {
            Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"STATE" value:state_tv.text]]];
            
            if(Eventlovlist.count==0)
            {
                Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
                Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
            }
        }
        else if (textField == state_tv)
        {
            Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
            //        if([country_tv.text length]=0)
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"COUNTRY" value:country_tv.text]]];
            
            if(Eventlovlist.count ==0)
            {
                Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
                Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
            }

        }
        else if (textField == country_tv)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"]];
        }

    }
    if(searchText.length == 0)
    {
        Eventlovlist = [helper query_alldata:@"Event_LOV"];
        if(textField == salutation_textfield)
        {
            salutation_textfield.text = @"";
             Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
        }
        else if (textField == status_tv)
        {
            status_tv.text = @"";
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (textField == speciality_tv)
        {
            speciality_tv.text = @"";
             Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (textField == city_tv)
        {
            Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"STATE" value:state_tv.text]]];
            
            if(Eventlovlist.count==0)
            {
                Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
                Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
            }
        }
        else if (textField == state_tv)
        {
            Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
            //        if([country_tv.text length]=0)
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"COUNTRY" value:country_tv.text]]];
            
            if(Eventlovlist.count ==0)
            {
                Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
                Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
            }
            
        }
        else if(textField == country_tv)
        {
            country_tv.text = @"";
             Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"]];
        }
    }
    else
    {
        Eventlovlist = [helper query_alldata:@"Event_LOV"];
        if(textField == salutation_textfield)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CONTACT_SAL"]];
        }
        else if (textField == status_tv)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if (textField == speciality_tv)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"SPECIALITY"]];
        }
        else if (textField == city_tv)
        {
            Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"STATE" value:state_tv.text]]];
            
            if(Eventlovlist.count==0)
            {
                Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
                Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
            }
        }
        else if (textField == state_tv)
        {
            Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
            //        if([country_tv.text length]=0)
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic like %@",[helper getLic:@"COUNTRY" value:country_tv.text]]];
            
            if(Eventlovlist.count ==0)
            {
                Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
                Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parlic = nil"]];
            }
            
        }
        else if (textField == country_tv)
        {
            Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"]];
        }

        count = searchText.length;
        NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.value contains[c] %@)",searchText];
        filteredArray = [NSMutableArray arrayWithArray:[Eventlovlist filteredArrayUsingPredicate:predicate]];
        Eventlovlist = [filteredArray copy];
    }
    [salutation_tableview reloadData];
    [self adjustHeightOfTableview:salutation_tableview];
    
}


#pragma mark picker view method
/*-(void) validfromdatedate
{
    
    [validfrom_tv resignFirstResponder];
    NSDate *todayDate = [NSDate date]; // get today date
    selecteddate = [formatter stringFromDate:todayDate ];
    getdate = @"fromdate";
    [self pickdate];


}*/
-(void) pickdate
{
    if ([self.view viewWithTag:9])
    {
        return;
    }
    
    
    CGRect toolbarTargetFrame = CGRectMake(270, 150, 280, 40);
    CGRect datePickerTargetFrame = CGRectMake(270, 190, 280, 200);
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, pickerview.frame.size.width, pickerview.frame.size.height)];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor grayColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDatePicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [pickerview addSubview:darkView];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(270, 190, 280, 200)];
    datePicker.tag = 10;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    datePicker.layer.cornerRadius = 5;
    datePicker.datePickerMode = UIDatePickerModeDate;

    [pickerview  addSubview:datePicker];
   
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(270, 150, 280, 40)];
    toolBar.tag = 11;
    toolBar.layer.cornerRadius = 5;
    toolBar.tintColor = [UIColor whiteColor];
    toolBar.barTintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Pick" style:UIBarButtonItemStyleDone target:self action:@selector(dismissDatePicker:)];
    
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(clearDatePicker:)];
    
   
    
    [toolBar setItems:[NSArray arrayWithObjects:doneButton, spacer, cancelButton, nil]];
    [pickerview  addSubview:toolBar];
   
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView.alpha = 0.8;
    [UIView commitAnimations];
}

- (void)dismissDatePicker:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(270, 150, 0, 40);
    CGRect datePickerTargetFrame = CGRectMake(270, 190, 0, 200);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
    
    if ([getdate  isEqual: @"fromdate"])
    {
        [valid_from_btn_outlet setTitle:selecteddate forState:UIControlStateNormal];
    }
    else if ([getdate  isEqual: @"todate"])
    {
        [valid_till_btn_outlet setTitle:selecteddate forState:UIControlStateNormal];
    }
    
    
    
}

- (void)changeDate:(UIDatePicker *)sender
{
   selecteddate = [formatter stringFromDate:sender.date ];
}
- (void)clearDatePicker:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(270, 150, 0, 40);
    CGRect datePickerTargetFrame = CGRectMake(270, 190, 0, 200);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
    
    if ([getdate  isEqual: @"fromdate"])
    {
       // [validfrom_tv setText:validfrom_tv.text];
        
        [valid_from_btn_outlet setTitle:valid_from_btn_outlet.titleLabel.text forState:UIControlStateNormal];
    }
    else if ([getdate  isEqual: @"todate"])
    {
       //[validtill_tv setText:validtill_tv.text];
        [valid_till_btn_outlet setTitle:valid_till_btn_outlet.titleLabel.text forState:UIControlStateNormal];
    }
    
}
- (void)removeViews:(id)object
{
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
    [self resignFirstResponder];
}
/*-(void) validtodatedate
{
   
    [validtill_tv resignFirstResponder];
    NSDate *todayDate = [NSDate date]; // get today date
    selecteddate = [formatter stringFromDate:todayDate ];
    getdate = @"todate";
    [self pickdate];


}*/
#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(touch.view != salutation_tableview)
    {
        DropDownView.hidden = YES;
       
    }
    [salutation_textfield resignFirstResponder];
    [status_tv resignFirstResponder];
    [speciality_tv resignFirstResponder];
    [city_tv resignFirstResponder];
    [state_tv resignFirstResponder];
    [country_tv resignFirstResponder];
    [firstname_tv resignFirstResponder];
    [lastname_tv resignFirstResponder];
    [company_tv resignFirstResponder];
    [designation_tv resignFirstResponder];
    [departnment_tv resignFirstResponder];
    [email_tv resignFirstResponder];
    [contactno_tv resignFirstResponder];
    [passportnumber_tv resignFirstResponder];
    [addressline1_tv resignFirstResponder];
    [addressline2_tv resignFirstResponder];
    [zipcode_tv resignFirstResponder];
    [executivesumery_textview resignFirstResponder];
    [costperhour_tv resignFirstResponder];
}
#pragma mark inserting event attachment to database
-(void)insertattachment:(NSDictionary *)attachmentdata
{
    
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
   // [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
     [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
    
    self.selectedDate = date;
    if ([startEndDate isEqualToString:@"fromdate"])
    {
        [valid_from_btn_outlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    }
    else if ([startEndDate isEqualToString:@"todate"])
    {
       [valid_till_btn_outlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChanggjeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
 
    if(textField == firstname_tv || textField == lastname_tv || textField == designation_tv || textField == departnment_tv || textField == email_tv || textField == contactno_tv || textField == passportnumber_tv || textField == company_tv || textField == addressline1_tv || textField == addressline2_tv || textField == costperhour_tv)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView == executivesumery_textview)
    {
        if(range.length + range.location > textView.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;;
    }
}

@end
