//
//  CustomSegueForLocationPopver.m
//  CRBard
//
//  Created by Administrator on 12/12/13.
//  Copyright (c) 2013 Amit Singh. All rights reserved.
//

#import "CustomSegueForLocationPopver.h"

@implementation CustomSegueForLocationPopver

-(void)perform{
    UIViewController *dst = [self destinationViewController];
    UIViewController *src = [self sourceViewController];
    
    [src addChildViewController:dst];
    [src.view addSubview:dst.view];
    [src.view bringSubviewToFront:dst.view];
    
    CGRect frame;
    
    frame.size.height = src.view.frame.size.height;
    frame.size.width = src.view.frame.size.width;
    
    frame.origin.x = src.view.bounds.origin.x;
    frame.origin.y = src.view.bounds.origin.y;
    
    dst.view.frame = frame;
    
    
    
    [UIView animateWithDuration:0.3f animations:^{
        
        dst.view.alpha = 1.0f;
        
        NSLog(@"%@", NSStringFromCGRect(dst.view.frame));
    }];
}


@end
