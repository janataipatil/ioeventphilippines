//
//  Detail.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 13/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Detail : UIView
@property(strong,nonatomic) IBOutlet UILabel *actualcost;
@property(strong,nonatomic) IBOutlet UILabel *category;
@property(strong,nonatomic) IBOutlet UILabel *product;
@property(strong,nonatomic) IBOutlet UILabel *estimatedcost;
@property(strong,nonatomic) IBOutlet UILabel *type;
@property(strong,nonatomic) IBOutlet UILabel *department;
@property(strong,nonatomic) IBOutlet UILabel *remainingbudget;
@property(strong,nonatomic) IBOutlet UILabel *approvedcost;
@property(strong,nonatomic) IBOutlet UILabel *subtype;
@property(strong,nonatomic)IBOutlet UILabel *estimatedcostlbl;
@property(strong,nonatomic)IBOutlet UILabel *approvedcostlbl;
@property(strong,nonatomic)IBOutlet UILabel *actualcostlbl;
@property(strong,nonatomic)IBOutlet UILabel *remainingbudgetlbl;

@property(strong,nonatomic) IBOutlet UILabel *department_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventcategory_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventtype_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventsubtype_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventprod_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventestcost_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventapprcost_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventactcost_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *eventremcost_ulbl;



@end
