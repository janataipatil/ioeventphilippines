//
//  Detail.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 13/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Detail.h"

@implementation Detail

@synthesize actualcost,category,product,estimatedcost,type,remainingbudget,approvedcost,subtype,estimatedcostlbl,approvedcostlbl,actualcostlbl,remainingbudgetlbl;

- (id)initWithFrame:(CGRect)frame
{
    
    
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"Detail" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    category = (UILabel *)[mainView viewWithTag:1];
    type = (UILabel *)[mainView viewWithTag:2];
    subtype  = (UILabel *)[mainView viewWithTag:3];
    product = (UILabel *)[mainView viewWithTag:4];

    estimatedcost = (UILabel *)[mainView viewWithTag:5];
    approvedcost = (UILabel *)[mainView viewWithTag:6];
    actualcost = (UILabel *)[mainView viewWithTag:7];
    remainingbudget = (UILabel *)[mainView viewWithTag:8];
    
    _eventcategory_ulbl = (UILabel *)[mainView viewWithTag:1901];
    _eventtype_ulbl = (UILabel *)[mainView viewWithTag:1902];
    _eventsubtype_ulbl = (UILabel *)[mainView viewWithTag:1903];
    _eventprod_ulbl = (UILabel *)[mainView viewWithTag:1904];
    _eventestcost_ulbl = (UILabel *)[mainView viewWithTag:1905];
    _eventapprcost_ulbl = (UILabel *)[mainView viewWithTag:1906];
    _eventactcost_ulbl = (UILabel *)[mainView viewWithTag:1907];
    _eventremcost_ulbl = (UILabel *)[mainView viewWithTag:1908];

    return self;
}
@end
