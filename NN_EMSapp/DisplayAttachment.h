//
//  DisplayAttachment.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 16/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayAttachment : UIViewController
@property(strong,nonatomic)NSString *webviewurl;
//@property(strong,nonatomic) NSString *fromview;
@property(strong,nonatomic)NSString *documentname;
@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UILabel *document_name;
@property(strong,nonatomic)NSString *cancelsegue;
@property(strong,nonatomic)NSString *type;
- (IBAction)back_btn:(id)sender;



@end
