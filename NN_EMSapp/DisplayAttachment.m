//
//  DisplayAttachment.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 16/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "DisplayAttachment.h"

@interface DisplayAttachment ()

@end

@implementation DisplayAttachment
@synthesize webviewurl,webview,document_name,documentname,cancelsegue;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //add swipegesture recogniser
    
    
  
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webviewurl]]];
    document_name.text = documentname;
    
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark swipe right method
-(IBAction)swiperight:(id)sender
{
    //[self performSegueWithIdentifier:@"DisplayAttachmentsToList" sender:self];
    [self performSegueWithIdentifier:cancelsegue sender:self];
}
- (IBAction)back_btn:(id)sender
{
    if([_type isEqualToString:@"expense"])
    {
        [self performSegueWithIdentifier:@"BackToExpense" sender:self];
    }
    else if ([_type isEqualToString:@"Speaker"] || [_type isEqualToString:@"Attendee"] || [_type isEqualToString:@"Itenary"])
    {
        [self performSegueWithIdentifier:@"BackToAttachmentList" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:cancelsegue sender:self];
    }
    
    
    
}
@end
