//
//  EventActivitiesViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "Activity_detail.h"
#import "updateactivity_ViewController.h"
#import "TitleBar.h"
#import "MenueBar.h"
#import "HelpViewController.h"
@interface EventActivitiesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (nonatomic,strong) HelperClass *helper;
@property(strong,nonatomic) Activity_detail *actdetailview;
@property (nonatomic) Reachability* reachability;


//@property (strong, nonatomic) IBOutlet UIView *Activitylistview;
@property (strong, nonatomic) IBOutlet UITableView *Activitylistview;

- (IBAction)Setting_btn:(id)sender;

- (IBAction)Activities_segmentcontrol:(id)sender;

@property (strong, nonatomic) IBOutlet UISegmentedControl *Activities_segmentcontrol;

@property (strong, nonatomic) IBOutlet UISearchBar *ActivitySearchBar;

-(IBAction)editactivityClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;

@property (strong, nonatomic) IBOutlet UILabel *daysleft;
@property (strong, nonatomic) IBOutlet UILabel *type;
@property (strong, nonatomic) IBOutlet UILabel *eventname;
@property (strong, nonatomic) IBOutlet UILabel *owner;
@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *priority;

- (IBAction)daysLeftBtn:(id)sender;
- (IBAction)typeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *eventNameBtn;
- (IBAction)statusBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ownerBtn;
@property (strong, nonatomic) IBOutlet UIButton *priorityBtn;

- (IBAction)eventNameBtn:(id)sender;
- (IBAction)ownerBtn:(id)sender;
- (IBAction)priorityBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *daysLeftImg;
@property (strong, nonatomic) IBOutlet UIImageView *typeImg;
@property (strong, nonatomic) IBOutlet UIImageView *eventNameImg;
@property (strong, nonatomic) IBOutlet UIImageView *ownerImg;
@property (strong, nonatomic) IBOutlet UIImageView *statusImg;
@property (strong, nonatomic) IBOutlet UIImageView *priorityImg;

@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;
@end
