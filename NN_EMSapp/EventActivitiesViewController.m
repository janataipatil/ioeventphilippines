//
//  EventActivitiesViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "EventActivitiesViewController.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "SOCustomCell.h"

@interface EventActivitiesViewController ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSPredicate *eventpredicate,*mypredicate,*teampredicate,*activityPredicate;
    int RefreshTableView;
    
    NSString *activityStatus;
    BOOL type;
    BOOL status;
    BOOL eventName;
    BOOL priority;
    BOOL owner;
    BOOL dayLeftFlag;
    
}

@end

@implementation EventActivitiesViewController
{
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    
    SLKeyChainStore *keychain;
    MSClient *client;
    NSArray *AssignedActivitylist;
    
    NSString *appID;
    NSString *custID;
    NSString *AESPrivateKey;
    NSInteger eventcount;
    NSInteger activitycount;
    NSInteger ln_expandedRowIndex;
    HelpViewController *HelpView;
    NSMutableArray *SelectedArray;
    
    NSArray *descriptors;
    NSString *sel_activityid;
}

@synthesize TitleBarView,MenueBarView,helper,Activitylistview,ActivitySearchBar,actdetailview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
  
    AESPrivateKey =[helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = YES;
    TitleBarView.UsernameLbl.text = [helper getUsername];
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"duration" ascending:NO];
    descriptors = [NSArray arrayWithObject:valueDescriptor];

    
    type=YES;
    status=YES;
    eventName=YES;
    owner=YES;
    priority=YES;
    dayLeftFlag=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
    [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    MenueBarView = [[MenueBar alloc]initWithFrame:CGRectMake(0, 20, 100, 748)];
    MenueBarView.ActivitiesButton.backgroundColor = [helper LightBlueColour];
    [MenueBarView.CalendarButton addTarget:self action:@selector(CalendarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.HomeButton addTarget:self action:@selector(HomeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.SpeakerButton addTarget:self action:@selector(SpeakerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.AttendeeButton addTarget:self action:@selector(AttendeeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivityPlanButton addTarget:self action:@selector(ActivityPlanButtonAction:) forControlEvents:UIControlEventTouchUpInside];
     MenueBarView.VersionNumber.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
    
    [self.view addSubview:MenueBarView];
    [self.view addSubview:TitleBarView];

    [self langsetuptranslations];
    
    actdetailview = [[Activity_detail alloc] init];
    
   // AssignedActivitylist = [helper query_alldata:@"Activities"];
    
    
    ln_expandedRowIndex = -1;
    
    [ActivitySearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.ActivitySearchBar valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    
    mypredicate = [NSPredicate predicateWithFormat:@"SELF.visibility like %@",@"MY"];
    teampredicate = [NSPredicate predicateWithFormat:@"SELF.visibility like %@",@"ALL"];
    activityPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[mypredicate, teampredicate]];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    
    if ([userarr count]>0)
    {
        _Username_lbl.text = [NSString stringWithFormat:@"%@ %@",[userarr[0] valueForKey:@"firstname"],[userarr[0] valueForKey:@"lastname"]];
    }
    
    [_Activities_segmentcontrol setSelectedSegmentIndex:0];
    [_Activities_segmentcontrol sendActionsForControlEvents:UIControlEventValueChanged];
    
    ActivitySearchBar.delegate = self;
    
    Activitylistview.delaysContentTouches = false;
    
    if (![appdelegate.UserRolesArray containsObject:@"ACT_ADM"])
    {
        MenueBarView.ActivityPlanButton.hidden = YES;
    }

    [self Activities_segmentcontrol:0];
    
   
    
    //Add swipe gesture recogniser
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeRightAction:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
}

-(void)setupTableViewData
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    SelectedArray = [[NSMutableArray alloc] init];
    
    for(int i=0;i<AssignedActivitylist.count;i++)
    {
        
        NSMutableDictionary *daysLeft=[[NSMutableDictionary alloc]init];
        daysLeft = AssignedActivitylist[i];
        [daysLeft setValue:[NSNumber numberWithInteger:[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[AssignedActivitylist[i] valueForKey:@"enddate"]]]
         forKey:@"daysleft"];
         
        
        [SelectedArray addObject:daysLeft];
    }
}



-(void)viewDidAppear:(BOOL)animated
{
    if(RefreshTableView == 1)
    {
        Activitylistview.delegate = self;
        Activitylistview.dataSource = self;
        [Activitylistview reloadData];
    }
}
#pragma mark swipe right method
-(IBAction)SwipeRightAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)langsetuptranslations
{
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [MenueBarView.HomeButton setTitle:[helper gettranslation:@"LBL_143"] forState:UIControlStateNormal];
    [MenueBarView.ActivitiesButton setTitle:[helper gettranslation:@"LBL_003"] forState:UIControlStateNormal];
    [MenueBarView.CalendarButton setTitle:[helper gettranslation:@"LBL_060"] forState:UIControlStateNormal];
    [MenueBarView.SpeakerButton setTitle:[helper gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [MenueBarView.AttendeeButton setTitle:[helper gettranslation:@"LBL_054"] forState:UIControlStateNormal];
    [MenueBarView.ActivityPlanButton setTitle:[helper gettranslation:@"LBL_007"] forState:UIControlStateNormal];
    
    [_Activities_segmentcontrol setTitle:[helper gettranslation:@"LBL_171"] forSegmentAtIndex:0];
    [_Activities_segmentcontrol setTitle:[helper gettranslation:@"LBL_256"] forSegmentAtIndex:1];
    
    _daysleft.text = [helper gettranslation:@"LBL_076"];
    _type.text = [helper gettranslation:@"LBL_266"];
    _eventname.text = [helper gettranslation:@"LBL_131"];
    _owner.text = [helper gettranslation:@"LBL_187"];
    _status.text = [helper gettranslation:@"LBL_242"];
    _priority.text = [helper gettranslation:@"LBL_203"];
    
}


#pragma mark menu buttons action
- (IBAction)CalendarButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventActivitiestoCalendarview" sender:self];
}

- (IBAction)HomeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromActivitiesToHome" sender:self];
}

- (IBAction)SpeakerButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventActivitiestoSpeakerview" sender:self];
}

- (IBAction)AttendeeButtonAction:(id)sender
{
   [self performSegueWithIdentifier:@"ActiviesToAttendee" sender:self];
}
-(IBAction)ActivityPlanButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"ActivitiesToActivityPlan" sender:self];
}



- (IBAction)Setting_btn:(id)sender
{
    
}

- (IBAction)Activities_segmentcontrol:(id)sender
{
   keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    ln_expandedRowIndex = -1;
    
    
    switch (self.Activities_segmentcontrol.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"segment index is %ld",(long)self.Activities_segmentcontrol.selectedSegmentIndex);
            
            AssignedActivitylist = [helper query_alldata:@"Activities"];
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
            
            AssignedActivitylist = [AssignedActivitylist sortedArrayUsingDescriptors:descriptors];
            
            [Activitylistview reloadData];
           
            [self setupTableViewData];
            break;
            
        case 1:
            NSLog(@"segment index is %ld",(long)self.Activities_segmentcontrol.selectedSegmentIndex);
            
            AssignedActivitylist = [helper query_alldata:@"Activities"];
            
            AssignedActivitylist = [AssignedActivitylist sortedArrayUsingDescriptors:descriptors];
            
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
            
            [Activitylistview reloadData];
            [self setupTableViewData];
            
            break;
            
        default:
            break;
    }
    
}

-(NSInteger)numberOfDaysBetween:(NSString *)sDate and:(NSString *)eDate
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *startDate = [f dateFromString:sDate];
    NSDate *endDate = [f dateFromString:eDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
  
    
    return [components day];
    
}
-(void)RefreshData
{
    AssignedActivitylist = [helper query_alldata:@"Activities"];
    if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
    {
        AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
    }
    else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
    {
        AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
    }
    [Activitylistview reloadData];
   
    

}
#pragma mark search bar delegate method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(activitycount > searchText.length)
    {
        AssignedActivitylist = [helper query_alldata:@"Activities"];
        if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
        {
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
        }
        else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
        {
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
        }
    }
    if(searchText.length == 0)
    {
        AssignedActivitylist = [helper query_alldata:@"Activities"];
        if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
        {
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
        }
        else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
        {
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
        }
        [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
    }
    else
    {
        AssignedActivitylist = [helper query_alldata:@"Activities"];
        if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
        {
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
        }
        else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
        {
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
        }
        
        NSMutableArray *EventNamearr = [[NSMutableArray alloc]init];
        
        //getting event name for the event id
        for (int i =0 ;i<[AssignedActivitylist count];i++)
        {
            NSString *eventid = [AssignedActivitylist[i] valueForKey:@"eventid"];
            NSString *eventname = [helper returneventname:eventid];
            
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            
            [newDict setObject:eventname forKey:@"eventname"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"eventid"] forKey:@"eventid"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"type"] forKey:@"type"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"ownedby"] forKey:@"ownedby"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"status"] forKey:@"status"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"priority"] forKey:@"priority"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"startdate"] forKey:@"startdate"];
            [newDict setObject:[AssignedActivitylist[i] valueForKey:@"enddate"] forKey:@"enddate"];
            [newDict setObject:@"" forKey:@"duration"];

            
            [EventNamearr addObject:newDict];
            
        }
        activitycount = searchText.length;
        NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.type contains[c] %@) OR (SELF.eventname contains[c] %@) OR (SELF.ownedby contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.priority contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
        
        filtereventarr = [NSMutableArray arrayWithArray:[EventNamearr filteredArrayUsingPredicate:predicate]];
        AssignedActivitylist = [filtereventarr copy];
        
    }
    [Activitylistview reloadData];
   
    
    
}


//------------------Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 157;
    }
    
    return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [AssignedActivitylist count] + (ln_expandedRowIndex != -1 ? 1 : 0);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = [indexPath row];
    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
    
   // if (!ln_expandedCell)
    //{
        SOCustomCell *customcell;
        UIImage *image;
        NSString *cellIdentifier = @"customcell";
        
    
//        if (indexPath.row >= AssignedActivitylist.count)
//        {
//            [SelectedArray insertObject:AssignedActivitylist[indexPath.row - 1] atIndex:0];
//        }
//        else
//        {
//          [SelectedArray insertObject:AssignedActivitylist[indexPath.row] atIndex:0];
//        }
    
        customcell = [self.Activitylistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
        customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        
        
        UIImageView *duration = (UIImageView *) [contentView viewWithTag:1];
        UILabel *daysleft = (UILabel *) [contentView viewWithTag:2];
        UILabel *act_type = (UILabel *) [contentView viewWithTag:3];
        UILabel *event_name = (UILabel *) [contentView viewWithTag:4];
        UILabel *ownedby = (UILabel *) [contentView viewWithTag:5];
        UILabel *status = (UILabel *) [contentView viewWithTag:6];
        UILabel *priority = (UILabel *) [contentView viewWithTag:7];
        UIButton *edit_btn = (UIButton *) [contentView viewWithTag:8];
        NSString *canedit = [AssignedActivitylist[indexPath.row] valueForKey:@"canEdit"];
        
        if ([canedit isEqualToString:@"N"])
        {
            [edit_btn setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
        
        }
        else
        {
            [edit_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        
        }
        [edit_btn addTarget:self action:@selector(editactivityClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [daysleft setText:[self geteventduration:[AssignedActivitylist[indexPath.row] valueForKey:@"startdate"] eDate:[AssignedActivitylist[indexPath.row] valueForKey:@"enddate"]]];
    
        [act_type setText:[NSString stringWithFormat:@"%@",[AssignedActivitylist[indexPath.row] valueForKey:@"type"]]];
        [event_name setText:[NSString stringWithFormat:@"%@",[self geteventname:[AssignedActivitylist[indexPath.row] valueForKey:@"eventid"]]]];
        [ownedby setText:[NSString stringWithFormat:@"%@",[AssignedActivitylist[indexPath.row] valueForKey:@"ownedby"]]];
        [status setText:[NSString stringWithFormat:@"%@",[AssignedActivitylist[indexPath.row] valueForKey:@"status"]]];
        [priority setText:[NSString stringWithFormat:@"%@",[AssignedActivitylist[indexPath.row] valueForKey:@"priority"]]];
        
        activityStatus=[AssignedActivitylist[indexPath.row] valueForKey:@"statuslic"];
    
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        
        NSDate *sDate = [dateFormatter dateFromString:[AssignedActivitylist[indexPath.row] valueForKey:@"startdate"]];
        NSDate *eDate = [dateFormatter dateFromString:[AssignedActivitylist[indexPath.row] valueForKey:@"enddate"]];
        
        
        
        if ([sDate timeIntervalSinceNow] > 0.0)
        {
           // image = [UIImage imageNamed:@"list_top.png"];
            image = [UIImage imageNamed:@"ActivityDaysToGo.png"];
            
        }
        else if ([eDate timeIntervalSinceNow] > 0.0)
        {
           // image = [UIImage imageNamed:@"list_equal.png"];
            image = [UIImage imageNamed:@"ActivityDaysRemaining"];
        }
        else
        {
          //  image = [UIImage imageNamed:@"list_bottom.png"];
            if([activityStatus isEqualToString:@"Completed"])
            {
                image = [UIImage imageNamed:@"ActivityStatusComplete.png"];
            }
            else
            image = [UIImage imageNamed:@"ActivityDaysOverdue.png"];
        }
        
        [duration setImage:image];
        
        return customcell;
   // }
    //else
    /*{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        [cell.contentView addSubview:actdetailview];
        
        [self setupActdetails:[AssignedActivitylist[indexPath.row-1] valueForKey:@"activityid"] backcolor:cell.backgroundColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
        return cell;
    }*/
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [helper serverdelete:@"ACTIVITY" entityid:[AssignedActivitylist[indexPath.row] valueForKey:@"activityid"]];
    NSPredicate *ActivityPredicate = [NSPredicate predicateWithFormat:@"activityid like %@",[AssignedActivitylist[indexPath.row] valueForKey:@"activityid"]];
    [helper delete_predicate:@"Activities" predicate:ActivityPredicate];
    [self RefreshData];
    
}
//---------------------Selection Functions start here

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    appdelegate.activityid = [AssignedActivitylist[indexPath.row] valueForKey:@"activityid"];
    appdelegate.canEdit= [AssignedActivitylist[indexPath.row] valueForKey:@"canEdit"];
    NSLog(@"canEdit:%@",appdelegate.canEdit);
    
    [self performSegueWithIdentifier:@"ActivitiesToUpsertActivity" sender:self];
    
    
}
/*- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSInteger row = [indexPath row];
    BOOL preventReopen = NO;
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        //        lineno = [POlinedetaildata[rowToAdd-1] valueForKey:@"line_no"];
        //        releaseno = [POlinedetaildata[rowToAdd-1] valueForKey:@"release_no"];
        //
        //        NSPredicate *lPredicate = [NSPredicate predicateWithFormat:@"SELF.line_no like %@",lineno];
        //        NSPredicate *rPredicate = [NSPredicate predicateWithFormat:@"SELF.release_no like %@",releaseno];
        //        pocodescount = [[[[helper query_predicate_po:@"PurchaseOrder_lineCodes" name:@"purchaseorder_no" value:getporeqid] filteredArrayUsingPredicate:lPredicate] filteredArrayUsingPredicate:rPredicate] count];
        
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
        
    }
    [tableView endUpdates];
    
    return nil;
}*/

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    //    cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
}


-(NSString *) geteventname:(NSString *)event_id
{
    NSString *eventname;
    context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",event_id];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count <=0)
    {
        eventname = @"";
    }
    else
    {
        eventname = [[matchingData valueForKey:@"name"] componentsJoinedByString:@""];
    }
    
    return eventname;
}

-(NSString *) geteventduration:(NSString *)sDate eDate:(NSString *)eDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    
    NSDate *startDate = [dateFormatter dateFromString:sDate];
    NSDate *endDate = [dateFormatter dateFromString:eDate];
    
    
    if (startDate && endDate)
    {
        if ([startDate timeIntervalSinceNow] > 0.0)
        {
           /* NSInteger numberofdays = [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:sDate];
            if (numberofdays == 0)
            {
                return @"Unscheduled";
            }
            else
                return [NSString stringWithFormat:@"%ld Days to go",(long) numberofdays];*/
            return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:sDate],[helper gettranslation:@"LBL_789"]];
        
        //  Start date yet yo come
        }
        else if ([endDate timeIntervalSinceNow] > 0.0)
        {
           /* NSInteger numberofdays = [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:eDate];
            if (numberofdays == 0)
            {
                return @"Unscheduled";
            }
            else
            return [NSString stringWithFormat:@"%ld Days remaining",(long)numberofdays];*/
         return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:eDate],[helper gettranslation:@"LBL_791"]];
        
        //Start date arrived but not ended yet
    }
    else
    {
        // Activity has passed the End Date
        
       /* NSInteger numberofdays = [self numberOfDaysBetween:eDate and:  [dateFormatter stringFromDate:[NSDate date]]];
        if (numberofdays == 0)
        {
            return @"Unscheduled";
        }
        else
            return [NSString stringWithFormat:@"%ld Days Overdue",(long)numberofdays];*/
        if([activityStatus isEqualToString:@"Completed"])
        {
            return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:eDate and:[dateFormatter stringFromDate:[NSDate date]]],[helper gettranslation:@"LBL_809"]];
   
        }
        else
        {
            return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:eDate and:  [dateFormatter stringFromDate:[NSDate date]]],[helper gettranslation:@"LBL_790"]];
        }
    }
    }
    else
        return @"Unscheduled";
}


-(NSInteger) daysLeft:(NSString *)sDate eDate:(NSString *)eDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    
    NSDate *startDate = [dateFormatter dateFromString:sDate];
    NSDate *endDate = [dateFormatter dateFromString:eDate];
    
    
    if (startDate && endDate)
    {
            return [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:sDate];
    }
    else
        return 0;
        
    
}


-(void) setupActdetails:(NSString *)act_id backcolor:(UIColor *)backcolor
{
    NSArray *actdata = [helper query_alldata:@"Activities"];
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"activityid like %@",act_id];
    
    actdata = [actdata filteredArrayUsingPredicate:aPredicate];
    
    
    [self.actdetailview.activity_desc setText:[actdata[0] valueForKey:@"desc"]];
    [self.actdetailview.activity_comment setText:[actdata[0] valueForKey:@"Comment"]];
    self.actdetailview.activity_comment.userInteractionEnabled = YES;
    //NSLog(@")
    [self.actdetailview.activity_commentdate setText:[actdata[0] valueForKey:@"activity_commentdate"]];
    [self.actdetailview.activity_commentby setText:[actdata[0] valueForKey:@"activity_commentby"]];
    [self.actdetailview.mainview setBackgroundColor:backcolor];
    
    
    
    self.actdetailview.actdesc_ulbl.text = [helper gettranslation:@"LBL_595"];
    self.actdetailview.actcomment_ulbl.text = [helper gettranslation:@"LBL_601"];
    self.actdetailview.actstime_ulbl.text = [helper gettranslation:@"LBL_240"];
    self.actdetailview.actetime_ulbl.text = [helper gettranslation:@"LBL_123"];
    self.actdetailview.actpri_ulbl.text = [helper gettranslation:@"LBL_203"];
    
    
    
    
    
    
    
    self.actdetailview.activity_startdate.text = [helper formatingdate:[actdata[0] valueForKey:@"startdate"] datetime:@"FormatDate"];
    self.actdetailview.activity_enddate.text = [helper formatingdate:[actdata[0] valueForKey:@"enddate"] datetime:@"FormatDate"];
}

-(IBAction)editactivityClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:Activitylistview];
    NSIndexPath *indexPath = [Activitylistview indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        sel_activityid = [AssignedActivitylist[indexPath.row] valueForKey:@"activityid"];
        appdelegate.activityid = [AssignedActivitylist[indexPath.row] valueForKey:@"activityid"];
        appdelegate.canEdit= [AssignedActivitylist[indexPath.row] valueForKey:@"canEdit"];
        
    }
    
    [self performSegueWithIdentifier:@"ActivitiesToUpsertActivity" sender:self];
    
   
    
}


//-----------------------------------------------------------------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
        if ([[segue identifier] isEqualToString:@"ActivitylisttoUpdateActivity"])
        {
            updateactivity_ViewController *controller =   segue.destinationViewController;
    
            controller.senderView = @"ActivityDetailView";
            controller.activity_id = sel_activityid;
            controller.cancelSegue = @"unwindtoActivityDetailView";
        }
        if ([[segue identifier] isEqualToString:@"ActivitiesToUpsertActivity"])
        {
            UpsertActivityView *controller = segue.destinationViewController;
            controller.TypeOfActionStr = @"Edit";
            controller.CancelSegue = @"unwindFromupsertActivityToActivity";
        }
    
}
- (IBAction)unwindToActivityDetailView:(UIStoryboardSegue *)segue;
{
    RefreshTableView = 1;
    AssignedActivitylist = [helper query_alldata:@"Activities"];
    
    if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
    {
        AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
    }
    else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
    {
        AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
    }
    Activitylistview.delegate = self;
    Activitylistview.dataSource = self;
    [Activitylistview reloadData];
    
    
    
}

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}
-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}
#pragma mark sort methods
- (IBAction)daysLeftBtn:(id)sender {
 
    [self setupTableViewData];
    dayLeftFlag=!dayLeftFlag;
    if(dayLeftFlag)
    {
     
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"daysleft" type:@"int" Ascending:NO];
        [self.Activitylistview reloadData];
        [_daysLeftImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];
        
       
    }
    else
    {
        
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"daysleft" type:@"int" Ascending:YES];
        [self.Activitylistview reloadData];
         [_daysLeftImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }

}

- (IBAction)typeBtn:(id)sender {
    [self setupTableViewData];
    type=!type;
    if(type)
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"type" type:@"abc" Ascending:NO];
        [self.Activitylistview reloadData];
        [_typeImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"type" type:@"abc" Ascending:YES];
        [self.Activitylistview reloadData];
          [_typeImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }

}
- (IBAction)statusBtn:(id)sender {
    [self setupTableViewData];
    status=!status;
    if(status)
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"status" type:@"abc" Ascending:NO];
        [self.Activitylistview reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"status" type:@"abc" Ascending:YES];
        [self.Activitylistview reloadData];
        [_statusImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }

}
- (IBAction)eventNameBtn:(id)sender {
    [self setupTableViewData];
    eventName=!eventName;
    if(eventName)
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"eventname" type:@"abc" Ascending:NO];
        [self.Activitylistview reloadData];
         [_eventNameImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"eventname" type:@"abc" Ascending:YES];
        [self.Activitylistview reloadData];
        [_eventNameImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}

- (IBAction)ownerBtn:(id)sender {
    [self setupTableViewData];
    owner=!owner;
    if(owner)
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"ownedby" type:@"abc" Ascending:NO];
        [self.Activitylistview reloadData];
        [_ownerImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"ownedby" type:@"abc" Ascending:YES];
        [self.Activitylistview reloadData];
        [_ownerImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_priorityImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }

}

- (IBAction)priorityBtn:(id)sender {
    [self setupTableViewData];
    priority=!priority;
    if(priority)
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"priority" type:@"abc" Ascending:NO];
        [self.Activitylistview reloadData];
        [_priorityImg setImage:[UIImage imageNamed:@"descending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        AssignedActivitylist=[helper sortData:SelectedArray colname:@"priority" type:@"abc" Ascending:YES];
        [self.Activitylistview reloadData];
        [_priorityImg setImage:[UIImage imageNamed:@"ascending.png"]];
        
        [_daysLeftImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_typeImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_statusImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ownerImg setImage:[UIImage imageNamed:@"nosort.png"]];
        [_eventNameImg setImage:[UIImage imageNamed:@"nosort.png"]];
    }

}
@end
