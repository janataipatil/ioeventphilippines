//
//  EventActivityPlanViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 27/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"

@interface EventActivityPlanViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *ActivityPlanTableview;
@property(strong,nonatomic) HelperClass *helper;
@property (strong, nonatomic) IBOutlet UISearchBar *ActivityPlanSearchbar;

- (IBAction)SaveButtonAction:(id)sender;
- (IBAction)CancelButtonAction:(id)sender;

@end
