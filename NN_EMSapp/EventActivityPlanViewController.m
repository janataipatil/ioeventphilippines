//
//  EventActivityPlanViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 27/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventActivityPlanViewController.h"

@interface EventActivityPlanViewController ()
{
    NSMutableArray *ActivityPlanArr;
    NSMutableArray *SelectedActivityPlnasArr;
    NSMutableArray *CopyofAtctivityplansArr;
    AppDelegate *appdelegate;
}

@end

@implementation EventActivityPlanViewController
@synthesize helper,ActivityPlanSearchbar,ActivityPlanTableview;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    ActivityPlanArr = [NSMutableArray arrayWithArray:[helper query_alldata:@"ActivityPlans"]];
    SelectedActivityPlnasArr = [[NSMutableArray alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //for checkbox
    [ActivityPlanTableview setEditing:YES animated:YES];
appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [ActivityPlanSearchbar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [ActivityPlanSearchbar valueForKey:@"_searchField"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
    NSArray *EventTypePlanArr = [helper query_alldata:@"EventTypePlans"];
    for (int i = 0; i < [EventTypePlanArr count]; i++)
    {
        NSString *planid = [EventTypePlanArr[i] valueForKey:@"planID"];
        for (int j= 0; j<[ActivityPlanArr count]; j++)
        {
            NSString *MasterPlanid = [ActivityPlanArr[j] valueForKey:@"planID"];
            if ([MasterPlanid isEqualToString:planid])
            {
                [ActivityPlanArr removeObjectAtIndex:j];
                j--;
            }
            
        }
    }
    //keep original copy for search
    CopyofAtctivityplansArr = [ActivityPlanArr mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ActivityPlanArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *PlanNamelbl = (UILabel *)[contentView viewWithTag:1];
    PlanNamelbl.text = [ActivityPlanArr[indexPath.row] valueForKey:@"name"];
    UILabel *PlanDescriptionlbl = (UILabel *)[contentView viewWithTag:2];
    PlanDescriptionlbl.text = [ActivityPlanArr[indexPath.row] valueForKey:@"activityPlandescription"];
    UILabel *CreatedBylbl = (UILabel *)[contentView viewWithTag:3];
    CreatedBylbl.text = [ActivityPlanArr[indexPath.row] valueForKey:@"createdBy"];
   // customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    
    customcell.tintColor = [helper DarkBlueColour];
    return customcell;
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SelectedActivityPlnasArr addObject:[ActivityPlanArr[indexPath.row] valueForKey:@"planID"]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[SelectedActivityPlnasArr count]; i++)
    {
        NSString *item = [SelectedActivityPlnasArr objectAtIndex:i];
        if ([item isEqualToString:[ActivityPlanArr[indexPath.row] valueForKey:@"planID"]])
        {
            [SelectedActivityPlnasArr removeObject:item];
            i--;
        }
    }
}


- (IBAction)SaveButtonAction:(id)sender
{
    appdelegate.cancelclicked =  @"NO";
    if (SelectedActivityPlnasArr.count<=0)
    {
        [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_617"] withDuaration:1.0];
    }
    else
    {
        [helper showWaitCursor:[helper getLogintranslation:@"LBL_537"]];
        NSMutableArray *PayloadArr = [[NSMutableArray alloc]init];
        
        for (int i=0;i<[SelectedActivityPlnasArr count]; i++)
        {
            NSArray*ActivityPlanDetailArr = [ActivityPlanArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",[SelectedActivityPlnasArr objectAtIndex:i]]];
            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
            
            if ([ActivityPlanDetailArr count]>0)
            {
                [Dict setValue:[ActivityPlanDetailArr[0] valueForKey:@"affiliateID"] forKey:@"AffiliateID"];
                [Dict setValue:[helper getLic:@"EVENT_SUBTYPE" value:appdelegate.EventSubType] forKey:@"EventSubTypeLIC"];
                [Dict setValue:[helper getLic:@"EVENT_TYPE" value:appdelegate.EventType] forKey:@"EventTypeLIC"];
                [Dict setValue:[helper generate_rowId] forKey:@"ID"];
                [Dict setValue:[ActivityPlanDetailArr[0] valueForKey:@"planID"] forKey:@"PlanID"];
            }
            [PayloadArr addObject:Dict];
        }
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArr options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AesPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventTypePlans" pageno:@"" pagecount:@"" lastpage:@""];
        
        
        //online only else on unwind null response
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            if (jsonData)
            {
                [helper inserEventTypePlans:PayloadArr];
                [helper removeWaitCursor];
                [self removePopup];
                
            }
          
            
        }

        
//        [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Add Activity Plans to Event Type" entityname:@"EventTypeActivityPlans" Status:@"Open"];
//        appdelegate.senttransaction = @"Y";
//        
//    
//        [helper inserEventTypePlans:PayloadArr];
//        [self CancelButtonAction:self];

    }
}
-(void)removePopup
{
    [self performSegueWithIdentifier:@"AddEventActivityPlanToActivityPlan" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
- (IBAction)CancelButtonAction:(id)sender
{
    appdelegate.cancelclicked =  @"YES";
    [self removePopup];

}
#pragma mark search bar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == ActivityPlanSearchbar)
    {
        ActivityPlanArr = [CopyofAtctivityplansArr mutableCopy];
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[c] %@) OR (SELF.activityPlandescription contains[c] %@) OR (SELF.createdBy contains[c] %@)",searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[ActivityPlanArr filteredArrayUsingPredicate:predicate]];
            ActivityPlanArr = [filtereventarr copy];
        }
        [ActivityPlanTableview reloadData];
       
       
    }
}

@end
