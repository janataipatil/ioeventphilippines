//
//  EventAdvanceSearchViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 28/07/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"
#import "HSDatePickerViewController.h"

@interface EventAdvanceSearchViewController : UIViewController<HSDatePickerViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
- (IBAction)searchBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *advanceSearchLBL;

@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *EventName_lbl;
@property (strong, nonatomic) IBOutlet UILabel *StartDate_lbl;
@property (strong, nonatomic) IBOutlet UILabel *EndDate_lbl;
@property (strong, nonatomic) IBOutlet UILabel *status_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Owner_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Department_lbl;
@property (strong, nonatomic) IBOutlet UILabel *SubType_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Venue_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Product_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Category_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Type_lbl;

@property (strong, nonatomic) IBOutlet UITableView *DropDownTableView;

@property (strong, nonatomic) IBOutlet UITextField *EventName_tf;
@property (strong, nonatomic) IBOutlet UITextField *StartDate_tf;
@property (strong, nonatomic) IBOutlet UITextField *EndDate_tf;
@property (strong, nonatomic) IBOutlet UITextField *Status_tf;
@property (strong, nonatomic) IBOutlet UITextField *Owner_tf;
@property (strong, nonatomic) IBOutlet UITextField *Department_tf;
@property (strong, nonatomic) IBOutlet UITextField *SubType_tf;
@property (strong, nonatomic) IBOutlet UITextField *Product_tf;
@property (strong, nonatomic) IBOutlet UITextField *Category_tf;
@property (strong, nonatomic) IBOutlet UITextField *Type_tf;
@property (strong, nonatomic) IBOutlet UITextField *Venue_tf;

@end
