//
//  EventAdvanceSearchViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 28/07/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventAdvanceSearchViewController.h"

@interface EventAdvanceSearchViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    HSDatePickerViewController *hsdpvc;
    NSString *DateType;
    NSDate *selectedStartDate;
    NSDate *selectedEndDate;
    NSArray *LOVArr;
    NSString *TextfieldFlag;


}
@end

@implementation EventAdvanceSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc] init];
    
    [_DropDownTableView setHidden:YES];
    
    // Do any additional setup after loading the view.
}


-(void)setupLanguageTranslation
{
    _EventName_lbl.text=[helper gettranslation:@"LBL_131"];
    _StartDate_lbl.text=[helper gettranslation:@"LBL_239"];
    _EndDate_lbl.text=[helper gettranslation:@"LBL_124"];
    _status_lbl.text=[helper gettranslation:@"LBL_242"];
    _Owner_lbl.text=[helper gettranslation:@"LBL_187"];
    _Product_lbl.text=[helper gettranslation:@"LBL_204"];
    _Category_lbl.text=[helper gettranslation:@"LBL_062"];
    _Type_lbl.text=[helper gettranslation:@"LBL_133"];
    _Venue_lbl.text=[helper gettranslation:@"LBL_277"];
    _SubType_lbl.text=[helper gettranslation:@"LBL_132"];
    
    
    
    _Department_lbl.text=[helper gettranslation:@"LBL_092"];
    
    _Department_tf.placeholder=[helper gettranslation:@"LBL_757"];
    _Status_tf.placeholder=[helper gettranslation:@"LBL_757"];
    _Owner_tf.placeholder=[helper gettranslation:@"LBL_757"];
    _Product_tf.placeholder=[helper gettranslation:@"LBL_757"];
    _Category_tf.placeholder=[helper gettranslation:@"LBL_757"];
    _Type_tf.placeholder=[helper gettranslation:@"LBL_757"];
    _SubType_tf.placeholder=[helper gettranslation:@"LBL_757"];

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if(textField == _Owner_tf)
    {
        TextfieldFlag = @"OwnerTextField";
        LOVArr = [helper query_alldata:@"User"];

    }
    
    if(textField == _Product_tf)
    {
        TextfieldFlag = @"ProductTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRODUCT"]];

    }

    if(textField == _Category_tf)
    {
        TextfieldFlag = @"CategoryTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_CATEGORY"]];

    }

    if(textField == _Type_tf)
    {
        TextfieldFlag = @"TypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];

    }

    if(textField == _SubType_tf)
    {
        TextfieldFlag = @"SubTypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_SUBTYPE"]];

    }
    if(textField == _Status_tf)
    {
        TextfieldFlag = @"StatusTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_STAT"]];

    }

    
    if (textField == _StartDate_tf)
    {
         [textField performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.0];
        hsdpvc=[[HSDatePickerViewController alloc]init];
        hsdpvc.delegate=self;
        
        DateType=@"StartDate";
        
        [self presentViewController:hsdpvc animated:YES completion:nil];
        
    }
    else if (textField == _EndDate_tf)
    {
        [textField performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.0];
        hsdpvc=[[HSDatePickerViewController alloc]init];
        hsdpvc.delegate=self;
        
        DateType=@"EndDate";
        
        [self presentViewController:hsdpvc animated:YES completion:nil];

    }
    else if (textField == _Department_tf)
    {
        TextfieldFlag = @"DepartmentTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_DEPT"]];
        
    }
    
    
    if ( textField == _Owner_tf || textField == _Status_tf || textField == _Product_tf || textField == _Type_tf || textField == _SubType_tf ||textField == _Category_tf  || textField==_Department_tf)
    {
        [_DropDownTableView reloadData];
        [self adjustHeightOfTableview:_DropDownTableView];
        _DropDownTableView.hidden = NO;
    }

    
}

- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == _DropDownTableView)
    {
        CGFloat height = _DropDownTableView.contentSize.height;
        CGFloat maxHeight = _DropDownTableView.superview.frame.size.height - _DropDownTableView.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = _DropDownTableView.frame;
            
            if ([TextfieldFlag isEqualToString:@"OwnerTextField"])
            {
                // frame.size.height = 100;
                frame.origin.x = 329;
                frame.origin.y = 129;
            }
            if ([TextfieldFlag isEqualToString:@"ProductTextField"])
            {
                frame.origin.x = 744;
                frame.origin.y = 65;
            }
            if ([TextfieldFlag isEqualToString:@"CategoryTextField"])
            {
                frame.origin.x = 119;
                frame.origin.y = 184;
            }
            if ([TextfieldFlag isEqualToString:@"TypeTextField"])
            {
                frame.origin.x = 329;
                frame.origin.y = 184;
            }
            if ([TextfieldFlag isEqualToString:@"SubTypeTextField"])
            {
                frame.origin.x = 539;
                frame.origin.y = 184;
            }
            if ([TextfieldFlag isEqualToString:@"StatusTextField"])
            {
                frame.origin.x = 119;
                frame.origin.y = 129;
            }
            if ([TextfieldFlag isEqualToString:@"DepartmentTextField"])
            {
                frame.origin.x = 537;
                frame.origin.y = 129;
            }
            
            _DropDownTableView.frame= frame;
            //            LOVTableview.frame = frame;
        }];
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    [dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormater.dateFormat = @"dd MMM yyyy";
    
    if ([DateType isEqualToString:@"StartDate"])
    {
        [_StartDate_tf setText:[dateFormater stringFromDate:date]];
        selectedStartDate=date;
    }
    else if ([DateType isEqualToString:@"EndDate"])
    {
        [_EndDate_tf setText:[dateFormater stringFromDate:date]];
        selectedEndDate=date;
    }
    
}


- (IBAction)searchBtn:(id)sender
{
    NSDateFormatter *eventDateFormatter=[[NSDateFormatter alloc]init];
    [eventDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [eventDateFormatter setDateFormat:@"yyyyMMdd"];

    NSString *SearchString;
    if(_EventName_tf.text.length >0 || _StartDate_tf.text.length>0 || _EndDate_tf.text.length>0 || _Department_tf.text.length>0 || _Owner_tf.text.length>0 || _Status_tf.text.length>0  || _SubType_tf.text.length>0 || _Type_tf.text.length>0 || _Category_tf.text.length>0 || _Venue_tf.text.length>0 || _Product_tf.text.length>0 )
    {
        
        if (_EventName_tf.text.length > 0)
        {
            SearchString = [NSString stringWithFormat:@"(SELF.name contains[c] \"%@\")",_EventName_tf.text];
        }
        if (_StartDate_tf.text.length > 0)
        {
            
            
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.startDate >=  \"%@000000\")",SearchString,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.startdate >= \"%@000000\")",[eventDateFormatter stringFromDate:selectedStartDate]];
            }
        }
        if (_EndDate_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.enddate <= \"%@235959\")",SearchString,[eventDateFormatter stringFromDate:selectedEndDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.enddate <= \"%@235959\")",[eventDateFormatter stringFromDate:selectedEndDate]];
            }
        }
        if (_Department_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.department contains[c] \"%@\")",SearchString,_Department_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.department contains[c] \"%@\")",_Department_tf.text];
            }
        }
        if (_Owner_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.ownedby contains[c] \"%@\")",SearchString,_Owner_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.ownedby contains[c] \"%@\")",_Owner_tf.text];
            }
        }
        if (_Status_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.status contains[c] \"%@\")",SearchString,_Status_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.status contains[c] \"%@\")",_Status_tf.text];
            }
        }
        if (_Category_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.category contains[c] \"%@\")",SearchString,_Category_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.category contains[c] \"%@\")",_Category_tf.text];
            }
        }
        if (_Product_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.product contains[c] \"%@\")",SearchString,_Product_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.product contains[c] \"%@\")",_Product_tf.text];
            }
        }
        if (_Type_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.type contains[c] \"%@\")",SearchString,_Type_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.type contains[c] \"%@\")",_Type_tf.text];
            }
        }
        if (_SubType_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.subtype contains[c] \"%@\")",SearchString,_SubType_tf.text];
            }
            else
            {
                    SearchString = [NSString stringWithFormat:@"(SELF.subtype contains[c] \"%@\")",_SubType_tf.text];
            }
        }
        if (_Venue_tf.text.length > 0)
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.venue contains[c] \"%@\")",SearchString,_Venue_tf.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.venue contains[c] \"%@\")",_Venue_tf.text];
            }
        }

        
        appdelegate.eventAdvanceSearch=[NSPredicate predicateWithFormat:SearchString];
        appdelegate.EventListFilter=@"EventAdvanceSearch";
        
    }
    else
    {
        appdelegate.eventAdvanceSearch=nil;
    }
    
    [self performSegueWithIdentifier:@"BackToHome" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [_DropDownTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
    
    if([TextfieldFlag isEqualToString:@"OwnerTextField"])
    {
        
        lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"userid"];

    }
    else
    lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    return customcell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([TextfieldFlag isEqualToString:@"OwnerTextField"])
    {
        _Owner_tf.text = [LOVArr[indexPath.row] valueForKey:@"userid"];
        [_Owner_tf resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"ProductTextField"])
    {
        _Product_tf.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_Product_tf resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"CategoryTextField"])
    {
        _Category_tf.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_Category_tf resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"TypeTextField"])
    {
        _Type_tf.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_Type_tf resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"SubTypeTextField"])
    {
        _SubType_tf.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_SubType_tf resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"StatusTextField"])
    {
        _Status_tf.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_Status_tf resignFirstResponder];
    }
    if ([TextfieldFlag isEqualToString:@"DepartmentTextField"])
    {
        _Department_tf.text= [LOVArr[indexPath.row] valueForKey:@"value"];
           [_Department_tf resignFirstResponder];
    }
//    {
//        _Status_tf.text = [LOVArr[indexPath.row] valueForKey:@"value"];
//        [_Status_tf resignFirstResponder];
//    }

    [_DropDownTableView setHidden:YES];

}


-(IBAction)cancelBtn:(id)sender
{
    [self performSegueWithIdentifier:@"BackToHome" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
@end
