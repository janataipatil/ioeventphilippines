//
//  EventAttendeePopoverViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 01/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"

@interface EventAttendeePopoverViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *AttendeeName_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Attendee_nm;
@property (strong, nonatomic) IBOutlet UILabel *Email_lbl;
@property (strong, nonatomic) IBOutlet UILabel *Attendee_email;
@property (strong, nonatomic) IBOutlet UILabel *AttendeePhn_lbl;
@property (strong, nonatomic) IBOutlet UILabel *AttendeeSpec_lbl;
@property (strong, nonatomic) IBOutlet UILabel *AttendeeSource_lbl;
@property (strong, nonatomic) IBOutlet UILabel *AttendeeClass_lbl;
@property (strong, nonatomic) IBOutlet UILabel *AttendeeStatus_lbl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *AttendeeStatus_segmentControl;

@property (strong, nonatomic) IBOutlet UILabel *Attendee_phone;
@property (strong, nonatomic) IBOutlet UILabel *Attendee_spec;
@property (strong, nonatomic) IBOutlet UILabel *Attendee_Source;
@property (strong, nonatomic) IBOutlet UILabel *Attendee_Class;

@property (strong,nonatomic)NSString *attendeeid;
- (IBAction)AttendeeStatus_segmentControl:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *AttendeeStatus;


@end
