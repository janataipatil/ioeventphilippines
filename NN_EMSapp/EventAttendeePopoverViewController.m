//
//  EventAttendeePopoverViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 01/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventAttendeePopoverViewController.h"

@interface EventAttendeePopoverViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSArray *AttendeeList;
    NSString *statuslic;
}
@end

@implementation EventAttendeePopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper=[[HelperClass alloc]init];
    
    AttendeeList=[helper query_alldata:@"Event_Attendee"];
    AttendeeList=[AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",_attendeeid]];
    
    [_AttendeeStatus setHidden:YES];
    if(AttendeeList.count>0)
    {
    
        if([[AttendeeList[0] valueForKey:@"salutation"] length]>0)
        {
            _Attendee_nm.text=[NSString stringWithFormat:@"%@ %@ %@",[AttendeeList[0] valueForKey:@"salutation"],[AttendeeList[0] valueForKey:@"firstname"],[AttendeeList[0] valueForKey:@"lastname"]];
        }
        else
        {
            _Attendee_nm.text=[NSString stringWithFormat:@"%@ %@",[AttendeeList[0] valueForKey:@"firstname"],[AttendeeList[0] valueForKey:@"lastname"]];

        }
        
        if([[AttendeeList[0] valueForKey:@"speciality"] length]>0)
            _Attendee_spec.text=[AttendeeList[0] valueForKey:@"speciality"];
        else
            _Attendee_spec.text=@"--";
        
        
        if([[AttendeeList[0] valueForKey:@"targetclass"] length]>0 && [[AttendeeList[0] valueForKey:@"subTargetClass"] length]>0)
        {
            _Attendee_Class.text=[NSString stringWithFormat:@"%@ - %@",[AttendeeList[0] valueForKey:@"targetclass"],[AttendeeList[0] valueForKey:@"subTargetClass"]];
        }
        else if ([[AttendeeList[0] valueForKey:@"targetclass"] length]>0)
        {
            _Attendee_Class.text=[AttendeeList[0] valueForKey:@"targetclass"];
        }
        else
        {
            _Attendee_Class.text=[AttendeeList[0] valueForKey:@"subTargetClass"];
        }
        
        
        if([[AttendeeList[0] valueForKey:@"contactnumber"] length]>0)
            _Attendee_phone.text=[AttendeeList[0] valueForKey:@"contactnumber"];
        else
            _Attendee_phone.text=@"--";
        
        if([[AttendeeList[0] valueForKey:@"contactnumber"] length]>0)
            _Attendee_email.text=[AttendeeList[0] valueForKey:@"emailaddress"];
        else
            _Attendee_email.text=@"--";
        
        if([[AttendeeList[0] valueForKey:@"integrationsource"] length]>0)
            _Attendee_Source.text=[AttendeeList[0] valueForKey:@"integrationsource"];
        else
            _Attendee_Source.text=@"--";
     
        
        if([[AttendeeList[0] valueForKey:@"statuslic"] isEqualToString:@"Accepted"])
        {
            _AttendeeStatus_segmentControl.selectedSegmentIndex=0;
        }
        else if ([[AttendeeList[0] valueForKey:@"statuslic"] isEqualToString:@"Declined"])
        {
            _AttendeeStatus_segmentControl.selectedSegmentIndex=1;
        }
        else if([[AttendeeList[0] valueForKey:@"statuslic"] isEqualToString:@"Tentative"])
        {
            _AttendeeStatus_segmentControl.selectedSegmentIndex=2;
        }
        else if([[AttendeeList[0] valueForKey:@"statuslic"] isEqualToString:@"Proposed"] || [[AttendeeList[0] valueForKey:@"statuslic"] isEqualToString:@"Rejected"])
        {
            [_AttendeeStatus_segmentControl setHidden:YES];
            [_AttendeeStatus setHidden:NO];
            _AttendeeStatus.textColor=[helper getstatuscolor:[AttendeeList[0] valueForKey:@"statuslic"] entityname:@"Attendee"];
            _AttendeeStatus.text=[AttendeeList[0] valueForKey:@"status"];
        }
        else
        {
            [_AttendeeStatus_segmentControl setHidden:NO];
        }
            
    }
    
    
    
    // Do any additional setup after loading the view.
}


-(void)setupLanguageTranslation
{
    _AttendeeName_lbl.text=[helper gettranslation:@"LBL_175"];
    _AttendeePhn_lbl.text=[helper gettranslation:@"LBL_070"];
    _AttendeeSpec_lbl.text=[helper gettranslation:@"LBL_238"];
    _AttendeeClass_lbl.text=[helper gettranslation:@"LBL_824"];
    _Email_lbl.text=[helper gettranslation:@"LBL_119"];
    _AttendeeStatus_lbl.text=[helper gettranslation:@"LBL_242"];
    _AttendeeSource_lbl.text=[helper gettranslation:@"LBL_148"];

    
    [_AttendeeStatus_segmentControl setTitle:[helper gettranslation:@"LBL_588"] forSegmentAtIndex:0];
    [_AttendeeStatus_segmentControl setTitle:[helper gettranslation:@"LBL_593"] forSegmentAtIndex:1];
    [_AttendeeStatus_segmentControl setTitle:[helper gettranslation:@"LBL_602"] forSegmentAtIndex:2];

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)SendRequestToServer:(NSString *)status
{
    NSString *rowid;
    
    NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
    
        rowid = [NSString stringWithFormat:@"EVENTATT-%@",[helper generate_rowId]];
        
        NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];
        
        [eventattendee_dict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"attendeeid"]?: @"" forKey:@"AttendeeID"];
        [eventattendee_dict setValue:appdelegate.eventid forKey:@"EventID"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"id"]?: @""  forKey:@"ID"];

        [eventattendee_dict setValue:[helper getvaluefromlic:@"E_ATT_STAT" lic:status] forKey:@"Status"];
        [eventattendee_dict setValue:status forKey:@"StatusLIC"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"firstname"]?: @"" forKey:@"FirstName"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"lastname"] ?: @"" forKey:@"LastName"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"salutation"] ?: @"" forKey:@"Salutation"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"salutationlic"]?: @"" forKey:@"SalutationLIC"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"speciality"]?: @"" forKey:@"Speciality"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"specialitylic"]?: @"" forKey:@"SpecialityLIC"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"targetclass"]?: @"" forKey:@"TargetClass"];
        [eventattendee_dict setValue:[AttendeeList[0] valueForKey:@"targetclasslic"]?: @"" forKey:@"TargetClassLIC"];
        NSString  *ApprovalFlag;
        if ([status isEqualToString:@"Approved"])
        {
            ApprovalFlag = @"false";
        }
        else if ([status isEqualToString:@"Rejected"])
        {
            ApprovalFlag = @"true";
        }
        [eventattendee_dict setValue:ApprovalFlag forKey:@"ApprovalFlag"];
        [EventAttendee_ar addObject:eventattendee_dict];
    
    
    NSString *jsonString;
    NSError *error;

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar options:0 error:&error];
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventAttendee" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        if (jsonData)
        {

                NSManagedObjectContext  *context = [appdelegate managedObjectContext];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_Attendee" inManagedObjectContext:context];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.attendeeid like %@)",appdelegate.eventid,_attendeeid];
                
                
                NSError *error;
                [fetchRequest setEntity:entity];
                [fetchRequest setPredicate:predicate];
                NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                NSString *ApprovalFlag;
                if ([status isEqualToString:@"Approved"])
                {
                    ApprovalFlag = [helper booltostring:0];
                }
                else if ([status isEqualToString:@"Rejected"])
                {
                    ApprovalFlag = [helper booltostring:1];
                }
                
                if([fetchedObjects count]>0)
                {
                    for (NSManagedObject *object in fetchedObjects)
                    {
                        [object setValue:status forKey:@"statuslic"];
                        [object setValue:[helper getvaluefromlic:@"E_ATT_STAT" lic:status] forKey:@"status"];
                        [object setValue:ApprovalFlag forKey:@"approvalFlag"];
                    }
                    [context save:&error];
                }
        }
    }
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)AttendeeStatus_segmentControl:(id)sender
{
    
    switch (self.AttendeeStatus_segmentControl.selectedSegmentIndex)
    {
        case 0:
            [self SendRequestToServer:@"Accepted"];
            break;
        case 1:
            [self SendRequestToServer:@"Declined"];
            break;
        case 2:
            [self SendRequestToServer:@"Tentative"];
            break;
        default:
            break;
    }
}

@end
