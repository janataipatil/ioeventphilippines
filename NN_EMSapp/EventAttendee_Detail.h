//
//  EventAttendee_Detail.h
//  NN_EMSapp
//
//  Created by iWizards XI on 02/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventAttendee_Detail : UIView

@property (strong, nonatomic) IBOutlet UILabel *attendee_emailaddress;
@property (strong, nonatomic) IBOutlet UILabel *attendee_integrationsource;
@property (strong, nonatomic) IBOutlet UILabel *attendee_contactno;
@property(strong,nonatomic)IBOutlet UILabel *SubTargetClass;
@property(strong,nonatomic)IBOutlet UILabel *attendee_Status;


@property (strong, nonatomic) IBOutlet UILabel *emailaddress_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *contactno_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *subtargetclass_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *integrationsrc_ulbl;
@property(strong,nonatomic)IBOutlet UILabel *Attendee_status_ulbl;

@property (strong,nonatomic)IBOutlet UIButton *attendeeDelete;


@property (strong,nonatomic) IBOutlet UIView *mainview;

@end
