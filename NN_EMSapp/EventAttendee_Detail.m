//
//  EventAttendee_Detail.m
//  NN_EMSapp
//
//  Created by iWizards XI on 02/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventAttendee_Detail.h"

@implementation EventAttendee_Detail

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"EventAttendee_Detail" owner:self options:nil];
    
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    
    self.attendee_emailaddress = (UILabel *)[mainView viewWithTag:5];
    self.attendee_integrationsource = (UILabel *)[mainView viewWithTag:8];
    self.attendee_contactno = (UILabel *)[mainView viewWithTag:6];
    self.SubTargetClass=(UILabel *)[mainView viewWithTag:7];
    self.attendee_Status=(UILabel *)[mainView viewWithTag:10];
  
    self.emailaddress_ulbl = (UILabel *)[mainView viewWithTag:1];
    self.contactno_ulbl = (UILabel *)[mainView viewWithTag:2];
     self.subtargetclass_ulbl = (UILabel *)[mainView viewWithTag:3];
    self.integrationsrc_ulbl = (UILabel *)[mainView viewWithTag:4];
    self.Attendee_status_ulbl=(UILabel *)[mainView viewWithTag:11];
    
    self.attendeeDelete=(UIButton *)[mainView viewWithTag:50];
    
       
    
    
    return self;
}



@end
