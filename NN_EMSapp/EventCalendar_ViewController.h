//
//  EventCalendar_ViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/19/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKitUI/EventKitUI.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "ToastView.h"
#import "TitleBar.h"
#import "MenueBar.h"
#import "HelpViewController.h"

@protocol CalendarViewControllerNavigation <NSObject>

@property (nonatomic, readonly) NSDate* centerDate;

- (void)moveToDate:(NSDate*)date animated:(BOOL)animated;
- (void)moveToNextPageAnimated:(BOOL)animated;
- (void)moveToPreviousPageAnimated:(BOOL)animated;

@optional

@property (nonatomic) NSSet* visibleCalendars;

@end


typedef  UIViewController<CalendarViewControllerNavigation> CalendarViewController;


@protocol CalendarViewControllerDelegate <NSObject>

@optional

- (void)calendarViewController:(CalendarViewController*)controller didShowDate:(NSDate*)date;
- (void)calendarViewController:(CalendarViewController*)controller didSelectEvent:(EKEvent*)event;

@end



@interface EventCalendar_ViewController : UIViewController<CalendarViewControllerDelegate, EKCalendarChooserDelegate>


@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic) CalendarViewController* calendarViewController;
@property (nonatomic) Reachability* reachability;

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UILabel *currentDateLabel;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *settingsButtonItem;
@property (nonatomic, weak) IBOutlet UISegmentedControl *viewChooser;


@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *toolbarbutton;

@property (strong, nonatomic) IBOutlet UIButton *showToday;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *showCalendars;
@property (nonatomic, weak) IBOutlet UIButton *Today;


@property (nonatomic) NSCalendar *calendar;
@property (nonatomic) EKEventStore *eventStore;



- (IBAction)menu_speaker_btn:(id)sender;
- (IBAction)menu_home_btn:(id)sender;
- (IBAction)menu_activity_btn:(id)sender;
- (IBAction)menu_synclog_btn:(id)sender;
- (IBAction)Setting_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;
@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;
@end
