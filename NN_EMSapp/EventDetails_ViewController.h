//
//  EventDetails_ViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/7/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "VenueView.h"
#import "ActivityView.h"
#import "AttendeesView.h"
#import "ScheduleView.h"
#import "Detail.h"
#import "ActivityView.h"
#import "SOCustomCell.h"
#import "Speaker.h"
#import "EventSpeakerViewController.h"
#import "Reachability.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Attachment.h"
#import "Team.h"
#import "Approval.h"
#import "SLKeyChainStore.h"
#import "TemplatesViewController.h"
#import "TitleBar.h"
#import "MenueBar.h"
#import "ExpenseViewController.h"
#import "SurveyView.h"
#import "HelpViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "EventProductViewController.h"



@interface EventDetails_ViewController : UIViewController<MKMapViewDelegate>
{
    
    IBOutlet UIView *mainview;
//    MKMapView *myMapView;
    GMSMapView *myMapView;
    
}
@property (nonatomic) NSString *returnedItem;

@property (strong, nonatomic) IBOutlet UIButton *AddAttachmentOutlet;
@property (weak, nonatomic) IBOutlet UILabel *groupsLBL;

@property (strong, nonatomic) IBOutlet UILabel *eventdetailheaderlbl;
@property (strong, nonatomic) IBOutlet UILabel *eventname;
@property (strong, nonatomic) IBOutlet UILabel *eventdescription;
@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UIButton *approvebtnoutlet;
@property (strong, nonatomic) IBOutlet UILabel *startdate;
@property (weak, nonatomic) IBOutlet UIButton *addNewGroupBtn;
@property (strong, nonatomic) IBOutlet UILabel *enddate;
@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet UILabel *owner;
@property (strong, nonatomic) IBOutlet UIView *eventDetailView;
- (IBAction)AddGroupBtn:(id)sender;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property(strong,nonatomic) HelperClass *helper;
@property (nonatomic) Reachability* reachability;
@property (strong, nonatomic) IBOutlet UIView *eventDetailUpperView;

@property (strong, nonatomic) IBOutlet UILabel *actual_cost;
@property (strong, nonatomic) IBOutlet UILabel *category;
@property (strong, nonatomic) IBOutlet UILabel *product;
@property (strong, nonatomic) IBOutlet UILabel *estimated_cost;
@property(nonatomic,strong)GIBadgeView *groupbadge;
@property (strong, nonatomic) IBOutlet UILabel *type;
@property (strong, nonatomic) IBOutlet UILabel *remaining_budget;
@property (strong, nonatomic) IBOutlet UILabel *approved_cost;
@property (strong, nonatomic) IBOutlet UILabel *sub_type;
@property (strong, nonatomic) IBOutlet UIButton *detailbtnoutlet;
@property (strong, nonatomic) IBOutlet UIButton *activitybtnoutlet;
@property (strong, nonatomic) IBOutlet UILabel *department_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *department;
@property (strong, nonatomic) IBOutlet UITableView *Activitylistview;
@property (strong, nonatomic) IBOutlet UIButton *attendybtnoutlet;

@property (strong, nonatomic) IBOutlet UIButton *CommunicationButtonOutlet;
@property(strong,nonatomic)NSString *ApprovalStr;
- (IBAction)attendybtnaction:(id)sender;


- (IBAction)activitybtnaction:(id)sender;

- (IBAction)detailbtnaction:(id)sender;
- (IBAction)venuebtnaction:(id)sender;
- (IBAction)speakerbtn:(id)sender;
- (IBAction)gallerybtnaction:(id)sender;
- (IBAction)editevent_btn:(id)sender;
- (IBAction)CommunicationButtonAction:(id)sender;
- (IBAction)ExpenseButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ExpenseButtonOutlet;


@property (strong, nonatomic) IBOutlet UIButton *editevent_btn;
@property (strong, nonatomic) IBOutlet UILabel *editevent_lbl;


@property (strong, nonatomic) IBOutlet UIButton *speakerbtnoutlet;
- (IBAction)approvalbtn:(id)sender;
- (IBAction)teambtn:(id)sender;
- (IBAction)attachmentbtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *venuebtnoutlet;

- (IBAction)approvebtnaction:(id)sender;


- (IBAction)menu_activity_btn:(id)sender;
- (IBAction)menu_synclog_btn:(id)sender;
- (IBAction)menu_home_btn:(id)sender;
- (IBAction)menu_speaker_btn:(id)sender;
- (IBAction)menu_calendar_btn:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;
- (IBAction)getlocation_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UISegmentedControl *Details_segmentcontrol;

- (IBAction)TeamButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *TeamButtonOutlet;

- (IBAction)ApproveButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ApproveButtonOutlet;
@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;

@property(strong,nonatomic)NSString *TypeofActionStr;


- (IBAction)submitevent_btn:(id)sender;
- (IBAction)cancelevent_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submitevent_btn;
@property (strong, nonatomic) IBOutlet UIButton *cancelevent_btn;
@property (strong, nonatomic) IBOutlet UILabel *submitevent_lbl;
@property (strong, nonatomic) IBOutlet UILabel *cancelevent_lbl;




@property (strong, nonatomic) IBOutlet UILabel *gallery_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventdesc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventstatus_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventloc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventsdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventedate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventowner_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *approvebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *detailbtnaction_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *attachmentbtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *speakerbtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *commbtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *teambtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *expensebtn_ulbl;











@end
