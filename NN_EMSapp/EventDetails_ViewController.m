//
//  EventDetails_ViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/7/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "EventDetails_ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "EventLocation.h"
#import "UpsertEventVC.h"
#import "AdvanceSearchView.h"
#import "ItineraryViewController.h"
#import "GIBadgeView.h"

@interface EventDetails_ViewController ()
{
    NSArray *Eventlist;
    AppDelegate *appdelegate;
    VenueView *venueview;
    SLKeyChainStore *keychain;
    AttendeesView *attendyview;
    ItineraryViewController *itinearyView;
    ScheduleView *scheduleview;
    Detail *detailview;
    ActivityView *actview;
    Speaker *speakerview;
    Attachment *AttachmentView;
    NSArray *AssignedActivitylist;
    Team *TeamView;
    NSString *canedit;
    Approval *ApprovalView;
    TemplatesViewController *TemplateView;
    ExpenseViewController *ExpenseView;
    EventProductViewController *EventProductView;
    SurveyView *surveyview;
    NSString *semp;
    dispatch_semaphore_t sem_event;
    NSString *eventstatus,*eventstatuslic;
    NSMutableArray *galleryimgArray;
    NSArray *galleryimagelist;
    HelpViewController *HelpView;
    BOOL eventStatusFlag;
    CLGeocoder *geocoder;
}
@end

@implementation EventDetails_ViewController
@synthesize helper,eventdetailheaderlbl,eventname,eventdescription,approvebtnoutlet,startdate,enddate,location,actual_cost,category,product,estimated_cost,type,remaining_budget,approved_cost,sub_type,detailbtnoutlet,venuebtnoutlet,activitybtnoutlet,attendybtnoutlet,speakerbtnoutlet,Details_segmentcontrol,AddAttachmentOutlet,TypeofActionStr,TeamButtonOutlet,ApproveButtonOutlet,CommunicationButtonOutlet,TitleBarView,MenueBarView,ExpenseButtonOutlet,ApprovalStr;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //initialising variable
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    
    //Add swipe gesture recogniser
    
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperightaction:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
   
    
    
    AssignedActivitylist = [helper query_alldata:@"Activities"];
   
    [self refreshdata];
    if([Eventlist count]>0)
    {
        if ([ApprovalStr isEqualToString:@"Approvals"])
        {
            [self setDetailView];
            [self ApproveButtonAction:self];
        }
        else if ([ApprovalStr isEqualToString:@"Event"])
        {
            [self setDetailView];
        }
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
     geocoder = [[CLGeocoder alloc] init];
     [_locationManager startUpdatingLocation];
    
    
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    
    if ([userarr count]>0)
    {
        _Username_lbl.text = [NSString stringWithFormat:@"%@ %@",[userarr valueForKey:@"firstname"],[userarr valueForKey:@"lastname"]];
    }
    
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = YES;
    TitleBarView.UsernameLbl.text = [helper getUsername];
     [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
    
    
    MenueBarView = [[MenueBar alloc]initWithFrame:CGRectMake(0, 20, 100, 748)];
    MenueBarView.HomeButton.backgroundColor = [UIColor colorWithRed:46.0/255.0f green:176.0/255.0f blue:238.0/255.0f alpha:1.0f];
    
    [MenueBarView.CalendarButton addTarget:self action:@selector(CalendarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivitiesButton addTarget:self action:@selector(ActivityButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.SpeakerButton addTarget:self action:@selector(SpeakerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.AttendeeButton addTarget:self action:@selector(AttendeeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivityPlanButton addTarget:self action:@selector(ActivityPlanButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.HomeButton addTarget:self action:@selector(HomeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:MenueBarView];
    [self.view addSubview:TitleBarView];
    
    [self langsetuptranslations];
    
    if (![appdelegate.UserRolesArray containsObject:@"ACT_ADM"])
    {
        MenueBarView.ActivityPlanButton.hidden = YES;
    }
   
    NSArray *TEMP =[[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    [self updateBadgeValue:TEMP.count];
}

-(void)langsetuptranslations
{
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    _groupsLBL.text = [helper gettranslation:@"LBL_923"];
    [MenueBarView.HomeButton setTitle:[helper gettranslation:@"LBL_143"] forState:UIControlStateNormal];
    [MenueBarView.ActivitiesButton setTitle:[helper gettranslation:@"LBL_003"] forState:UIControlStateNormal];
    [MenueBarView.CalendarButton setTitle:[helper gettranslation:@"LBL_060"] forState:UIControlStateNormal];
    [MenueBarView.SpeakerButton setTitle:[helper gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [MenueBarView.AttendeeButton setTitle:[helper gettranslation:@"LBL_054"] forState:UIControlStateNormal];
    [MenueBarView.ActivityPlanButton setTitle:[helper gettranslation:@"LBL_007"] forState:UIControlStateNormal];
     MenueBarView.VersionNumber.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
    
    [Details_segmentcontrol setTitle:[helper gettranslation:@"LBL_390"] forSegmentAtIndex:0];
    [Details_segmentcontrol setTitle:[helper gettranslation:@"LBL_054"] forSegmentAtIndex:1];
    [Details_segmentcontrol setTitle:[helper gettranslation:@"LBL_223"] forSegmentAtIndex:2];
    [Details_segmentcontrol setTitle:[helper gettranslation:@"LBL_003"] forSegmentAtIndex:3];
    [Details_segmentcontrol setTitle:[helper gettranslation:@"LBL_251"] forSegmentAtIndex:4];
    [Details_segmentcontrol setTitle:[helper gettranslation:@"LBL_872"] forSegmentAtIndex:5];

    
    
    _editevent_lbl.text = [helper gettranslation:@"LBL_380"];

    _eventname_ulbl.text = [helper gettranslation:@"LBL_131"];
    _eventdesc_ulbl.text = [helper gettranslation:@"LBL_094"];
    _eventstatus_ulbl.text = [helper gettranslation:@"LBL_242"];
    _eventloc_ulbl.text = [helper gettranslation:@"LBL_161"];
    _eventsdate_ulbl.text = [helper gettranslation:@"LBL_239"];
    _eventedate_ulbl.text = [helper gettranslation:@"LBL_122"];
    _eventowner_ulbl.text = [helper gettranslation:@"LBL_187"];
    
    [_approvebtn_ulbl setTitle:[helper gettranslation:@"LBL_046"] forState:UIControlStateNormal];
    [_detailbtnaction_ulbl setTitle:[helper gettranslation:@"LBL_096"] forState:UIControlStateNormal];
    [_attachmentbtn_ulbl setTitle:[helper gettranslation:@"LBL_322"] forState:UIControlStateNormal];
    [_speakerbtn_ulbl setTitle:[helper gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [_commbtn_ulbl setTitle:[helper gettranslation:@"LBL_343"] forState:UIControlStateNormal];
    [_teambtn_ulbl setTitle:[helper gettranslation:@"LBL_562"] forState:UIControlStateNormal];
    [_expensebtn_ulbl setTitle:[helper gettranslation:@"LBL_397"] forState:UIControlStateNormal];

//    submitevent_lbl
//    cancelevent_lbl
    
    _gallery_ulbl.text = [helper gettranslation:@"LBL_142"];

}


-(void)refreshdata
{
    Eventlist = [helper query_alldata:@"Event"];
    Eventlist = [Eventlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark navigate to home page method
-(IBAction)swiperightaction:(id)sender
{
    NSLog(@"appdelegate swipe variable value is %@",appdelegate.swipevar);
    if(!([appdelegate.swipevar isEqualToString:@"Sign"] || [appdelegate.swipevar isEqualToString:@"Gallery"]))
    {
        [self performSegueWithIdentifier:@"EventDetailToHomeScreen" sender:self];
    }
}

- (IBAction)details_segmentcontrol:(id)sender
{
    
    
    switch (self.Details_segmentcontrol.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"segment index is %ld",(long)self.Details_segmentcontrol.selectedSegmentIndex);

           // [attendyview.view removeFromSuperview];
            //[actview.view removeFromSuperview];
            
            //
            //[scheduleview.view removeFromSuperview];
            //[surveyview.view removeFromSuperview];
            appdelegate.selectedItineraraySegment=0;
            appdelegate.itinearySearch =@"NO";
            appdelegate.ItiniearySearch=nil;
            [self setDetailView];
            
            break;
        case 1:
            NSLog(@"segment index is %ld",(long)self.Details_segmentcontrol.selectedSegmentIndex);
           // [attendyview.view removeFromSuperview];
            
            //
            
            appdelegate.selectedItineraraySegment=0;
            appdelegate.itinearySearch =@"NO";
            appdelegate.ItiniearySearch=nil;
             [self attendybtnaction:self];
            
            break;
        case 2:
            appdelegate.selectedItineraraySegment=0;
            appdelegate.itinearySearch =@"NO";
            appdelegate.ItiniearySearch=nil;
            [self schedulebtnaction:self];
            
            
            break;
        case 3:
            NSLog(@"Activity segment index is %ld",(long)self.Details_segmentcontrol.selectedSegmentIndex);
           // [actview.view removeFromSuperview];
            appdelegate.selectedItineraraySegment=0;
            appdelegate.itinearySearch =@"NO";
            appdelegate.ItiniearySearch=nil;
            [self activitybtnaction:self];
            
            break;
            
        case 4:
            NSLog(@"Activity segment index is %ld",(long)self.Details_segmentcontrol.selectedSegmentIndex);
              appdelegate.selectedItineraraySegment=0;
            [self surveybtnaction:self];
          
            break;
            
        case 5:
            appdelegate.selectedItineraraySegment=0;
            [self ItinearyBtnAction:self];


        default:
            break;
    }
    
}


- (IBAction)attendybtnaction:(id)sender
{
 
   
    [attendyview.view removeFromSuperview];
    
    [scheduleview.view removeFromSuperview];
    [actview.view removeFromSuperview];
    [surveyview.view removeFromSuperview];
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [detailview removeFromSuperview];
    [attendyview removeFromParentViewController];
    [scheduleview removeFromParentViewController];
    [actview removeFromParentViewController];
    [surveyview removeFromParentViewController];
    
    
    [itinearyView removeFromParentViewController];
    [itinearyView.view removeFromSuperview];

    
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [venuebtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [venuebtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    attendyview = [self.storyboard instantiateViewControllerWithIdentifier:@"AttendeesView"];

    [attendyview.view setFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
    [self addChildViewController:attendyview];
    [mainview addSubview:attendyview.view];
    [attendyview didMoveToParentViewController:self];
}

- (IBAction)schedulebtnaction:(id)sender
{
    
    
    [attendyview.view removeFromSuperview];
    [scheduleview.view removeFromSuperview];
    [actview.view removeFromSuperview];
    [surveyview.view removeFromSuperview];
   
    
    [detailview removeFromSuperview];
    [itinearyView removeFromParentViewController];
     [itinearyView.view removeFromSuperview];
    [attendyview removeFromParentViewController];
    [scheduleview removeFromParentViewController];
    [surveyview removeFromParentViewController];
    [actview removeFromParentViewController];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [venuebtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [venuebtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    scheduleview = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleView"];
    
    [scheduleview.view setFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
    [self addChildViewController:scheduleview];
    [mainview addSubview:scheduleview.view];
    [scheduleview didMoveToParentViewController:self];
}

- (IBAction)surveybtnaction:(id)sender
{
    [attendyview.view removeFromSuperview];
    [scheduleview.view removeFromSuperview];
    [surveyview.view removeFromSuperview];
    [scheduleview.view removeFromSuperview];
    [itinearyView.view removeFromSuperview];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [detailview removeFromSuperview];
    [itinearyView removeFromParentViewController];
    [attendyview removeFromParentViewController];
    [scheduleview removeFromParentViewController];
    [surveyview removeFromParentViewController];
   // [scheduleview removeFromParentViewController];
    
//    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
//    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [venuebtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [attendybtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
//    
//    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [venuebtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    surveyview = [self.storyboard instantiateViewControllerWithIdentifier:@"SurveyView"];
    
    [surveyview.view setFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
    [self addChildViewController:surveyview];
    [mainview addSubview:surveyview.view];
    [surveyview didMoveToParentViewController:self];
}
-(IBAction)ItinearyBtnAction:(id)sender
{
    [attendyview.view removeFromSuperview];
    [scheduleview.view removeFromSuperview];
    [surveyview.view removeFromSuperview];
    [scheduleview.view removeFromSuperview];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [itinearyView removeFromParentViewController];
    [itinearyView.view removeFromSuperview];
    [detailview removeFromSuperview];
    [attendyview removeFromParentViewController];
  //  [scheduleview removeFromParentViewController];
    [surveyview removeFromParentViewController];
    [scheduleview removeFromParentViewController];
    
    itinearyView =  [self.storyboard instantiateViewControllerWithIdentifier:@"ItinearyScreen"];
    [itinearyView.view setFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
    [self addChildViewController:itinearyView];
    [mainview addSubview:itinearyView.view];
    [itinearyView didMoveToParentViewController:self];
}



- (IBAction)activitybtnaction:(id)sender
{
  
  
    
    [actview.view removeFromSuperview];
    [scheduleview.view removeFromSuperview];
    [attendyview.view removeFromSuperview];
    [surveyview.view removeFromSuperview];
    [itinearyView.view removeFromSuperview];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [detailview removeFromSuperview];
    [itinearyView removeFromParentViewController];
    [actview removeFromParentViewController];
    [attendyview removeFromParentViewController];
    [scheduleview removeFromParentViewController];
    [surveyview removeFromParentViewController];
    
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
    [activitybtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [venuebtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [venuebtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    actview = [self.storyboard instantiateViewControllerWithIdentifier:@"Activityview"];
   //
   [actview.view setFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
    
//    [actview.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    [self addChildViewController:actview];
    [mainview addSubview:actview.view];
 
   [actview didMoveToParentViewController:self];
    
 
}


- (IBAction)venuebtnaction:(id)sender
{
    [venueview removeFromSuperview];
    [detailview removeFromSuperview];
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    venueview = [[VenueView alloc]initWithFrame:CGRectMake(0, 300, 844, 272)];
    NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
    eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if([eventvenue count] >0)
    {
        venueview.addresslineone.text = [eventvenue[0] valueForKey:@"address1"];
        venueview.addresslinetwo.text = [eventvenue[0] valueForKey:@"address2"];
        venueview.city.text = [eventvenue[0] valueForKey:@"city"];
        venueview.state.text = [eventvenue[0] valueForKey:@"state"];
        venueview.pincode.text = [eventvenue[0] valueForKey:@"zipcode"];
        
        NSString *Address = [NSString stringWithFormat:@"%@\n%@ %@ %@, %@, %@, %@  %@ ",[eventvenue[0] valueForKey:@"location"]?: @"",[eventvenue[0] valueForKey:@"address1"]?: @"",[eventvenue[0] valueForKey:@"address2"]?: @"",[eventvenue[0] valueForKey:@"address3"]?: @"",[eventvenue[0] valueForKey:@"city"]?: @"",[eventvenue[0] valueForKey:@"state"]?: @"",[eventvenue[0] valueForKey:@"country"]?: @"",[eventvenue[0] valueForKey:@"zipcode"]?: @""];
        
        CLLocationDegrees lat = [[eventvenue[0] valueForKey:@"latitude"] doubleValue];
        CLLocationDegrees lon = [[eventvenue[0] valueForKey:@"longitude"]doubleValue];
        
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat longitude:lon  zoom:15];
        
        // Indicating the map frame bounds :venueview.mapView.bounds
        myMapView = [GMSMapView mapWithFrame:CGRectMake(326, 8, 549, 283) camera: camera];
        myMapView.myLocationEnabled = YES;
        
        
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(lat, lon);
        marker.title = @"";
        marker.snippet = Address;
        marker.map = myMapView;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        
        
        [venueview.mapView addSubview: myMapView];
   
        
    }
    
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [venuebtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
    [venuebtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
  //  NSLog(@"latitude is %@", [eventvenue[0] valueForKey:@"latitude"]);
   // NSLog(@"longitude is %@", [eventvenue[0] valueForKey:@"longitude"]);
    

    
    
    [mainview addSubview:venueview];

    
}



- (IBAction)gallerybtnaction:(id)sender
{
   galleryimagelist = [[helper query_alldata:@"Event_Gallery"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if ([galleryimagelist count]>0)
    {
       /* for(int i=0;i<galleryimagelist.count;i++)
        {
        
            galleryimgArray=[galleryimagelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"location_url=nil"]];
        }
        
        if(galleryimgArray.count>0)
      {
           [self checknullentry];
        }*/
        
        galleryimgArray = [[NSMutableArray alloc]init];
        NSString *GalleryFDRdownload = [helper query_data:@"name" inputdata:@"GalleryFDRdownload" entityname:@"S_Config_Table"];
       // if ([GalleryFDRdownload isEqualToString:@"NO"])
        //{
           for (int i =0; i<[galleryimagelist count]; i++)
            {
                if ([[galleryimagelist[i] valueForKey:@"imageid"] length]>0)
                {
                    if ([galleryimagelist[i] valueForKey:@"location_url"])
                    {
                        UIImage *Galleryimage = [UIImage imageWithContentsOfFile:[galleryimagelist[i] valueForKey:@"location_url"]];
                        if(!Galleryimage)
                        {
                            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                            [dict setValue:[galleryimagelist[i] valueForKey:@"imageid"] forKey:@"imageurl"];
                            [dict setValue:[galleryimagelist[i] valueForKey:@"id"] forKey:@"imageid"];
                            [galleryimgArray addObject:dict];
                       }
                    }
                    else
                    {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        [dict setValue:[galleryimagelist[i] valueForKey:@"imageid"] forKey:@"imageurl"];
                        [dict setValue:[galleryimagelist[i] valueForKey:@"id"] forKey:@"imageid"];
                        [galleryimgArray addObject:dict];

                    }
               }
                
            }
           if ([appdelegate.fdrstr isEqualToString:@"N"])
            {
                if ([galleryimgArray count]>0)                {
                    [helper imagesDownloader:nil galleryimagearr:galleryimgArray resumeimages:nil];
                }
               
            }
//       // }
        

       [self performSegueWithIdentifier:@"EventDetailToGallery" sender:self];
    }
    else
    {
      [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_621"] withDuaration:4.0];
    }
    
    
  //  [self performSegueWithIdentifier:@"DetailstoReschedue" sender:self];
    
}
/*-(void) checknullentry
{
    
    
    NSError *error;
    //    imageid is the server url for image
    
    [helper showWaitCursor:@"Getting details. Please Wait !"];
    NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Galleryimages"];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:documentsPath])
    {
        [[NSFileManager defaultManager]createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    
    for(NSDictionary *item in galleryimgArray)
    {
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.png",[item valueForKey:@"fileName"]]]; //Add the file name
        NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[item valueForKey:@"imageid"]]];
        if (curlData)
        {
            [curlData writeToFile:filePath atomically:YES];
            [helper update_imageurl:filePath entityname:@"Event_Gallery" entityid:[item valueForKey:@"id"]];
        }
        else
        {
            [helper printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download GALLERY: %@",[item valueForKey:@"imageurl"]]] ;
        }
    }
    
   // galleryimagelist = [helper query_alldata:@"Event_Gallery"];
    //galleryimagelist = [galleryimagelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    [helper removeWaitCursor];
}*/

- (IBAction)editevent_btn:(id)sender
{
    NSString *networkstatus = [helper checkinternetconnection];
    
    if ([networkstatus isEqualToString:@"Y"])
    {
        [self performSegueWithIdentifier:@"Editevent" sender:self];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_441"] message:[helper gettranslation:@"LBL_630"] action:[helper gettranslation:@"LBL_462"]];
    }
}


- (IBAction)submitevent_btn:(id)sender
{
    @autoreleasepool
    {
        if ([eventstatuslic isEqualToString:@"Planned"])
        {
            NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
            eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
            if(eventvenue.count>0)
            {
                eventStatusFlag=NO;
                [self changeeventstatus:@"Submitted"];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_573"] delegate:self  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
                [alert show];
            }
        }
    else if ([eventstatuslic isEqualToString:@"Completed"])
    {
        eventStatusFlag=NO;
        [self changeeventstatus:@"Closed"];
    }
    else if ([eventstatuslic isEqualToString:@"Approved"])
    {
        eventStatusFlag=NO;
        [self changeeventstatus:@"Started"];
    }
    else if ([eventstatuslic isEqualToString:@"Started"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        NSDate *today=[NSDate date];
        NSDate *endDate=[dateFormatter dateFromString:[Eventlist[0] valueForKey:@"enddate"]];
        
        if ([today compare:endDate] == NSOrderedDescending)
        {
            eventStatusFlag=YES;
            [self changeeventstatus:@"Completed"];
            //NSLog(@"date1 is later than date2");
        }
        else if ([today compare:endDate] == NSOrderedAscending)
        {
            eventStatusFlag=NO;
             [self changeeventstatus:@"Completed"];
           // NSLog(@"date1 is earlier than date2");
        }
        else
        {
            eventStatusFlag=NO;
             [self changeeventstatus:@"Completed"];
           // NSLog(@"dates are the same");
        }
        
       // [self changeeventstatus:@"Completed"];
    }
    else if ([eventstatuslic isEqualToString:@"Rejected"])
    {
        [self changeeventstatus:@"Submitted"];
    }
    }
}

- (IBAction)cancelevent_btn:(id)sender;
{
    [self changeeventstatus:@"Cancelled"];
}





#pragma mark event detail button action
- (IBAction)detailbtnaction:(id)sender
{
    //assigning value to the detail view
    [self setDetailView];
}
- (IBAction)ExpenseButtonAction:(id)sender
{
    
    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];
    [detailview removeFromSuperview];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
    //[ExpenseButtonOutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [TeamButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    ExpenseView = [self.storyboard instantiateViewControllerWithIdentifier:@"ExpenseView"];
    [ExpenseView.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:ExpenseView];
    [mainview addSubview:ExpenseView.view];
    [ExpenseView didMoveToParentViewController:self];
}

- (IBAction)CommunicationButtonAction:(id)sender
{
   // [detailview removeFromSuperview];
    
    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];
    [detailview removeFromSuperview];
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
   // [CommunicationButtonOutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [TeamButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    TemplateView = [self.storyboard instantiateViewControllerWithIdentifier:@"TemplateView"];
    [TemplateView.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:TemplateView];
    [mainview addSubview:TemplateView.view];
    [TemplateView didMoveToParentViewController:self];
}
- (IBAction)speakerbtn:(id)sender
{
    
   // [detailview removeFromSuperview];
    
    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];
    [detailview removeFromSuperview];
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
   // [speakerbtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0f green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [TeamButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    speakerview = [self.storyboard instantiateViewControllerWithIdentifier:@"SpeakerView"];
    [speakerview.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:speakerview];
    [mainview addSubview:speakerview.view];
    [speakerview didMoveToParentViewController:self];
}
- (IBAction)attachmentbtn:(id)sender
{
    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];
    [detailview removeFromSuperview];
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    //[detailview removeFromSuperview];
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
   // [AddAttachmentOutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [TeamButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    AttachmentView = [self.storyboard instantiateViewControllerWithIdentifier:@"AttachmentView"];
    [AttachmentView.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:AttachmentView];
    [mainview addSubview:AttachmentView.view];
    [AttachmentView didMoveToParentViewController:self];
}
- (IBAction)TeamButtonAction:(id)sender
{
   // [detailview removeFromSuperview];
    
    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];
    [detailview removeFromSuperview];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
    //[TeamButtonOutlet setTitleColor:[UIColor colorWithRed:75.0/255.0f green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [TeamButtonOutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    TeamView = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamView"];
    [TeamView.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:TeamView];
    [mainview addSubview:TeamView.view];
    [TeamView didMoveToParentViewController:self];
}
- (IBAction)ApproveButtonAction:(id)sender
{

    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];
    [detailview removeFromSuperview];
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
   // [ApproveButtonOutlet setTitleColor:[UIColor colorWithRed:75.0/255.0f green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [TeamButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    ApprovalView = [self.storyboard instantiateViewControllerWithIdentifier:@"ApprovalView"];
    [ApprovalView.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:ApprovalView];
    [mainview addSubview:ApprovalView.view];
    [ApprovalView didMoveToParentViewController:self];
}


- (IBAction)approvalbtn:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailToApproval" sender:self];
}

- (IBAction)teambtn:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailTeam" sender:self];
}

#pragma mark menu button action
- (IBAction)AttendeeButtonAction:(id)sender
{
    //delete sync log segue
    [self performSegueWithIdentifier:@"EventDetailToAttendee" sender:self];
}

- (IBAction)HomeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailToHomeScreen" sender:self];
}

- (IBAction)SpeakerButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailtoSpeakerview" sender:self];
}

- (IBAction)ActivityButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailtoActivitiesview" sender:self];
}

- (IBAction)CalendarButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailtoCalendarview" sender:self];
}
-(IBAction)ActivityPlanButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailToActivityPlan" sender:self];
}


#pragma mark detail view method
-(void)setDetailView
{
   
    [scheduleview removeFromParentViewController];
    [attendyview removeFromParentViewController];
    [actview removeFromParentViewController];
    [surveyview removeFromParentViewController];
    [scheduleview.view removeFromSuperview];
    [attendyview.view removeFromSuperview];
    [actview.view removeFromSuperview];
    [surveyview.view removeFromSuperview];
    [detailview removeFromSuperview];
    [itinearyView removeFromParentViewController];
    [itinearyView.view removeFromSuperview];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
  
    [ExpenseView removeFromParentViewController];
    [speakerview removeFromParentViewController];
    [AttachmentView removeFromParentViewController];
    [TeamView removeFromParentViewController];
    [ApprovalView removeFromParentViewController];
    [TemplateView removeFromParentViewController];

   
    
     [self refreshdata];
    
    [detailbtnoutlet setBackgroundImage:[UIImage imageNamed:@"selectback.png"] forState:UIControlStateNormal];
   // [detailbtnoutlet setTitleColor:[UIColor colorWithRed:75.0/255.0 green:99.0/255.0f blue:124.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [detailbtnoutlet setTitleColor:[helper DarkBlueColour] forState:UIControlStateNormal];
    
    [AddAttachmentOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [activitybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [attendybtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [TeamButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [speakerbtnoutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ApproveButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [activitybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [attendybtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddAttachmentOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [TeamButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [speakerbtnoutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ApproveButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CommunicationButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ExpenseButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    

    
    if ([Eventlist count]>0)
    {
        eventname.text = [Eventlist[0] valueForKey:@"name"];
        eventdescription.text = [Eventlist[0] valueForKey:@"desc"];
        NSLog(@"status here %@",[Eventlist[0] valueForKey:@"status"]);
        [approvebtnoutlet setTitle:[Eventlist[0] valueForKey:@"status"] forState:UIControlStateNormal];
    
        UIColor *titlecolor = [helper getstatuscolor:[Eventlist[0] valueForKey:@"statuslic"] entityname:@"Event"];
        [approvebtnoutlet setTitleColor:titlecolor forState:UIControlStateNormal];
    
        startdate.text = [helper formatingdate:[Eventlist[0] valueForKey:@"startdate"] datetime:@"FormatDate"];
        enddate.text = [helper formatingdate:[Eventlist[0] valueForKey:@"enddate"] datetime:@"FormatDate"];
        _owner.text = [Eventlist[0] valueForKey:@"ownedby"];
        
        
        eventstatuslic = [Eventlist[0] valueForKey:@"statuslic"];
        eventstatus = [Eventlist[0] valueForKey:@"status"];
        
        _department_ulbl.text=[helper gettranslation:@"LBL_092"];
        _department.text=[Eventlist[0] valueForKey:@"department"];

    
        NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
        eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        if (eventvenue.count >0)
        {
            location.text = [eventvenue[0] valueForKey:@"location"];
        }
        else
        {
            location.text = @"Pick Event Location";
        }
        
        
        detailview = [[Detail alloc]initWithFrame:CGRectMake(0, 300, 844, 272)];
        //calculate cost value from expenses
        
        
        detailview.eventcategory_ulbl.text = [helper gettranslation:@"LBL_062"];
        detailview.eventtype_ulbl.text = [helper gettranslation:@"LBL_266"];
        detailview.eventsubtype_ulbl.text = [helper gettranslation:@"LBL_246"];
        detailview.eventprod_ulbl.text = [helper gettranslation:@"LBL_204"];
        detailview.eventestcost_ulbl.text = [helper gettranslation:@"LBL_128"];
        detailview.eventapprcost_ulbl.text = [helper gettranslation:@"LBL_049"];
        detailview.eventactcost_ulbl.text = [helper gettranslation:@"LBL_010"];
        detailview.eventremcost_ulbl.text = [helper gettranslation:@"LBL_211"];
       
        
        
        NSArray *ExpenseArr = [[helper query_alldata:@"Event_Expense"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        ExpenseArr=[ExpenseArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"statuslic like %@",@"Approved"]];
        float actualcost = 0;
        for (int i =0; i<[ExpenseArr count]; i++)
        {
            actualcost = actualcost + [[ExpenseArr[i] valueForKey:@"amount"] floatValue];
        }
    
        detailview.category.text = [Eventlist[0] valueForKey:@"category"];
        [self setupEventProductView];
       // detailview.product.text  = [Eventlist[0] valueForKey:@"product"];
        detailview.type.text = [Eventlist[0] valueForKey:@"type"];
        detailview.subtype.text = [Eventlist[0] valueForKey:@"subtype"];

    
        if([appdelegate.EventUserRoleArray containsObject:@"E_BGT_MNG"])
        {
             NSString *currencyCode=[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"];
            
            detailview.actualcost.text = [NSString stringWithFormat:@"%@ %.2f",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],actualcost];
            
            
            detailview.estimatedcost.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"estimatedcost"]];
            
            detailview.approvedcost.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"approvedcost"]];
            
            float remaining_budget_float = [[Eventlist[0] valueForKey:@"approvedcost"] floatValue] - actualcost;
            detailview.remainingbudget.text = [NSString stringWithFormat:@"%@ %.2f",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],remaining_budget_float];
        }
        else
        {
            detailview.eventestcost_ulbl.hidden = YES;
            detailview.estimatedcost.hidden = YES;
            
            detailview.eventapprcost_ulbl.hidden = YES;
            detailview.approvedcost.hidden = YES;
            
            detailview.eventactcost_ulbl.hidden = YES;
            detailview.actualcost.hidden = YES;
            
            detailview.eventremcost_ulbl.hidden = YES;
            detailview.remainingbudget.hidden = YES;
        }
            
        
        
       canedit = [Eventlist[0] valueForKey:@"canEdit"];
    
        if ([canedit isEqualToString:@"N"])
        {
            _editevent_btn.hidden = YES;
            _editevent_lbl.hidden = YES;
            
            _submitevent_btn.hidden = YES;
            _submitevent_lbl.hidden = YES;
            
            _cancelevent_btn.hidden = YES;
            _cancelevent_lbl.hidden = YES;
            
            [_addNewGroupBtn setHidden:YES];
            [_groupsLBL setHidden:YES];
            
        }
        else
        {
            _editevent_lbl.hidden = NO;
            [_groupsLBL setHidden:NO];
            
            _submitevent_btn.hidden = NO;
            _submitevent_lbl.hidden = NO;
            
            _cancelevent_btn.hidden = NO;
            _cancelevent_lbl.hidden = NO;
           
            [_addNewGroupBtn setHidden:NO];
        }
        [self statemodelcheck:[Eventlist[0] valueForKey:@"statuslic"]];
    }
   
    [mainview addSubview:detailview];

}
#pragma mark unwind method
-(IBAction)unwindtoEventDetail:(UIStoryboardSegue *)segue
{
    
    
    if([appdelegate.swipevar isEqualToString:@"Sign"])
    {
        appdelegate.swipevar = @"UnSign";
        [self attendybtnaction:self];
        
    }
    else if ([appdelegate.swipevar isEqualToString:@"activityview"])
    {
      // [actview refreshactivitylist];
        [self activitybtnaction:self];
    }
    else if ([appdelegate.swipevar isEqualToString:@"attendeeview"])
    {
        [self attendybtnaction:self];
    }
    else if ([appdelegate.swipevar isEqualToString:@"speakerview"])
    {
        [self speakerbtn:self];
    }
    else if ([appdelegate.swipevar isEqualToString:@"AttachmentView"])
    {
        [self attachmentbtn:self];
    }
    else if ([appdelegate.swipevar isEqualToString:@"Itineary"])
    {
        [self ItinearyBtnAction:self];

    }
    else if([appdelegate.swipevar isEqualToString:@"Product"])
    {
        [self setDetailView];
    }
    else if ([appdelegate.swipevar isEqualToString:@"EditEvent"] || [appdelegate.swipevar isEqualToString:@"locationview"])
    {
        NSLog(@"appdelegate.eventid: %@",appdelegate.eventid);
        Eventlist = [[helper query_alldata:@"Event"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
        [self setDetailView];
    }
    else if ([appdelegate.swipevar isEqualToString:@"approvals"])
    {
        [self setDetailView];
        [self ApproveButtonAction:self];
    }
    else if ([appdelegate.swipevar isEqualToString:@"Group"])
    {
        NSArray *TEMP =[[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        if(TEMP.count == 0 ){
            [_groupbadge setHidden:YES];
        }else
        [self updateBadgeValue:TEMP.count];
        
        [self setDetailView];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"EventDetailToUpdateActivity"])
    {
        updateactivity_ViewController *controller = segue.destinationViewController;
        controller.cancelSegue = @"UnwindfromUpdateToDetail";
        controller.activity_id = appdelegate.activityid;
       // NSLog(@"cancel segue value %@",controller.cancelSegue);
    }
    if ([[segue identifier] isEqualToString:@"ActivityListtoUpsertActivity"])
    {
        //ActivityView *var = [[ActivityView alloc]init];
        //NSLog(@"type of actio string %@",var.TypeOfActionStr);
        UpsertActivityView *controller = segue.destinationViewController;
        controller.TypeOfActionStr = @"Add";
        controller.CancelSegue = @"UnwindfromActivityUpsert";
    }
    if ([[segue identifier] isEqualToString:@"ActivityListtoEditUpsertActivity"])
    {
        UpsertActivityView *controller = segue.destinationViewController;
        controller.TypeOfActionStr = @"Edit";
        controller.CancelSegue = @"UnwindfromActivityUpsert";
    }
    else if ([[segue identifier] isEqualToString:@"EventSpeakerToSpeaker"])
    {
        EventSpeakerViewController *controller = segue.destinationViewController;
        controller.fromview = @"EventSpeaker";
    }
    else if ([[segue identifier] isEqualToString:@"Editevent"])
    {
        UpsertEventVC *controller = segue.destinationViewController;
        controller.senderview = @"Editevent";
        controller.cancelsegue = @"editcancel_btn";
    }
    else if ([[segue identifier] isEqualToString:@"EventDetailToAddlocation"])
    {
        
        EventLocation *controller = segue.destinationViewController;
        NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
        eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        if([eventvenue count] >0)
        {
            controller.LocName = [eventvenue[0] valueForKey:@"location"];
            controller.Address1 = [eventvenue[0] valueForKey:@"address1"];
            controller.Address2 = [eventvenue[0] valueForKey:@"address2"];
            controller.Address3 = [eventvenue[0] valueForKey:@"address3"];
            controller.City = [eventvenue[0] valueForKey:@"city"];
            controller.State = [eventvenue[0] valueForKey:@"state"];
            controller.Country = [eventvenue[0] valueForKey:@"country"];
            controller.ZipCode = [eventvenue[0] valueForKey:@"zipcode"];
            controller.GeoLat = [eventvenue[0] valueForKey:@"latitude"];
            controller.GeoLong = [eventvenue[0] valueForKey:@"longitude"];
            controller.AddressId = [eventvenue[0] valueForKey:@"id"];
            controller.map_attached_flg = [eventvenue[0] valueForKey:@"map_attached_flg"];
            if (![appdelegate.EventUserRoleArray containsObject:@"E_LOC_MNG"])
            {
                controller.editable = @"NO";
            }
        }
    }
    else if ([[segue identifier] isEqualToString:@"EventDetailToUserRole"])
    {
       
    }
    else if ([[segue identifier] isEqualToString:@"EventDetailToupsertapprovals"])
    {
        
    }
    else if ([[segue identifier] isEqualToString:@"EventDetailToaddspeaker"])
    {
        
    }
    else if([[segue identifier] isEqualToString:@"Attendeeappr_to_AdvanceSearch"])
    {
        AdvanceSearchView *controller = segue.destinationViewController;
        controller.cancelSegue = @"UnwindtoAttendeeappr";
    }
   
    else if ([[segue identifier] isEqualToString:@"showAttendeeAttachment"])
    {
        AttachmentViewController *Controller=segue.destinationViewController;
        
        if([appdelegate.AttachmentType isEqualToString:@"Attendee"])
            Controller.senderView=@"Attendee";
        else
            Controller.senderView=@"Itenary";
    }
    
   // EventDetailToUpdateActivity        //EventDetailstoAddSurvey
}
- (IBAction)getlocation_btn:(id)sender
{
    [self performSegueWithIdentifier:@"EventDetailToAddlocation" sender:self];
}
- (IBAction)check_btn:(id)sender
{
    NSArray *array = [helper query_alldata:@"Event"];
    NSMutableArray*combined = [NSMutableArray new];
    
    // Iterate over each unique id value
    for(id key  in [NSSet setWithArray:[array valueForKeyPath:@"name"]])
    {
        NSLog(@"Key initial %@", [[key substringWithRange:NSMakeRange(0,1)] capitalizedString]);
        
        
        // skip missing keys
        if([key isKindOfClass:[NSNull class]])
            continue;
        
        // Sub array with only id = key
        NSArray* filtered = [array filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSDictionary* evaluatedObject, NSDictionary *bindings) {
            return [evaluatedObject valueForKey:@"venue"] && [[[[evaluatedObject valueForKey:@"name"] substringWithRange:NSMakeRange(0,1)] capitalizedString] isEqual:[[key substringWithRange:NSMakeRange(0,1)] capitalizedString]];
        }]];
        
        // Grab the dates
        NSArray*        dates = [filtered valueForKeyPath:@"venue"];
        
        // add the new dictionary
        [combined addObject:@{ @"Initial":[[key substringWithRange:NSMakeRange(0,1)] capitalizedString], @"venue":dates }];
    }
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:combined];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
     NSLog(@"arrayWithoutDuplicates count: %lu",(unsigned long)arrayWithoutDuplicates.count);
    //exit(0);

    
   // NSArray *uniqueKeys = [arrayWithoutDuplicates valueForKeyPath:@"@distinctUnionOfArrays.@allKeys"];
    //NSArray *names = [arrayWithoutDuplicates valueForKey:<#(nonnull NSString *)#>]
    
    NSMutableArray *initialarray = [[NSMutableArray alloc] init];
    
    for (int i=0; i<arrayWithoutDuplicates.count; i++)
    {
        NSString *string = [arrayWithoutDuplicates[i] valueForKey:@"Initial"];
     //   NSArray *Values = [arrayWithoutDuplicates[i] valueForKey:@"venue"];
        
        [initialarray addObject:string];
    }
}


- (void)addItemViewController:(Team *)controller didFinishEnteringItem:(NSString *)item
{
    //using delegate method, get data back from second page view controller and set it to property declared in here
    NSLog(@"This was returned from secondPageViewController: %@",item);
    self.returnedItem=item;
    
    //add item to array here and call reload
}

-(void) changeeventstatus:(NSString *) status_val
{
    @autoreleasepool
    {
    NSString *status = [helper getvaluefromlic:@"EVENT_STAT" lic:status_val];
    if([status isEqualToString:@"Completed"] && (!eventStatusFlag))
    {
        sem_event = dispatch_semaphore_create(0);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_584"] message:[NSString stringWithFormat:@"%@", [helper gettranslation:@"LBL_769"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_624"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                          {
                              semp = @"No";
                              dispatch_semaphore_signal(sem_event);
                              
                          }]];
        [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                          {
                              semp = @"Yes";
                              dispatch_semaphore_signal(sem_event);
                          }]];
        
        
        [self presentViewController:alert animated:true completion:nil];
        
        while (dispatch_semaphore_wait(sem_event, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }

    }
   else
   {
       sem_event = dispatch_semaphore_create(0);
       UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_584"] message:[NSString stringWithFormat:@"%@ %@",[helper gettranslation:@"LBL_478"],status] preferredStyle:UIAlertControllerStyleAlert];
       [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_624"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                      {
                          semp = @"No";
                          dispatch_semaphore_signal(sem_event);
                          
                      }]];
       [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                      {
                          semp = @"Yes";
                          dispatch_semaphore_signal(sem_event);
                      }]];
    
    
       [self presentViewController:alert animated:true completion:nil];
       while (dispatch_semaphore_wait(sem_event, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
       
   }
    
    if ([semp isEqualToString:@"Yes"])
    {
        
        NSString *status_licval = [helper getLic:@"EVENT_STAT" value:status];
        
        NSError *error;
        [helper showWaitCursor:[helper gettranslation:@"LBL_576"]];
        
        NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
        
        [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
        [JsonDict setValue:status_licval forKey:@"StatusLIC"];
        [JsonDict setValue:status forKey:@"Status"];
        
        
        NSString *jsonString;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
            
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEvent" pageno:@"" pagecount:@"" lastpage:@""];
        
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            
            
            NSManagedObjectContext  *context = [appdelegate managedObjectContext];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.eventid like %@",appdelegate.eventid];
            
            NSError *error;
            [fetchRequest setEntity:entity];
            [fetchRequest setPredicate:predicate];
            NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
            
            if([fetchedObjects count]>0)
            {
                for (NSManagedObject *object in fetchedObjects)
                {
                    [object setValue:status forKey:@"status"];
                    [object setValue:status_licval forKey:@"statuslic"];
                }
                
                [context save:&error];
            }
            
        }
        
        Eventlist = [[helper query_alldata:@"Event"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
        
        appdelegate.PerformDeltaSync = @"YES";
        [helper get_EMS_data];
        
        
        [self setDetailView];
        [helper removeWaitCursor];
        
    }
    }
}



-(void) statemodelcheck:(NSString *) status
{
    if ([status isEqualToString:@"Planned"])
    {
        _cancelevent_btn.hidden = YES;
        _cancelevent_lbl.hidden = YES;
        
        _submitevent_lbl.text = [helper gettranslation:@"LBL_248"]; //@"Submit";
    }
    else if ([status isEqualToString:@"Submitted"])
    {
        _submitevent_btn.hidden = YES;
        _submitevent_lbl.hidden = YES;
    }
    else if ([status isEqualToString:@"Approved"])
    {
//        _submitevent_lbl.text = @"Start";
        if([canedit isEqualToString:@"Y"])
        {
            _submitevent_btn.hidden = NO;
            _submitevent_lbl.hidden = NO;
        }
            _submitevent_lbl.text = [helper gettranslation:@"LBL_556"]; // @"Start";
    }
    else if ([status isEqualToString:@"Rejected"])
    {
        ///Both options will be available
//        _submitevent_lbl.text = @"Re-Submit";
        if([canedit isEqualToString:@"Y"])
        {
            _submitevent_btn.hidden = NO;
            _submitevent_lbl.hidden = NO;
        }
        _submitevent_lbl.text = [helper gettranslation:@"LBL_518"]; // @"Re-Submit";
    }
    else if ([status isEqualToString:@"Started"])
    {
        if([canedit isEqualToString:@"Y"])
        {
            _submitevent_btn.hidden = NO;
            _submitevent_lbl.hidden = NO;
        }
        _submitevent_lbl.text = [helper gettranslation:@"LBL_068"]; // @"Complete";
    }
    else if ([status isEqualToString:@"Completed"])
    {
        _cancelevent_btn.hidden = YES;
        _cancelevent_lbl.hidden = YES;
        if([canedit isEqualToString:@"Y"])
        {
            _submitevent_btn.hidden = NO;
            _submitevent_lbl.hidden = NO;
        }
//        _submitevent_lbl.text = @"Close";
        _submitevent_lbl.text = [helper gettranslation:@"LBL_342"]; // @"Close";
    }
    else if ([status isEqualToString:@"Cancelled"])
    {
        _cancelevent_btn.hidden = YES;
        _cancelevent_lbl.hidden = YES;
        
        _submitevent_btn.hidden = YES;
        _submitevent_lbl.hidden = YES;
    }
    else if ([status isEqualToString:@"Closed"])
    {
        _cancelevent_btn.hidden = YES;
        _cancelevent_lbl.hidden = YES;
        
        _submitevent_btn.hidden = YES;
        _submitevent_lbl.hidden = YES;
    }

}


/* -------------------Do Not Delete this Function. Its Important
 -(NSPredicate *) getstatusvalue:(NSString *)inputvalue
 {
 NSPredicate *predicate;
 
 inputvalue = [helper getLic:@"EVENT_STAT" value:inputvalue];
 
 LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_STAT"]];
 
 
 
 LOVArr = [helper query_alldata:@"Event_LOV"];
 
 
 return predicate;
 
 }*/

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}
-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}

-(void)setupEventProductView
{
    EventProductView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductView"];
     [EventProductView.view setFrame:CGRectMake(720, 415, 280, 330)];
    
    [EventProductView.view removeFromSuperview];
    [EventProductView removeFromParentViewController];
    
    [self addChildViewController:EventProductView];
    [self.view addSubview:EventProductView.view];
    [EventProductView didMoveToParentViewController:self];
//    [UIView animateWithDuration:0.5 animations:^{
//
//        [EventProductView.view setFrame:CGRectMake(705, 415, 300, 335)];
//
//    }];
}
- (IBAction)AddGroupBtn:(id)sender {
    [self performSegueWithIdentifier:@"AddNewGroup" sender:self];
    
}
-(void)updateBadgeValue:(NSUInteger)value
{
    _groupbadge= [GIBadgeView new];
    _groupbadge.backgroundColor = [UIColor colorWithRed:0/255.0 green:158/255.0 blue:222/255.0 alpha:1.0];
    _groupbadge.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    _groupbadge.topOffset = 0.0f;
    _groupbadge.rightOffset = 0.0f;

    [_groupbadge setBadgeValue:value];
    
    [_addNewGroupBtn addSubview:self.groupbadge];
}
@end
