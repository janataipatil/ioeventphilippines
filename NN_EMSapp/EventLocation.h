//
//  EventLocation.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 12/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import <GoogleMaps/GoogleMaps.h>


@interface EventLocation : UIViewController<UISearchBarDelegate,CLLocationManagerDelegate>
{
    
    GMSMapView *myMapView;
    IBOutlet UIWebView *mainwebview;
    IBOutlet UITableView *pickup_tableview;
    
}

@property(strong,nonatomic) HelperClass *helper;
@property (nonatomic) Reachability* reachability;
@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic) IBOutlet UIButton *checkbox;
@property (strong,nonatomic) NSMutableArray *filteredArray;
@property (strong, nonatomic) IBOutlet UIView *searchbarView;
@property (strong, nonatomic) CLLocationManager *locationManager;


@property(strong,nonatomic)NSString *AddressId;
@property(strong,nonatomic)NSString *LocName;
@property(strong,nonatomic)NSString *Address1;
@property(strong,nonatomic)NSString *Address2;
@property(strong,nonatomic)NSString *Address3;
@property(strong,nonatomic)NSString *City;
@property(strong,nonatomic)NSString *State;
@property(strong,nonatomic)NSString *Country;
@property(strong,nonatomic)NSString *ZipCode;
@property(strong,nonatomic)NSString *GeoLat;
@property(strong,nonatomic)NSString *GeoLong;
@property(strong,nonatomic)NSString *editable;
@property(strong,nonatomic)NSString *map_attached_flg;
@property (strong, nonatomic) IBOutlet UIButton *SaveButtonOutlet;


@property (strong, nonatomic) IBOutlet UITextField *addressline1_tv;
@property (strong, nonatomic) IBOutlet UITextField *addressline2_tv;
@property (strong, nonatomic) IBOutlet UITextField *city_tv;
@property (strong, nonatomic) IBOutlet UITextField *state_tv;
@property (strong, nonatomic) IBOutlet UITextField *country_tv;
@property (strong, nonatomic) IBOutlet UITextField *zipcode_tv;
@property (strong, nonatomic) IBOutlet UITextField *LocName_tv;


@property (strong, nonatomic) IBOutlet UILabel *Address_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *location_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrline1_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrline2_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrcity_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrzipcode_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrstate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *addrcountry_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *titleview_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;

@property (strong, nonatomic) IBOutlet UIButton *linkmapbtn_ulbl;




@end
