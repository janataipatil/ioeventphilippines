//
//  EventLocation.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 12/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "EventLocation.h"
#import <GooglePlaces/GooglePlaces.h>
#import <CoreLocation/CoreLocation.h>


@import GooglePlaces;
@import GooglePlacePicker;


@interface EventLocation ()<GMSAutocompleteViewControllerDelegate>
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    CLLocationDegrees lat;
    CLLocationDegrees lon;
    BOOL checkBoxSelected;
    NSString *dropdownflag;
    NSUInteger count;
    NSArray *Eventlovlist;
    dispatch_semaphore_t sem_add;
    
    NSString *filedownloadloc;
    NSString *loclocation;

    
}

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation EventLocation
{
    GMSPlacesClient *_placesClient;
    GMSPlacePicker *_placePicker;
    GMSAutocompleteResultsViewController *_resultsViewController;
    UISearchController *_searchController;
    
    
    
    GMSAutocompleteTableDataSource *_tableDataSource;
    UISearchDisplayController *_searchDisplayController;
    
}



@synthesize helper,checkbox,SaveButtonOutlet;
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    _placesClient = [GMSPlacesClient sharedClient];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    // Do any additional setup after loading the view, typically from a nib.
    
    _searchbar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, -8, 580, 50)];
    _searchbar.showsCancelButton = false;
    
    //CLLocation Manager to get your current Location
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];

    _searchbar.backgroundImage = [[UIImage alloc] init];
    _searchbar.backgroundColor = [UIColor clearColor];
    _searchbar.barTintColor = [UIColor colorWithRed:227/255.0 green:234/255.0 blue:243/255.0 alpha:1];
    _searchbar.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    _searchbar.delegate = self;
    
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;
    
    
    _searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:_searchbar contentsController:self];

    _searchDisplayController.searchResultsDataSource = _tableDataSource;
    _searchDisplayController.searchResultsDelegate = _tableDataSource;
    _searchDisplayController.delegate = self;
    
    
    [self.searchbarView addSubview:_searchbar];
    
    lat = [_GeoLat doubleValue];
    lon = [_GeoLong doubleValue];
    
    
    
    if ([_map_attached_flg isEqualToString:@"Y"])
    {
        lat = -23.550520;
        lon = -46.633309;
        [self checkboxSelected:nil];
        myMapView.hidden = YES;
    }
    
    if (!_AddressId)
    {
        _AddressId = [helper generate_rowId];
    }
    
//    _editable = @"NO";
    
    _addressline1_tv.text = _Address1?: @"";
    if (_Address2.length>0)
    {
        _addressline2_tv.text = _Address2?: @"";
    }
    
    if (_Address3.length>0)
    {
        _addressline2_tv.text = [NSString stringWithFormat:@"%@  %@ ",_addressline2_tv.text,_Address3?: @""];
    }
    
//    _addressline2_tv.text = [NSString stringWithFormat:@"%@  %@ ",_Address2?: @"",_Address3?: @""];
    _city_tv.text = _City?: @"";
    _state_tv.text = _State?: @"";
    _country_tv.text = _Country?: @"";
    _zipcode_tv.text = _ZipCode?: @"";
    _LocName_tv.text = _LocName?: @"";
    
    NSString *map_attached_flg = map_attached_flg;
    
    if ([_editable isEqualToString:@"NO"])
    {
        _addressline1_tv.enabled = NO;
        _addressline2_tv.enabled = NO;
        _city_tv.enabled = NO;
        _state_tv.enabled = NO;
        _country_tv.enabled = NO;
        _zipcode_tv.enabled = NO;
        _LocName_tv.enabled = NO;
        _searchbar.userInteractionEnabled = NO;
        SaveButtonOutlet.userInteractionEnabled = NO;
        checkbox.userInteractionEnabled = NO;
    }
    
    [self loadgooglemap:lat lon:lon address:_LocName updatefields:@"YES"];
    
//    [checkbox setBackgroundImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
//    [checkbox setBackgroundImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateSelected];
//    [checkbox setBackgroundImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateHighlighted];
    
    [checkbox setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
    [checkbox setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateSelected];
    [checkbox setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateHighlighted];
    
    
    checkbox.adjustsImageWhenHighlighted=YES;
    [checkbox addTarget:self action:@selector(checkboxSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (checkBoxSelected)
    {
        myMapView.hidden = YES;
        mainwebview.scalesPageToFit = YES;
//        if (loclocation.length == 0)
//        {
//            loclocation = [self getstaticmap];
//        }
//        
//        if (loclocation.length > 0)
//        {
//            [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:loclocation]]];
//        }
//        else
//        {
////            [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.saj.usace.army.mil/portals/44/siteimagesInvasiveSpec/sprayschedules/No%20Map%20Available.jpg"]]];
//            [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://events.sparkdial.in/images/map.gif"]]];
//        }
    }
    else
    {
        myMapView.hidden = NO;
    }
    
    
//    [self.view addSubview:checkbox];
    
    
    
    
    [_city_tv addTarget:self action:@selector(selectrecord:) forControlEvents:UIControlEventEditingDidBegin];
    [_city_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_city_tv addTarget:self action:@selector(removetable:) forControlEvents:UIControlEventEditingDidEnd];
    
    
    [_state_tv addTarget:self action:@selector(selectrecord:) forControlEvents:UIControlEventEditingDidBegin];
    [_state_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_state_tv addTarget:self action:@selector(removetable:) forControlEvents:UIControlEventEditingDidEnd];
    
    [_country_tv addTarget:self action:@selector(selectrecord:) forControlEvents:UIControlEventEditingDidBegin];
    [_country_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_country_tv addTarget:self action:@selector(removetable:) forControlEvents:UIControlEventEditingDidEnd];

//     [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:]]];
//    NSString * webviewurl = @"";
    
    mainwebview.scalesPageToFit = YES;
    [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:loclocation]]];
    
    [self langsetuptranslations];
}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _titleview_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_161"]];
    
 
    _location_ulbl.text = [helper gettranslation:@"LBL_161"];
//    [_linkmapbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    
    
    _Address_ulbl.text = [helper gettranslation:@"LBL_030"];
    _addrline1_ulbl.text = [helper gettranslation:@"LBL_031"];
    _addrline2_ulbl.text = [helper gettranslation:@"LBL_032"];
    _addrcity_ulbl.text = [helper gettranslation:@"LBL_064"];
    _addrzipcode_ulbl.text = [helper gettranslation:@"LBL_283"];
    _addrstate_ulbl.text = [helper gettranslation:@"LBL_241"];
    _addrcountry_ulbl.text = [helper gettranslation:@"LBL_072"];
    
}

-(void) markasrequired:(UILabel *) label
{
    int labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}

-(void)checkboxSelected:(id)sender
{
    checkBoxSelected = !checkBoxSelected; /* Toggle */
    [checkbox setSelected:checkBoxSelected];

    if (checkBoxSelected)
    {
        myMapView.hidden = YES;
        mainwebview.scalesPageToFit = YES;
        if (loclocation.length == 0)
        {
            loclocation = [self getstaticmap];
        }
        
        if (loclocation.length > 0)
        {
            [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:loclocation]]];
        }
        else
        {
            //            [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.saj.usace.army.mil/portals/44/siteimages/InvasiveSpec/sprayschedules/No%20Map%20Available.jpg"]]];
            [mainwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://events.sparkdial.in/images/map.gif"]]];
        }
    }
    else
    {
        myMapView.hidden = NO;
    }

}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
//    [helper MoveViewUp:YES Height:150 ViewToBeMoved:self.parentViewController.view];
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    
    [searchBar setShowsCancelButton:NO animated:YES];
//    [helper MoveViewUp:NO Height:150 ViewToBeMoved:self.parentViewController.view];
    return YES;
}


- (BOOL)searchDisplayController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [_tableDataSource sourceTextHasChanged:searchString];
    return NO;
}
- (void)searchDisplayController:(UISearchController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    tableView.frame = CGRectMake(385, 0, 560, 362);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelbtnClicked:(UIButton *)sender
{
    
    [self removepopup];
    
//    [self getLocationFromAddressString:@"106, 1st A Main Rd, East of NGEF Layout, Bangalore, Karnataka"];
    
}

-(IBAction)savebtnClicked:(UIButton *)sender
{
//    [self removepopup];
    
    
    
    NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
    eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    if (checkbox.selected)
    {
        _map_attached_flg = @"Y";
        
    }
    else
    {
        _map_attached_flg = @"N";
    }
    
    
    if (eventvenue.count>0)
    {
        if ([_LocName_tv.text isEqualToString:[eventvenue[0] valueForKey:@"location"]?: @""] && [_addressline1_tv.text isEqualToString:[eventvenue[0] valueForKey:@"address1"]?: @""] && [_addressline2_tv.text isEqualToString:[NSString stringWithFormat:@"%@  %@ ",[eventvenue[0] valueForKey:@"address2"]?: @"",[eventvenue[0] valueForKey:@"address3"]?: @""]] && [_city_tv.text isEqualToString:[eventvenue[0] valueForKey:@"city"]?: @""] && [_country_tv.text isEqualToString:[eventvenue[0] valueForKey:@"country"]?: @""] && [_state_tv.text isEqualToString:[eventvenue[0] valueForKey:@"state"]?: @""] && [_zipcode_tv.text isEqualToString:[eventvenue[0] valueForKey:@"zipcode"]?: @""] && [_map_attached_flg isEqualToString:[eventvenue[0] valueForKey:@"map_attached_flg"]?: @""])
        {
            [self removepopup];
        }
        else
        {
            [self savemapdata];
        }
    }
    else
    {
        if (_LocName_tv.text.length == 0 && _addressline1_tv.text.length == 0 && _addressline2_tv.text.length == 0 && _city_tv.text.length == 0 && _country_tv.text.length == 0 && _state_tv.text.length == 0 && _zipcode_tv.text.length == 0)
        {
            [self removepopup];
        }
        else
        {
            [self savemapdata];
        }
        
    }

}

-(void)savemapdata
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_535"]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",_AddressId];
//    [helper delete_predicate:@"Event_Venue" predicate:predicate];
    [helper deletewitheventid:@"Event_Venue" value:appdelegate.eventid];
    
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Venue" inManagedObjectContext:context];
    NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
    
    [newrecord setValue:appdelegate.eventid forKey:@"eventid"];
    [newrecord setValue:_addressline1_tv.text forKey:@"address1"];
    [newrecord setValue:_addressline2_tv.text forKey:@"address2"];
    //        [newrecord setValue:[item valueForKey:@"Address3"] forKey:@"address3"];
//    [newrecord setValue:_city_tv.text forKey:@"city"];
//    [newrecord setValue:_country_tv.text forKey:@"country"];
    
    [newrecord setValue:_city_tv.text forKey:@"city"];
    [newrecord setValue:[helper getLic:@"CITY" value:_city_tv.text] forKey:@"citylic"];
    
    [newrecord setValue:_country_tv.text forKey:@"country"];
    [newrecord setValue:[helper getLic:@"COUNTRY" value:_country_tv.text] forKey:@"countrylic"];
    
    [newrecord setValue:_state_tv.text forKey:@"state"];
    [newrecord setValue:[helper getLic:@"STATE" value:_state_tv.text] forKey:@"statelic"];
    
    [newrecord setValue:_AddressId forKey:@"id"];
    [newrecord setValue:_LocName_tv.text forKey:@"location"];
    [newrecord setValue:_state_tv.text forKey:@"state"];
    [newrecord setValue:_zipcode_tv.text forKey:@"zipcode"];
    [newrecord setValue:[NSString stringWithFormat:@"%f",lat] forKey:@"latitude"];
    [newrecord setValue:[NSString stringWithFormat:@"%f",lon] forKey:@"longitude"];
    if (checkbox.selected)
    {
        NSLog(@"checkbox is selected");
        [newrecord setValue:@"Y" forKey:@"map_attached_flg"];
        
    }
    else
    {
        NSLog(@"checkbox is not selected");
        [newrecord setValue:@"N" forKey:@"map_attached_flg"];
    }
    
    
    
    NSError *error;
    [context save:&error];
    
    
    NSMutableDictionary *eventaddress_dict = [[NSMutableDictionary alloc]init];
    NSMutableArray *EventAddress_ar = [[NSMutableArray alloc]init];
    
    [eventaddress_dict setValue:_AddressId forKey:@"ID"];
    [eventaddress_dict setValue:appdelegate.eventid forKey:@"EventID"];
    [eventaddress_dict setValue:_LocName_tv.text forKey:@"Location"];
    [eventaddress_dict setValue:_addressline1_tv.text forKey:@"Address1"];
    [eventaddress_dict setValue:_addressline2_tv.text forKey:@"Address2"];
    [eventaddress_dict setValue:@"" forKey:@"Address3"];
    
    
    [eventaddress_dict setValue:[helper getLic:@"CITY" value:_city_tv.text] forKey:@"CityLIC"];
    [eventaddress_dict setValue:_city_tv.text forKey:@"City"];
    
    [eventaddress_dict setValue:[helper getLic:@"STATE" value:_state_tv.text] forKey:@"StateLIC"];
    [eventaddress_dict setValue:_state_tv.text forKey:@"State"];
    
    [eventaddress_dict setValue:[helper getLic:@"COUNTRY" value:_country_tv.text] forKey:@"CountryLIC"];
    [eventaddress_dict setValue:_country_tv.text forKey:@"Country"];
    
    
    
    [eventaddress_dict setValue:_zipcode_tv.text forKey:@"ZipCode"];
    [eventaddress_dict setValue:[NSString stringWithFormat:@"%f",lat] forKey:@"GeoLat"];
    [eventaddress_dict setValue:[NSString stringWithFormat:@"%f",lon] forKey:@"GeoLong"];
    if (checkbox.selected)
    {
        [eventaddress_dict setValue:[helper stringtobool:@"Y"] forKey:@"MapAttached"];
        
    }
    else
    {
        [eventaddress_dict setValue:[helper stringtobool:@"N"] forKey:@"MapAttached"];
    }
    
    
    
    [EventAddress_ar addObject:eventaddress_dict];
    
    NSString *jsonString;
    
    // for (int i =0; i<[EventAttendee_ar count]; i++)
    // {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAddress_ar  options:0 error:&error];
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventVenue" pageno:@"" pagecount:@"" lastpage:@""];
    
    appdelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:appdelegate.eventid type:@"Update Event Location" entityname:@"Event Location" Status:@"Open"];
    
    [helper update_Venue_data:appdelegate.eventid inputdata:_LocName_tv.text entityname:@"Event"];

    [helper removeWaitCursor];
    
    [self removepopup];
}

-(void)removepopup
{
    appdelegate.swipevar =@"locationview";
    [self performSegueWithIdentifier:@"locationToEventDetail" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}


// Handle the user's selection.
- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource didAutocompleteWithPlace:(GMSPlace *)place
{
    [_searchDisplayController setActive:NO animated:YES];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    //_searchbar.text = [NSString stringWithFormat:@"%f,%f",place.coordinate.latitude, place.coordinate.longitude];
    
    _Address1 = @"";
    _Address2 = @"";
    _Address3 = @"";
    _City = @"";
    _State = @"";
    _Country = @"";
    _ZipCode = @"";
    
    NSArray *addresscomponent = place.addressComponents;
    for (int i=0; i<addresscomponent.count; i++)
    {
        GMSAddressComponent *comp = addresscomponent[i];
        NSLog(@"Place attributions %@", [comp valueForKey:@"type"] );
        
        if ([[comp valueForKey:@"type"] isEqualToString:@"street_number"] || [[comp valueForKey:@"type"] isEqualToString:@"route"]||[[comp valueForKey:@"type"] isEqualToString:@"sublocality_level_1"])
        {
            _Address1 =  [NSString stringWithFormat:@"%@  %@ ",_Address1?: @"",[comp valueForKey:@"name"]?: @""];
        }
        else if ([[comp valueForKey:@"type"] isEqualToString:@"sublocality_level_2"])
        {
             _Address2 = [comp valueForKey:@"name"]?: @"";
        }
        else if ([[comp valueForKey:@"type"] isEqualToString:@"sublocality_level_3"])
        {
             _Address3 = [comp valueForKey:@"name"]?: @"";
        }
        else if ([[comp valueForKey:@"type"] isEqualToString:@"locality"])
        {
             _City = [comp valueForKey:@"name"]?: @"";
        }
        else if ([[comp valueForKey:@"type"] isEqualToString:@"administrative_area_level_1"])
        {
             _State = [comp valueForKey:@"name"]?: @"";
        }
        else if ([[comp valueForKey:@"type"] isEqualToString:@"country"])
        {
             _Country = [comp valueForKey:@"name"]?: @"";
        }
        else if ([[comp valueForKey:@"type"] isEqualToString:@"postal_code"])
        {
             _ZipCode = [comp valueForKey:@"name"]?: @"";
        }
    }
    
    _LocName = place.name;
//    NSLog(@"Place administrative_area_level_1 %@", [addresscomponent[0] valueForKey:@"administrative_area_level_1" ]);
    
    _searchbar.text = place.name;
    _searchbar.frame = CGRectMake(0, -8, 580, 50);
    
    [self loadgooglemap:place.coordinate.latitude lon:place.coordinate.longitude address:place.formattedAddress updatefields:@"Yes"];
}

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource didFailAutocompleteWithError:(NSError *)error
{
    [_searchDisplayController setActive:NO animated:YES];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

- (void)didUpdateAutocompletePredictionsForTableDataSource: (GMSAutocompleteTableDataSource *)tableDataSource
{
    // Turn the network activity indicator off.
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    // Reload table data.
    [_searchDisplayController.searchResultsTableView reloadData];
    _searchbar.frame = CGRectMake(0, -8, 580, 50);
}

- (void)didRequestAutocompletePredictionsForTableDataSource: (GMSAutocompleteTableDataSource *)tableDataSource
{
    // Turn the network activity indicator on.
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    // Reload table data.
    
    [_searchDisplayController.searchResultsTableView reloadData];
    _searchbar.frame = CGRectMake(0, -8, 580, 50);
}

-(void) loadgooglemap:(CLLocationDegrees )latitude lon:(CLLocationDegrees )longitude address:(NSString *)address updatefields:(NSString *)updatefields
{
    GMSCameraPosition *camera;
    if(latitude)
    {
         camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude  zoom:15];
    }
    else
    {
        NSString *cod = [helper query_data:@"name" inputdata:@"Geocodes" entityname:@"S_Config_Table"];
        
        if(![cod isEqualToString:@"No Matching Data Found"])
        {
            NSArray *coordinates = [cod componentsSeparatedByString:@","];
            if(coordinates.count >0)
            {
                NSString *GLat = [NSString stringWithFormat:@"%f",[coordinates[0]floatValue]];
                NSString *GLon = [NSString stringWithFormat:@"%f",[coordinates[1]floatValue]];
        
                camera = [GMSCameraPosition cameraWithLatitude:[GLat doubleValue] longitude:[GLon doubleValue]  zoom:15];
            }
        }
        else
        {
            NSString *GLat =[NSString stringWithFormat:@"%f",12.9716];
            NSString *GLon =[NSString stringWithFormat:@"%f",77.5946];
            camera = [GMSCameraPosition cameraWithLatitude:[GLat doubleValue] longitude:[GLon doubleValue]  zoom:15];
        }
    }
    // Indicating the map frame bounds :venueview.mapView.bounds
    [myMapView removeFromSuperview];
    
    myMapView = [GMSMapView mapWithFrame:mainwebview.frame camera: camera];
    myMapView.myLocationEnabled = YES;
    _searchbar.frame = CGRectMake(0, -8, 580, 50);
    

    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(latitude, longitude);
    marker.title = @"";
    marker.snippet = address;
    marker.map = myMapView;
    marker.appearAnimation = kGMSMarkerAnimationPop;

    [_searchView addSubview: myMapView];
    
    if ([updatefields isEqualToString:@"Yes"])
    {
        _addressline1_tv.text = _Address1;
        _addressline2_tv.text = [NSString stringWithFormat:@"%@  %@ ",_Address2?: @"",_Address3?: @""];
        _city_tv.text = _City;
        _state_tv.text = _State;
        _country_tv.text = _Country;
        _zipcode_tv.text = _ZipCode;
        _LocName_tv.text = _LocName;
        
        lat = latitude;
        lon = longitude;
    }
    
    
}

#pragma mark textfeild delegate method
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    if(textField == _LocName_tv)
    {
        [helper MoveViewUp:YES Height:0 ViewToBeMoved:self.parentViewController.view];
    }
    else if(textField == _addressline1_tv)
    {
        [helper MoveViewUp:YES Height:0 ViewToBeMoved:self.parentViewController.view];

    }
    else if (textField == _addressline2_tv)
    {
        [helper MoveViewUp:YES Height:0 ViewToBeMoved:self.parentViewController.view];

    }
    else if (textField == _city_tv)
    {
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];

    }
    else if (textField == _state_tv)
    {
        [helper MoveViewUp:YES Height:150 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == _country_tv)
    {
        [helper MoveViewUp:YES Height:200 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == _zipcode_tv)
    {
        [helper MoveViewUp:YES Height:200 ViewToBeMoved:self.parentViewController.view];
    }
    
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField == _LocName_tv)
    {
        [helper MoveViewUp:NO Height:0 ViewToBeMoved:self.parentViewController.view];
    }
    else if(textField == _addressline1_tv)
    {
        [helper MoveViewUp:NO Height:0 ViewToBeMoved:self.parentViewController.view];
        
    }
    else if (textField == _addressline2_tv)
    {
        [helper MoveViewUp:NO Height:0 ViewToBeMoved:self.parentViewController.view];
        
    }
    else if (textField == _city_tv)
    {
        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.parentViewController.view];
        
    }
    else if (textField == _state_tv)
    {
        [helper MoveViewUp:NO Height:150 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == _country_tv)
    {
        [helper MoveViewUp:NO Height:200 ViewToBeMoved:self.parentViewController.view];
    }
    else if (textField == _zipcode_tv)
    {
        [helper MoveViewUp:NO Height:200 ViewToBeMoved:self.parentViewController.view];
    }
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSString *searchaddress = [NSString stringWithFormat:@"%@, %@ %@ %@, %@, %@",_LocName_tv.text?: @"",_addressline1_tv.text?: @"",_addressline2_tv.text?: @"",_city_tv.text?: @"",_state_tv.text?: @"",_country_tv.text?: @""];
    
    NSLog(@"Search Completed: %@",searchaddress);

    [self.view endEditing:YES];
//    CLLocationCoordinate2D center = [self getLocationFromAddressString:@"106, 1st A Main Rd, East of NGEF Layout, Bangalore, Karnataka" ];
//    CLLocationCoordinate2D center1 = [self getLocationFromAddressString:@"Vivanta by Taj, Mahatma Gandhi Road Yellappa Chetty Layout  Sivanchetti Gardens  Bengaluru, Karnataka, India" ];
    CLLocationCoordinate2D center2 = [self getLocationFromAddressString:searchaddress];
    
    
    
        return NO;
}

- (void) reverseGeocodeCoordinate:(CLLocationCoordinate2D)coordinate completionHandler:(GMSReverseGeocodeCallback)handler
{
    
}

#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == pickup_tableview)
    {
        
        CGFloat height = pickup_tableview.contentSize.height;
        CGFloat maxHeight = pickup_tableview.superview.frame.size.height - pickup_tableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = pickup_tableview.frame;
            if([dropdownflag isEqualToString:@"citydropdown"])
            {
                frame.origin.x = 14;
                frame.origin.y = 255;
                frame.size.width = 270;
            }
            if([dropdownflag isEqualToString:@"statedropdown"])
            {
                frame.origin.x = 14;
                frame.origin.y = 299;
                frame.size.width = 270;
            }
            if([dropdownflag isEqualToString:@"countrydropdown"])
            {
                frame.origin.x = 14;
                frame.origin.y = 346;
                frame.size.width = 270;
            }
            
            frame.size.height = height;
            pickup_tableview.frame = frame;
        }];
    }
    
}

- (IBAction)city_btn:(id)sender
{
    
}


- (IBAction)state_btn:(id)sender
{
    
}

- (IBAction)country_btn:(id)sender
{
    
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [Eventlovlist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"scellidentifier";
    
    customcell = [pickup_tableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
    
    
    UILabel *Value = (UILabel *) [contentView viewWithTag:1];
    Value.text = [Eventlovlist[indexPath.row] valueForKey:@"Value"];
    
    return customcell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([dropdownflag isEqualToString:@"citydropdown"])
    {
        _city_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        [_city_tv resignFirstResponder];
        
    }
    if([dropdownflag isEqualToString:@"statedropdown"])
    {
        _state_tv.text = [Eventlovlist[indexPath.row] valueForKey:@"value"];
        [_state_tv resignFirstResponder];
        
    }
    if([dropdownflag isEqualToString:@"countrydropdown"])
    {
        _country_tv.text =[Eventlovlist[indexPath.row] valueForKey:@"value"];
        [_country_tv resignFirstResponder];
        
    }
    pickup_tableview.hidden = YES;
    
}



//-------------------------------------------------------------------

-(void)selectrecord:(UITextField *)theTextField
{
    if ([theTextField isEqual:_city_tv])
    {
        pickup_tableview.hidden = YES;
        dropdownflag = @"citydropdown";
        Eventlovlist = [helper query_alldata:@"Event_LOV"];
        Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"]];
        [pickup_tableview reloadData];
        [self adjustHeightOfTableview:pickup_tableview];
        pickup_tableview.hidden = NO;
    }
    else if ([theTextField isEqual:_state_tv])
    {
        pickup_tableview.hidden = YES;
        dropdownflag = @"statedropdown";
        Eventlovlist = [helper query_alldata:@"Event_LOV"];
        Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"]];
        [pickup_tableview reloadData];
        [self adjustHeightOfTableview:pickup_tableview];
        pickup_tableview.hidden = NO;
        
    }
    else if ([theTextField isEqual:_country_tv])
    {
        pickup_tableview.hidden = YES;
        dropdownflag = @"countrydropdown";
        Eventlovlist = [helper query_alldata:@"Event_LOV"];
        Eventlovlist = [Eventlovlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"]];
        [pickup_tableview reloadData];
        [self adjustHeightOfTableview:pickup_tableview];
        pickup_tableview.hidden = NO;
        
    }
    else if ([theTextField isEqual:_LocName_tv])
    {
        
    }

}

-(void)removetable:(UITextField *)theTextField
{

    if ([theTextField isEqual:_city_tv])
    {
        [_city_tv resignFirstResponder];
    }
    else if ([theTextField isEqual:_state_tv])
    {
        [_state_tv resignFirstResponder];
    }
    else if ([theTextField isEqual:_country_tv])
    {
        [_country_tv resignFirstResponder];
        
    }
    
}
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText;
    NSPredicate *lovpredicate;
    
    
    if ([theTextField isEqual:_city_tv])
    {
        searchText = _city_tv.text;
        lovpredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"CITY"];
    }
    else if ([theTextField isEqual:_state_tv])
    {
        searchText = _state_tv.text;
        lovpredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"STATE"];
    }
    else if ([theTextField isEqual:_country_tv])
    {
       searchText = _country_tv.text;
        lovpredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"COUNTRY"];
    }
    
    if (count > searchText.length)
    {
        // NSLog(@"Text has been deleted");
        Eventlovlist = nil;
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:lovpredicate];
    }
    else
    {
        // NSLog(@"Text has added");
    }
    
    if(searchText.length == 0)
    {
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:lovpredicate];
        // NSLog(@"partcatlog_data: %@",Salespartcatlog_data);
    }
    else
    {
        Eventlovlist = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:lovpredicate];
        count = searchText.length;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        Eventlovlist = [NSMutableArray arrayWithArray:[Eventlovlist filteredArrayUsingPredicate:predicate]];
    }
    
    [pickup_tableview reloadData];
    [pickup_tableview reloadInputViews];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // NSLog(@"touches began");
    UITouch *touch = [touches anyObject];
    
    if(touch.view!=pickup_tableview)
    {
        pickup_tableview.hidden = YES;
        
        [_city_tv resignFirstResponder];
        [_state_tv resignFirstResponder];
        [_country_tv resignFirstResponder];
    }
}

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressText {
//    double latitude = 0, longitude = 0;
//    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
//    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
//    if (result) {
//        NSScanner *scanner = [NSScanner scannerWithString:result];
//        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
//            [scanner scanDouble:&latitude];
//            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
//                [scanner scanDouble:&longitude];
//            }
//        }
//    }
    CLLocationCoordinate2D center;
//    center.latitude=latitude;
//    center.longitude = longitude;
//
//    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
//    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
//    return center;

    
    //addressText = here send Address  like as @"Jaipur Rajasthan"
    [helper showWaitCursor:[helper gettranslation:@"LBL_407"]];
    
    sem_add = dispatch_semaphore_create(0);
    CLGeocoder* gc = [[CLGeocoder alloc] init];
    [gc geocodeAddressString:addressText completionHandler:^(NSArray *placemarks, NSError *error)
    {
        if ([placemarks count]>0)
        {
            // get the first one
            CLPlacemark* mark = (CLPlacemark*)[placemarks objectAtIndex:0];
            double latval = mark.location.coordinate.latitude;
            double lngval = mark.location.coordinate.longitude;
            
            lat = [[NSString stringWithFormat:@"%f",latval] doubleValue];
            lon = [[NSString stringWithFormat:@"%f",lngval] doubleValue];
            
            
            [self loadgooglemap:lat lon:lon address:addressText updatefields:@"NO"];
            
            dispatch_semaphore_signal(sem_add);

        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_573"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
            
             dispatch_semaphore_signal(sem_add);
        }
    }];
    
    
    
    
    
    while (dispatch_semaphore_wait(sem_add, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    [helper removeWaitCursor];

    return center;
}

-(NSString *) getstaticmap
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_407"]];
    filedownloadloc = @"";
    NSString *storageurl;
    NSArray *eventattachments = [helper query_alldata:@"Event_Attachments"];
    eventattachments = [eventattachments filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    

    eventattachments = [eventattachments filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attachmenttypelic LIKE[c] %@",@"Map"]];
    
    if (eventattachments.count >0)
    {
        storageurl = [eventattachments[0] valueForKey:@"storageid"];
        sem_add = dispatch_semaphore_create(0);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
            
                
            
            filedownloadloc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"Attachmentfile.png"];
                           
             NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:storageurl]];
             //  NSData *curlData = [NSData dataWithContentsOfFile:attachmentfile_url];
                           
            if ( curlData )
            {
                [curlData writeToFile:filedownloadloc atomically:YES];
            }
            else
            {
                NSLog(@"Unable to download Attachment: %@",storageurl);
            }
            dispatch_semaphore_signal(sem_add);
           
         });
        while (dispatch_semaphore_wait(sem_add, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }

    }
        [helper removeWaitCursor];
    return filedownloadloc;
}




@end
