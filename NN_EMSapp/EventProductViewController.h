//
//  EventProductViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 08/11/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"

@interface EventProductViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *NameProductLBL;
@property (strong, nonatomic) IBOutlet UILabel *ProductValueLBL;
@property (strong, nonatomic) IBOutlet UITableView *ProductTableView;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
- (IBAction)addProductBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *titleLBL;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *ProductView;

@end
