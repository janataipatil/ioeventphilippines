//
//  EventProductViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 08/11/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventProductViewController.h"

@interface EventProductViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSArray *Products;
    NSString *semp;
}
@end

@implementation EventProductViewController

- (void)viewDidLoad {
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    Products = [[helper query_alldata:@"Product"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eid like %@",appdelegate.eventid]];
    
    [self setupTranslations];
    
    
    _ProductView.layer.borderColor = [[UIColor blackColor] CGColor];
    _ProductView.layer.borderWidth = 0.3;

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupTranslations
{
    _titleLBL.text = [helper gettranslation:@"LBL_906"];
    _ProductValueLBL.text = [helper gettranslation:@"LBL_895"];
    _NameProductLBL.text = [helper gettranslation:@"LBL_204"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [Products count];
   // return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *cell = [self.ProductTableView dequeueReusableCellWithIdentifier:@"ProductTypes"];
     UIView *contentView = cell.contentView;
    
    UILabel *ProductName = (UILabel *)[contentView viewWithTag:1];
    UILabel *ProuductValue = (UILabel *)[contentView viewWithTag:2];
    UIButton *EditButton = (UIButton *)[contentView viewWithTag:3];
    
    ProductName.text = [Products[indexPath.row] valueForKey:@"product"];
    ProuductValue.text = [NSString stringWithFormat:@"%ld",[[Products[indexPath.row] valueForKey:@"weight"] integerValue]];
    
    [EditButton addTarget:self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    dispatch_queue_t sem_event;
    sem_event=dispatch_semaphore_create(0);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_740"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   semp = @"Yes";
                                   dispatch_semaphore_signal(sem_event);
                                   
                               }];
    [alert addAction:okaction];
    
    
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       semp = @"No";
                                       dispatch_semaphore_signal(sem_event);
                                   }];
    
    [alert addAction:cancelaction];
    [self presentViewController:alert animated:NO completion:nil];
    
    
    while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}

    if([semp isEqualToString:@"Yes"])
    {
        [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
        NSMutableArray *ReqArray =[[NSMutableArray alloc]init];
    
        NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
        [JsonDict setValue:[Products[indexPath.row] valueForKey:@"id"] forKey:@"ID"];
        [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
        [JsonDict setValue:[Products[indexPath.row] valueForKey:@"lic"] forKey:@"ProductLIC"];
        [JsonDict setValue:[Products[indexPath.row] valueForKey:@"weight"] forKey:@"Weightage"];
        [JsonDict setValue:[helper stringtobool:@"Y"] forKey:@"Deleted"];
    
        [ReqArray addObject:JsonDict];
    
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ReqArray options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventProduct" pageno:@"" pagecount:@"" lastpage:@""];
    
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_590"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            NSLog(@"Json data is %@",jsonData);
        
            NSPredicate *ExpenseIDPredicate = [NSPredicate predicateWithFormat:@"id like %@",[Products[indexPath.row] valueForKey:@"id"]];
            [helper delete_predicate:@"Product" predicate:ExpenseIDPredicate];
        }
   
    Products = [[helper query_alldata:@"Product"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eid like %@",appdelegate.eventid]];
    [_ProductTableView reloadData];
    [helper removeWaitCursor];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}

-(IBAction)editButton:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.ProductTableView];
    NSIndexPath *indexPath = [self.ProductTableView indexPathForRowAtPoint:buttonPosition];
    
    appdelegate.itnearyid = [Products[indexPath.row] valueForKey:@"id"];
     [self.parentViewController performSegueWithIdentifier:@"AddProductSegue" sender:self];
}

-(IBAction)addProductBtn:(id)sender {
    appdelegate.itnearyid = nil;
    [self.parentViewController performSegueWithIdentifier:@"AddProductSegue" sender:self];
}



@end
