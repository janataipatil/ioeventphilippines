//
//  EventReschedule_ViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 4/3/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "HSDatePickerViewController.h"
#import "UpsertEventVC.h"

@interface EventReschedule_ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UILabel *eventname_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventstdate_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventenddate_lbl;

- (IBAction)save_btn:(id)sender;
- (IBAction)cancel_btn:(id)sender;


@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *titlebar_ulbl;
@property (strong, nonatomic)  NSMutableArray *eventDetails;

@property (strong, nonatomic) IBOutlet UILabel *titleBar_lbl;

@property (strong, nonatomic) IBOutlet UITableView *schedulelistview;
@property(strong,nonatomic) HelperClass *helper;

@property (strong, nonatomic) IBOutlet UILabel *eventname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventsdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventedate_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *delete_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *sessionname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *oldstarttime_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *oldendtime_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *newstarttime_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *newendtime_ulbl;



@end
