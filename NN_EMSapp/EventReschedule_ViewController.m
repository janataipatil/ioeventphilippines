//
//  EventReschedule_ViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 4/3/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventReschedule_ViewController.h"

@interface EventReschedule_ViewController ()
{
    NSArray *sessionlist;
    NSInteger count;
    AppDelegate *appdelegate;
    NSPredicate *eventidpredicate;
    NSMutableArray *RescheduleSessionList;
    
    UpsertEventVC *eventVC;
    NSManagedObjectContext *context;
    NSIndexPath *selected_index;
    NSString *selected_field;
    double eventStartDate;
    double eventEndDate;
    NSMutableArray *SelectedSpeaker;
    NSString *startEndDate;

   // UIButton *startDate;
    //UIButton *endDate;
    NSDate *eventEnd;
    NSDate *eventStart;
    dispatch_semaphore_t sem_session;
    
    NSMutableArray *modifiedSession;
    NSIndexPath *indexPath;
     NSString *startSessionDate,*endSessionDate;
    NSArray *eventData;
    BOOL validated;
    
}

@end

@implementation EventReschedule_ViewController

@synthesize helper,schedulelistview,eventDetails;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    eventidpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    [schedulelistview setEditing:YES animated:YES];
      
    validated=NO;
    RescheduleSessionList=[[NSMutableArray alloc]init];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [self SetupEventSchedule];
    sessionlist = [[helper query_alldata:@"Event_Session"] filteredArrayUsingPredicate:eventidpredicate];
    context=  [appdelegate managedObjectContext];
    
    modifiedSession=[[NSMutableArray alloc]init];
    
    for (int i=0; i<sessionlist.count; i++)
    {
        NSString *sdate=[sessionlist[i] valueForKey:@"startdate"];
        NSString *edate=[sessionlist[i] valueForKey:@"enddate"];
        double SessionStartDate=[sdate doubleValue];
        double SessionEndDate=[edate doubleValue];

        if((SessionStartDate<eventStartDate || SessionStartDate>eventEndDate) || (SessionEndDate>eventEndDate || SessionEndDate<eventStartDate))
        {
           
                [RescheduleSessionList addObject:sessionlist[i]];
        }
        
    }
    
    modifiedSession=[RescheduleSessionList mutableCopy];
    
    SelectedSpeaker = [[NSMutableArray alloc] init];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];
    
    eventStart=[dateFormatter1 dateFromString:[eventData[0] valueForKey:@"startdate"]];
    eventEnd=[dateFormatter1 dateFromString:[eventData[0] valueForKey:@"enddate"]];
    
    [self langsetuptranslations];
 
    
}
-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    
    _titlebar_ulbl.text = [helper gettranslation:@"LBL_026"];
    
    _eventname_ulbl.text = [helper gettranslation:@"LBL_131"];
    _eventsdate_ulbl.text = [helper gettranslation:@"LBL_239"];
    _eventedate_ulbl.text = [helper gettranslation:@"LBL_122"];
    _sessionname_ulbl.text=[helper gettranslation:@"LBL_223"];
    _oldstarttime_ulbl.text=[NSString stringWithFormat:@"%@ %@",[helper gettranslation:@"LBL_771"],[helper gettranslation:@"LBL_239"]];
    _oldendtime_ulbl.text=[NSString stringWithFormat:@"%@ %@",[helper gettranslation:@"LBL_771"],[helper gettranslation:@"LBL_122"]];
    
    _newstarttime_ulbl.text=[NSString stringWithFormat:@"%@ %@",[helper gettranslation:@"LBL_772"],[helper gettranslation:@"LBL_239"]];
    _newendtime_ulbl.text=[NSString stringWithFormat:@"%@ %@",[helper gettranslation:@"LBL_772"],[helper gettranslation:@"LBL_122"]];
    
    _delete_ulbl.text=[helper gettranslation:@"LBL_080"];
    _titleBar_lbl.text=[helper gettranslation:@"LBL_764"];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) SetupEventSchedule
{
    eventData=[[helper query_alldata:@"Event"]filteredArrayUsingPredicate:eventidpredicate];
    eventStartDate=[appdelegate.eventTempStartDate doubleValue];
    eventEndDate=[appdelegate.eventTempEndDate doubleValue];
    
//    sessionlist = [[helper query_alldata:@"Event_Session"] filteredArrayUsingPredicate:eventidpredicate];
    
    
    if([eventData count]>0)
    {
        _eventname_lbl.text = [eventData[0] valueForKey:@"name"];
        
        _eventstdate_lbl.text = [helper formatingdate:appdelegate.eventTempStartDate datetime:@"FormatDate"];
        _eventenddate_lbl.text = [helper formatingdate:appdelegate.eventTempEndDate datetime:@"FormatDate"];
        
        
        
    }
}

#pragma mark tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [RescheduleSessionList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    customcell = [self.schedulelistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;

    UILabel *sessionname = (UILabel *) [contentView viewWithTag:1];
    UILabel *old_stdate = (UILabel *) [contentView viewWithTag:2];
    UILabel *old_enddate = (UILabel *) [contentView viewWithTag:3];
    
   UIButton *startDate = (UIButton *) [contentView viewWithTag:4];
    UIButton *endDate = (UIButton *) [contentView viewWithTag:5];
   
    
    [startDate addTarget:self action:@selector(startDate:) forControlEvents:UIControlEventTouchUpInside];
    
    [endDate addTarget:self action:@selector(endDate:) forControlEvents:UIControlEventTouchUpInside];
    
    

    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];

    
    NSDate *sessionstdate= [dateFormatter1 dateFromString:[RescheduleSessionList[indexPath.row] valueForKey:@"startdate"]];
    NSDate *sessionenddate = [dateFormatter1 dateFromString:[RescheduleSessionList[indexPath.row] valueForKey:@"enddate"]];
    
    
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM HH:mm"];
    NSString *upcomingStartDate = [dateFormatter1 stringFromDate:sessionstdate];
    NSString *upcomingEndDate = [dateFormatter1 stringFromDate:sessionenddate];
    
    
    sessionname.text = [RescheduleSessionList[indexPath.row] valueForKey:@"title"];
//    session_desc.text = [arr valueForKey:@"session_desc"];
    
    
    old_stdate.text = upcomingStartDate;
    old_enddate.text = upcomingEndDate;
    //customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    customcell.tintColor = [helper DarkBlueColour];
    
    return customcell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SelectedSpeaker addObject:[sessionlist[indexPath.row] valueForKey:@"id"]];
    
    NSDictionary *data=[modifiedSession objectAtIndex:indexPath.row];
    [data setValue:@"Yes" forKey:@"Status"];
    
    [modifiedSession replaceObjectAtIndex:indexPath.row withObject:data];
}



- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[SelectedSpeaker count]; i++)
    {
        NSString *item = [SelectedSpeaker objectAtIndex:i];
        if ([item isEqualToString:[sessionlist[indexPath.row] valueForKey:@"id"]])
        {
            [SelectedSpeaker removeObject:item];
            i--;
        }
    }
    NSDictionary *data=[modifiedSession objectAtIndex:indexPath.row];
    [data setValue:@"No" forKey:@"Status"];
    
    [modifiedSession replaceObjectAtIndex:indexPath.row withObject:data];

}



-(void)delete_data:(NSMutableArray *)name
{

    
    for(int i=0;i<name.count;i++)
    {
        NSError *error;
       // NSString *id=[name[i] valueForKey:@"id"];
        [helper serverdelete:@"E_SESSION" entityid:name[i]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",name[i]];
        
        NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Event_Session"];
        
        fetchRequest.predicate=predicate;
        
        NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
        int cnt = 0;
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
            cnt++;
        }
        [context save:&error];
        
        if(matchingData)
        {
            NSLog(@"Removed");
        }
        else
        {
            NSLog(@"Error:%@",error);
        }
    }
}


- (IBAction)save_btn:(id)sender
{
    [self validateSessionData];
    
   
    if(validated)
    {
        [self savedata:eventDetails];
        [self saveServerData];
        [self saveLocalData];
        [self delete_data:SelectedSpeaker];
        [self removepopup];
        
    }
}

- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}

-(void)saveServerData
{
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
     NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    NSError *error;
    for(int i=0;i<modifiedSession.count;i++)
    {
        NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
        [JsonDict setValue:[modifiedSession[i] valueForKey:@"id"] forKey:@"ID"];
        [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
        [JsonDict setValue:[modifiedSession[i] valueForKey:@"title"] forKey:@"Title"];
        [JsonDict setValue:[modifiedSession[i] valueForKey:@"session_desc"] forKey:@"Description"];
        [JsonDict setValue:[modifiedSession[i] valueForKey:@"location"] forKey:@"Location"];
        [JsonDict setValue:[modifiedSession[i] valueForKey:@"startdate"] forKey:@"StartDate"];
        [JsonDict setValue:[modifiedSession[i] valueForKey:@"enddate"]  forKey:@"EndDate"];
        [JsonDict setValue:[helper getLic:@"SSN_TYPE" value:[modifiedSession[i] valueForKey:@"typelic"]] forKey:@"TypeLIC"];
        [JsonDict setValue:[helper stringtobool:[modifiedSession[i] valueForKey:@"canEdit"]] forKey:@"CanEdit"];
        [JsonDict setValue:[helper stringtobool:[modifiedSession[i] valueForKey:@"canDelete"]] forKey:@"CanDelete"];
    
       
        [JsonArray addObject:JsonDict];
        
    }
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArray options:0 error:&error];
    if (! JsonArray)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventSession" pageno:@"" pagecount:@"" lastpage:@""];
    
    appdelegate.senttransaction = @"Y";
    
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Edit Session" entityname:@"Add Event Sessions"  Status:@"Open"];

}
-(void)removepopup
{
    
    [self performSegueWithIdentifier:@"backtoAddEvent" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

- (void)dateTextFieldDidChange:(UITextField *)sender
{
    NSString *Sessionid;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    NSIndexPath *indexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
    
    Sessionid = [sessionlist[indexPath.row] valueForKey:@"id"];
    NSLog(@"Sessionid %@",Sessionid);
    

}


-(void)validateSessionData
{
    
    for(int i=0;i<modifiedSession.count;i++)
    {
        
            if([[modifiedSession[i] valueForKey:@"status"] isEqualToString:@"No"] || [[modifiedSession[i] valueForKey:@"status"] length]==0)
            {
                [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_774"] withDuaration:1.0];
                validated=NO;
                i=modifiedSession.count;
            }
            else
                validated = YES;
                //return YES;
        
    }
    //return nil;
}

-(void)saveSessionData:(NSString *)id startDate:(NSString *)startDate endDate:(NSString *)endDate
{
    NSError *error;
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Event_Session"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id like %@",id]; // If required to fetch specific vehicle
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    [matchingData setValue:startDate forKey:@"startdate"];
    [matchingData setValue:endDate forKey:@"enddate"];
    [context save:&error];
    
    if (!error)
    {
        
        NSLog(@"Successfully saved");
    }
    else
    {
        NSLog(@"Not Successfull");
    }

}

-(void)startDate:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    indexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];

        startEndDate = @"StartDate";
        NSDate *actEndDate;
        SOCustomCell *cell = [schedulelistview cellForRowAtIndexPath:indexPath];
        UIView *contentView = cell.contentView;
        UIButton *endDate = (UIButton *) [contentView viewWithTag:5];

        HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
        NSString *activityEndDate=[endDate.titleLabel text];

    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];
    
    
    NSDate *actStartDate=[dateFormatter1 dateFromString:appdelegate.eventTempStartDate];
    NSDate *activity_EndDate=[dateFormatter1 dateFromString:appdelegate.eventTempEndDate];
    
    
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
    NSString *eDate=[dateFormatter1 stringFromDate:activity_EndDate];
    NSString *StartDate=[dateFormatter1 stringFromDate:actStartDate];
    
    actStartDate=[dateFormatter1 dateFromString:StartDate];
    activity_EndDate=[dateFormatter1 dateFromString:eDate];

    
        hsdpvc.delegate = self;
        hsdpvc.date =[dateFormatter1 dateFromString:[eventData[0] valueForKey:@"startdate"]];

    
        if(activityEndDate.length>0)
        {
            
           
            hsdpvc.date =actStartDate;
            hsdpvc.minDate =actStartDate;
            hsdpvc.maxDate=activity_EndDate;
            
            
        }
    else
    {
        hsdpvc.date =actStartDate;
        hsdpvc.minDate =actStartDate;
        hsdpvc.maxDate=activity_EndDate;
    }
        
        [self presentViewController:hsdpvc animated:YES completion:nil];
        
}
-(void)endDate:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    indexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
    
        startEndDate = @"EndDate";
    
        SOCustomCell *cell = [schedulelistview cellForRowAtIndexPath:indexPath];
        UIView *contentView = cell.contentView;
        UIButton *startDate = (UIButton *) [contentView viewWithTag:4];
    
        HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];
    
    
        NSDate *actStartDate=[dateFormatter1 dateFromString:appdelegate.eventTempStartDate];
        NSDate *activity_EndDate=[dateFormatter1 dateFromString:appdelegate.eventTempEndDate];
    
    
        [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
        NSString *eDate=[dateFormatter1 stringFromDate:activity_EndDate];
        NSString *StartDate=[dateFormatter1 stringFromDate:actStartDate];
    
        actStartDate=[dateFormatter1 dateFromString:StartDate];
        activity_EndDate=[dateFormatter1 dateFromString:eDate];

        NSString *activityStartDate=[startDate.titleLabel text];
        hsdpvc.delegate = self;
    
        if(activityStartDate.length>0)
        {
            hsdpvc.date = actStartDate;
            hsdpvc.minDate = actStartDate;
            hsdpvc.maxDate=activity_EndDate;
            
        }
        else
        {
            hsdpvc.date=actStartDate;
            hsdpvc.minDate=actStartDate;
            hsdpvc.maxDate=activity_EndDate;
        }
        
        [self presentViewController:hsdpvc animated:YES completion:nil];
        
        
}

-(void)saveLocalData
{
    for(int i=0;i<modifiedSession.count;i++)
    {
        [self saveSessionData:[modifiedSession[i] valueForKey:@"id"] startDate:[modifiedSession[i] valueForKey:@"startdate"] endDate:[modifiedSession[i] valueForKey:@"enddate"]];
    }
}
#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
    
    SOCustomCell *cell = [schedulelistview cellForRowAtIndexPath:indexPath];
    UIView *contentView = cell.contentView;
    UIButton *startDate = (UIButton *) [contentView viewWithTag:4];
    UIButton *endDate = (UIButton *) [contentView viewWithTag:5];

   
    
    if ([startEndDate isEqualToString:@"StartDate"])
    {
        
        
        // [StartDateButtonOutlet setTitle:[dateFormater stringFromDate:date] forState:UIControlStateNormal];
       [startDate setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
        [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];

        startSessionDate=[dateFormatter1 stringFromDate:date];
    }
    else if ([startEndDate isEqualToString:@"EndDate"])
    {
        // [EndDateButtonOutlet setTitle:[dateFormater stringFromDate:date] forState:UIControlStateNormal];
        [endDate setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
        [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];

        endSessionDate=[dateFormatter1 stringFromDate:date];
    }
    
    if([startDate.titleLabel.text length]>0 && [endDate.titleLabel.text length]>0)
    {
        NSDictionary *data=[modifiedSession objectAtIndex:indexPath.row];
        [data setValue:startSessionDate forKey:@"startdate"];
        [data setValue:endSessionDate forKey:@"enddate"];
        [data setValue:@"Yes" forKey:@"Status"];
        
        [modifiedSession replaceObjectAtIndex:indexPath.row withObject:data];
        
    }
}

-(void)savedata:(NSArray *) Eventlistdata
    {
        NSError *error;
        
        [helper showWaitCursor:[helper gettranslation:@"LBL_576"]];
        
        NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
        
        
        [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"ActualCost"] forKey:@"ActualCost"];
        [JsonDict setValue:[[[helper query_alldata:@"User"] objectAtIndex:0] valueForKey:@"affiliateid"] forKey:@"AffiliateID"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"ApprovedCost"] forKey:@"ApprovedCost"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"BalanceBudget"] forKey:@"BalanceBudget"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Description"] forKey:@"Description"];
        
        
        //  [JsonDict setValue:[dateFormatter stringFromDate:edate] forKey:@"EndDate"];
        [JsonDict setValue:appdelegate.eventTempStartDate  forKey:@"StartDate"];
        
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"EstimatedCost"] forKey:@"EstimatedCost"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Name"] forKey:@"Name"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"OwnedBy"] forKey:@"OwnedBy"];
        
        //[JsonDict setValue:[dateFormatter stringFromDate:sdate] forKey:@"StartDate"];
        [JsonDict setValue:appdelegate.eventTempEndDate forKey:@"EndDate"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Visibility"] forKey:@"Visibility"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Status"] forKey:@"Status"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"CategoryLIC"] forKey:@"CategoryLIC"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"ProductLIC"] forKey:@"ProductLIC"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"StatusLIC"] forKey:@"StatusLIC"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"SubTypeLIC"] forKey:@"SubTypeLIC"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"TypeLIC"] forKey:@"TypeLIC"];
        
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Category"] forKey:@"Category"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Product"] forKey:@"Product"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Status"] forKey:@"Status"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"SubType"] forKey:@"SubType"];
        [JsonDict setValue:[Eventlistdata[0] valueForKey:@"Type"] forKey:@"Type"];

    
        
        
        NSString *jsonString;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEvent" pageno:@"" pagecount:@"" lastpage:@""];
        
        //    appdelegate.senttransaction = @"Y";
        //    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Events"  Status:@"Open"];
        //    [helper removeWaitCursor];
        //    [self removepopup];
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            
           // if([senderview isEqualToString:@"Editevent"])
            //{
                appdelegate.swipevar = @"EditEvent";
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
                NSString *calidentify = [[[[[helper query_alldata:@"Event"] filteredArrayUsingPredicate:predicate] valueForKey:@"calevent_refid"] allObjects] objectAtIndex:0];
                
                [helper delete_predicate:@"Event" predicate:predicate];
                [helper deletecal_event:calidentify];
                
                NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
                NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                
                [newrecord setValue:[JsonDict valueForKey:@"ActualCost"] forKey:@"actualcost"];
                [newrecord setValue:[JsonDict valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
                [newrecord setValue:[JsonDict valueForKey:@"ApprovedCost"] forKey:@"approvedcost"];
                [newrecord setValue:[JsonDict valueForKey:@"BalanceBudget"] forKey:@"balancebudget"];
                [newrecord setValue:[JsonDict valueForKey:@"Category"] forKey:@"category"];
                [newrecord setValue:[JsonDict valueForKey:@"Description"] forKey:@"desc"];
                [newrecord setValue:[JsonDict valueForKey:@"EndDate"] forKey:@"enddate"];
                [newrecord setValue:[JsonDict valueForKey:@"EstimatedCost"] forKey:@"estimatedcost"];
                [newrecord setValue:[JsonDict valueForKey:@"EventID"] forKey:@"eventid"];
                [newrecord setValue:[JsonDict valueForKey:@"EventID"] forKey:@"id"];
                [newrecord setValue:[JsonDict valueForKey:@"Name"] forKey:@"name"];
                [newrecord setValue:[JsonDict valueForKey:@"OwnedBy"] forKey:@"ownedby"];
                [newrecord setValue:[JsonDict valueForKey:@"Product"] forKey:@"product"];
                [newrecord setValue:[JsonDict valueForKey:@"StartDate"] forKey:@"startdate"];
                [newrecord setValue:[JsonDict valueForKey:@"Status"] forKey:@"status"];
                [newrecord setValue:[JsonDict valueForKey:@"SubType"] forKey:@"subtype"];
                [newrecord setValue:[JsonDict valueForKey:@"Type"] forKey:@"type"];
                [newrecord setValue:[JsonDict valueForKey:@"Visibility"] forKey:@"visibility"];
                [newrecord setValue:[JsonDict valueForKey:@"ActCompleted"] forKey:@"actcompleted"];
                [newrecord setValue:[JsonDict valueForKey:@"ActTotal"] forKey:@"acttotal"];
                [newrecord setValue:[JsonDict valueForKey:@"ApprovalFlag"] forKey:@"approvalflag"];
                [newrecord setValue:[JsonDict valueForKey:@"AttApproved"] forKey:@"attapproved"];
                [newrecord setValue:[JsonDict valueForKey:@"AttTotal"] forKey:@"atttotal"];
                [newrecord setValue:[JsonDict valueForKey:@"CategoryLIC"] forKey:@"categorylic"];
                [newrecord setValue:[JsonDict valueForKey:@"ProductLIC"] forKey:@"productlic"];
                [newrecord setValue:[JsonDict valueForKey:@"StatusLIC"] forKey:@"statuslic"];
                [newrecord setValue:[JsonDict valueForKey:@"SubTypeLIC"] forKey:@"subtypelic"];
                [newrecord setValue:[JsonDict valueForKey:@"TypeLIC"] forKey:@"typelic"];
                [newrecord setValue:[JsonDict valueForKey:@"Venue"] forKey:@"venue"];
                [newrecord setValue:[JsonDict valueForKey:@"Visibility"] forKey:@"visibility"];
                [newrecord setValue:nil forKey:@"calevent_refid"];
                
                
                NSError *error;
                [context save:&error];
                
               // [helper insert_transaction_local:msgbody entity_id:appdelegate.eventid type:@"Event Update" entityname:[NSString stringWithFormat:@"Update event : %@ ",[JsonDict valueForKey:@"Name"]] Status:@"Completed"];
           // }
            
            [helper DeltaSyncCalander];
        }
        [helper removeWaitCursor];
        [self removepopup];
    }

   // self.selectedDate = date;







@end
