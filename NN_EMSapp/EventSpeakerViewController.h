//
//  EventSpeakerViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/13/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "Speaker_detail.h"
#import "CreateSpeakerViewController.h"
#import "DisplayAttachment.h"
#import "TitleBar.h"
#import "MenueBar.h"
#import "HelpViewController.h"
#import "AttachmentViewController.h"

@class HelperClass;
@interface EventSpeakerViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (nonatomic,strong) HelperClass *helper;
@property(strong,nonatomic) Speaker_detail *spkdetailview;
@property (nonatomic) Reachability* reachability;

- (IBAction)menu_calendar_btn:(id)sender;
- (IBAction)menu_home_btn:(id)sender;
- (IBAction)menu_activity_btn:(id)sender;
- (IBAction)menu_synclog_btn:(id)sender;
- (IBAction)addspeaker_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *Speakerlistview;
@property(strong,nonatomic)NSString *fromview;
@property (strong, nonatomic) IBOutlet UIButton *AddSpeakerButtonOutlet;

@property (strong, nonatomic) IBOutlet UILabel *downloadProgress;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progessIndicator;


- (IBAction)Setting_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UISearchBar *SpeakerSearchBar;
@property (strong, nonatomic) IBOutlet UILabel *speaker_lbl;

//-(void)reloadspeakerlist;
-(void)refreshspeakerlist:(NSNotification*)notification;
@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;
@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;
@end
