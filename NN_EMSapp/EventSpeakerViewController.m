//
//  EventSpeakerViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/13/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "EventSpeakerViewController.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "SOCustomCell.h"

@interface EventSpeakerViewController ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSString *resumeurl;
    NSString *fileextension;
    NSMutableArray *speakerimagearr;
    NSMutableArray *resumefilearre;
    int RefreshTableview;
    NSTimer *speakerListUpdateTimer;
    NSString *speakerName;
    
}


@end

@implementation EventSpeakerViewController
{
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    
    SLKeyChainStore *keychain;
    MSClient *client;
    NSArray *speakerlist;
    
    NSString *appID;
    NSString *custID;
    NSString *AESPrivateKey;
    NSInteger ln_expandedRowIndex;
    
    NSString *sel_speakerid;
    UIButton *edit_btn;
    UIButton *del_btn;
    
    NSInteger count;
    NSString *resumedownloadloc;
    NSMutableArray *imagepatharr;
    HelpViewController *HelpView;
    NSString *currency;
}

@synthesize helper,Speakerlistview,SpeakerSearchBar,speaker_lbl,spkdetailview,fromview,TitleBarView,MenueBarView,AddSpeakerButtonOutlet;

- (void)viewDidLoad
{

    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
    
      keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    AESPrivateKey =[helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
   
    appdelegate.CurrencyCode = [helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"];
    
    currency = [[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"]];
    
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = YES;
    TitleBarView.UsernameLbl.text = [helper getUsername];
      [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
    
    _downloadProgress.text=[helper gettranslation:@"LBL_628"]; //Translation for Downloading Data
    
    //Retrieve data from user defaults
    
    NSString *lastpageStatus=[[NSUserDefaults standardUserDefaults]valueForKey:@"SpeakerListLastPage"];
    NSString *page=[[NSUserDefaults standardUserDefaults]valueForKey:@"SpeakerListPageNo"];
    
    
    //Check whether a download is in progroess or not
    
    if([appdelegate.speakerDownloadInProgress isEqualToString:@"NO"] && [lastpageStatus isEqualToString:@"N"])
    {
        appdelegate.speakerListLastPage=lastpageStatus;
        appdelegate.speakerListPageNo=page;

        [helper dispatchSpeakerList];
        [_progessIndicator startAnimating];
    }
    
    //Hide the progress indicator when on completion of download
    if([lastpageStatus isEqualToString:@"Y"])
    {
        appdelegate.speakerListLastPage=@"Y";
        [_progessIndicator setHidden:YES];
        [_downloadProgress setHidden:YES];
    }
    else
    {
        appdelegate.speakerListLastPage=@"N";
    }
    
    [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self selector:@selector(checkProgressUpdate:) userInfo: nil repeats:YES];
    
    MenueBarView = [[MenueBar alloc]initWithFrame:CGRectMake(0, 20, 100, 748)];
    MenueBarView.SpeakerButton.backgroundColor = [helper LightBlueColour];
    [MenueBarView.CalendarButton addTarget:self action:@selector(CalendarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivitiesButton addTarget:self action:@selector(ActivityButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.HomeButton addTarget:self action:@selector(HomeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.AttendeeButton addTarget:self action:@selector(AttendeeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivityPlanButton addTarget:self action:@selector(ActivityPlanButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    MenueBarView.VersionNumber.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
    
    [self.view addSubview:MenueBarView];
    [self.view addSubview:TitleBarView];
    
    [self langsetuptranslations];
    
    if (![appdelegate.UserRolesArray containsObject:@"ACT_ADM"])
    {
        MenueBarView.ActivityPlanButton.hidden = YES;
    }
    //Role back access
    NSLog(@"Appdelegate userorles %@",appdelegate.UserRolesArray);
    if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] ||(![appdelegate.UserRolesArray containsObject:@"SPK_R"] && ![appdelegate.UserRolesArray containsObject:@"SPK_W"]) )
    {
        AddSpeakerButtonOutlet.hidden = YES;
    }
    if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] && [appdelegate.UserRolesArray containsObject:@"SPK_W"])
    {
        AddSpeakerButtonOutlet.hidden = NO;
    }
    
    speakerimagearr = [[NSMutableArray alloc]init];
    resumefilearre = [[NSMutableArray alloc]init];
    spkdetailview = [[Speaker_detail alloc] init];
    if([fromview isEqualToString:@"EventSpeaker"])
    {
        speakerlist = [helper query_alldata:@"Speaker"];
        NSLog(@"Speaker ids %@", [speakerlist valueForKey:@"id"]);
        speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.speakerid]];
    }
    else
    {
         speakerlist = [helper query_alldata:@"Speaker"];
    }
    
    
    ln_expandedRowIndex = -1;
    
    speaker_lbl.text = [NSString stringWithFormat:@"%@ (%lu)",[helper gettranslation:@"LBL_236"],(unsigned long)speakerlist.count];
    
    [SpeakerSearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.SpeakerSearchBar valueForKey:@"_searchField"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    
    if ([userarr count]>0)
    {
        _Username_lbl.text = [NSString stringWithFormat:@"%@ %@",[userarr[0] valueForKey:@"firstname"],[userarr[0] valueForKey:@"lastname"]];
    }
    
    SpeakerSearchBar.delegate = self;
    Speakerlistview.delaysContentTouches = false;
    
    //Add swipe gesture recogniser
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeRightAction:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    
    
    
    //speaker image array
    for (int i = 0; i < [speakerlist count]; i++)
    {
        if ([[speakerlist[i] valueForKey:@"cvid"] length]>0)
        {
            if([speakerlist[i] valueForKey:@"resume_location_url"])
            {
                NSData *dt = [NSData dataWithContentsOfFile:[speakerlist[i] valueForKey:@"resume_location_url"]];
                if (!dt)
                {
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    [dict setValue:[speakerlist[i] valueForKey:@"cvid"] forKey:@"resumeurl"];
                    [dict setValue:[speakerlist[i] valueForKey:@"id"] forKey:@"resumeid"];
                    [dict setValue:[speakerlist[i] valueForKey:@"cvFileType"] forKey:@"cvFileType"];
                    [resumefilearre addObject:dict];
                }
            }
            else
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:[speakerlist[i] valueForKey:@"cvid"] forKey:@"resumeurl"];
                [dict setValue:[speakerlist[i] valueForKey:@"id"] forKey:@"resumeid"];
                [dict setValue:[speakerlist[i] valueForKey:@"cvFileType"] forKey:@"cvFileType"];
                [resumefilearre addObject:dict];
            }
        }
    }
    //resume image arr
    for (int i =0; i < [speakerlist count]; i++)
    {
        if ([[speakerlist[i] valueForKey:@"imageicon"] length]>0)
        {
            if([speakerlist[i] valueForKey:@"location_url"])
            {
                //check for file exist
                UIImage *speakerprofileimage = [UIImage imageWithContentsOfFile:[speakerlist[i] valueForKey:@"location_url"]];
                if(!speakerprofileimage)
                {
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    [dict setValue:[speakerlist[i] valueForKey:@"imageicon"] forKey:@"imageurl"];
                    [dict setValue:[speakerlist[i] valueForKey:@"id"] forKey:@"imageid"];
                    [speakerimagearr addObject:dict];
                }
            }
            else
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:[speakerlist[i] valueForKey:@"imageicon"] forKey:@"imageurl"];
                [dict setValue:[speakerlist[i] valueForKey:@"id"] forKey:@"imageid"];
                [speakerimagearr addObject:dict];
                
            }
        }
    }
    

    /*NSString *GalleryFDRdownload = [helper query_data:@"name" inputdata:@"GalleryFDRdownload" entityname:@"S_Config_Table"];
    if ([GalleryFDRdownload isEqualToString:@"NO"])
    {
        for (int i =0; i < [speakerlist count]; i++)
        {
            if ([[speakerlist[i] valueForKey:@"imageicon"] length]>0)
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:[speakerlist[i] valueForKey:@"imageicon"] forKey:@"imageurl"];
                [dict setValue:[speakerlist[i] valueForKey:@"id"] forKey:@"imageid"];
                [speakerimagearr addObject:dict];

            }
        }
        for (int i = 0; i < [speakerlist count]; i++)
        {
            if ([[speakerlist[i] valueForKey:@"cvid"] length]>0)
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:[speakerlist[i] valueForKey:@"cvid"] forKey:@"resumeurl"];
                [dict setValue:[speakerlist[i] valueForKey:@"id"] forKey:@"resumeid"];
                [dict setValue:[speakerlist[i] valueForKey:@"cvFileType"] forKey:@"cvFileType"];
                [resumefilearre addObject:dict];

            }
        }
    }*/

}

-(void)langsetuptranslations
{
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [MenueBarView.HomeButton setTitle:[helper gettranslation:@"LBL_143"] forState:UIControlStateNormal];
    [MenueBarView.ActivitiesButton setTitle:[helper gettranslation:@"LBL_003"] forState:UIControlStateNormal];
    [MenueBarView.CalendarButton setTitle:[helper gettranslation:@"LBL_060"] forState:UIControlStateNormal];
    [MenueBarView.SpeakerButton setTitle:[helper gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [MenueBarView.AttendeeButton setTitle:[helper gettranslation:@"LBL_054"] forState:UIControlStateNormal];
    [MenueBarView.ActivityPlanButton setTitle:[helper gettranslation:@"LBL_007"] forState:UIControlStateNormal];
    
    
    
}


#pragma mark swipe right method
-(IBAction)SwipeRightAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
   // [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)refreshspeakerlist
{
    ln_expandedRowIndex = -1;
    helper = [[HelperClass alloc]init];
    if([fromview isEqualToString:@"EventSpeaker"])
    {
        speakerlist = [helper query_alldata:@"Speaker"];
        NSLog(@"Speaker ids %@", [speakerlist valueForKey:@"id"]);
        speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.speakerid]];
    }
    else
    {
        speakerlist = [helper query_alldata:@"Speaker"];
    }
    [Speakerlistview reloadData];
   
}
-(void)viewDidAppear:(BOOL)animated
{
   // NSLog(@"Speaker image array and resume array count is %uld %uld",[speakerimagearr count],[resumefilearre count]);
    if([appdelegate.fdrstr isEqualToString:@"N"])
    {
        if([speakerimagearr count]>0 || [resumefilearre count]>0)
        {
       
           // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                      // ^{
                           appdelegate.currentscreen = @"Speaker";
                           [helper imagesDownloader:speakerimagearr galleryimagearr:nil resumeimages:resumefilearre];
                      // });
        }
    }
    if (RefreshTableview == 1)
    {
        Speakerlistview.delegate = self;
        Speakerlistview.dataSource = self;
        [Speakerlistview reloadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark menu button action
- (IBAction)CalendarButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventSpeakertoCalendarview" sender:self];
}

- (IBAction)HomeButtonAction:(id)sender
{
    if([fromview isEqualToString:@"EventSpeaker"])
    {
        [self performSegueWithIdentifier:@"UnwindFromSpeakerToEventSpeaker" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:@"backtoHomescreen" sender:self];
    }
}

- (IBAction)ActivityButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"EventSpeakertoActivitiesview" sender:self];
}

- (IBAction)AttendeeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"SpeakerToAttendee" sender:self];
}
-(IBAction)ActivityPlanButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"SpeakerToActivityPlan" sender:self];
}


- (IBAction)Setting_btn:(id)sender
{
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(count > searchText.length)
    {
        speakerlist = [helper query_alldata:@"Speaker"];
    }
    if(searchText.length == 0)
    {
        speakerlist = [helper query_alldata:@"Speaker"];
      
        [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
    }
    else
    {
        speakerlist = [helper query_alldata:@"Speaker"];
        count = searchText.length;
        NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.emailaddress contains[c] %@) OR (SELF.contactno contains[c] %@) OR (SELF.chargeperhour contains[c] %@) OR (SELF.designation contains[c] %@) OR (SELF.department contains[c] %@)OR (SELF.company contains[c] %@) OR (SELF.speciality contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText,searchText,searchText,searchText,searchText,searchText];
        //speciality
        
        filtereventarr = [NSMutableArray arrayWithArray:[speakerlist filteredArrayUsingPredicate:predicate]];
        speakerlist = [filtereventarr copy];
        
    }
    ln_expandedRowIndex=-1;
    [Speakerlistview reloadData];
    
    
    
}


//------------------Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 200;
    }
    return 250;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [speakerlist count] + (ln_expandedRowIndex != -1 ? 1 : 0);
    //return [speakerlist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSInteger row = [indexPath row];
    NSInteger dataIndex = [self dataIndexForRowIndex:row];

    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
    
    if (!ln_expandedCell)
    {
        SOCustomCell *customcell;
        NSString *cellIdentifier = @"customcell";
        
        NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
//        if (indexPath.row >= speakerlist.count)
//        {
//            [SelectedArray insertObject:speakerlist[indexPath.row - 1] atIndex:0];
//        }
//        else
//        {
            [SelectedArray insertObject:speakerlist[dataIndex] atIndex:0]; ;
       // }
        
        customcell = [self.Speakerlistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
      //  customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        
        UILabel *company_ulbl = (UILabel *) [contentView viewWithTag:1901];
        UILabel *department_ulbl = (UILabel *) [contentView viewWithTag:1902];
        UILabel *designation_ulbl = (UILabel *) [contentView viewWithTag:1903];
        UILabel *costperhr_ulbl = (UILabel *) [contentView viewWithTag:1904];
        UILabel *emailaddr_ulbl = (UILabel *) [contentView viewWithTag:1905];
        UILabel *contactno_ulbl = (UILabel *) [contentView viewWithTag:1906];
        UILabel *address_ulbl = (UILabel *) [contentView viewWithTag:1907];
        UILabel *internal_ulbl = (UILabel *) [contentView viewWithTag:1908];
        UILabel *passportdtl_ulbl = (UILabel *) [contentView viewWithTag:1909];
        UILabel *passportno_ulbl = (UILabel *) [contentView viewWithTag:1910];
        UILabel *validfrm_ulbl = (UILabel *) [contentView viewWithTag:1911];
        UILabel *validtill_ulbl = (UILabel *) [contentView viewWithTag:1912];
        
        
        
        company_ulbl.text = [helper gettranslation:@"LBL_067"];
        department_ulbl.text = [helper gettranslation:@"LBL_092"];
        designation_ulbl.text = [helper gettranslation:@"LBL_095"];
        costperhr_ulbl.text = [helper gettranslation:@"LBL_071"];
        emailaddr_ulbl.text = [helper gettranslation:@"LBL_119"];
        contactno_ulbl.text = [helper gettranslation:@"LBL_070"];
        address_ulbl.text = [helper gettranslation:@"LBL_030"];
        internal_ulbl.text = [helper gettranslation:@"LBL_149"];
        passportdtl_ulbl.text = [helper gettranslation:@"LBL_193"];
        passportno_ulbl.text = [helper gettranslation:@"LBL_194"];
        validfrm_ulbl.text = [helper gettranslation:@"LBL_274"];
        validtill_ulbl.text = [helper gettranslation:@"LBL_275"];
        
        
        
        UILabel *title = (UILabel *) [contentView viewWithTag:1];
       
        UILabel *status = (UILabel *) [contentView viewWithTag:2];
        UILabel *department = (UILabel *) [contentView viewWithTag:50];
        UILabel *designation = (UILabel *) [contentView viewWithTag:6];
        UIImageView *speakerimage = (UIImageView *)[contentView viewWithTag:14];
        UILabel *company = (UILabel *)[contentView viewWithTag:17];
        UILabel *emailaddress = (UILabel *)[contentView viewWithTag:8];
        UILabel *contactnumber = (UILabel *)[contentView viewWithTag:9];
        UILabel *address = (UILabel *)[contentView viewWithTag:10];
        UILabel *passportnumber = (UILabel *)[contentView viewWithTag:11];
        UILabel *validform = (UILabel *)[contentView viewWithTag:12];
        UILabel *validto = (UILabel *)[contentView viewWithTag:13];
        UILabel *costperhour = (UILabel *) [contentView viewWithTag:5];
        UIButton *resume_btn = (UIButton *)[contentView viewWithTag:60];
        UISwitch *internalbtn_switch = (UISwitch *)[contentView viewWithTag:90];
        UIButton *attachmentBtn=(UIButton *)[contentView viewWithTag:20];
        
        [attachmentBtn addTarget:self action:@selector(viewAttachment:) forControlEvents:UIControlEventTouchUpInside];
        [resume_btn addTarget:self action:@selector(viewresumebtnaction:) forControlEvents:UIControlEventTouchUpInside];
        edit_btn = (UIButton *) [contentView viewWithTag:3];
        del_btn = (UIButton *) [contentView viewWithTag:4];
        //edit_btn.tag = indexPath.row;
        
        NSString *cvname = [SelectedArray[0] valueForKey:@"cvFileName"];
        if ([cvname length]>0)
        {
            cvname = [NSString stringWithFormat:@"%@%@",[[cvname substringToIndex:1]uppercaseString],[[cvname substringFromIndex:1]lowercaseString] ];
            [resume_btn setTitle:cvname forState:UIControlStateNormal];
            [resume_btn setUserInteractionEnabled:YES];
        }
        else
        {
             [resume_btn setTitle:@"" forState:UIControlStateNormal];
            [resume_btn setUserInteractionEnabled:NO];
        }
        NSLog(@"resume name is %@",[SelectedArray[0] valueForKey:@"cvFileName"]);
         
        
        [edit_btn addTarget:self action:@selector(editspeakerClicked:) forControlEvents:UIControlEventTouchUpInside];
       // [del_btn addTarget:self action:@selector(deletespeakerClicked:) forControlEvents:UIControlEventTouchUpInside];
       
        NSLog(@"image local url is %@",[SelectedArray[0] valueForKey:@"location_url"]);
        
        if([SelectedArray[0] valueForKey:@"location_url"])
        {
            //check for file exist
            UIImage *speakerprofileimage = [UIImage imageWithContentsOfFile:[SelectedArray[0] valueForKey:@"location_url"]];
            if(speakerprofileimage)
            {
               speakerimage.image = speakerprofileimage;
            }
            else
            {
                speakerimage.image = [UIImage imageNamed:@"User.png"];
            }
        }
        else
        {
            speakerimage.image = [UIImage imageNamed:@"User.png"];
        }

        speakerimage.layer.cornerRadius = speakerimage.image.size.width/2;
        internalbtn_switch.enabled = NO;
        internalbtn_switch.transform = CGAffineTransformMakeScale(0.60, 0.60);
       // internalbtn_switch.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
        internalbtn_switch.onTintColor = [helper DarkBlueColour];
        NSString *spkrspec=[SelectedArray[0] valueForKey:@"speciality"];
        
        
        if(spkrspec.length>0)
        {
            [title setText:[NSString stringWithFormat:@"%@ %@ %@ - %@",[SelectedArray[0] valueForKey:@"salutation"],[SelectedArray[0] valueForKey:@"firstname"],[SelectedArray[0] valueForKey:@"lastname"],[SelectedArray[0] valueForKey:@"speciality"]]];
        }
        else
        {
             [title setText:[NSString stringWithFormat:@"%@ %@ %@ ",[SelectedArray[0] valueForKey:@"salutation"],[SelectedArray[0] valueForKey:@"firstname"],[SelectedArray[0] valueForKey:@"lastname"]]];
        }
        [emailaddress setText:[SelectedArray[0] valueForKey:@"emailaddress"]];
        [contactnumber setText:[SelectedArray[0] valueForKey:@"contactno"]];
        [status setText:[SelectedArray[0] valueForKey:@"status"]];
        NSLog(@"Status is %@",status.text);
        [designation setText:[SelectedArray[0] valueForKey:@"designation"]];
      
//        costperhour.text = [helper stringtocurrency:[SelectedArray[0] valueForKey:@"totalAmount"]];
        costperhour.text =[NSString stringWithFormat:@"%@ %@",currency,[SelectedArray[0] valueForKey:@"totalAmount"]];
        
        passportnumber.text = [SelectedArray[0] valueForKey:@"passportnumber"];
        
        
        validform.text = [helper formatingdate:[SelectedArray[0] valueForKey:@"passportvalidfrom"] datetime:@"Passportdate"];
        validto.text = [helper formatingdate:[SelectedArray[0] valueForKey:@"passportvalidto"] datetime:@"Passportdate"];

        
        NSString *addressstring;
        
        
        if ([[SelectedArray[0] valueForKey:@"streetaddress1"] length] >0)
        {
            addressstring = [NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"streetaddress1"]];
        }
        
        if ([[SelectedArray[0] valueForKey:@"streetaddress2"] length] >0)
        {
            if([addressstring length]>0)
                addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[SelectedArray[0] valueForKey:@"streetaddress2"]];
            else
                addressstring = [NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"streetaddress2"]];
        }
        
        if ([[SelectedArray[0] valueForKey:@"city"] length] >0)
        {
             if([addressstring length]>0)
                 addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[SelectedArray[0] valueForKey:@"city"]];
            else
                addressstring = [NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"city"]];

        }
         if ([[SelectedArray[0] valueForKey:@"state"] length] >0)
        {
            if([addressstring length]>0)
                addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[SelectedArray[0] valueForKey:@"state"]];
            else
                addressstring = [NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"state"]];

        }
         if ([[SelectedArray[0] valueForKey:@"country"] length] >0)
        {
            if([addressstring length]>0)
            addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[SelectedArray[0] valueForKey:@"country"]];
            else
            addressstring = [NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"country"]];
        }
        
        
        address.text = addressstring;
        

        department.text = [SelectedArray[0] valueForKey:@"department"];
        
      //  NSLog(@"[speakerlist[0] valueForKey: %@",[speakerlist[0] valueForKey:@"internalFlag"]);
        
        NSLog(@"Internal flag in speaker list is %@",[speakerlist[0] valueForKey:@"internalFlag"]);
        if([[SelectedArray[0] valueForKey:@"internalFlag"] isEqualToString:@"Y"])
        {
            [internalbtn_switch setOn:YES];
            
        }
        else
        {
            [internalbtn_switch setOn:NO];
        }
        
        
        [company setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"company"]]];
  
    
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        [customcell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] || (![appdelegate.UserRolesArray containsObject:@"SPK_R"] && ![appdelegate.UserRolesArray containsObject:@"SPK_W"]))
        {
            [edit_btn setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
        }
        if ([appdelegate.UserRolesArray containsObject:@"SPK_W"])
        {
            [edit_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
        if ([appdelegate.UserRolesArray containsObject:@"SPK_R"] && [appdelegate.UserRolesArray containsObject:@"SPK_W"])
        {
            [edit_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        }
       
        
        
        return customcell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        
        [cell.contentView addSubview:spkdetailview];
        
        [self setupspeakerdetails:[speakerlist[indexPath.row-1] valueForKey:@"id"] backcolor:cell.backgroundColor];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;

}

//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
//    /* Create custom view to display section header... */
//    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
//    
//    [view addSubview:spkdetailview];
//    return view;
//
//}
//
//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
//{
//    
//}
//---------------------Selection Functions start here

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSInteger row = [indexPath row];
    BOOL preventReopen = NO;
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        //cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
         cell.contentView.backgroundColor = [UIColor colorWithRed:242/255.f green:246/255.f blue:250/255.f alpha: 1] ;
    }
    [tableView endUpdates];
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    //    cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
}


-(void) setupspeakerdetails:(NSString *)speaker_id backcolor:(UIColor *)backcolor
{
    NSArray *spklist = [helper query_alldata:@"Speaker"];
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"id like %@",speaker_id];
    
    spklist = [spklist filteredArrayUsingPredicate:aPredicate];
    
    /*[self.spkdetailview.speaker_email setText:@"Email@soucesystem.com"];
    [self.spkdetailview.speaker_contactno setText:@"+1 676 345 2990"];
    [self.spkdetailview.speaker_intsource setText:@"Integration Source"];
    [self.spkdetailview.speaker_updatedby setText:[spklist[0] valueForKey:@"updatedby"]];
   
    UIImage *image = [UIImage imageNamed:@""];
//    [self.spkdetailview.speaker_image setImage:[UIImage new]];

     [self.spkdetailview.mainview setBackgroundColor:backcolor];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSLog(@"updated %@",[spklist[0] valueForKey:@"updated"]);
    
//    NSDate *udate = [dateFormatter dateFromString:[spklist[0] valueForKey:@"updated"]];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    
    [self.spkdetailview.speaker_updated setText:[dateFormatter stringFromDate:[NSDate date]]];*/
    
    [self.spkdetailview.speaker_executivesum setText:[helper gettranslation:@"LBL_134"]];
    
    if([spklist count]>0)
    {
        self.spkdetailview.executivesummery_textview.text = [spklist[0] valueForKey:@"summary"];
    }
 
}


-(IBAction)editspeakerClicked:(id)sender
{
    ln_expandedRowIndex=-1;
    [Speakerlistview reloadData];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.Speakerlistview];
    NSIndexPath *indexPath = [self.Speakerlistview indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        sel_speakerid = [speakerlist[indexPath.row] valueForKey:@"id"];
    }
    
   [self performSegueWithIdentifier:@"AddSpeaker" sender:self];
}

/*-(IBAction)deletespeakerClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.Speakerlistview];
    NSIndexPath *indexPath = [self.Speakerlistview indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        sel_speakerid = [speakerlist[indexPath.row] valueForKey:@"id"];
    }
    
//    [self performSegueWithIdentifier:@"ActivitylisttoUpdateActivity" sender:self];
}*/

//-----------------------------------------------------------------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AddSpeaker"])
    {
        CreateSpeakerViewController *controller =   segue.destinationViewController;
        
        controller.speakerid = sel_speakerid;
        //controller.activity_id = sel_activityid;
        //controller.cancelSegue = @"unwindToEventActivitiesView";
    }
    else if ([[segue identifier] isEqualToString:@"EventSpeakerToDisplayAttachment"])
    {
        DisplayAttachment *Controller = segue.destinationViewController;
        Controller.cancelsegue = @"UnwindFromEventSpeakerToAttachment";
        Controller.webviewurl =   resumeurl;
         Controller.documentname = fileextension;
        
    }
    else if ([[segue identifier] isEqualToString:@"AttachmentDetails"])
    {
        AttachmentViewController *Controller=segue.destinationViewController;
        Controller.senderView=@"Speaker";
        Controller.speakerID=sel_speakerid;
        Controller.speakerName=speakerName;
        
    }
    
}
/*-(void)reloadspeakerlist
{
    speakerlist = [helper query_alldata:@"Speaker"];
    [Speakerlistview reloadData];
}*/

- (IBAction)unwindToEventSpeakerView:(UIStoryboardSegue *)segue;
{
   // [self refreshspeakerlist];
    
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"YourNotificationName"
                                                       //object:nil];

    /*if([fromview isEqualToString:@"EventSpeaker"])
    {
        speakerlist = [helper query_alldata:@"Speaker"];
        NSLog(@"Speaker ids %@", [speakerlist valueForKey:@"id"]);
        speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.speakerid]];
    }
    else
    {
        speakerlist = [helper query_alldata:@"Speaker"];
    }
    [Speakerlistview reloadData];*/
   /* ln_expandedRowIndex = -1;
    if([fromview isEqualToString:@"EventSpeaker"])
    {
        speakerlist = [helper query_alldata:@"Speaker"];
        NSLog(@"Speaker ids %@", [speakerlist valueForKey:@"id"]);
        speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.speakerid]];
    }
    else
    {
        speakerlist = [helper query_alldata:@"Speaker"];
    }
    Speakerlistview.delegate = self;
    Speakerlistview.dataSource = self;
    
    [Speakerlistview reloadData];*/
   
    [self refreshspeakerlist];

    
    
}
- (IBAction)viewAttachment:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.Speakerlistview];
    NSIndexPath *indexPath = [self.Speakerlistview indexPathForRowAtPoint:buttonPosition];
    
    sel_speakerid = [speakerlist[indexPath.row] valueForKey:@"id"];
    speakerName=[NSString stringWithFormat:@"%@ %@",[speakerlist[indexPath.row] valueForKey:@"firstname"],[speakerlist[indexPath.row] valueForKey:@"lastname"]];
    [self performSegueWithIdentifier:@"AttachmentDetails" sender:self];
}
- (IBAction)addspeaker_btn:(id)sender
{
    
    if([appdelegate.speakerListLastPage isEqualToString:@"Y"])
    {
        sel_speakerid = nil;
        [self performSegueWithIdentifier:@"AddSpeaker" sender:self];
    }
    else
    {
         [helper showalert:[helper gettranslation:@"LBL_590"] message:[NSString stringWithFormat:@"%@! %@",[helper gettranslation:@"LBL_808"],[helper gettranslation:@"LBL_628"]] action:[helper gettranslation:@"LBL_462"]];
    }
    
}
- (IBAction)viewresumebtnaction:(UIButton *)sender
{
   // NSLog(@"resume button tag is %ld",sender.tag);
    
    [self refreshspeakerlist];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.Speakerlistview];
    NSIndexPath *indexPath = [self.Speakerlistview indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
       // resumeurl = [speakerlist[indexPath.row] valueForKey:@"cvid"];
        NSLog(@"indexpath:%lu",indexPath.row);
        resumeurl = [speakerlist[indexPath.row] valueForKey:@"resume_location_url"];
        
        fileextension = [speakerlist[indexPath.row] valueForKey:@"cvFileName"];
    
    }
   /* NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileextension]; //Add the file name
    NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:resumeurl]];
       if ( curlData )
    {
        [curlData writeToFile:filePath atomically:YES];
    }
    else
    {
        //[helpervar printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download Attachment: %@",locfilename]] ;
        
        NSLog(@"Unable to download Attachment: %@",resumeurl);
    }*/
    
    if ([resumeurl length]>0)
    {
        [self performSegueWithIdentifier:@"EventSpeakerToDisplayAttachment" sender:self];
    }
    else
    {
        [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_622"] withDuaration:2.0];
    }
}

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}
-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}

-(void)checkProgressUpdate:(NSTimer *)timer
{
    if([appdelegate.speakerListLastPage isEqualToString:@"Y"])
    {
        [_progessIndicator setHidden:YES];
        [_downloadProgress setHidden:YES];
        [speakerListUpdateTimer invalidate];
    }
}
- (NSInteger)dataIndexForRowIndex:(NSInteger)row
{
    if (ln_expandedRowIndex != -1 && ln_expandedRowIndex <= row)
    {
        if (ln_expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}
@end
