//
//  EventSynclogsViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/14/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "SOCustomCell.h"
#import "HelpViewController.h"

@interface EventSynclogsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic) Reachability* reachability;

- (IBAction)menu_calendar_btn:(id)sender;
- (IBAction)menu_home_btn:(id)sender;
- (IBAction)menu_speaker_btn:(id)sender;
- (IBAction)menu_activity_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *Txnlogslistview;

- (IBAction)Setting_btn:(id)sender;

@property(strong,nonatomic)TitleBar *TitleBarView;

- (IBAction)Txnlogs_segmentcontrol:(id)sender;

@property (strong, nonatomic) IBOutlet UISegmentedControl *Txnlogs_segmentcontrol;

@property (strong, nonatomic) IBOutlet UISearchBar *TxnlogsSearchBar;

@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;
-(void)reloadtransactionqueue;
@end
