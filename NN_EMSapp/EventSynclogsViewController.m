//
//  EventSynclogsViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/14/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "EventSynclogsViewController.h"

@interface EventSynclogsViewController ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSPredicate *logspredicate,*allpredicate;
    NSUInteger count;
    NSPredicate *queuedpredicate;
    NSPredicate *failedpredicate;
    HelpViewController *HelpView;

   // NSDateFormatter *formatter;
    
}

@end

@implementation EventSynclogsViewController
{
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    
    SLKeyChainStore *keychain;
    MSClient *client;
    NSArray *transactionlogslist;
    
    NSString *appID;
    NSString *custID;
    NSString *AESPrivateKey;
    NSSortDescriptor *sortdescriptor;
    NSString *sel_txlogid;
}

@synthesize helper,Txnlogslistview,TxnlogsSearchBar,TitleBarView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
  
      keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    AESPrivateKey =[helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
    sortdescriptor = [[NSSortDescriptor alloc]initWithKey:@"transaction_date" ascending:NO];
    transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];
    
    [TxnlogsSearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.TxnlogsSearchBar valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    
    if ([userarr count]>0)
    {
        _Username_lbl.text = [NSString stringWithFormat:@"%@ %@",[userarr[0] valueForKey:@"firstname"],[userarr[0] valueForKey:@"lastname"]];
    }
    
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = YES;
    TitleBarView.UsernameLbl.text = [helper getUsername];
    [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }

    [self.view addSubview:TitleBarView];
    
    [_Txnlogs_segmentcontrol setSelectedSegmentIndex:0];
    [_Txnlogs_segmentcontrol sendActionsForControlEvents:UIControlEventValueChanged];
    
    TxnlogsSearchBar.delegate = self;
    Txnlogslistview.delaysContentTouches = false;
    
    //Add swipe gesture recogniser
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menu_home_btn:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadtransactionqueue:)
                                                 name:@"synclogsnotification"
                                               object:nil];
    
   // formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"dd MMM yyyy hh:mm a"];;
    //[formatter setDateFormat:@"yyyyMMddHHmmss"];
    //        [formatter setDateStyle:NSDateFormatterMediumStyle];
    //NSString *dateToday = [formatter stringFromDate:[NSDate date]];

}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menu_calendar_btn:(id)sender
{
    [self performSegueWithIdentifier:@"EventlogstoCalendarview" sender:self];
}

- (IBAction)menu_home_btn:(id)sender
{
    [self performSegueWithIdentifier:@"backtoHomescreen" sender:self];
}

- (IBAction)menu_speaker_btn:(id)sender
{
    [self performSegueWithIdentifier:@"EventlogstoSpeakerview" sender:self];
}

- (IBAction)menu_activity_btn:(id)sender
{
    [self performSegueWithIdentifier:@"EventlogstoActivitiesview" sender:self];
}
- (IBAction)Setting_btn:(id)sender
{
    
}


- (IBAction)Txnlogs_segmentcontrol:(id)sender
{
  
      keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    failedpredicate = [NSPredicate predicateWithFormat:@"SELF.transaction_status like %@",@"Failed"];
    queuedpredicate = [NSPredicate predicateWithFormat:@"SELF.transaction_status like %@",@"Queued"];
    
    switch (self.Txnlogs_segmentcontrol.selectedSegmentIndex)
    {
        case 0:
            transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
            transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];
            
//          transactionlogslist = [Eventlist filteredArrayUsingPredicate:mypredicate];
            [Txnlogslistview reloadData];
            
            break;
            
        case 1:
            transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
             transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];
            transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:queuedpredicate];
            [Txnlogslistview reloadData];
            
            break;
            
        case 2:
            transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
            transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];
            transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:failedpredicate];
            [Txnlogslistview reloadData];
            
            break;
        default:
            break;
    }
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(count > searchText.length)
    {
        transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
        transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];

        if(self.Txnlogs_segmentcontrol.selectedSegmentIndex == 0)
        {
        }
        else if (self.Txnlogs_segmentcontrol.selectedSegmentIndex == 1)
        {
           transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:queuedpredicate];
        }
        else if (self.Txnlogs_segmentcontrol.selectedSegmentIndex == 2)
        {
           transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:failedpredicate];
        }
    }
    if(searchText.length == 0)
    {
        transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
        transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];

        if(self.Txnlogs_segmentcontrol.selectedSegmentIndex == 0)
        {
        }
        else if (self.Txnlogs_segmentcontrol.selectedSegmentIndex == 1)
        {
            transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:queuedpredicate];
        }
        else if (self.Txnlogs_segmentcontrol.selectedSegmentIndex == 2)
        {
            transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:failedpredicate];
        }
        [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
    }
    else
    {
         transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
         transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];
        if(self.Txnlogs_segmentcontrol.selectedSegmentIndex == 0)
        {
        }
        else if (self.Txnlogs_segmentcontrol.selectedSegmentIndex == 1)
        {
            transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:queuedpredicate];
        }
        else if (self.Txnlogs_segmentcontrol.selectedSegmentIndex == 2)
        {
            transactionlogslist = [transactionlogslist filteredArrayUsingPredicate:failedpredicate];
        }
        
        NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.transaction_date contains[c] %@) OR (SELF.transaction_type contains[c] %@) OR (SELF.transaction_status contains[c] %@) OR (SELF.transaction_resp_message contains[c] %@) OR (SELF.transaction_entityname contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
        filtereventarr = [NSMutableArray arrayWithArray:[transactionlogslist filteredArrayUsingPredicate:predicate]];
        transactionlogslist = [filtereventarr copy];
    }
    [Txnlogslistview reloadData];
    
}

//------------------Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return transactionlogslist.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    customcell = [self.Txnlogslistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    // customcell.backgroundColor = [UIColor colorWithRed:58/255.f green:87/255.f blue:96/255.f alpha: 1];
    
    UILabel *txn_date = (UILabel *) [contentView viewWithTag:1];
    UILabel *txn_type = (UILabel *) [contentView viewWithTag:2];
    UILabel *txn_status = (UILabel *) [contentView viewWithTag:3];
    UILabel *txn_description = (UILabel *) [contentView viewWithTag:4];
    UILabel *txn_entityname = (UILabel *) [contentView viewWithTag:6];

    UIButton *txn_retry_btn = (UIButton *) [contentView viewWithTag:5];
//    UIButton *txn_send_btn = (UIButton *) [contentView viewWithTag:6];
    
    //[txn_date setText:[transactionlogslist[indexPath.row] valueForKey:@"transaction_date"]];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"dd MMM yyyy hh:mm a"];;
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    //        [formatter setDateStyle:NSDateFormatterMediumStyle];
   
    NSDate *datefromstring = [formatter dateFromString:[transactionlogslist[indexPath.row] valueForKey:@"transaction_date"]];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mm:ss"];
    NSString *datestr = [formatter stringFromDate:datefromstring];
    txn_date.text = datestr;
    
    
    [txn_type setText:[transactionlogslist[indexPath.row] valueForKey:@"transaction_type"]];
    [txn_status setText:[transactionlogslist[indexPath.row] valueForKey:@"transaction_status"]];
    [txn_description setText:[transactionlogslist[indexPath.row] valueForKey:@"transaction_resp_message"]];
    [txn_entityname setText:[transactionlogslist[indexPath.row] valueForKey:@"transaction_entityname"]];
    
    [txn_retry_btn addTarget:self action:@selector(retryTransactionClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [txn_send_btn addTarget:self action:@selector(sendTransactionClicked:) forControlEvents:UIControlEventTouchUpInside];
    txn_retry_btn.hidden = YES;
    
    if ([txn_status.text isEqualToString:@"Completed"])
    {
       txn_status.textColor =  [[UIColor alloc]initWithRed:59/255.0 green:111/255.0 blue:11/255.0 alpha:1];
    }
    else if ([txn_status.text isEqualToString:@"Failed"])
    {
        txn_retry_btn.hidden = NO;
        txn_status.textColor =  [[UIColor alloc]initWithRed:236/255.0 green:63/255.0 blue:11/255.0 alpha:1];
    }
    if ([txn_status.text isEqualToString:@"Submitted"])
    {
        txn_status.textColor =  [[UIColor alloc]initWithRed:97/255.0 green:101/255.0 blue:255/255.0 alpha:1];
    }
    if ([txn_status.text isEqualToString:@"Open"])
    {
        txn_status.textColor =  [[UIColor alloc]initWithRed:255/255.0 green:194/255.0 blue:62/255.0 alpha:1];
    }
    return customcell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}
//---------------------Selection Functions start here

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
}

//-----------------------------------------------------------------------------------------------------------------
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ActivitylisttoUpdateActivity"])
    {
        updateactivity_ViewController *controller =   segue.destinationViewController;
        
        controller.senderView = @"ActivityDetailView";
        controller.activity_id = sel_activityid;
        controller.cancelSegue = @"unwindToEventActivitiesView";
    }
    
}
*/
-(void)reloadtransactionqueue:(NSNotification *)notification
{
    transactionlogslist = [helper query_alldata:@"Transaction_Queue"];
   // NSSortDescriptor *sortdescriptor = [[NSSortDescriptor alloc]initWithKey:@"transaction_date" ascending:NO];
    transactionlogslist = [transactionlogslist sortedArrayUsingDescriptors:@[sortdescriptor]];
    [self.Txnlogslistview reloadData];
}
- (IBAction)unwindToEventSynclogsView:(UIStoryboardSegue *)segue;
{
}

-(IBAction)retryTransactionClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.Txnlogslistview];
    NSIndexPath *indexPath = [self.Txnlogslistview indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        sel_txlogid = [transactionlogslist[indexPath.row] valueForKey:@"transaction_id"];
        NSLog(@"Transaction Logs: %@",sel_txlogid);
    }
    
}

-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helper gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}

@end
