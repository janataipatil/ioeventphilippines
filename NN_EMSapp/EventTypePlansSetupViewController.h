//
//  EventTypePlansSetupViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 23/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"

@interface EventTypePlansSetupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *EventTypeTextField;
@property (strong, nonatomic) IBOutlet UIButton *EventTypePlanButtonOutlet;
@property (strong, nonatomic) IBOutlet UITextField *EventSubTypeTextField;

@property (strong, nonatomic) IBOutlet UIView *DropDownView;
@property (strong, nonatomic) IBOutlet UITableView *LOVTableView;
@property (strong, nonatomic) IBOutlet UILabel *LOVTableViewHeaderlbl;

@property (strong, nonatomic) IBOutlet UITableView *PlanNameTableView;
@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UIView *PlanNameTableviewHeader;

- (IBAction)AddActivityPlanButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *eventtypeplan_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventsubtypeplan_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *title_ulbl;


@end
