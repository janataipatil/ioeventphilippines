//
//  EventTypePlansSetupViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 23/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "EventTypePlansSetupViewController.h"

@interface EventTypePlansSetupViewController ()
{
    NSString *TextfieldFlag;
    NSArray *LOVArr;
    NSArray *EventActivitiesPlanArr;
    AppDelegate *appdelegate;
    NSString *EventType;
    NSString *EventSubType;
}

@end

@implementation EventTypePlansSetupViewController
@synthesize EventTypeTextField,EventSubTypeTextField,helper,LOVTableView,DropDownView,LOVTableViewHeaderlbl,PlanNameTableView,EventTypePlanButtonOutlet,PlanNameTableviewHeader;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];

    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    LOVTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableView.layer.borderWidth = 1;
    PlanNameTableviewHeader.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    PlanNameTableviewHeader.layer.borderWidth = 1;
    [EventTypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [EventSubTypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
   // PlanNameTableView.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    //PlanNameTableView.layer.borderWidth = 1;

    
    _eventtypeplan_ulbl.text = [helper gettranslation:@"LBL_133"];
    EventTypeTextField.placeholder=[helper gettranslation:@"LBL_757"];
    
    
    _eventsubtypeplan_ulbl.text = [helper gettranslation:@"LBL_132"];
    EventSubTypeTextField.placeholder=[helper gettranslation:@"LBL_757"];
    _title_ulbl.text = [helper gettranslation:@"LBL_394"];
    
}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        LOVArr = [helper query_alldata:@"Event_LOV"];
        if ([TextfieldFlag isEqualToString:@"EventTypeTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];
        }
        else if ([TextfieldFlag  isEqualToString:@"EventSubTypeTextField"])
        {
             LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_SUBTYPE"]];
        }
    }
    else
    {
        LOVArr = [helper query_alldata:@"Event_LOV"];
        if ([TextfieldFlag isEqualToString:@"EventTypeTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];
        }
        else if ([TextfieldFlag  isEqualToString:@"EventSubTypeTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_SUBTYPE"]];
        }
        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
        LOVArr = [filteredpartArray copy];
        
    }
    [LOVTableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == EventTypeTextField)
    {
        TextfieldFlag = @"EventTypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];
    }
    else if (textField == EventSubTypeTextField)
    {
        TextfieldFlag = @"EventSubTypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.lovtype like %@) AND (SELF.parlov like %@) AND (SELF.parlic like %@)",@"EVENT_SUBTYPE",@"EVENT_TYPE",EventTypeTextField.text]];
        
       // NSLog(@"event subtype is %@",[LOVArr])
    }
    [LOVTableView reloadData];
    [self adjustHeightOfTableview:LOVTableView];
    DropDownView.hidden = NO;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == LOVTableView)
    {
        return [LOVArr count];
    }
    else if (tableView == PlanNameTableView)
    {
        return [EventActivitiesPlanArr count];
    }
    else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (tableView == PlanNameTableView )
    {
        UIView *contentView = customcell.contentView;
        UILabel *PlanNamelbl = (UILabel *)[contentView viewWithTag:1];
        UILabel *PlanDescriptionlbl = (UILabel *)[contentView viewWithTag:2];
        PlanNamelbl.text = [EventActivitiesPlanArr[indexPath.row] valueForKey:@"planName"];
        PlanDescriptionlbl.text = [EventActivitiesPlanArr[indexPath.row] valueForKey:@"planDesc"];
    }
    else if (tableView == LOVTableView)
    {
        UIView *contentView = customcell.contentView;
        UILabel *Lovlbl = (UILabel *)[contentView viewWithTag:1];
        Lovlbl.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([TextfieldFlag isEqualToString:@"EventTypeTextField"])
    {
        EventTypeTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [EventTypeTextField resignFirstResponder];
    }
    else if ([TextfieldFlag isEqualToString:@"EventSubTypeTextField"])
    {
        EventSubTypeTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [EventSubTypeTextField resignFirstResponder];
       EventType = [helper getLic:@"EVENT_TYPE" value:EventTypeTextField.text];
       EventSubType = [helper getLic:@"EVENT_SUBTYPE" value:EventSubTypeTextField.text];
        [self getEventTypePlans];
    }
   
    
    DropDownView.hidden = YES;
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == PlanNameTableView)
    {
        [helper serverdelete:@"EVENT_TYPE_PLAN" entityid:[EventActivitiesPlanArr[indexPath.row] valueForKey:@"iD"]];
        [helper delete_ActivitiesinPlan:@"EventTypePlans" planid:[EventActivitiesPlanArr[indexPath.row] valueForKey:@"planID"] activityid:[EventActivitiesPlanArr[indexPath.row] valueForKey:@"iD"]];
        [self refreshdata];
        
    }
}
#pragma mark get eventype
-(void)getEventTypePlans
{
    [helper showWaitCursor:[helper getLogintranslation:@"LBL_628"]];
    NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
    [PayloadDict setValue:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
    [PayloadDict setValue:EventType forKey:@"EventTypeLIC"];
    [PayloadDict setValue:EventSubType forKey:@"EventSubTypeLIC"];
    appdelegate.EventType = EventTypeTextField.text;
    appdelegate.EventSubType = EventSubTypeTextField.text;
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSGetEventTypePlans" pageno:@"" pagecount:@"" lastpage:@""];
    
   //online only functionality
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        NSArray *jsonArr = [jsonDict valueForKey:@"PlanList"];
        [helper delete_alldata:@"EventTypePlans"];
        [helper inserEventTypePlans:jsonArr];
        [self refreshdata];
        EventTypePlanButtonOutlet.hidden = NO;
        [helper removeWaitCursor];
    }

 
    
    
    
}
-(void)refreshdata
{
    EventActivitiesPlanArr = [helper query_alldata:@"EventTypePlans"];
    if ([EventActivitiesPlanArr count]>0)
    {
        PlanNameTableviewHeader.hidden = NO;
    }
    else
    {
        PlanNameTableviewHeader.hidden = YES;
        
    }
    [PlanNameTableView reloadData];
   
    
}
#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    
    CGFloat height = LOVTableView.contentSize.height;
    CGFloat maxHeight = LOVTableView.superview.frame.size.height - LOVTableView.frame.origin.y;
    if (height > maxHeight)
        height = maxHeight;
    [UIView animateWithDuration:0.0 animations:^{
        CGRect frame = LOVTableView.frame;
        
        
        if([TextfieldFlag isEqualToString:@"EventTypeTextField"])
        {
            frame.origin.x = 30;
            frame.origin.y = 112;
        }
        if([TextfieldFlag isEqualToString:@"EventSubTypeTextField"])
        {
            frame.origin.x = 401;
            frame.origin.y = 112;
        }
               DropDownView.frame = frame;
    }];
    
}
#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (DropDownView.hidden == NO)
    {
        DropDownView.hidden = YES;
        [EventTypeTextField resignFirstResponder];
        [EventSubTypeTextField resignFirstResponder];
    }
}
- (IBAction)AddActivityPlanButtonAction:(id)sender
{
    [self.parentViewController performSegueWithIdentifier:@"EventTypeToActivityPlan" sender:self];
}
#pragma mark unwind method
-(IBAction)unwindToEventTypeSetup:(UIStoryboardSegue *)sender
{
    if ([appdelegate.cancelclicked isEqualToString:@"NO"])
    {
        [self getEventTypePlans];
    }
}
@end
