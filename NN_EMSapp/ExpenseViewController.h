//
//  ExpenseViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 02/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "DisplayAttachment.h"
@interface ExpenseViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UISearchBar *ExpenseSearchBarOutlet;
@property (strong, nonatomic) IBOutlet UITableView *ExpenseTableView;
@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UIButton *AddExpenseButtonOutlet;

- (IBAction)AddExpenseButtonAction:(id)sender;


@property(strong,nonatomic) IBOutlet UILabel *username_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *exptype_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *paymenttype_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *expdate_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *expamount_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *expnotes_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *expreceipt_ulbl;

@end
