//
//  ExpenseViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 02/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ExpenseViewController.h"

@interface ExpenseViewController ()
{
    NSArray *ExpenseArr;
    AppDelegate *appdelegate;
    NSUInteger count;
    NSPredicate *EventIdPredicate;
    int RefreshData;
    NSString *ReceiptUrl;
    NSString *documentName;
    NSManagedObjectContext *context;
    NSString *currencyCode;
    NSString *currencySymbol;
}

@end

@implementation ExpenseViewController
@synthesize ExpenseSearchBarOutlet,ExpenseTableView,helper,AddExpenseButtonOutlet;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    context=[appdelegate managedObjectContext];
    
  appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    EventIdPredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
   
    [ExpenseSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [ExpenseSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
    NSLog(@"TRANS sEARCH:%@",[helper gettranslation:@"LBL_224"]);
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    
    currencyCode=[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:currencyCode];
    currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:currencyCode]];
 
    
    
       [self refreshData];
    NSLog(@"Event user role arr %@",appdelegate.EventUserRoleArray);
    if (![appdelegate.EventUserRoleArray containsObject:@"E_EXP_MNG"])
    {
        AddExpenseButtonOutlet.hidden = YES;
    }
   // RefreshData = 0;
    [self langsetuptranslations];
    
}
-(void)langsetuptranslations
{
    _username_ulbl.text = [helper gettranslation:@"LBL_268"];
    _exptype_ulbl.text = [helper gettranslation:@"LBL_266"];
    _paymenttype_ulbl.text = [helper gettranslation:@"LBL_195"];
    _expdate_ulbl.text = [helper gettranslation:@"LBL_136"];
    _expamount_ulbl.text = [helper gettranslation:@"LBL_841"];
    _expnotes_ulbl.text = [helper gettranslation:@"LBL_242"];
    _expreceipt_ulbl.text = [helper gettranslation:@"LBL_210"];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshData == 1)
    {
        ExpenseTableView.delegate = self;
        ExpenseTableView.dataSource = self;
        [ExpenseTableView reloadData];
       }
}
-(void)refreshData
{
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"expenseDate" ascending:NO];
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    

    
    ExpenseArr = [[helper query_alldata:@"Event_Expense"] filteredArrayUsingPredicate:EventIdPredicate];
    ExpenseArr = [ExpenseArr sortedArrayUsingDescriptors:descriptors];
    ExpenseTableView.delegate = self;
    ExpenseTableView.dataSource = self;
    [ExpenseTableView reloadData];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)AddExpenseButtonAction:(id)sender
{
    appdelegate.ExpenseID = @"";
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToAddExpense" sender:self];
}
#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if(searchBar == ExpenseSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == ExpenseSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == ExpenseSearchBarOutlet)
    {
        ExpenseArr = [helper query_alldata:@"Event_Expense"];
        ExpenseArr = [ExpenseArr filteredArrayUsingPredicate:EventIdPredicate];
        if(searchText.length == 0)
        {
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.ownedBy contains[c] %@) OR (SELF.expenseTypeLIC contains[c] %@) OR (SELF.paymentTypeLIC contains[c] %@) OR (SELF.amount contains[c] %@) OR (SELF.purpose contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[ExpenseArr filteredArrayUsingPredicate:predicate]];
            ExpenseArr = [filtereventarr copy];
        }
        
        [ExpenseTableView reloadData];
    }
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ExpenseArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *UserNamelbl = (UILabel *)[contentView viewWithTag:1];
    UILabel *ExpenseTypelbl = (UILabel *)[contentView viewWithTag:2];
    UILabel *PaymentTypelbl = (UILabel *)[contentView viewWithTag:3];
    UILabel *ExpenseDatelbl = (UILabel *)[contentView viewWithTag:4];
    UILabel *Amountlbl = (UILabel *)[contentView viewWithTag:5];
    UILabel *Noteslbl = (UILabel *)[contentView viewWithTag:6];
    UISwitch *RecieptSwitchButton = (UISwitch *)[contentView viewWithTag:7];
    UIButton *EditButtonOutlet = (UIButton *)[contentView viewWithTag:8];
    UIButton *NotesButton = (UIButton *)[contentView viewWithTag:9];
    UIButton *ViewAttachmentBtn=(UIButton *)[contentView viewWithTag:500];
    
    UIButton *submitExpense=(UIButton *)[contentView viewWithTag:20];
    
    submitExpense.layer.borderColor = [[UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:101.0/255.0 alpha:1.0]CGColor];
    submitExpense.layer.borderWidth = 0.3;
     submitExpense.layer.cornerRadius = 13.0;
    submitExpense.layer.masksToBounds = YES;
   
    [submitExpense setTitle:[helper gettranslation:@"LBL_248"] forState:UIControlStateNormal];
    
    RecieptSwitchButton.transform = CGAffineTransformMakeScale(0.60, 0.60);
   // RecieptSwitchButton.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    RecieptSwitchButton.onTintColor =  [helper DarkBlueColour];
    
    [EditButtonOutlet addTarget:self action:@selector(EditButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *AttachmentUrl=[ExpenseArr[indexPath.row] valueForKey:@"reciepturl"];
    
    if([AttachmentUrl length]>0)
    {
             [ViewAttachmentBtn setHidden:NO];
    }
    else
    {
             [ViewAttachmentBtn setHidden:YES];
    }
    
    [ViewAttachmentBtn addTarget:self action:@selector(AttachmentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [submitExpense addTarget:self action:@selector(submitExpenseAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UserNamelbl.text = [ExpenseArr[indexPath.row] valueForKey:@"ownedBy"];
    ExpenseTypelbl.text = [ExpenseArr[indexPath.row] valueForKey:@"expensetype"];
    PaymentTypelbl.text = [ExpenseArr[indexPath.row] valueForKey:@"paymenttype"];
    
    ExpenseDatelbl.text = [helper formatingdate:[ExpenseArr[indexPath.row] valueForKey:@"ExpenseDate"] datetime:@"datetime"];
    
    Amountlbl.text = [NSString stringWithFormat:@"%@ %@",currencySymbol,[ExpenseArr[indexPath.row] valueForKey:@"amount"]];
    Noteslbl.text = [ExpenseArr[indexPath.row] valueForKey:@"status"];
    
    [Noteslbl setTextColor:[helper getstatuscolor:[ExpenseArr[indexPath.row] valueForKey:@"statuslic"] entityname:@"Expense"]];
  
    if([[ExpenseArr[indexPath.row] valueForKey:@"statuslic"] isEqualToString:@"Submitted"] || [[ExpenseArr[indexPath.row] valueForKey:@"statuslic"] isEqualToString:@"Approved"])
    {
        [submitExpense setHidden:YES];
    }
    else
    {
        [submitExpense setHidden:NO];
    }
    
    
    
    NSString *canedit = [ExpenseArr[indexPath.row] valueForKey:@"canEdit"];
  
    if ([canedit isEqualToString:@"Y"])
    {
       [EditButtonOutlet setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        [submitExpense setHidden:NO];
    }
    
     if ([canedit isEqualToString:@"N"] ||[[ExpenseArr[indexPath.row] valueForKey:@"statuslic"] isEqualToString:@"Submitted"])
    {
         [EditButtonOutlet setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
        [submitExpense setHidden:YES];
    }
    
    
    
//    if ([Noteslbl.text length]>11)
//    {
//        NotesButton.hidden = NO;
//    }
//    else
//    {
//        NotesButton.hidden = YES;
//    }
//    [NotesButton addTarget:self action:@selector(NotesButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[ExpenseArr[indexPath.row] valueForKey:@"recieptsFlag"] isEqualToString:@"Y"])
    {
        [RecieptSwitchButton setOn:YES];
    }
   else if ([[ExpenseArr[indexPath.row] valueForKey:@"recieptsFlag"] isEqualToString:@"N"])
    {
        [RecieptSwitchButton setOn:NO];
    }
    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[ExpenseArr[indexPath.row] valueForKey:@"canDelete"] isEqualToString:@"Y"])
    {
        return YES;
    }
    else if ([[ExpenseArr[indexPath.row] valueForKey:@"canDelete"] isEqualToString:@"N"])
    {
        return NO;
    }
    else
        return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == ExpenseTableView)
    {
        [helper serverdelete:@"E_EXPENSE" entityid:[ExpenseArr[indexPath.row] valueForKey:@"id"]];
        NSPredicate *ExpenseIDPredicate = [NSPredicate predicateWithFormat:@"id like %@",[ExpenseArr[indexPath.row] valueForKey:@"id"]];
        [helper delete_predicate:@"Event_Expense" predicate:ExpenseIDPredicate];
        [self refreshData];
    }
}
-(IBAction)EditButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:ExpenseTableView];
    NSIndexPath *indexPath = [ExpenseTableView indexPathForRowAtPoint:buttonPosition];
    appdelegate.ExpenseID = [ExpenseArr[indexPath.row] valueForKey:@"id"];
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToAddExpense" sender:self];
}
-(IBAction)submitExpenseAction:(id)sender
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:ExpenseTableView];
    NSIndexPath *indexPath = [ExpenseTableView indexPathForRowAtPoint:buttonPosition];
    
    NSString *expenseid=[ExpenseArr[indexPath.row] valueForKey:@"id"];
    appdelegate.ExpenseID=expenseid;
    NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
    [PayloadDict setValue:expenseid forKey:@"ID"];
    [PayloadDict setValue:appdelegate.eventid forKey:@"EventID"];
    [PayloadDict setValue:[helper getLic:@"EXP_STATUS" value:@"Submitted"] forKey:@"StatusLIC"];
    
    NSMutableArray *PayloadArray = [[NSMutableArray alloc]init];
    [PayloadArray addObject:PayloadDict];
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArray options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventExpense" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
     
        
        NSString *response;
        NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Event_Expense"];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id like %@",appdelegate.ExpenseID]; // If required to fetch specific vehicle
        fetchRequest.predicate=predicate;
        
        NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
        
        [matchingData setValue:[helper getvaluefromlic:@"EXP_STATUS" lic:@"Submitted"] forKey:@"status"];
        [matchingData setValue:[helper getLic:@"EXP_STATUS" value:@"Submitted"] forKey:@"statuslic"];
        
        [context save:&error];
        
        if (!error)
        {
            response = @"Success";
            NSLog(@"Value for the Key  updated in DB");
        }
        else
        {
            response = @"Failure While Update";
        }

    }
    
    [self DownloadApprovalsOnExpenseSubmition];
    
    [helper removeWaitCursor];
    [self refreshData];
    
    //appdelegate.ExpenseID = [ExpenseArr[indexPath.row] valueForKey:@"id"];
    //[self.parentViewController performSegueWithIdentifier:@"EventDetailToNotes" sender:self];
}

-(void)DownloadApprovalsOnExpenseSubmition
{
    NSString *process_result = @"Success";
    NSString *AESPrivateKey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [helper create_trans_payload:@"EXP_AFTER_APPROVALS" fltervalue:appdelegate.eventid];
    NSDictionary *EVENTLOV_msgbody = [helper create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetEventDetails" pageno:@"" pagecount:@"" lastpage:@""];
    
   
    
    NSMutableDictionary *TRANXAPI_response = [helper invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        
        NSString *authresponse =  [helper decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSArray *EventExpenseApprovalArr=[jsonData valueForKey:@"ExpenseApproval"];
        
        NSLog(@"%@",appdelegate.eventid);
       [helper deletewitheventid:@"Expense_Approvals" value:appdelegate.eventid];
        if(EventExpenseApprovalArr.count>0)
            [helper insertEventExpenseApproval:EventExpenseApprovalArr];
    }
}
-(IBAction)AttachmentBtnAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:ExpenseTableView];
    NSIndexPath *indexPath = [ExpenseTableView indexPathForRowAtPoint:buttonPosition];
    appdelegate.ExpenseID = [ExpenseArr[indexPath.row] valueForKey:@"id"];
    ReceiptUrl=[ExpenseArr[indexPath.row] valueForKey:@"reciepturl"];
    documentName=[ExpenseArr[indexPath.row] valueForKey:@"filename"];

    
    [self performSegueWithIdentifier:@"showReceipts" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showReceipts"]){
        DisplayAttachment *controller1 = (DisplayAttachment *)segue.destinationViewController;
        controller1.webviewurl=ReceiptUrl;
        
        controller1.documentname=documentName;
        
        controller1.type=@"expense";
    }
}

#pragma mark unwind method
-(IBAction)unwindToEventExpense:(UIStoryboardSegue *)segue
{
    RefreshData  = 1;
   [self refreshData];
}
@end
