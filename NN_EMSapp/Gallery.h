//
//  Gallery.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "LikesViewController.h"
@class HelperClass;
@interface Gallery : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    
    IBOutlet UIView *mainview;
}
@property (strong, nonatomic) IBOutlet iCarousel *carouselview;
@property(strong,nonatomic)HelperClass *helper;
- (IBAction)closebtnaction:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *displayimage;

@property (strong, nonatomic) IBOutlet UILabel *title_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *close_ulbl;



@end
