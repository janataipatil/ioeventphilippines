//
//  Gallery.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Gallery.h"

@interface Gallery ()
{
    // NSMutableArray *imagenamearr,*imagepatharr,*imagedescriptionarr;
    NSArray *galleryimagelist;
    AppDelegate *appdelegate;
    NSString *GalleryImageId;
    NSArray *galleryimgArray;
    
}

@property (nonatomic, strong) NSMutableArray *items;


@end

@implementation Gallery
@synthesize items,carouselview,helper,displayimage;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //configure carousel
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    carouselview.type = iCarouselTypeRotary;
    
    galleryimagelist = [helper query_alldata:@"Event_Gallery"];
    galleryimagelist = [galleryimagelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    carouselview.delegate = self;
    carouselview.dataSource = self;
    appdelegate.currentscreen = @"Gallery";
    appdelegate.swipevar = @"Gallery";
    
    
   /* for(int i=0;i<galleryimagelist.count;i++)
    {
        
        galleryimgArray=[galleryimagelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"location_url=nil"]];
    }
    
    if(galleryimgArray.count>0)
    {
        [self checknullentry];
    }*/
    
    _title_ulbl.text = [helper gettranslation:@"LBL_142"];
    [_close_ulbl setTitle:[helper gettranslation:@"LBL_342"] forState:UIControlStateNormal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(touch.view != mainview)
    {
        // [self removepopup];
    }
    
}
-(void)removepopup
{
    [self performSegueWithIdentifier:@"UnwindfromeventDetailtoGallery" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [galleryimagelist count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UITextView *filedesciprion;
    UIImageView *galleryimages;
    UIButton *LikesCommentsButton;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(carouselview.frame.origin.x, carouselview.frame.origin.y, carouselview.frame.size.width, carouselview.frame.size.height)];
        view.backgroundColor = [UIColor whiteColor];
        view.contentMode = UIViewContentModeCenter;
        
        galleryimages = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 800, 466)];
        galleryimages.tag = 1;
        
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, 474, 303, 40)];
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.f];
        label.tag = 2;
        
        filedesciprion = [[UITextView alloc]initWithFrame:CGRectMake(10, 517, 572, 44)];
        filedesciprion.textAlignment = NSTextAlignmentLeft;
        filedesciprion.font = [UIFont fontWithName:@"Helvetica" size:12.f];
        filedesciprion.tag = 3;
        
        
        LikesCommentsButton = [[UIButton alloc] initWithFrame:CGRectMake(570, 474, 230, 40)];
        LikesCommentsButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        LikesCommentsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.f];

        
        //[LikesCommentsButton setTitleColor:[UIColor colorWithRed:0 green:159.0/255.0f blue:218.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
        
        [LikesCommentsButton setTitleColor:[helper LightBlueColour] forState:UIControlStateNormal];
        
        [LikesCommentsButton addTarget:self action:@selector(LikesCommentsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        LikesCommentsButton.tag = 4;
        LikesCommentsButton.accessibilityValue = [NSString stringWithFormat:@"%ld",(long)index];
        
        [view addSubview:label];
        [view addSubview:galleryimages];
        [view addSubview:filedesciprion];
        [view addSubview:LikesCommentsButton];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:2];
        
        galleryimages = (UIImageView *)[view viewWithTag:1];
        
        filedesciprion = (UITextView *)[view viewWithTag:3];
        
        LikesCommentsButton = (UIButton *)[view viewWithTag:4];
    }
    label.text = [galleryimagelist[index] valueForKey:@"fileName"];
    filedesciprion.text = [galleryimagelist[index] valueForKey:@"desc"];
    galleryimages.image = [UIImage imageWithContentsOfFile:[galleryimagelist[index] valueForKey:@"location_url"]];
    
    [LikesCommentsButton.superview setUserInteractionEnabled:YES];
    [LikesCommentsButton setTitle:[NSString stringWithFormat:@"%@ Likes | %@ Comments",[galleryimagelist[index] valueForKey:@"likeCount"],[galleryimagelist[index] valueForKey:@"commentCount"]] forState:UIControlStateNormal];
    
    
    return view;
}
#pragma mark like comment button action
-(IBAction)LikesCommentsButtonAction:(UIButton *)sender
{
    NSString *index = sender.accessibilityValue;
    NSInteger indexintValue = [index integerValue];
    GalleryImageId = [galleryimagelist[indexintValue] valueForKey:@"id"];
    [self performSegueWithIdentifier:@"GalleryToLikes" sender:self];
}
- (IBAction)closebtnaction:(id)sender
{
    appdelegate.swipevar = @"Galleryclosed";
    [self removepopup];
}
#pragma mark unwind method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"GalleryToLikes"] )
    {
        LikesViewController *Controller = segue.destinationViewController;
        Controller.GalleryImageId = GalleryImageId;
    }
}
-(IBAction)unwindToGallery:(UIStoryboardSegue *)segue
{
    
}

/*-(void) checknullentry
{
    
    
    NSError *error;
    //    imageid is the server url for image
    
    [helper showWaitCursor:@"Getting details. Please Wait !"];
    NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Galleryimages"];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:documentsPath])
    {
        [[NSFileManager defaultManager]createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    
    for(NSDictionary *item in galleryimgArray)
    {
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.png",[item valueForKey:@"fileName"]]]; //Add the file name
        NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[item valueForKey:@"imageid"]]];
        if (curlData)
        {
            [curlData writeToFile:filePath atomically:YES];
            [helper update_imageurl:filePath entityname:@"Event_Gallery" entityid:[item valueForKey:@"id"]];
        }
        else
        {
            [helper printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download GALLERY: %@",[item valueForKey:@"imageurl"]]] ;
        }
    }
    
    galleryimagelist = [helper query_alldata:@"Event_Gallery"];
    galleryimagelist = [galleryimagelist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    [helper removeWaitCursor];
}*/

@end
