//
//  HelpViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 30/03/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"

@interface HelpViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *BckButtonOutlet;
- (IBAction)BackButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *HelpTitleBarlbl;

@property (strong, nonatomic) IBOutlet UIWebView *WebviewOutlet;
@end
