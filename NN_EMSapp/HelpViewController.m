//
//  HelpViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 30/03/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "HelpViewController.h"


@interface HelpViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
}
@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    
    NSArray *helpDocData=[helper query_alldata:@"S_Config_Table"];
    helpDocData=[helpDocData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name like %@",@"helpdocument"]];
    NSString *url=[helpDocData[0] valueForKey:@"value"];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    _HelpTitleBarlbl.text = [helper gettranslation:@"LBL_758"];
    NSURL *URL=[NSURL URLWithString:url];
    NSURLRequest *request=[NSURLRequest requestWithURL:URL];
    [_WebviewOutlet loadRequest:request];
    
    NSLog(@"view controller is");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//-(IBAction)BackButtonAction:(id)sender
//{
    
       // [self performSegueWithIdentifier:@"NotificationviewToHome" sender:self];
    
   // }
//}
- (IBAction)BackButtonAction:(id)sender
{
   // [self.navigationController popViewControllerAnimated:YES];
    [UIView animateWithDuration:0.5 animations:^{
        [self.view setFrame:CGRectMake(1200, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    }
                     completion:^(BOOL finished){[self.view removeFromSuperview];
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];}];
}
@end
