//
//  Database_helper.h
//  Silverline_Client_v1
//
//  Created by iWizardsIV on 7/28/15.
//  Copyright (c) 2015 iWizards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import "NSDataEncryption.h"
#import "SLKeyChainStore.h"
#import "asl.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <EventKit/EventKit.h>
#import "ToastView.h"
#import "EventSpeakerViewController.h"
#import "Gallery.h"
#import "Reachability.h"
#import "CreateSpeakerViewController.h"
#import "EventSynclogsViewController.h"
#import "UpsertActivityView.h"
#import "SLKeyChainStore.h"
#import "AllAttendeesVC.h"



@interface HelperClass : NSObject

@property (nonatomic,strong) HelperClass *DBhelper;

@property (nonatomic) Reachability* reachability;

@property (nonatomic, readonly) NSString* caleventId;


@property (nonatomic, readonly) NSMutableArray* Speakerimages;
@property (nonatomic, readonly) NSMutableArray* Galleryimages;
@property (nonatomic, readonly) NSMutableArray* Resumeimages;
-(void)InsertBackupLabels;
-(NSString*) query_data:(NSString *)colname inputdata:(NSString *)data entityname:(NSString *)ename;
-(NSString*) insert_data:(NSString *)name valuedata:(NSString *)value entityname:(NSString *)ename;
-(NSString*) update_data:(NSString *)colname inputdata:(NSString *)data entityname:(NSString *)ename;
-(void)insertEventProduct:(NSArray *)EventProductArr;
-(NSArray *) query_alldata:(NSString*) entityname;
-(NSString *) delete_alldata:(NSString*)entityname;
-(void)UpdateGroupForAttendee:(NSString*)attendeeid groupName:(NSString *)GroupName;
-(NSArray *) query_predicate:(NSString*) entityname name:(NSString*)name value:(NSString*)value;
-(void) deletewitheventid:(NSString*)entityname value:(NSString*)value;
-(void) delete_predicate:(NSString*)entityname predicate:(NSPredicate*)predicate;
-(NSString *)getGroupNameforGroupID:(NSString *)groupid;
-(void)InsertGroupDataForEventId:(NSArray*)GroupArray sender:(NSString *)sender;
-(void)delete_data:(NSString *)name;
-(NSMutableDictionary *) invokewebservice: (NSDictionary *)inputrequest invokeAPI:(NSString *)APIname HTTPMethod:(NSString *)HTTPMethod parameters:(NSDictionary *)inparameters headers:(NSDictionary *)inheaders;
-(BOOL)CheckIfGroupIdCanBeDeleted:(NSString *)GroupID;
-(void)deletewithGroupID:(NSString *)groupid;
-(NSArray *) predicateSearch:(NSString *)entityname name:(NSString *)name predicate:(NSPredicate *)predicate;
-(NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key;
-(NSString*) decryptData:(NSString*)inputtext withKey:(NSString*)key;
-(NSString *)convertToStringFromCurrency:(NSString *)currency;
-(void) markasrequired:(UILabel *)label;
-(NSString *) create_auth_payload:(NSString*)passcode pushtag:(NSString*)pushtag;
-(NSDictionary *) create_auth_msgbody:(NSString *)payload username:(NSString *)usercode custID:(NSString *)custID appID:(NSString *)appID;
-(NSDictionary *) create_newinputrequest:(NSDictionary *)oldinputrequest AuthToken:(NSString *)AuthToken;
-(void)MoveViewUp:(BOOL)Up Height:(int)Height  ViewToBeMoved:(id)View;
-(void)GetEventApprovalData;
-(NSString*) update_Venue_data:(NSString *)eventid inputdata:(NSString *)location entityname:(NSString *)ename;
-(NSArray *)sortData:(NSArray *)array colname:(NSString *)colname type:(NSString *)type Ascending:(BOOL)Ascending;
-(void)get_EMS_data;
-(NSString *)returneventname:(NSString *)eventid;
-(NSString *)formatingdate:(NSString *)datestr datetime:(NSString *)datetimestr;
-(NSString *) geteventduration:(NSString *)sDate eDate:(NSString *)eDate;
-(void)insertspeakerdata:(NSMutableArray *)dict;
-(NSString *) Process_EMSAct_Config_Table;
-(void) ProcessEventDetails:(NSString *)eventid;
-(NSString *)ProcessItinearyMetaData;
-(void)insertEventExpenseApproval:(NSArray *)ExpenseApprovalArr;
-(void)addEventAttendeeAttachment:(NSMutableArray *)EventAttendeedata;
-(void)insertspeakerattachmentdata:(NSMutableArray *)dict;

-(void)downloadDocument:(NSString *)docURL;
-(NSString *)getLogintranslation: (NSString *)labelid;
-(void)dispatchAttendeeThread;
-(void)showalert:(NSString *)titlestr message:(NSString *)messagestr action:(NSString *)actionstr;
-(NSArray *) capture_nslogs:(NSString *) server_fromtime server_totime:(NSString *)server_totime;

-(NSString *) stringtocurrency:(NSString *)inputstring;
-(NSString *) CurrencytoFloat:(NSString *)currency;

//** Detail insert Functions
-(void)addEventAttendee:(NSString*)event_id EventAttendeedata:(NSMutableArray *)EventAttendeedata;
-(void)addEventSpeaker:(NSString*)event_id EventSpeakerdata:(NSMutableArray *)EventSpeakerdata;

-(NSString *) update_activitydata:(NSString *)activity_id inputdata:(NSDictionary *)inputdata source:(NSString *)source;

-(void) delete_speaker:(NSString*)entityname value:(NSString*)value;
-(void)update_resumeurl:(NSString *)location_url entityname:(NSString *)ename entityid:(NSString *)entityid;

-(void)dispatchSpeakerList;

-(NSDate *) get_midweek_date:(NSDate *)thedate;

//*** cal functions
-(void) update_calid:(NSString *)calevent_refid entityname:(NSString *)ename entityid:(NSString *)entityid;
-(void)insertcal_event:(NSString*)eventid;
-(void)deletecal_event:(NSString*)calevent_refid;
-(void)insertcal_activity:(NSString*)activityid;
-(void)CreateCustomCalendar;
-(BOOL)checkForCalendar:(NSString *)CalName;
-(void) deleteCalendar:(NSString *)titlename;
-(void)showcallist;
-(NSString *)ProcessActivityPlans;

-(void) Insert_PushNotify:(NSString *)messagedetail message_sender:(NSString *)message_sender message_entity:(NSString *)message_entity message_type:(NSString *)message_type message_id:(NSString *)message_id  meassage_read:(NSString *)meassage_read;

-(void)showWaitCursor:(NSString *)txtToShow;
-(void)removeWaitCursor;;
-(NSDictionary *) create_trans_msgbody:(NSString *)payload txType:(NSString *)txType pageno:(NSString *)pageno pagecount:(NSString *)pagecount lastpage:(NSString *)lastpage;
-(NSString *)generate_rowId;
-(NSString *)converingformateddatetooriginaldate:(NSString *)formateddate datetype:(NSString *)datetypestr;

-(void) insert_transaction_local:(NSDictionary*)transaction_message entity_id:(NSString *)entity_id type:(NSString *)type entityname:(NSString *)entityname Status:(NSString *)Status;
-(NSArray *) send_transaction_server;
-(void)imagesDownloader:(NSMutableArray *)speakerimagearr galleryimagearr:(NSMutableArray *)galleryimagearr resumeimages:(NSMutableArray *)resumeimagearr;

-(void)printloginconsole:(NSString *)localloglevel logtext:(NSString *)logtext;
-(void) Reachabilitychanged;

-(void)DeleteItinearyRecord:(NSString *)id;
-(void)InsertItenaryData:(NSArray *)item eventid:(NSString *)eventid;

-(void) send_appack:(NSString *)pushtoken log_mgs:(NSString *)log_mgs;
-(NSString *) getpushmessage;

-(UIColor *)getstatuscolor:(NSString *)statuslabel entityname:(NSString *)entityname;
-(void) update_imageurl:(NSString *)location_url entityname:(NSString *)ename entityid:(NSString *)entityid;

-(NSArray *) getcolumn_names:(NSString*) entityname;// GetCoLUMN NAME DECLARATION 

-(NSString *) ProcessEventList:(NSString *)eventid;
-(NSString *)ProcessEventStats;
//-(void) pickdate;
//-(void)initialiseDateFormatter;
-(void)insertEventAttachment:(NSArray *)EventAttachment EventId:(NSString *)EventId;
-(void)insertActivityCodeFields:(NSArray *)Activityfodefields;
-(NSString *)getUsername;
-(NSString *)getLic:(NSString *)lovtype value:(NSString *)value;
-(NSString *)getvaluefromlic:(NSString *)lovtype lic:(NSString *)lic;


-(void)serverdelete:(NSString *)entityname entityid:(NSString *)entityid;
-(void)insertActivityPlans:(NSArray *)PlanDictionary;
-(void)insertPlanActivities:(NSArray *)ActivitiesDictionary;
-(void) delete_ActivityPlan:(NSString*)entityname value:(NSString*)value;
-(void) delete_ActivitiesinPlan:(NSString*)entityname planid:(NSString*)planid activityid:(NSString *)activityid;
-(void) delete_TeamMember:(NSString*)entityname teamid:(NSString*)teamid eventid:(NSString *)eventid;
-(NSString *)inserEventTypePlans:(NSArray *)PlanlistArr;
-(void)insertEventTeamMember:(NSString *)eventid TeamArr:(NSArray *)TeamArr;
-(NSString *)insertEmailTemplates:(NSArray *)TemplateArr;
-(void)insertEventEmail:(NSArray *)EmailArr;
-(void)insertEventExpense:(NSArray *)ExpenseArr;
-(void)insertEventSurvey:(NSString *)eventid SurveyArr:(NSArray *)SurveyArr;
-(void)insertEventSurveyQn:(NSString *)eventid SurveyQnArr:(NSArray *)SurveyQnArr;
-(void)insertAttendeeList:(NSArray *)AttendeeListArr;
-(NSString *)stringtobool:(NSString *)inputflag;
-(NSString *)booltostring:(BOOL) inputflag;
-(void)insertAttendeePostion:(NSArray *)AttendeePositionArr;
-(void) UpdateEKCalendarevents;
-(NSUInteger)checkForRescheduling;
-(NSString *)getUserPosition;
-(void) FDRCalander;
-(void) DeltaSyncCalander;
-(NSString *)EventCanEditFlag:(NSString *)Flag;
-(void)insertEventSession:(NSString *)eventid SessionArr:(NSArray *)SessionArr;

-(BOOL)CostValidate:(NSString*)number;
-(NSString *) checkinternetconnection;
-(BOOL)isdropdowntextCorrect:text lovtype:(NSString *)lovtype;
-(NSString *)FormatCalendarDate:(NSDate *)Calendardate DateTime:(NSString *)DateTimeStr;

-(NSString *) create_trans_payload:(NSString*)fltertype fltervalue:(NSString*)fltervalue;
-(void)InsertEventMailingList:(NSArray *)MailingList eventid:(NSString *)eventid;
-(BOOL)ValidPassword:(NSString *)pass;
-(NSString *)getPositionID;
-(NSString *)ProcessLanguagetrans;
-(void) ProcessDeleteRequest;
-(NSString *)gettranslation: (NSString *)labelid;
-(NSString *)GetPlanName:Planid;
-(void)InsertItenaryAttachment:(NSMutableArray *)AttachmentList eventid:(NSString *)Eventid;

-(NSString *)getNSDateStr;
-(void)ProcessItinearyDetails:(NSString *)EventID LICType:(NSString *)lic;
-(BOOL)NumberValidate:(NSString*)number;
-(void)dispatchEventAttendeeThread:(NSString *)eventid;
-(void) setlaunchview:(NSString *) targetview;
-(NSString *)Create_ItinearyPayload:(NSString*)fltertype fltervalue:(NSString*)fltervalue lic:(NSString *)LIC;
-(void)insertLanguageTranslatedLabels:(NSArray *)TranslationsArr;
-(void) sendemail:(NSString *)eventmailid emailaddr:(NSString *)emailaddr attendeeid:(NSString *)attendeeid;
-(UIColor *)LightBlueColour;
-(UIColor *)DarkBlueColour;
-(NSString *)geturl:(NSString *)environment;
-(NSString *)getlanguageCode;

-(void) getdata;

@end
