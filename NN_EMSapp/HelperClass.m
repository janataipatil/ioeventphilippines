//
//
//  Database_helper.m
//  Silverline_Client_v1
//
//  Created by iWizardsIV on 7/28/15.
//  Copyright (c) 2015 iWizards. All rights reserved.
//

#import "HelperClass.h"
#import "AppDelegate.h"


@implementation HelperClass
{
    NSManagedObjectContext *context;
    MSClient *client;
    NSMutableDictionary *api_resp;
    UIViewController *currentTopVC;
    AppDelegate *appdelegate;
    NSString * DatatypeLoad;
    NSString *selecteddate;
    NSDateFormatter *formatter;
    UIToolbar *toolBar;
    UpsertActivityView *activity;
    AllAttendeesVC *allAttendee;
    EKEventStore *store;
    NSString *semp;
}
+ (id)sharedManager {
    
    static HelperClass *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
- init
{
    _Speakerimages = [[NSMutableArray alloc] init];
    _Galleryimages = [[NSMutableArray alloc] init];
    _Resumeimages =  [[NSMutableArray alloc] init];
    store = [[EKEventStore alloc] init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    if (self = [super init]) {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [delegate managedObjectContext];
        
        
    }
    
    return self;
}

@synthesize DBhelper;

-(NSString*) query_data:(NSString *)colname inputdata:(NSString *)data entityname:(NSString *)ename
{
    @autoreleasepool{
   // context = [appdelegate managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:ename inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",data];
    [request setPredicate:predicate];
    [request setReturnsObjectsAsFaults:YES];
    
    NSError *error;
    NSString *result;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count <=0)
    {
        result = @"No Matching Data Found";
    }
    else
    {
        result = [[matchingData valueForKey:@"value"] componentsJoinedByString:@""];
    }
    
    return result;
    }
}

-(NSString*) insert_data:(NSString *)name valuedata:(NSString *)value entityname:(NSString *)ename
{
    NSError *error;
    NSString *response;
  //  appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
  //  context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:ename inManagedObjectContext:context];
    NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
    
    [newrecord setValue:name forKey:@"name"];
    [newrecord setValue:value forKey:@"value"];
    
    [context save:&error];
    
    if (!error)
    {
        response = @"Success";
    }
    else
    {
        response =[NSString stringWithFormat:@"%@",error];
    }
    
    return response;
}

-(NSString*) update_data:(NSString *)colname inputdata:(NSString *)data entityname:(NSString *)ename
{
    NSError *error;
    NSString *response;
    //appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:ename inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",colname];
    [request setPredicate:predicate];
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    
    if (matchingData.count <=0)
    {
        //        @"No Data Found";
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:ename inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:colname forKey:@"name"];
        [newrecord setValue:data forKey:@"value"];
        [context save:&error];
        
        if (!error)
        {
            response = @"Success";
            NSLog(@"Value for the Key %@ inserted in DB", colname);
        }
        else
        {
            response = [NSString stringWithFormat:@"%@",error];
        }
        
        
    }
    else
    {
        NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:ename];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"name == %@",colname]; // If required to fetch specific vehicle
        fetchRequest.predicate=predicate;
        
        NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
        
        if(matchingData.count>0)
        {
            [matchingData setValue:data forKey:@"value"];
            [context save:&error];
        }
        if (!error)
        {
            response = @"Success";
            NSLog(@"Value for the Key %@ updated in DB", colname);
        }
        else
        {
            response = @"Failure While Update";
        }
    }
    
    
    return response;
}

-(void) update_calid:(NSString *)calevent_refid entityname:(NSString *)ename entityid:(NSString *)entityid
{
    NSError *error;
    NSString *response;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    NSPredicate *predicate;
    
    if ([ename isEqualToString:@"Event"])
    {
        predicate=[NSPredicate predicateWithFormat:@"eventid like %@",entityid];
    }
    else if ([ename isEqualToString:@"Activities"])
    {
        predicate=[NSPredicate predicateWithFormat:@"activityid like %@",entityid];
    }
    
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:ename];
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    if (matchingData.count>0)
    {
        [matchingData setValue:calevent_refid forKey:@"calevent_refid"];
        [context save:&error];
    }
    
    
    if (!error)
    {
        response = @"Success";
    }
    else
    {
        response = @"Failure While Update";
    }
    
    NSLog(@"%@",response);
}

-(NSString*) update_Venue_data:(NSString *)eventid inputdata:(NSString *)location entityname:(NSString *)ename
{
    NSError *error;
    NSString *response;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:ename inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",eventid];
    [request setPredicate:predicate];
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count>0)
    {
        [matchingData setValue:location forKey:@"venue"];
        [context save:&error];
    }
    if (!error)
    {
        response = @"Success";
        NSLog(@"Value for the Key %@ updated in DB", eventid);
    }
    else
    {
        response = @"Failure While Update";
    }
    
    return response;
}

-(void)delete_data:(NSString *)name
{
    
    NSError *error;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",name];
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"S_Config_Table"];
    
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    int cnt = 0;
    for (NSManagedObject *obj in matchingData)
    {
        [context deleteObject:obj];
        cnt++;
    }
    [context save:&error];
    
    if(matchingData)
    {
        NSLog(@"Removed");
    }
    else
    {
        NSLog(@"Error:%@",error);
    }
    
}
-(void)update_resumeurl:(NSString *)location_url entityname:(NSString *)ename entityid:(NSString *)entityid
{
    NSError *error;
    NSString *response;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id like %@",entityid];;
    
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:ename];
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    [matchingData setValue:location_url forKey:@"resume_location_url"];
    [context save:&error];
    
    if (!error)
    {
        response = @"Success";
    }
    else
    {
        response = @"Failure While Update";
    }
    
    NSLog(@"%@",response);
}

-(void) update_imageurl:(NSString *)location_url entityname:(NSString *)ename entityid:(NSString *)entityid
{
    NSError *error;
    NSString *response;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id like %@",entityid];;
    
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:ename];
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    
    [matchingData setValue:location_url forKey:@"location_url"];
    [context save:&error];
    
    if (!error)
    {
        response = @"Success";
    }
    else
    {
        response = @"Failure While Update";
    }
    
    NSLog(@"%@",response);
}

-(NSArray *) query_predicate:(NSString*) entityname name:(NSString*)name value:(NSString*)value
{
    //appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    NSString *result;
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",value];
    
    [request setEntity:entitydesc];
    [request setPredicate:predicate];
    [request setReturnsObjectsAsFaults:NO];
    
    
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    
    if (matchingData.count <=0)
    {
        result = @"Error: No Data Found";
    }
    else
    {
        result = @"Successfully received Data";
        
    }
    
    return matchingData;
}

-(NSArray *) predicateSearch:(NSString *)entityname name:(NSString *)name predicate:(NSPredicate *)predicate
{
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    NSString *result;
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    [request setEntity:entitydesc];
    [request setPredicate:predicate];
    [request setReturnsObjectsAsFaults:NO];
    
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    
    if (matchingData.count <=0)
    {
        result = @"Error: No Data Found";
    }
    else
    {
        result = @"Successfully received Data";
        
    }
    
    return matchingData;
}

-(void) delete_speaker:(NSString*)entityname value:(NSString*)value
{
    NSString *result;
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",value];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
        
    }
    else
    {
        int count = 0;
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
            count++;
        }
        [context save:&error];
        
        result = [NSString stringWithFormat:@"Success: Records deleted from database  %d ",count];
    }
}

-(void) deletewitheventid:(NSString*)entityname value:(NSString*)value
{
    NSString *result;
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",value];
    
    if ([entityname isEqualToString:@"Activities"])
    {
        NSArray *activities = [[self query_alldata:@"Activities"] filteredArrayUsingPredicate:predicate];
        for (int i=0; i<activities.count;i++)
        {
            NSString *calevent_refid = [activities[i] valueForKey:@"calevent_refid"];
            
            if (calevent_refid.length>0)
            {
                [self deletecal_event:calevent_refid];
            }
            
        }
    }
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
    }
    else
    {
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
        }
        [context save:&error];
        result = [NSString stringWithFormat:@"Success: Records deleted from database"];
    }
    
    
    
}

-(void)deletewithExpenseID:(NSString *)expenseid
{
    NSString *result;
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Expense_Approvals" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"expenseID like %@",expenseid];
    
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
    }
    else
    {
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
        }
        [context save:&error];
        result = [NSString stringWithFormat:@"Success: Records deleted from database"];
    }
    
    
}

-(void) delete_predicate:(NSString*)entityname predicate:(NSPredicate*)predicate
{
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    NSString *pred = [NSString stringWithFormat:@"%@",predicate];
    
    if ([pred containsString:@"nil"])
    {
        return;
    }
    
    NSString *result;
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",value];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
        
    }
    else
    {
        int count = 0;
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
            count++;
        }
        [context save:&error];
        
        result = [NSString stringWithFormat:@"Success: Records deleted from database  %d ",count];
    }
}




-(NSString *) update_activitydata:(NSString *)activity_id inputdata:(NSDictionary *)inputdata source:(NSString *)source
{
    NSError *error;
    NSString *response;
  //  appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Activities" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activityid like %@",activity_id];
    [request setPredicate:predicate];
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    //NSArray *alldata = [[self query_alldata:@"Activities"]filteredArrayUsingPredicate:predicate];
    
    if (matchingData.count <=0)
    {
        //        @"No Data Found";
        //insert new activity
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Activities" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[inputdata valueForKey:@"ActivityID"] forKey:@"activityid"];
        [newrecord setValue:[inputdata valueForKey:@"Description"] forKey:@"desc"];
        [newrecord setValue:[inputdata valueForKey:@"EndDate"] forKey:@"enddate"];
        [newrecord setValue:[inputdata valueForKey:@"EventID"] forKey:@"eventid"];
        [newrecord setValue:[inputdata valueForKey:@"OwnedBy"]?:@"" forKey:@"ownedby"];
        [newrecord setValue:[inputdata valueForKey:@"Priority"] forKey:@"priority"];
        [newrecord setValue:[inputdata valueForKey:@"StartDate"] forKey:@"startdate"];
        [newrecord setValue:[inputdata valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[inputdata valueForKey:@"Type"] forKey:@"type"];
        [newrecord setValue:[inputdata valueForKey:@"Visibility"] forKey:@"visibility"];
        
        [newrecord setValue:[self booltostring:[[inputdata valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[inputdata valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        
        [newrecord setValue:[inputdata valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
        [newrecord setValue:[inputdata valueForKey:@"EventName"] forKey:@"eventname"];
        [newrecord setValue:[inputdata valueForKey:@"PriorityLIC"] forKey:@"prioritylic"];
        [newrecord setValue:[inputdata valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[inputdata valueForKey:@"TypeLIC"] forKey:@"typelic"];
        
        [newrecord setValue:[inputdata valueForKey:@"Comment"] forKey:@"comment"];
        [newrecord setValue:[inputdata valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
        // [newrecord setValue:[inputdata valueForKey:@"UpdatedOn"] forKey:@"updated"];
        
        if ([inputdata valueForKey:@"PlanName"])
        {
            [newrecord setValue:[inputdata valueForKey:@"PlanName"] forKey:@"plan_name"];
        }
        else
        {
            [newrecord setValue:@"Ad-Hoc Activities" forKey:@"plan_name"];
        }
        
        [newrecord setValue:[inputdata valueForKey:@"PlanID"] forKey:@"planid"];
        [newrecord setValue:[inputdata valueForKey:@"PlanActivityID"] forKey:@"planactivityid"];
        [newrecord setValue:nil forKey:@"calevent_refid"];
        
        
        
        
        
        NSError *error;
        [context save:&error];
        
        if (!error)
        {
            response = @"Success";
        }
        else
        {
            response = [NSString stringWithFormat:@"%@",error];
        }
        
        
    }
    else
    {
        //update activity
        NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Activities"];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"activityid like %@",activity_id]; // If required to fetch specific vehicle
        fetchRequest.predicate=predicate;
        
        NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
        
        [matchingData setValue:[inputdata valueForKey:@"ActivityID"] forKey:@"activityid"];
        [matchingData setValue:[inputdata valueForKey:@"Description"] forKey:@"desc"];
        [matchingData setValue:[inputdata valueForKey:@"EndDate"] forKey:@"enddate"];
        [matchingData setValue:[inputdata valueForKey:@"EventID"] forKey:@"eventid"];
        [matchingData setValue:[inputdata valueForKey:@"OwnedBy"]?:@"" forKey:@"ownedby"];
        [matchingData setValue:[inputdata valueForKey:@"Priority"] forKey:@"priority"];
        [matchingData setValue:[inputdata valueForKey:@"StartDate"] forKey:@"startdate"];
        [matchingData setValue:[inputdata valueForKey:@"Status"] forKey:@"status"];
        [matchingData setValue:[inputdata valueForKey:@"Type"] forKey:@"type"];
        [matchingData setValue:[inputdata valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
        [matchingData setValue:[inputdata valueForKey:@"EventName"] forKey:@"eventname"];
        [matchingData setValue:[inputdata valueForKey:@"PriorityLIC"] forKey:@"prioritylic"];
        [matchingData setValue:[inputdata valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [matchingData setValue:[inputdata valueForKey:@"TypeLIC"] forKey:@"typelic"];
        [matchingData setValue:[self booltostring:[[inputdata valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [matchingData setValue:[self booltostring:[[inputdata valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        [matchingData setValue:[inputdata valueForKey:@"Comment"] forKey:@"comment"];
        [matchingData setValue:[inputdata valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
        //[matchingData setValue:[inputdata valueForKey:@"UpdatedOn"] forKey:@"updated"];
        [matchingData setValue:nil forKey:@"calevent_refid"];
        
        if ([inputdata valueForKey:@"PlanName"])
        {
            [matchingData setValue:[inputdata valueForKey:@"PlanName"] forKey:@"plan_name"];
        }
        else
        {
            [matchingData setValue:@"Ad-Hoc Activities" forKey:@"plan_name"];
        }
        
        
        
        if ([source isEqualToString:@"Parent"])
        {
            [matchingData setValue:[inputdata valueForKey:@"Visibility"] forKey:@"visibility"];
        }
        
        [context save:&error];
        
        if (!error)
        {
            response = @"Success";
        }
        else
        {
            response = @"Failure While Update";
        }
    }
    
    
    return response;
}


-(NSArray *) getcolumn_names:(NSString*) entityname
{
   // appdelegate =(AppDelegate *) [[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    
    return [[entitydesc attributesByName ] allKeys];
}

-(NSArray *) query_alldata:(NSString*) entityname
{
    
//    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
  //  context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];

    [request setEntity:entitydesc];
    [request setReturnsObjectsAsFaults:NO];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    return matchingData;
    
}

-(NSString *) delete_alldata:(NSString*) entityname
{
    NSString *message;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    [request setEntity:entitydesc];
    //    [request setReturnsObjectsAsFaults:NO];
    
    NSArray *matchingData = [context executeFetchRequest:request error:nil];
    NSError *error;
    
    for (NSManagedObject *obj in matchingData)
    {
        [context deleteObject:obj];
    }
    
    [context save:&error];
    
    if (error)
    {
        message = [NSString stringWithFormat:@"%@ table truncated",error];
    }
    else
    {
        message = [NSString stringWithFormat:@"%@ in table truncated",entityname];
    }
    
    return message;
}

-(NSMutableDictionary *) invokewebservice: (NSDictionary *)inputrequest invokeAPI:(NSString *)APIname HTTPMethod:(NSString *)HTTPMethod parameters:(NSDictionary *)inparameters headers:(NSDictionary *)inheaders
{
    api_resp = [[NSMutableDictionary alloc] init];
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        /* UIAlertController *alert = [UIAlertController alertControllerWithTitle:[self gettranslation:@"LBL_510"] message:[self gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
         [alert addAction:[UIAlertAction actionWithTitle:[self gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
         currentTopVC = [self currentTopViewController];
         [currentTopVC presentViewController:alert animated:true completion:nil];*/
        // [self showalert:[self gettranslation:@"LBL_590"] message:[self gettranslation:@"LBL_441"] action:[self gettranslation:@"LBL_462"]];
        
        [api_resp setObject:@"999" forKey:@"response_code"];
        [api_resp setObject:[self gettranslation:@"LBL_441"] forKey:@"response_msg"];
        dispatch_semaphore_signal(sem);
    }
    else
    {
        
        
        
        NSString *azureappurl = [self query_data:@"name" inputdata:@"azureappurl" entityname:@"S_Config_Table"];
        // NSString *azureappkey =[self query_data:@"name" inputdata:@"azureappkey" entityname:@"S_Config_Table"];
        NSString *appID = [self query_data:@"name" inputdata:@"applicationid" entityname:@"S_Config_Table"];
        NSString *custID = [self query_data:@"name" inputdata:@"customerid" entityname:@"S_Config_Table"];
        
        NSMutableDictionary *httpheader;
        
        client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
        
        SLKeyChainStore * keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
        //  NSURL *url = [NSURL URLWithString:azureappurl];
        //    client = [MSClient clientWithApplicationURL:url applicationKey:azureappkey];
        client = [MSClient clientWithApplicationURLString:azureappurl];
        
        
        
        if ([APIname isEqualToString:@"SL_Tx"])
        {
            httpheader = [[NSMutableDictionary alloc]init];
            [httpheader setObject:@"2.0.0" forKey:@"ZUMO-API-VERSION"];
            [httpheader setObject:[keychain stringForKey:@"SLToken"] forKey:@"X-ZUMO-AUTH"];
        }
        else
        {
            httpheader = nil;
        }
        
        
        
        
        
        [client invokeAPI:APIname body:inputrequest HTTPMethod:HTTPMethod parameters:inparameters headers:httpheader completion:^(id result, NSHTTPURLResponse *response, NSError *error)
         {
             
             if (error)
             {
                 
                 NSString *errcode= [NSString stringWithFormat:@"%ld",(long)[error code]];
                 [api_resp setObject:errcode forKey:@"response_code"];
                 [api_resp setObject:[error localizedDescription] forKey:@"response_msg"];
                 dispatch_semaphore_signal(sem);
             }
             else
             {
                 // NSString *statusCode = @"004";
                 NSString *statusCode = [[result valueForKey:@"respHeader"]valueForKey:@"statusCode"];
                 if (!([statusCode  isEqual: @"010"] || [statusCode isEqual: @"004"]))
                 {
                     if ([statusCode  isEqual: @"000"])
                     {
                         
                         NSString *Page = [NSString stringWithFormat:@"%@",[[result valueForKey:@"respHeader"]valueForKey:@"page"]];
                         NSString *PageCount = [NSString stringWithFormat:@"%@",[[result valueForKey:@"respHeader"]valueForKey:@"pageCount"]];
                         NSString *LastPage = [NSString stringWithFormat:@"%@",[[result valueForKey:@"respHeader"]valueForKey:@"lastPage"]];
                         
                         [api_resp setObject:statusCode forKey:@"response_code"];
                         [api_resp setObject:Page forKey:@"Page"];
                         [api_resp setObject:PageCount forKey:@"PageCount"];
                         [api_resp setObject:LastPage forKey:@"LastPage"];
                         
                         [api_resp setObject:[result valueForKey:@"respBody"] forKey:@"response_msg"];
                         
                         NSLog(@"\nresult: \n%@",[result valueForKey:@"respBody"]);
                         dispatch_semaphore_signal(sem);
                         
                     }
                     else
                     {
                         [api_resp setObject:statusCode forKey:@"response_code"];
                         [api_resp setObject:[[result valueForKey:@"respHeader"]valueForKey:@"errorMsg"] forKey:@"response_msg"];
                         
                         dispatch_semaphore_signal(sem);
                         
                     }
                 }
                 else
                 {
                     
                     NSString *payload = [self create_auth_payload:[keychain stringForKey:@"password"] pushtag:[keychain stringForKey:@"app_uniquetag"]];
                     NSDictionary *msgbody = [self create_auth_msgbody:payload username:[keychain stringForKey:@"username"] custID:custID appID:appID];
                     
                     [client invokeAPI:@"SL_Auth" body:msgbody HTTPMethod:@"POST" parameters:nil headers:nil completion:^(id logresult, NSHTTPURLResponse *response, NSError *error)
                      {
                          if (error)
                          {
                              
                              NSString *errcode= [NSString stringWithFormat:@"%ld",(long)[error code]];
                              [api_resp setObject:errcode forKey:@"response_code"];
                              [api_resp setObject:[error localizedDescription] forKey:@"response_msg"];
                              
                              dispatch_semaphore_signal(sem);
                              
                              
                              
                          }
                          else
                          {
                              NSString *statusCode = [[logresult valueForKey:@"respHeader"]valueForKey:@"statusCode"];
                              if (!([statusCode  isEqual: @"000"]))
                              {
                                  [self update_data:@"GoToDashboard" inputdata:@"NO" entityname:@"S_Config_Table"];
                                  // [keychain removeItemForKey:@"username"];
                                  //[keychain removeItemForKey:@"password"];
                                  
                                  [self setlaunchview:@"LoginView"];
                                  appdelegate.navpurpose = @"LoginFail";
                                  dispatch_semaphore_signal(sem);
                                  
                              }
                              else
                              {
                                  
                                  NSString *authresponse =  [self decryptData:[[logresult valueForKey:@"respBody"] valueForKey:@"payload"] withKey:[self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"]];
                                  
                                  NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                                  NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                                  
                                  keychain[@"SLToken"] = jsonData[@"SLToken"];
                                  keychain[@"LoginID"] = jsonData[@"LoginID"];
                                  NSDictionary * newinputrequest = [self create_newinputrequest:inputrequest];
                                  
                                  
                                  [client invokeAPI:APIname body:newinputrequest HTTPMethod:HTTPMethod parameters:inparameters headers:httpheader completion:^(id result, NSHTTPURLResponse *response, NSError *error)
                                   {
                                       
                                       if (error)
                                       {
                                           NSString *errcode= [NSString stringWithFormat:@"%ld",(long)[error code]];
                                           
                                           [api_resp setObject:errcode forKey:@"response_code"];
                                           [api_resp setObject:[error localizedDescription] forKey:@"response_msg"];
                                           
                                           dispatch_semaphore_signal(sem);
                                       }
                                       else
                                       {
                                           NSString *statusCode = [[result valueForKey:@"respHeader"]valueForKey:@"statusCode"];
                                           if (!([statusCode  isEqual: @"010"] || [statusCode isEqual: @"004"]))
                                           {
                                               if ([statusCode  isEqual: @"000"])
                                               {
                                                   [api_resp setObject:statusCode forKey:@"response_code"];
                                                   [api_resp setObject:[result valueForKey:@"respBody"] forKey:@"response_msg"];
                                                   
                                                   NSLog(@"\nresult: \n%@",[result valueForKey:@"respBody"]);
                                                   dispatch_semaphore_signal(sem);
                                                   
                                               }
                                               else
                                               {
                                                   [api_resp setObject:statusCode forKey:@"response_code"];
                                                   [api_resp setObject:[[result valueForKey:@"respHeader"]valueForKey:@"errorMsg"] forKey:@"response_msg"];
                                                   
                                                   dispatch_semaphore_signal(sem);
                                                   
                                               }
                                           }
                                           NSLog(@"RESULT------");
                                           //                                   [api_resp setObject:status forKey:@"response_code"];
                                           //                                   [api_resp setObject:[dataresult valueForKey:@"respBody"] forKey:@"response_msg"];
                                           //
                                           dispatch_semaphore_signal(sem);
                                           
                                       }
                                   }];
                              }
                          }
                          
                      }];
                 }
             }
         }];
        
        //    NSLog(@"Delete this ");
        
        while (dispatch_semaphore_wait(sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    }
    return api_resp;
    
}

- (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key
{
    return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key];
}

- (NSString*) decryptData:(NSString*)inputtext withKey:(NSString*)key
{
    
    NSData *nsdata = [[NSData alloc] initWithBase64EncodedString:inputtext options:0];
    
    return [[NSString alloc] initWithData:[nsdata AES256DecryptWithKey:key] encoding:NSUTF8StringEncoding];
}

-(NSString *) create_auth_payload:(NSString*)passcode pushtag:(NSString*)pushtag
{
    NSString *jsonString ;
    NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
    
    [payload setValue:passcode forKey:@"Password"];
    [payload setValue:@"" forKey:@"AuthToken"];
    [payload setValue:pushtag forKey:@"Tag"];
    [payload setValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceId"];
    [payload setValue:[NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemVersion]] forKey:@"OSVer"];
    [payload setValue:@"IOS" forKey:@"OS"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
    
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    } else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSString *key = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    
    return base64String;
}

-(NSDictionary *) create_auth_msgbody:(NSString *)payload username:(NSString *)usercode custID:(NSString *)custID appID:(NSString *)appID
{
    NSString *TxType = @"EMSAUTHM";
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *gmtZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmtZone];
    [dateFormatter setDateFormat:@"yyyyMMddhhmmssSSS"];
    
    NSMutableDictionary *messagebody = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *msgbodypart1= [[NSMutableDictionary alloc] init];
    [msgbodypart1 setObject:usercode forKey:@"username"];
    [msgbodypart1 setObject:custID forKey:@"customerID"];
    [msgbodypart1 setObject:appID forKey:@"appID"];
    
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    [msgbodypart1 setObject:languageCode forKey:@"langCode"];
    [msgbodypart1 setObject:[[NSTimeZone localTimeZone] abbreviation] forKey:@"timeZone"];
    [msgbodypart1 setObject:TxType forKey:@"txType"];
    [msgbodypart1 setObject:[[NSString stringWithFormat:@"%@#%@#%@#%@",usercode,appID,TxType,[dateFormatter stringFromDate:[NSDate date]]]uppercaseString] forKey:@"messageId"];
    [msgbodypart1 setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timestamp"];
    [msgbodypart1 setObject:@"true" forKey:@"encrypted"];
    
    NSMutableDictionary *msgbodypart2= [[NSMutableDictionary alloc] init];
    [msgbodypart2 setObject:payload forKey:@"payload"];
    
    [messagebody setObject:msgbodypart1 forKey:@"reqHeader"];
    [messagebody setObject:msgbodypart2 forKey:@"reqBody"];
    
    // NSLog(@"\nmessagebody: \n%@\n",messagebody);
    return messagebody;
}

//--------------------------------------------------------------------* /
/*-(NSDictionary *) create_newinputrequest:(NSDictionary *)oldinputrequest AuthToken:(NSString *)AuthToken
 {
 NSDictionary *newrequest;
 NSError *error;
 NSString *AESPrivateKey =[self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
 NSString *oldpayload = [[oldinputrequest valueForKey:@"reqBody"] valueForKey:@"payload"];
 NSString *txType = [[oldinputrequest valueForKey:@"reqHeader"] valueForKey:@"txType"];
 
 
 NSString *authresponse =  [self decryptData:oldpayload withKey:AESPrivateKey];
 NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
 
 NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
 
 
 
 NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
 [newDict addEntriesFromDictionary: jsonData];
 [newDict setObject:AuthToken forKey:@"IFSToken"];
 
 // NSLog(@"new newDict: %@",newDict);
 
 NSData *payData = [NSJSONSerialization dataWithJSONObject:newDict options:0 error:&error];
 
 NSString *jsonString = [[NSString alloc] initWithData:payData encoding:NSUTF8StringEncoding];
 
 NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivateKey];
 NSString *newpayload = [encryptedstring base64EncodedStringWithOptions:0];
 
 newrequest = [self create_trans_msgbody:newpayload txType:txType];
 
 return newrequest;
 
 }*/

-(NSDictionary *) create_newinputrequest:(NSDictionary *)oldinputrequest
{
    NSDictionary *newrequest;
    
    NSString *oldpayload = [[oldinputrequest valueForKey:@"reqBody"] valueForKey:@"payload"];
    NSString *txType = [[oldinputrequest valueForKey:@"reqHeader"] valueForKey:@"txType"];
    
    newrequest = [self create_trans_msgbody:oldpayload txType:txType pageno:@"" pagecount:@"" lastpage:@""];
    
    
    return newrequest;
}

//--------------------------------------------------------------------

#pragma Download Functions

-(void)get_EMS_data
{
    
    // dispatch_semaphore_t sem_event;
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        /* UIAlertController *alert = [UIAlertController alertControllerWithTitle:[self gettranslation:@"LBL_510"] message:[self gettranslation:@"LBL_326"] preferredStyle:UIAlertControllerStyleAlert];
         [alert addAction:[UIAlertAction actionWithTitle:[self gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
         currentTopVC = [self currentTopViewController];
         [currentTopVC presentViewController:alert animated:true completion:nil];*/
        // [self showalert:[self gettranslation:@"LBL_590"] message:[self gettranslation:@"LBL_441"] action:[self gettranslation:@"LBL_462"]];
        currentTopVC = [self currentTopViewController];
        [ToastView showToastInParentView:currentTopVC.view withText:[self gettranslation:@"LBL_441"] withDuaration:2.0];
    }
    else
    {
        appdelegate.logout=@"NO";
        if ([appdelegate.PerformDeltaSync isEqualToString:@"NO"])
        {
            [self delete_alldata:@"Activities"];
            [self delete_alldata:@"Event"];
//            [self delete_alldata:@"BackupLabel"];
            [self delete_alldata:@"Event_LOV"];
            [self delete_alldata:@"Act_Config_Table"];
            [self delete_alldata:@"Act_dynamicdetails"];
            [self delete_alldata:@"Speaker"];
            [self delete_alldata:@"Event_Itenary"];
            [self delete_alldata:@"ItinearyFields"];
            [self delete_alldata:@"Attendee"];
            [self delete_alldata:@"AttendeePositions"];
            [self delete_alldata:@"User"];
            [self delete_alldata:@"SpeakerAttachment"];
            [self delete_alldata:@"AttendeeAttachment"];
            [self delete_alldata:@"ActivityPlans"];
            [self delete_alldata:@"PlanActivities"];
            [self delete_alldata:@"Survey_templates"];
            [self delete_alldata:@"Template"];
            [self delete_alldata:@"Product"];
            [self delete_alldata:@"Position_List"];
            [self delete_alldata:@"Lang_translations"];
            [self delete_alldata:@"Expense_Approvals"];
            [self delete_alldata:@"ItinearyAttachment"];
            
            DatatypeLoad = @"FDR";
        }
        else
        {
            DatatypeLoad = @"Delta Download";
        }
        appdelegate.DataDownloaded = @"NO";
        if ([[self Process_EMSAct_Config_Table] isEqualToString:@"Success"])
        {
            if([[self ProcessLanguagetrans] isEqualToString:@"Success"])
            {
                if([[self ProcessItinearyMetaData] isEqualToString:@"Success"])
                {
                    if([[self ProcessEventList:@""] isEqualToString:@"Success"])
                    {
                        if([[self ProcessEventLOV] isEqualToString:@"Success"])
                        {
                            if([[self ProcessEventPositions] isEqualToString:@"Success"])
                            {
                                if([[self ProcessEventSpeakerList] isEqualToString:@"Success"])
                                {
                                    if([[self ProcessActivitylist] isEqualToString:@"Success"])
                                    {
                                        if([[self ProcessEventUserList] isEqualToString:@"Success"])
                                        {
                                            if([[self ProcessEventStats] isEqualToString:@"Success"])
                                            {
                                                if([[self ProcessActivityPlans] isEqualToString:@"Success"])
                                                {
                                                    if([[self ProcessEmailTemplate] isEqualToString:@"Success"])
                                                    {
                                                        if([[self processSurveyTemp] isEqualToString:@"Success"])
                                                        {
                                                            if([[self ProcessAttendeeList] isEqualToString:@"Success"])
                                                            {
                                                                SLKeyChainStore *Keychain;
                                                           
                                                                Keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
                                                                NSArray *LoggedinUserArr = [[self query_alldata:@"User"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"username like %@",[Keychain stringForKey:@"username"]]];
                                                                if ([LoggedinUserArr count]>0)
                                                                {
                                                                    NSString *UserPosition = [LoggedinUserArr[0] valueForKey:@"EMSPosition"];
                                                                    if ([UserPosition isEqualToString:@"Administrator"])
                                                                    {
                                                                        [self update_data:@"GalleryFDRdownload" inputdata:@"NO" entityname:@"S_Config_Table"];
                                                                    }
                                                                    else
                                                                    {
                                                                        [self update_data:@"GalleryFDRdownload" inputdata:@"YES" entityname:@"S_Config_Table"];
                                                                    }
                                                                }
                                                                //                                                              NSString *GalleryFDRdownload = [self query_data:@"name" inputdata:@"GalleryFDRdownload" entityname:@"S_Config_Table"];
                                                                //                                                            if ([GalleryFDRdownload isEqualToString:@"YES"])
                                                                //                                                            {
                                                                //                                                                [self imagesDownloader:_Speakerimages galleryimagearr:_Galleryimages resumeimages:_Resumeimages];
                                                                //                                                            }
                                                                if ([appdelegate.PerformDeltaSync isEqualToString:@"NO"])
                                                                {
                                                                    [self FDRCalander];
                                                                }
                                                                else
                                                                {
                                                                    [self DeltaSyncCalander];
                                                                    [self ProcessDeleteRequest];
                                                                }
                                                                appdelegate.DataDownloaded = @"YES";
                                                            }
                                                            else
                                                            {
                                                                
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if ([appdelegate.PerformDeltaSync isEqualToString:@"NO"] && [appdelegate.DataDownloaded isEqualToString:@"NO"])
    {
        currentTopVC = [self currentTopViewController];
        NSString *CurrentSelectedCViewController = NSStringFromClass([[((UINavigationController *)currentTopVC) visibleViewController] class]);
        if ([CurrentSelectedCViewController isEqualToString:@"HomeViewController"])
        {
            [self setlaunchview:@"LoginView"];
        }
        
        
    }
    if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"] && [appdelegate.DataDownloaded isEqualToString:@"NO"])
    {
        //semp = @"No";
        // [self showalert:[self gettranslation:@"LBL_590"] message:appdelegate.ProcessStatus action:[self gettranslation:@"LBL_462"]];
        /* sem_event = dispatch_semaphore_create(0);
         currentTopVC = [self currentTopViewController];
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:[self gettranslation:@"LBL_590"] message:appdelegate.ProcessStatus preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *okaction = [UIAlertAction actionWithTitle:[self gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
         {
         semp = @"No";
         dispatch_semaphore_signal(sem_event);
         }];
         [alert addAction:okaction];
         [currentTopVC presentViewController:alert animated:NO completion:nil];*/
        
        //currentTopVC = [self currentTopViewController];
        //[ToastView showToastInParentView:currentTopVC.view withText:appdelegate.ProcessStatus withDuaration:2.0];
        
        //[self setlaunchview:@"HomeView"];
        
    }
    // while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
    
    //    if ([semp isEqualToString:@"Yes"])
    //    {
    //         [self setlaunchview:@"LoginView"];
    // }
    
    //        else
    //        {
    //            [ToastView showToastInParentView:currentTopVC.view withText:@"Delta Sync Completed" withDuaration:2.0];
    //        }
    
}


-(NSString *)ProcessLanguagetrans
{
    //NSString *Result;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    
    
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *Language_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
    NSDictionary *Language_msgbody = [self create_trans_msgbody:Language_payload txType:@"EMSGetLabelTL" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:Language_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
        NSMutableArray *transdata = [jsonData valueForKey:@"ListOfLabels"];
        for (int i=0; i<transdata.count; i++)
        {
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Lang_translations" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[transdata[i] valueForKey:@"LabelID"] forKey:@"label_id"];
            [newrecord setValue:[transdata[i] valueForKey:@"Description"] forKey:@"label_enu"];
            [newrecord setValue:[transdata[i] valueForKey:@"LabelText"]forKey:@"label_text"];
            //            [newrecord setValue:[NSString stringWithFormat:@"L_%@",[transdata[i] valueForKey:@"LabelText"]] forKey:@"label_text"];
            NSError *error;
            [context save:&error];
            //
            //            NSLog(@"Translation tbl count: %d",i);
        }
        
        appdelegate.translations = [self query_alldata:@"Lang_translations"];
        
    }
//    [self InsertBackupLabels];
    
    return process_result;
}
-(void)insertLanguageTranslatedLabels:(NSArray *)TranslationsArr
{
    //appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    for (int i=0; i<TranslationsArr.count; i++)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"label_id like %@",[TranslationsArr[i] valueForKey:@"labelID"]];
        [self delete_predicate:@"Lang_translations" predicate:predicate];
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Lang_translations" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[TranslationsArr[i] valueForKey:@"labelID"] forKey:@"label_id"];
        // [newrecord setValue:[TranslationsArr[i] valueForKey:@"Description"] forKey:@"label_enu"];
        [newrecord setValue:[TranslationsArr[i] valueForKey:@"labelText"]forKey:@"label_text"];
        
        NSError *error;
        [context save:&error];
    }
    
}
-(NSString *)gettranslation: (NSString *)labelid
{
    // NSLog(@"appdelegate.translations counr %lu",(unsigned long)appdelegate.translations.count);
    NSString *translation;
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"label_id like %@",labelid];
    
    // NSArray *transarray = [self query_alldata:@"Lang_translations"];
    
    if (appdelegate.translations.count == 0)
    {
        appdelegate.translations = [self query_alldata:@"Lang_translations"];
    }
   
    NSArray *transarray = [appdelegate.translations filteredArrayUsingPredicate:userpredicate];

    if ([transarray count]>0)
    {
        translation = [transarray[0] valueForKey:@"label_text"];
    }
    else
    {
        if(appdelegate.backupTranslations.count == 0)
        {
            appdelegate.backupTranslations = [self query_alldata:@"BackupLabel"];
        }
    
        NSArray *transarray = [appdelegate.backupTranslations filteredArrayUsingPredicate:userpredicate];
        
        if ([transarray count]>0)
        {
            translation = [transarray[0] valueForKey:@"label"];
        }
        else
        translation = labelid ;

    }
    
    return translation;
}

-(NSString *)getLogintranslation: (NSString *)labelid
{
    // NSLog(@"appdelegate.translations counr %lu",(unsigned long)appdelegate.translations.count);
    NSString *translation;
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"label_id like %@",labelid];
    NSArray *transarray = [appdelegate.translations filteredArrayUsingPredicate:userpredicate];
    
    if ([transarray count]>0)
    {
        translation = [transarray[0] valueForKey:@"label_text"];
    }
    else{
        if(appdelegate.backupTranslations.count == 0)
        {
            appdelegate.backupTranslations = [self query_alldata:@"BackupLabel"];
        }
        NSArray *transarray = [appdelegate.backupTranslations filteredArrayUsingPredicate:userpredicate];
    
        if ([transarray count]>0)
        {
            translation = [transarray[0] valueForKey:@"label"];
        }
        else
            translation = labelid ;
    }
//    else
//    {
//        if([labelid isEqualToString:@"LBL_272"]){translation=@"Username";}
//
//        else if ([labelid isEqualToString:@"LBL_724"]){translation=@"Password";}
//
//        else if([labelid isEqualToString:@"LBL_723"]){translation=@"Login";}
//
//        else if([labelid isEqualToString:@"LBL_462"]){translation=@"OK";}
//
//        else if ([labelid isEqualToString:@"LBL_419"]){translation=@"Login Error";}
//
//        else if([labelid isEqualToString:@"LBL_727"]){translation=@"UserName and Password are required for authentication";}
//
//        else if([labelid isEqualToString:@"LBL_726"]){translation=@"Please try again later";}
//
//        else if([labelid isEqualToString:@"LBL_725"]){translation=@"Active internet connection needed.";}
//
//        else if([labelid isEqualToString:@"LBL_628"]){translation=@"Downloading Data.";}
//
//        else if([labelid isEqualToString:@"LBL_331"]){translation=@"Authenticating User";}
//
//        else if([labelid isEqualToString:@"LBL_520"]){translation=@"Registering application";}
//
//        else if([labelid isEqualToString:@"LBL_590"]){translation=@"Alert";}
//
//        else if([labelid isEqualToString:@"LBL_278"]) {translation=@"Version";}
//
//        else if([labelid isEqualToString:@"LBL_472"]) {translation=@"Password should contain atleast 1 digit 1 char and 1 special char";}
//
//        else if([labelid isEqualToString:@"LBL_424"]) {translation=@"Min length";}
//
//        else if([labelid isEqualToString:@"LBL_469"]) {translation=@"Password Reset";}
//
//        else if([labelid isEqualToString:@"LBL_490"]) {translation=@"Please Enter New Password";}
//
//        else if([labelid isEqualToString:@"LBL_427"]) {translation=@"New Password";}
//
//        else if([labelid isEqualToString:@"LBL_345"]) {translation=@"Confirm Password";}
//
//        else if([labelid isEqualToString:@"LBL_219"]) {translation=@"Save";}
//
//        else if([labelid isEqualToString:@"LBL_312"]) {translation=@"Agree";}
//
//        else if([labelid isEqualToString:@"LBL_608"]) {translation=@"Terms and Conditions";}
//
//        else if([labelid isEqualToString:@"LBL_367"]) {translation=@"Disagree";}
//
//        else {translation=labelid;}
//    }
//
    return translation;
}

-(NSString *)ProcessEmailTemplate
{
    NSString *Result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *ActivityPlan_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
    NSDictionary *ActivityPlan_msgbody = [self create_trans_msgbody:ActivityPlan_payload txType:@"EMSGetETemplates" pageno:@"" pagecount:@"" lastpage:@""];
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:ActivityPlan_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        Result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        Result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        // [self delete_alldata:@"Template"];
        NSArray *TemplateList = [jsonData valueForKey:@"TemplateList"];
        [self insertEmailTemplates:TemplateList];
        
    }
    return Result;
    
}
-(NSString *)insertEmailTemplates:(NSArray *)TemplateArr
{
    NSString *success;
  //  appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    for (NSDictionary *record in TemplateArr )
    {
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Template" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        [newrecord setValue:[record valueForKey:@"AffiliateID"] forKey:@"affiliateID"];
        [newrecord setValue:[record valueForKey:@"CreatedBy"] forKey:@"createdBy"];
        [newrecord setValue:[record valueForKey:@"Description"] forKey:@"templateDesc"];
        [newrecord setValue:[record valueForKey:@"LastSentDate"] forKey:@"lastSentDate"];
        [newrecord setValue:[record valueForKey:@"Name"] forKey:@"templateName"];
        [newrecord setValue:[record valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[record valueForKey:@"StatusLIC"] forKey:@"statusLIC"];
        [newrecord setValue:[record valueForKey:@"Subject"] forKey:@"subject"];
        [newrecord setValue:[record valueForKey:@"TemplateID"] forKey:@"templateID"];
        [newrecord setValue:[record valueForKey:@"Type"] forKey:@"templateType"];
        [newrecord setValue:[record valueForKey:@"TypeLIC"] forKey:@"typeLIC"];
        [newrecord setValue:[record valueForKey:@"Category"] forKey:@"templatecategory"];
        [newrecord setValue:[record valueForKey:@"SubCategory"] forKey:@"templatesubcategory"];
        
        NSError *error;
        [context save:&error];
        if (error)
        {
            success = @"Error";
        }
        
    }
    
    return success;
    
}

-(NSString *)processSurveyTemp
{
    NSError *error;
    NSString *PreocessResult;
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:@"TEMPLATE" forKey:@"Entity"];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSGetSurveyTemplate" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        
        //  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        // [alert show];
        
        // [self showalert:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[self gettranslation:@"LBL_462"]];
        PreocessResult = [AUTHWEBAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [AUTHWEBAPI_response valueForKey:@"response_msg"];
        
    }
    else
    {
        PreocessResult = @"Success";
        NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSArray *templatedata = [jsonData valueForKey:@"TemplateList"];
        
        for (NSDictionary *record in templatedata)
        {
            
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[record valueForKey:@"ID"]];
                [self delete_predicate:@"Survey_templates" predicate:predicate];
            }
            
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Survey_templates" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[record valueForKey:@"Status"] forKey:@"status"];
            [newrecord setValue:[record valueForKey:@"ID"] forKey:@"id"];
            [newrecord setValue:[record valueForKey:@"Name"] forKey:@"name"];
            [newrecord setValue:[record valueForKey:@"Description"] forKey:@"templatedescription"];
            [newrecord setValue:[record valueForKey:@"StatusLIC"] forKey:@"statuslic"];
            
            
            NSError *error;
            [context save:&error];
        }
        
    }
    return PreocessResult;
}


-(void) ProcessDeleteRequest
{
    NSError *error;
    NSString *ProceesResult;
    NSPredicate *predicate;
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    //    [JsonDict setValue:@"TEMPLATE" forKey:@"Entity"];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSGetDeletedData" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        
        // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        // [alert show];
        
        // [self showalert:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[self gettranslation:@"LBL_462"]];
        
        ProceesResult = [AUTHWEBAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [AUTHWEBAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSArray *deletiondata = [jsonData valueForKey:@"DeletedList"];
        
        for (NSDictionary *record in deletiondata)
        {
            NSString *entityname = [record valueForKey:@"Entity"];
            NSString *entityid = [record valueForKey:@"ID"];
            
            if ([entityname isEqualToString:@"EMS_Event"])
            {
                NSPredicate *eventpred = [NSPredicate predicateWithFormat:@"eventid like %@",entityid];
                NSArray *EventArray = [[self query_alldata:@"Event"] filteredArrayUsingPredicate:eventpred];
                
                if (EventArray.count >0)
                {
                    NSString *calidentify = [EventArray[0] valueForKey:@"calevent_refid"];
                    [self deletecal_event:calidentify];
                }
                
                
                
                [self delete_predicate:@"Event" predicate:eventpred];
                [self deletewitheventid:@"Activities" value:entityid];
                [self deletewitheventid:@"Act_dynamicdetails" value:entityid];
                [self deletewitheventid:@"Event_Analytics" value:entityid];
                
                [self deletewitheventid:@"Event_Attachments" value:entityid];
                [self deletewitheventid:@"Event_Attendee" value:entityid];
                [self deletewitheventid:@"Event_Comm" value:entityid];
                [self deletewitheventid:@"Event_Gallery" value:entityid];
                [self deletewitheventid:@"Event_Mail" value:entityid];
                [self deletewitheventid:@"Email_Attachment" value:entityid];
                [self deletewitheventid:@"Event_Speaker" value:entityid];
                [self deletewitheventid:@"Event_Team" value:entityid];
                [self deletewitheventid:@"Event_Venue" value:entityid];
                [self deletewitheventid:@"Event_Approvals" value:entityid];
                
                
                
                
            }
            else if ([entityname isEqualToString:@"EMS_Activity"])
            {
                predicate = [NSPredicate predicateWithFormat:@"activityid like %@",entityid];
                [self delete_predicate:@"Activities" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventApproval"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Approvals" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventAttachment"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Attachments" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventAttendee"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Attendee" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventExpense"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Expense" predicate:predicate];
            }
            //            else if ([entityname isEqualToString:@"EMS_EventMail"])
            //            {
            //                predicate = [NSPredicate predicateWithFormat:@"iD like %@",entityid];
            //                [self delete_predicate:@"Event_Mail" predicate:predicate];
            //            }
            else if ([entityname isEqualToString:@"EMS_EventSession"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Session" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventSpeaker"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Speaker" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventSurvey"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Survey" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventTeam"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Team" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EventVenue"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_Venue" predicate:predicate];
            }
            //            else if ([entityname isEqualToString:@"EMS_ApprovalConfig"])
            //            {
            //                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
            //                [self delete_predicate:@"" predicate:predicate];
            //            }
            //            else if ([entityname isEqualToString:@"EMS_ApprovalRoute"])
            //            {
            //                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
            //                [self delete_predicate:@"" predicate:predicate];
            //            }
            //            else if ([entityname isEqualToString:@"EMS_ApprovalRule"])
            //            {
            //                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
            //                [self delete_predicate:@"" predicate:predicate];
            //            }
            else if ([entityname isEqualToString:@"EMS_CodeFieldConfig"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Act_Config_Table" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_EmailTemplate"])
            {
                predicate = [NSPredicate predicateWithFormat:@"iD like %@",entityid];
                [self delete_predicate:@"Event_Mail" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_LabelTL"])
            {
                predicate = [NSPredicate predicateWithFormat:@"label_id like %@",entityid];
                [self delete_predicate:@"Lang_translations" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_LOV"])
            {
                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
                [self delete_predicate:@"Event_LOV" predicate:predicate];
            }
            //            else if ([entityname isEqualToString:@"EMS_MessageTL"])
            //            {
            //                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
            //                [self delete_predicate:@"" predicate:predicate];
            //            }
            else if ([entityname isEqualToString:@"EMS_Plan"])
            {
                predicate = [NSPredicate predicateWithFormat:@"planID like %@",entityid];
                [self delete_predicate:@"ActivityPlans" predicate:predicate];
            }
            else if ([entityname isEqualToString:@"EMS_PlanActivity"])
            {
                predicate = [NSPredicate predicateWithFormat:@"iD like %@",entityid];
                [self delete_predicate:@"PlanActivities" predicate:predicate];
            }
            //            else if ([entityname isEqualToString:@"EMS_RoleAction"])
            //            {
            //                predicate = [NSPredicate predicateWithFormat:@"id like %@",entityid];
            //                [self delete_predicate:@"" predicate:predicate];
            //            }
            
        }
        
    }
}

-(NSString *) ProcessAttendeeList
{
    
    NSString *process_result;
    appdelegate.lstpage = @"N";
    appdelegate.pagecount = @"1";
    appdelegate.pageno = @"1";
    int page;
    
    
    
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTAttendee_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
    NSDictionary *EVENTAttendee_msgbody = [self create_trans_msgbody:EVENTAttendee_payload txType:@"EMSGetAttendeeList" pageno:appdelegate.pageno pagecount:appdelegate.pagecount lastpage:[self stringtobool:@"lstpage"]];
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTAttendee_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
        
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
        page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
        appdelegate.pageno = [NSString stringWithFormat:@"%d",page+1];
        appdelegate.pagecount = [TRANXAPI_response valueForKey:@"PageCount"];
        appdelegate.lstpage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
        
        
        
        NSDictionary *attendeelistdata = [jsonData valueForKey:@"AttendeeList"];
        
        
        [self insert_transaction_local:attendeelistdata entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Attendee List",DatatypeLoad] entityname:@"Attendee List" Status:@"Completed"];
        
        [self insertAttendeeList:[jsonData valueForKey:@"AttendeeList"]];
        [self insertAttendeePostion:[jsonData valueForKey:@"AttendeePositions"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.lstpage forKey:@"lastPageStatus"];
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.pageno forKey:@"pageno"];
        
    }
    
    if ([appdelegate.lstpage isEqualToString:@"N"])
    {
        
        [self dispatchAttendeeThread];
    }
    
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
}

-(NSString *)ProcessActivityPlans
{
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *ActivityPlan_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
    NSDictionary *ActivityPlan_msgbody = [self create_trans_msgbody:ActivityPlan_payload txType:@"EMSGetActPlans" pageno:@"" pagecount:@"" lastpage:@""];
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:ActivityPlan_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
        
        [self insertActivityPlans:[jsonData valueForKey:@"PlanList"]];
        [self insertPlanActivities:[jsonData valueForKey:@"PlanActivity"]];
    }
    return process_result;
}
-(void)insertActivityPlans:(NSArray *)PlanDictionary
{
    
    for (NSDictionary *record in PlanDictionary)
    {
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"planID like %@",[record valueForKey:@"PlanID"]];
            [self delete_predicate:@"ActivityPlans" predicate:predicate];
        }
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"ActivityPlans" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[record valueForKey:@"PlanID"] forKey:@"planID"];
        [newrecord setValue:[record valueForKey:@"AffiliateID"] forKey:@"affiliateID"];
        [newrecord setValue:[record valueForKey:@"Name"] forKey:@"name"];
        [newrecord setValue:[record valueForKey:@"Description"] forKey:@"activityPlandescription"];
        [newrecord setValue:[record valueForKey:@"StatusLIC"] forKey:@"statusLIC"];
        [newrecord setValue:[record valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[record valueForKey:@"CreatedBy"] forKey:@"createdBy"];
        [newrecord setValue:[record valueForKey:@"UpdatedBy"] forKey:@"updatedBy"];
        
        NSError *error;
        [context save:&error];
    }
}
-(void)insertPlanActivities:(NSArray *)ActivitiesDictionary
{
    for (NSDictionary *record in ActivitiesDictionary )
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"iD like %@",[record valueForKey:@"ID"]];
            [self delete_predicate:@"PlanActivities" predicate:predicate];
        }
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"PlanActivities" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        [newrecord setValue:[record valueForKey:@"ID"] forKey:@"iD"];
        [newrecord setValue:[record valueForKey:@"PlanID"] forKey:@"planID"];
        [newrecord setValue:[record valueForKey:@"TypeLIC"] forKey:@"typeLIC"];
        [newrecord setValue:[record valueForKey:@"Type"] forKey:@"type"];
        [newrecord setValue:[record valueForKey:@"Description"] forKey:@"activityDescription"];
        [newrecord setValue:[record valueForKey:@"Duration"] forKey:@"duration"];
        [newrecord setValue:[record valueForKey:@"PriorityLIC"] forKey:@"priorityLIC"];
        [newrecord setValue:[record valueForKey:@"Priority"] forKey:@"priority"];
        [newrecord setValue:[record valueForKey:@"StatusLIC"] forKey:@"statusLIC"];
        [newrecord setValue:[record valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[record valueForKey:@"CreatedBy"] forKey:@"createdBy"];
        [newrecord setValue:[record valueForKey:@"UpdatedBy"] forKey:@"updatedBy"];
        NSError *error;
        [context save:&error];
    }
}
-(NSString *)inserEventTypePlans:(NSArray *)PlanlistArr
{
    NSString *success = @"Success";;
    for (NSDictionary *record in PlanlistArr )
    {
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"EventTypePlans" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        [newrecord setValue:[record valueForKey:@"ID"] forKey:@"iD"];
        [newrecord setValue:[record valueForKey:@"PlanID"] forKey:@"planID"];
        [newrecord setValue:[record valueForKey:@"AffiliateID"] forKey:@"affiliateID"];
        [newrecord setValue:[record valueForKey:@"EventTypeLIC"] forKey:@"eventTypeLIC"];
        [newrecord setValue:[record valueForKey:@"EventSubTypeLIC"] forKey:@"eventSubTypeLIC"];
        [newrecord setValue:[record valueForKey:@"PlanName"] forKey:@"planName"];
        [newrecord setValue:[record valueForKey:@"PlanDesc"] forKey:@"planDesc"];
        
        
        NSError *error;
        [context save:&error];
        if (error)
        {
            success = @"Error";
        }
        
    }
    
    return success;
}


-(NSString *)ProcessActivitylist
{
    
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTActivitylist_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
    NSDictionary *EVENTActivitylist_msgbody = [self create_trans_msgbody:EVENTActivitylist_payload txType:@"EMSGetActList" pageno:@"" pagecount:@"" lastpage:@""];
    
  
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTActivitylist_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [self insert_transaction_local:jsonData entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Activity List",DatatypeLoad] entityname:@"ActivityList " Status:@"Completed"];
        
        
        NSDictionary *Activitydata = [jsonData valueForKey:@"ActivityList"];
        
        for(NSDictionary *item in Activitydata)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activityid like %@",[item valueForKey:@"ActivityID"]];
                
                NSArray *allactivities= [[self query_alldata:@"Activities"] filteredArrayUsingPredicate:predicate];
                
                if ([allactivities count]>0)
                {
                    NSString *calidentify = [allactivities[0] valueForKey:@"calevent_refid"];
                    [self deletecal_event:calidentify];
                    [self delete_predicate:@"Activities" predicate:predicate];
                    //delete codefields associated with activity
                    [self delete_predicate:@"Act_dynamicdetails" predicate:predicate];
                }
                
            }
            [self update_activitydata:[item valueForKey:@"ActivityID"] inputdata:item source:@"Parent"];
        }
        
        
        NSArray *Activitycodefields = [jsonData valueForKey:@"ActCodeFields"];
        
        [self insertActivityCodeFields:Activitycodefields];
        
        
    }
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
}
-(void)insertActivityCodeFields:(NSArray *)Activityfodefields
{
    NSArray *arr = [self query_alldata:@"Act_dynamicdetails"];
    NSLog(@"Arr before is %@",arr);
    for(NSDictionary *item in Activityfodefields)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
        NSArray *checkvalue = [arr  filteredArrayUsingPredicate:predicate];
        if (checkvalue.count > 0)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Act_dynamicdetails" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Act_dynamicdetails" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"ActivityID"] forKey:@"activityid"];
        //                    [newrecord setValue:[inputdata valueForKey:@""] forKey:@"affiliateID"];
        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
        
        // [newrecord setValue:[item valueForKey:@"FieldLIC"] forKey:@"fieldLIC"];
        // [newrecord setValue:[item valueForKey:@"FieldType"] forKey:@"fieldType"];
        
        [newrecord setValue:[item valueForKey:@"FieldName"] forKey:@"fieldName"];
        [newrecord setValue:[item valueForKey:@"FieldNameLIC"] forKey:@"fieldNameLIC"];
        [newrecord setValue:[item valueForKey:@"FieldType"] forKey:@"fieldType"];
        [newrecord setValue:[item valueForKey:@"FieldTypeLIC"] forKey:@"fieldTypeLic"];
        
        [newrecord setValue:[item valueForKey:@"FieldValue"] forKey:@"fieldValue"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"PickListType"] forKey:@"picklistType"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Required"] boolValue]]forKey:@"required"];
        [newrecord setValue:[item valueForKey:@"Sequence"] forKey:@"sequence"];
        
        NSError *error;
        [context save:&error];
    }
    NSArray *afterarr = [self query_alldata:@"Act_dynamicdetails"];
    //NSLog(@"Arr after is %@",afterarr);
}

-(NSString *)ProcessEventList:(NSString *)eventid
{
    
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [self create_trans_payload:@"UserName" fltervalue:eventid];
    NSDictionary *EVENTLOV_msgbody = [self create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetEventList" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [self insert_transaction_local:jsonData entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Events Details ",DatatypeLoad] entityname:@"Event Details" Status:@"Completed"];
        
        NSDictionary *Eventlistdata = [jsonData valueForKey:@"EventList"];
        for(NSDictionary *item in Eventlistdata)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",[item valueForKey:@"EventID"]];
                
                NSArray *allEvents= [[self query_alldata:@"Event"] filteredArrayUsingPredicate:predicate];
                
                if ([allEvents count]>0)
                {
                    NSString *calidentify = [allEvents[0] valueForKey:@"calevent_refid"];
                    [self deletecal_event:calidentify];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",[item valueForKey:@"EventID"]];
                    [self delete_predicate:@"Event" predicate:predicate];
                    //                    [self deletewitheventid:@"Event" value:[item valueForKey:@"EventID"]];
                }
            }
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[item valueForKey:@"ActualCost"] forKey:@"actualcost"];
            [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
            [newrecord setValue:[item valueForKey:@"ApprovedCost"] forKey:@"approvedcost"];
            [newrecord setValue:[item valueForKey:@"BalanceBudget"] forKey:@"balancebudget"];
            [newrecord setValue:[item valueForKey:@"Category"] forKey:@"category"];
            [newrecord setValue:[item valueForKey:@"Department"] forKey:@"department"];
            [newrecord setValue:[item valueForKey:@"DepartmentLIC"] forKey:@"departmentlic"];
            
            [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
            [newrecord setValue:[item valueForKey:@"EndDate"] forKey:@"enddate"];
            [newrecord setValue:[item valueForKey:@"EstimatedCost"] forKey:@"estimatedcost"];
            [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
            [newrecord setValue:[item valueForKey:@"Name"] forKey:@"name"];
            [newrecord setValue:[item valueForKey:@"OwnedBy"] forKey:@"ownedby"];
            [newrecord setValue:[item valueForKey:@"Product"] forKey:@"product"];
            [newrecord setValue:[item valueForKey:@"StartDate"] forKey:@"startdate"];
            [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
            [newrecord setValue:[item valueForKey:@"SubType"] forKey:@"subtype"];
            [newrecord setValue:[item valueForKey:@"Type"] forKey:@"type"];
            [newrecord setValue:[item valueForKey:@"Visibility"] forKey:@"visibility"];
            [newrecord setValue:[item valueForKey:@"ActCompleted"] forKey:@"actcompleted"];
            [newrecord setValue:[item valueForKey:@"ActTotal"] forKey:@"acttotal"];
            [newrecord setValue:[item valueForKey:@"ApprovalFlag"] forKey:@"approvalflag"];
            [newrecord setValue:[item valueForKey:@"AttApproved"] forKey:@"attapproved"];
            [newrecord setValue:[item valueForKey:@"AttTotal"] forKey:@"atttotal"];
            [newrecord setValue:[item valueForKey:@"CategoryLIC"] forKey:@"categorylic"];
            [newrecord setValue:[item valueForKey:@"ProductLIC"] forKey:@"productlic"];
            [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
            [newrecord setValue:[item valueForKey:@"SubTypeLIC"] forKey:@"subtypelic"];
            [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typelic"];
            [newrecord setValue:[item valueForKey:@"Venue"] forKey:@"venue"];
            [newrecord setValue:[item valueForKey:@"EMSUserRoleActions"] forKey:@"eMSUserRoleActions"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"canDelete"] boolValue] ] forKey:@"canDelete"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"canEdit"] boolValue] ]forKey:@"canEdit"];
            //            [newrecord setValue:[item valueForKey:@"Venue"] forKey:@"venue"];
            [newrecord setValue:nil forKey:@"calevent_refid"];
            
            NSError *error;
            [context save:&error];
            
            
            [self ProcessEventDetails:[item valueForKey:@"EventID"]];
            [self ProcessItinearyDetails:[item valueForKey:@"EventID"] LICType:@"ATTENDEE"];
            [self ProcessItinearyDetails:[item valueForKey:@"EventID"] LICType:@"SPEAKER"];
            [self ProcessItinearyDetails:[item valueForKey:@"EventID"] LICType:@"TEAM"];
            
            
        }
    }
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
    
}

-(void) ProcessEventDetails:(NSString *)eventid
{
    NSString *process_result = @"Success";
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [self create_trans_payload:@"Entity" fltervalue:eventid];
    NSDictionary *EVENTLOV_msgbody = [self create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetEventDetails" pageno:@"" pagecount:@"" lastpage:@""];
    
   
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [self insert_transaction_local:jsonData entity_id:eventid type:[NSString stringWithFormat:@"%@ for Event Details ",DatatypeLoad] entityname:@"Event Details" Status:@"Completed"];
        
        
        //        [self delete_alldata:@"Event"];
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"NO"])
        {
            [self deletewitheventid:@"Event_Attachments" value:eventid];
            [self deletewitheventid:@"Event_Attendee" value:eventid];
            [self deletewitheventid:@"Event_Comm" value:eventid];
            [self deletewitheventid:@"Event_Gallery" value:eventid];
            [self deletewitheventid:@"Event_Mail" value:eventid];
            [self deletewitheventid:@"Email_Attachment" value:eventid];
            [self deletewitheventid:@"Event_Speaker" value:eventid];
            [self deletewitheventid:@"Event_Team" value:eventid];
            [self deletewitheventid:@"Event_Session" value:eventid];
            [self deletewitheventid:@"Event_SessionQnA" value:eventid];
            [self deletewitheventid:@"Event_MailingList" value:eventid];
            [self deletewitheventid:@"Product" value:eventid];
            //[self deletewitheventid:@"Event_Itenary" value:eventid];
            //[self deletewitheventid:@"ItinearyFields" value:eventid];
            
            [self deletewitheventid:@"Event_Survey" value:eventid];
            [self deletewitheventid:@"Event_SurveyQuestion" value:eventid];
            [self deletewitheventid:@"Event_Group" value:eventid];
            [self deletewitheventid:@"Event_Venue" value:eventid];
            [self deletewitheventid:@"Event_Approvals" value:eventid];
            [self deletewitheventid:@"Event_Expense" value:eventid];
            
        }
        NSArray *EventExpenseArr = [jsonData valueForKey:@"EventExpense"];
        [self insertEventExpense:EventExpenseArr];
        
        NSArray *EventGroup = [jsonData valueForKey:@"EventGroup"];
        [self InsertGroupDataForEventId:EventGroup sender:@""];
        
        NSArray *EventMailingList = [jsonData valueForKey:@"EventMailingList"];
        [self InsertEventMailingList:EventMailingList eventid:eventid];
        
        
        NSArray *EventProductList =[jsonData valueForKey:@"EventProduct"];
        [self insertEventProduct:EventProductList];
        
        NSArray *EventExpenseApprovalArr=[jsonData valueForKey:@"ExpenseApproval"];
        
        if(EventExpenseApprovalArr.count>0)
        [self insertEventExpenseApproval:EventExpenseApprovalArr];
        
        NSDictionary *EventActivity = [jsonData valueForKey:@"EventActivity"];
        
        for(NSDictionary *item in EventActivity)
        {
            [self update_activitydata:[item valueForKey:@"ActivityID"] inputdata:item source:@"Child"];
        }
        
        NSArray *Activitycodefields = [jsonData valueForKey:@"ActCodeFields"];
        
        [self insertActivityCodeFields:Activitycodefields];
        
        
        NSArray *EventAttachment = [jsonData valueForKey:@"EventAttachment"];
        NSLog(@"Attachment array is %@",EventAttachment);
        [self insertEventAttachment:EventAttachment EventId:eventid];
        
        
        NSDictionary *EventComm = [jsonData valueForKey:@"EventComm"];
        
        for(NSDictionary *item in EventComm)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
                [self delete_predicate:@"Event_Comm" predicate:predicate];
            }
            
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Comm" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[item valueForKey:@"AckDate"] forKey:@"ackdate"];
            [newrecord setValue:eventid forKey:@"eventid"];
            [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeid"];
            [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
            [newrecord setValue:[item valueForKey:@"CommmSGID"] forKey:@"commmsgid"];
            [newrecord setValue:[item valueForKey:@"CommType"] forKey:@"commtype"];
            [newrecord setValue:[item valueForKey:@"RetryCount"] forKey:@"retrycount"];
            [newrecord setValue:[item valueForKey:@"SentDate"] forKey:@"sentdate"];
            [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
            
            NSError *error;
            [context save:&error];
        }
        
        NSArray *EventMail = [jsonData valueForKey:@"EventMail"];
        [self insertEventEmail:EventMail];
        
        NSDictionary *EventMailAtt = [jsonData valueForKey:@"EventMailAtt"];
        
        for(NSDictionary *item in EventMailAtt)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
                [self delete_predicate:@"Email_Attachment" predicate:predicate];
            }
            
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Email_Attachment" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[item valueForKey:@"EventMailID"] forKey:@"eventmailid"];
            [newrecord setValue:eventid forKey:@"eventid"];
            [newrecord setValue:[item valueForKey:@"EventAttachmentID"] forKey:@"eventattachmentid"];
            [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
            
            NSError *error;
            [context save:&error];
        }
        
        
        [self addEventSpeaker:eventid EventSpeakerdata:[jsonData valueForKey:@"EventSpeaker"]];
        
        NSArray *EventTeam = [jsonData valueForKey:@"EventTeam"];
        
        [self insertEventTeamMember:eventid TeamArr:EventTeam];
        
        NSArray *EventSession = [jsonData valueForKey:@"EventSession"];
        
        [self insertEventSession:eventid SessionArr:EventSession];
        
        NSArray *EventSessionQnA = [jsonData valueForKey:@"SessionQnA"];
        
        [self insertEventSessionQnA:eventid SessionQA_arr:EventSessionQnA];
        
        NSArray *EventSurvey = [jsonData valueForKey:@"EventSurvey"];
        if (EventSurvey.count >0)
        {
            [self insertEventSurvey:eventid SurveyArr:EventSurvey];
        }
        
        NSArray *EventSurveyQn = [jsonData valueForKey:@"EventSurveyQn"];
        if (EventSurveyQn.count >0)
        {
            [self insertEventSurveyQn:eventid SurveyQnArr:EventSurveyQn];
        }
        
        NSDictionary *EventVenue = [jsonData valueForKey:@"EventVenue"];
        
        for(NSDictionary *item in EventVenue)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
                [self delete_predicate:@"Event_Venue" predicate:predicate];
            }
            
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Venue" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:eventid forKey:@"eventid"];
            [newrecord setValue:[item valueForKey:@"Address1"] forKey:@"address1"];
            [newrecord setValue:[item valueForKey:@"Address2"] forKey:@"address2"];
            [newrecord setValue:[item valueForKey:@"Address3"] forKey:@"address3"];
            [newrecord setValue:[item valueForKey:@"City"] forKey:@"city"];
            [newrecord setValue:[item valueForKey:@"Country"] forKey:@"country"];
            [newrecord setValue:[item valueForKey:@"State"] forKey:@"state"];
            [newrecord setValue:[item valueForKey:@"CityLIC"] forKey:@"citylic"];
            [newrecord setValue:[item valueForKey:@"CountryLIC"] forKey:@"countrylic"];
            [newrecord setValue:[item valueForKey:@"StateLIC"] forKey:@"statelic"];
            [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
            [newrecord setValue:[item valueForKey:@"Location"] forKey:@"location"];
            //            [newrecord setValue:[item valueForKey:@"StateLIC"] forKey:@"state"];
            [newrecord setValue:[item valueForKey:@"ZipCode"] forKey:@"zipcode"];
            [newrecord setValue:[item valueForKey:@"GeoLat"] forKey:@"latitude"];
            [newrecord setValue:[item valueForKey:@"GeoLong"] forKey:@"longitude"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"MapAttached"] boolValue]] forKey:@"map_attached_flg"];
            
            
            NSError *error;
            [context save:&error];
        }
        
        NSDictionary *EventApproval = [jsonData valueForKey:@"EventApproval"];
        
        for(NSDictionary *item in EventApproval)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
                [self delete_predicate:@"Event_Approvals" predicate:predicate];
            }
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Approvals" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:eventid forKey:@"eventid"];
            [newrecord setValue:[item valueForKey:@"ApproverID"] forKey:@"approverid"];
            [newrecord setValue:[item valueForKey:@"Comment"] forKey:@"comment"];
            [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
            [newrecord setValue:[item valueForKey:@"ParentApprovalID"] forKey:@"parentroutingid"];
            [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
            [newrecord setValue:[item valueForKey:@"ApprovalFor"] forKey:@"approvalfor"];
            [newrecord setValue:[item valueForKey:@"ApproverName"] forKey:@"approverName"];
            [newrecord setValue:[item valueForKey:@"ApproverTypeLIC"] forKey:@"approverTypeLIC"];
            
            
            
            [newrecord setValue:[item valueForKey:@"ParentApprovalName"] forKey:@"parentapprovalname"];
            [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
            [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
            [newrecord setValue:[item valueForKey:@"ApproverTypeLIC"] forKey:@"approverTypeLIC"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"BudgetApprover"] boolValue]] forKey:@"budgetapprover"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue] ]forKey:@"approvalflag"];
            
            NSError *error;
            [context save:&error];
        }
        
        
        [self  ProcessEventAttendees:eventid];
        
        
    }
    NSLog(@"Process Completed: %@",process_result);
}
-(void)insertAttendeeList:(NSArray *)AttendeeListArr
{
    for(NSDictionary *item in AttendeeListArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"AttendeeID"]];
            [self delete_predicate:@"Attendee" predicate:predicate];
        }
        
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Attendee" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"Created"] forKey:@"created"];
        [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"id"];
        // [newrecord setValue:[item valueForKey:@"Updated"] forKey:@"updated"];
        // [newrecord setValue:[item valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
        [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
        [newrecord setValue:[item valueForKey:@"ContactNumber"] forKey:@"contactnumber"];
        [newrecord setValue:[item valueForKey:@"EmailAddress"] forKey:@"emailaddress"];
        [newrecord setValue:[item valueForKey:@"FirstName"] forKey:@"firstname"];
        [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageid"];
        [newrecord setValue:[item valueForKey:@"IntegrationID"] forKey:@"integrationid"];
        [newrecord setValue:[item valueForKey:@"IntegrationSource"] forKey:@"integrationsource"];
        [newrecord setValue:[item valueForKey:@"LastName"] forKey:@"lastname"];
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"Speciality"] forKey:@"speciality"];
        [newrecord setValue:[item valueForKey:@"TargetClass"] forKey:@"targetclass"];
        
        [newrecord setValue:[item valueForKey:@"IntegrationSourceLIC"] forKey:@"integrationsourcelic"];
        [newrecord setValue:[item valueForKey:@"OwnedBy"] forKey:@"ownedby"];
        [newrecord setValue:[item valueForKey:@"SalutationLIC"] forKey:@"salutationlic"];
        [newrecord setValue:[item valueForKey:@"SpecialityLIC"] forKey:@"specialitylic"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[item valueForKey:@"TargetClassLIC"] forKey:@"targetclasslic"];
        [newrecord setValue:[item valueForKey:@"Institution"] forKey:@"institution"];
        [newrecord setValue:[item valueForKey:@"SubTargetClassLIC"] forKey:@"subTargetClassLIC"];
        [newrecord setValue:[item valueForKey:@"SubTargetClass"] forKey:@"subTargetClass"];
        
        NSError *error;
        [context save:&error];
    }
    
}
-(void)insertAttendeePostion:(NSArray *)PositionArr
{
    for(NSDictionary *item in PositionArr)
    {
        /*
         NSArray *Arr = [self query_alldata:@"AttendeePositions"];
         //check wether record is already exist
         
         NSLog(@"%lu",(unsigned long)Arr.count);
         
         NSArray *AttendeePostnArr = [[self query_alldata:@"AttendeePositions"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.iD contains[c] %@)",[item valueForKey:@"ID"]]];
         */
        NSManagedObjectContext *Managedcontext= [appdelegate managedObjectContext];
        //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"AttendeeID"]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.iD contains[c] %@)",[item valueForKey:@"ID"]];
        [self delete_predicate:@"AttendeePositions" predicate:predicate];
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"AttendeePositions" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"iD"];
        [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeID"];
        [newrecord setValue:[item valueForKey:@"PositionID"] forKey:@"positionID"];
        [newrecord setValue:[item valueForKey:@"PositionName"] forKey:@"positionName"];
        NSError *error;
        [Managedcontext save:&error];
        
        /*
         if ([AttendeePostnArr count] == 0)
         {
         NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"AttendeePositions" inManagedObjectContext:context];
         NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
         
         [newrecord setValue:[item valueForKey:@"ID"] forKey:@"iD"];
         [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeID"];
         [newrecord setValue:[item valueForKey:@"PositionID"] forKey:@"positionID"];
         [newrecord setValue:[item valueForKey:@"PositionName"] forKey:@"positionName"];
         NSError *error;
         [context save:&error];
         }
         else
         {
         NSLog(@"Attendee already exist");
         }
         */
    }
    
}
-(void)insertEventExpense:(NSArray *)ExpenseArr
{
    
    for(NSDictionary *item in ExpenseArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Expense" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Expense" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"Amount"] forKey:@"amount"];
        [newrecord setValue:[item valueForKey:@"ExpenseDate"] forKey:@"expenseDate"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"ExpenseType"] forKey:@"expensetype"];
        [newrecord setValue:[item valueForKey:@"ExpenseTypeLIC"] forKey:@"expenseTypeLIC"];
        [newrecord setValue:[item valueForKey:@"OwnedBy"] forKey:@"ownedBy"];
        [newrecord setValue:[item valueForKey:@"PaymentType"] forKey:@"paymenttype"];
        [newrecord setValue:[item valueForKey:@"PaymentTypeLIC"] forKey:@"paymentTypeLIC"];
        [newrecord setValue:[item valueForKey:@"Purpose"] forKey:@"purpose"];
        [newrecord setValue:[item valueForKey:@"RecieptName"] forKey:@"filename"];
        [newrecord setValue:[item valueForKey:@"ExpenseAmount"] forKey:@"expenseAmount"];
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        
        [newrecord setValue:[item valueForKey:@"ExpenseCurrency"] forKey:@"expenseCurrency"];
        [newrecord setValue:[item valueForKey:@"ExpenseCurrencyLIC"] forKey:@"expenseCurrencyLIC"];
        [newrecord setValue:[item valueForKey:@"ConversionRate"] forKey:@"conversionRate"];
        
        
        
        
        
        if([[item valueForKey:@"RecieptURL"]isEqualToString:@"||BLANK__VALUE||"])
        {
            [newrecord setValue:@"" forKey:@"reciepturl"];
        }
        else
        {
            [newrecord setValue:[item valueForKey:@"RecieptURL"] forKey:@"reciepturl"];
        }
        [newrecord setValue:[self booltostring:[[item valueForKey:@"RecieptsFlag"] boolValue]] forKey:@"recieptsFlag"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue] ] forKey:@"approvalFlag"];
        
        //        [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue] ]forKey:@"approvalflag"];
        
        NSError *error;
        [context save:&error];
        
    }
    
    
}

-(void)insertEventExpenseApproval:(NSArray *)ExpenseApprovalArr
{
    for(NSDictionary *item in ExpenseApprovalArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Expense" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Expense_Approvals" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"ApprovalFor"] forKey:@"approvalFor"];
        [newrecord setValue:[item valueForKey:@"ApproverID"] forKey:@"approverID"];
        [newrecord setValue:[item valueForKey:@"ApproverName"] forKey:@"approverName"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"ApproverType"] forKey:@"approverType"];
        [newrecord setValue:[item valueForKey:@"ApproverTypeLIC"] forKey:@"approverTypeLIC"];
        [newrecord setValue:[item valueForKey:@"Comment"] forKey:@"comment"];
        [newrecord setValue:[item valueForKey:@"ExpenseID"] forKey:@"expenseID"];
        [newrecord setValue:[item valueForKey:@"ParentApprovalID"] forKey:@"parentApprovalID"];
        [newrecord setValue:[item valueForKey:@"ParentApprovalName"] forKey:@"parentApprovalName"];
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
        
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statusLIC"];
        
        
        
        
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue] ] forKey:@"approvalFlag"];
        
        
        NSError *error;
        [context save:&error];
        
    }
    
}

//-(void)insertEventProduct:(NSArray *)EventProductArr
//{
//    for(NSDictionary *item in EventProductArr)
//    {
//        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
//        {
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
//            [self delete_predicate:@"Product" predicate:predicate];
//        }
//
//        NSManagedObjectContext *context = [appdelegate managedObjectContext];
//
//        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:context];
//
//        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
//
//        [newrecord setValue:[item valueForKey:@"Weightage"] forKey:@"weightage"];
//        [newrecord setValue:[item valueForKey:@"ProductLIC"] forKey:@"productlic"];
//        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
//        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
//        [newrecord setValue:[self booltostring:[[item valueForKey:@"Deleted"] boolValue]]  forKey:@"delete"];
//
//        NSError *error;
//        [context save:&error];
//    }
//
//}

-(void)insertEventProduct:(NSArray *)EventProductArr
{
    for(NSDictionary *item in EventProductArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Product" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Weightage"] forKey:@"weight"];
        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eid"];
        [newrecord setValue:[item valueForKey:@"ProductLIC"] forKey:@"lic"];
        [newrecord setValue:[item valueForKey:@"Product"] forKey:@"product"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Deleted"]boolValue]] forKey:@"del"];
        
        NSError *error;
        [context save:&error];
        
    }
    
}

-(void)insertEventTeamMember:(NSString *)eventid TeamArr:(NSArray *)TeamArr
{
    for(NSDictionary *item in TeamArr)
    {
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Team" predicate:predicate];
            
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Team" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"Role"] forKey:@"role"];
        [newrecord setValue:[item valueForKey:@"TeamID"] forKey:@"teamid"];
        [newrecord setValue:[item valueForKey:@"TeamName"] forKey:@"teamName"];
        [newrecord setValue:[item valueForKey:@"UserName"] forKey:@"username"];
        [newrecord setValue:[item valueForKey:@"Type"] forKey:@"type"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typelic"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[item valueForKey:@"EMSPosition"] forKey:@"eMSPosition"];
        [newrecord setValue:[item valueForKey:@"EMSPositionLIC"] forKey:@"eMSPositionLIC"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[item valueForKey:@"Email"] forKey:@"email"];
        [newrecord setValue:[item valueForKey:@"RoleLIC"] forKey:@"rolelic"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"AdminDelegate"] boolValue]]  forKey:@"admindelegate"];
        
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        //        [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue] ]forKey:@"approvalflag"];
        
        NSError *error;
        [context save:&error];
        
    }
}
-(void)insertEventEmail:(NSArray *)EmailArr
{
    for(NSDictionary *item in EmailArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"iD like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Mail" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Mail" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"Comment"] forKey:@"comment"];
        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"TemplateID"] forKey:@"templateID"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"iD"];
        [newrecord setValue:[item valueForKey:@"LastSentDate"] forKey:@"lastSentDate"];
        [newrecord setValue:[item valueForKey:@"TemplateName"] forKey:@"templateName"];
        [newrecord setValue:[item valueForKey:@"TemplateType"] forKey:@"templateType"];
        [newrecord setValue:[item valueForKey:@"Category"] forKey:@"category"];
        [newrecord setValue:[item valueForKey:@"CategoryLIC"] forKey:@"categoryLIC"];
        [newrecord setValue:[item valueForKey:@"SubCategory"] forKey:@"subCategory"];
        
        NSError *error;
        [context save:&error];
    }
}

-(void)insertEventSession:(NSString *)eventid SessionArr:(NSArray *)SessionArr
{
    
    for(NSDictionary *item in SessionArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Session" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Session" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typelic"];
        [newrecord setValue:[item valueForKey:@"Title"] forKey:@"title"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"session_desc"];
        [newrecord setValue:[item valueForKey:@"Type"] forKey:@"type"];
        //GROUP CHANGE
        [newrecord setValue:[item valueForKey:@"GroupID"]?:@"" forKey:@"groupID"];
        [newrecord setValue:[item valueForKey:@"Location"] forKey:@"location"];
        [newrecord setValue:[item valueForKey:@"StartDate"] forKey:@"startdate"];
        [newrecord setValue:[item valueForKey:@"EndDate"] forKey:@"enddate"];
        [newrecord setValue:[item valueForKey:@"Rating"] forKey:@"rating"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        //        [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue] ]forKey:@"approvalflag"];
        
        
        NSError *error;
        [context save:&error];
    }
    
}
-(void)insertEventSessionQnA:(NSString *)eventid SessionQA_arr:(NSArray *)SessionQA_arr
{
    
    for(NSDictionary *item in SessionQA_arr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_SessionQnA" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_SessionQnA" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        
        [newrecord setValue:[item valueForKey:@"AnsBy"] forKey:@"answer_by"];
        [newrecord setValue:[item valueForKey:@"AnsDate"] forKey:@"answer_date"];
        [newrecord setValue:[item valueForKey:@"AnsText"] forKey:@"answer_text"];
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"QnBy"] forKey:@"question_by"];
        [newrecord setValue:[item valueForKey:@"QnByName"] forKey:@"question_byname"];
        [newrecord setValue:[item valueForKey:@"QnDate"] forKey:@"question_date"];
        [newrecord setValue:[item valueForKey:@"QnFor"] forKey:@"question_for"];
        [newrecord setValue:[item valueForKey:@"QnForName"] forKey:@"question_forname"];
        [newrecord setValue:[item valueForKey:@"QnText"] forKey:@"question_text"];
        [newrecord setValue:[item valueForKey:@"SessionID"] forKey:@"sessionid"];
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Public"] boolValue]] forKey:@"public"];
        
        NSError *error;
        [context save:&error];
    }
    
}

-(void)InsertEventMailingList:(NSArray *)MailingList eventid:(NSString *)eventid
{
    for(NSDictionary *item in MailingList)
    {
        
        // if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        //{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
        [self delete_predicate:@"Event_MailingList" predicate:predicate];
        //}
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_MailingList" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"EventMailID"] forKey:@"eventmailid"];
        [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeid"];
        [newrecord setValue:[item valueForKey:@"EventAttendeeID"] forKey:@"eventattendeeid"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Deleted"] boolValue]] forKey:@"deleteFlag"];
        
        NSError *error;
        [context save:&error];
    }
    
}

-(void)insertEventSurvey:(NSString *)eventid SurveyArr:(NSArray *)SurveyArr
{
    
    for(NSDictionary *item in SurveyArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Survey" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Survey" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"survey_desc"];
        [newrecord setValue:[item valueForKey:@"EndDate"] forKey:@"survey_enddate"];
        [newrecord setValue:[item valueForKey:@"StartDate"] forKey:@"survey_startdate"];
        [newrecord setValue:[item valueForKey:@"SurveyTemplateID"] forKey:@"survey_templateid"];
        [newrecord setValue:[item valueForKey:@"Title"] forKey:@"title"];
        [newrecord setValue:[item valueForKey:@"QCount"] forKey:@"question_count"];
        
        
        NSError *error;
        [context save:&error];
    }
    
}

-(void)insertEventSurveyQn:(NSString *)eventid SurveyQnArr:(NSArray *)SurveyQnArr
{
    
    for(NSDictionary *item in SurveyQnArr)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_SurveyQuestion" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_SurveyQuestion" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"EventSurveyID"] forKey:@"eventsurveyid"];
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Required"] boolValue]] forKey:@"surveyqn_required"];
        [newrecord setValue:[item valueForKey:@"Sequence"] forKey:@"surveyqn_sequence"];
        [newrecord setValue:[item valueForKey:@"TemplateQnID"] forKey:@"surveyqn_templateid"];
        [newrecord setValue:[item valueForKey:@"QnText"] forKey:@"surveyqn_text"];
        [newrecord setValue:[item valueForKey:@"QnType"] forKey:@"surveyqn_type"];
        [newrecord setValue:[item valueForKey:@"QnTypeLIC"] forKey:@"surveyqn_typelic"];
        
        NSError *error;
        [context save:&error];
    }
    
}
/*-(void)insertEventSurvey:(NSString *)eventid SurveyArr:(NSArray *)SurveyArr
 {
 
 for(NSDictionary *item in SurveyArr)
 {
 if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
 {
 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
 [self delete_predicate:@"Event_Survey" predicate:predicate];
 }
 
 NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Survey" inManagedObjectContext:context];
 NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
 
 [newrecord setValue:eventid forKey:@"eventid"];
 [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
 [newrecord setValue:[item valueForKey:@"Description"] forKey:@"survey_desc"];
 [newrecord setValue:[item valueForKey:@"EndDate"] forKey:@"survey_enddate"];
 [newrecord setValue:[item valueForKey:@"StartDate"] forKey:@"survey_startdate"];
 [newrecord setValue:[item valueForKey:@"SurveyTemplateID"] forKey:@"survey_templateid"];
 [newrecord setValue:[item valueForKey:@"Title"] forKey:@"title"];
 
 NSError *error;
 [context save:&error];
 }
 
 }
 
 -(void)insertEventSurveyQn:(NSString *)eventid SurveyQnArr:(NSArray *)SurveyQnArr
 {
 
 for(NSDictionary *item in SurveyQnArr)
 {
 if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
 {
 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
 [self delete_predicate:@"Event_SurveyQuestion" predicate:predicate];
 }
 
 NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_SurveyQuestion" inManagedObjectContext:context];
 NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
 
 [newrecord setValue:[item valueForKey:@"EventSurveyID"] forKey:@"eventid"];
 [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
 [newrecord setValue:[self booltostring:[item valueForKey:@"Required"] ] forKey:@"surveyqn_required"];
 [newrecord setValue:[item valueForKey:@"Sequence"] forKey:@"surveyqn_sequence"];
 [newrecord setValue:[item valueForKey:@"TemplateQnID"] forKey:@"surveyqn_templateid"];
 [newrecord setValue:[item valueForKey:@"QnText"] forKey:@"surveyqn_text"];
 [newrecord setValue:[item valueForKey:@"QnType"] forKey:@"surveyqn_type"];
 [newrecord setValue:[item valueForKey:@"QnTypeLIC"] forKey:@"surveyqn_typelic"];
 
 NSError *error;
 [context save:&error];
 }
 
 }*/
/*-(void)insertEventSurvey:(NSString *)eventid SurveyArr:(NSArray *)SurveyArr
 {
 
 for(NSDictionary *item in SurveyArr)
 {
 if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
 {
 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
 [self delete_predicate:@"Event_Survey" predicate:predicate];
 }
 
 NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Survey" inManagedObjectContext:context];
 NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
 
 [newrecord setValue:eventid forKey:@"eventid"];
 [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
 [newrecord setValue:[item valueForKey:@"Description"] forKey:@"survey_desc"];
 [newrecord setValue:[item valueForKey:@"EndDate"] forKey:@"survey_enddate"];
 [newrecord setValue:[item valueForKey:@"StartDate"] forKey:@"survey_startdate"];
 [newrecord setValue:[item valueForKey:@"SurveyTemplateID"] forKey:@"survey_templateid"];
 [newrecord setValue:[item valueForKey:@"Title"] forKey:@"title"];
 
 NSError *error;
 [context save:&error];
 }
 
 }
 
 -(void)insertEventSurveyQn:(NSString *)eventid SurveyQnArr:(NSArray *)SurveyQnArr
 {
 
 for(NSDictionary *item in SurveyQnArr)
 {
 if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
 {
 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
 [self delete_predicate:@"Event_SurveyQuestion" predicate:predicate];
 }
 
 NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_SurveyQuestion" inManagedObjectContext:context];
 NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
 
 [newrecord setValue:[item valueForKey:@"EventSurveyID"] forKey:@"eventid"];
 [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
 [newrecord setValue:[self booltostring:[item valueForKey:@"Required"] ] forKey:@"surveyqn_required"];
 [newrecord setValue:[item valueForKey:@"Sequence"] forKey:@"surveyqn_sequence"];
 [newrecord setValue:[item valueForKey:@"TemplateQnID"] forKey:@"surveyqn_templateid"];
 [newrecord setValue:[item valueForKey:@"QnText"] forKey:@"surveyqn_text"];
 [newrecord setValue:[item valueForKey:@"QnType"] forKey:@"surveyqn_type"];
 [newrecord setValue:[item valueForKey:@"QnTypeLIC"] forKey:@"surveyqn_typelic"];
 
 NSError *error;
 [context save:&error];
 }
 
 }*/



-(void)insertEventAttachment:(NSArray *)EventAttachment EventId:(NSString *)EventId
{
    NSMutableArray *GalleryArr = [[NSMutableArray alloc]init];
    for(NSDictionary *item in EventAttachment)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Attachments" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Attachments" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
        [newrecord setValue:EventId forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"StorageID"] forKey:@"storageid"];
        [newrecord setValue:[item valueForKey:@"FileName"] forKey:@"filename"];
        [newrecord setValue:[item valueForKey:@"FileSize"] forKey:@"filesize"];
        [newrecord setValue:[item valueForKey:@"FileType"] forKey:@"filetype"];
        [newrecord setValue:[item valueForKey:@"UploadDate"] forKey:@"uploaddate"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"attachmenttypelic"];
        [newrecord setValue:[item valueForKey:@"Type"] forKey:@"attachmenttype"];
        //GROUP CHANGE
        [newrecord setValue:[item valueForKey:@"GroupID"]?:@"" forKey:@"groupID"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Public"] boolValue]] forKey:@"public"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        
        
        if ([[item valueForKey:@"TypeLIC"] isEqualToString:@"Gallery"])
        {
            [GalleryArr addObject:item];
        }
        
        NSError *error;
        [context save:&error];
    }
    NSLog(@"Gallery arr is %@",GalleryArr);
    [self insertgalleryimages:GalleryArr];
}
-(void)insertgalleryimages:(NSMutableArray *)gallerydict
{
    
    for(NSDictionary *item in gallerydict)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Gallery" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Gallery" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
        [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"StorageID"] forKey:@"imageid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"FileSize"] forKey:@"fileSize"];
        [newrecord setValue:[item valueForKey:@"FileName"] forKey:@"fileName"];
        [newrecord setValue:[item valueForKey:@"FileType"] forKey:@"fileType"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Public"] boolValue]] forKey:@"public"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typeLIC"];
        [newrecord setValue:[item valueForKey:@"UploadDate"] forKey:@"uploadDate"];
        [newrecord setValue:[item valueForKey:@"CommentCount"] forKey:@"commentCount"];
        [newrecord setValue:[item valueForKey:@"LikeCount"] forKey:@"likeCount"];
        NSError *error;
        [context save:&error];
        
        NSString *imageurl = [item valueForKey:@"StorageID"];
        if (imageurl.length > 0)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:[item valueForKey:@"StorageID"] forKey:@"imageurl"];
            [dict setValue:[item valueForKey:@"ID"] forKey:@"imageid"];
            [_Galleryimages addObject:dict];
        }
    }
    
    
    
    
    
}
-(NSString *) Process_EMSAct_Config_Table
{
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *ActConfig_payload = [self create_trans_payload:@"" fltervalue:@""];
    NSDictionary *ActConfig_msgbody = [self create_trans_msgbody:ActConfig_payload txType:@"EMSGetCodeFields" pageno:@"" pagecount:@"" lastpage:@""];
    
   
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:ActConfig_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
        
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSDictionary *Codedata = [jsonData valueForKey:@"CodeFieldList"];
        
        for(NSDictionary *item in Codedata)
        {
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
                [self delete_predicate:@"Act_Config_Table" predicate:predicate];
            }
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Act_Config_Table" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[item valueForKey:@"ActivityTypeLIC"] forKey:@"activity_typelic"];
            [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliate_id"];
            [newrecord setValue:[item valueForKey:@"FieldNameLIC"] forKey:@"fieldnamelic"];
            [newrecord setValue:[item valueForKey:@"FieldType"] forKey:@"fieldtype"];
            [newrecord setValue:[item valueForKey:@"FieldTypeLIC"] forKey:@"fieldtypelic"];
            [newrecord setValue:[item valueForKey:@"FieldName"] forKey:@"fieldname"];
            [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
            [newrecord setValue:[item valueForKey:@"PickListType"] forKey:@"picklist_type"];
            [newrecord setValue:[self booltostring:[[item valueForKey:@"Required"] boolValue]] forKey:@"required"];
            [newrecord setValue:[item valueForKey:@"Sequence"] forKey:@"sequence"];
            
            
            NSError *error;
            [context save:&error];
        }
    }
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
    
}
-(NSString *) ProcessEventLOV
{
    
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [self create_trans_payload:@"" fltervalue:@""];
    NSDictionary *EVENTLOV_msgbody = [self create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetLOV" pageno:@"" pagecount:@"" lastpage:@""];
    
   
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSDictionary *LOVdata = [jsonData valueForKey:@"ListOfValues"];
        
        [self insert_transaction_local:LOVdata entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for LOV details",DatatypeLoad] entityname:@"Event LOV details" Status:@"Completed"];
        
        for(NSDictionary *item in LOVdata)
        {
            
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"LOVID"]];
                [self delete_predicate:@"Event_LOV" predicate:predicate];
            }
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_LOV" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            
            [newrecord setValue:[item valueForKey:@"LOVID"] forKey:@"id"];
            [newrecord setValue:[item valueForKey:@"Name"] forKey:@"name"];
            [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
            // [newrecord setValue:[item valueForKey:@"Updated"] forKey:@"updated"];
            [newrecord setValue:[item valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
            [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
            [newrecord setValue:[item valueForKey:@"LIC"] forKey:@"lic"];
            [newrecord setValue:[item valueForKey:@"LOVTYPE"] forKey:@"lovtype"];
            [newrecord setValue:[item valueForKey:@"LangCode"] forKey:@"langcode"];
            [newrecord setValue:[item valueForKey:@"Sequence"] forKey:@"sequence"];
            [newrecord setValue:[item valueForKey:@"Value"] forKey:@"value"];
            
            [newrecord setValue:[item valueForKey:@"LangCodeLIC"] forKey:@"langcodelic"];
            [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
            
            if ([[item valueForKey:@"LOVTYPE"] isEqualToString:@"EVENT_TYPE"] || [[item valueForKey:@"LOVTYPE"] isEqualToString:@"EVENT_SUBTYPE"] ||
                [[item valueForKey:@"LOVTYPE"] isEqualToString:@"SUB_TRGT_CLASS"] ||[[item valueForKey:@"LOVTYPE"] isEqualToString:@"ATT_TYPE"] || [[item valueForKey:@"LOVTYPE"] isEqualToString:@"STATE"] ||[[item valueForKey:@"LOVTYPE"] isEqualToString:@"CITY"] )
            {
                [newrecord setValue:[item valueForKey:@"PARLOV"] forKey:@"parlov"];
                [newrecord setValue:[item valueForKey:@"PARLIC"] forKey:@"parlic"];
                
            }
            
            NSError *error;
            [context save:&error];
        }
    }
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
}


-(NSString *) ProcessEventUserList
{
    NSString *process_result;
    NSString *lstpage = @"N";
    NSString *pagecount = @"1";
    NSString *pageno = @"1";
    int page;
    
    while ([lstpage isEqual: @"N"])
    {
        NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        //if we pass username it will return only that user else it returns all user list
        //NSString *EVENTUser_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
        NSString *EVENTUser_payload = [self create_trans_payload:@"" fltervalue:@""];
        NSDictionary *EVENTUser_msgbody = [self create_trans_msgbody:EVENTUser_payload txType:@"EMSGetUserList" pageno:pageno pagecount:pagecount lastpage:[self stringtobool:@"lstpage"]];
        
        
        NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTUser_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            process_result = [TRANXAPI_response valueForKey:@"response_msg"];
            appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
        }
        else
        {
            process_result = @"Success";
            NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            
            
            page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
            pageno = [NSString stringWithFormat:@"%d",page+1];
            pagecount = [TRANXAPI_response valueForKey:@"PageCount"];
            lstpage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
            
            
            NSDictionary *Userlistdata = [jsonData valueForKey:@"UserList"];
            
            //        [self delete_alldata:@"User"];
            
            for(NSDictionary *item in Userlistdata)
            {
                if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[item valueForKey:@"UserID"]];
                    
                    [self delete_predicate:@"User" predicate:predicate];
                }
                
                NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
                NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                
                
                [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
                [newrecord setValue:[item valueForKey:@"ContactNumber"] forKey:@"contactnumber"];
                [newrecord setValue:[item valueForKey:@"Email"] forKey:@"emailaddress"];
                [newrecord setValue:[item valueForKey:@"ManagerID"] forKey:@"managerid"];
                [newrecord setValue:[item valueForKey:@"FirstName"] forKey:@"firstname"];
                [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageid"];
                [newrecord setValue:[item valueForKey:@"LastName"] forKey:@"lastname"];
                [newrecord setValue:[item valueForKey:@"LoginID"] forKey:@"loginid"];
                [newrecord setValue:[item valueForKey:@"IntegrationSource"] forKey:@"integrationsource"];
                [newrecord setValue:[item valueForKey:@"IntegrationID"] forKey:@"integrationid"];
                
                [newrecord setValue:[NSString stringWithFormat:@"%@ %@(%@)",[item valueForKey:@"FirstName"],[item valueForKey:@"LastName"],[item valueForKey:@"UserID"]] forKey:@"fullname"];
                
                [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
                [newrecord setValue:[item valueForKey:@"Created"] forKey:@"created"];
                [newrecord setValue:[item valueForKey:@"CreatedBy"] forKey:@"createdby"];
                [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
                //[newrecord setValue:[item valueForKey:@"Updated"] forKey:@"updated"];
                [newrecord setValue:[item valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
                [newrecord setValue:[item valueForKey:@"LastMobileDelta"] forKey:@"lastmobiledeltasync"];
                [newrecord setValue:[item valueForKey:@"LastMobileSync"] forKey:@"lastmobilesync"];
                [newrecord setValue:[item valueForKey:@"LastWebLogin"] forKey:@"lastweblogin"];
                [newrecord setValue:[item valueForKey:@"Phone"] forKey:@"phone"];
                [newrecord setValue:[item valueForKey:@"UserID"] forKey:@"userid"];
                [newrecord setValue:[item valueForKey:@"UserName"] forKey:@"username"];
                [newrecord setValue:[item valueForKey:@"EMSUserRole"] forKey:@"emsuserrole"];
                [newrecord setValue:[item valueForKey:@"EMSUserRoleLIC"] forKey:@"emsuserrolelic"];
                [newrecord setValue:[item valueForKey:@"EMSPosition"] forKey:@"eMSPosition"];
                [newrecord setValue:[item valueForKey:@"EMSPositionLIC"] forKey:@"eMSPositionLIC"];
                [newrecord setValue:[item valueForKey:@"IntegrationSourceLIC"] forKey:@"integrationsourcelic"];
                [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
                
                
                
                NSError *error;
                [context save:&error];
            }
        }
    }
    
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
}

-(NSString *) ProcessEventPositions
{
    NSString *process_result;
    NSString *lstpage = @"N";
    NSString *pagecount = @"1";
    NSString *pageno = @"1";
    int page;
    
    while ([lstpage isEqual: @"N"])
    {
        NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        //if we pass username it will return only that user else it returns all user list
        //NSString *EVENTUser_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
        NSString *EVENTUser_payload = [self create_trans_payload:@"" fltervalue:@""];
        NSDictionary *EVENTUser_msgbody = [self create_trans_msgbody:EVENTUser_payload txType:@"EMSGetPosition" pageno:pageno pagecount:pagecount lastpage:[self stringtobool:@"lstpage"]];
        
        
        
        NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTUser_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            process_result = [TRANXAPI_response valueForKey:@"response_msg"];
            appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
        }
        else
        {
            process_result = @"Success";
            NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            
            
            page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
            pageno = [NSString stringWithFormat:@"%d",page+1];
            pagecount = [TRANXAPI_response valueForKey:@"PageCount"];
            lstpage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
            
            
            NSDictionary *Userlistdata = [jsonData valueForKey:@"PositionList"];
            
            //        [self delete_alldata:@"User"];
            
            for(NSDictionary *item in Userlistdata)
            {
                
                
                NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Position_List" inManagedObjectContext:context];
                NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                
                
                [newrecord setValue:[item valueForKey:@"EndDate"] forKey:@"endDate"];
                [newrecord setValue:[item valueForKey:@"IntegrationSource"] forKey:@"integrationSource"];
                [newrecord setValue:[item valueForKey:@"IntegrationSourceLIC"] forKey:@"integrationSourceLIC"];
                [newrecord setValue:[item valueForKey:@"ParentPositionID"] forKey:@"parentPositionID"];
                [newrecord setValue:[item valueForKey:@"ParentPositionName"] forKey:@"parentPositionName"];
                [newrecord setValue:[item valueForKey:@"PositionID"] forKey:@"positionID"];
                [newrecord setValue:[item valueForKey:@"PositionName"] forKey:@"positionName"];
                [newrecord setValue:[item valueForKey:@"PositionType"] forKey:@"positionType"];
                [newrecord setValue:[item valueForKey:@"PositionTypeLIC"] forKey:@"positionTypeLIC"];
                [newrecord setValue:[item valueForKey:@"StartDate"] forKey:@"startDate"];
                [newrecord setValue:[item valueForKey:@"IntegrationID"] forKey:@"integrationid"];
                [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
                [newrecord setValue:[item valueForKey:@"Created"] forKey:@"created"];
                [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
                
                
                
                NSError *error;
                [context save:&error];
            }
        }
    }
    
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
}


-(void) ProcessEventAttendees:(NSString *) eventid
{
    NSString *process_result = @"Success";
    appdelegate.EAlstpage = @"N";
    appdelegate.EApagecount = @"1";
    appdelegate.EApageno= @"1";
    int page;
    
    
    //while ([lstpage isEqual: @"N"])
    //{
    NSError *error;
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:eventid forKey:@"EventID"];
    [JsonDict setValue:@"ATTENDEE" forKey:@"Entity"];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSGetEventDetails" pageno:appdelegate.EApageno pagecount:appdelegate.EApagecount lastpage:[self stringtobool:@"lstpage"]];
    
    
    NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        //[self removeWaitCursor];
        // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        //[alert show];
        
        // [self showalert:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[self gettranslation:@"LBL_462"]];
        NSLog(@"Server error %@",[AUTHWEBAPI_response valueForKey:@"response_code"]);
    }
    else
    {
        NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
        page = [[AUTHWEBAPI_response valueForKey:@"Page"] intValue];
        appdelegate.EApageno = [NSString stringWithFormat:@"%d",page+1];
        appdelegate.EApagecount = [AUTHWEBAPI_response valueForKey:@"PageCount"];
        appdelegate.EAlstpage = [self booltostring:[[AUTHWEBAPI_response valueForKey:@"LastPage"] boolValue]];
        
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.EAlstpage forKey:@"EAlastPageStatus"];
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.EApageno forKey:@"EApageno"];
        
        NSLog(@"JSON Data : %@",jsonData);
        
        [self addEventAttendee:eventid EventAttendeedata:[jsonData valueForKey:@"EventAttendee"]];
        
        [self addEventAttendeeAttachment:[jsonData valueForKey:@"AttAttachment"]];
    }
    // }
    if([appdelegate.EAlstpage isEqualToString:@"N"])
    {
        [self dispatchEventAttendeeThread:eventid];
    }
    
    
    
}
-(void)addEventAttendeeAttachment:(NSMutableArray *)EventAttendeedata
{
    for(NSDictionary *item in EventAttendeedata)
    {
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID like %@",[item valueForKey:@"ID"]];
            [self delete_predicate:@"AttendeeAttachment" predicate:predicate];
        }
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"AttendeeAttachment" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
        [newrecord setValue:[item valueForKey:@"EntityID"] forKey:@"entityid"];
        [newrecord setValue:[item valueForKey:@"FileName"] forKey:@"filename"];
        [newrecord setValue:[item valueForKey:@"FileSize"] forKey:@"fileSize"];
        [newrecord setValue:[item valueForKey:@"FileType"] forKey:@"fileType"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Type"] forKey:@"type"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typeLIC"];
        [newrecord setValue:[item valueForKey:@"UploadDate"] forKey:@"uploaddate"];
        [newrecord setValue:[item valueForKey:@"StorageID"] forKey:@"storageID"];
        
        NSError *error;
        [context save:&error];
    }
    
}
/*
 -(void)dispatchAttendeeThread
 {
 appdelegate.downloadInProgress=@"YES";
 
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
 ^{
 int page;
 NSLog(@"last page:%@",appdelegate.lstpage);
 while ([appdelegate.lstpage isEqualToString:@"N"])
 {
 NSString *Response;
 
 NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
 NSString *EVENTAttendee_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
 NSDictionary *EVENTAttendee_msgbody = [self create_trans_msgbody:EVENTAttendee_payload txType:@"EMSGetAttendeeList" pageno:appdelegate.pageno pagecount:appdelegate.pagecount lastpage:[self stringtobool:@"lstpage"]];
 
 NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTAttendee_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
 
 if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
 {
 // Response = ;
 // [ToastView showToastInParentView:self.window withText:[TRANXAPI_response valueForKey:@"response_msg"] withDuaration:2.0];
 }
 else
 {
 NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
 NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
 NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
 
 
 page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
 appdelegate.pageno = [NSString stringWithFormat:@"%d",page+1];
 appdelegate.pagecount = [TRANXAPI_response valueForKey:@"PageCount"];
 NSLog(@"pagecount:%@",appdelegate.pagecount);
 appdelegate.lstpage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
 
 
 [[NSUserDefaults standardUserDefaults]setObject:appdelegate.lstpage forKey:@"lastPageStatus"];
 [[NSUserDefaults standardUserDefaults]setObject:appdelegate.pageno forKey:@"pageno"];
 
 NSLog(@"last page %@ %@",appdelegate.lstpage,appdelegate.pageno);
 
 NSDictionary *attendeelistdata = [jsonData valueForKey:@"AttendeeList"];
 
 
 [self insert_transaction_local:attendeelistdata entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Attendee List",DatatypeLoad] entityname:@"Attendee List" Status:@"Completed"];
 
 [self insertAttendeeList:[jsonData valueForKey:@"AttendeeList"]];
 [self insertAttendeePostion:[jsonData valueForKey:@"AttendeePositions"]];
 
 
 if([appdelegate.logout isEqualToString:@"YES"])
 {
 appdelegate.lstpage=@"Y";
 }
 
 }
 }
 
 dispatch_async(dispatch_get_main_queue(),
 ^{
 
 appdelegate.downloadInProgress=@"NO";
 
 });
 });
 
 
 }
 */
-(void)dispatchAttendeeThread
{
    appdelegate.downloadInProgress=@"YES";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       while ([appdelegate.lstpage isEqualToString:@"N"])
                       {
                           @autoreleasepool {
                               
                               
                               int page;
                               NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
                               NSString *EVENTAttendee_payload = [self create_trans_payload:@"UserName" fltervalue:@""];
                               NSDictionary *EVENTAttendee_msgbody = [self create_trans_msgbody:EVENTAttendee_payload txType:@"EMSGetAttendeeList" pageno:appdelegate.pageno pagecount:appdelegate.pagecount lastpage:[self stringtobool:@"lstpage"]];
                               
                               NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTAttendee_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
                               
                               if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
                               {
                                   // Response = ;
                                   // [ToastView showToastInParentView:self.window withText:[TRANXAPI_response valueForKey:@"response_msg"] withDuaration:2.0];
                               }
                               else
                               {
                                   NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
                                   NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                                   NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                                   
                                   
                                   page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
                                   appdelegate.pageno = [NSString stringWithFormat:@"%d",page+1];
                                   appdelegate.pagecount = [TRANXAPI_response valueForKey:@"PageCount"];
                                   NSLog(@"pagecount:%@",appdelegate.pagecount);
                                   appdelegate.lstpage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
                                   
                                   
                                   [[NSUserDefaults standardUserDefaults]setObject:appdelegate.lstpage forKey:@"lastPageStatus"];
                                   [[NSUserDefaults standardUserDefaults]setObject:appdelegate.pageno forKey:@"pageno"];
                                   
                                   NSLog(@"last page %@ %@",appdelegate.lstpage,appdelegate.pageno);
                                   
                                   //NSDictionary *attendeelistdata = [jsonData valueForKey:@"AttendeeList"];
                                   
                                   
                                   // [self insert_transaction_local:attendeelistdata entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Attendee List",DatatypeLoad] entityname:@"Attendee List" Status:@"Completed"];
                                   
                                   [self insertAttendeeList:[jsonData valueForKey:@"AttendeeList"]];
                                   
                                   [self insertAttendeePostion:[jsonData valueForKey:@"AttendeePositions"]];
                                   
                                   
                                   if([appdelegate.logout isEqualToString:@"YES"])//To prevent crash if user logs out and the thread is in process
                                   {
                                       appdelegate.lstpage=@"Y";
                                   }
                                   
                               }
                           }
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          
                                          appdelegate.downloadInProgress=@"NO";
                                          
                                      });
                   });
    
    
}





-(NSString *) ProcessEventSpeakerList
{
    
    
    NSString *process_result;
    
    //Pagination for SpeakerList
    appdelegate.speakerListLastPage=@"N";
    appdelegate.speakerListPageNo=@"1";
    appdelegate.speakerListPageCount=@"1";
    int page;
    
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTSpeaker_payload = [self create_trans_payload:@"" fltervalue:@""];
    NSDictionary *EVENTSpeaker_msgbody = [self create_trans_msgbody:EVENTSpeaker_payload txType:@"EMSGetSpeakerList" pageno:appdelegate.speakerListPageNo pagecount:appdelegate.speakerListPageCount lastpage:appdelegate.speakerListLastPage];
    
    
    
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTSpeaker_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
        appdelegate.speakerListPageNo = [NSString stringWithFormat:@"%d",page+1];
        appdelegate.speakerListPageCount = [TRANXAPI_response valueForKey:@"PageCount"];
        appdelegate.speakerListLastPage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
        
        
        
        
        NSMutableArray *Speakerlistdata = [jsonData valueForKey:@"SpeakerList"];
        [self insertspeakerdata:Speakerlistdata];
        
        NSMutableArray *SpeakerAttachmentlistdata = [jsonData valueForKey:@"AttachmentList"];
        [self insertspeakerattachmentdata:SpeakerAttachmentlistdata];
        
        
        
        [self insert_transaction_local:[jsonData valueForKey:@"SpeakerList"] entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Speaker Details",DatatypeLoad] entityname:@"Speaker List Details" Status:@"Completed"];
        
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.speakerListLastPage forKey:@"SpeakerListLastPage"];
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.speakerListPageNo forKey:@"SpeakerListPageNo"];
        
        
        
        if([appdelegate.speakerListLastPage isEqualToString:@"N"])
        {
            [self dispatchSpeakerList];
        }
        
    }
    NSLog(@"Process Completed: %@",process_result);
    return process_result;
}
-(void)insertspeakerattachmentdata:(NSMutableArray *)dict
{
    
    for(NSDictionary *item in dict)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
            
            [self delete_predicate:@"SpeakerAttachment" predicate:predicate];
        }
        
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"SpeakerAttachment" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
        [newrecord setValue:[item valueForKey:@"EntityID"] forKey:@"entityid"];
        [newrecord setValue:[item valueForKey:@"FileName"] forKey:@"filename"];
        [newrecord setValue:[item valueForKey:@"FileSize"] forKey:@"fileSize"];
        [newrecord setValue:[item valueForKey:@"FileType"] forKey:@"fileType"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Type"] forKey:@"type"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typeLIC"];
        [newrecord setValue:[item valueForKey:@"UploadDate"] forKey:@"uploaddate"];
        [newrecord setValue:[item valueForKey:@"StorageID"] forKey:@"storageID"];
        
        
        NSError *error;
        [context save:&error];
    }
    
}
-(void)dispatchSpeakerList //Background Thread for downloading Speakers
{
    appdelegate.speakerDownloadInProgress=@"YES";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       int page;
                       NSLog(@"last page:%@",appdelegate.speakerListLastPage);
                       
                       while ([appdelegate.speakerListLastPage isEqualToString:@"N"])
                       {
                           @autoreleasepool
                           {
                               NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
                               NSString *EVENTSpeaker_payload = [self create_trans_payload:@"" fltervalue:@""];
                               NSDictionary *EVENTSpeaker_msgbody = [self create_trans_msgbody:EVENTSpeaker_payload txType:@"EMSGetSpeakerList" pageno:appdelegate.speakerListPageNo pagecount:appdelegate.speakerListPageCount lastpage:appdelegate.speakerListLastPage];
                               
                               
                              
                               
                               NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTSpeaker_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
                               
                               if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
                               {
                                   // process_result = [TRANXAPI_response valueForKey:@"response_msg"];
                                   appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
                               }
                               else
                               {
                                   // process_result = @"Success";
                                   NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
                                   NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                                   NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                                   
                                   page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
                                   appdelegate.speakerListPageNo = [NSString stringWithFormat:@"%d",page+1];
                                   appdelegate.speakerListPageCount = [TRANXAPI_response valueForKey:@"PageCount"];
                                   appdelegate.speakerListLastPage = [self booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
                                   
                                   NSMutableArray *Speakerlistdata = [jsonData valueForKey:@"SpeakerList"];
                                   [self insertspeakerdata:Speakerlistdata];
                                   
                                   [self insert_transaction_local:[jsonData valueForKey:@"SpeakerList"] entity_id:[self generate_rowId] type:[NSString stringWithFormat:@"%@ for Speaker Details",DatatypeLoad] entityname:@"Speaker List Details" Status:@"Completed"];
                                   
                                   [[NSUserDefaults standardUserDefaults]setObject:appdelegate.speakerListLastPage forKey:@"SpeakerListLastPage"];
                                   [[NSUserDefaults standardUserDefaults]setObject:appdelegate.speakerListPageNo forKey:@"SpeakerListPageNo"];
                                   
                                   if([appdelegate.logout isEqualToString:@"YES"]) //To prevent crashing if user logs out and the thread is running
                                   {
                                       appdelegate.speakerListLastPage=@"Y";
                                   }
                                   
                                   
                               }
                           }
                       }
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          
                                          appdelegate.speakerDownloadInProgress=@"NO";
                                          NSString *GalleryFDRdownload = [self query_data:@"name" inputdata:@"GalleryFDRdownload" entityname:@"S_Config_Table"];
                                          if ([GalleryFDRdownload isEqualToString:@"YES"])
                                          {
                                              //[self imagesDownloader:_Speakerimages galleryimagearr:_Galleryimages resumeimages:_Resumeimages];
                                          }
                                          
                                      });
                   });
    
}

-(NSString *)ProcessEventStats
{
    
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTStats_payload = [self create_trans_payload:@"" fltervalue:@""];
    NSDictionary *EVENTStats_msgbody = [self create_trans_msgbody:EVENTStats_payload txType:@"EMSGetEventGraph" pageno:@"" pagecount:@"" lastpage:@""];
    
    
   
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTStats_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        NSLog(@"jsonData :%@",jsonData);
        
        [self delete_alldata:@"Event_Analytics"];
        NSMutableArray *PieGraphListdata = [jsonData valueForKey:@"PieGraphList"];
        
        for(NSDictionary *item in PieGraphListdata)
        {
            
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"ID"]];
                [self delete_predicate:@"Event_Analytics" predicate:predicate];
            }
            
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Analytics" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            [newrecord setValue:[item valueForKey:@"EntityName"] forKey:@"entityname"];
            [newrecord setValue:[item valueForKey:@"EventID"] forKey:@"eventid"];
            [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
            [newrecord setValue:[item valueForKey:@"Label"] forKey:@"label"];
            [newrecord setValue:[item valueForKey:@"LabelLIC"] forKey:@"labellic"];
            [newrecord setValue:[item valueForKey:@"Percent"] forKey:@"percent"];
            [newrecord setValue:[item valueForKey:@"Value"] forKey:@"value"];
            
            NSError *error;
            [context save:&error];
        }
    }
    return process_result;
}



-(void)InsertEKCalendarevents
{
    NSArray *EventData = [self query_alldata:@"Event"];
    for (int i=0; i<EventData.count; i++)
    {
        [self insertcal_event:[EventData[i] valueForKey:@"eventid"]];
    }
    
    NSArray *actData = [self query_alldata:@"Activities"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.visibility like %@) OR (SELF.visibility like %@)",@"MY",@"TEAM"];
    actData = [NSMutableArray arrayWithArray:[actData filteredArrayUsingPredicate:predicate]];
    
    for (int i=0; i<actData.count; i++)
    {
        [self insertcal_activity:[actData[i] valueForKey:@"activityid"]];
    }
    
}

-(void) UpdateEKCalendarevents
{
//    NSPredicate *calevent_refid_predicate = [NSPredicate predicateWithFormat:@"calevent_refid.length = 0"];
    
//    NSPredicate *calevent_refid_predicate = [NSPredicate predicateWithFormat:@"calevent_refid.length <= 0"];
    
    
  //  context = [appdelegate managedObjectContext];
    NSPredicate *calevent_refid_predicate = [NSPredicate predicateWithFormat:@"calevent_refid = nil"];
    
    NSArray *EventData = [self query_alldata:@"Event"];
    EventData = [EventData filteredArrayUsingPredicate:calevent_refid_predicate];
    
    for (int i=0; i<EventData.count; i++)
    {
        [self insertcal_event:[EventData[i] valueForKey:@"eventid"]];
    }
    
        NSArray *actData = [self query_alldata:@"Activities"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.visibility like %@) OR (SELF.visibility like %@)",@"MY",@"TEAM"];
        actData = [NSMutableArray arrayWithArray:[actData filteredArrayUsingPredicate:predicate]];
        for (int i=0; i<actData.count; i++)
        {
            [self insertcal_activity:[actData[i] valueForKey:@"activityid"]];
        }
}


-(NSString *) create_trans_payload:(NSString*)fltertype fltervalue:(NSString*)fltervalue
{
    NSString *jsonString ;
    NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];

    SLKeyChainStore  *keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    NSString *affiliate = [self query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"];
    
    
    [payload setValue:affiliate forKey:@"AffiliateID"];
    
    
    if (fltervalue.length >0)
    {
        [payload setValue:fltervalue forKey:@"EventID"];
    }
    
    if ([fltertype isEqualToString:@"UserName"])
    {
        [payload setValue:[keychain stringForKey:@"username"] forKey:@"UserName"];
        
    }
    
    else if ([fltertype isEqualToString:@"Entity"])
    {
        [payload setValue:@"M_ALL" forKey:@"Entity"];
    }
    else if ([fltertype isEqualToString:@"EXP_APPROVAL"])
    {
        [payload setValue:@"EXP_APPROVAL" forKey:@"Entity"];
        [payload setObject:appdelegate.ExpenseID forKey:@"ExpenseID"];
    }
    else if ([fltertype isEqualToString:@"EXPENSE"])
    {
        [payload setValue:@"EXPENSE" forKey:@"Entity"];
    }
    else if ([fltertype isEqualToString:@"EXP_AFTER_APPROVALS"])
    {
        [payload setValue:@"EXP_APPROVAL" forKey:@"Entity"];
    }
    
    if ([appdelegate.EventApprovals isEqualToString:@"YES"])
    {
        [payload setValue:appdelegate.eventid forKey:@"EventID"];
    }
    
    
    
    
    
    /* else if ([fltertype isEqualToString:@"RSHD"])
     {
     double newdate=[appdelegate.eventStartDate doubleValue] ;
     
     
     
     [payload setValue:appdelegate.eventid forKey:@"EventID"];
     [payload setValue:[NSString stringWithFormat:@"%@",@"20170418150502"]  forKey:@"EventStartDate"];
     [payload setValue:appdelegate.eventEndDate forKey:@"EventEndDate"];
     [payload setValue:@"SESSION_RSHD" forKey:@"Entity"];
     }*/
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
    
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    } else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    // NSLog(@"jsonString : %@",jsonString);
    NSString *key = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    
    return base64String;
}


-(NSString *)Create_ItinearyPayload:(NSString*)fltertype fltervalue:(NSString*)fltervalue lic:(NSString *)LIC
{
    NSString *jsonString ;
    NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
 
    SLKeyChainStore  *keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    NSString *affiliate = [self query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"];
    
    
    [payload setValue:affiliate forKey:@"AffiliateID"];
    
    
    if (fltervalue.length >0)
    {
        [payload setValue:fltervalue forKey:@"EventID"];
    }
    
    if ([fltertype isEqualToString:@"UserName"])
    {
        [payload setValue:[keychain stringForKey:@"username"] forKey:@"UserName"];
        
    }
    
    else if ([fltertype isEqualToString:@"Entity"])
    {
        [payload setValue:@"M_ALL" forKey:@"Entity"];
    }
    else if ([fltertype isEqualToString:@"EXP_APPROVAL"])
    {
        [payload setValue:@"EXP_APPROVAL" forKey:@"Entity"];
        [payload setObject:appdelegate.ExpenseID forKey:@"ExpenseID"];
    }
    else if ([fltertype isEqualToString:@"EXPENSE"])
    {
        [payload setValue:@"EXPENSE" forKey:@"Entity"];
    }
    else if ([fltertype isEqualToString:@"EXP_AFTER_APPROVALS"])
    {
        [payload setValue:@"EXP_APPROVAL" forKey:@"Entity"];
    }
    
    if ([appdelegate.EventApprovals isEqualToString:@"YES"])
    {
        [payload setValue:appdelegate.eventid forKey:@"EventID"];
    }
    
    if([LIC isEqualToString:@"ATTENDEE"])
    {
        [payload setValue:@"ATTENDEE" forKey:@"PartyTypeLIC"];
    }
    else if([LIC isEqualToString:@"SPEAKER"])
    {
        [payload setValue:@"SPEAKER" forKey:@"PartyTypeLIC"];
    }
    else if([LIC isEqualToString:@"TEAM"])
    {
        [payload setValue:@"TEAM" forKey:@"PartyTypeLIC"];
    }
    
    //    if([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
    //    {
    //       // [payload setValue:[self stringtobool:@"Y"] forKey:@"IsAdvanceSearch"];
    //    }
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
    
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    } else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    // NSLog(@"jsonString : %@",jsonString);
    NSString *key = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    
    return base64String;
}


-(NSDictionary *) create_trans_msgbody:(NSString *)payload txType:(NSString *)txType pageno:(NSString *)pageno pagecount:(NSString *)pagecount lastpage:(NSString *)lastpage
{

    SLKeyChainStore *keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    @try
    {
        
        NSString *appID = [self query_data:@"name" inputdata:@"applicationid" entityname:@"S_Config_Table"];
        NSString *LoginID = [keychain stringForKey:@"LoginID"];
        NSString *usercode = [keychain stringForKey:@"username"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *gmtZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmtZone];
        [dateFormatter setDateFormat:@"yyyyMMddhhmmssSSS"];
        
        NSMutableDictionary *messagebody = [[NSMutableDictionary alloc]init];
        
        NSMutableDictionary *msgbodypart1= [[NSMutableDictionary alloc] init];
        [msgbodypart1 setObject:usercode forKey:@"username"];
        [msgbodypart1 setObject:LoginID forKey:@"LoginID"];
        [msgbodypart1 setObject:txType forKey:@"txType"];
        [msgbodypart1 setObject:[[NSString stringWithFormat:@"%@#%@#%@#%@",usercode,appID,txType,[dateFormatter stringFromDate:[NSDate date]]]uppercaseString] forKey:@"messageId"];
        [msgbodypart1 setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timestamp"];
        [msgbodypart1 setObject:@"true" forKey:@"encrypted"];
        
        //    NSArray *userarr1 = [self query_alldata:@"User"];
        
        NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
        
        NSArray *userarr = [[self query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSString *synctime = [userarr[0] valueForKey:@"lastmobilesync"];
            [msgbodypart1 setObject:synctime forKey:@"LastSyncTime"];
            if([appdelegate.EventApprovals isEqualToString:@"YES"])
            {
                [msgbodypart1 setObject:appdelegate.eventid forKey:@"EventID"];
            }
            
        }
        
        
        if ([txType isEqualToString:@"EMSGetDeletedData"])
        {
            NSString *synctime = [userarr[0] valueForKey:@"lastmobilesync"];
            [msgbodypart1 setObject:synctime forKey:@"LastSyncTime"];
            
        }
        
        
        if (pageno.length >0)
        {
            [msgbodypart1 setObject:pageno forKey:@"Page"];
        }
        if (pagecount.length >0)
        {
            [msgbodypart1 setObject:pagecount forKey:@"PageCount"];
        }
        if (lastpage.length >0)
        {
            [msgbodypart1 setObject:lastpage forKey:@"LastPage"];
        }
        
        NSMutableDictionary *msgbodypart2= [[NSMutableDictionary alloc] init];
        [msgbodypart2 setObject:payload forKey:@"payload"];
        
        [messagebody setObject:msgbodypart1 forKey:@"reqHeader"];
        [messagebody setObject:msgbodypart2 forKey:@"reqBody"];
        
        return messagebody;
    }
    @catch(NSException *e)
    {
        [self removeWaitCursor];
        [self showalert:[NSString stringWithFormat:@"%@ %@",txType,[keychain stringForKey:@"username"]]  message:[NSString stringWithFormat:@"%@",e.reason] action:@"OK"];
        
    }
}
#pragma mark view move up method
-(void)MoveViewUp:(BOOL)Up Height:(int)Height ViewToBeMoved:(UIView *)View
{
    const int movementdistance = Height;
    const float movementduration = 0.3f;
    int movement = (Up ? -movementdistance : movementdistance);
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationDuration:movementduration];
    [UIView setAnimationBeginsFromCurrentState:true];
    View.frame = CGRectOffset(View.frame, 0, movement);
    [UIView commitAnimations];
    
}
#pragma mark getting event name method
-(NSString *)returneventname:(NSString *)eventid;
{
    NSString *eventname;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
 //   context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",eventid];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData.count <=0)
    {
        eventname = @"";
    }
    else
    {
        eventname = [[matchingData valueForKey:@"name"] componentsJoinedByString:@""];
    }
    return eventname;
}
#pragma mark formating date
-(NSString *)formatingdate:(NSString *)datestr datetime:(NSString *)datetimestr
{
    if ([datestr length]>0)
    {
        NSString *formateddatestr;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        if([datetimestr isEqualToString:@"datetime"])
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            NSDate *date = [dateFormatter dateFromString:datestr];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            formateddatestr = [dateFormatter stringFromDate:date];
            
        }
        else if ([datetimestr isEqualToString:@"date"])
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            NSDate *date = [dateFormatter dateFromString:datestr];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            formateddatestr = [dateFormatter stringFromDate:date];
            
        }
        else if ([datetimestr isEqualToString:@"Passportdate"])
        {
            [dateFormatter setDateFormat:@"yyyyMMdd"];
            NSDate *date = [dateFormatter dateFromString:datestr];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            formateddatestr = [dateFormatter stringFromDate:date];
            
        }
        else if ([datetimestr isEqualToString:@"FormatDate"])
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            NSDate *date = [dateFormatter dateFromString:datestr];
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            formateddatestr = [dateFormatter stringFromDate:date];
        }
        
        return formateddatestr;
    }
    else
    return nil;
}
-(NSString *)FormatCalendarDate:(NSDate *)Calendardate DateTime:(NSString *)DateTimeStr
{
    NSString *CalendarDate;
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    if ([DateTimeStr isEqualToString:@"Date"])
    {
        dateFormater.dateFormat = @"dd MMM yyyy";
    }
    else if ([DateTimeStr isEqualToString:@"Time"])
    {
        //[dateFormater setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
        dateFormater.dateFormat = @"dd MMM yyyy HH:mm";
    }
    
    CalendarDate = [dateFormater stringFromDate:Calendardate];
    return CalendarDate;
}
-(NSString *)converingformateddatetooriginaldate:(NSString *)formateddate datetype:(NSString *)datetypestr
{
    NSString *originaldate;
    NSDateFormatter *dateformatter;
    if ([datetypestr isEqualToString:@"PassportFormTillDate"])
    {
        dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"dd MM yyyyy"];
        NSDate *formated_date = [dateformatter dateFromString:formateddate];
        [dateformatter setDateFormat:@"yyyyMMdd"];
        originaldate = [dateformatter stringFromDate:formated_date];
    }
    else if ([datetypestr isEqualToString:@"ServerDate"])
    {
        dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"dd MMM yyyyy HH:mm"];
        [dateformatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *formated_date = [dateformatter dateFromString:formateddate];
        [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
        [dateformatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        originaldate = [dateformatter stringFromDate:formated_date];
    }
    else if([datetypestr isEqualToString:@"ExpenseDate"])
    {
        dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"dd MM yyyyy"];
        [dateformatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *formated_date = [dateformatter dateFromString:formateddate];
        [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
        [dateformatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        originaldate = [dateformatter stringFromDate:formated_date];
    }
    return originaldate;
}

-(NSDate *) getNSDate:(NSString *)datestr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    return [dateFormatter dateFromString:datestr];
}

-(NSString *)getNSDateStr
{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
    [dateformatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSString *datestr = [dateformatter stringFromDate:[NSDate date]];
    return datestr;
    
}
-(NSString *) stringtocurrency:(NSString *)inputstring
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    NSNumber *num = [numberFormatter numberFromString:inputstring];
    
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    // optional - [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setRoundingMode:NSNumberFormatterRoundDown];
    NSString *currency = [numberFormatter stringFromNumber:num];
    
    return currency;
    
    
}


-(NSString *) CurrencytoFloat:(NSString *)currency
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setRoundingMode:NSNumberFormatterRoundDown];
    
    //    NSString *currency = [numberFormatter stringFromNumber:num];
    
    
    
    
    
    
    
    //    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    //    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    currency = [currency stringByReplacingOccurrencesOfString:numberFormatter.internationalCurrencySymbol withString:@""];
    
    NSNumber *currnumber = [numberFormatter numberFromString:currency];
    //    NSLog(@"%@",[NSString stringWithFormat:@"%@",currnumber]);
    //    NSLog(@"%@",[NSString stringWithFormat:@"%.02f",currnumber.floatValue]);
    //    NSLog(@"%@",[NSString stringWithFormat:@"%.02f",currency.floatValue]);
    //
    
    
    
    
    if (currnumber != nil)
    {
        return [NSString stringWithFormat:@"%@",currnumber];
    }
    else
    {
        return [NSString stringWithFormat:@"%.02f", currency.floatValue];
    }
    
}



#pragma mark calculating duration
-(NSString *) geteventduration:(NSString *)sDate eDate:(NSString *)eDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    //[dateFormatter setDateFormat:@"yyyyMMddHHmm"];
    
    NSDate *startDate = [dateFormatter dateFromString:sDate];
    NSDate *endDate = [dateFormatter dateFromString:eDate];
    
    
    if(startDate && endDate)
    {
        if ([startDate timeIntervalSinceNow] > 0.0)
        {
            /* NSInteger numberofdays = [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:sDate];
             if (numberofdays == 0)
             {
             return @"Unscheduled";
             }
             else
             return [NSString stringWithFormat:@"%ld Days to go",(long)numberofdays];*/
            return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:sDate],[self gettranslation:@"LBL_789"]];
            
            //  Start date yet yo come
        }
        else if ([endDate timeIntervalSinceNow] > 0.0)
        {
            /* NSInteger numberofdays = [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:eDate];
             if (numberofdays == 0)
             {
             return @"Unscheduled";
             }
             else
             
             return [NSString stringWithFormat:@"%ld Days remaining",(long)numberofdays];*/
            return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:eDate],[self gettranslation:@"LBL_790"]];
            
            //Start date arrived but not ended yet
        }
        else
        {
            // Activity has passed the End Date
            /* NSInteger numberofdays = [self numberOfDaysBetween:eDate and:  [dateFormatter stringFromDate:[NSDate date]]];
             if (numberofdays == 0)
             {
             return @"Unscheduled";
             }
             else
             return [NSString stringWithFormat:@"%ld Days Overdue",(long)numberofdays];*/
            
            
            
            return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:eDate and:  [dateFormatter stringFromDate:[NSDate date]]],[self gettranslation:@"LBL_791"]];
        }
    }
    else
    return @"Unscheduled";
}
- (NSInteger)numberOfDaysBetween:(NSString *)sDate and:(NSString *)eDate
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *startDate = [f dateFromString:sDate];
    NSDate *endDate = [f dateFromString:eDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    //    NSString *returndays = [NSString stringWithFormat:@"%ld",[components day]];
    //    NSLog(@"returndays: %@",returndays);
    
    return [components day];
    
}
-(void)insertspeakerdata:(NSMutableArray *)dict
{
    
    for(NSDictionary *item in dict)
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",[item valueForKey:@"SpeakerID"]];
            [self delete_predicate:@"Speaker" predicate:predicate];
        }
        
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Speaker" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        
        [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
        [newrecord setValue:[item valueForKey:@"ChargePerHour"] forKey:@"chargeperhour"];
        [newrecord setValue:[item valueForKey:@"CVID"] forKey:@"cvid"];
        [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageicon"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"InternalFlag"] boolValue]] forKey:@"internalFlag"];
        [newrecord setValue:[item valueForKey:@"Name"] forKey:@"name"];
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"Created"] forKey:@"created"];
        [newrecord setValue:[item valueForKey:@"CreatedBy"] forKey:@"createdby"];
        [newrecord setValue:[item valueForKey:@"SpeakerID"] forKey:@"id"];
        //[newrecord setValue:[item valueForKey:@"Updated"] forKey:@"updated"];
        [newrecord setValue:[item valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
        
        [newrecord setValue:[item valueForKey:@"City"] forKey:@"city"];
        [newrecord setValue:[item valueForKey:@"Country"] forKey:@"country"];
        [newrecord setValue:[item valueForKey:@"FirstName"] forKey:@"firstname"];
        [newrecord setValue:[item valueForKey:@"LastName"] forKey:@"lastname"];
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"StreetAddress1"] forKey:@"streetaddress1"];
        [newrecord setValue:[item valueForKey:@"StreetAddress2"] forKey:@"streetaddress2"];
        [newrecord setValue:[item valueForKey:@"ZipCode"] forKey:@"zipcode"];
        [newrecord setValue:[item valueForKey:@"CityLIC"] forKey:@"citylic"];
        [newrecord setValue:[item valueForKey:@"CountryLIC"] forKey:@"countrylic"];
        [newrecord setValue:[item valueForKey:@"StateLIC"] forKey:@"statelic"];
        [newrecord setValue:[item valueForKey:@"State"] forKey:@"state"];
        [newrecord setValue:[item valueForKey:@"SalutationLIC"] forKey:@"salutationlic"];
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        
        [newrecord setValue:[item valueForKey:@"ExpenseCurrencyLIC"] forKey:@"currencylic"];
        [newrecord setValue:[item valueForKey:@"ConversionRate"] forKey:@"conversionRate"];
        //  [newrecord setValue:[item valueForKey:@"ChargePerHour"] forKey:@"chargeperhour"];
        [newrecord setValue:[item valueForKey:@"TotalAmount"] forKey:@"totalAmount"];
        
        [newrecord setValue:[item valueForKey:@"EmailAddress"] forKey:@"emailaddress"];
        [newrecord setValue:[item valueForKey:@"ContactNo"] forKey:@"contactno"];
        [newrecord setValue:[item valueForKey:@"Speciality"] forKey:@"speciality"];
        [newrecord setValue:[item valueForKey:@"Company"] forKey:@"company"];
        [newrecord setValue:[item valueForKey:@"Designation"] forKey:@"designation"];
        [newrecord setValue:[item valueForKey:@"Summary"] forKey:@"summary"];
        [newrecord setValue:[item valueForKey:@"PassportNumber"] forKey:@"passportnumber"];
        [newrecord setValue:[item valueForKey:@"PassportValidFrom"] forKey:@"passportvalidfrom"];
        [newrecord setValue:[item valueForKey:@"PassportValidTo"] forKey:@"passportvalidto"];
        [newrecord setValue:[item valueForKey:@"Department"] forKey:@"department"];
        [newrecord setValue:[item valueForKey:@"CVFileName"] forKey:@"cvFileName"];
        [newrecord setValue:[item valueForKey:@"CVFileType"] forKey:@"cvFileType"];
        NSError *error;
        [context save:&error];
        
        NSString *imageurl = [item valueForKey:@"ImageIcon"];
        if (imageurl.length > 0)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageurl"];
            [dict setValue:[item valueForKey:@"SpeakerID"] forKey:@"imageid"];
            
            NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Speaker"];
            NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.png",[item valueForKey:@"SpeakerID"]]]; //Add the file name
            
            [newrecord setValue:filePath forKey:@"location_url"];
            
            
            [_Speakerimages addObject:dict];
        }
        NSString *resumeurl = [item valueForKey:@"CVID"];
        if(resumeurl.length >0)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setValue:[item valueForKey:@"CVID"] forKey:@"resumeurl"];
            [dict setValue:[item valueForKey:@"SpeakerID"] forKey:@"resumeid"];
            [dict setValue:[item valueForKey:@"CVFileType"] forKey:@"cvFileType"];
            
            NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Resume"];
            NSString *fileextension = [item  valueForKey:@"CVFileType"];
            NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.%@",[item valueForKey:@"SpeakerID"],fileextension]]; //Add the file name
            
            [newrecord setValue:filePath forKey:@"resume_location_url"];
            
            [_Resumeimages addObject:dict];
        }
    }
    
}

//----------------------------Calendar Events----------------------------------------
#pragma Calendar Events

#pragma CALANDER FUNCTIONS

-(void) FDRCalander
{
    
    store = [[EKEventStore alloc] init];
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         NSLog(@"granted : %d",granted);
         if (granted)
         {
             [self deleteCalendar:@"EMS My Team Calendar"];
             [self deleteCalendar:@"EMS My Calendar"];
             
             [self deleteCalendar:@"IO Events My Team Calendar"];
             [self deleteCalendar:@"IO Events My Calendar"];
             
             [self InsertEKCalendarevents];
             
         }
         else
         {
             //             [ToastView showToastInParentView:self.view withText:@"Calander Access Denied" withDuaration:2.0];
         }
     }];
    
}


-(void) DeltaSyncCalander
{
    store = [[EKEventStore alloc] init];
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         NSLog(@"granted : %d",granted);
         if (granted)
         {
//             [self UpdateEKCalendarevents];
         }
         else
         {
             //             [ToastView showToastInParentView:self.view withText:@"Calander Access Denied" withDuaration:2.0];
         }
     }];
}




-(void)insertcal_event:(NSString*)eventid
{
    //    store = [[EKEventStore alloc] init];
    [self CreateCustomCalendar];
    NSString *mycalendar_identifier = [self query_data:@"name" inputdata:@"mycalendar_identifier" entityname:@"S_Config_Table"];
    NSString *teamcalendar_identifier = [self query_data:@"name" inputdata:@"teamcalendar_identifier" entityname:@"S_Config_Table"];
    
    
    
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         NSPredicate *eventpredicate = [NSPredicate predicateWithFormat:@"SELF.eventid like %@",eventid];
         NSArray *Eventdata = [[self query_alldata:@"Event"] filteredArrayUsingPredicate:eventpredicate];
         NSArray *venuedata = [[self query_alldata:@"Event_Venue"] filteredArrayUsingPredicate:eventpredicate];
         NSString*Address;
         NSString* lat;
         NSString* log;
         CLLocation * LocationAtual = [[CLLocation alloc]init];
         
         if(Eventdata.count>0)
         {
             NSString*calidentify = [Eventdata[0] valueForKey:@"calevent_refid"];
             NSLog(@"Event Already Exist at calidentify: %@",calidentify);
             
             if (calidentify.length >0)
             {
                 [self deletecal_event:calidentify];
                 [self update_calid:nil entityname:@"Event" entityid:eventid];
             }
             
             
             NSString* Notefield = [NSString stringWithFormat:@"\n\nDescription: %@ \n\nEvent Status : %@ \n\nOwner : %@ \n\nTotal Attendees : %@ \n\nApproved Attendees : %@ \n\nTotal Activities : %@ \n\nApproved Activities : %@",[Eventdata[0] valueForKey:@"desc"],[Eventdata[0] valueForKey:@"status"],[Eventdata[0] valueForKey:@"ownedby"],[Eventdata[0] valueForKey:@"atttotal"],[Eventdata[0] valueForKey:@"attapproved"],[Eventdata[0] valueForKey:@"acttotal"],[Eventdata[0] valueForKey:@"actcompleted"]];
             
             if (venuedata.count >0)
             {
                 NSString *addressstring;
                 
                 
                 if ([[venuedata[0] valueForKey:@"location"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@",[venuedata[0] valueForKey:@"location"]];
                 }
                 if ([[venuedata[0] valueForKey:@"address1"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"address1"]];
                 }
                 if ([[venuedata[0] valueForKey:@"address2"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"address2"]];
                 }
                 if ([[venuedata[0] valueForKey:@"address3"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"address3"]];
                 }
                 if ([[venuedata[0] valueForKey:@"city"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"city"]];
                 }
                 if ([[venuedata[0] valueForKey:@"state"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"state"]];
                 }
                 if ([[venuedata[0] valueForKey:@"country"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"country"]];
                 }
                 if ([[venuedata[0] valueForKey:@"zipcode"] length] >0)
                 {
                     addressstring = [NSString stringWithFormat:@"%@, %@",addressstring,[venuedata[0] valueForKey:@"zipcode"]];
                 }
                 
                 
                 Address = addressstring;
                 
                 //             Address = [NSString stringWithFormat:@"%@, %@ %@ %@, %@, %@, %@  %@ ",[venuedata[0] valueForKey:@"location"],[venuedata[0] valueForKey:@"address1"],[venuedata[0] valueForKey:@"address2"],[venuedata[0] valueForKey:@"address3"],[venuedata[0] valueForKey:@"city"],[venuedata[0] valueForKey:@"state"],[venuedata[0] valueForKey:@"country"],[venuedata[0] valueForKey:@"zipcode"]];
                 //             lat = [venuedata[0] valueForKey:@"latitude"];
                 //             log = [venuedata[0] valueForKey:@"longitude"];
                 
                 LocationAtual=[[CLLocation alloc] initWithLatitude:[[venuedata[0] valueForKey:@"latitude"] doubleValue] longitude:[[venuedata[0] valueForKey:@"longitude"] doubleValue]];
                 
             }
             
             NSString* visibility = [Eventdata[0] valueForKey:@"visibility"];
             
             
             if (!granted) { return; }
             EKEvent *event = [EKEvent eventWithEventStore:store];
             EKAlarm *dayalarm = [EKAlarm alarmWithRelativeOffset:-86400]; // 1 Day
             
             event.title = [Eventdata[0] valueForKey:@"name"];
             event.startDate = [self getNSDate:[Eventdata[0] valueForKey:@"startdate"]];
             event.endDate = [self getNSDate:[Eventdata[0] valueForKey:@"enddate"]];
             event.allDay = YES;
             [event addAlarm:dayalarm];
             event.notes = Notefield;
             
             if ([visibility isEqualToString:@"MY"])
             {
                 event.calendar = [store calendarWithIdentifier:mycalendar_identifier];
             }
             else if ([visibility isEqualToString:@"TEAM"])
             {
                 event.calendar = [store calendarWithIdentifier:teamcalendar_identifier];
             }
             //         else
             //         {
             //             NSLog(@"This EVENT is Added to the Default Calendar:%@",[Eventdata[0] valueForKey:@"name"]);
             //             event.calendar = [store defaultCalendarForNewEvents];
             //         }
             
             
             if (lat.length > 0 && log.length >0)
             {
                 EKStructuredLocation* structuredLocation = [EKStructuredLocation locationWithTitle:Address];
                 CLLocation* location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[log doubleValue]];
                 structuredLocation.geoLocation = location;
                 
                 [event setValue:structuredLocation forKey:@"structuredLocation"];
             }
             else
             {
                 [event setLocation:Address];
             }
             
             //         [event setLocation:Address];
             
             NSError *err = nil;
             [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
             [self update_calid:event.eventIdentifier entityname:@"Event" entityid:eventid];
         }
         dispatch_semaphore_signal(sem);
         
     }];
    while (dispatch_semaphore_wait(sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:3]]; }
}

-(void)deletecal_event:(NSString*)calevent_refid
{
    
            store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             EKEvent* eventToRemove = [store eventWithIdentifier:calevent_refid];
             if (eventToRemove)
             {
                 NSError* error = nil;
                 [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&error];
             };
         }
         
     }];
    
}

-(void)insertcal_activity:(NSString*)activityid
{
    store = [[EKEventStore alloc] init];
    [self CreateCustomCalendar];
    NSString *mycalendar_identifier = [self query_data:@"name" inputdata:@"mycalendar_identifier" entityname:@"S_Config_Table"];;
    NSString *teamcalendar_identifier = [self query_data:@"name" inputdata:@"teamcalendar_identifier" entityname:@"S_Config_Table"];;
    
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         NSPredicate *eventpredicate = [NSPredicate predicateWithFormat:@"SELF.activityid like %@",activityid];
         NSArray *activitydata = [[self query_alldata:@"Activities"] filteredArrayUsingPredicate:eventpredicate];
         
         if(activitydata.count>0)
         {
             NSString* visibility = [activitydata[0] valueForKey:@"visibility"];
             
             NSString*calidentify = [activitydata[0] valueForKey:@"calevent_refid"];
             
             if (calidentify.length >0)
             {
                 [self deletecal_event:calidentify];
                 [self update_calid:nil entityname:@"Event" entityid:activityid];
             }
             
            
                 NSString* Notefield = [NSString stringWithFormat:@"\n\nOwner: %@ \n\nStatus : %@ \n\nPriority : %@ \n\nEnd Date : %@ \n\n",[activitydata[0] valueForKey:@"ownedby"],[activitydata[0] valueForKey:@"status"],[activitydata[0] valueForKey:@"priority"],[self formatingdate:[activitydata[0] valueForKey:@"enddate"] datetime:@"datetime"]];
             
                 
             
             if (!granted) { return; }
             EKEvent *event = [EKEvent eventWithEventStore:store];
             EKAlarm *dayalarm = [EKAlarm alarmWithRelativeOffset:-3600]; // 1 hour
             
//             event.title = [NSString stringWithFormat:@"%@ - %@",[activitydata[0] valueForKey:@"type"],[activitydata[0] valueForKey:@"eventname"]];
             event.title  = [activitydata[0] valueForKey:@"type"];
             event.location = [activitydata[0] valueForKey:@"desc"];
             event.startDate = [self getNSDate:[activitydata[0] valueForKey:@"startdate"]];
             event.endDate = [self getNSDate:[activitydata[0] valueForKey:@"enddate"]];
             //         event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
             if ([visibility isEqualToString:@"MY"])
             {
                 event.calendar = [store calendarWithIdentifier:mycalendar_identifier];
             }
             else if ([visibility isEqualToString:@"TEAM"])
             {
                 event.calendar = [store calendarWithIdentifier:teamcalendar_identifier];
             }
             //         else
             //         {
             //             NSLog(@"This Activity is Added to the Default Calendar:%@ - %@",[activitydata[0] valueForKey:@"eventname"],[activitydata[0] valueForKey:@"type"]);
             //             event.calendar = [store defaultCalendarForNewEvents];
             //         }
             
             event.allDay = NO;
             [event addAlarm:dayalarm];
             event.notes = Notefield;
             
             
             
             NSError *err = nil;
             [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
             [self update_calid:event.eventIdentifier entityname:@"Activities" entityid:activityid];
         }
         dispatch_semaphore_signal(sem);
     }];
    while (dispatch_semaphore_wait(sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:3]]; }
    
}

-(void)CreateCustomCalendar
{
    //    store = [[EKEventStore alloc] init];
    
    EKSource *localSource;
    EKSource *defaultSource = [store defaultCalendarForNewEvents].source;
    
    if (defaultSource.sourceType == EKSourceTypeCalDAV)
    NSLog(@"iCloud Enable");
    else
    NSLog(@"iCloud Disable");
    
    
    for (EKSource *source in store.sources)
    {
        if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"])
        {
            localSource = source;
            break;
        }
    }
    
    if (localSource == nil)
    {
        for (EKSource *source in store.sources)
        {
            if (source.sourceType == EKSourceTypeLocal)
            {
                localSource = source;
                break;
            }
        }
    }
    
    
    
    
    if ([self checkForCalendar:@"IO Events My Team Calendar"] == NO)
    {
        EKCalendar *teamCalendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:store];
        teamCalendar.source = localSource;
        teamCalendar.title = @"IO Events My Team Calendar";
        //teamCalendar.CGColor = [UIColor colorWithRed:15/255.0 green:72/255.0 blue:138/255.0 alpha:1.0].CGColor;
        teamCalendar.CGColor = [self DarkBlueColour].CGColor;
        
        NSError *errorCalendar;
        [store saveCalendar:teamCalendar commit:YES error:&errorCalendar];
        NSLog(@"teamcalendar_identifier: %@",teamCalendar.calendarIdentifier);
        [self update_data:@"teamcalendar_identifier" inputdata:teamCalendar.calendarIdentifier entityname:@"S_Config_Table"];
        
        if (errorCalendar)
        {
            NSLog(@"ERROR CREATING CALENDAR: %@",errorCalendar);
        }
    }
    
    //if ([self checkForCalendar:@"EMS My Calendar"] == NO)
    if ([self checkForCalendar:@"IO Events My Calendar"] == NO)
    {
        EKCalendar *myCalendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:store];
        myCalendar.source = localSource;
        // myCalendar.title = @"EMS My Calendar";
        
        myCalendar.title = @"IO Events My Calendar";
        //myCalendar.CGColor = [UIColor colorWithRed:81/255.0 green:120/255.0 blue:164/255.0 alpha:1.0].CGColor;
        myCalendar.CGColor = [self LightBlueColour].CGColor;
        NSError *errorCalendar;
        [store saveCalendar:myCalendar commit:YES error:&errorCalendar];
        NSLog(@"mycalendar_identifier: %@",myCalendar.calendarIdentifier);
        [self update_data:@"mycalendar_identifier" inputdata:myCalendar.calendarIdentifier entityname:@"S_Config_Table"];
        
        if (errorCalendar)
        {
            NSLog(@"ERROR CREATING CALENDAR: %@",errorCalendar);
        }
        
    }
    
}


-(BOOL)checkForCalendar:(NSString *)CalName
{
    //    store = [[EKEventStore alloc] init];
    NSArray *calendarArray = [store calendarsForEntityType:EKEntityTypeEvent];
    EKCalendar *cal;
    
    for (int x = 0; x < [calendarArray count]; x++)
    {
        
        cal = [calendarArray objectAtIndex:x];
        NSString *calTitle = [cal title];
        
        // if the calendar is found, return YES
        if ([calTitle isEqualToString:CalName])
        {
            return YES;
        }
    }
    return NO;
}

-(void)showcallist
{
    //    store = [[EKEventStore alloc] init];
    NSArray *calendarArray = [store calendarsForEntityType:EKEntityTypeEvent];
    EKCalendar *cal;
    
    for (int x = 0; x < [calendarArray count]; x++)
    {
        
        cal = [calendarArray objectAtIndex:x];
        NSString *calTitle = [cal title];
        NSLog(@"Available Calenders in Store: %@",calTitle);
    }
    
}

-(void) deleteCalendar:(NSString *)titlename
{
    store = [[EKEventStore alloc] init];
    
    NSArray *calendarArray = [store calendarsForEntityType:EKEntityTypeEvent];
    
    for (EKCalendar *Calendar in calendarArray)
    {
        
        if ([Calendar.title isEqualToString:titlename])
        {
            NSError *err;
            [store removeCalendar:Calendar commit:YES error:&err];
            
            if (!err)
            {
                NSLog(@"deleted %@", Calendar.title);
                
            }
            else
            {
                NSLog(@"Error While Deleting Calendar:  %@", err.localizedDescription);
            }
            
        }
        
    }
    
    
    
    /*
     NSString *mycalendar_identifier = [self query_data:@"name" inputdata:@"mycalendar_identifier" entityname:@"S_Config_Table"];;
     NSString *teamcalendar_identifier = [self query_data:@"name" inputdata:@"teamcalendar_identifier" entityname:@"S_Config_Table"];;
     
     if ([mycalendar_identifier isEqualToString:@"No Matching Data Found"] || [teamcalendar_identifier isEqualToString:@"No Matching Data Found"])
     {
     NSArray *calendarArray = [store calendarsForEntityType:EKEntityTypeEvent];
     
     for (EKCalendar *Calendar in calendarArray)
     {
     
     if ([Calendar.title isEqualToString:titlename])
     {
     
     NSError *err;
     
     [store removeCalendar:Calendar commit:YES error:&err];
     
     if (!err)
     {
     NSLog(@"deleted %@", Calendar.title);
     
     }
     else
     {
     NSLog(@"Error While Deleting Calendar:  %@", err.localizedDescription);
     }
     
     }
     
     }
     
     
     }
     else
     {
     EKCalendar *teamCalendar = [store calendarWithIdentifier:teamcalendar_identifier];
     EKCalendar *myCalendar = [store calendarWithIdentifier:mycalendar_identifier];
     
     NSError *terr;
     [store removeCalendar:teamCalendar commit:YES error:&terr];
     
     if (!terr)
     {
     NSLog(@"teamCalendar deleted ");
     
     }
     else
     {
     NSLog(@"Error While Deleting Calendar:  %@", terr.localizedDescription);
     }
     
     
     NSError *merr;
     [store removeCalendar:myCalendar commit:YES error:&merr];
     
     if (!merr)
     {
     NSLog(@"myCalendar deleted ");
     
     }
     else
     {
     NSLog(@"Error While Deleting Calendar:  %@", merr.localizedDescription);
     }
     }
     
     NSArray *calendarArray = [store calendarsForEntityType:EKEntityTypeEvent];
     
     for (EKCalendar *Calendar in calendarArray){
     
     if ([Calendar.title isEqualToString:titlename])
     {
     
     NSError *err;
     
     [store removeCalendar:Calendar commit:YES error:&err];
     
     if (!err)
     {
     NSLog(@"deleted %@", Calendar.title);
     
     }
     else
     {
     NSLog(@"Error While Deleting Calendar:  %@", err.localizedDescription);
     }
     
     }
     
     }
     */
}


/*
 -(void)insert_calevents
 {
 EKEventStore *store = [EKEventStore new];
 
 [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
 {
 if (!granted) { return; }
 EKEvent *event = [EKEvent eventWithEventStore:store];
 event.title = @"iWizards Team Meeting: We need to Discuss the progress of NN EMS Project";
 event.notes = @"We need to Discuss the progress of NN EMS Project \n Comments:";
 event.startDate = [NSDate date]; //today
 event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
 event.calendar = [store defaultCalendarForNewEvents];
 event.allDay = NO;
 
 EKStructuredLocation* structuredLocation = [EKStructuredLocation locationWithTitle:@"40, Promenade Rd, Sindhi Colony, Pulikeshi Nagar, Bengaluru, Karnataka 560005"]; // locationWithTitle has the same behavior as event.location
 CLLocation* location = [[CLLocation alloc] initWithLatitude:51.5032520 longitude:-0.1278990];
 structuredLocation.geoLocation = location;
 
 [event setValue:structuredLocation forKey:@"structuredLocation"];
 
 
 
 //        [event setLocation:@"40, Promenade Rd, Sindhi Colony, Pulikeshi Nagar, Bengaluru, Karnataka 560005"];
 NSError *err = nil;
 [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
 NSLog(@"Event Added to the database: %@",event.eventIdentifier);
 
 
 //        self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
 }];
 
 //    return [NSString stringWithFormat:@"%@",event.eventIdentifier] ;
 
 }
 
 -(void)remove_calevents:(NSString *)EventId
 {
 EKEventStore* store = [EKEventStore new];
 [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
 if (!granted) { return; }
 EKEvent* eventToRemove = [store eventWithIdentifier:EventId];
 if (eventToRemove) {
 NSError* error = nil;
 [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&error];
 }
 }];
 
 }
 */
//----------------------------Calendar Events----------------------------------------








-(NSDate *) get_midweek_date:(NSDate *)thedate
{
    //    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *nowComponents = [gregorian components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:thedate];
    
    [nowComponents setWeekday:1]; //Monday = 2
    [nowComponents  setWeekOfYear: [nowComponents weekOfYear]]; //Next week
    [nowComponents setHour:8]; //8a.m.
    [nowComponents setMinute:0];
    [nowComponents setSecond:0];
    
    return [gregorian dateFromComponents:nowComponents];
    
}
#pragma mark showing and removing cursor
-(void)showWaitCursor:(NSString *)txtToShow
{
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    currentTopVC = [self currentTopViewController];
    appdelegate.activityView = [self getActivityIndicator];
    appdelegate.loadingView = [self getWaitCursorView:txtToShow activityIndicator:appdelegate.activityView parentView:currentTopVC.view];
    [self showWaitCursor:currentTopVC.view waitCursorView:appdelegate.loadingView activityIndicator:appdelegate.activityView];
}
-(void)removeWaitCursor
{
    [self removeWaitCursorView:appdelegate.loadingView activityIndicator:appdelegate.activityView];
}
-(UIActivityIndicatorView *) getActivityIndicator
{
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.frame = CGRectMake(90, 40, activityView.bounds.size.width, activityView.bounds.size.height);
    activityView.color = [UIColor blackColor];
    return activityView;
}

// Activity indicator method
-(UIView*) getWaitCursorView:(NSString*) textToShow activityIndicator:(UIActivityIndicatorView *)activityView parentView:(UIView *) parent
{
    UIView *loadingView;
    
    loadingView= [[UIView alloc] initWithFrame:CGRectMake(402, 314, 220, 140)];
    
    // loadingView.backgroundColor = [UIColor colorWithRed:210/255.0 green:235/255.0 blue:250/255.0 alpha:0.9];
    loadingView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:86.0/255.0 alpha:0.8];
    loadingView.clipsToBounds = YES;
    loadingView.layer.cornerRadius = 10.0;
    activityView.color = [UIColor whiteColor];
    [loadingView addSubview:activityView];
    
    // UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 100, 140, 22)];
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 220, 22)];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textColor = [UIColor whiteColor];
    loadingLabel.adjustsFontSizeToFitWidth = YES;
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
    loadingLabel.text = textToShow;
    [loadingView addSubview:loadingLabel];
    
    return loadingView;
}
-(void) showWaitCursor:(UIView *) parentView waitCursorView:(UIView *) childView activityIndicator:(UIActivityIndicatorView *)activityView
{
    [parentView addSubview:childView];
    [activityView startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}
-(void) removeWaitCursorView:(UIView *) waitCursorView activityIndicator:(UIActivityIndicatorView *)activityView
{
    [activityView stopAnimating];
    [waitCursorView removeFromSuperview];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}
#pragma mark topviewcontroller
- (UIViewController *)currentTopViewController
{
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}
#pragma mark generate row id
-(NSString *) generate_rowId
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *generatestr = [NSMutableString stringWithCapacity:15];
    for (NSUInteger i= 0U; i<10; i++)
    {
        u_int32_t random = arc4random() % [letters length];
        unichar character = [letters characterAtIndex:random];
        [generatestr appendFormat:@"%C",character];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *gmtZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:[[NSTimeZone localTimeZone] abbreviation]]];
    [dateFormatter setTimeZone:gmtZone];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    return [NSString stringWithFormat:@"D-%@-%@",[dateFormatter stringFromDate:[NSDate date]],generatestr];
}

#pragma mark Transaction Queue methods

-(void) insert_transaction_local:(NSDictionary*)transaction_message entity_id:(NSString *)entity_id type:(NSString *)type entityname:(NSString *)entityname Status:(NSString *)Status
{
    if (transaction_message.count <=0 )
    {
        NSLog(@"No Data Found for Entity %@",entityname);
    }
    else
    {
       // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        //context = [appdelegate managedObjectContext];
        
        NSString *transaction_id = [self generate_rowId];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        //[formatter setDateFormat:@"dd MMM yyyy hh:mm a"];;
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        //        [formatter setDateStyle:NSDateFormatterMediumStyle];
        NSString *dateToday = [formatter stringFromDate:[NSDate date]];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:transaction_message options:0 error:&err];
        NSString * trns_string = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Transaction_Queue" inManagedObjectContext:context];
        NSManagedObject *newtransaction = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newtransaction setValue:transaction_id forKey:@"transaction_id"];
        [newtransaction setValue:trns_string forKey:@"transaction_message"];
        [newtransaction setValue:dateToday forKey:@"transaction_date"];
        [newtransaction setValue:Status forKey:@"transaction_status"];
        [newtransaction setValue:entity_id forKey:@"transaction_entityId"];
        [newtransaction setValue:type forKey:@"transaction_type"];
        [newtransaction setValue:entityname forKey:@"transaction_entityname"];
        
        if ([Status isEqualToString:@"Completed"])
        {
            [newtransaction setValue:@"Your Transaction has been successfully processed" forKey:@"transaction_resp_message"];
        }
        
        NSError *error;
        [context save:&error];
    }
    
    //    NSLog(@"record inserted into local table ");
}

-(NSArray *) send_transaction_server
{
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    NSMutableArray *error_records = [[NSMutableArray alloc] init];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Transaction_Queue" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transaction_status like 'Open'"];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *offlinedata = [context executeFetchRequest:request error:&error];
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    if (offlinedata.count <=0)
    {
        //        [self printloginconsole:@"6" logtext:@"No Data Found in Database to be sent to server"];
        dispatch_semaphore_signal(sem);
    }
    else
    {
        NSString *t_id;
        NSString *t_message;
        NSString *t_date;
        NSString *t_status;
        NSString *t_entityid;
        
        
        for (NSManagedObject *obj in offlinedata)
        {
            t_id = [obj valueForKey:@"transaction_id"];
            t_message = [obj valueForKey:@"transaction_message"];
            t_date = [obj valueForKey:@"transaction_date"];
            t_status = [obj valueForKey:@"transaction_status"];
            t_entityid = [obj valueForKey:@"transaction_entityId"];
            
            [obj setValue:@"Queued" forKey:@"transaction_status"];
            
            NSError * err;
            NSData *data =[t_message dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *msgbody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            
            NSMutableDictionary *upsertdataAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
            
            if (![[upsertdataAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
            {
                [obj setValue: @"Failed" forKey:@"transaction_status"];
                [obj setValue:[NSString stringWithFormat:@"%@ - %@",[upsertdataAPI_response valueForKey:@"response_code"],[upsertdataAPI_response valueForKey:@"response_msg"]] forKey:@"transaction_resp_message"];
                [context save:&error];
                
            }
            else
            {
                [obj setValue:@"Completed" forKey:@"transaction_status"];
                [obj setValue:[NSString stringWithFormat:@"%@ - Your Transaction has been successfully processed",[upsertdataAPI_response valueForKey:@"response_code"]] forKey:@"transaction_resp_message"];
                [context save:&error];
                
            }
        }
        dispatch_semaphore_signal(sem);
    }
    while (dispatch_semaphore_wait(sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    return error_records;
}




-(void)printloginconsole:(NSString *)localloglevel logtext:(NSString *)logtext
{
    NSString  *apploglevel = [[NSUserDefaults standardUserDefaults]valueForKey:@"logleveldefault"];
    
    if([apploglevel intValue] >= [localloglevel intValue])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:[[NSTimeZone localTimeZone] abbreviation]]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss.SSS"];
        
        NSString *Currentdate = [dateFormatter stringFromDate:[NSDate date]];
        NSString *errormsg = [[NSString alloc] init];
        
        switch ([localloglevel intValue])
        {
            case 0:
                errormsg = [NSString stringWithFormat:@"%@ - Emergency - %@ \n",Currentdate,logtext];
                break;
            case 1:
                errormsg = [NSString stringWithFormat:@"%@ - Alert - %@ \n",Currentdate,logtext];
                break;
            case 2:
                errormsg = [NSString stringWithFormat:@"%@ - Critical - %@ \n",Currentdate,logtext];
                break;
            case 3:
                errormsg = [NSString stringWithFormat:@"%@ - Error - %@ \n",Currentdate,logtext];
                break;
            case 4:
                errormsg = [NSString stringWithFormat:@"%@ - Warning - %@ \n",Currentdate,logtext];
                break;
            case 5:
                errormsg = [NSString stringWithFormat:@"%@ - Notice - %@ \n",Currentdate,logtext];
                break;
            case 6:
                errormsg = [NSString stringWithFormat:@"%@ - Info - %@ \n",Currentdate,logtext];
                break;
            case 7:
                errormsg = [NSString stringWithFormat:@"%@ - Debug - %@ \n",Currentdate,logtext];
                break;
            default:
                errormsg = [NSString stringWithFormat:@"%@ - ISSUE - %@ \n",Currentdate,logtext];
                break;
        }
        NSLog(@"%@",errormsg);
    }
}

#pragma mark Download all Images
-(void)imagesDownloader:(NSMutableArray *)speakerimagearr galleryimagearr:(NSMutableArray *)galleryimagearr resumeimages:(NSMutableArray *)resumeimagearr
{
    
    NSString *downloadtype;
    if([speakerimagearr count]>0 || [resumeimagearr count] >0)
    {
        downloadtype = @"Speaker Images and Resume Files";
    }
    else if ([galleryimagearr count]>0)
    {
        downloadtype = @"Gallery Image";
    }
    /*else if([resumeimagearr count] >0)
     {
     downloadtype = @"Resume Files";
     }*/
    else
    {
        downloadtype = @"";
    }
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [networkReachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        NSLog(@"No internet connection ");
    }
    else
    {
        appdelegate.fdrstr = @"Y";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           dispatch_semaphore_t sem;
                           sem = dispatch_semaphore_create(0);
                           NSError *error;
                           @autoreleasepool {
                               
                               if (speakerimagearr.count > 0)
                               {
                                   
                                   NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Speaker"];
                                   
                                   if(![[NSFileManager defaultManager]fileExistsAtPath:documentsPath])
                                   [[NSFileManager defaultManager]createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
                                   for(NSDictionary *item in speakerimagearr)
                                   {
                                       NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.png",[item valueForKey:@"imageid"]]]; //Add the file name
                                       NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[item valueForKey:@"imageurl"]]];
                                       if ( curlData )
                                       {
                                           [curlData writeToFile:filePath atomically:YES];
                                           //  [self update_imageurl:filePath entityname:@"Speaker" entityid:[item valueForKey:@"imageid"]];
                                       }
                                       else
                                       {
                                           [self printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download Attachment: %@",[item valueForKey:@"imageurl"]]] ;
                                       }
                                       
                                   }
                                   
                                   [speakerimagearr removeAllObjects];
                                   dispatch_semaphore_signal(sem);
                               }
                               else
                               {
                                   dispatch_semaphore_signal(sem);
                               }
                               
                               if (galleryimagearr.count > 0)
                               {
                                   
                                   appdelegate.downloadinggalleryimages = @"Y";
                                   NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Galleryimages"];
                                   if(![[NSFileManager defaultManager]fileExistsAtPath:documentsPath])
                                   [[NSFileManager defaultManager]createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
                                   for(NSDictionary *item in galleryimagearr)
                                   {
                                       NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.png",[item valueForKey:@"imageid"]]]; //Add the file name
                                       NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[item valueForKey:@"imageurl"]]];
                                       if ( curlData )
                                       {
                                           [curlData writeToFile:filePath atomically:YES];
                                           
                                           [self update_imageurl:filePath entityname:@"Event_Gallery" entityid:[item valueForKey:@"imageid"]];
                                       }
                                       else
                                       {
                                           [self printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download Attachment: %@",[item valueForKey:@"imageurl"]]] ;
                                       }
                                   }
                                   
                                   [galleryimagearr removeAllObjects];
                                   
                                   
                                   dispatch_semaphore_signal(sem);
                                   
                               }
                               else
                               {
                                   
                                   dispatch_semaphore_signal(sem);
                                   
                               }
                               if(resumeimagearr.count >0)
                               {
                                   NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Resume"];
                                   
                                   if(![[NSFileManager defaultManager]fileExistsAtPath:documentsPath])
                                   [[NSFileManager defaultManager]createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
                                   for(NSDictionary *item in resumeimagearr)
                                   {
                                       NSString *fileextension = [item  valueForKey:@"cvFileType"];
                                       NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.%@",[item valueForKey:@"resumeid"],fileextension]]; //Add the file name
                                       NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[item valueForKey:@"resumeurl"]]];
                                       if ( curlData )
                                       {
                                           [curlData writeToFile:filePath atomically:YES];
                                           //                                       if ([item valueForKey:@"resumeid"])
                                           //                                       {
                                           //                                           [self update_resumeurl:filePath entityname:@"Speaker" entityid:[item valueForKey:@"resumeid"]];
                                           //                                       }
                                           
                                       }
                                       else
                                       {
                                           [self printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download Attachment: %@",[item valueForKey:@"resumeurl"]]] ;
                                       }
                                       
                                   }//for loop end
                                   
                                   [resumeimagearr removeAllObjects];
                                   dispatch_semaphore_signal(sem);
                               }
                               else
                               {
                                   dispatch_semaphore_signal(sem);
                               }
                           }
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              while (dispatch_semaphore_wait(sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
                                              currentTopVC = [self currentTopViewController];
                                              if ([downloadtype isEqualToString:@"Gallery Image"])
                                              {
                                                  appdelegate.downloadinggalleryimages = @"N";
                                              }
                                              
                                              
                                              if ([downloadtype length]>0)
                                              {
                                                  // [ToastView showToastInParentView:currentTopVC.view withText:[NSString stringWithFormat:@"%@ Download Completed",downloadtype] withDuaration:2.0];
                                              }
                                              
                                              if([appdelegate.currentscreen isEqualToString:@"Speaker"])
                                              {
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"YourNotificationName"
                                                                                                      object:nil];
                                                  
                                              }
                                              [_Speakerimages removeAllObjects];
                                              [_Galleryimages removeAllObjects];
                                              [_Resumeimages removeAllObjects];
                                              appdelegate.fdrstr = @"N";
                                              //if([appdelegate.currentscreen isEqualToString:@"Speaker"])
                                              //{
                                              // [[NSNotificationCenter defaultCenter] removeObserver:self];
                                              //}
                                              
                                              
                                          });
                       });
    }
}

#pragma mark show alert
-(void)showalert:(NSString *)titlestr message:(NSString *)messagestr action:(NSString *)actionstr
{
    currentTopVC = [self currentTopViewController];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:titlestr message:messagestr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:actionstr style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okaction];
    [currentTopVC presentViewController:alert animated:NO completion:nil];
    
}

//-(void)showalert2action:(NSString *)titlestr message:(NSString *)messagestr action1:(NSString *)action1str action2:(NSString *)action2str
//{
//    currentTopVC = [self currentTopViewController];
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:titlestr message:messagestr preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *okaction = [UIAlertAction actionWithTitle:action1str style:UIAlertActionStyleDefault handler:nil];
//    [alert addAction:okaction];
//
//    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:action2str style:UIAlertActionStyleDefault handler:nil];
//    [alert addAction:cancelaction];
//
//    [currentTopVC presentViewController:alert animated:NO completion:nil];
//
//}
-(void)addEventAttendee:(NSString*)event_id EventAttendeedata:(NSMutableArray *)EventAttendeedata
{
    for(NSDictionary *item in EventAttendeedata)
    {
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"attendeeid like %@",[item valueForKey:@"AttendeeID"]];
            [self delete_predicate:@"Event_Attendee" predicate:predicate];
        }
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Attendee" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"Approver"] forKey:@"approver"];
        [newrecord setValue:event_id forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        //GROUP CHANGE
        [newrecord setValue:[item valueForKey:@"GroupID"]?:@"" forKey:@"groupID"];
        [newrecord setValue:[item valueForKey:@"FirstName"] forKey:@"firstname"];
        [newrecord setValue:[item valueForKey:@"LastName"] forKey:@"lastname"];
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"SalutationLIC"] forKey:@"salutationlic"];
        [newrecord setValue:[item valueForKey:@"Speciality"] forKey:@"speciality"];
        [newrecord setValue:[item valueForKey:@"SpecialityLIC"] forKey:@"specialitylic"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[item valueForKey:@"TargetClass"] forKey:@"targetclass"];
        [newrecord setValue:[item valueForKey:@"TargetClassLIC"] forKey:@"targetclasslic"];
        [newrecord setValue:[item valueForKey:@"SignatureID"] forKey:@"signatureid"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"SignFlag"] boolValue]] forKey:@"signflag"];
        
        [newrecord setValue:[item valueForKey:@"IntegrationSource"] forKey:@"integrationsource"];
        [newrecord setValue:[item valueForKey:@"SignatureID"] forKey:@"signatureid"];
        
        [newrecord setValue:[item valueForKey:@"ContactNumber"] forKey:@"contactnumber"];
        [newrecord setValue:[item valueForKey:@"EmailAddress"] forKey:@"emailaddress"];
        
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanDelete"] boolValue] ] forKey:@"canDelete"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"CanEdit"] boolValue] ]forKey:@"canEdit"];
        
        [newrecord setValue:[self booltostring:[[item valueForKey:@"ApprovalFlag"] boolValue]] forKey:@"approvalFlag"];
        
        [newrecord setValue:[item valueForKey:@"Institution"] forKey:@"institution"];
        [newrecord setValue:[item valueForKey:@"SubTargetClassLIC"] forKey:@"subTargetClassLIC"];
        [newrecord setValue:[item valueForKey:@"SubTargetClass"] forKey:@"subTargetClass"];
        
        NSError *error;
        [context save:&error];
    }
    
}


-(void)addEventSpeaker:(NSString*)event_id EventSpeakerdata:(NSMutableArray *)EventSpeakerdata
{
    for(NSDictionary *item in EventSpeakerdata)
    {
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"speakerid like %@",[item valueForKey:@"SpeakerID"]];
            [self delete_predicate:@"Event_Speaker" predicate:predicate];
        }
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Speaker" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:event_id forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"SpeakerID"] forKey:@"speakerid"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        // [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"FirstName"] forKey:@"firstname"];
        [newrecord setValue:[item valueForKey:@"LastName"] forKey:@"lastname"];
        //        [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageicon"];
        //        [newrecord setValue:[item valueForKey:@"CVID"] forKey:@"cvid"];
        // [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"SalutationLIC"] forKey:@"salutationlic"];
        [newrecord setValue:[item valueForKey:@"EmailAddress"] forKey:@"emailaddress"];
        [newrecord setValue:[item valueForKey:@"ContactNo"] forKey:@"contactno"];
        [newrecord setValue:[item valueForKey:@"Speciality"] forKey:@"speciality"];
        [newrecord setValue:[item valueForKey:@"SpecialityLIC"] forKey:@"specialitylic"];
        [newrecord setValue:[item valueForKey:@"SessionID"] forKey:@"sessionid"];
        [newrecord setValue:[item valueForKey:@"Company"] forKey:@"company"];
        [newrecord setValue:[item valueForKey:@"Designation"] forKey:@"designation"];
        [newrecord setValue:[item valueForKey:@"Department"] forKey:@"department"];
        NSError *error;
        [context save:&error];
        
    }
    
}

-(void) Reachabilitychanged
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       
                       
                       
                       Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
                       NetworkStatus remoteHostStatus = [networkReachability currentReachabilityStatus];
                       if(remoteHostStatus == NotReachable)
                       {
                           [self printloginconsole:@"2" logtext:@"Internet connection not available"];
                       }
                       else
                       {
                           [self send_transaction_server];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [appdelegate Register_pushnotification];
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"synclogsnotification" object:nil];
                                          
                                          
                                          
                                      });
                   });
}


-(void) Insert_PushNotify:(NSString *)messagedetail message_sender:(NSString *)message_sender message_entity:(NSString *)message_entity message_type:(NSString *)message_type message_id:(NSString *)message_id  meassage_read:(NSString *)meassage_read
{
    
    NSError *error;
    NSString *response;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Push_Notification_Table" inManagedObjectContext:context];
    NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"yyyyMMddHHmmss";
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    
    [newrecord setValue:meassage_read forKey:@"meassage_read"];
    [newrecord setValue:[dateFormat stringFromDate:[NSDate date]] forKey:@"message_date"];
    [newrecord setValue:messagedetail forKey:@"message_detail"];
    [newrecord setValue:message_entity forKey:@"message_entity"];
    [newrecord setValue:message_id forKey:@"message_id"];
    [newrecord setValue:message_type forKey:@"message_type"];
    [newrecord setValue:message_sender forKey:@"sender_name"];
    
    [context save:&error];
    
    if (!error)
    {
        NSArray *pushnotificationdatacountarray = [self query_alldata:@"Push_Notification_Table"];
        int notifycount = 0;
        for (int i =0; i<[pushnotificationdatacountarray count]; i++)
        {
            if([[pushnotificationdatacountarray[i] valueForKey:@"meassage_read"] isEqualToString:@"N"])
            {
                notifycount++;
            }
        }
        appdelegate.badgevalue = notifycount;
        
    }
    else
    {
        response =[NSString stringWithFormat:@"%@",error];
    }
}


-(NSArray *) capture_nslogs:(NSString *) server_fromtime server_totime:(NSString *)server_totime
{
    
    NSString *logtime;
    NSString *logmsg;
    NSString *svr_ftime;
    NSString *svr_tilltime;
    NSMutableArray *logarray = [[NSMutableArray alloc] init];
    aslmsg q, m;
    int i;
    const char *key, *val;
    
    q = asl_new(ASL_TYPE_QUERY);
    asl_set_query(q, ASL_KEY_SENDER, "NN_EMSapp", ASL_QUERY_OP_EQUAL);
    
    
    aslresponse r = asl_search(NULL, q);
    while (NULL != (m = asl_next(r)))
    {
        NSMutableDictionary *tmpDict = [NSMutableDictionary dictionary];
        
        for (i = 0; (NULL != (key = asl_key(m, i))); i++)
        {
            NSString *keyString = [NSString stringWithUTF8String:(char *)key];
            
            val = asl_get(m, key);
            
            NSString *string = val?[NSString stringWithUTF8String:val]:@"";
            [tmpDict setObject:string forKey:keyString];
            
        }
        logtime = [tmpDict valueForKey:@"CFLog Local Time"];
        logmsg = [tmpDict valueForKey:@"Message"];
        
        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:[[NSTimeZone localTimeZone] abbreviation]]];
        [df_local setDateFormat:@"yyyyMMddHHmmss"];
        
        NSDate *serverfrom = [[NSDate alloc] init];
        serverfrom = [df_local dateFromString:server_fromtime];
        svr_ftime = [df_local stringFromDate:serverfrom];
        
        NSDate *servertill = [[NSDate alloc] init];
        servertill = [df_local dateFromString:server_totime];
        svr_tilltime = [df_local stringFromDate:servertill];
        
        NSMutableDictionary *logmessage = [[NSMutableDictionary alloc] init];
        if (logtime)
        {
            
            NSComparisonResult fromresult = [logtime compare:svr_ftime];
            
            if(fromresult == NSOrderedDescending )
            {
                //                NSLog(@"servertime is later than logtime");
            }
            else
            {
                NSComparisonResult tillresult = [logtime compare:svr_tilltime];
                
                if(tillresult == NSOrderedDescending )
                {
                    [logmessage setObject:@"No Logs Available" forKey:@"LogMessage"];
                    [logmessage setObject:[tmpDict valueForKey:@"CFLog Local Time"] forKey:@"LogLocalTime"];
                }
                else
                {
                    
                    [logmessage setObject:logmsg forKey:@"LogMessage"];
                    [logmessage setObject:[tmpDict valueForKey:@"CFLog Local Time"] forKey:@"LogLocalTime"];
                }
            }
        }
        else
        {
            //            [logmessage setValue:@"No Logs Available" forKey:@"LogMessage"];
            //            [logmessage setValue:[tmpDict valueForKey:@"CFLog Local Time"] forKey:@"LogLocalTime"];
        }
        [logarray addObject:logmessage];
    }
    
    asl_release(r);
    return logarray;
}

-(void) send_appack:(NSString *)pushtoken log_mgs:(NSString *)log_mgs
{
    //    helper = [[HelperClass alloc] init];
    NSString *AESPrivateKey;
    NSDictionary *Ackrespdata;
    
    NSString *jsonString ;
    NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
    
    [payload setValue:pushtoken forKey:@"ID"];
    [payload setValue:log_mgs forKey:@"Log"];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
    
    if (! jsonData)
    {
        UIAlertController *error_alert = [UIAlertController alertControllerWithTitle:[self gettranslation:@"LBL_545"] message:[NSString stringWithFormat:@"%@",error] preferredStyle:UIAlertControllerStyleAlert];
        [error_alert addAction:[UIAlertAction actionWithTitle:[self gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        [alertWindow.rootViewController presentViewController:error_alert animated:YES completion:nil];
        
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivateKey];
    NSString *base64payload = [encryptedstring base64EncodedStringWithOptions:0];
    
    NSDictionary *msgbody = [self create_trans_msgbody:base64payload txType:@"EMSPUSHAck" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        NSString *response_message = [NSString stringWithFormat:@"-:ERROR:- Acknowledgement Message Sent to server: %@\n response received from server: %@\n",msgbody, [TRANXAPI_response valueForKey:@"response_msg"]];
        [self printloginconsole:@"4" logtext:response_message];
        
        
    }
    else
    {
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        Ackrespdata = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSString *response_message = [NSString stringWithFormat:@"-:SUCCESS:- \nAcknowledgement Message Sent to server: %@\n response received from server: %@\n",msgbody, Ackrespdata];
        [self printloginconsole:@"4" logtext:response_message];
    }
}


-(NSString *) getpushmessage
{
    //    helper = [[HelperClass alloc] init];
    NSInteger msgcount = 0;
    NSString *localpushmsg;
    
    appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    appdelegate.Push_received = @"NO";
    
    
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    UIViewController *mainController = [keyWindow rootViewController];
    
    
    NSString *AESPrivateKey;
    NSString *jsonString ;
    NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
    
    NSDictionary *json_msg;
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
    
    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivateKey];
    NSString *base64payload = [encryptedstring base64EncodedStringWithOptions:0];
    
    NSDictionary *msgbody = [self create_trans_msgbody:base64payload txType:@"EMSGetPUSHLog" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        NSString *response_message = [NSString stringWithFormat:@"-:ERROR:- Push message Pooling failed. response received from server: %@\n", [TRANXAPI_response valueForKey:@"response_msg"]];
        [self printloginconsole:@"4" logtext:response_message];
    }
    else
    {
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        [self printloginconsole:@"6" logtext:[NSString stringWithFormat:@"Opty List Data request: %@",authresponse]];
        
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSDictionary *pushdata = jsonData[@"ListOfRecords"];
        
        msgcount = [pushdata count];
        
        for(NSDictionary *item in pushdata)
        {
            NSError *jsonError;
            NSData *objectData = [[item valueForKey:@"PUSHMsg"] dataUsingEncoding:NSUTF8StringEncoding];
            json_msg = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
            
            [self Process_remote_msg:json_msg];
        }
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive || [appdelegate.PerformDeltaSync isEqualToString:@"YES"] || [appdelegate.DataDownloaded isEqualToString:@"NO"])
        {
            //Do checking here.
            NSArray *pushdata = [[self query_alldata:@"Push_Notification_Table"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.meassage_read like %@",@"N"]];
            localpushmsg = [NSString stringWithFormat:@"%lu %@",(unsigned long)[pushdata count],[self gettranslation:@"LBL_426"]];
            
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:7];
            notification.alertBody = localpushmsg;
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.soundName =  UILocalNotificationDefaultSoundName;
            //            notification.applicationIconBadgeNumber = notification.applicationIconBadgeNumber + 1;
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = pushdata.count;
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
        }
        else
        {
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            for(NSDictionary *item in pushdata)
            {
                if ([pushdata count] > 1 )
                {
                    [self showalert:[self gettranslation:@"LBL_461"] message:[NSString stringWithFormat:@"%lu %@",(unsigned long)[pushdata count],[self gettranslation:@"LBL_426"]] action:[self gettranslation:@"LBL_462"] ];
                    localpushmsg = [NSString stringWithFormat:@"%lu %@",(unsigned long)[pushdata count],[self gettranslation:@"LBL_426"]];
                }
                else
                {
                    NSError *jsonError;
                    NSData *objectData = [[item valueForKey:@"PUSHMsg"] dataUsingEncoding:NSUTF8StringEncoding];
                    json_msg = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
                    
                    NSString *pmsg_type = [NSString stringWithFormat:@"%@",[[json_msg objectForKey:@"aps"] valueForKey:@"alert"]];
                    NSString *pmsg_value = [NSString stringWithFormat:@"%@",[json_msg valueForKey:@"Message"]];
                    
                    NSString *TitleStr = [json_msg objectForKey:@"Type"];
                    localpushmsg = [NSString stringWithFormat:@"%@ - %@",pmsg_type,pmsg_value];
                    
                    if ([TitleStr  isEqual: @"inAppMessage"])
                    {
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:pmsg_type message:pmsg_value preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:[self gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
                        
                        [mainController presentViewController:alert animated:true completion:nil];
                    }
                    else if ([TitleStr  isEqual: @"Delta"])
                    {
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[self gettranslation:@"LBL_461"] message:[NSString stringWithFormat:@"%@ \n%@ ",pmsg_value,[self gettranslation:@"LBL_406"]] preferredStyle:UIAlertControllerStyleAlert];
                        
                        [alert addAction:[UIAlertAction actionWithTitle:[self gettranslation:@"LBL_729"] style:UIAlertActionStyleDefault handler:nil]];
                        [alert addAction:[UIAlertAction actionWithTitle:[self gettranslation:@"LBL_728"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                          {
                                              [self showWaitCursor:[self gettranslation:@"LBL_476"]];
                                              appdelegate.PerformDeltaSync = @"YES";
                                              
                                              //                            [self get_EMS_data];
                                              //                            [ToastView showToastInParentView:mainController.view withText:@"Delta Sync Completed" withDuaration:2.0];
                                              //                            appdelegate.PerformDeltaSync = @"NO";
                                              //                            [self removeWaitCursor];
                                              
                                              dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                                                             ^{
                                                                 
                                                                 [self get_EMS_data];
                                                                 
                                                                 dispatch_async(dispatch_get_main_queue(),
                                                                                ^{
                                                                                    [ToastView showToastInParentView:mainController.view withText:@"Delta Sync Completed" withDuaration:2.0];
                                                                                    appdelegate.PerformDeltaSync = @"NO";
                                                                                    
                                                                                    [self removeWaitCursor];
                                                                                    [self setlaunchview:@"HomeView"];
                                                                                });
                                                             });
                                              
                                          }]];
                        
                        [mainController presentViewController:alert animated:true completion:nil];
                    }
                }
                
            }
            
            
            
        }
        
        
        
        
        
    }
    
    return @"Completed";
}

-(void) Process_remote_msg:(NSDictionary *) userInfo
{
    
    //NSString *notifymessage_name = [NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] valueForKey:@"alert"]];
    NSString *notifymessage_name = [userInfo objectForKey:@"Type"];
    NSString *notifymessage_value = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"Message"]];
    NSString *notifymessage_pushtoken = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"PushToken"]];
    NSString *notifymessage_entity = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"Entity"]];
    NSString *notifymessage_entityid = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"EntityId"]];
    NSString *notifymessage_sendername = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"Sender"]];
    
    NSString *log_mgs;
    
    
    NSArray *pushmessage = [[self query_alldata:@"Push_Notification_Table"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.message_id like %@",notifymessage_pushtoken]];
    
    if (pushmessage.count < 1)
    {
        if ([notifymessage_name  isEqual: @"INFO"])
        {
            log_mgs = nil;
        }
        else if ([notifymessage_name  isEqual: @"inAppMessage"])
        {
            [self Insert_PushNotify:notifymessage_value message_sender:notifymessage_sendername message_entity:notifymessage_entity message_type:notifymessage_name message_id:notifymessage_pushtoken meassage_read:@"N"];
            log_mgs = nil;
            
        }
        else if ([notifymessage_name  isEqual: @"Delta"])
        {
            [self Insert_PushNotify:notifymessage_value message_sender:notifymessage_sendername message_entity:notifymessage_entity message_type:notifymessage_name message_id:notifymessage_pushtoken meassage_read:@"N"];
            log_mgs = nil;
            
        }
        else if ([notifymessage_name  isEqual: @"Unassigned"])
        {
            [self Insert_PushNotify:notifymessage_value message_sender:notifymessage_sendername message_entity:notifymessage_entity message_type:notifymessage_name message_id:notifymessage_pushtoken meassage_read:@"N"];
            if ([notifymessage_entity isEqualToString:@"ACTIVITY"])
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activityid like %@",notifymessage_entityid];
                [self delete_predicate:@"Activities" predicate:predicate];
            }
            log_mgs = nil;
        }
        
        else if ([notifymessage_name  isEqual: @"logLevel"])
        {
            [self Insert_PushNotify:notifymessage_value message_sender:notifymessage_sendername message_entity:notifymessage_entity message_type:notifymessage_name message_id:notifymessage_pushtoken meassage_read:@"N"];
            
            [[NSUserDefaults  standardUserDefaults]setObject:notifymessage_name forKey:@"logleveldefault"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            log_mgs = nil;
            
        }
        else if ([notifymessage_name  isEqual: @"logFetch"])
        {
            NSString *notifymessage_fromtym = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"fromTimestamp"]];
            NSString *notifymessage_totym = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"toTimestamp"]];
            NSString *notifymessage_rstlevelflg = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"resetLogLevel"]];
            NSString *logfetchmessage = [NSString stringWithFormat:@"Logs Fetched for time period:%@ till %@",notifymessage_fromtym,notifymessage_totym];
            
            NSLog(@"logfetchmessage: %@",logfetchmessage);
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self capture_nslogs:notifymessage_fromtym server_totime:notifymessage_totym] options:NSJSONWritingPrettyPrinted error:&error];
            
            
            [self Insert_PushNotify:logfetchmessage message_sender:notifymessage_sendername message_entity:notifymessage_entity message_type:notifymessage_name message_id:notifymessage_pushtoken meassage_read:@"Y"];
            
            log_mgs = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            if ([notifymessage_rstlevelflg  isEqual: @"Y"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"3" forKey:@"logleveldefault"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           @autoreleasepool {
                               
                               [self send_appack:notifymessage_pushtoken log_mgs:log_mgs];
                           }
                       });
    }
    else
    {
        [self printloginconsole:@"6" logtext:@"Push Message already received"];
    }
}
#pragma mark status colour
-(UIColor *)getstatuscolor:(NSString *)statuslabel entityname:(NSString *)entityname
{
    UIColor *returncolor;
    if([entityname isEqualToString:@"Event"])
    {
        if ([statuslabel isEqual: @"Approved"]) { returncolor = [UIColor colorWithRed:118/255.f green:193/255.f blue:83/255.f alpha: 1]; } // ----- green
        else if ([statuslabel isEqual: @"Planned"]){returncolor = [UIColor colorWithRed:252/255.f green:173/255.f blue:58/255.f alpha: 1];} // ----- blue
        else if ([statuslabel isEqual: @"Submitted"]){returncolor = [UIColor colorWithRed:0/255.f green:128/255.f blue:0/255.f alpha: 1];} //  ----- yellow
        else if ([statuslabel isEqual: @"Started"]){returncolor = [UIColor colorWithRed:221/255.f green:215/255.f blue:3/255.f alpha: 1];} // ----- purple
        else if ([statuslabel isEqual: @"Cancelled"]){returncolor = [UIColor colorWithRed:237/255.f green:58/255.f blue:58/255.f alpha: 1];} // ----- red
        else if ([statuslabel isEqual: @"Completed"]){returncolor = [UIColor colorWithRed:81/255.f green:221/255.f blue:74/255.f alpha: 1];}
        else if ([statuslabel isEqual: @"Closed"]){returncolor = [UIColor colorWithRed:0/255.f green:171/255.f blue:169/255.f alpha: 1];}
        else returncolor = [UIColor colorWithRed:255/255.f green:15/255.f blue:0/255.f alpha: 1];
    }
    else if ([entityname isEqualToString:@"Attendee"])
    {
        if ([statuslabel isEqual: @"Approved"]) { returncolor = [UIColor colorWithRed:118/255.f green:193/255.f blue:83/255.f alpha: 1]; } // ----- green
        else if ([statuslabel isEqual: @"Rejected"]){returncolor = [UIColor colorWithRed:237/255.f green:58/255.f blue:58/255.f alpha: 1];} // ----- red
        else if ([statuslabel isEqual: @"Proposed"]){returncolor = [UIColor colorWithRed:83/255.f green:152/255.f blue:239/255.f alpha: 1];} //  ----- yellow
        else if ([statuslabel isEqual: @"Accepted"]){returncolor = [UIColor colorWithRed:5/255.f green:168/255.f blue:5/255.f alpha: 1];} // ----- purple
        else if ([statuslabel isEqual: @"Declined"]){returncolor = [UIColor colorWithRed:237/255.f green:58/255.f blue:58/255.f alpha: 1];} // ----- red
        else if ([statuslabel isEqual: @"Tentative"]){returncolor = [UIColor colorWithRed:163/255.f green:146/255.f blue:98/255.f alpha: 1];}
        else if ([statuslabel isEqual: @"Signed"]){returncolor = [UIColor colorWithRed:245/255.f green:206/255.f blue:74/255.f alpha: 1];}
        else returncolor = [UIColor colorWithRed:255/255.f green:15/255.f blue:0/255.f alpha: 1];
    }
    else if ([entityname isEqualToString:@"Activity"])
    {
        if ([statuslabel isEqual: @"Planned"]) { [UIColor colorWithRed:252/255.f green:173/255.f blue:58/255.f alpha: 1]; } // ----- blue
        else if ([statuslabel isEqual: @"Assigned"]){returncolor = [UIColor colorWithRed:245/255.f green:206/255.f blue:74/255.f alpha: 1];} // ----- blue
        else if ([statuslabel isEqual: @"Completed"]){returncolor = [UIColor colorWithRed:81/255.f green:221/255.f blue:74/255.f alpha: 1];} //  ----- yellow
        else if ([statuslabel isEqual: @"Cancelled"]){returncolor = [UIColor colorWithRed:237/255.f green:58/255.f blue:58/255.f alpha: 1];} // ----- purple
        else returncolor = [UIColor colorWithRed:255/255.f green:15/255.f blue:0/255.f alpha: 1];
        
    }
    else if ([entityname isEqualToString:@"Expense"])
    {
        if ([statuslabel isEqual: @"Approved"]) { returncolor = [UIColor colorWithRed:118/255.f green:193/255.f blue:83/255.f alpha: 1]; } // ----- blue
        else if ([statuslabel isEqual: @"Planned"]){[UIColor colorWithRed:252/255.f green:173/255.f blue:58/255.f alpha: 1];} // ----- blue
        else if ([statuslabel isEqual: @"Pending"]){returncolor = [UIColor colorWithRed:245/255.f green:206/255.f blue:74/255.f alpha: 1];} //  ----- yellow
        else if ([statuslabel isEqual: @"Rejected"]){returncolor = [UIColor colorWithRed:237/255.f green:58/255.f blue:58/255.f alpha: 1];}
        else if ([statuslabel isEqual: @"New"]){returncolor = [UIColor colorWithRed:249/255.f green:167/255.f blue:70/255.f alpha: 1];}
        else if ([statuslabel isEqual: @"Submitted"]){returncolor = [UIColor colorWithRed:0/255.f green:128/255.f blue:0/255.f alpha: 1];}
        
        
        // ----- purple
    }
    
    
    return returncolor;
}




-(NSArray *)sortData:(NSMutableArray *)array colname:(NSString *)colname type:(NSString *)type Ascending:(BOOL)Ascending
{
    NSArray *sortedArray;
    
    if(![colname isEqualToString:@"daysleft"])
    {
        NSSortDescriptor *Descriptor = [NSSortDescriptor sortDescriptorWithKey:colname ascending:Ascending selector:@selector(localizedCaseInsensitiveCompare:)];
        NSArray *sortDescriptors = [NSArray arrayWithObject:Descriptor];
        sortedArray=[array sortedArrayUsingDescriptors:sortDescriptors];
        
        
    }
    else
    {
        NSSortDescriptor *Descriptor = [NSSortDescriptor sortDescriptorWithKey:colname ascending:Ascending];
        NSArray *sortDescriptors = [NSArray arrayWithObject:Descriptor];
        sortedArray=[array sortedArrayUsingDescriptors:sortDescriptors];
        
    }
    
    
    
    
    return sortedArray;
}


#pragma mark get username
-(NSString *)getUsername
{
   
    SLKeyChainStore *keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[self query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    NSString *UserName;
    if ([userarr count]>0)
    {
        UserName  = [NSString stringWithFormat:@"%@ %@",[userarr[0] valueForKey:@"firstname"],[userarr[0] valueForKey:@"lastname"]];
    }
    
    return UserName;
    
}

-(NSString *)getPositionID
{

    SLKeyChainStore *keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    //    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    NSArray *userarr = [[self query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    NSString *UserName;
    if ([userarr count]>0)
    {
        UserName  = [NSString stringWithFormat:@"%@",[userarr[0] valueForKey:@"eMSPositionLIC"]];
    }
    return UserName;
    
}

-(NSString *)getUserPosition
{
    
    SLKeyChainStore *keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    //    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[self query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    NSString *UserName;
    if ([userarr count]>0)
    {
        UserName  = [NSString stringWithFormat:@"%@",[userarr[0] valueForKey:@"eMSPositionLIC"]];
    }
    return UserName;
    
}
#pragma mark get LOV LIC/value
-(NSString *)getLic:(NSString *)lovtype value:(NSString *)value
{
    
    NSString *lic;
    // NSArray *LovArr2 = [[self query_alldata:@"Event_LOV"];
    
    NSArray *LovArr = [[self query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.lovtype like %@) AND (SELF.value like %@)",lovtype,value]];
    
    if ([LovArr count]>0)
    {
        lic = [LovArr[0] valueForKey:@"lic"];
    }
    return lic;
}

-(NSString *)getvaluefromlic:(NSString *)lovtype lic:(NSString *)lic
{
    
        NSString *value;
    
       // context = [appdelegate managedObjectContext];
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_LOV" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        
        [request setEntity:entitydesc];
        [request setReturnsObjectsAsFaults:YES];
        
        NSError *error;
        NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
        NSArray *LovArr = [matchingData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.lovtype like %@) AND (SELF.lic like %@)",lovtype,lic]];
    
    
    
    if ([LovArr count]>0)
    {
        value = [LovArr[0] valueForKey:@"value"];
    }
    
    return value;

}


#pragma mark delete activity plan
-(void) delete_ActivityPlan:(NSString*)entityname value:(NSString*)value
{
    NSString *result;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"planID like %@",value];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
        
    }
    else
    {
        int count = 0;
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
            count++;
        }
        [context save:&error];
        
        result = [NSString stringWithFormat:@"Success: Records deleted from database  %d ",count];
    }
}
#pragma mark delete activities in plan
-(void) delete_ActivitiesinPlan:(NSString*)entityname planid:(NSString*)planid activityid:(NSString *)activityid
{
    NSString *result;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.planID like %@) AND (SELF.iD like %@)",planid,activityid];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
        
    }
    else
    {
        int count = 0;
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
            count++;
        }
        [context save:&error];
        
        result = [NSString stringWithFormat:@"Success: Records deleted from database  %d ",count];
    }
}
#pragma mark delete member in team
-(void) delete_TeamMember:(NSString*)entityname teamid:(NSString*)teamid eventid:(NSString *)eventid
{
    NSString *result;
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //context = [appdelegate managedObjectContext];
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:entityname inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.id like %@)",eventid,teamid];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
        
    }
    else
    {
        int count = 0;
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
            count++;
        }
        [context save:&error];
        
        result = [NSString stringWithFormat:@"Success: Records deleted from database  %d ",count];
    }
}

-(void)serverdelete:(NSString *)entityname entityid:(NSString *)entityid
{
    if (!([entityname isEqualToString:@"EVENT"] || [entityname isEqualToString:@"ENTITY_ATT"] || [entityname isEqualToString:@"E_ATTENDEE"]|| [entityname isEqualToString:@"E_SESSION"]||[entityname isEqualToString:@"E_SPEAKER"] ))
    {
        [self showWaitCursor:[self gettranslation:@"LBL_734"]];
    }
    
    NSString *transaction_type = entityname;
    NSString *returnmessage;;
    NSError *error;
    NSString *jsonString;
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    //    appdelegate.PerformDeltaSync = @"NO";
    [JsonDict setValue:entityname forKey:@"Entity"];
    [JsonDict setValue:entityid forKey:@"EntityID"];
    
    NSMutableArray *JsonArr = [[NSMutableArray alloc]init];
    [JsonArr addObject:JsonDict];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArr options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    
    NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSDeleteEntity" pageno:@"" pagecount:@"" lastpage:@""];
    // NSMutableArray *JsonArr = [[NSMutableArray alloc]init];
    // [JsonArr addObject:msgbody];
    
    //    appdelegate.senttransaction = @"Y";
    //    [self insert_transaction_local:msgbody entity_id:[self generate_rowId] type:transaction_type entityname:@"Delete Event Data"  Status:@"Open"];
    
    
    NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        returnmessage = [NSString stringWithFormat:@"Error: %@",[AUTHWEBAPI_response valueForKey:@"response_msg"]];
    }
    else
    {
        //        NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        //        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        //        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        returnmessage = [NSString stringWithFormat:@"Event Delete Success"];
    }
    if (!([entityname isEqualToString:@"EVENT"] || [entityname isEqualToString:@"ENTITY_ATT"] || [entityname isEqualToString:@"E_ATTENDEE"]|| [entityname isEqualToString:@"E_SESSION"]||[entityname isEqualToString:@"E_SPEAKER"] ))
    {
        [self removeWaitCursor];
    }
    
}

-(NSString *)booltostring:(BOOL) inputflag
{
    if (inputflag)
    {
        return @"Y";
    }
    else
    {
        return @"N";
    }
}
-(NSString *)stringtobool:(NSString *)inputflag
{
    if ([inputflag isEqualToString:@"Y"])
    {
        return @"true";
    }
    else
    {
        return @"false";
    }
}
#pragma mark returns canedit
-(NSString *)EventCanEditFlag:(NSString *)Flag
{
    NSString *CanEdit;
    NSArray *EventArr = [[self query_alldata:@"Event"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if ([EventArr count]>0)
    {
        if ([Flag isEqualToString:@"canEdit"])
        {
            CanEdit = [EventArr[0] valueForKey:@"canEdit"];
        }
        else if ([Flag isEqualToString:@"canDelete"])
        {
            CanEdit = [EventArr[0] valueForKey:@"canDelete"];
        }
    }
    
    return CanEdit;
}



-(NSString *) checkinternetconnection
{
    NSString *status;
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [networkReachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        NSLog(@"No internet connection ");
        status = @"N";
    }
    else
    {
        status = @"Y";
    }
    return status;
}
#pragma mark validate textfield text
-(BOOL)isdropdowntextCorrect:text lovtype:(NSString *)lovtype
{
    NSArray *LOVArr = [[self query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",lovtype]];
    NSMutableArray *ValueArr = [[NSMutableArray alloc]init];
    for (int i =0; i < [LOVArr count]; i++)
    {
        [ValueArr addObject:[LOVArr[i] valueForKey:@"value"]];
    }
    if ([ValueArr containsObject:text])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}
-(NSString *)GetPlanName:Planid
{
    NSString *PlanName;
    NSArray *PlanNameArr = [[self query_alldata:@"ActivityPlans"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"planID like %@",Planid ]];
    if ([PlanNameArr count]>0)
    {
        PlanName = [PlanNameArr[0] valueForKey:@"name"];
    }
    return PlanName;
}
#pragma mark validate phone number
-(BOOL)NumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[+()0-9]{10,16}";
    //only enter numbers
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
    {
        if(number.length>=10 && number.length<=16 )
        return TRUE;
        else
        return FALSE;
    }
    return FALSE;
}

-(BOOL)ValidPassword:(NSString *)pass
{
    NSString *numberRegEx= @"(?=.*?[a-zA-Z])(?=.*?[^a-zA-Z]).{8,}";
    ;
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:pass] == YES)
    {
        if(pass.length>=8 && pass.length<=20){
            NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
            NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                                   characterSetWithCharactersInString:specialCharacterString];
            if ([pass.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
                return TRUE;
            }
        }
        else{
            return FALSE;
        }
    }
    return FALSE;
}

-(BOOL)CostValidate:(NSString*)number
{
    NSString *numberRegEx = @"\[0-9]{1,12}([.]\[0-9]{1,5})?|[.]\[0-9]{1,5}";
    //only enter numbersz
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
    {
        return TRUE;
    }
    else
    return FALSE;
}

-(void) setlaunchview:(NSString *) targetview
{
    //replace and push rootview manually
    
    
    if([targetview isEqualToString:@"LoginView"])
    {
        [self update_data:@"GoToDashboard" inputdata:@"NO" entityname:@"S_Config_Table"];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeViewController  *lvc = [storyboard instantiateViewControllerWithIdentifier:targetview];
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:lvc];
    navigationController.navigationBarHidden = YES;
    appdelegate.window.rootViewController =nil;
    appdelegate.window.rootViewController = navigationController;
    [appdelegate.window makeKeyAndVisible];
    
}

-(void)CopyData
{
    NSArray *AttendeeListArr=[self query_alldata:@"Attendee_Temp"];
    
    
    for(NSDictionary *item in AttendeeListArr)
    {
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Attendee" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        
        [newrecord setValue:[item valueForKey:@"Status"] forKey:@"status"];
        [newrecord setValue:[item valueForKey:@"Created"] forKey:@"created"];
        [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Updated"] forKey:@"updated"];
        [newrecord setValue:[item valueForKey:@"UpdatedBy"] forKey:@"updatedby"];
        [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
        [newrecord setValue:[item valueForKey:@"ContactNumber"] forKey:@"contactnumber"];
        [newrecord setValue:[item valueForKey:@"EmailAddress"] forKey:@"emailaddress"];
        [newrecord setValue:[item valueForKey:@"FirstName"] forKey:@"firstname"];
        [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageid"];
        [newrecord setValue:[item valueForKey:@"IntegrationID"] forKey:@"integrationid"];
        [newrecord setValue:[item valueForKey:@"IntegrationSource"] forKey:@"integrationsource"];
        [newrecord setValue:[item valueForKey:@"LastName"] forKey:@"lastname"];
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"Speciality"] forKey:@"speciality"];
        [newrecord setValue:[item valueForKey:@"TargetClass"] forKey:@"targetclass"];
        
        [newrecord setValue:[item valueForKey:@"IntegrationSourceLIC"] forKey:@"integrationsourcelic"];
        [newrecord setValue:[item valueForKey:@"OwnedBy"] forKey:@"ownedby"];
        [newrecord setValue:[item valueForKey:@"SalutationLIC"] forKey:@"salutationlic"];
        [newrecord setValue:[item valueForKey:@"SpecialityLIC"] forKey:@"specialitylic"];
        [newrecord setValue:[item valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[item valueForKey:@"TargetClassLIC"] forKey:@"targetclasslic"];
        [newrecord setValue:[item valueForKey:@"Institution"] forKey:@"institution"];
        [newrecord setValue:[item valueForKey:@"SubTargetClassLIC"] forKey:@"subTargetClassLIC"];
        [newrecord setValue:[item valueForKey:@"SubTargetClass"] forKey:@"subTargetClass"];
        
        NSError *error;
        [context save:&error];
    }
    
    [self delete_alldata:@"Attendee_Temp"];
}

/*-(void) ProcessEventAttendees:(NSString *) eventid
 {
 NSString *process_result = @"Success";
 appdelegate.EAlstpage = @"N";
 appdelegate.EApagecount = @"1";
 appdelegate.EApageno= @"1";
 int page;
 
 
 //while ([lstpage isEqual: @"N"])
 //{
 NSError *error;
 NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
 
 [JsonDict setValue:eventid forKey:@"EventID"];
 [JsonDict setValue:@"ATTENDEE" forKey:@"Entity"];
 
 NSString *jsonString;
 NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
 if (! jsonData)
 {
 NSLog(@"Got an error: %@", error);
 }
 else
 {
 jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
 }
 NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
 NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
 NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
 NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSGetEventDetails" pageno:appdelegate.EApageno pagecount:appdelegate.EApagecount lastpage:[self stringtobool:@"lstpage"]];
 
 
 NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
 
 if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
 {
 [self removeWaitCursor];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
 
 [alert show];
 }
 else
 {
 NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
 NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
 NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
 
 
 page = [[AUTHWEBAPI_response valueForKey:@"Page"] intValue];
 appdelegate.EApageno = [NSString stringWithFormat:@"%d",page+1];
 appdelegate.EApagecount = [AUTHWEBAPI_response valueForKey:@"PageCount"];
 appdelegate.EAlstpage = [self booltostring:[[AUTHWEBAPI_response valueForKey:@"LastPage"] boolValue]];
 
 NSLog(@"JSON Data : %@",jsonData);
 
 [self addEventAttendee:eventid EventAttendeedata:[jsonData valueForKey:@"EventAttendee"]];
 
 }
 // }
 if([appdelegate.EAlstpage isEqualToString:@"N"])
 {
 [self dispatchEventAttendeeThread:eventid];
 }
 
 
 
 }
 -(void)dispatchEventAttendeeThread:(NSString *)eventid
 {
 appdelegate.downloadInProgress=@"YES";
 
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
 ^{
 int page;
 while([appdelegate.EAlstpage isEqualToString:@"N"])
 {
 NSError *error;
 NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
 
 [JsonDict setValue:eventid forKey:@"EventID"];
 [JsonDict setValue:@"ATTENDEE" forKey:@"Entity"];
 
 NSString *jsonString;
 NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
 if (! jsonData)
 {
 NSLog(@"Got an error: %@", error);
 }
 else
 {
 jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
 }
 NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
 NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
 NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
 NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSGetEventDetails" pageno:appdelegate.EApageno pagecount:appdelegate.EApagecount lastpage:[self stringtobool:@"lstpage"]];
 
 
 
 
 NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
 
 if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
 {
 [self removeWaitCursor];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
 
 [alert show];
 }
 else
 {
 NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"]   valueForKey:@"payload"] withKey:AESPrivatekey];
 NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
 NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
 
 
 page = [[AUTHWEBAPI_response valueForKey:@"Page"] intValue];
 appdelegate.EApageno = [NSString stringWithFormat:@"%d",page+1];
 appdelegate.EApagecount = [AUTHWEBAPI_response valueForKey:@"PageCount"];
 appdelegate.EAlstpage = [self booltostring:[[AUTHWEBAPI_response valueForKey:@"LastPage"] boolValue]];
 
 [[NSUserDefaults standardUserDefaults]setObject:appdelegate.EAlstpage forKey:@"EAlastPageStatus"];
 [[NSUserDefaults standardUserDefaults]setObject:appdelegate.EApageno forKey:@"EApageno"];
 
 NSLog(@"JSON Data : %@",jsonData);
 
 [self addEventAttendee:eventid EventAttendeedata:[jsonData valueForKey:@"EventAttendee"]];
 
 if([appdelegate.logout isEqualToString:@"YES"])
 {
 appdelegate.EAlstpage=@"Y";
 }
 
 
 }
 }
 
 dispatch_async(dispatch_get_main_queue(),
 ^{
 appdelegate.downloadInProgress=@"NO";
 
 
 
 });
 });
 
 
 }
 */

-(void)dispatchEventAttendeeThread:(NSString *)eventid   //Background thred for downloading event attendees
{
    
    appdelegate.EAdownloadInProgress=@"YES";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       int page;
                       while([appdelegate.EAlstpage isEqualToString:@"N"])
                       {
                           @autoreleasepool {
                               NSError *error;
                               NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
                               
                               [JsonDict setValue:eventid forKey:@"EventID"];
                               [JsonDict setValue:@"ATTENDEE" forKey:@"Entity"];
                               
                               NSString *jsonString;
                               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
                               if (! jsonData)
                               {
                                   NSLog(@"Got an error: %@", error);
                               }
                               else
                               {
                                   jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                               }
                               NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
                               NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
                               NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
                               NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSGetEventDetails" pageno:appdelegate.EApageno pagecount:appdelegate.EApagecount lastpage:[self stringtobool:@"lstpage"]];
                               
                               
                               
                               
                               NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
                               
                               if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
                               {
                                   [self removeWaitCursor];
                                   
                                   // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
                                   
                                   //  [alert show];
                                   
                                   [self showalert:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[self gettranslation:@"LBL_462"]];
                                   
                               }
                               else
                               {
                                   NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"]   valueForKey:@"payload"] withKey:AESPrivatekey];
                                   NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                                   NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                                   
                                   
                                   page = [[AUTHWEBAPI_response valueForKey:@"Page"] intValue];
                                   appdelegate.EApageno = [NSString stringWithFormat:@"%d",page+1];
                                   appdelegate.EApagecount = [AUTHWEBAPI_response valueForKey:@"PageCount"];
                                   appdelegate.EAlstpage = [self booltostring:[[AUTHWEBAPI_response valueForKey:@"LastPage"] boolValue]];
                                   
                                   //save the data into user defaults in order to restore download if the application is force closed
                                   [[NSUserDefaults standardUserDefaults]setObject:appdelegate.EAlstpage forKey:@"EAlastPageStatus"];
                                   [[NSUserDefaults standardUserDefaults]setObject:appdelegate.EApageno forKey:@"EApageno"];
                                   
                                   NSLog(@"JSON Data : %@",jsonData);
                                   
                                   [self addEventAttendee:eventid EventAttendeedata:[jsonData valueForKey:@"EventAttendee"]];
                                   
                                   if([appdelegate.logout isEqualToString:@"YES"])
                                   {
                                       appdelegate.EAlstpage=@"Y";
                                   }
                                   
                                   
                               }
                           }
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          appdelegate.downloadInProgress=@"NO";
                                      });
                   });
    
    
}

-(void) sendemail:(NSString *)eventmailid emailaddr:(NSString *)emailaddr attendeeid:(NSString *)attendeeid
{
    NSError *error;
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:emailaddr  forKey:@"EmailAddress"];
    [JsonDict setValue:eventmailid forKey:@"EventMailID"];
    
    if (attendeeid.length > 0)
    {
        [JsonDict setValue:attendeeid forKey:@"AttendeeID"];
    }
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [self encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [self create_trans_msgbody:base64String txType:@"EMSPreviewMail" pageno:@"" pagecount:@"" lastpage:@""];
    
    
    appdelegate.senttransaction = @"Y";
    [self insert_transaction_local:msgbody entity_id:[self generate_rowId] type:@"EMSPreviewMail" entityname:@"Send Test Email"  Status:@"Open"];
    currentTopVC = [self currentTopViewController];
    [ToastView showToastInParentView:currentTopVC.view withText:[self gettranslation:@"LBL_761"] withDuaration:1.0];
    
    /*
     
     NSMutableDictionary *AUTHWEBAPI_response = [self invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
     
     if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
     {
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[self gettranslation:@"LBL_462"] otherButtonTitles:nil];
     
     [alert show];
     }
     else
     {
     NSString *authresponse =  [self decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
     NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
     NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
     
     NSLog(@"Email Successfully Sent");
     
     }
     */
}



-(void)downloadDocument:(NSString *)docURL
{
    NSError *error;
    NSString *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Documents"];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:documentsPath])
    [[NSFileManager defaultManager]createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.pdf",@"helpDocument"]]; //Add the file name
    NSData *curlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:docURL]];
    if (curlData)
    {
        [curlData writeToFile:filePath atomically:YES];
        [self insert_data:@"helpdocument" valuedata:filePath entityname:@"S_Config_Table"];
    }
    else
    {
        [self printloginconsole:@"3" logtext:[NSString stringWithFormat:@"Unable to download Attachment: %@",docURL]] ;
    }
}

-(NSUInteger)checkForRescheduling
{
    NSPredicate *eventidpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    NSArray *sessionlist = [[self query_alldata:@"Event_Session"] filteredArrayUsingPredicate:eventidpredicate];
    NSMutableArray *RescheduleSessionList=[[NSMutableArray alloc]init];
    for (int i=0; i<sessionlist.count; i++)
    {
        NSString *sdate=[sessionlist[i] valueForKey:@"startdate"];
        NSString *edate=[sessionlist[i] valueForKey:@"enddate"];
        double SessionStartDate=[sdate doubleValue];
        double SessionEndDate=[edate doubleValue];
        
        if((SessionStartDate<[appdelegate.eventTempStartDate doubleValue]|| SessionStartDate>[appdelegate.eventTempEndDate doubleValue]) || (SessionEndDate>[appdelegate.eventTempEndDate doubleValue] || SessionEndDate<[appdelegate.eventTempStartDate doubleValue]))
        {
            
            [RescheduleSessionList addObject:sessionlist[i]];
            
        }
    }
    return  RescheduleSessionList.count;
}
-(UIColor *)LightBlueColour
{
    UIColor *Colour = [UIColor colorWithRed:0.0f green:159.0/255.0f blue:218.0/255.0f alpha:1.0f];
    return Colour;
}
-(UIColor *)DarkBlueColour
{
    UIColor *Colour = [UIColor colorWithRed:0.0f green:25.0/255.0f blue:101.0/255.0f alpha:1.0f];
    return Colour;
}
-(NSString *)geturl:(NSString *)environment
{
    if ([environment isEqualToString:@"UAT"])
    {
        return @"https://ioeventuat.azurewebsites.net";
    }
    else if ([environment isEqualToString:@"QA"])
    {
        return @"https://ioeventsapiqa.azurewebsites.net";
    }
    else if ([environment isEqualToString:@"PREPROD"])
    {
        return @"https://ioeventsapiuat.azurewebsites.net";
    }
    else if ([environment isEqualToString:@"PROD"])
    {
        return @"https://ioeventsapiprod.azurewebsites.net/";
    }
    else if ([environment isEqualToString:@"DEV"])
    {
        return @"https://ioeventsapidev.azurewebsites.net";
    }
    
    
    else
    return nil;
}
-(NSString *)getlanguageCode
{
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    return languageCode;
}

-(void)GetEventApprovalData
{
    [self ProcessEventList:@""];
}

-(void)ProcessItinearyDetails:(NSString *)EventID LICType:(NSString *)lic
{
    NSString *process_result;
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    //NSString *EVENTLOV_payload = [self create_trans_payload:@"UserName" fltervalue:EventID ];
    NSString *EVENTLOV_payload = [self Create_ItinearyPayload:@"UserName" fltervalue:EventID lic:lic];
    NSDictionary *EVENTLOV_msgbody = [self create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetItineraryList" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        appdelegate.ProcessStatus = [TRANXAPI_response valueForKey:@"response_msg"];
    }
    else
    {
        process_result = @"Success";
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        NSArray *ItinearyData = [jsonData valueForKey:@"ItineraryList"];
        
        NSArray *AttachmentArray = [jsonData valueForKey:@"AttachmentList"];
        
        [self InsertItenaryData:ItinearyData eventid:EventID];
        
        [self InsertItenaryAttachment:AttachmentArray eventid:EventID];
    }
}

-(void)InsertItenaryData:(NSArray *)ItinearyData eventid:(NSString *)eventid
{
    for(NSDictionary *item in ItinearyData)
    {
        
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.eventID like %@) AND (SELF.entityID LIKE %@)" ,eventid,[item valueForKey:@"EntityID"]];
            [self delete_predicate:@"Event_Itenary" predicate:predicate];
        }
        
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Itenary" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"ItineraryID"] forKey:@"itineraryID"];
        [newrecord setValue:[item valueForKey:@"AffiliateID"] forKey:@"affiliateID"];
        [newrecord setValue:[item valueForKey:@"SalutationLIC"] forKey:@"salutationLIC"];
        
        [newrecord setValue:[item valueForKey:@"FoodPreference"] forKey:@"foodPreference"];
        [newrecord setValue:[item valueForKey:@"FoodPreferenceLIC"] forKey:@"foodPreferenceLIC"];
        
        [newrecord setValue:[self booltostring:[[item valueForKey:@"DBRec"] boolValue]] forKey:@"dbrec"];
        
        [newrecord setValue:[item valueForKey:@"Salutation"] forKey:@"salutation"];
        [newrecord setValue:[item valueForKey:@"Name"] forKey:@"fullname"];
        
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Accommodation"] boolValue]] forKey:@"accommodation"];
        [newrecord setValue:[self booltostring:[[item valueForKey:@"Travel"] boolValue]] forKey:@"travel"];
        
        [newrecord setValue:[item valueForKey:@"Note"] forKey:@"note"];
        [newrecord setValue:[item valueForKey:@"PartyID"] forKey:@"partyID"];
        [newrecord setValue:[item valueForKey:@"PartyTypeLIC"] forKey:@"partyTypeLIC"];
        [newrecord setValue:[item valueForKey:@"PartyType"] forKey:@"partyType"];
        [newrecord setValue:[item valueForKey:@"EntityID"] forKey:@"entityID"];
        [newrecord setValue:[item valueForKey:@"EntityTypeLIC"] forKey:@"entityTypeLIC"];
        [newrecord setValue:[item valueForKey:@"EntityType"] forKey:@"entityType"];
        [newrecord setValue:[item valueForKey:@"ArrivalDate"] forKey:@"arrivalDate"];
        [newrecord setValue:[item valueForKey:@"FlightDetails"] forKey:@"flightDetails"];
        [newrecord setValue:[item valueForKey:@"FlightClassLIC"] forKey:@"flightClassLIC"];
        [newrecord setValue:[item valueForKey:@"FlightClass"] forKey:@"flightClass"];
        [newrecord setValue:[item valueForKey:@"HotelDetails"] forKey:@"hotelDetails"];
        [newrecord setValue:[item valueForKey:@"HotelClassLIC"] forKey:@"hotelClassLIC"];
        [newrecord setValue:[item valueForKey:@"HotelClass"] forKey:@"hotelClass"];
        [newrecord setValue:[item valueForKey:@"HotelRoomNo"] forKey:@"hotelRoomNo"];
        [newrecord setValue:[item valueForKey:@"VisaTypeLIC"] forKey:@"visaTypeLIC"];
        [newrecord setValue:[item valueForKey:@"VisaType"] forKey:@"visaType"];
        [newrecord setValue:[item valueForKey:@"AuthorizationTypeLIC"] forKey:@"authorizationTypeLIC"];
        [newrecord setValue:[item valueForKey:@"AuthorizationType"] forKey:@"authorizationType"];
        
        NSError *error;
        [context save:&error];
    }
}

-(void)DeleteItinearyRecord:(NSString *)id
{
    NSString *result;
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Itenary" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itineraryID like %@",id];
    
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
    }
    else
    {
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
        }
        [context save:&error];
        result = [NSString stringWithFormat:@"Success: Records deleted from database"];
    }
    
}


-(NSString *)ProcessItinearyMetaData
{
    NSString *process_result = @"Success";
    NSString *AESPrivateKey = [self query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSString *EVENTLOV_payload = [self create_trans_payload:@"Entity" fltervalue:@""];
    NSDictionary *EVENTLOV_msgbody = [self create_trans_msgbody:EVENTLOV_payload txType:@"EMSGetAffiliate" pageno:@"" pagecount:@"" lastpage:@""];
    
   
    
    NSMutableDictionary *TRANXAPI_response = [self invokewebservice:EVENTLOV_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        process_result = [TRANXAPI_response valueForKey:@"response_msg"];
        return @"fail";
    }
    else
    {
        
        NSString *authresponse =  [self decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"]withKey:AESPrivateKey];
        
        
        NSData *data=[authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *temp=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        [self update_data:@"Geocodes" inputdata:temp[@"Geocodes"] entityname:@"S_Config_Table"];
        
        NSString  *tempDictItinearay=[temp valueForKey:@"ItineraryFields"];
        
        tempDictItinearay = [tempDictItinearay stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
        if([tempDictItinearay length]>0)
        {
            NSDictionary *MetadataDict=[NSJSONSerialization JSONObjectWithData:[tempDictItinearay dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
            
            NSArray *Name=[MetadataDict allKeys];
            
            int index=0;
            for(NSDictionary *item in MetadataDict)
            {
                NSDictionary *item2 = [MetadataDict  valueForKey:item];
                
                
                NSError *error;
                NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"ItinearyFields" inManagedObjectContext:context];
                NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                
                
                [newrecord setValue:Name[index] forKey:@"name"];
                
                if([Name[index] isEqualToString:@"Name"])
                [newrecord setValue:@"fullname" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"HotelClass"])
                [newrecord setValue:@"hotelClassLIC" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"FlightClass"])
                [newrecord setValue:@"flightClassLIC" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"RoomNo"])
                [newrecord setValue:@"hotelRoomNo" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"ArrivalDate"])
                [newrecord setValue:@"arrivalDate" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"FlightName"])
                [newrecord setValue:@"flightDetails" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"VisaType"])
                [newrecord setValue:@"visaTypeLIC" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"FoodPreference"])
                [newrecord setValue:@"foodPreferenceLIC" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"HotelDetails"])
                [newrecord setValue:@"hotelDetails" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"FlightDetails"])
                [newrecord setValue:@"flightDetails" forKey:@"localDatabasename"];
                
                if([Name[index] isEqualToString:@"HotelRoomNo" ])
                [newrecord setValue:@"hotelRoomNo" forKey:@"localDatabasename"];
                
                
                [newrecord setValue:[item2 valueForKey:@"Label"] forKey:@"labelid"];
                [newrecord setValue:[item2 valueForKey:@"Type"] forKey:@"type"];
                [newrecord setValue:[item2 valueForKey:@"DataIndex"] forKey:@"dataindex"];
                [newrecord setValue:[self booltostring:[[item2 valueForKey:@"AdvanceSearch"] boolValue]] forKey:@"advanceSearch"];
                [newrecord setValue:[item2 valueForKey:@"LICRef"] forKey:@"licref"];
                [context save:&error];
                index++;
            }
        }
        return  @"Success";
    }
}

-(void)InsertItenaryAttachment:(NSMutableArray *)AttachmentList eventid:(NSString *)Eventid
{
    for(NSDictionary *item in AttachmentList)
    {
        NSError *error;
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"ItinearyAttachment" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:Eventid forKey:@"eventid"];
        [newrecord setValue:[item valueForKey:@"Description"] forKey:@"desc"];
        [newrecord setValue:[item valueForKey:@"EntityID"] forKey:@"entityid"];
        [newrecord setValue:[item valueForKey:@"FileName"] forKey:@"filename"];
        [newrecord setValue:[item valueForKey:@"FileSize"] forKey:@"filesize"];
        [newrecord setValue:[item valueForKey:@"FileType"] forKey:@"filetype"];
        [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[item valueForKey:@"Type"] forKey:@"type"];
        [newrecord setValue:[item valueForKey:@"TypeLIC"] forKey:@"typelic"];
        [newrecord setValue:[item valueForKey:@"UploadDate"] forKey:@"uploaddate"];
        [newrecord setValue:[item valueForKey:@"StorageID"] forKey:@"storageid"];
        [context save:&error];
    }
}

-(NSString *)convertToStringFromCurrency:(NSString *)currency
{
    NSString *curr;
    
    if([currency length]>0)
    curr = [currency substringFromIndex:1];
    else
    curr = nil;
    
    return curr;
}

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}
-(NSString *)getGroupNameforGroupID:(NSString *)groupid
{
  //  appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    NSString *result;
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Group" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupid like %@",groupid];
    
    [request setEntity:entitydesc];
    [request setPredicate:predicate];
    [request setReturnsObjectsAsFaults:NO];
    
    
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    
    if (matchingData.count <=0)
    {
        result = @"Error: No Data Found";
    }
    else
    {
        result = [matchingData[0] valueForKey:@"groupname"];
        
    }
    
    return  result;
}
-(void)InsertGroupDataForEventId:(NSArray*)GroupArray sender:(NSString *)sender{
    
  //  appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // context = [appdelegate managedObjectContext];
    for (NSDictionary *record in GroupArray )
    {
        if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupid like %@",[record valueForKey:@"ID"]];
            [self delete_predicate:@"Event_Group" predicate:predicate];
        }
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Group" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        // if([sender isEqualToString:@"fdr"]){
        [newrecord setValue:[record valueForKey:@"EventID"]?:[record valueForKey:@"eventid"] forKey:@"eventid"];
        [newrecord setValue:[record valueForKey:@"ID"]?:[record valueForKey:@"id"] forKey:@"groupid"];
        [newrecord setValue:[record valueForKey:@"GroupName"]?:[record valueForKey:@"groupname"] forKey:@"groupname"];
    }
    NSError *error;
    [context save:&error];
}


-(void)deletewithGroupID:(NSString *)groupid
{
    NSString *result;
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Group" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupid like %@",groupid];
    
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if (matchingData<=0)
    {
        result = @"Error: No records found to be deleted";
    }
    else
    {
        for (NSManagedObject *obj in matchingData)
        {
            [context deleteObject:obj];
        }
        [context save:&error];
        result = [NSString stringWithFormat:@"Success: Records deleted from database"];
    }
    
    
}
-(BOOL)CheckIfGroupIdCanBeDeleted:(NSString *)GroupID{
    NSArray *SessionArray = [[self query_alldata:@"Event_Session"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",GroupID]];
    
    NSArray *AttendeeArray = [[self query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",GroupID]];
    
    NSArray *AttachmntArray = [[self query_alldata:@"Event_Attachments"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",GroupID]];
    
    if(AttendeeArray.count >0 || AttachmntArray.count>0 ||SessionArray.count>0 )
    {
        return false;
    }
    else
    return true;
}
-(void)UpdateGroupForAttendee:(NSString *)attendeeid groupName:(NSString *)GroupName
{
    NSError *error;
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Event_Attendee"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id like %@",attendeeid];
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    
    if(matchingData.count>0)
    {
        [matchingData setValue:GroupName forKey:@"groupName"];
        [context save:&error];
    }
}

- (NSDictionary *)JSONFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"csvjson" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

-(void)InsertBackupLabels{
   NSDictionary* label = [self JSONFromFile];
//     @autoreleasepool{
    for(NSDictionary *item in label)
    {
      // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
       //context = [appdelegate managedObjectContext];
       
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"BackupLabel" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:[item valueForKey:@"LabelID"] forKey:@"label_id"];
        [newrecord setValue:[item valueForKey:@"LabelText"] forKey:@"label"];
        
        NSError *error;
        [context save:&error];
    }
    appdelegate.backupTranslations = [self query_alldata:@"BackupLabel"];
//     }
}
@end

