//
//  HomeMenuPopover.h
//  NN_EMSapp
//
//  Created by iWizards XI on 10/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeMenuPopover : UIView



@property (strong, nonatomic) IBOutlet UIButton *HelpBtn;
@property (strong, nonatomic) IBOutlet UIButton *SettingsBtn;
@end
