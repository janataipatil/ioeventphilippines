//
//  HomeMenuPopover.m
//  NN_EMSapp
//
//  Created by iWizards XI on 10/08/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "HomeMenuPopover.h"

@implementation HomeMenuPopover

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"HomeMenuPopover" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    
    self.HelpBtn = (UIButton *)[mainView viewWithTag:1];
    self.SettingsBtn=(UIButton *)[mainView viewWithTag:2];
    
    
    return self;
}


@end
