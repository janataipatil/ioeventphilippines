//
//  HomeViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/7/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "GIBadgeView.h"
#import "Reachability.h"
#import <ShinobiGrids/ShinobiDataGrid.h>
#import "ARSPopover.h"
#import "HomeMenuPopover.h"
#import "SettingMenu.h"
#import "AnalyticsViewController.h"
#import "TitleBar.h"
#import "MenueBar.h"
#import "EventDetails_ViewController.h"
#import "HelpViewController.h"
#import "SIGridHeaderCell.h"
#import <MessageUI/MessageUI.h>


@class HelperClass;
@interface HomeViewController : UIViewController<UISearchBarDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *EventCollectionView;

//@property (nonatomic) HelperClass *helper;
@property(strong,nonatomic) SettingMenu *settingmenuview;
@property(strong,nonatomic)HomeMenuPopover *homeMenu;
@property (nonatomic) Reachability* reachability;
@property(strong,nonatomic)IBOutlet UIButton *AddEventButtonOutlet;
@property (strong, nonatomic) IBOutlet UILabel *NoEventsAvailablelbl;

@property (strong, nonatomic) IBOutlet UIButton *toggleBtn;
- (IBAction)toggleBtn:(id)sender;

- (IBAction)logo_btn:(id)sender;
- (IBAction)add_event:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *AdvanceSearch_img;

- (IBAction)Event_AdvanceSearch:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Event_AdvanceSearch;

@property (strong, nonatomic) IBOutlet UIView *Activitylistview;

@property (nonatomic, weak) IBOutlet UIButton *Setting_btn;
- (IBAction)Setting_btn:(id)sender;
- (IBAction)notification_btn:(id)sender;
-(void)updatenotificationbadge;

- (IBAction)events_segmentcontrol:(id)sender;
- (IBAction)Activities_segmentcontrol:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *add_event;

@property (strong, nonatomic) IBOutlet UISegmentedControl *events_segmentcontrol;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Activities_segmentcontrol;

@property (strong, nonatomic) IBOutlet UISearchBar *EventSearchbar;
@property (strong, nonatomic) IBOutlet UISearchBar *ActivitySearchBar;
@property (strong, nonatomic) IBOutlet UILabel *activities_lbl;

@property (strong, nonatomic) IBOutlet UILabel *Username_lbl;

@property(nonatomic,strong)GIBadgeView *notificationbadge;
@property (strong, nonatomic) IBOutlet UIButton *notify_button;
@property(strong,nonatomic)TitleBar *TitleBarView;
@property(strong,nonatomic)MenueBar *MenueBarView;
@property (strong, nonatomic) IBOutlet UIView *activityView;

-(void) RefreshView;
-(void) getpudhmessages;

@property (strong, nonatomic) IBOutlet UITableView *EventList_TableView;

@property (strong, nonatomic) IBOutlet UIView *eventTableHeaderView;

@property (strong, nonatomic) IBOutlet UILabel *EventNamelbl_tableView;
@property (strong, nonatomic) IBOutlet UILabel *EventOwnerlbl_tableView;
@property (strong, nonatomic) IBOutlet UILabel *EventDepartmentlbl_tableView;
@property (strong, nonatomic) IBOutlet UILabel *EventStartDatelbl_tableView;
@property (strong, nonatomic) IBOutlet UILabel *EventEndDatelbl_tableView;
@property (strong, nonatomic) IBOutlet UILabel *EventStatus_tableView;

@property (strong, nonatomic) IBOutlet UIImageView *toggleBtnImage;

@property (strong, nonatomic) IBOutlet UITableView *HomeMenuTableView;

@property (strong, nonatomic) IBOutlet UIButton *clearSearch;
- (IBAction)clearSearch:(id)sender;

//SortView Buttons
@property (strong, nonatomic) IBOutlet UIButton *status_sortBtn;
@property (strong, nonatomic) IBOutlet UIButton *endDate_sortBtn;
@property (strong, nonatomic) IBOutlet UIButton *startDate_sortBtn;
@property (strong, nonatomic) IBOutlet UIButton *venue_sortBtn;
@property (strong, nonatomic) IBOutlet UIButton *owner_sortBtn;
@property (strong, nonatomic) IBOutlet UIButton *eventName_sortBtn;

- (IBAction)status_sortBtn:(id)sender;
- (IBAction)endDate_sortBtn:(id)sender;
- (IBAction)startDate_sortBtn:(id)sender;
- (IBAction)venue_sortBtn:(id)sender;
- (IBAction)owner_sortBtn:(id)sender;
- (IBAction)eventName_sortBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *endDate_sortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *status_sortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *eventName_sortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *venue_sortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *startDate_sortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *owner_sortIcon;

@end
