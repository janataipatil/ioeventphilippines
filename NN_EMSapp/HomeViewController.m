//
//  HomeViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/7/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "ShinobiCustomCell.h"
#import "NotificationViewController.h"
#import "UpsertEventVC.h"
#import "EventActivitiesViewController.h"

@interface HomeViewController ()<SDataGridDataSourceHelperDelegate>
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    ShinobiDataGrid *shinobigrid_att;
    NSPredicate *eventpredicate,*mypredicate,*teampredicate,*activityPredicate,*apprpredicate;
    NSArray *eventdetailarr;
    NSString *selectedeventid;
    NSIndexPath *selindex;
    NSString *ApprovalStr;
    NSMutableArray *ShinobiArr;
    NSString *semp;
    HelperClass *helperClass;
    dispatch_semaphore_t sem_event;
    NSString *calidentify;
    BOOL EventName;
    BOOL EventVenue;
    BOOL EventOwner;
    BOOL EventStatus;
    BOOL EventStartDate;
    BOOL EventEndDate;
   
    BOOL EventView;
}
@property (nonatomic, strong) SDataGridDataSourceHelper *datasourceHelper_att;
@end

@implementation HomeViewController
{
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    
    SLKeyChainStore *keychain;
    MSClient *client;

    NSArray *Eventlist;
    NSArray *AssignedActivitylist;
    
    NSMutableArray *ApprovedAttendees;
    NSArray *ApprovedAttendeeCopy;
    
    NSMutableArray *ActivityTotal;
    NSArray *ActivityTotalCopy;
    
    BOOL DispHomeMenu;
    
    NSString *AESPrivateKey;
    NSInteger eventcount;
    NSInteger activitycount;
    NotificationViewController *notificationview;
    HelpViewController *HelpView;
    ARSPopover *popoverController;
    
}
static NSString * const reuseIdentifier = @"eventcollectioncard";

@synthesize EventCollectionView,Activitylistview,EventSearchbar,ActivitySearchBar,activities_lbl,settingmenuview,notificationbadge,TitleBarView,MenueBarView,AddEventButtonOutlet,add_event,NoEventsAvailablelbl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helperClass = [[HelperClass alloc] init];
    
    [self reloadActivityAndAttendeeCardData];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
 
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];// for dev
    AESPrivateKey =[helperClass query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    appdelegate.homeviewController = self;
    NSLog(@"last page %@ %@",appdelegate.lstpage,appdelegate.pageno);
    
    //Changes For Toggle between Listview and Collection View
    EventView=YES;
    [_toggleBtnImage setImage:[UIImage imageNamed:@"DashBoard_Listview.png"]];

    [_EventList_TableView setHidden:YES];
    [_eventTableHeaderView setHidden:YES];
    appdelegate.EventListViewType=@"GRID";
    appdelegate.EventListFilter=@"MY";
    
    [_clearSearch setHidden:YES];
    [_HomeMenuTableView setHidden:YES];
    DispHomeMenu=NO;
    
    _homeMenu=[[HomeMenuPopover alloc]init];
    
    appdelegate.translations = [helperClass query_alldata:@"Lang_translations"];
    appdelegate.backupTranslations = [helperClass query_alldata:@"BackupLabel"];
    NSLog(@"TRnslation for :%@",[helperClass gettranslation:@"LBL_714"]);
    
    
    
    NSLog(@"TRnslation for :%@",[helperClass gettranslation:@"LBL_714"]);
    
    
    NSLog(@"Count: %lu",(unsigned long)[appdelegate.translations count] );
    
    TitleBarView = [[TitleBar alloc]initWithFrame:CGRectMake(0, 20, 1024, 53)];
    TitleBarView.SettingButton.hidden = NO;
    
    TitleBarView.NotificationButton.hidden=NO;
    
    
    
    
    //TitleBarView.UsernameLbl.text =[NSString stringWithFormat:@"%@,%@",[helper gettranslation:@"LBL_585"],[helper getUsername]];
    
   // TitleBarView.UsernameLbl.text =[NSString stringWithFormat:@"%@",[helper getUsername]];
    
    
    if ([[helperClass getUsername] length]>0)
    {
        TitleBarView.UsernameLbl.text =[NSString stringWithFormat:@"%@,%@",[helperClass gettranslation:@"LBL_585"],[helperClass getUsername]];
    }
    else
    {
        TitleBarView.UsernameLbl.text = @" ";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helperClass gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helperClass gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
    [TitleBarView.HelpButton addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
   // [TitleBarView.]
    
    
    [TitleBarView.NotificationButton addTarget:self action:@selector(NotificationAction) forControlEvents:UIControlEventTouchUpInside];

    MenueBarView = [[MenueBar alloc]initWithFrame:CGRectMake(0, 20, 100, 748)];
    //MenueBarView.HomeButton.backgroundColor = [UIColor colorWithRed:46.0/255.0f green:176.0/255.0f blue:238.0/255.0f alpha:1.0f];
   
    MenueBarView.HomeButton.backgroundColor = [helperClass LightBlueColour];
    
    
    //    Setting_btn
    [TitleBarView.SettingButton addTarget:self action:@selector(Setting_btn:) forControlEvents:UIControlEventTouchUpInside];
//    [TitleBarView.HomeMenuButton addTarget:self action:@selector(displayHomeMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [MenueBarView.CalendarButton addTarget:self action:@selector(CalendarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivitiesButton addTarget:self action:@selector(ActivityButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.SpeakerButton addTarget:self action:@selector(SpeakerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.AttendeeButton addTarget:self action:@selector(AttendeeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [MenueBarView.ActivityPlanButton addTarget:self action:@selector(ActivityPlanButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    MenueBarView.VersionNumber.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
    MenueBarView.ActivityPlanButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    MenueBarView.ActivityPlanButton.titleLabel.numberOfLines = 2;
    
    
    [self langsetuptranslations];
    
    
    
    appdelegate.UserRolesArray = [[helperClass query_data:@"name" inputdata:@"EMSUserRoleActions" entityname:@"S_Config_Table"] componentsSeparatedByString:@"|"];
    
    NSLog(@"Roles array is %@",appdelegate.UserRolesArray);
    
    
    if (![appdelegate.UserRolesArray containsObject:@"ACT_ADM"])
    {
        MenueBarView.ActivityPlanButton.hidden = YES;
    }
    if (![appdelegate.UserRolesArray containsObject:@"E_MNG"] )
    {
          add_event.hidden = YES;
    }
    
    [self.view addSubview:MenueBarView];
    [self.view addSubview:TitleBarView];
    
    settingmenuview = [[SettingMenu alloc] init];
    
    self.EventCollectionView.allowsMultipleSelection = NO;
    
    Eventlist = [helperClass query_alldata:@"Event"];
    
    [self reloadActivityAndAttendeeCardData];
    
    
//    AssignedActivitylist = [helperClass query_alldata:@"Activities"];
//
//    for(int i=0;i<Eventlist.count;i++)
//    {
//        NSArray *TEMP=[helperClass query_predicate:@"Event_Attendee" name:@"eventid" value:[Eventlist[i] valueForKey:@"eventid"]];
//
//       // for(int j=0;j<TEMP.count;j++)
//       //{
//            [ApprovedAttendees addObject:TEMP];
//        //}
//    }
//    ApprovedAttendeeCopy=[ApprovedAttendees mutableCopy];
//
//    for(int i=0;i<Eventlist.count;i++)
//    {
//        NSArray *TEMP=[helperClass query_predicate:@"Activities" name:@"eventid" value:[Eventlist[i] valueForKey:@"eventid"]];
////        for (int j=0; j<TEMP.count;j++)
////        {
//           [ActivityTotal addObject:TEMP];
////        }
//    }
//    ActivityTotalCopy=[ActivityTotal  mutableCopy];
    
    
    if (Eventlist.count>0)
    {
        [self Setuphomeview];
    }
    
    [EventSearchbar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.EventSearchbar valueForKey:@"_searchField"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helperClass DarkBlueColour];
    searchField.placeholder = [helperClass gettranslation:@"LBL_224"];
    
    [ActivitySearchBar setBackgroundImage:[UIImage new]];
    ActivitySearchBar.barTintColor = [UIColor colorWithRed:220/255.0 green:228/255.0 blue:240/255.0 alpha:1] ;
    UITextField *actsearchField = [self.ActivitySearchBar valueForKey:@"_searchField"];
    //actsearchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    
    actsearchField.textColor = [helperClass DarkBlueColour];
    actsearchField.backgroundColor = [UIColor colorWithRed:220/255.0 green:228/255.0 blue:240/255.0 alpha:1] ;
    actsearchField.placeholder = [helperClass gettranslation:@"LBL_224"];
    
    [_events_segmentcontrol setSelectedSegmentIndex:0];
    [_events_segmentcontrol sendActionsForControlEvents:UIControlEventValueChanged];
    
    [_Activities_segmentcontrol setSelectedSegmentIndex:0];
    [_Activities_segmentcontrol sendActionsForControlEvents:UIControlEventValueChanged];
    
    ActivitySearchBar.delegate = self;
    EventSearchbar.delegate = self;
    
   // _Username_lbl.text = [helper getUsername];
    
    NSString *pushregister_check = [[NSUserDefaults standardUserDefaults]valueForKey:@"Pushregister"];
    if([pushregister_check isEqualToString:@"N"])
    {
        [appdelegate Register_pushnotification];
    }
    
    
    NSArray *pushnotificationdatacountarray = [helperClass query_alldata:@"Push_Notification_Table"];
    int notifycount = 0;
    for (int i =0; i<[pushnotificationdatacountarray count]; i++)
    {
        if([[pushnotificationdatacountarray[i] valueForKey:@"meassage_read"] isEqualToString:@"N"])
        {
            notifycount++;
        }
    }
//    [self performSegueWithIdentifier:@"HometoCalendarView" sender:self];
    
    appdelegate.badgevalue = notifycount;
    //    appdelegate.badgevalue = 5;
    
    notificationbadge = [GIBadgeView new];
    notificationbadge.backgroundColor = [UIColor colorWithRed:251/255.0 green:45/255.0 blue:62/255.0 alpha:1.0];
    notificationbadge.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    notificationbadge.topOffset = 9.0f;
    notificationbadge.rightOffset = 30.0f;
    [notificationbadge setBadgeValue:appdelegate.badgevalue];
    
    //[notificationview setBadgeValue:badgevalue];
    [_notify_button addSubview:self.notificationbadge];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateDeletionMode:)];
    longPress.delegate = self;
    
    [self.EventCollectionView addGestureRecognizer:longPress];
    
    
    appdelegate.ProcessALERT = [helperClass gettranslation:@"LBL_590"];
    appdelegate.ProcessBUTTON = [helperClass gettranslation:@"LBL_462"];
    
    NSString *DefaultView = [helperClass query_data:@"name" inputdata:@"DefaultEventView" entityname:@"S_Config_Table"];
    
    if ([DefaultView isEqualToString:@"CalendarView"])
    {
        [self performSegueWithIdentifier:@"HometoCalendarView" sender:self];
    }
    
  
   
}

-(void)reloadActivityAndAttendeeCardData
{
    AssignedActivitylist = [helperClass query_alldata:@"Activities"];
    ApprovedAttendees = [[NSMutableArray alloc]init];
    ActivityTotal = [[NSMutableArray alloc]init];
    for(int i=0;i<Eventlist.count;i++)
    {
        NSArray *TEMP=[helperClass query_predicate:@"Event_Attendee" name:@"eventid" value:[Eventlist[i] valueForKey:@"eventid"]];
        
        // for(int j=0;j<TEMP.count;j++)
        //{
        [ApprovedAttendees addObject:TEMP];
        //}
    }
    ApprovedAttendeeCopy=[ApprovedAttendees mutableCopy];
    
    for(int i=0;i<Eventlist.count;i++)
    {
        NSArray *TEMP=[helperClass query_predicate:@"Activities" name:@"eventid" value:[Eventlist[i] valueForKey:@"eventid"]];
        //        for (int j=0; j<TEMP.count;j++)
        //        {
        [ActivityTotal addObject:TEMP];

        
        //        }
    }
    ActivityTotalCopy=[ActivityTotal  mutableCopy];
    
}

-(void)viewWillAppear:(BOOL)animated
{
   }
-(void)langsetuptranslations
{
  [MenueBarView.HomeButton setTitle:[helperClass gettranslation:@"LBL_143"] forState:UIControlStateNormal];
    [MenueBarView.ActivitiesButton setTitle:[helperClass gettranslation:@"LBL_003"] forState:UIControlStateNormal];
    [MenueBarView.CalendarButton setTitle:[helperClass gettranslation:@"LBL_060"] forState:UIControlStateNormal];
    [MenueBarView.SpeakerButton setTitle:[helperClass gettranslation:@"LBL_236"] forState:UIControlStateNormal];
    [MenueBarView.AttendeeButton setTitle:[helperClass gettranslation:@"LBL_054"] forState:UIControlStateNormal];
    [MenueBarView.ActivityPlanButton setTitle:[helperClass gettranslation:@"LBL_007"] forState:UIControlStateNormal];
    
    
    [_events_segmentcontrol setTitle:[helperClass gettranslation:@"LBL_173"] forSegmentAtIndex:0];
    [_events_segmentcontrol setTitle:[helperClass gettranslation:@"LBL_040"] forSegmentAtIndex:1];
    [_events_segmentcontrol setTitle:[helperClass gettranslation:@"LBL_172"] forSegmentAtIndex:2];
    
    [_Activities_segmentcontrol setTitle:[helperClass gettranslation:@"LBL_171"] forSegmentAtIndex:0];
    [_Activities_segmentcontrol setTitle:[helperClass gettranslation:@"LBL_256"] forSegmentAtIndex:1];
    
    _EventNamelbl_tableView.text=[helperClass gettranslation:@"LBL_131"];
    _EventStartDatelbl_tableView.text=[helperClass gettranslation:@"LBL_239"];
    _EventEndDatelbl_tableView.text=[helperClass gettranslation:@"LBL_122"];
    _EventStatus_tableView.text=[helperClass gettranslation:@"LBL_242"];
    _EventOwnerlbl_tableView.text=[helperClass gettranslation:@"LBL_187"];
    _EventDepartmentlbl_tableView.text=[helperClass gettranslation:@"LBL_277"];
    
    NoEventsAvailablelbl.text = [helperClass gettranslation:@"LBL_671"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Setuphomeview
{
    AssignedActivitylist = [helperClass query_alldata:@"Activities"];
    [self setupaactivityShinobi];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
   
    if (Eventlist.count == 0)
    {
        NoEventsAvailablelbl.hidden = NO;
    }
    else
    {
        NoEventsAvailablelbl.hidden = YES;
    }
    return Eventlist.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIView *contentView = cell.contentView;
    UILabel *eventname_lv = (UILabel *) [contentView viewWithTag:1];
    UILabel *startdate_lv = (UILabel *) [contentView viewWithTag:4];
    UILabel *enddate_lv = (UILabel *) [contentView viewWithTag:5];
    UILabel *status_lv = (UILabel *) [contentView viewWithTag:8];
    UILabel *venue_lv = (UILabel *) [contentView viewWithTag:9];
    UITextView *eventdetails_tv = (UITextView *) [contentView viewWithTag:10];
    UILabel *owner_lv = (UILabel *) [contentView viewWithTag:12];
    UILabel *attendee_total_lv = (UILabel *) [contentView viewWithTag:16];
    UILabel *attendee_approved_lv = (UILabel *) [contentView viewWithTag:20];
    UILabel *activity_total_lv = (UILabel *) [contentView viewWithTag:18];
    UILabel *activity_approved_lv = (UILabel *) [contentView viewWithTag:22];
    UILabel *dept_lv = (UILabel *) [contentView viewWithTag:2001];

    
    UIButton *graph_btn = (UIButton *) [contentView viewWithTag:25];
    [graph_btn setTitle:[helperClass gettranslation:@"LBL_129"] forState:UIControlStateNormal];
    [graph_btn addTarget:self action:@selector(graph_btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *delete_btn = (UIButton *) [contentView viewWithTag:26];
    [delete_btn addTarget:self action:@selector(delete_btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    delete_btn.hidden = YES;
    
   /* UIButton *approval_btn = (UIButton *) [contentView viewWithTag:27];
    
    
    
    [approval_btn addTarget:self action:@selector(approval_btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSLog(@"approvalflag: %@",[Eventlist[indexPath.row] valueForKey:@"approvalflag"]);
    
    
    [approval_btn setHighlighted:NO];
    
    
    if ([[Eventlist[indexPath.row] valueForKey:@"approvalflag"] isEqualToString:@"A"])
    {

       [approval_btn setTitle:@"Attendee" forState:UIControlStateNormal];
        
    }
    else if([[Eventlist[indexPath.row] valueForKey:@"approvalflag"] isEqualToString:@"E"])
    {

        [approval_btn setTitle:@"Event" forState:UIControlStateNormal];
        
    }
    else if([[Eventlist[indexPath.row] valueForKey:@"approvalflag"] isEqualToString:@"B"])
    {

        [approval_btn setTitle:@"Event, Attendee" forState:UIControlStateNormal];
    }
    else
    {
        [approval_btn setTitle:@"" forState:UIControlStateNormal];
    }*/
    
    
    
    UILabel *approval_lbl = (UILabel *)[contentView viewWithTag:27];
    if ([[Eventlist[indexPath.row] valueForKey:@"approvalflag"] isEqualToString:@"A"])
    {
       // approval_lbl.text = @"Attendee";
        approval_lbl.text = [helperClass gettranslation:@"LBL_054"];
        
    }
    else if([[Eventlist[indexPath.row] valueForKey:@"approvalflag"] isEqualToString:@"E"])
    {
       // approval_lbl.text = @"Event";
       approval_lbl.text = [helperClass gettranslation:@"LBL_386"];
        
    }
    else if([[Eventlist[indexPath.row] valueForKey:@"approvalflag"] isEqualToString:@"B"])
    {
       // approval_lbl.text = @"Event, Attendee";
        approval_lbl.text = [helperClass gettranslation:@"LBL_387"];
    }
    else
    {
        approval_lbl.text = @"";
       
    }
    
    dept_lv.text=[Eventlist[indexPath.row] valueForKey:@"department"];
    
    NSString *eveid = [Eventlist[indexPath.row] valueForKey:@"eventid"];
    
    
    if (eveid.length == 0)
    {
        NSArray *Eventlist1 = [helperClass query_alldata:@"Event"];
        NSLog(@"eventid: %@",[Eventlist[indexPath.row] valueForKey:@"name"]);
        NSLog(@"eventid: %@",[Eventlist[indexPath.row] valueForKey:@"desc"]);
    }
    
    
    
  //  NSString *attendee_total = [NSString stringWithFormat:@"%lu",(unsigned long)[[helperClass query_predicate:@"Event_Attendee" name:@"eventid" value:[Eventlist[indexPath.row] valueForKey:@"eventid"]] count]];
    
    NSString *attendee_total = [NSString stringWithFormat:@"%lu",(unsigned long)[ApprovedAttendeeCopy[indexPath.row] count]];
//
    NSPredicate *attendeepredicate = [NSPredicate predicateWithFormat:@"statuslic like %@",@"Approved"];
//
    NSString *attendee_approved =[NSString stringWithFormat:@"%lu",(unsigned long)[[ApprovedAttendeeCopy[indexPath.row] filteredArrayUsingPredicate:attendeepredicate] count]];
    
//    NSString *attendee_approved = [NSString stringWithFormat:@"%lu",(unsigned long) [[[helperClass query_predicate:@"Event_Attendee" name:@"eventid" value:[Eventlist[indexPath.row] valueForKey:@"eventid"]] filteredArrayUsingPredicate:attendeepredicate] count]];

   // NSString *activity_total = [NSString stringWithFormat:@"%lu",(unsigned long)[[helperClass query_predicate:@"Activities" name:@"eventid" value:[Eventlist[indexPath.row] valueForKey:@"eventid"]] count]];
   
    NSString *activity_total = [NSString stringWithFormat:@"%lu",(unsigned long)[ActivityTotalCopy[indexPath.row] count]];
    
    NSPredicate *activitypredicate = [NSPredicate predicateWithFormat:@"statuslic like %@",@"Completed"];
    
    NSString *activity_approved = [NSString stringWithFormat:@"%lu",(unsigned long) [[ActivityTotalCopy[indexPath.row] filteredArrayUsingPredicate:activitypredicate]count]];
    
    [startdate_lv setText:[helperClass formatingdate:[Eventlist[indexPath.row] valueForKey:@"startdate"] datetime:@"FormatDate"]];
    [enddate_lv setText:[helperClass formatingdate:[Eventlist[indexPath.row] valueForKey:@"enddate"] datetime:@"FormatDate"]];
    
    [eventname_lv setText:[Eventlist[indexPath.row] valueForKey:@"name"]];
    [status_lv setText:[Eventlist[indexPath.row] valueForKey:@"status"]];
    status_lv.textColor = [helperClass getstatuscolor:[Eventlist[indexPath.row] valueForKey:@"statuslic"] entityname:@"Event"];
    [venue_lv setText:[Eventlist[indexPath.row] valueForKey:@"venue"]];
    [eventdetails_tv setText:[Eventlist[indexPath.row] valueForKey:@"desc"]];
    [owner_lv setText:[Eventlist[indexPath.row] valueForKey:@"ownedby"]];
    
    [attendee_total_lv setText:attendee_total];
    [attendee_approved_lv setText:attendee_approved];
    [activity_total_lv setText:activity_total];
    [activity_approved_lv setText:activity_approved];
    
    
    

    
    appdelegate.translations = [helperClass query_alldata:@"Lang_translations"];

    UILabel *startdate_lbl = (UILabel *) [contentView viewWithTag:1901];
    UILabel *enddate_lbl = (UILabel *) [contentView viewWithTag:1902];
    UILabel *status_lbl = (UILabel *) [contentView viewWithTag:1903];
    UILabel *owner_lbl = (UILabel *) [contentView viewWithTag:1904];
    UILabel *venue_lbl = (UILabel *) [contentView viewWithTag:1905];
    UILabel *approvalpend_lbl = (UILabel *) [contentView viewWithTag:1906];
    UILabel *attendee_lbl = (UILabel *) [contentView viewWithTag:1907];
    UILabel *activity_lbl = (UILabel *) [contentView viewWithTag:1908];
    UILabel *atttotal_lbl = (UILabel *) [contentView viewWithTag:1909];
    UILabel *attappr_lbl = (UILabel *) [contentView viewWithTag:1910];
    UILabel *acttotal_lbl = (UILabel *) [contentView viewWithTag:1911];
    UILabel *actappr_lbl = (UILabel *) [contentView viewWithTag:1912];
    UILabel *dept_lbl = (UILabel *) [contentView viewWithTag:2000];
    
    
    startdate_lbl.text = [helperClass gettranslation:@"LBL_239"];
    enddate_lbl.text = [helperClass gettranslation:@"LBL_122"];
    status_lbl.text = [helperClass gettranslation:@"LBL_242"];
    owner_lbl.text = [helperClass gettranslation:@"LBL_187"];
    venue_lbl.text = [helperClass gettranslation:@"LBL_277"];
    approvalpend_lbl.text = [helperClass gettranslation:@"LBL_045"];
    attendee_lbl.text = [helperClass gettranslation:@"LBL_054"];
    activity_lbl.text = [helperClass gettranslation:@"LBL_003"];
    atttotal_lbl.text = [helperClass gettranslation:@"LBL_262"];
    attappr_lbl.text = [helperClass gettranslation:@"LBL_047"];
    acttotal_lbl.text = [helperClass gettranslation:@"LBL_262"];
    actappr_lbl.text = [helperClass gettranslation:@"LBL_068"];
    dept_lbl.text = [helperClass gettranslation:@"LBL_092"];
    
 
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected row is : %ld",(long)indexPath.row);
    appdelegate.eventid = [Eventlist[indexPath.row] valueForKey:@"eventid"];
    
    appdelegate.eventStatus = [Eventlist[indexPath.row] valueForKey:@"statuslic"];
    
  //  appdelegate.eventStartDate=[Eventlist[indexPath.row] valueForKey:@"startdate"];
   // appdelegate.eventEndDate=[Eventlist[indexPath.row] valueForKey:@"enddate"];
    
    
    NSLog(@"Event id is %@",appdelegate.eventid);
    appdelegate.EventUserRoleArray = [[Eventlist[indexPath.row] valueForKey:@"eMSUserRoleActions"] componentsSeparatedByString:@"|"];
    [[NSUserDefaults standardUserDefaults]setObject:appdelegate.eventid forKey:@"eventid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self performSegueWithIdentifier:@"HometoDetailsView" sender:self];
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


#pragma mark tableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView ==_HomeMenuTableView)
    {
        return 2;
    }
    else
    {
        return Eventlist.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
           NSString *cellIdentifier=@"EventTableViewCell";
    
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        NSDateFormatter *EventList_DateFormatter=[[NSDateFormatter alloc]init];
        [EventList_DateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [EventList_DateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    
        UILabel *EventName=(UILabel*)[cell viewWithTag:1];
        EventName.text=[Eventlist[indexPath.row] valueForKey:@"name"];
    
    
        UILabel *EventStartDate=(UILabel*)[cell viewWithTag:4];
    
        NSDate *Event_startDate=[EventList_DateFormatter dateFromString:[Eventlist[indexPath.row] valueForKey:@"startdate"]];
        [EventList_DateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        EventStartDate.text=[EventList_DateFormatter stringFromDate:Event_startDate];
    
        [EventList_DateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        UILabel *EventEndDate=(UILabel*)[cell viewWithTag:5];
        NSDate *Event_endDate=[EventList_DateFormatter dateFromString:[Eventlist[indexPath.row] valueForKey:@"enddate"]];
        [EventList_DateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        EventEndDate.text=[EventList_DateFormatter stringFromDate:Event_endDate];
    
        UILabel *EventStatus=(UILabel*)[cell viewWithTag:6];
        EventStatus.textColor=[helperClass getstatuscolor:[Eventlist[indexPath.row] valueForKey:@"statuslic"] entityname:@"Event"];
        EventStatus.text=[Eventlist[indexPath.row] valueForKey:@"status"];
    
        UILabel *EventOwner=(UILabel*)[cell viewWithTag:2];
        EventOwner.text=[Eventlist[indexPath.row] valueForKey:@"ownedby"];
    
        UILabel *EventVenue=(UILabel*)[cell viewWithTag:3];
        EventVenue.text=[Eventlist[indexPath.row] valueForKey:@"venue"];
    
    
//    eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",[Eventlist[indexPath.row] valueForKey:@"eventid"]]];
//        if (eventvenue.count >0)
//        {
//            EventVenue.text = [eventvenue[0] valueForKey:@"location"];
//        }
//        else
//        {
//            EventVenue.text = @"Pick Event Location";
//        }
            return cell;
    }




- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
        {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
        {
            [tableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)])
        {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        if (indexPath.row % 2)
        {
            cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
        }
        else
        {
            cell.contentView.backgroundColor = [UIColor whiteColor] ;
        }
        
        
    }



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

        appdelegate.eventid=[Eventlist[indexPath.row] valueForKey:@"eventid"];
        appdelegate.EventUserRoleArray = [[Eventlist[indexPath.row] valueForKey:@"eMSUserRoleActions"] componentsSeparatedByString:@"|"];
    
        [[NSUserDefaults standardUserDefaults]setObject:appdelegate.eventid forKey:@"eventid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    
    
        [self performSegueWithIdentifier:@"HometoDetailsView" sender:self];

}




#pragma mark menu button action
- (IBAction)CalendarButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"HometoCalendarView" sender:self];
}

- (IBAction)ActivityButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"HometoActivityView" sender:self];
}
- (IBAction)SpeakerButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"HometoSpeakerView" sender:self];
}

- (IBAction)ActivityPlanButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"HomeToActivityPlan" sender:self];
}
- (IBAction)AttendeeButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"HometoallAttendeesView" sender:self];
}

- (IBAction)toggleBtn:(id)sender
{

    EventView=!EventView;
    
    if(!EventView)
    {
        
        
        appdelegate.EventListViewType=@"LIST";
        [_eventTableHeaderView setHidden:NO];
        [_EventList_TableView setHidden:NO];
        [EventCollectionView setHidden:YES];
   
        [self RefreshView];
        
        [_EventList_TableView reloadData];
        [_toggleBtnImage setImage:[UIImage imageNamed:@"Dashboard_GridView.png"]];

    }
    else
    {
        
        [_EventList_TableView setHidden:YES];
        [EventCollectionView setHidden:NO];
        [_eventTableHeaderView setHidden:YES];
        
        appdelegate.EventListViewType=@"GRID";
        [_toggleBtnImage setImage:[UIImage imageNamed:@"DashBoard_Listview.png"]];
       
        [self RefreshView];
        
    
        [_EventList_TableView reloadData];
        
        //[_toggleBtn setImage:[UIImage imageNamed:@"DashBoard_Listview.png"] forState:UIControlStateNormal];
               //[_events_segmentcontrol setHidden:NO];
       // [_Event_AdvanceSearch setHidden:YES];
        //[_AdvanceSearch_img setHidden:YES];
        
       // [self RefreshView];

    }
    


}

- (IBAction)logo_btn:(id)sender
{
}

- (IBAction)add_event:(id)sender
{
    NSString *networkstatus = [helperClass checkinternetconnection];
    
    if ([networkstatus isEqualToString:@"Y"])
    {
        [self performSegueWithIdentifier:@"Addnewevent" sender:self];
    }
    else
    {
        [helperClass showalert:[helperClass gettranslation:@"LBL_441"] message:[helperClass gettranslation:@"LBL_630"] action:[helperClass gettranslation:@"LBL_462"]];
    }
}

-(IBAction)graph_btnClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.EventCollectionView];
    NSIndexPath *indexPath = [self.EventCollectionView indexPathForItemAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        selectedeventid = [Eventlist[indexPath.row] valueForKey:@"eventid"];
    }
    
    [self performSegueWithIdentifier:@"HometoAnalyticsView" sender:self];
}
-(IBAction)delete_btnClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.EventCollectionView];
    NSIndexPath *indexPath = [self.EventCollectionView indexPathForItemAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        selectedeventid = [Eventlist[indexPath.row] valueForKey:@"eventid"];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helperClass gettranslation:@"LBL_584"] message:[helperClass gettranslation:@"LBL_569"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[helperClass gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
        
        
        
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",selectedeventid];
//        [helper delete_predicate:@"Event" predicate:predicate];
//        [self RefreshView];
    }
    
//    [self performSegueWithIdentifier:@"HometoAnalyticsView" sender:self];
}
-(IBAction)approval_btnClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.EventCollectionView];
    NSIndexPath *indexPath = [self.EventCollectionView indexPathForItemAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        selectedeventid = [Eventlist[indexPath.row] valueForKey:@"eventid"];
    }
    
//    [self performSegueWithIdentifier:@"HometoAnalyticsView" sender:self];
}
//--------------- Setting Menu controls-----------------------------

- (IBAction)Setting_btn:(id)sender
{
    popoverController = [ARSPopover new];
    popoverController.sourceView = self.Setting_btn;
//    popoverController.sourceRect = CGRectMake(900, 50, 0, 0);
    
    
    
    popoverController.sourceRect = CGRectMake(CGRectGetMidX(self.Setting_btn.bounds), CGRectGetMaxY(self.Setting_btn.bounds), 0, 0);
    popoverController.contentSize = CGSizeMake(300, 500);
    popoverController.arrowDirection = UIPopoverArrowDirectionUp;
    popoverController.backgroundColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
   // popoverController.backgroundColor = [helper LightBlueColour];
    
    [self presentViewController:popoverController animated:YES completion:^{
        [popoverController insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight)
        {
            
            
            
            settingmenuview.admin_ulbl.text = [helperClass gettranslation:@"LBL_033"];
            settingmenuview.fdrimage_ulbl.text = [helperClass gettranslation:@"LBL_667"];
            settingmenuview.dcal_ulbl.text = [helperClass gettranslation:@"LBL_078"];
            settingmenuview.syncDB_ulbl.text = [helperClass gettranslation:@"LBL_668"];
            settingmenuview.fdrlastsync_ulbl.text = [helperClass gettranslation:@"LBL_156"];
            settingmenuview.mlastsync_ulbl.text = [helperClass gettranslation:@"LBL_156"];
            settingmenuview.errormsg_ulbl.text = [helperClass gettranslation:@"LBL_126"];
            

            UIButton *fdr_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [fdr_btn setTitle:[helperClass gettranslation:@"LBL_140"] forState:UIControlStateNormal];
            fdr_btn.frame = CGRectMake(12, 269, 115, 30);
            fdr_btn.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1];
            [fdr_btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
            
           // [fdr_btn setTitleColor:[UIColor colorWithRed:99/255.0 green:118/255.0 blue:140/255.0 alpha:1]forState:UIControlStateNormal];
            [fdr_btn setTitleColor:[helperClass DarkBlueColour] forState:UIControlStateNormal];
            fdr_btn.layer.cornerRadius = 5.0;
            fdr_btn.tag = 1;
            
            
            UIButton *msync_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [msync_btn setTitle:[helperClass gettranslation:@"LBL_091"] forState:UIControlStateNormal];
            msync_btn.frame = CGRectMake(12, 309, 115, 30);
            msync_btn.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1];
            [msync_btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
          
           // [msync_btn setTitleColor:[UIColor colorWithRed:99/255.0 green:118/255.0 blue:140/255.0 alpha:1]forState:UIControlStateNormal];
            
            [msync_btn setTitleColor:[helperClass DarkBlueColour] forState:UIControlStateNormal];
            msync_btn.layer.cornerRadius = 5.0;
            msync_btn.tag = 2;

            UIButton *show_message = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [show_message setTitle:[helperClass gettranslation:@"LBL_234"] forState:UIControlStateNormal];
            show_message.frame = CGRectMake(12, 398, 115, 30);
            show_message.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1];
            [show_message.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
           // [show_message setTitleColor:[UIColor colorWithRed:99/255.0 green:118/255.0 blue:140/255.0 alpha:1]forState:UIControlStateNormal];
            
            [show_message setTitleColor:[helperClass DarkBlueColour] forState:UIControlStateNormal];
            show_message.layer.cornerRadius = 5.0;
            show_message.tag = 3;
            
            UIButton *sendLogs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [sendLogs setTitle:[helperClass gettranslation:@"LBL_936"] forState:UIControlStateNormal];
            sendLogs.frame = CGRectMake(157, 95, 115, 30);
            sendLogs.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1];
            [sendLogs.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
            [sendLogs setTitleColor:[helperClass DarkBlueColour] forState:UIControlStateNormal];
            sendLogs.layer.cornerRadius = 5.0;
            sendLogs.tag = 9;

            UIButton *clear_message = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [clear_message setTitle:[helperClass gettranslation:@"LBL_065"] forState:UIControlStateNormal];
            clear_message.frame = CGRectMake(157, 398, 115, 30);
            clear_message.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1];
            [clear_message.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
           
            //[clear_message setTitleColor:[UIColor colorWithRed:99/255.0 green:118/255.0 blue:140/255.0 alpha:1]forState:UIControlStateNormal];
            
            [clear_message setTitleColor:[helperClass DarkBlueColour] forState:UIControlStateNormal];
            
            clear_message.layer.cornerRadius = 5.0;
            clear_message.tag = 4;

            UIButton *uploadlogs_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [uploadlogs_btn setTitle:[helperClass gettranslation:@"LBL_162"] forState:UIControlStateNormal];
            uploadlogs_btn.frame = CGRectMake(12, 455, 276, 30);
            uploadlogs_btn.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1];
            [uploadlogs_btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
            //[uploadlogs_btn setTitleColor:[UIColor colorWithRed:99/255.0 green:118/255.0 blue:140/255.0 alpha:1]forState:UIControlStateNormal];
            
            [uploadlogs_btn setTitleColor:[helperClass DarkBlueColour] forState:UIControlStateNormal];
            uploadlogs_btn.layer.cornerRadius = 5.0;
            uploadlogs_btn.tag = 5;

           /* UISegmentedControl *defaultview_segment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[helper gettranslation:@"LBL_160"],[helper gettranslation:@"LBL_060"],nil]];
            defaultview_segment.frame = CGRectMake(12, 95, 143, 29);
            defaultview_segment.selectedSegmentIndex = 0;
            defaultview_segment.tintColor = [UIColor whiteColor];
            defaultview_segment.backgroundColor = [UIColor clearColor];
            defaultview_segment.tag = 6;
            [defaultview_segment addTarget:self action:@selector(segmentclicked:) forControlEvents: UIControlEventValueChanged];*/
            
            UISegmentedControl *defaultcalview_segment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[helperClass gettranslation:@"LBL_075"], [helperClass gettranslation:@"LBL_281"],[helperClass gettranslation:@"LBL_170"],nil]];
            defaultcalview_segment.frame = CGRectMake(12, 182, 264, 29);
            defaultcalview_segment.selectedSegmentIndex = 0;
            defaultcalview_segment.tintColor = [UIColor whiteColor];
            defaultcalview_segment.backgroundColor = [UIColor clearColor];
            defaultcalview_segment.tag = 7;
            [defaultcalview_segment addTarget:self action:@selector(segmentclicked:) forControlEvents: UIControlEventValueChanged];
            
           // CGRect myFrame = CGRectMake(232.0f, 95.0f, 51.0f, 31.0f);
            CGRect myFrame = CGRectMake(12.0f, 95.0f, 51.0f, 31.0f);
            UISwitch *FDRimagesSwitch  = [[UISwitch alloc] initWithFrame:myFrame];
            [FDRimagesSwitch setOn:YES];
            FDRimagesSwitch.transform = CGAffineTransformMakeScale(0.80, 0.80);
            FDRimagesSwitch.tag = 8;
            FDRimagesSwitch.onTintColor = [UIColor colorWithRed:94/255.0F green:145/255.0F blue:121/255.0F alpha:1.0F];
            
            
            [FDRimagesSwitch addTarget:self action:@selector(switchIsChanged:)forControlEvents:UIControlEventValueChanged];
            [fdr_btn addTarget:self action:@selector(menubtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [msync_btn addTarget:self action:@selector(menubtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [show_message addTarget:self action:@selector(menubtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [clear_message addTarget:self action:@selector(menubtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [uploadlogs_btn addTarget:self action:@selector(menubtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [sendLogs addTarget:self action:@selector(menubtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [popover.view addSubview:settingmenuview];
            [popover.view addSubview:fdr_btn];
            [popover.view addSubview:msync_btn];
            [popover.view addSubview:show_message];
            [popover.view addSubview:clear_message];
            [popover.view addSubview:uploadlogs_btn];
            [popover.view addSubview:sendLogs];
           // [popover.view addSubview:defaultview_segment];
            [popover.view addSubview:FDRimagesSwitch];
            [popover.view addSubview:defaultcalview_segment];
            
            
            
            //get Vales for Adminview
            
            //NSString *DefaultEventView = [helper query_data:@"name" inputdata:@"DefaultEventView" entityname:@"S_Config_Table"];
            NSString *DefaultCalendarView = [helperClass query_data:@"name" inputdata:@"DefaultCalendarView" entityname:@"S_Config_Table"];
            NSString *GalleryFDRdownload = [helperClass query_data:@"name" inputdata:@"GalleryFDRdownload" entityname:@"S_Config_Table"];
            
            
            
            NSArray *userarray= [helperClass query_alldata:@"User"];
            userarray=[userarray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"username like %@",keychain[@"username"]]];
            
            if ([userarray count]>0)
            {
                settingmenuview.fdr_lastsync.text = [helperClass formatingdate:[userarray[0] valueForKey:@"lastmobilesync"] datetime:@"FormatDate"];
                settingmenuview.msync_lastsync.text = [helperClass formatingdate:[userarray[0] valueForKey:@"lastmobiledeltasync"] datetime:@"FormatDate"];
            }
            
            
           /* if (DefaultEventView.length >0)
            {
                if ([DefaultEventView isEqualToString:@"ListView"])
                {
                    defaultview_segment.selectedSegmentIndex = 0;
                }
                else if ([DefaultEventView isEqualToString:@"CalendarView"])
                {
                    defaultview_segment.selectedSegmentIndex = 1;
                }
            }*/
            
            
            if (DefaultCalendarView.length >0)
            {
                if ([DefaultCalendarView isEqualToString:@"DayPlannerView"])
                {
                    defaultcalview_segment.selectedSegmentIndex = 0;
                }
                else if ([DefaultCalendarView isEqualToString:@"WeekPlannerView"])
                {
                    defaultcalview_segment.selectedSegmentIndex = 1;
                }
                else if ([DefaultCalendarView isEqualToString:@"MonthPlannerView"])
                {
                    defaultcalview_segment.selectedSegmentIndex = 2;
                }
            }
            
            if ([GalleryFDRdownload isEqualToString:@"YES"])
            {
                [FDRimagesSwitch setOn:YES animated:YES];
            }
            else
            {
                [FDRimagesSwitch setOn:NO animated:YES];
            }
           

        }];
    }];
}


-(IBAction)menubtnClicked:(id)sender
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [networkReachability currentReachabilityStatus];
    
    NSInteger i = [sender tag];
    if (i == 1)
    {
        helperClass = [[HelperClass alloc] init];
        
        appdelegate.sem = dispatch_semaphore_create(0);
        [popoverController closePopover];
        
        while (dispatch_semaphore_wait( appdelegate.sem , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
        
        if(remoteHostStatus == NotReachable)
        {
            [ToastView showToastInParentView:self.view withText:[helperClass gettranslation:@"LBL_630"] withDuaration:2.0];
        }
        else
        {
            [helperClass showWaitCursor:[helperClass gettranslation:@"LBL_373"]];
            appdelegate.PerformDeltaSync = @"NO";
            
            [self relogintosystem];
            
            [helperClass get_EMS_data];
            [self RefreshView];
            [self updatenotificationbadge];
            [self langsetuptranslations];
            [helperClass removeWaitCursor];
            
            
             if ([appdelegate.PerformDeltaSync isEqualToString:@"NO"] && [appdelegate.DataDownloaded isEqualToString:@"NO"])
            {
                // [ToastView showToastInParentView:self.view withText:appdelegate.ProcessStatus withDuaration:2.0];
                appdelegate.ProcessALERT = [helperClass gettranslation:@"LBL_590"];
                appdelegate.ProcessBUTTON = [helperClass gettranslation:@"LBL_462"];
                
                [helperClass showalert:appdelegate.ProcessALERT  message:appdelegate.ProcessStatus action:appdelegate.ProcessBUTTON];
            }

            
        }
    }
    else if (i == 2)
    {
       // helperClass = [[HelperClass alloc] init];
        
        appdelegate.sem = dispatch_semaphore_create(0);
        [popoverController closePopover];
        
        while (dispatch_semaphore_wait( appdelegate.sem , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
        
        if(remoteHostStatus == NotReachable)
        {
            [ToastView showToastInParentView:self.view withText:[helperClass gettranslation:@"LBL_630"] withDuaration:2.0];
        }
        else
        {
            [helperClass showWaitCursor:[helperClass gettranslation:@"LBL_476"]];
            appdelegate.PerformDeltaSync = @"YES";
           
            [helperClass get_EMS_data];
            [self reloadActivityAndAttendeeCardData];
            
            [self RefreshView];
            
            
            [helperClass removeWaitCursor];
            
            if ([appdelegate.PerformDeltaSync isEqualToString:@"YES"] && [appdelegate.DataDownloaded isEqualToString:@"NO"])
            {
                //[ToastView showToastInParentView:self.view withText:appdelegate.ProcessStatus withDuaration:2.0];
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:appdelegate.ProcessStatus preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                           {
//                                               semp = @"No";
//                                               dispatch_semaphore_signal(sem_event);
//                                           }];
//                [alert addAction:okaction];
//                [self presentViewController:alert animated:NO completion:nil];
                
                [helperClass showalert:[helperClass gettranslation:@"LBL_590"] message:appdelegate.ProcessStatus action:[helperClass gettranslation:@"LBL_462"]];
            }
            appdelegate.PerformDeltaSync = @"NO";
        }
    }
    else if (i == 3)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self performSegueWithIdentifier:@"HometoSynclogsView" sender:self];
    }
    else if (i == 4)
    {

//        [helper stringtocurrency:@"197283654.67"];
//        NSLog(@"stringtocurrency: %@",[helper stringtocurrency:@"197283654.67"]);
//        NSLog(@"stringtocurrency: %@",[helper CurrencytoFloat:[helper stringtocurrency:@"197283654.67"]]);
        
        NSArray *txn_messages = [helperClass query_alldata:@"Transaction_Queue"];
        
        if (txn_messages.count >0)
        {
            [helperClass delete_alldata:@"Transaction_Queue"];
            [ToastView showToastInParentView:self.view withText:[NSString stringWithFormat:@"%lu %@",(unsigned long)txn_messages.count,[helperClass gettranslation:@"LBL_629"]] withDuaration:1.0];
        }
        else
        {
            [helperClass showalert:appdelegate.ProcessALERT  message:appdelegate.ProcessStatus action:appdelegate.ProcessBUTTON];
        }
        
    }
    if (i == 9)
    {
        NSString *emailTitle = [NSString stringWithFormat:@"IOEvents-%@-%@",[helperClass getUsername],[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
        NSString *emailBody = [NSString stringWithFormat:@"Affiliate:%@\nLanguage Code:%@",[helperClass query_data:@"" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"],[helperClass getlanguageCode]];
        NSString *Recipients = [helperClass query_data:@"" inputdata:@"SupportMail" entityname:@"S_Config_Table"];
        
        [self showEmail:appdelegate.logFilePath emailTitle:emailTitle messageBody:emailBody toRecipents:Recipients];
    }
    else if (i == 5)
    {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        sem_event = dispatch_semaphore_create(0);
      
        NSString *Message;
        NSArray *txn_messages = [[helperClass query_alldata:@"Transaction_Queue"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"transaction_status like 'Open'"]];
        if (txn_messages.count >0)
        {
            Message = [NSString stringWithFormat:@"%lu %@\n%@",(unsigned long)txn_messages.count,[helperClass gettranslation:@"LBL_776"],[helperClass gettranslation:@"LBL_597"]] ;
        }
        else
        {
            Message = [helperClass gettranslation:@"LBL_597"];
        }
       // filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"transaction_status like 'Open'"] ];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helperClass gettranslation:@"LBL_420"] message:Message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helperClass gettranslation:@"LBL_797"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            semp = @"Yes";
            dispatch_semaphore_signal(sem_event);

        }];
        [alert addAction:okaction];
      
        
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:[helperClass gettranslation:@"LBL_798"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                       {
                                           semp = @"No";
                                           dispatch_semaphore_signal(sem_event);
                                       }];
        
        [alert addAction:cancelaction];
        [self presentViewController:alert animated:NO completion:nil];
        
        
        while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
        
        if ([semp isEqualToString:@"Yes"])
        {
            [helperClass showWaitCursor:[helperClass gettranslation:@"LBL_736"]];
            [self LogoutButtonAction];
            [self dismissViewControllerAnimated:YES completion:nil];
            [helperClass delete_alldata:@"Transaction_Queue"];
            [helperClass delete_alldata:@"Push_Notification_Table"];
            [helperClass removeWaitCursor];
            [helperClass update_data:@"GoToDashboard" inputdata:@"NO" entityname:@"S_Config_Table"];
            [self performSegueWithIdentifier:@"UnwindToLoginScreen" sender:self];
        
            //QA Signal R Conn
//               SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://nnemsdev.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=r1RN4Uvgsc3IdVjaG8HUdQoyEJePFAEIZBzjK8qa8nE=" notificationHubPath:@"nnemsdev_app"];
            
            
            // UAT Signal R Conn
            // SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://ioeventnphub.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=X3HRmisgwhpMvF8ge71ZA8s5OuiPMOgXfpajbMoYn6s=" notificationHubPath:@"ioevent_app"];
            
            
            
            // PREPROD Signal R Conn
            SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://ioeventshubstaging.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=Jzrf7huIoi12u+C6geiF1eY6brMeLqLzbwwKwThz3oc=" notificationHubPath:@"ioevents_app"];
            
            
            
            
            //PROD Signal R Conn
//              SBNotificationHub* hub = [[SBNotificationHub alloc] initWithConnectionString:@"Endpoint=sb://ioeventshubprod.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=YFVptRJzbRKcmCG2t3zcdRgPZYJx2sQOdc6DCRdxrcM=" notificationHubPath:@"ioevents_app"];
            
            
            
            
            [hub unregisterAllWithDeviceToken:appdelegate.Devicetoken completion:^(NSError* error)
             {
                 if (error != nil)
                 {
                     NSString *errormsg = [NSString stringWithFormat:@"%@",error];
                 }
             }];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"N" forKey:@"Pushregister"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            appdelegate.lstpage=@"Y";
            appdelegate.logout=@"YES";
            
        }
        
    }
}

-(void)LogoutButtonAction
{
  
    NSDictionary *deRef_msgbody = [helperClass create_trans_msgbody:@"" txType:@"EMSLogOut" pageno:@"" pagecount:@"" lastpage:@""];
    
    [helperClass printloginconsole:@"6" logtext:[NSString stringWithFormat:@"Request sent to De Register for Push Notification ---- %@",deRef_msgbody]];
    
    
    NSMutableDictionary *TRANXAPI_response = [helperClass invokewebservice:deRef_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helperClass printloginconsole:@"4" logtext:[NSString stringWithFormat:@"%@%@",@"Error Received de-registering push notification: ",TRANXAPI_response]];
    }
    else
    {
        
        NSString *authresponse =  [helperClass decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        [helperClass printloginconsole:@"6" logtext:[NSString stringWithFormat:@"Successfully de-registered push notification: %@",authresponse]];
    }
    
    NSLog(@"User has signed out");
 
}

- (void)segmentclicked:(UISegmentedControl *)segment
{
    NSInteger i = [segment tag];
    if (i == 6)
    {
        if(segment.selectedSegmentIndex == 0)
        {
            [helperClass update_data:@"DefaultEventView" inputdata:@"ListView" entityname:@"S_Config_Table"];
        }
        else if(segment.selectedSegmentIndex == 1)
        {
            [helperClass update_data:@"DefaultEventView" inputdata:@"CalendarView" entityname:@"S_Config_Table"];
        }
        NSString *DefaultView = [helperClass query_data:@"name" inputdata:@"DefaultEventView" entityname:@"S_Config_Table"];
        NSLog(@"Default view is %@",DefaultView);
    }
    else if (i == 7)
    {
        if(segment.selectedSegmentIndex == 0)
        {
            [helperClass update_data:@"DefaultCalendarView" inputdata:@"DayPlannerView" entityname:@"S_Config_Table"];

        }
        else if(segment.selectedSegmentIndex == 1)
        {
            [helperClass update_data:@"DefaultCalendarView" inputdata:@"WeekPlannerView" entityname:@"S_Config_Table"];

        }
        else if(segment.selectedSegmentIndex == 2)
        {
            [helperClass update_data:@"DefaultCalendarView" inputdata:@"MonthPlannerView" entityname:@"S_Config_Table"];
        }
    }
}


- (void) switchIsChanged:(UISwitch *)paramSender
{
    
    if ([paramSender isOn])
    {
        [helperClass update_data:@"GalleryFDRdownload" inputdata:@"YES" entityname:@"S_Config_Table"];
    }
    else
    {
        [helperClass update_data:@"GalleryFDRdownload" inputdata:@"NO" entityname:@"S_Config_Table"];
    }
    
}
//--------------------------------------------


-(void)updatenotificationbadge
{
    
    NSArray *pushnotificationdatacountarray = [helperClass query_alldata:@"Push_Notification_Table"];
    int notifycount = 0;
    for (int i =0; i<[pushnotificationdatacountarray count]; i++)
    {
        if([[pushnotificationdatacountarray[i] valueForKey:@"meassage_read"] isEqualToString:@"N"])
        {
            notifycount++;
        }
    }
    appdelegate.badgevalue = notifycount;
    
    [notificationbadge setBadgeValue:appdelegate.badgevalue];
}

-(void)NotificationAction
{
    notificationview = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationView"];
    [notificationview.view setFrame:CGRectMake(1024, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [notificationbadge setBadgeValue:0];
    
    [self addChildViewController:notificationview];
    [self.view addSubview:notificationview.view];
    [notificationview didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [notificationview.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
 
}
//- (IBAction)notification_btn:(id)sender
//{
//    
//    
//    notificationview = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationView"];
//    [notificationview.view setFrame:CGRectMake(1024, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    
//    [notificationbadge setBadgeValue:0];
//    
//    [self addChildViewController:notificationview];
//    [self.view addSubview:notificationview.view];
//    [notificationview didMoveToParentViewController:self];
//    [UIView animateWithDuration:0.5 animations:^{
//        
//        [notificationview.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        
//    }];
//
//}

- (IBAction)events_segmentcontrol:(id)sender
{
  
   keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];// for preprod
 
    
    mypredicate = [NSPredicate predicateWithFormat:@"SELF.visibility like %@",@"MY"];
    teampredicate = [NSPredicate predicateWithFormat:@"SELF.visibility like %@",@"TEAM"];
    apprpredicate = [NSPredicate predicateWithFormat:@"NOT (SELF.approvalflag like %@)",@"N"];
    eventpredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[mypredicate, teampredicate]];
    
   // if([appdelegate.EventListViewType isEqualToString:@"GRID"])
    //{
        switch (self.events_segmentcontrol.selectedSegmentIndex)
        {
            case 0:
                NSLog(@"segment index is %ld",(long)self.events_segmentcontrol.selectedSegmentIndex);
        
                Eventlist = [helperClass query_alldata:@"Event"];
                Eventlist = [Eventlist filteredArrayUsingPredicate:mypredicate];
                [self reloadActivityAndAttendeeCardData];
                [EventCollectionView reloadData];
                [_EventList_TableView reloadData];
                EventSearchbar.text=@"";
                [EventSearchbar resignFirstResponder];
                ApprovalStr = @"Event";
                appdelegate.EventListFilter=@"MY";
                appdelegate.eventAdvanceSearch=nil;//reset the advnc search predicate once the tab are changed
                [_clearSearch setHidden:YES];
                
                break;
            case 1:
                NSLog(@"segment index is %ld",(long)self.events_segmentcontrol.selectedSegmentIndex);
                appdelegate.EventListFilter=@"TEAM";
                Eventlist = [helperClass query_alldata:@"Event"];
                Eventlist = [Eventlist filteredArrayUsingPredicate:eventpredicate];
        
                [self reloadActivityAndAttendeeCardData];
        
                [EventCollectionView reloadData];
                [_EventList_TableView reloadData];
                EventSearchbar.text=@"";
                [EventSearchbar resignFirstResponder];
                ApprovalStr = @"Event";
                appdelegate.eventAdvanceSearch=nil;
                 [_clearSearch setHidden:YES];
                
                break;
            case 2:
                NSLog(@"segment index is %ld",(long)self.events_segmentcontrol.selectedSegmentIndex);
                appdelegate.EventListFilter=@"APPROVAL";
                Eventlist = [helperClass query_alldata:@"Event"];
                Eventlist = [Eventlist filteredArrayUsingPredicate:apprpredicate];
                [self reloadActivityAndAttendeeCardData];
                [EventCollectionView reloadData];
                [_EventList_TableView reloadData];
                 EventSearchbar.text=@"";
                [EventSearchbar resignFirstResponder];
                ApprovalStr = @"Approvals";
                appdelegate.eventAdvanceSearch=nil;
                 [_clearSearch setHidden:YES];
                break;
                default:
            break;
        }    
}




- (IBAction)Activities_segmentcontrol:(id)sender
{
   
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];// for preprod
   // NSPredicate *activityPredicate,*mypredicate,*teampredicate;
   // NSPredicate *activityPredicate;
    mypredicate = [NSPredicate predicateWithFormat:@"SELF.visibility like %@",@"MY"];
    teampredicate = [NSPredicate predicateWithFormat:@"SELF.visibility like %@",@"ALL"];
    activityPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[mypredicate, teampredicate]];
    
    
    switch (self.Activities_segmentcontrol.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"segment index is %ld",(long)self.Activities_segmentcontrol.selectedSegmentIndex);
            
            AssignedActivitylist = [helperClass query_alldata:@"Activities"];
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
            [self setupaactivityShinobi];
            
            break;
            
        case 1:
            NSLog(@"segment index is %ld",(long)self.Activities_segmentcontrol.selectedSegmentIndex);
            
            AssignedActivitylist = [helperClass query_alldata:@"Activities"];
            AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
            [self setupaactivityShinobi];
            
            break;
            
        default:
            break;
    }
    
}


-(void)setupaactivityShinobi
{
    [shinobigrid_att removeFromSuperview];
    
    activities_lbl.text = [NSString stringWithFormat:@"%@ (%lu)",[helperClass gettranslation:@"LBL_913"],(unsigned long)AssignedActivitylist.count];
    self.Activitylistview.backgroundColor = [UIColor whiteColor];
    
   // [self setupTableViewData];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSMutableArray *SelectedArray = [[NSMutableArray alloc]init];
    
    for(int i=0;i<AssignedActivitylist.count;i++)
    {
        
      //  NSLog(@"days left before %@",AssignedActivitylist[i]);
        NSMutableDictionary *daysLeft=[[NSMutableDictionary alloc]init];
        daysLeft = AssignedActivitylist[i];
        [daysLeft setValue:[NSNumber numberWithInteger:[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[AssignedActivitylist[i] valueForKey:@"enddate"]]]
                    forKey:@"duration"];
        
        
      
        
        [SelectedArray addObject:daysLeft];
        
    }
//        for (NSDictionary *record in AssignedActivitylist)
//        {
//            NSNumber *number = [NSNumber numberWithInteger:[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[record valueForKey:@"enddate"]]];
//            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
//            [Dict addEntriesFromDictionary:record];
//            [Dict setValue:number forKey:@"duration"];
//            [SelectedArray addObject:Dict];
//            
//           // [AssignedActivitylist replaceObjectAtIndex:[NSIndexSet indexSetWithIndex:1] withObjects:[NSArray arrayWithObject:yourObject]]
//        }
    //}

    
    
    /*
     NSString *duration = [helper gettranslation:@"LBL_076"];
     [columndnames addObject:duration];
     
     NSString *type = [helper gettranslation:@"LBL_266"];
     [columndnames addObject:type];
     
     NSString *eventid = [helper gettranslation:@"LBL_131"];
     [columndnames addObject:eventid];
     
     NSString *ownedby = [helper gettranslation:@"LBL_187"];
     [columndnames addObject:ownedby];
     
     NSString *status = [helper gettranslation:@"LBL_242"];
     [columndnames addObject:status];
     
     NSString *priority =[helper gettranslation:@"LBL_203"];
     [columndnames addObject:priority];

     */
    
    NSArray *columnnames = [[NSMutableArray alloc] initWithObjects:@"duration",@"type",@"eventid",@"ownedby",@"status",@"priority",nil];
    NSArray *columndnames = [[NSMutableArray alloc] initWithObjects:[helperClass gettranslation:@"LBL_076"], [helperClass gettranslation:@"LBL_266"],[helperClass gettranslation:@"LBL_131"],[helperClass gettranslation:@"LBL_187"],[helperClass gettranslation:@"LBL_242"],[helperClass gettranslation:@"LBL_203"],nil];
   
//    NSArray *columndnames = [[NSMutableArray alloc] initWithObjects:@"Duration",@"Type",@"Eventid",@"Ownedby",@"Status",@"Priority" ,nil];
    NSArray *columnlength = [[NSMutableArray alloc] initWithObjects:@"130",@"160",@"230",@"115",@"120",@"100",nil];
    
    
    
//    NSArray *columnnames = [[NSMutableArray alloc] initWithObjects:@"duration",@"eventid",@"type",@"ownedby",@"status",@"startdate",@"enddate",nil];
//    NSArray *columndnames = [[NSMutableArray alloc] initWithObjects:@"Days left",@"Event Name",@"Type",@"Owner",@"Status",@"Start Date",@"End Date",nil];
//    NSArray *columnlength = [[NSMutableArray alloc] initWithObjects:@"150",@"250",@"130",@"110",@"110",@"110",@"210",nil];
    
    
    shinobigrid_att = [[ShinobiDataGrid alloc] initWithFrame:CGRectInset(Activitylistview.bounds, 0, 0)];;
    
    shinobigrid_att.defaultCellStyleForHeaderRow.backgroundColor = [UIColor whiteColor];
    shinobigrid_att.defaultCellStyleForHeaderRow.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.f];
   // shinobigrid_att.defaultCellStyleForHeaderRow.textColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    
    shinobigrid_att.defaultCellStyleForHeaderRow.textColor = [helperClass DarkBlueColour];
   
    shinobigrid_att.defaultRowHeight = @33;
    shinobigrid_att.selectionMode = SDataGridSelectionModeRowSingle;
    shinobigrid_att.defaultGridLineStyle.width = 0;
    
    
    SDataGridCellStyle* boldStyle = [SDataGridCellStyle new];
    boldStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    boldStyle.textAlignment = NSTextAlignmentLeft;
    boldStyle.textColor = [helperClass DarkBlueColour];
   // boldStyle.textColor = [UIColor redColor];
    
    shinobigrid_att.defaultCellStyleForSelectedRows = boldStyle;
    SDataGridCellStyle *RowStyle = [SDataGridCellStyle new];
    RowStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    RowStyle.textAlignment = NSTextAlignmentLeft;
   // RowStyle.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    RowStyle.textColor = [helperClass DarkBlueColour];
    RowStyle.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    
    
    SDataGridCellStyle *altRowStyle = [SDataGridCellStyle new];
    altRowStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    altRowStyle.textAlignment = NSTextAlignmentLeft;
   // altRowStyle.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    altRowStyle.backgroundColor = [UIColor clearColor];
    altRowStyle.textColor = [helperClass DarkBlueColour];
    
    
    SDataGridColumn *tickColumn = [[SDataGridColumn alloc] initWithTitle:@"" forProperty:@"startdate" cellType:[ShinobiCustomCell class] headerCellType:nil];
    tickColumn.width = @50;
    tickColumn.minimumWidth = @50;
    [shinobigrid_att addColumn:tickColumn];
    
    
    for (int i=0; i<columnnames.count; i++)
        
    {
        NSString *name = [columnnames objectAtIndex:i];
        NSString *dname = [columndnames objectAtIndex:i];
        
        SDataGridColumn *newcolumn = [[SDataGridColumn alloc] initWithTitle:dname forProperty:name];
        newcolumn.width = [columnlength objectAtIndex:i];
        newcolumn.cellStyle = boldStyle;
        newcolumn.sortMode = SDataGridColumnSortModeBiState;
        newcolumn.headerCellType = [SIGridHeaderCell class];
        newcolumn.sortMode = SDataGridColumnSortModeBiState;
        newcolumn.headerCellType = [SIGridHeaderCell class];
//        if(i % 2 == 0)
//        {
//            newcolumn.cellStyle = RowStyle;
//        }
//        else
//        {
//            newcolumn.cellStyle = altRowStyle;
//        }
        

        [shinobigrid_att addColumn:newcolumn];
        newcolumn.canReorderViaLongPress = YES;
        newcolumn.canResizeViaPinch = YES;
    }
    
    
    // Create the helper
    self.datasourceHelper_att = [[SDataGridDataSourceHelper alloc] initWithDataGrid:shinobigrid_att];
    self.datasourceHelper_att.delegate = self;
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"duration" ascending:NO];
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray = [SelectedArray sortedArrayUsingDescriptors:descriptors];
    
    self.datasourceHelper_att.data = sortedArray;
    
    
    
    [self.Activitylistview addSubview:shinobigrid_att];
    
}

-(void)shinobiDataGrid:(ShinobiDataGrid *)grid alterStyle:(SDataGridCellStyle *)styleToApply beforeApplyingToCellAtCoordinate:(SDataGridCoord *)coordinate
{
    
    if(coordinate.row.rowIndex % 2 == 0)
    {
        styleToApply.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
        //styleToApply.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
        
    }
    else
    {
        styleToApply.backgroundColor = [UIColor clearColor];
        //styleToApply.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    }

}
- (void)shinobiDataGrid:(ShinobiDataGrid *)grid didChangeSortOrderForColumn:(SDataGridColumn *)column
                   from:(SDataGridColumnSortOrder)oldSortOrder
{
    
    
    for (SDataGridColumn* col in  shinobigrid_att.columns) {
        if([col.headerCell class]==[SIGridHeaderCell class])
        {
            ((SIGridHeaderCell*)col.headerCell).sortOrder = SDataGridColumnSortOrderNone;
        }
    }
    
    if([column.headerCell class]==[SIGridHeaderCell class])
        ((SIGridHeaderCell*)column.headerCell).sortOrder = column.sortOrder;
}
-(BOOL)dataGridDataSourceHelper:(SDataGridDataSourceHelper *)helper populateCell:(SDataGridCell *)cell withValue:(id)value forProperty:(NSString *)propertyKey sourceObject:(id)object
{
    if( [propertyKey isEqualToString:@"startdate"])
    {
        ShinobiCustomCell *checkCell = (ShinobiCustomCell *)cell;
        checkCell.myCheckCellDelegate = self;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        
        NSDate *sDate = [dateFormatter dateFromString:[object valueForKey:@"startdate"]];
        NSDate *eDate = [dateFormatter dateFromString:[object valueForKey:@"enddate"]];
        
        if ([sDate timeIntervalSinceNow] > 0.0)
        {
            //  Start date yet yo come
            checkCell.checked = 1;
        }
        else if ([eDate timeIntervalSinceNow] > 0.0)
        {
            //Start date arrived but not ended yet\checkCell.checked = 0;
            checkCell.checked = 2;
            
        }
        else
        {
            // Activity has passed the End Date
            
            if([[object valueForKey:@"statuslic"]isEqualToString:@"Completed"])
            {
                checkCell.checked = 3;
            }
            else
            {
                checkCell.checked = 0;
            }
            
        }
        
        return YES;
        
        
    }
    
    return NO;
}

-(id)dataGridDataSourceHelper:(SDataGridDataSourceHelper *)helper displayValueForProperty:(NSString *)propertyKey withSourceObject:(id)object
{
    
    if ([propertyKey isEqualToString:@"eventid"])
    {
        NSString *eventid = [object valueForKey:@"eventid"];
        NSString *eventname;
        context = [appdelegate managedObjectContext];
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        [request setEntity:entitydesc];
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",eventid];
        [request setPredicate:predicate];
        
        NSError *error;
        NSArray *matchingData = [context executeFetchRequest:request error:&error];
        
        if (matchingData.count <=0)
        {
            eventname = @"";
        }
        else
        {
            eventname = [[matchingData valueForKey:@"name"] componentsJoinedByString:@""];
        }
        
        return eventname;
    }
/*    else if ([propertyKey isEqualToString:@"startdate"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        NSDate *date = [dateFormatter dateFromString:[object valueForKey:@"startdate"]];
        [dateFormatter setDateFormat:@"dd MMM yy"];
        
        return [dateFormatter stringFromDate:date];

    }
    else if ([propertyKey isEqualToString:@"enddate"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        NSDate *date = [dateFormatter dateFromString:[object valueForKey:@"enddate"]];
        [dateFormatter setDateFormat:@"dd MMM yy"];
        
        return [dateFormatter stringFromDate:date];
        
    }
*/
    else if ([propertyKey isEqualToString:@"duration"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        
        NSDate *sDate = [dateFormatter dateFromString:[object valueForKey:@"startdate"]];
        NSDate *eDate = [dateFormatter dateFromString:[object valueForKey:@"enddate"]];
        
        
        if (sDate && eDate)
        {
            if ([sDate timeIntervalSinceNow] > 0.0)
            {
                /* NSInteger numberofdays = [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[object valueForKey:@"startdate"]];
                 if (numberofdays == 0)
                 {
                 return  @"Unscheduled";
                 }
                 else*/
                
                return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[object valueForKey:@"startdate"]],[helperClass gettranslation:@"LBL_789"]];
            
                // return [NSString stringWithFormat:@"%ld Days to go",(long)numberofdays];
            
                //  Start date yet yo come
            }
            else if ([eDate timeIntervalSinceNow] > 0.0)
            {
                /* NSInteger numberofdays = [self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[object valueForKey:@"enddate"]];
                 if (numberofdays == 0)
                 {
                    return  @"Unscheduled";
                 }
                 else*/
                // return [NSString stringWithFormat:@"%ld Days remaining",(long)numberofdays];
                return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[dateFormatter stringFromDate:[NSDate date]] and:[object valueForKey:@"enddate"]],[helperClass gettranslation:@"LBL_791"]];
            
                //Start date arrived but not ended yet
            
            }
            else
            {
                // Activity has passed the End Date
                /* NSInteger numberofdays = [self numberOfDaysBetween:[object valueForKey:@"enddate"] and:  [dateFormatter stringFromDate:[NSDate date]]];
                 if (numberofdays == 0)
                 {
                    return  @"Unscheduled";
                 }
                 else*/
                // return [NSString stringWithFormat:@"%ld Days Overdue",(long)numberofdays];
                
                if([[object valueForKey:@"statuslic"] isEqualToString:@"Completed"])
                {
                    return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[object valueForKey:@"enddate"] and:  [dateFormatter stringFromDate:[NSDate date]]],[helperClass gettranslation:@"LBL_809"]];
                }
                else
                {
                    return [NSString stringWithFormat:@"%ld %@",(long)[self numberOfDaysBetween:[object valueForKey:@"enddate"] and:  [dateFormatter stringFromDate:[NSDate date]]],[helperClass gettranslation:@"LBL_790"]];
                }
            }
        }
        else
             return  @"Unscheduled";
            
    }
    return nil;
}


- (NSInteger)numberOfDaysBetween:(NSString *)sDate and:(NSString *)eDate
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *startDate = [f dateFromString:sDate];
    NSDate *endDate = [f dateFromString:eDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
//    NSString *returndays = [NSString stringWithFormat:@"%ld",[components day]];
//    NSLog(@"returndays: %@",returndays);

    return [components day];
    
}
#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == ActivitySearchBar)
    {
        [helperClass MoveViewUp:YES Height:350 ViewToBeMoved:self.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == ActivitySearchBar)
    {
        
        [helperClass MoveViewUp:NO Height:350 ViewToBeMoved:self.view];
    }
    [self setupaactivityShinobi];
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == EventSearchbar)
    {
        if(eventcount > searchText.length)
        {
            if(appdelegate.eventAdvanceSearch)
            {
                    Eventlist = [[helperClass query_alldata:@"Event"] filteredArrayUsingPredicate:appdelegate.eventAdvanceSearch];
            }
            else
            {
                    Eventlist = [helperClass query_alldata:@"Event"];
            }

            if(self.events_segmentcontrol.selectedSegmentIndex == 0)
            {
               Eventlist = [Eventlist filteredArrayUsingPredicate:mypredicate];
            }
            else if (self.events_segmentcontrol.selectedSegmentIndex == 1)
            {
                Eventlist = [Eventlist filteredArrayUsingPredicate:eventpredicate];
            }
            else if (self.events_segmentcontrol.selectedSegmentIndex == 2)
            {
                Eventlist = [Eventlist filteredArrayUsingPredicate:apprpredicate];
                
            }
        }
        if(searchText.length == 0)
        {
            if(appdelegate.eventAdvanceSearch)
            {
                Eventlist = [[helperClass query_alldata:@"Event"] filteredArrayUsingPredicate:appdelegate.eventAdvanceSearch];
            }
            else
            {
                Eventlist = [helperClass query_alldata:@"Event"];
            }

            if(self.events_segmentcontrol.selectedSegmentIndex == 0)
            {
               Eventlist = [Eventlist filteredArrayUsingPredicate:mypredicate];
            }
            else if (self.events_segmentcontrol.selectedSegmentIndex == 1)
            {
                Eventlist = [Eventlist filteredArrayUsingPredicate:eventpredicate];
            }
            else if (self.events_segmentcontrol.selectedSegmentIndex == 2)
            {
                Eventlist = [Eventlist filteredArrayUsingPredicate:apprpredicate];
            }
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];

        }
        else
        {
            if(appdelegate.eventAdvanceSearch)
            {
                Eventlist = [[helperClass query_alldata:@"Event"] filteredArrayUsingPredicate:appdelegate.eventAdvanceSearch];
            }
            else
            {
                Eventlist = [helperClass query_alldata:@"Event"];
            }

            eventcount = searchText.length;
            if(self.events_segmentcontrol.selectedSegmentIndex == 0)
            {
               Eventlist = [Eventlist filteredArrayUsingPredicate:mypredicate];
               
            }
            else if (self.events_segmentcontrol.selectedSegmentIndex == 1)
            {
                Eventlist = [Eventlist filteredArrayUsingPredicate:eventpredicate];
            }
            else if (self.events_segmentcontrol.selectedSegmentIndex == 2)
            {
                Eventlist = [Eventlist filteredArrayUsingPredicate:apprpredicate];
            }
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.venue contains[c] %@) OR (SELF.desc contains[c] %@) OR (SELF.ownedby contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[Eventlist filteredArrayUsingPredicate:predicate]];
            Eventlist = [filtereventarr copy];
            
            
        }
        [self reloadActivityAndAttendeeCardData];
        [EventCollectionView reloadData];
        [_EventList_TableView reloadData];
    }
    else if (searchBar == ActivitySearchBar)
    {
        if(activitycount > searchText.length)
        {
            AssignedActivitylist = [helperClass query_alldata:@"Activities"];
            if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
            {
                AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
               
            }
            else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
            {
                AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
            }
        }
        if(searchText.length == 0)
        {
             AssignedActivitylist = [helperClass query_alldata:@"Activities"];
            if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
            {
                AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
            }
            else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
            {
                AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
            }
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            AssignedActivitylist = [helperClass query_alldata:@"Activities"];
            if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
            {
               AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
            }
            else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
            {
                AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
            }
            
            
            
            NSMutableArray *EventNamearr = [[NSMutableArray alloc]init];
            
            //getting event name for the event id
            for (int i =0 ;i<[AssignedActivitylist count];i++)
            {
                NSString *eventid = [AssignedActivitylist[i] valueForKey:@"eventid"];
                NSString *eventname = [helperClass returneventname:eventid];
                
                NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                
                [newDict setObject:eventname forKey:@"eventname"];
                [newDict setObject:[AssignedActivitylist[i] valueForKey:@"eventid"] forKey:@"eventid"];
                [newDict setObject:[AssignedActivitylist[i] valueForKey:@"type"] forKey:@"type"];
                [newDict setObject:[AssignedActivitylist[i] valueForKey:@"ownedby"] forKey:@"ownedby"];
                [newDict setObject:[AssignedActivitylist[i] valueForKey:@"status"] forKey:@"status"];
                [newDict setObject:[AssignedActivitylist[i] valueForKey:@"priority"] forKey:@"priority"];
              
                [newDict setObject:[AssignedActivitylist[i] valueForKey:@"startdate"] forKey:@"startdate"];
                 [newDict setObject:[AssignedActivitylist[i] valueForKey:@"enddate"] forKey:@"enddate"];
                [newDict setObject:@"" forKey:@"duration"];
                [EventNamearr addObject:newDict];
                
            }
            
            
            activitycount = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.type contains[c] %@) OR (SELF.eventname contains[c] %@) OR (SELF.ownedby contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.priority contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[EventNamearr filteredArrayUsingPredicate:predicate]];
            AssignedActivitylist = [filtereventarr copy];
            
        }
        [self setupaactivityShinobi];
        
    }
    
    
}
#pragma mark unwind method
-(IBAction)unwindToHomeScreen:(UIStoryboardSegue *)segue
{
    
    if([appdelegate.EventListFilter isEqualToString:@"EventAdvanceSearch"])
    {
        Eventlist=[helperClass query_alldata:@"Event"];
        if(appdelegate.eventAdvanceSearch)
        {
            Eventlist=[Eventlist filteredArrayUsingPredicate:appdelegate.eventAdvanceSearch];
            
            [_clearSearch setHidden:NO];
        }
       
        [EventCollectionView reloadData];
        [self setupaactivityShinobi];
    }
    else
    {
        [self RefreshView];
    }
        [self updatenotificationbadge];
         [self reloadActivityAndAttendeeCardData];
        [_EventList_TableView reloadData];
    
   
}

#pragma mark <ARSPopoverDelegate>

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view {
    // delegate for you to use.
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    // delegate for you to use.
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    // delegate for you to use.
    return YES;
}
-(void) RefreshView
{
    
    Eventlist = [helperClass query_alldata:@"Event"];
    
    if(self.events_segmentcontrol.selectedSegmentIndex == 0)
    {
        Eventlist = [Eventlist filteredArrayUsingPredicate:mypredicate];
    }
    else if (self.events_segmentcontrol.selectedSegmentIndex == 1)
    {
        Eventlist = [Eventlist filteredArrayUsingPredicate:eventpredicate];
    }
    else if (self.events_segmentcontrol.selectedSegmentIndex == 2)
    {
        Eventlist = [Eventlist filteredArrayUsingPredicate:apprpredicate];
    }
    
     [self reloadActivityAndAttendeeCardData];
    
    [EventCollectionView reloadData];
    
    
    
    AssignedActivitylist = [helperClass query_alldata:@"Activities"];
    if(self.Activities_segmentcontrol.selectedSegmentIndex == 0)
    {
        AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:mypredicate];
        
    }
    else if (self.Activities_segmentcontrol.selectedSegmentIndex == 1)
    {
        AssignedActivitylist = [AssignedActivitylist filteredArrayUsingPredicate:activityPredicate];
    }
    
    [self setupaactivityShinobi];
    
    // _Username_lbl.text = [helper getUsername];
    if ([[helperClass getUsername] length]>0)
    {
         TitleBarView.UsernameLbl.text =[NSString stringWithFormat:@"%@,%@",[helperClass gettranslation:@"LBL_585"],[helperClass getUsername]];
    }
    else
    {
        TitleBarView.UsernameLbl.text = @" ";
    }
   
    
   
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"HometoAnalyticsView"])
    {
        AnalyticsViewController *controller = segue.destinationViewController;
        controller.event_id = selectedeventid;
        // NSLog(@"cancel segue value %@",controller.cancelSegue);
    }
    else if ([[segue identifier] isEqualToString:@"Addnewevent"])
    {
        UpsertEventVC *controller = segue.destinationViewController;
        controller.senderview = @"Addevent";
        controller.cancelsegue = @"newcancel_btn";
    }
    else if ([[segue identifier] isEqualToString:@"HometoDetailsView"])
    {
        EventDetails_ViewController *controller = segue.destinationViewController;
        controller.ApprovalStr = ApprovalStr;
    }
}


- (void)activateDeletionMode:(UILongPressGestureRecognizer *)gesture
{
    
  //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                  // ^{
   // sem_event = dispatch_semaphore_create(0);
    
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        // update cell to display delete functionality
        NSIndexPath *indexPath = [EventCollectionView indexPathForItemAtPoint:[gesture locationInView:EventCollectionView]];
        
       // NSLog(@"Index Path: %ld",(long)indexPath.row);
        selindex = indexPath;
        
        UICollectionViewCell* cell = [EventCollectionView cellForItemAtIndexPath:indexPath];
        
       // NSLog(@"indexpath row is %ld",indexPath.row);
        NSString *candelete = [Eventlist[indexPath.row] valueForKey:@"canDelete"];
        if ([candelete isEqualToString:@"Y"])
        {
            UIView *contentView = cell.contentView;
            UIButton *delete_btn = (UIButton *) [contentView viewWithTag:26];
            UILabel *status_lv = (UILabel *) [contentView viewWithTag:8];
            
            
           // NSString *status = status_lv.text;
           // NSString *status = [Eventlist[indexPath.row] valueForKey:@"statuslic"];
          //  NSLog(@"Status lic is %@",status);
            //if ([status isEqualToString:@"Planned"])
            //{
               // delete_btn.hidden = NO;
                [self startjingle:indexPath];
                
                selectedeventid = [Eventlist[indexPath.row] valueForKey:@"eventid"];
                calidentify = [Eventlist[indexPath.row] valueForKey:@"calevent_refid"];
                
                
            
           // dispatch_async(dispatch_get_main_queue(),
                     //      ^{
//                               sem_event = dispatch_semaphore_create(0);
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helperClass gettranslation:@"LBL_584"] message:[helperClass gettranslation:@"LBL_569"] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:[helperClass gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      semp = @"No";
                                      delete_btn.hidden = YES;
                                      [cell.layer removeAllAnimations];
//                                      dispatch_semaphore_signal(sem_event);
                                  }]];
                [alert addAction:[UIAlertAction actionWithTitle:[helperClass gettranslation:@"LBL_644"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      semp = @"Yes";
                                      [self DeleteEvent];
                                    
                                  }]];
                
                
                [self presentViewController:alert animated:true completion:nil];
            
            
                          
                
//                while (dispatch_semaphore_wait(sem_event , DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];}
                               // });
            
            
//            if ([semp isEqualToString:@"Yes"])
//                {
//                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",selectedeventid];
//
//                    [helperClass showWaitCursor:[helperClass gettranslation:@"LBL_364"]];
//
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
//                    ^{
//                        @autoreleasepool {
//                        [helperClass serverdelete:@"EVENT" entityid:selectedeventid];
//
//
//
//                        [helperClass deletecal_event:calidentify];
//                        [helperClass delete_predicate:@"Event" predicate:predicate];
//                        [helperClass deletewitheventid:@"Activities" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Act_dynamicdetails" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Analytics" value:selectedeventid];
//
//                        [helperClass deletewitheventid:@"Event_Attachments" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Attendee" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Comm" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Gallery" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Mail" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Email_Attachment" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Speaker" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Team" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Venue" value:selectedeventid];
//                        [helperClass deletewitheventid:@"Event_Approvals" value:selectedeventid];
//                        }
//                        dispatch_async(dispatch_get_main_queue(),
//                        ^{
//
//
//                            [helperClass removeWaitCursor];
//                            [self RefreshView];
//
//                        });
//                    });
//                   //
//
//
//                }
//                else
//                {
//
//                }
       // }

        }
        else
        {
            [ToastView showToastInParentView:self.view withText:[helperClass gettranslation:@"LBL_623"] withDuaration:1.5];
        }

        
    }                    //    });
}

-(void)DeleteEvent{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",selectedeventid];
    
    [helperClass showWaitCursor:[helperClass gettranslation:@"LBL_364"]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       @autoreleasepool {
                           [helperClass serverdelete:@"EVENT" entityid:selectedeventid];
                           
                           
                           
                           [helperClass deletecal_event:calidentify];
                           [helperClass delete_predicate:@"Event" predicate:predicate];
                           [helperClass deletewitheventid:@"Activities" value:selectedeventid];
                           [helperClass deletewitheventid:@"Act_dynamicdetails" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Analytics" value:selectedeventid];
                           
                           [helperClass deletewitheventid:@"Event_Attachments" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Attendee" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Comm" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Gallery" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Mail" value:selectedeventid];
                           [helperClass deletewitheventid:@"Email_Attachment" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Speaker" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Team" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Venue" value:selectedeventid];
                           [helperClass deletewitheventid:@"Event_Approvals" value:selectedeventid];
                       }
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          
                                          
                                          [helperClass removeWaitCursor];
                                          [self RefreshView];
                                          
                                      });
                   });
}


-(void) startjingle:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell = [EventCollectionView cellForItemAtIndexPath:indexPath];
    
    CAKeyframeAnimation *position = [CAKeyframeAnimation animation];
    position.keyPath = @"position";
    position.values = @[
                        [NSValue valueWithCGPoint:CGPointZero],
                        [NSValue valueWithCGPoint:CGPointMake(-1, 0)],
                        [NSValue valueWithCGPoint:CGPointMake(1, 0)],
                        [NSValue valueWithCGPoint:CGPointMake(-1, 1)],
                        [NSValue valueWithCGPoint:CGPointMake(1, -1)],
                        [NSValue valueWithCGPoint:CGPointZero]
                        ];
    position.timingFunctions = @[
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]
                                 ];
    position.additive = YES;
    
    CAKeyframeAnimation *rotation = [CAKeyframeAnimation animation];
    rotation.keyPath = @"transform.rotation";
    rotation.values = @[
                        @0,
                        @0.03,
                        @0,
                        [NSNumber numberWithFloat:-0.02]
                        ];
    rotation.timingFunctions = @[
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]
                                 ];
    
    CAAnimationGroup *group = [[CAAnimationGroup alloc] init];
    group.animations = @[position, rotation];
    group.duration = 0.4;
    group.repeatCount = HUGE_VALF;
    
    
    
    [cell.layer addAnimation:group forKey:@"wiggle"];
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-(void) relogintosystem
{
    NSString *username = [keychain stringForKey:@"username"];
    NSString *password = [keychain stringForKey:@"password"];
    NSString *pushtag = [keychain stringForKey:@"app_uniquetag"];
    NSString *appID = [helperClass query_data:@"name" inputdata:@"applicationid" entityname:@"S_Config_Table"];
    NSString *custID = [helperClass query_data:@"name" inputdata:@"customerid" entityname:@"S_Config_Table"];
    
    
    NSString *payload = [helperClass create_auth_payload:password pushtag:pushtag];
    NSDictionary *msgbody = [helperClass create_auth_msgbody:payload username:username custID:custID appID:appID];
    NSMutableDictionary *AUTHWEBAPI_response = [helperClass invokewebservice:msgbody invokeAPI:@"SL_Auth" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helperClass removeWaitCursor];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helperClass gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helperClass gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helperClass decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
        keychain[@"SLToken"] = jsonData[@"SLToken"];
        keychain[@"LoginID"] = jsonData[@"LoginID"];
        keychain[@"username"] = username;
        keychain[@"password"] = password;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        keychain[@"SyncDate"] = [dateFormatter stringFromDate:[NSDate date]];
        
        [helperClass update_data:@"EMSUserRoleActions" inputdata:jsonData[@"EMSUserRoleActions"] entityname:@"S_Config_Table"];
        
        [helperClass update_data:@"StorageContainer" inputdata:jsonData[@"StorageContainer"] entityname:@"S_Config_Table"];
        [helperClass update_data:@"StorageAccount" inputdata:jsonData[@"StorageAccount"] entityname:@"S_Config_Table"];
        [helperClass update_data:@"StorageKey" inputdata:jsonData[@"StorageKey"] entityname:@"S_Config_Table"];
        [helperClass update_data:@"StorageURI" inputdata:jsonData[@"StorageURI"] entityname:@"S_Config_Table"];
        
        [helperClass update_data:@"EMSAffiliateID" inputdata:jsonData[@"EMSAffiliateID"] entityname:@"S_Config_Table"];
        [helperClass update_data:@"TimeZoneLIC" inputdata:jsonData[@"TimeZoneLIC"] entityname:@"S_Config_Table"];
        
    }
}

- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        TitleBarView.networkLbl.text = [helperClass gettranslation:@"LBL_635"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:231.0/255.0f green:76.0/255.0f blue:60.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"offline.png"];
    }
    else
    {
        TitleBarView.networkLbl.text = [helperClass gettranslation:@"LBL_634"];
        TitleBarView.networkLbl.textColor = [UIColor colorWithRed:39.0/255.0f green:174.0/255.0f blue:96.0/255.0f alpha:1.0f];
        TitleBarView.networkImg.image = [UIImage imageNamed:@"online.png"];
    }
}
-(IBAction)HelpButtonAction:(id)sender
{
    HelpView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpView"];
    [HelpView.view setFrame:CGRectMake(1240, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
   
    
    [self addChildViewController:HelpView];
    [self.view addSubview:HelpView.view];
    [HelpView didMoveToParentViewController:self];
    [UIView animateWithDuration:0.5 animations:^{
        
        [HelpView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }];
}

- (IBAction)Event_AdvanceSearch:(id)sender {
    [EventSearchbar resignFirstResponder];
    [self performSegueWithIdentifier:@"showAdvanceSearch" sender:self];
}
- (IBAction)status_sortBtn:(id)sender {
    EventStatus=!EventStatus;
    
    if(EventStatus)
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"status" type:@"abc" Ascending:NO];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_status_sortIcon setImage:[UIImage imageNamed:@"descending.png"]];

        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"status" type:@"abc" Ascending:YES];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_status_sortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    
    
}

- (IBAction)endDate_sortBtn:(id)sender {
    
    EventEndDate=!EventEndDate;
    
    if(EventEndDate)
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"enddate" type:@"abc" Ascending:NO];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"enddate" type:@"abc" Ascending:YES];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
}

- (IBAction)startDate_sortBtn:(id)sender {
    
    EventStartDate=!EventStartDate;
    
    if(EventStartDate)
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"startdate" type:@"abc" Ascending:NO];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
  
    }
    else
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"startdate" type:@"abc" Ascending:YES];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
}

- (IBAction)venue_sortBtn:(id)sender {
    EventVenue=!EventVenue;
    
    if(EventVenue)
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"venue" type:@"abc" Ascending:NO];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"venue" type:@"abc" Ascending:YES];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}

- (IBAction)owner_sortBtn:(id)sender {
    EventOwner=!EventOwner;
    
    if(EventOwner)
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"ownedby" type:@"abc" Ascending:NO];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"ownedby" type:@"abc" Ascending:YES];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_owner_sortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
        
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];

    }
}

- (IBAction)eventName_sortBtn:(id)sender {
    
    EventName=!EventName;
    
    if(EventName)
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"name" type:@"abc" Ascending:NO];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        
        
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];

    }
    else
    {
        Eventlist=[helperClass sortData:Eventlist colname:@"name" type:@"abc" Ascending:YES];
        [self.EventList_TableView reloadData];
        [self.EventCollectionView reloadData];
        [_eventName_sortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
        
        [_owner_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_endDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_status_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_startDate_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_venue_sortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
   
    }
    
    
    
}

//-(IBAction)displayHomeMenu:(id)sender
//{
//    popoverController = [ARSPopover new];
//    popoverController.sourceView = self.TitleBarView.HomeMenuButton;
//
//    popoverController.sourceRect = CGRectMake(CGRectGetMidX(self.TitleBarView.HomeMenuButton.bounds), CGRectGetMaxY(self.TitleBarView.HomeMenuButton.bounds), 0, 0);
//    popoverController.contentSize = CGSizeMake(242, 127);
//    popoverController.arrowDirection = UIPopoverArrowDirectionUp;
//   // popoverController.backgroundColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
//    // popoverController.backgroundColor = [helper LightBlueColour];
//    
//    [self presentViewController:popoverController animated:YES completion:^{
//        [popoverController insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight)
//         {
//             
//             [self.homeMenu.HelpBtn setTitle:[helperClass gettranslation:@"LBL_758"] forState:UIControlStateNormal];
//             
//             [self.homeMenu.SettingsBtn setTitle:[helperClass gettranslation:@"LBL_232"] forState:UIControlStateNormal];
//             
//             
//             [self.homeMenu.HelpBtn addTarget:self action:@selector(HelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//             
//             [self.Setting_btn addTarget:self action:@selector(Setting_btn:) forControlEvents:UIControlEventTouchUpInside];
//             
//             
//             
//            
//         }];
//    }];
//    
//}




- (IBAction)clearSearch:(id)sender
{
    [_clearSearch setHidden:YES];
    appdelegate.eventAdvanceSearch=nil;
    appdelegate.EventListFilter=@"";
    switch (self.events_segmentcontrol.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"segment index is %ld",(long)self.events_segmentcontrol.selectedSegmentIndex);
            
            Eventlist = [helperClass query_alldata:@"Event"];
            Eventlist = [Eventlist filteredArrayUsingPredicate:mypredicate];
            [self reloadActivityAndAttendeeCardData];
            [EventCollectionView reloadData];
            [_EventList_TableView reloadData];
            ApprovalStr = @"Event";
            appdelegate.EventListFilter=@"MY";
            
            break;
        case 1:
            NSLog(@"segment index is %ld",(long)self.events_segmentcontrol.selectedSegmentIndex);
            appdelegate.EventListFilter=@"TEAM";
            Eventlist = [helperClass query_alldata:@"Event"];
            Eventlist = [Eventlist filteredArrayUsingPredicate:eventpredicate];
             [self reloadActivityAndAttendeeCardData];
            [EventCollectionView reloadData];
            [_EventList_TableView reloadData];
            
            ApprovalStr = @"Event";
            
            break;
        case 2:
            NSLog(@"segment index is %ld",(long)self.events_segmentcontrol.selectedSegmentIndex);
            appdelegate.EventListFilter=@"APPROVAL";
            Eventlist = [helperClass query_alldata:@"Event"];
            Eventlist = [Eventlist filteredArrayUsingPredicate:apprpredicate];
             [self reloadActivityAndAttendeeCardData];
            [EventCollectionView reloadData];
            [_EventList_TableView reloadData];
            ApprovalStr = @"Approvals";
            break;
        default:
            break;
    }

}
- (void)showEmail:(NSString*)file emailTitle:(NSString *)emailTitle messageBody:(NSString *)messageBody toRecipents:(NSString *)toRecipents  {
    
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:@[toRecipents]];

    
    // Determine the file name and extension
    NSArray *filepart = [file componentsSeparatedByString:@"."];
    NSString *filename = @"IO-Events";
    NSString *extension = [filepart objectAtIndex:1];
 
    NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:file];
    
    // Determine the MIME type
    NSString *mimeType;
    if ([extension isEqualToString:@"jpg"]) {
        mimeType = @"image/jpeg";
    } else if ([extension isEqualToString:@"png"]) {
        mimeType = @"image/png";
    } else if ([extension isEqualToString:@"doc"]) {
        mimeType = @"application/msword";
    } else if ([extension isEqualToString:@"ppt"]) {
        mimeType = @"application/vnd.ms-powerpoint";
    } else if ([extension isEqualToString:@"html"]) {
        mimeType = @"text/html";
    } else if ([extension isEqualToString:@"pdf"]) {
        mimeType = @"application/pdf";
    }
    else if ([extension isEqualToString:@"log"]){
        mimeType = @"text/log";
    }
    
    if(filename.length>0 && mimeType.length>0 && fileData!=NULL){
        [mc addAttachmentData:fileData mimeType:mimeType fileName:filename];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self presentViewController:mc animated:YES completion:NULL];
    }
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }

    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
