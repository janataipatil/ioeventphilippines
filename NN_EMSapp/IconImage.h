//
//  ClosedStatusCustomCell.h
//  SPM
//
//  Created by iWizardsVI on 07/01/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import <ShinobiGrids/ShinobiGrids.h>
@class IconImage;
@protocol IconImagedelegate <NSObject>

@end
@interface IconImage : SDataGridCell


@property (nonatomic, assign) BOOL checked;
@property (nonatomic, weak) id <IconImagedelegate> imageDelegate;


@end
