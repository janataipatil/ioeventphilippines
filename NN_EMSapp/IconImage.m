//
//  ClosedStatusCustomCell.m
//  SPM
//
//  Created by iWizardsVI on 07/01/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import "IconImage.h"
#import "AppDelegate.h"
#define CHECKBOX_IMAGE_SIZE CGSizeMake(20,20)
@implementation IconImage
{
    UIImageView *_checkBoxs;
    UIImage *_onImage;
    BOOL _checked;
}
-(id)initWithReuseIdentifier:(NSString *)identifier
{
    if (self = [super initWithReuseIdentifier:identifier])
    {
        
        
        
      //  [self setChecked:_checked];
        
        // Initialise the checkbox with the off image and the correct size.
        _checkBoxs = [[UIImageView alloc] initWithImage:_onImage];
        _checkBoxs.frame = CGRectMake(10, 10, CHECKBOX_IMAGE_SIZE.width, CHECKBOX_IMAGE_SIZE.height);
       // _checkBoxs.image = [UIImage imageNamed:@"Marker.png"];
                           

        _checkBoxs.userInteractionEnabled = YES;
        [self addSubview:_checkBoxs];
        
        
    }
    return self;
}
-(void)setChecked:(BOOL)checked
{
    _checked = checked;
    
    if (_checked)
    {
        _onImage = [UIImage imageNamed:@"Marker.png"];
        _checkBoxs.image = _onImage;
    }
    else
    {
        _onImage = [UIImage imageNamed:@""];
        
        _checkBoxs.image = _onImage;
    }
}

@end
