//
//  ItinearyAccordianView.h
//  NN_EMSapp
//
//  Created by iWizards XI on 18/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItinearyAccordianView : UIView
@property (strong, nonatomic) IBOutlet  UICollectionView *ItenaryDetailCollectionView;
@property (strong, nonatomic) IBOutlet  UIButton *EditBtn;
@property (strong, nonatomic) IBOutlet  UIButton *AttachBtn;
@end
