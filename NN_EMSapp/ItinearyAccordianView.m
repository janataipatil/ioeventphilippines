//
//  ItinearyAccordianView.m
//  NN_EMSapp
//
//  Created by iWizards XI on 18/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ItinearyAccordianView.h"

@implementation ItinearyAccordianView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ItinearyAccordianView" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    

    self.ItenaryDetailCollectionView=(UICollectionView *)[mainView viewWithTag:1];
    self.EditBtn = (UIButton *)[mainView viewWithTag:2];
    self.AttachBtn = (UIButton *)[mainView viewWithTag:3];
    return self;
}

@end
