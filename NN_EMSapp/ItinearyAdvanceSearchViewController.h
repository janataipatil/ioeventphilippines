//
//  ItinearyAdvanceSearchViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 27/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"
#import "HSDatePickerViewController.h"

@interface ItinearyAdvanceSearchViewController : UIViewController<HSDatePickerViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *TextField1;

@property (strong, nonatomic) IBOutlet UITextField *TextField2;

@property (strong, nonatomic) IBOutlet UITextField *TextField3;
@property (strong, nonatomic) IBOutlet UITextField *TextField4;

@property (strong, nonatomic) IBOutlet UITextField *TextField5;

@property (strong, nonatomic) IBOutlet UITextField *TextField6;

@property (strong, nonatomic) IBOutlet UITextField *TextField7;

@property (strong, nonatomic) IBOutlet UILabel *Label1;


@property (strong, nonatomic) IBOutlet UILabel *Label2;

@property (strong, nonatomic) IBOutlet UILabel *Label3;


@property (strong, nonatomic) IBOutlet UILabel *Label4;

@property (strong, nonatomic) IBOutlet UILabel *Label5;

@property (strong, nonatomic) IBOutlet UILabel *Label6;

@property (strong, nonatomic) IBOutlet UILabel *Label7;
@property (strong, nonatomic) IBOutlet UITableView *DropdownTableView;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
- (IBAction)searchBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *TextField1Icon;

@property (strong, nonatomic) IBOutlet UIImageView *TextField2Icon;

@property (strong, nonatomic) IBOutlet UIImageView *TextField3Icon;

@property (strong, nonatomic) IBOutlet UIImageView *TextField4Icon;

@property (strong, nonatomic) IBOutlet UIImageView *TextField5Icon;

@property (strong, nonatomic) IBOutlet UIImageView *TextField6Icon;

@property (strong, nonatomic) IBOutlet UIImageView *TextField7Icon;
@property (strong, nonatomic) IBOutlet UILabel *titleView;

@property (strong, nonatomic) IBOutlet UILabel *TravelLBL;
@property (strong, nonatomic) IBOutlet UISwitch *TravelSwitch;
@property (strong, nonatomic) IBOutlet UILabel *AccomodationLBL;
@property (strong, nonatomic) IBOutlet UISwitch *AccmmodationSwitch;
@property (strong, nonatomic) IBOutlet UIView *TextField1UnderLineView;
@property (strong, nonatomic) IBOutlet UIView *TextField2UnderLineView;
@property (strong, nonatomic) IBOutlet UIView *TextField7UnderLineView;
@property (strong, nonatomic) IBOutlet UIView *TextField6UnderLineView;
@property (strong, nonatomic) IBOutlet UIView *TextField3UnderLineView;
@property (strong, nonatomic) IBOutlet UIView *TextField5UnderLineView;
@property (strong, nonatomic) IBOutlet UIView *TextField4UnderLineView;
- (IBAction)travelSwitch:(id)sender;
- (IBAction)AccommodationSwitch:(id)sender;

@end
