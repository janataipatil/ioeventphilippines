//
//  ItinearyAdvanceSearchViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 27/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ItinearyAdvanceSearchViewController.h"

@interface ItinearyAdvanceSearchViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSArray *ItinearyFields;
    int CurrentTextField;
    NSArray *LOVArr;
    NSString *hotellic;
    NSString *flightlic;
    NSString *visalic;
    NSDate *selectedStartDate;
    HSDatePickerViewController *hsdpvc;
    NSString *dt;
    
    BOOL travelSwitch;
    BOOL accommodationSwitch;
}
@end

@implementation ItinearyAdvanceSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    helper = [[HelperClass alloc]init];
    appdelegate.ItiniearySearch = nil;
    appdelegate.itinearySearch = @"NO";
    ItinearyFields = [helper query_alldata:@"ItinearyFields"];
    ItinearyFields = [ItinearyFields filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (name like %@)",@"Travel"]];
    ItinearyFields = [ItinearyFields filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (name like %@)",@"Accommodation"]];
    ItinearyFields = [ItinearyFields filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"advanceSearch like %@",@"Y"]];
    
    travelSwitch=NO;
    accommodationSwitch=NO;
    
    self.DropdownTableView.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    self.DropdownTableView.layer.borderWidth = 1.0;
    [self HideAllViews];
    for(int i=1 ; i<=ItinearyFields.count; i++)
    {
        switch (i)
        {
            case 1:
                _Label1.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                    _TextField1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                     _TextField1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];

               // _TextField1.placeholder = [ItinearyFields[i-1] valueForKey:@"localDatabasename"];
                [_Label1 setHidden:NO];
                [_TextField1UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                    [_TextField1Icon setHidden:NO];
                [_TextField1 setHidden:NO];
               
                break;
                
            case 2:
                _Label2.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                 _TextField2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                 _TextField2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
         
                [_Label2 setHidden:NO];
                [_TextField2UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                [_TextField2Icon setHidden:NO];
                [_TextField2 setHidden:NO];
                         break;
                
            case 3:
                _Label3.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                //_TextField3.placeholder = [ItinearyFields[i-1] valueForKey:@"localDatabasename"];
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                 _TextField3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                     _TextField3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                
                [_Label3 setHidden:NO];
                [_TextField3UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                [_TextField3Icon setHidden:NO];
                [_TextField3 setHidden:NO];
               // [_View3 setHidden:NO];
                break;
                
            case 4:
                _Label4.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                    _TextField4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                     _TextField4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                
               // _TextField4.placeholder = [ItinearyFields[i-1] valueForKey:@"localDatabasename"];
                [_Label4 setHidden:NO];
                [_TextField4UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                [_TextField4Icon setHidden:NO];
                [_TextField4 setHidden:NO];
               // [_View4 setHidden:NO];
                break;
                
            case 5:
                _Label5.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                    _TextField5.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                     _TextField5.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                
                
                //_TextField5.placeholder = [ItinearyFields[i-1] valueForKey:@"localDatabasename"];
                [_Label5 setHidden:NO];
                [_TextField5UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                [_TextField5Icon setHidden:NO];
                [_TextField5 setHidden:NO];
              //  [_View5 setHidden:NO];
                break;
                
            case 6:
                _Label6.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                    _TextField6.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                     _TextField6.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                
                
//                _TextField6.placeholder = [ItinearyFields[i-1] valueForKey:@"localDatabasename"];
                [_Label6 setHidden:NO];
                [_TextField6UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                [_TextField6Icon setHidden:NO];
                [_TextField6 setHidden:NO];
              //  [_View6 setHidden:NO];
                break;
                
            case 7:
                _Label7.text = [helper gettranslation:[ItinearyFields[i-1] valueForKey:@"labelid"]];
                
                if([ItinearyFields[i-1] valueForKey:@"localDatabasename"])
                    _TextField7.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[ItinearyFields[i-1] valueForKey:@"localDatabasename"] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                else
                     _TextField7.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
                
//                _TextField7.placeholder = [ItinearyFields[i-1] valueForKey:@"localDatabasename"];
                [_Label7 setHidden:NO];
                [_TextField7UnderLineView setHidden:NO];
                if([[ItinearyFields[i-1] valueForKey:@"type"] isEqualToString:@"picklist"])
                [_TextField7Icon setHidden:NO];
                [_TextField7 setHidden:NO];
             //   [_View7 setHidden:NO];
                break;
                
            
                
            default:
                break;
        }
    }
    
   
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)HideAllViews
{
    [_Label1 setHidden:YES];
    [_Label2 setHidden:YES];
    [_Label3 setHidden:YES];
    [_Label4 setHidden:YES];
    [_Label5 setHidden:YES];
    [_Label6 setHidden:YES];
    [_Label7 setHidden:YES];
    
    [_TextField1 setHidden:YES];
    [_TextField2 setHidden:YES];
    [_TextField3 setHidden:YES];
    [_TextField4 setHidden:YES];
    [_TextField5 setHidden:YES];
    [_TextField6 setHidden:YES];
    [_TextField7 setHidden:YES];
    
    [_TextField1Icon setHidden:YES];
    [_TextField2Icon setHidden:YES];
    [_TextField3Icon setHidden:YES];
    [_TextField4Icon setHidden:YES];
    [_TextField5Icon setHidden:YES];
    [_TextField6Icon setHidden:YES];
    [_TextField7Icon setHidden:YES];
    
    [_TextField1UnderLineView setHidden:YES];
    [_TextField2UnderLineView setHidden:YES];
    [_TextField3UnderLineView setHidden:YES];
    [_TextField4UnderLineView setHidden:YES];
    [_TextField5UnderLineView setHidden:YES];
    [_TextField6UnderLineView setHidden:YES];
    [_TextField7UnderLineView setHidden:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CurrentTextField=[textField tag];

    if([[self getTextFieldType:[ItinearyFields[CurrentTextField-1] valueForKey:@"name"]] isEqualToString:@"picklist"])
    {
                NSPredicate *lovpredicate;
        
        lovpredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",[ItinearyFields[CurrentTextField-1] valueForKey:@"dataindex"]];
        
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:lovpredicate];
        switch (CurrentTextField)
        {
            case 1:
                self.DropdownTableView.frame=CGRectMake(13,131,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:80 ViewToBeMoved:self.view];
                break;
                
            case 2:
                self.DropdownTableView.frame=CGRectMake(228,131,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:80 ViewToBeMoved:self.view];
                break;
                
            case 3:
                self.DropdownTableView.frame=CGRectMake(433,131,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:80 ViewToBeMoved:self.view];
                break;
                
            case 4:
                self.DropdownTableView.frame=CGRectMake(648,131,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
                break;
                
            case 5:
                self.DropdownTableView.frame=CGRectMake(13,212,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
                break;
                
            case 6:
                self.DropdownTableView.frame=CGRectMake(228,60,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
                break;
                
            case 7:
                self.DropdownTableView.frame=CGRectMake(436,60,self.DropdownTableView.frame.size.width, self.DropdownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
                break;
                
                
            default:
                break;
        }
        
        [self.DropdownTableView setHidden:NO];
        [self.DropdownTableView reloadData];
        
        
    }
    else if ([[self getTextFieldType:[ItinearyFields[CurrentTextField-1] valueForKey:@"name"]] isEqualToString:@"date"])
    {
      
        //[textField resignFirstResponder];
        [textField performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.0];
        hsdpvc=[[HSDatePickerViewController alloc]init];
        hsdpvc.delegate=self;
        
        //  DateType=@"StartDate";
        
       
       
        [self presentViewController:hsdpvc animated:YES completion:nil];
    
    }
    [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
}

- (void)hsDatePickerPickedDate:(NSDate *)date
{
   
    
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    [dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormater.dateFormat = @"dd MMM yyyy hh:mm";
    
    dt=[dateFormater stringFromDate:date];
    
    switch (CurrentTextField)
    {
        case 1:
            _TextField1.text = dt;
            break;
            
        case 2:
        
        _TextField2.text = dt;
            
            break;
            
        case 3:
            
            _TextField3.text = dt;
            
            break;
            
        case 4:
            
            _TextField4.text = dt;
            
            break;
            
        case 5:
            _TextField5.text = dt;
            break;
            
        case 6:
            
            _TextField6.text = dt;
            
            break;
            
        case 7:
            
            _TextField7.text =dt;
            
            break;
            
            
        default:
            break;
    }
    selectedStartDate=date;
//    NSDateFormatter *eventDateFormatter=[[NSDateFormatter alloc]init];
//    [eventDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
//    [eventDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
//    arrivalDate = [eventDateFormatter stringFromDate:selectedStartDate];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{

   [textField resignFirstResponder];
    [self.DropdownTableView setHidden:YES];
   [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.view];
    

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell   *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
    
    lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    return customcell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    [(UITextField *)[self.view viewWithTag:CurrentTextField] setText:[LOVArr[indexPath.row] valueForKey:@"value"]];
    
    NSString *text=[(UITextField *)[self.view viewWithTag:CurrentTextField] text];
    
    [_DropdownTableView setHidden:YES];
    
    if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"HotelClass"])
    {
        hotellic = [helper getLic:@"HOTEL_CLASS" value:text];
    }
    else if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"VisaType"])
    {
         visalic = [helper getLic:@"VISA_TYPE" value:text];
    }
    else if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"FlightClass"])
    {
        flightlic = [helper getLic:@"FLIGHT_CLASS" value:text];
    }
    
   
    
    [_TextField1 resignFirstResponder];
    [_TextField2 resignFirstResponder];
    [_TextField3 resignFirstResponder];
    [_TextField4 resignFirstResponder];
    [_TextField5 resignFirstResponder];
    [_TextField6 resignFirstResponder];
    [_TextField7 resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSString *)getTextFieldType:(NSString *)name
{
    NSString *type;
    NSArray *TextFieldData;
    TextFieldData = [ItinearyFields filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Name like %@",name]];
    
    if([TextFieldData count]>0)
        type = [TextFieldData[0] valueForKey:@"Type"];
    else
        type =@" ";
    
    return type;
    
}

- (IBAction)searchBtn:(id)sender
{
    NSDateFormatter *eventDateFormatter=[[NSDateFormatter alloc]init];
    [eventDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [eventDateFormatter setDateFormat:@"yyyyMMdd"];
    
    NSString *SearchString;
    
    if([_TextField1.text length]>0 || [_TextField2.text length]>0 || [_TextField3.text length]>0 || [_TextField4.text length]>0 || [_TextField5.text length]>0 ||[_TextField6.text length]>0 || [_TextField7.text length]>0  || [_AccmmodationSwitch isOn] || [_TravelSwitch isOn])
    {
        if (_TextField1.text.length > 0 && _TextField1.placeholder.length>0)
        {
            if([_TextField1.placeholder isEqualToString:@"arrivalDate"])
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField1.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField1.placeholder,_TextField1.text];
            }
        }
  
        if (_TextField2.text.length > 0 && _TextField2.placeholder.length>0)
        {
            if([_TextField2.placeholder isEqualToString:@"arrivalDate"])
            {
                if (SearchString.length > 0)
                {
                    SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField2.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
                }
                else
                {
                    SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField2.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
                }
            }
            else
            {
                if (SearchString.length > 0)
                {
                    SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField2.placeholder,_TextField2.text];
                }
                else
                {
                    SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField2.placeholder,_TextField2.text];
                }
            }
        }
    
    if (_TextField3.text.length > 0 && _TextField3.placeholder.length>0)
    {
        if([_TextField3.placeholder isEqualToString:@"arrivalDate"])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField3.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField3.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField3.placeholder,_TextField3.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField3.placeholder,_TextField3.text];
            }
        }
    }
    
    if (_TextField4.text.length > 0 && _TextField4.placeholder.length>0)
    {
        if([_TextField4.placeholder isEqualToString:@"arrivalDate"])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField4.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField4.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField4.placeholder,_TextField4.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField4.placeholder,_TextField4.text];
            }
        }
    }
    
    if (_TextField5.text.length > 0 && _TextField5.placeholder.length>0)
    {
        if([_TextField5.placeholder isEqualToString:@"arrivalDate"])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField5.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField5.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField5.placeholder,_TextField5.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField5.placeholder,_TextField5.text];
            }
        }
    }
    
    
    if (_TextField6.text.length > 0 && _TextField6.placeholder.length>0)
    {
    
        if([_TextField6.placeholder isEqualToString:@"arrivalDate"])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField6.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField6.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField6.placeholder,_TextField6.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField6.placeholder,_TextField6.text];
            }
        }
    }
    if (_TextField7.text.length > 0 && _TextField7.placeholder.length>0)
    {
        if([_TextField7.placeholder isEqualToString:@"arrivalDate"])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField7.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField7.placeholder,[eventDateFormatter stringFromDate:selectedStartDate]];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.%@ contains[c] \"%@\")",SearchString,_TextField7.placeholder,_TextField7.text];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.%@ contains[c] \"%@\")",_TextField7.placeholder,_TextField7.text];
            }
        }
    }
    if(travelSwitch)
    {
        if([_TravelSwitch isOn])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.travel contains[c] \"%@\")",SearchString,@"Y"];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.travel contains[c] \"%@\")",@"Y"];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.travel contains[c] \"%@\")",SearchString,@"N"];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.travel contains[c] \"%@\")",@"N"];
            }
        }
    }
    
    if(accommodationSwitch)
    {
        if([_AccmmodationSwitch isOn])
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.accommodation contains[c] \"%@\")",SearchString,@"Y"];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.accommodation contains[c] \"%@\")",@"Y"];
            }
        }
        else
        {
            if (SearchString.length > 0)
            {
                SearchString = [NSString stringWithFormat:@"%@ AND (SELF.accommodation contains[c] \"%@\")",SearchString,@"N"];
            }
            else
            {
                SearchString = [NSString stringWithFormat:@"(SELF.accommodation contains[c] \"%@\")",@"N"];
            }
        }
    }
        
        
       if([SearchString length]>0)
       {
           appdelegate.ItiniearySearch=[NSPredicate predicateWithFormat:SearchString];
           appdelegate.itinearySearch=@"YES";
       }
    }
    else
    {
            appdelegate.itinearySearch=@"NO";
            appdelegate.ItiniearySearch=nil;
    }
    
   
        
        
    appdelegate.swipevar=@"Itineary";
    [self performSegueWithIdentifier:@"BackToItineary" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
    
    
}

- (IBAction)cancelBtn:(id)sender
{
    appdelegate.swipevar=@"Itineary";
    [self performSegueWithIdentifier:@"BackToItineary" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)travelSwitch:(id)sender {
    travelSwitch= YES;
}

- (IBAction)AccommodationSwitch:(id)sender {
    accommodationSwitch = YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _DropdownTableView)
    {
        _DropdownTableView.hidden = YES;
        [_TextField1 resignFirstResponder];
        [_TextField2 resignFirstResponder];
        [_TextField3 resignFirstResponder];
        [_TextField4 resignFirstResponder];
        [_TextField5 resignFirstResponder];
        [_TextField6 resignFirstResponder];
        [_TextField7 resignFirstResponder];
    }
}


@end
