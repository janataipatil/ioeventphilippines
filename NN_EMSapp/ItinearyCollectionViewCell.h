//
//  ItinearyCollectionViewCell.h
//  NN_EMSapp
//
//  Created by iWizards XI on 24/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItinearyCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *Label;
@property (strong, nonatomic) IBOutlet UILabel *value;

@end
