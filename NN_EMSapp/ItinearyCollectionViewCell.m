//
//  ItinearyCollectionViewCell.m
//  NN_EMSapp
//
//  Created by iWizards XI on 24/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ItinearyCollectionViewCell.h"

@implementation ItinearyCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ItinearyCollectionViewCell" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    _Label = (UILabel *)[mainView viewWithTag:1];
    _value = (UILabel *)[mainView viewWithTag:2];
    
    return self;
}

@end
