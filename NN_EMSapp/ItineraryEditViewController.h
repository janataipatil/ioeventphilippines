//
//  ItineraryEditViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 24/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"
#import "HSDatePickerViewController.h"

@interface ItineraryEditViewController : UIViewController<HSDatePickerViewControllerDelegate>

@property (strong, nonatomic)IBOutlet NSArray *ItinearyData;
@property (strong, nonatomic) IBOutlet UITableView *DropDownTableView;

@property (strong, nonatomic) IBOutlet UIView *View1;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UITextField *textfield1;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown1;

@property (strong, nonatomic) IBOutlet UIView *View2;
@property (strong, nonatomic) IBOutlet UILabel *Label2;
@property (strong, nonatomic) IBOutlet UITextField *Textfield2;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown2;

@property (strong, nonatomic) IBOutlet UIView *View3;
@property (strong, nonatomic) IBOutlet UITextField *TextField3;
@property (strong, nonatomic) IBOutlet UILabel *Label3;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown3;

@property (strong, nonatomic) IBOutlet UIView *View4;
@property (strong, nonatomic) IBOutlet UITextField *TextField4;
@property (strong, nonatomic) IBOutlet UILabel *Label4;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown4;

@property (strong, nonatomic) IBOutlet UIView *View5;
@property (strong, nonatomic) IBOutlet UITextField *TextField5;
@property (strong, nonatomic) IBOutlet UILabel *Label5;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown5;

@property (strong, nonatomic) IBOutlet UIView *View6;
@property (strong, nonatomic) IBOutlet UITextField *TexttField6;
@property (strong, nonatomic) IBOutlet UILabel *Label6;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown6;

@property (strong, nonatomic) IBOutlet UIView *View7;
@property (strong, nonatomic) IBOutlet UILabel *Label7;
@property (strong, nonatomic) IBOutlet UITextField *TextField7;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown7;

@property (strong, nonatomic) IBOutlet UIView *View9;
@property (strong, nonatomic) IBOutlet UILabel *Label9;
@property (strong, nonatomic) IBOutlet UITextField *TextField9;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown9;

@property (strong, nonatomic) IBOutlet UIView *View8;
@property (strong, nonatomic) IBOutlet UILabel *Label8;
@property (strong, nonatomic) IBOutlet UITextField *TextField8;
@property (strong, nonatomic) IBOutlet UIImageView *dropdown8;

@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)saveBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *cancelbtn;
- (IBAction)cancelbtn:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *Travellbl;
@property (strong, nonatomic) IBOutlet UILabel *Accommodationlbl;
@property (strong, nonatomic) IBOutlet UISwitch *travelSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *accommodationSwitch;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

- (IBAction)AccommodationSwitch:(id)sender;
- (IBAction)travelSwitch:(id)sender;


@end
