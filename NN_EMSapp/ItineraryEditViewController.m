//
//  ItineraryEditViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 24/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ItineraryEditViewController.h"


@interface ItineraryEditViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSArray *LOVArr;
    int CurrentTextField;
    int TextFieldTag;
    NSString *hotelLIC;
    NSString *visaTypeLIC;
    NSString *flightClassLIC;
    NSString *foodPreferenceLIC;
    HSDatePickerViewController *hsdpvc;
    NSDate *selectedStartDate;
    NSString *arrivalDate;
    NSString *dt;
    

    
}
@end

@implementation ItineraryEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    [self.DropDownTableView setHidden:YES];
     [_View1 setHidden:YES];
     [_View2 setHidden:YES];
     [_View3 setHidden:YES];
     [_View4 setHidden:YES];
     [_View5 setHidden:YES];
     [_View6 setHidden:YES];
     [_View7 setHidden:YES];
     [_View8 setHidden:YES];
     [_View9 setHidden:YES];
    
    self.DropDownTableView.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    self.DropDownTableView.layer.borderWidth = 1.0;
    
    _ItinearyData = appdelegate.ItnearyData;
    
    
    NSArray *SelItinitary =[[helper query_alldata:@"Event_Itenary"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
     SelItinitary = [SelItinitary filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"itineraryID like %@",appdelegate.itnearyid]];
   
    
    _Travellbl.text = [helper gettranslation:@"LBL_874"];
    _Accommodationlbl.text = [helper gettranslation:@"LBL_873"];
    [_saveBtn setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    [_cancelbtn setTitle:[helper gettranslation:@"LBL_061"]  forState:UIControlStateNormal];
    
    [_titleLbl setText:[helper gettranslation:@"LBL_881"]];
    
    
    if([SelItinitary count]>0)
    {
        if([[SelItinitary[0] valueForKey:@"travel"] isEqualToString:@"Y"])
        {
            [_travelSwitch setOn:YES];
        }
        else
        {
            [_travelSwitch setOn:NO];
        }
        
        if([[SelItinitary[0] valueForKey:@"accommodation"] isEqualToString:@"Y"])
        {
            [_accommodationSwitch setOn:YES];
        }
        else
        {
            [_accommodationSwitch setOn:NO];
        }
    }
    
    for (int i=0; i<[_ItinearyData count]+1; i++)
    {
        switch (i)
        {
            case 1:
                _label1.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
               
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _textfield1.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                 _textfield1.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _textfield1.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown1 setHidden:NO];
                
                
                
                [_View1 setHidden:NO];
                break;
                
            case 2:
                _Label2.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
             
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _Textfield2.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _Textfield2.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _Textfield2.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown2 setHidden:NO];
               
                [_View2 setHidden:NO];
                break;
                
            case 3:
                _Label3.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
               
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TextField3.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TextField3.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TextField3.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown3 setHidden:NO];
                
                [_View3 setHidden:NO];
                break;
                
            case 4:
                _Label4.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
               
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TextField4.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TextField4.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TextField4.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown4 setHidden:NO];
                [_View4 setHidden:NO];
                break;
                
            case 5:
                _Label5.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TextField5.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TextField5.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TextField5.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown5 setHidden:NO];
                [_View5 setHidden:NO];
                break;
                
            case 6:
                _Label6.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TexttField6.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TexttField6.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TexttField6.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown6 setHidden:NO];
                [_View6 setHidden:NO];
                break;
                
            case 7:
                _Label7.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TextField7.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TextField7.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TextField7.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown7 setHidden:NO];
                
                [_View7 setHidden:NO];
                break;
                
            case 8:
                _Label8.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TextField8.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TextField8.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TextField8.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown8 setHidden:NO];
                
                [_View8 setHidden:NO];
                break;
                
            case 9:
                _Label9.text = [helper gettranslation:[_ItinearyData[i-1] valueForKey:@"Label"]];
                if([[_ItinearyData[i-1] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
                    _TextField9.text =[helper formatingdate:[_ItinearyData[i-1] valueForKey:@"Value"] datetime:@"FormatDate" ];
                else
                    _TextField9.text =[_ItinearyData[i-1] valueForKey:@"Value"];
                
                _TextField9.placeholder =[_ItinearyData[i-1] valueForKey:@"Name"];
                
                if([[_ItinearyData[i-1] valueForKey:@"Type"] isEqualToString:@"picklist"])
                    [_dropdown9 setHidden:NO];
                
                [_View9 setHidden:NO];
                break;
                
            default:
                break;
        }
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CurrentTextField=[textField tag];
    if([[self getTextFieldType:textField.placeholder] isEqualToString:@"picklist"])
    {
        
        NSPredicate *lovpredicate;

            lovpredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",[_ItinearyData[CurrentTextField-1] valueForKey:@"DataIndex"]];

        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:lovpredicate];
        switch (CurrentTextField)
        {
            case 1:
                 self.DropDownTableView.frame=CGRectMake(128,150,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:80 ViewToBeMoved:self.view];
                break;
                
            case 2:
                 self.DropDownTableView.frame=CGRectMake(390,150,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:80 ViewToBeMoved:self.view];
                break;
                
            case 3:
                self.DropDownTableView.frame=CGRectMake(644,150,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:80 ViewToBeMoved:self.view];
                break;
                
            case 4:
                 self.DropDownTableView.frame=CGRectMake(128,240,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
                break;
                
            case 5:
               self.DropDownTableView.frame=CGRectMake(390,240,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
                break;
                
            case 6:
                self.DropDownTableView.frame=CGRectMake(644,240,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
                break;
                
            case 7:
                self.DropDownTableView.frame=CGRectMake(128,320,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
                break;
                
            case 8:
                self.DropDownTableView.frame=CGRectMake(400,320,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
                break;
                
            case 9:
             self.DropDownTableView.frame=CGRectMake(644,320,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
                break;
                
            default:
                break;
        }
        
        [self.DropDownTableView setHidden:NO];
        [self.DropDownTableView reloadData];
        
        
    }
    else if ([[self getTextFieldType:textField.placeholder] isEqualToString:@"date"])
    {
        
        [textField performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.0];
        hsdpvc=[[HSDatePickerViewController alloc]init];
        hsdpvc.delegate=self;
        
      //  DateType=@"StartDate";
        
        [self presentViewController:hsdpvc animated:YES completion:nil];
    }
    
    
    [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    int TextField=[textField tag];

//    switch (TextField)
//    {
//        case 1:
//              [helper MoveViewUp:NO Height:80 ViewToBeMoved:self.view];
//            break;
//
//        case 2:
//
//            [helper MoveViewUp:NO Height:80 ViewToBeMoved:self.view];
//            break;
//
//        case 3:
////            self.DropDownTableView.frame=CGRectMake(654,297,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:80 ViewToBeMoved:self.view];
//            break;
//
//        case 4:
////            self.DropDownTableView.frame=CGRectMake(123,379,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
//            break;
//
//        case 5:
////            self.DropDownTableView.frame=CGRectMake(394,379,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
//            break;
//
//        case 6:
////            self.DropDownTableView.frame=CGRectMake(654,379,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
//            break;
//
//        case 7:
////            self.DropDownTableView.frame=CGRectMake(123,463,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
//            break;
//
//        case 8:
////            self.DropDownTableView.frame=CGRectMake(394,463,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
//            break;
//
//        case 9:
////            self.DropDownTableView.frame=CGRectMake(654,463,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//                            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
//            break;
//
//        default:
//            break;
//    }
    [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
    [textField resignFirstResponder];
    [self.DropDownTableView setHidden:YES];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
   
    lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    return customcell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    [(UITextField *)[self.view viewWithTag:CurrentTextField] setText:[LOVArr[indexPath.row] valueForKey:@"value"]];
    
    NSString *text=[(UITextField *)[self.view viewWithTag:CurrentTextField] text];
    
    [_DropDownTableView setHidden:YES];
    
  if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"HotelClass"])
  {
      hotelLIC = [helper getLic:@"HOTEL_CLASS" value:text];
  }
  else if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"VisaType"])
  {
      visaTypeLIC = [helper getLic:@"VISA_TYPE" value:text];
  }
  else if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"FlightClass"])
  {
      flightClassLIC = [helper getLic:@"FLIGHT_CLASS" value:text];
  }
  else if ([[(UITextField *)[self.view viewWithTag:CurrentTextField] placeholder] isEqualToString:@"FoodPreference"])
  {
      foodPreferenceLIC = [helper getLic:@"FOOD_PRF" value:text];
  }
    
    [_textfield1 resignFirstResponder];
    [_Textfield2 resignFirstResponder];
    [_TextField3 resignFirstResponder];
    [_TextField4 resignFirstResponder];
    [_TextField5 resignFirstResponder];
    [_TexttField6 resignFirstResponder];
    [_TextField7 resignFirstResponder];
    [_TextField8 resignFirstResponder];
    [_TextField9 resignFirstResponder];

}
/*
#pragma mark - Navigation

// In a storyboar-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
     int TextField=[textField tag];
//    switch (TextField)
//    {
//        case 1:
//            [helper MoveViewUp:NO Height:80 ViewToBeMoved:self.view];
//            break;
//
//        case 2:
//
//            [helper MoveViewUp:NO Height:80 ViewToBeMoved:self.view];
//            break;
//
//        case 3:
//            //            self.DropDownTableView.frame=CGRectMake(654,297,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:80 ViewToBeMoved:self.view];
//            break;
//
//        case 4:
//            //            self.DropDownTableView.frame=CGRectMake(123,379,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
//            break;
//
//        case 5:
//            //            self.DropDownTableView.frame=CGRectMake(394,379,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
//            break;
//
//        case 6:
//            //            self.DropDownTableView.frame=CGRectMake(654,379,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
//            break;
//
//        case 7:
//            //            self.DropDownTableView.frame=CGRectMake(123,463,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
//            break;
//
//        case 8:
//            //            self.DropDownTableView.frame=CGRectMake(394,463,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
//            break;
//
//        case 9:
//            //            self.DropDownTableView.frame=CGRectMake(654,463,self.DropDownTableView.frame.size.width, self.DropDownTableView.frame.size.height);
//            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
//            break;
//
//        default:
//            break;
//    }
    [textField resignFirstResponder];
    return YES;
}

-(NSString *)getTextFieldType:(NSString *)name
{
    NSString *type;
    NSArray *TextFieldData;
    TextFieldData = [_ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Name like %@",name]];
    
    if([TextFieldData count]>0)
        type = [TextFieldData[0] valueForKey:@"Type"];
    else
        type =@" ";
    
    return type;
    
}

-(void)SaveItinearyData
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
  
    NSArray *SelectedItinearyData = [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"itineraryID like %@",appdelegate.itnearyid]];
    
    if(!arrivalDate)
    {
        arrivalDate =  [SelectedItinearyData[0] valueForKey:@"arrivalDate"];
    }
    NSMutableDictionary *ReqDict = [[NSMutableDictionary alloc]init];
    if([SelectedItinearyData count]>0)
    {
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"eventid"] forKey:@"EventID"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"id"] forKey:@"ID"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"itineraryID"] forKey:@"ItineraryID"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"affiliateID"] forKey:@"AffiliateID"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"salutationLIC"]forKey:@"SalutationLIC"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"salutation"] forKey:@"Salutation"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"fullname"] forKey:@"Name"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"partyID"] forKey:@"PartyID"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"partyTypeLIC"] forKey:@"PartyTypeLIC"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"entityID"] forKey:@"EntityID"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"entityTypeLIC"] forKey:@"EntityTypeLIC"];
        [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"authorizationTypeLIC"] forKey:@"AuthorizationTypeLIC"];
        
        if([hotelLIC length]>0)
            [ReqDict setValue:hotelLIC forKey:@"HotelClassLIC"];
        else
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"hotelClassLIC"] forKey:@"HotelClassLIC"];
       
        if([flightClassLIC length]>0)
            [ReqDict setValue:flightClassLIC forKey:@"FlightClassLIC"];
        else
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"flightClassLIC"] forKey:@"FlightClassLIC"];
        
        if([visaTypeLIC length]>0)
            [ReqDict setValue:visaTypeLIC forKey:@"VisaTypeLIC"];
        else
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"visaTypeLIC"] forKey:@"VisaTypeLIC"];
        
        if([foodPreferenceLIC length]>0)
            [ReqDict setValue:foodPreferenceLIC forKey:@"FoodPreferenceLIC"];
        else
            [ReqDict setValue:[SelectedItinearyData[0] valueForKey:@"foodPreferenceLIC"] forKey:@"FoodPreferenceLIC"];

        
        [ReqDict setValue:[helper getvaluefromlic:@"HOTEL_CLASS" lic:hotelLIC] forKey:@"HotelClass"];
        [ReqDict setValue:[helper getvaluefromlic:@"FLIGHT_CLASS" lic:flightClassLIC] forKey:@"FlightClass"];
        [ReqDict setValue:[helper getvaluefromlic:@"VISA_TYPE" lic:visaTypeLIC] forKey:@"VisaType"];
        [ReqDict setValue:[helper getvaluefromlic:@"FOOD_PRF" lic:foodPreferenceLIC] forKey:@"FoodPreference"];
        
        if([_textfield1.placeholder length]>0)
        {
            if ([_textfield1.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_textfield1.placeholder];
            }
            else
            {
                [ReqDict setValue:_textfield1.text forKey:_textfield1.placeholder];
            }
        }
        
        if([_Textfield2.placeholder length]>0)
        {
            if ([_Textfield2.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_Textfield2.placeholder];
            }
            else
            {
                [ReqDict setValue:_Textfield2.text forKey:_Textfield2.placeholder];
            }
        }
        
        if([_TextField3.placeholder length]>0)
        {
            if ([_TextField3.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TextField3.placeholder];
            }
            else
            {
                [ReqDict setValue:_TextField3.text forKey:_TextField3.placeholder];
            }
        }
        
        if([_TextField4.placeholder length]>0)
        {
            if ([_TextField4.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TextField4.placeholder];
            }
            else
            {
                [ReqDict setValue:_TextField4.text forKey:_TextField4.placeholder];
            }
        }
        if([_TextField5.placeholder length]>0)
        {
            if ([_TextField5.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TextField5.placeholder];
            }
            else
            {
                [ReqDict setValue:_TextField5.text forKey:_TextField5.placeholder];
            }
        }
        
        if([_TexttField6.placeholder length]>0)
        {
            if ([_TexttField6.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TexttField6.placeholder];
            }
            else
            {
            [ReqDict setValue:_TexttField6.text forKey:_TexttField6.placeholder];
            }
        }
        if([_TextField7.placeholder length]>0)
        {
            if ([_TextField7.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TextField7.placeholder];
            }
            else
            {
            [ReqDict setValue:_TextField7.text forKey:_TextField7.placeholder];
            }
        }
        
        if([_TextField8.placeholder length]>0)
        {
            if ([_TextField8.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TextField8.placeholder];
            }
            else
            {
                [ReqDict setValue:_TextField8.text forKey:_TextField8.placeholder];
            }
        }
        if([_TextField9.placeholder length]>0)
        {
            if ([_TextField9.placeholder isEqualToString:@"ArrivalDate"])
            {
                [ReqDict setValue:arrivalDate forKey:_TextField9.placeholder];
            }
            else
            {
                [ReqDict setValue:_TextField9.text forKey:_TextField9.placeholder];
            }
        }
        if([_travelSwitch isOn])
        {
            [ReqDict setValue:[helper stringtobool:@"Y"] forKey:@"Travel"];
        }
        else
        {
            [ReqDict setValue:[helper stringtobool:@"N"] forKey:@"Travel"];
        }
        
        if([_accommodationSwitch isOn])
        {
            [ReqDict setValue:[helper stringtobool:@"Y"] forKey:@"Accommodation"];
        }
        else
        {
            [ReqDict setValue:[helper stringtobool:@"N"] forKey:@"Accommodation"];
        }
        
        [ReqDict setValue:arrivalDate forKey:@"ArrivalDate"];
        
//        NSDictionary *test = [SelectedItinearyData objectAtIndex:0];
    
  
            NSString *jsonString;
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ReqDict options:0 error:&error];
            if (! jsonData)
            {
                NSLog(@"Got an error: %@", error);
            }
            else
            {
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
            NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
            NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
            NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
            NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetItinerary" pageno:@"" pagecount:@"" lastpage:@""];

            NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
            if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
            {
                [helper removeWaitCursor];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
                [alert show];
            }
            else
            {
                NSMutableArray *JsonArray = [[NSMutableArray alloc] init];
                [JsonArray addObject:ReqDict];
            
                [helper DeleteItinearyRecord:appdelegate.itnearyid];
                [helper InsertItenaryData:JsonArray eventid:appdelegate.eventid];
                [helper removeWaitCursor];
                [self RemovePopup];
            
            }    
    }
    else
    {
        [helper removeWaitCursor];
        [helper showalert:@"Alert" message:@"An Error Occured ! " action:@"OK"];
    }
    
}


- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    [dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormater.dateFormat = @"dd MMM yyyy hh:mm";
    
    dt=[dateFormater stringFromDate:date];
    
    switch (CurrentTextField)
    {
        case 1:
            _textfield1.text = dt;
            break;
            
        case 2:
            
            _Textfield2.text = dt;
           
         
            break;
            
        case 3:
            
            _TextField3.text = dt;
            
            break;
            
        case 4:
           
            _TextField4.text = dt;
 
            break;
            
        case 5:
            _TextField5.text = dt;
            break;
            
        case 6:
          
            _TexttField6.text = dt;
            break;
            
        case 7:
            
            _TextField7.text =dt;
            break;
            
        case 8:
          
            _TextField8.text = dt;
            break;
            
        case 9:
           
            _TextField9.text = dt;
            break;
            
        default:
            break;
    }
        selectedStartDate = date;
    
        NSDate *oldDate = [dateFormater dateFromString:_textfield1.text];
    
        NSDateFormatter *eventDateFormatter=[[NSDateFormatter alloc]init];
        [eventDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [eventDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        if(selectedStartDate)
        {
             arrivalDate = [eventDateFormatter stringFromDate:selectedStartDate];
        }
        else
        {
            arrivalDate =  [eventDateFormatter stringFromDate:oldDate];
        }
}

- (IBAction)saveBtn:(id)sender {
    [self SaveItinearyData];
}
- (IBAction)cancelbtn:(id)sender {
    [self RemovePopup];
    
}
-(void)RemovePopup
{
    appdelegate.swipevar=@"Itineary";
    [self performSegueWithIdentifier:@"BackToEventDetail" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)AccommodationSwitch:(id)sender {
}

- (IBAction)travelSwitch:(id)sender {
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _DropDownTableView)
    {
        _DropDownTableView.hidden = YES;
        [_textfield1 resignFirstResponder];
        [_Textfield2 resignFirstResponder];
        [_TextField3 resignFirstResponder];
        [_TextField4 resignFirstResponder];
        [_TextField5 resignFirstResponder];
        [_TexttField6 resignFirstResponder];
        [_TextField7 resignFirstResponder];
        [_TextField8 resignFirstResponder];
        [_TextField9 resignFirstResponder];
        
    }
}

@end
