//
//  ItineraryViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 11/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItinearyAccordianView.h"
#import "ItinearyCollectionViewCell.h"

@interface ItineraryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *SyncDataBtn;
- (IBAction)SyncData:(id)sender;

@property (strong, nonatomic) ItinearyAccordianView *AccordianView;
@property (strong, nonatomic) ItinearyCollectionViewCell *collectionViewCell;
@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;
@property (strong, nonatomic) IBOutlet UISegmentedControl *ItenaryTypeSegmentControl;
- (IBAction)ItenaryTypeSegmentControl:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *eventName_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventStartDate_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventEndDate_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventName_value_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventStartDate_value_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventEndDate_value_lbl;
@property (strong, nonatomic) IBOutlet UITableView *ItinearyTableView;
@property (strong, nonatomic) IBOutlet UILabel *ItinearyTableViewHeaderSalutation;
@property (strong, nonatomic) IBOutlet UILabel *ItinearyTableViewHeaderLastName;
@property (strong, nonatomic) IBOutlet UILabel *ItinearyTableViewHeaderSpeciality;

@property (strong, nonatomic) IBOutlet UILabel *ItinearyTableViewHeaderFirstName;
@property (strong, nonatomic) IBOutlet UILabel *ItinearyTableViewHeaderAccomadation;
@property (strong, nonatomic) IBOutlet UILabel *ItinearyTableViewHeaderStatus;

- (IBAction)SalutationSortButton:(id)sender;
- (IBAction)TravelSortButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *AccommodationSortButton;
- (IBAction)AccommodationSortButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *ArrivalDateSortButton;

@property (strong, nonatomic) IBOutlet UIButton *ItinearyAdvanceSearchBtn;
- (IBAction)ItinearyAdvanceSearchBtn:(id)sender;


@property (strong, nonatomic) IBOutlet UIImageView *FullNameSortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *TravelSortIcon;

@property (strong, nonatomic) IBOutlet UIImageView *AccommadationSortIcon;
@property (strong, nonatomic) IBOutlet UIImageView *ArrivalDateSortIcon;
- (IBAction)ArrivalDateSortButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *clearSearch;
- (IBAction)clearSearchBtn:(id)sender;

@end
