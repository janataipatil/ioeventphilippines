//
//  ItineraryViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 11/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ItineraryViewController.h"
#import "HelperClass.h"
#import "AppDelegate.h"

@interface ItineraryViewController ()
{
    AppDelegate *appdelegate;   
    HelperClass *helper;
    
    NSArray *ItinearyData;
    NSArray *ItinearyFields;
    NSInteger ln_expandedRowIndex;
    NSArray *itenaryRecord;
    NSArray *EventDetails;
    NSArray *ItinearyDataSorted;
    
    NSString *itinearyName;
    BOOL name;
    BOOL travel;
    BOOL accommodation;
    BOOL ArrivalDate;
    
    
}
@end

@implementation ItineraryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    
    
  //  NSArray *TEST=[helper query_alldata:@"Event_Itenary"];
    
  // if([appdelegate.itinearySearch isEqualToString:@"YES"])
   //{
    
    [_SearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchfield = [_SearchBar valueForKey:@"_searchField"];
    searchfield.placeholder = [helper gettranslation:@"LBL_224"];
    searchfield.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:242.0/255.0f green:246.0/255.0f blue:250.0f/255.0f alpha:1.0f];
       _ItenaryTypeSegmentControl.selectedSegmentIndex=appdelegate.selectedItineraraySegment;
       [_clearSearch setHidden:NO];
    
    [self refreshData];

    ln_expandedRowIndex = -1;
    if([ItinearyData count]>0)
    {
        ItinearyFields = [helper query_alldata:@"ItinearyFields"];

        [self.AccordianView.ItenaryDetailCollectionView registerNib:[UINib nibWithNibName:@"ItinearyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ItinearyCollectionViewCell"];
    }
   
    _AccordianView = [[ItinearyAccordianView alloc] init];
    _collectionViewCell =[[ItinearyCollectionViewCell alloc]init];
    
    [self.AccordianView.ItenaryDetailCollectionView registerNib:[UINib nibWithNibName:@"ItinearyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ItinearyCollectionViewCell"];
    
    [self setupLangTranslations];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupLangTranslations
{
    _eventName_lbl.text = [helper gettranslation:@"LBL_131"] ;
    _eventStartDate_lbl.text = [helper gettranslation:@"LBL_239"] ;
    _eventEndDate_lbl.text = [helper gettranslation:@"LBL_122"] ;
    
    _ItinearyTableViewHeaderSalutation.text = [helper gettranslation:@"LBL_175"];
   _ItinearyTableViewHeaderFirstName.text = [helper gettranslation:@"LBL_141"];
    _ItinearyTableViewHeaderLastName.text = [helper gettranslation:@"LBL_153"];
    _ItinearyTableViewHeaderSpeciality.text = [helper gettranslation:@"LBL_874"];
    _ItinearyTableViewHeaderAccomadation.text = [helper gettranslation:@"LBL_873"];
    _ItinearyTableViewHeaderStatus.text = [helper gettranslation:@"LBL_875"];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 145; // 60+77
    }
    
    return 40;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ItinearyData count] + (ln_expandedRowIndex != -1 ? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = [indexPath row];
    NSInteger dataIndex = [self dataIndexForRowIndex:row];
    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
    if (!ln_expandedCell)
    {
        SOCustomCell *customcell;
        
        NSString *cellIdentifier = @"customcell";
        
        NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
        /* if (indexPath.row >= attendeelist.count)
         {
         [SelectedArray insertObject:attendeelist[indexPath.row - 1] atIndex:0];
         }
         else
         {*/
        [SelectedArray insertObject:ItinearyData[dataIndex] atIndex:0];
        //}
        
        customcell = [self.ItinearyTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
        customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        
        UILabel *salutation = (UILabel *) [contentView viewWithTag:1];
//        UILabel *firstname = (UILabel *) [contentView viewWithTag:2];
//        UILabel *lastname = (UILabel *) [contentView viewWithTag:3];
        UIImageView *travel = (UIImageView *) [contentView viewWithTag:4];
        UIImageView *accommodation = (UIImageView *) [contentView viewWithTag:5];
        
        UILabel *arrivaldate = (UILabel *) [contentView viewWithTag:6];

        
        
        
       // [editing_btn addTarget:self action:@selector(editactivityClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSLog(@"indexPath.row = %ld",(long)indexPath.row);
        
        [salutation setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"fullname"]?:@""]];
        
        if([[SelectedArray[0] valueForKey:@"travel"] isEqualToString:@"Y"])
        {
            [travel setImage:[UIImage imageNamed:@"true"]];
        }
        else
        {
            [travel setImage:[UIImage imageNamed:@"false"]];
        }
        
        if([[SelectedArray[0] valueForKey:@"accommodation"] isEqualToString:@"Y"])
        {
            [accommodation setImage:[UIImage imageNamed:@"true"]];
        }
        else
        {
            [accommodation setImage:[UIImage imageNamed:@"false"]];
        }
        //[ownedby setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"accommodation"]?:@""]];
       // [status setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"Travel"]?:@""]];
        
        
                
        
        [arrivaldate setText:[helper formatingdate:[SelectedArray[0] valueForKey:@"arrivalDate"] datetime:@"FormatDate"]];
        
        [customcell setSelectionStyle:UITableViewCellSelectionStyleNone];
//
//        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] || (![appdelegate.UserRolesArray containsObject:@"ATT_R"] && ![appdelegate.UserRolesArray containsObject:@"ATT_W"]))
//        {
//            [editing_btn setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
//        }
//        if ([appdelegate.UserRolesArray containsObject:@"ATT_W"])
//        {
//            [editing_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
//        }
//        if ([appdelegate.UserRolesArray containsObject:@"ATT_R"] && [appdelegate.UserRolesArray containsObject:@"ATT_W"])
//        {
//            [editing_btn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
//        }
        int count=indexPath.row;
        NSLog(@"count:%d",count);
        
        
        return customcell;
        
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        
        
        self.AccordianView.ItenaryDetailCollectionView.delegate = self;
        self.AccordianView.ItenaryDetailCollectionView.dataSource = self;
        
        UIButton *attachButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [attachButton setImage:[UIImage imageNamed:@"attach.png"] forState:UIControlStateNormal];
        attachButton.frame=CGRectMake(826, 48,48,44);
        
        UIButton *edit=[UIButton buttonWithType:UIButtonTypeCustom];
        [edit setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
        edit.frame=CGRectMake(826, 0,48,44);
        
        [cell.contentView addSubview:self.AccordianView];
        [cell.contentView addSubview:attachButton];
        [cell.contentView addSubview:edit];
        
        [edit addTarget:self action:@selector(EditItinearyRecord:) forControlEvents:UIControlEventTouchUpInside];
        [attachButton addTarget:self action:@selector(uploadAttachment:) forControlEvents:UIControlEventTouchUpInside];
        
        
       // [self setupAttendeedetails:[attendeelist[indexPath.row-1] valueForKey:@"id"] backcolor:cell.backgroundColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
        [self.AccordianView.ItenaryDetailCollectionView reloadData];
        return cell;
    }
    

}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    //itenaryRecord
    NSInteger row = [indexPath row];
    NSInteger dataIndex = [self dataIndexForRowIndex:row];
    
    
    NSArray *colName = [helper getcolumn_names:@"Event_Itenary"];
    NSMutableArray *SelectedItenearyData=[[NSMutableArray alloc]init];
    for(int i=0;i<colName.count;i++)
    {
        NSArray *TranslationArray = [ItinearyFields mutableCopy];
        NSString *Label = [colName objectAtIndex:i];
        NSString *values = [ItinearyData[dataIndex] valueForKey:Label];
    //    NSPredicate *pred=[NSPredicate predicateWithFormat:@"name like %@",Label];
        NSArray *LabelTranslation = [TranslationArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name contains[c] %@",Label]];
        
        if ([LabelTranslation count]>0 )
        {
            NSMutableDictionary *Iteneary=[[NSMutableDictionary alloc]init];
            if([[LabelTranslation[0] valueForKey:@"name"] isEqualToString:@"Accommodation"] ||[[LabelTranslation[0] valueForKey:@"name"] isEqualToString:@"FirstName"] || [[LabelTranslation[0] valueForKey:@"name"] isEqualToString:@"LastName"] || [[LabelTranslation[0] valueForKey:@"name"] isEqualToString:@"Travel"] || [[LabelTranslation[0] valueForKey:@"name"] isEqualToString:@"Salutation"])
            {
                
            }
            else
            {
                [Iteneary setValue:[LabelTranslation[0] valueForKey:@"labelid"]forKey:@"Label"];
                [Iteneary setValue:[LabelTranslation[0] valueForKey:@"name"]forKey:@"Name"];
                [Iteneary setValue:[LabelTranslation[0] valueForKey:@"type"] forKey:@"Type"];
                [Iteneary setValue:[LabelTranslation[0] valueForKey:@"dataindex"] forKey:@"DataIndex"];
                [Iteneary setValue:[ItinearyData[dataIndex] valueForKey:colName[i]] forKey:@"Value"];
                [SelectedItenearyData addObject:Iteneary];
            }
        }
    }
    itenaryRecord = [SelectedItenearyData mutableCopy];
    
    appdelegate.ItnearyData = [SelectedItenearyData mutableCopy];
    
    appdelegate.itnearyid = [ItinearyData[dataIndex] valueForKey:@"itineraryID"];
    
    appdelegate.attendeeName = [ItinearyData[dataIndex] valueForKey:@"fullname"];
    
    [self.AccordianView.ItenaryDetailCollectionView reloadData];
    
    BOOL preventReopen = NO;
    
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        //        cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
        
    }
    [tableView endUpdates];
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}

- (IBAction)ItenaryTypeSegmentControl:(id)sender
{
    switch (self.ItenaryTypeSegmentControl.selectedSegmentIndex) {
        case 0:
                ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"ATTENDEE"]];
                if([ItinearyData count]>0)
                {
                    ItinearyFields = [helper query_alldata:@"ItinearyFields"];
                }
                appdelegate.selectedItineraraySegment=0;
                appdelegate.itinearySearch=@"NO";
                ln_expandedRowIndex=-1;
                [_clearSearch setHidden:YES];
                _SearchBar.text=@"";
                [self.ItinearyTableView reloadData];
                break;
       case 1:
                ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"TEAM"]];
                if([ItinearyData count]>0)
                {
                    ItinearyFields = [helper query_alldata:@"ItinearyFields"];
                }
                ln_expandedRowIndex=-1;
                appdelegate.itinearySearch=@"NO";
                appdelegate.selectedItineraraySegment=1;
                [_clearSearch setHidden:YES];
                _SearchBar.text=@"";
                [self.ItinearyTableView reloadData];
                break;
       case 2:
                ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"SPEAKER"]];
                if([ItinearyData count]>0)
                {
                    ItinearyFields = [helper query_alldata:@"ItinearyFields"];
                }
                ln_expandedRowIndex=-1;
                appdelegate.itinearySearch=@"NO";
                appdelegate.selectedItineraraySegment=2;
                [_clearSearch setHidden:YES];
                _SearchBar.text=@"";
                [self.ItinearyTableView reloadData];
            break;
            
        default:
            break;
    }

}
- (IBAction)SalutationSortButton:(id)sender
{
    name=!name;
    if(name)
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"fullname" type:@"abc" Ascending:YES];

        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"fullname" type:@"abc" Ascending:NO];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
    
}

- (IBAction)TravelSortButton:(id)sender
{
    travel=!travel;
    if(travel)
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"travel" type:@"abc" Ascending:YES];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"travel" type:@"abc" Ascending:NO];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }
}

- (NSInteger)dataIndexForRowIndex:(NSInteger)row
{
    if (ln_expandedRowIndex != -1 && ln_expandedRowIndex <= row)
    {
        if (ln_expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return itenaryRecord.count;
  //  return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ItinearyCollectionViewCell *cell=(ItinearyCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ItinearyCollectionViewCell" forIndexPath:indexPath];
    
    NSLog(@"%@:",[helper gettranslation:[itenaryRecord[indexPath.row] valueForKey:@"Label"]]);
     NSLog(@"%@:",[itenaryRecord[indexPath.row] valueForKey:@"Value"]);
    
    cell.Label.text = [helper gettranslation:[itenaryRecord[indexPath.row] valueForKey:@"Label"]];
    
    if([[itenaryRecord[indexPath.row] valueForKey:@"Name"] isEqualToString:@"ArrivalDate"])
        cell.value.text = [helper formatingdate:[itenaryRecord[indexPath.row] valueForKey:@"Value"] datetime:@"FormatDate"];
    else
        cell.value.text = [itenaryRecord[indexPath.row] valueForKey:@"Value"];

    return  cell;
}

-(IBAction)EditItinearyRecord:(id)sender
{
    [self.parentViewController performSegueWithIdentifier:@"EditItinearyRecord" sender:self];
}

-(IBAction)uploadAttachment:(id)sender
{
    appdelegate.AttachmentType =@"Itineary";
    
    [self.parentViewController performSegueWithIdentifier:@"showAttendeeAttachment" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAttendeeAttachment"])
    {
        AttachmentViewController *Controller=segue.destinationViewController;
        Controller.senderView=@"Itineary";
        Controller.attendeeName=itinearyName;
        //Controller.attendeeID=appdelegate.attendyid;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    int count;
    if(searchBar == _SearchBar)
    {
        if(count > searchText.length)
        {
         ItinearyData= [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            
            if(self.ItenaryTypeSegmentControl.selectedSegmentIndex == 0)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"ATTENDEE"]];
            }
            else if (self.ItenaryTypeSegmentControl.selectedSegmentIndex == 1)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"TEAM"]];
            }
            else if (self.ItenaryTypeSegmentControl.selectedSegmentIndex == 2)
            {
                 ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"SPEAKER"]];
            }
            
            
        }
        if(searchText.length == 0)
        {
            ItinearyData= [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            
            if(self.ItenaryTypeSegmentControl.selectedSegmentIndex == 0)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"ATTENDEE"]];
            }
            else if (self.ItenaryTypeSegmentControl.selectedSegmentIndex == 1)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"TEAM"]];
            }
            else if (self.ItenaryTypeSegmentControl.selectedSegmentIndex == 2)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"SPEAKER"]];
            }
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            ItinearyData= [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            
            if(self.ItenaryTypeSegmentControl.selectedSegmentIndex == 0)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"ATTENDEE"]];
            }
            else if (self.ItenaryTypeSegmentControl.selectedSegmentIndex == 1)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"TEAM"]];
            }
            else if (self.ItenaryTypeSegmentControl.selectedSegmentIndex == 2)
            {
                ItinearyData = [ItinearyData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"SPEAKER"]];
            }
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            
            
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.fullname contains[c] %@) OR (SELF.flightDetails contains[c] %@) OR (SELF.visaType contains[c] %@) OR (SELF.hotelRoomNo contains[c] %@) OR (SELF.hotelClass contains[c] %@) OR (SELF.flightClass contains[c] %@) OR (SELF.hotelDetails contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText,searchText,searchText];
           
            filtereventarr = [NSMutableArray arrayWithArray:[ItinearyData filteredArrayUsingPredicate:predicate]];
            ItinearyData = [filtereventarr copy];
        }
        ln_expandedRowIndex=-1;
        [_ItinearyTableView reloadData];
        
    }
}
- (IBAction)AccommodationSortButton:(id)sender
{
    accommodation=!accommodation;
    if(accommodation)
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"accommodation" type:@"abc" Ascending:YES];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"descending.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
    }
    else
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"accommodation" type:@"abc" Ascending:NO];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        
    }

}
- (IBAction)ArrivalDateSortButton:(id)sender
{
    ArrivalDate=!ArrivalDate;
    if(ArrivalDate)
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"arrivalDate" type:@"abc" Ascending:YES];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"descending.png"]];
    }
    else
    {
        ItinearyData = [helper sortData:ItinearyData colname:@"arrivalDate" type:@"abc" Ascending:NO];
        
        [_ItinearyTableView reloadData];
        
        [_FullNameSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_TravelSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_AccommadationSortIcon setImage:[UIImage imageNamed:@"nosort.png"]];
        [_ArrivalDateSortIcon setImage:[UIImage imageNamed:@"ascending.png"]];
        
    }
    
}
- (IBAction)ItinearyAdvanceSearchBtn:(id)sender {
    [self.parentViewController performSegueWithIdentifier:@"showItinearyAdvanceSearch" sender:self];
}
- (IBAction)clearSearchBtn:(id)sender {
    
    appdelegate.itinearySearch =@"NO";
    appdelegate.ItiniearySearch=nil;
    [_clearSearch setHidden:YES];
     if(appdelegate.selectedItineraraySegment == 0)
     {
         ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"ATTENDEE"]];
     }
    else if (appdelegate.selectedItineraraySegment == 1)
    {
        ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"TEAM"]];
    }
    else
    {
     ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"SPEAKER"]];
    }
    if([ItinearyData count]>0)
    {
        ItinearyFields = [helper query_alldata:@"ItinearyFields"];
    }
    //_ItenaryTypeSegmentControl.selectedSegmentIndex=0;
    
    [self.ItinearyTableView reloadData];
    
}

-(void)getEventItinearyData
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_628"]];
    //appdelegate.PerformDeltaSync=@"YES";
    [helper deletewitheventid:@"Event_Itenary" value:appdelegate.eventid];
    
    [helper deletewitheventid:@"ItinearyAttachment" value:appdelegate.eventid];
    
    [helper ProcessItinearyDetails:appdelegate.eventid LICType:@"ATTENDEE"];
    [helper ProcessItinearyDetails:appdelegate.eventid LICType:@"TEAM"];
    [helper ProcessItinearyDetails:appdelegate.eventid LICType:@"SPEAKER"];
    
    [self refreshData];
    ln_expandedRowIndex=-1;
    [self.ItinearyTableView reloadData];
    [helper removeWaitCursor];
}


- (IBAction)SyncData:(id)sender {
  //  appdelegate.selectedItineraraySegment=0;
    [self getEventItinearyData];
}


-(void)refreshData
{
    if(appdelegate.selectedItineraraySegment == 0)
    {
        ItinearyDataSorted = [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"ATTENDEE"]];
        
        if([appdelegate.itinearySearch isEqualToString:@"YES"])
        {
            ItinearyData = [ItinearyData filteredArrayUsingPredicate:appdelegate.ItiniearySearch];
            [_clearSearch setHidden:NO];
        }
        else
            [_clearSearch setHidden:YES];
    }
    else if (appdelegate.selectedItineraraySegment == 1)
    {
        ItinearyDataSorted = [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"TEAM"]];
        
        if([appdelegate.itinearySearch isEqualToString:@"YES"])
        {
            ItinearyData = [ItinearyData filteredArrayUsingPredicate:appdelegate.ItiniearySearch];
            [_clearSearch setHidden:NO];
        }
        else
            [_clearSearch setHidden:YES];
    }
    else if (appdelegate.selectedItineraraySegment == 2)
    {
        ItinearyDataSorted = [[helper query_alldata:@"Event_Itenary"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        ItinearyData = [ItinearyDataSorted filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"partyTypeLIC like %@",@"SPEAKER"]];
        
        if([appdelegate.itinearySearch isEqualToString:@"YES"])
        {
            ItinearyData = [ItinearyData filteredArrayUsingPredicate:appdelegate.ItiniearySearch];
            [_clearSearch setHidden:NO];
        }
        else
            [_clearSearch setHidden:YES];
    }
    
    
    
    
    EventDetails=[[helper query_alldata:@"Event"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if([EventDetails count]>0)
    {
        _eventName_value_lbl.text=[EventDetails[0] valueForKey:@"name"];
        _eventStartDate_value_lbl.text = [helper formatingdate:[EventDetails[0] valueForKey:@"startdate"] datetime:@"FormatDate"];
        _eventEndDate_value_lbl.text = [helper formatingdate:[EventDetails[0] valueForKey:@"enddate"] datetime:@"FormatDate"];
    }
}

@end
