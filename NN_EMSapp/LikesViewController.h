//
//  LikesViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 28/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
@interface LikesViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl *LikesCommentOutlet;
@property (strong, nonatomic) IBOutlet UITableView *LikesTableView;
@property(strong,nonatomic)HelperClass *Helper;
@property(strong,nonatomic)NSString *GalleryImageId;
- (IBAction)LikesCommentsValueChangedAction:(id)sender;
- (IBAction)CloseButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *CommentsTableView;


@end
