//
//  LikesViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 28/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "LikesViewController.h"

@interface LikesViewController ()
{
    NSString *pagecount;
    NSString *pageno;
    NSString *lstpage;
    NSString *Commentlstpage;
    NSArray *LikesArr;
    NSArray *CommentsArr;
    NSArray *JsonArr;
    NSArray *CommentJsonArr;
    int page;
    NSString *Commentpagecount;
    NSString *Commentpageno;
    int Commentpage;
    int likesCount;
    int CommentsCount;
    int CommentDownload;
    BOOL SyncLikes;
    BOOL SyncComments;
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSMutableArray *GalleryLikesIndexpathArr;
    NSMutableArray *GalleryCommentsIndexpathArr;

}

@end

@implementation LikesViewController
@synthesize LikesTableView,Helper,GalleryImageId,LikesCommentOutlet,CommentsTableView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Helper = [[HelperClass alloc]init];
    //LikesArr = [[NSMutableArray alloc]init];
    //CommentsArr = [[NSMutableArray alloc]init];
    lstpage = @"N";
    pagecount = @"1";
    pageno = @"1";
    page=1;
    Commentlstpage = @"N";
    Commentpagecount = @"1";
    Commentpageno = @"1";
    Commentpage=1;
    CommentDownload = 1;
    appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    GalleryLikesIndexpathArr = [[NSMutableArray alloc]init];
    GalleryCommentsIndexpathArr = [[NSMutableArray alloc]init];
    [Helper delete_alldata:@"EventGalleryLikes"];
    [Helper delete_alldata:@"EventGalleryComments"];
   
   
}
-(void)viewDidAppear:(BOOL)animated
{
    [Helper showWaitCursor:[Helper gettranslation:@"LBL_377"]];
    [self ProcessGalleryLikes];
    [self RefreshLikes];
    [Helper removeWaitCursor];
}
-(void)ProcessGalleryLikes
{
   
    if ([lstpage isEqual: @"N"])
    {
        SyncLikes = NO;
        NSString *AESPrivateKey = [Helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSMutableDictionary *LikesPayload = [[NSMutableDictionary alloc]init];
        [LikesPayload setValue:GalleryImageId forKey:@"AttachmentID"];
        [LikesPayload setValue:@"GLRLIKE" forKey:@"Entity"];
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:LikesPayload options:0 error:&error];
        NSString *jsonString;
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
    
        NSString *key = [Helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [Helper encryptString:jsonString withKey:key];
        NSString *EncryptrdPayloadString = [encryptedstring base64EncodedStringWithOptions:0];
    
        NSDictionary *Attachment_msgbody = [Helper create_trans_msgbody:EncryptrdPayloadString txType:@"EMSGetEventDetails" pageno:[NSString stringWithFormat:@"%@",pageno] pagecount:pagecount lastpage:lstpage];
    
        NSMutableDictionary *TRANXAPI_response = [Helper invokewebservice:Attachment_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
        if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
           [Helper showalert:@"Error" message:[TRANXAPI_response valueForKey:@"response_msg"] action:[Helper gettranslation:@"LBL_462"]];
        }
        else
        {
        
            NSString *authresponse = [Helper decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            page = [[TRANXAPI_response valueForKey:@"Page"] intValue];
            pageno = [NSString stringWithFormat:@"%d",page+1];
            pagecount = [TRANXAPI_response valueForKey:@"PageCount"];
            lstpage = [Helper booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
            NSLog(@"Last page in likes %@",lstpage);
            JsonArr = [jsonData valueForKey:@"EventGlrLike"];
            
            for(NSDictionary *item in JsonArr)
            {
                NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"EventGalleryLikes" inManagedObjectContext:context];
                NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                
                [newrecord setValue:[item valueForKey:@"AttachmentID"] forKey:@"attachmentID"];
                [newrecord setValue:[item valueForKey:@"AttendeeName"] forKey:@"attendeeName"];
                [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageIcon"];
                [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeID"];
                [newrecord setValue:[item valueForKey:@"EventAttendeeID"] forKey:@"eventAttendeeID"];
                [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
                [newrecord setValue:[Helper booltostring:[item valueForKey:@"Like"]] forKey:@"like"];
                [newrecord setValue:[item valueForKey:@"LikedOn"] forKey:@"likedOn"];
                NSError *error;
                [context save:&error];
            }

            
            
        }
        SyncLikes = YES;
    }
    

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark view touch method
- (IBAction)LikesCommentsValueChangedAction:(id)sender
{
    switch (LikesCommentOutlet.selectedSegmentIndex)
    {
        case 0:
            CommentsTableView.hidden = YES;
            LikesTableView.hidden = NO;
            [LikesTableView reloadData];
            break;
        case 1:
            if (CommentDownload == 1)
            {
                [self DownloadDataFirstTimeSegmentControllerTapped];
            }
            LikesTableView.hidden = YES;
            CommentsTableView.hidden = NO;
            [CommentsTableView reloadData];
            break;
        default:
            break;
    }

}
-(void)DownloadDataFirstTimeSegmentControllerTapped
{
    [Helper showWaitCursor:[Helper gettranslation:@"LBL_377"]];
    [self ProcessComments];
    [self RefreshComment];
    [Helper removeWaitCursor];
    CommentDownload = 0;
}
-(void)ProcessComments
{
    
    if ([Commentlstpage isEqual: @"N"])
    {
        SyncComments = NO;
        NSString *AESPrivateKey = [Helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSMutableDictionary *CommentsPayload = [[NSMutableDictionary alloc]init];
        [CommentsPayload setValue:GalleryImageId forKey:@"AttachmentID"];
        [CommentsPayload setValue:@"GLRCMT" forKey:@"Entity"];
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:CommentsPayload options:0 error:&error];
        NSString *jsonString;
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        NSString *key = [Helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [Helper encryptString:jsonString withKey:key];
        NSString *EncryptrdPayloadString = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *Comment_msgbody = [Helper create_trans_msgbody:EncryptrdPayloadString txType:@"EMSGetEventDetails" pageno:Commentpageno pagecount:Commentpagecount lastpage:Commentlstpage];
        
        NSMutableDictionary *TRANXAPI_response = [Helper invokewebservice:Comment_msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[TRANXAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [Helper showalert:@"Error" message:[TRANXAPI_response valueForKey:@"response_msg"] action:[Helper gettranslation:@"LBL_462"]];
        }
        else
        {
            NSString *authresponse = [Helper decryptData:[[TRANXAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            CommentJsonArr  = [jsonData valueForKey:@"EventGlrComment"];
            Commentpage = [[TRANXAPI_response valueForKey:@"Page"] intValue];
            Commentpageno = [NSString stringWithFormat:@"%d",Commentpage+1];
            Commentpagecount = [TRANXAPI_response valueForKey:@"PageCount"];
            Commentlstpage = [Helper booltostring:[[TRANXAPI_response valueForKey:@"LastPage"] boolValue]];
             NSLog(@"Last page in comment %@",Commentlstpage);
            for(NSDictionary *item in CommentJsonArr)
            {
                NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"EventGalleryComments" inManagedObjectContext:context];
                NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
                
                [newrecord setValue:[item valueForKey:@"AttachmentID"] forKey:@"attachmentID"];
                [newrecord setValue:[item valueForKey:@"AttendeeName"] forKey:@"attendeeName"];
                [newrecord setValue:[item valueForKey:@"ImageIcon"] forKey:@"imageIcon"];
                [newrecord setValue:[item valueForKey:@"Comment"] forKey:@"comment"];
                [newrecord setValue:[item valueForKey:@"CommentOn"] forKey:@"commentOn"];
                [newrecord setValue:[item valueForKey:@"AttendeeID"] forKey:@"attendeeID"];
                [newrecord setValue:[item valueForKey:@"EventAttendeeID"] forKey:@"eventAttendeeID"];
                [newrecord setValue:[item valueForKey:@"ID"] forKey:@"id"];
                
                NSError *error;
                [context save:&error];
            }

        }
        SyncComments = YES;
    }
}
- (IBAction)CloseButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromLikesToGallery" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}
#pragma mark tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == LikesTableView)
    {
        return [LikesArr count];
    }
    else if (tableView == CommentsTableView)
    {
        return [CommentsArr count];
    }
    else
        return 0;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    if (tableView == LikesTableView)
    {
     
            likesCount=likesCount+1;
            if(![GalleryLikesIndexpathArr containsObject:[NSString stringWithFormat:@"%lu",indexPath.row]])
            {
                [GalleryLikesIndexpathArr addObject:[NSString stringWithFormat:@"%lu",indexPath.row]];
                if(likesCount%10 == 0)
                {
                    if (SyncLikes)
                    {
                    
                dispatch_queue_t queue=dispatch_queue_create(0,0);
            
                dispatch_async(queue,^{
                
                    [self ProcessGalleryLikes];
                
                });
                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
//                               ^{
//                                   [self ProcessGalleryLikes];
//                               });
                
                
                
                
            }
        }
            }

      //  UIImageView *ImageView = (UIImageView *)[contentView viewWithTag:1];
       // ImageView.image = [UIImage imageNamed:@"User.png"];
        UILabel *Namelbl = (UILabel *)[contentView viewWithTag:2];
        Namelbl.text = [LikesArr[indexPath.row] valueForKey:@"attendeeName"];
        
//        NSString *img=[LikesArr [indexPath.row] valueForKey:@"ImageIcon"];
//        
//        NSData *imgdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:img]];
//        UIImage *Img = [UIImage imageWithData:imgdata];
//        ImageView.image = Img;
//        
//        ImageView.layer.cornerRadius = ImageView.frame.size.width / 2;
//        ImageView.layer.masksToBounds = YES;
        
       /* dispatch_queue_t queue =  dispatch_queue_create(0, 0);
        dispatch_async(queue,
                       ^{
                           NSString *img=[LikesArr [indexPath.row] valueForKey:@"ImageIcon"];
                           
                           NSData *imgdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:img]];
                           UIImage *Img = [UIImage imageWithData:imgdata];
                          
                           if(Img)
                           {
                               dispatch_async(dispatch_get_main_queue(),
                                              ^{
                                                  
                                                  UITableViewCell *updatecell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                                  
                                                  if(updatecell)
                                                  {
                                                    ImageView.image = Img;
                                                  }
                                            });
                           }
                       });
        ImageView.layer.cornerRadius = ImageView.frame.size.width / 2;
        ImageView.layer.masksToBounds = YES;*/
        
        UIWebView *WebView = (UIWebView *)[contentView viewWithTag:4];
        
        // NSString *strTemplateHTML = [NSString stringWithFormat:@"<html><head><style>img{max-width:100%%;height:auto !important;width:auto !important;};</style></head><body style='margin:0; padding:0;'>%@</body></html>", [LikesArr [indexPath.row] valueForKey:@"imageIcon"]];
        ////
        ////            [speakerwebimage loadHTMLString:strTemplateHTML baseURL:nil];
      
       // NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[LikesArr [indexPath.row] valueForKey:@"imageIcon"]]];
       
      // NSString* newStr = [[NSString alloc] initWithData:imgData encoding:NSUTF8StringEncoding];

       // NSString *htmlString = @"<html><body><img src='%@' width='200'></body></html>";
       // NSString *imageHTML  = [[NSString alloc] initWithFormat:htmlString, newStr];
        
        
        
        
        //[WebView loadHTMLString:imageHTML baseURL:nil];
//
        // [WebView loadHTMLString:@"" baseURL:[NSURL URLWithString:[LikesArr [indexPath.row] valueForKey:@"imageIcon"]]];
        [WebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[LikesArr [indexPath.row] valueForKey:@"imageIcon"]]]];
        
        WebView.layer.cornerRadius = WebView.frame.size.width / 2;
        WebView.layer.masksToBounds = YES;


        
    }
    if (tableView == CommentsTableView)
    {
        
            CommentsCount = CommentsCount+1;
            if(![GalleryCommentsIndexpathArr containsObject:[NSString stringWithFormat:@"%lu",indexPath.row]])
            {
                [GalleryCommentsIndexpathArr addObject:[NSString stringWithFormat:@"%lu",indexPath.row]];
            
            if(CommentsCount%10 == 0)
            {
                if (SyncComments)
                {
                
//                dispatch_queue_t queue=dispatch_queue_create(0,0);
//            
//                dispatch_async(queue,^{
//                    [self ProcessComments];
//                
//            });
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                               ^{
                                   @autoreleasepool {
                                       
                                   [self ProcessComments];
                                   }
                                   });
                

        }
            }
        }

       // UIImageView *ImageView = (UIImageView *)[contentView viewWithTag:1];
        //ImageView.image = [UIImage imageNamed:@"User.png"];
        UILabel *Namelbl = (UILabel *)[contentView viewWithTag:2];
        Namelbl.text = [CommentsArr[indexPath.row] valueForKey:@"attendeeName"];
        
        UILabel *CommentTimelbl = (UILabel *)[contentView viewWithTag:3];
        CommentTimelbl.text = [Helper formatingdate:[CommentsArr[indexPath.row] valueForKey:@"commentOn"] datetime:@"FormatDate"];
        
        UITextView *CommentDescriptionTextView = (UITextView *)[contentView viewWithTag:4];
        CommentDescriptionTextView.text = [CommentsArr[indexPath.row] valueForKey:@"comment"];
        //[CommentDescriptionTextView setTextContainerInset:UIEdgeInsetsZero];

        
       /* dispatch_queue_t queue = dispatch_queue_create(0, 0);
        dispatch_async(queue,
                       ^{
                           NSString *img=[CommentsArr [indexPath.row] valueForKey:@"ImageIcon"];
                           
                           NSData *imgdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:img]];
                           UIImage *Img = [UIImage imageWithData:imgdata];
                           
                           if(Img)
                           {
                               dispatch_async(dispatch_get_main_queue(),
                                              ^{
                                                  
                                                  UITableViewCell *updatecell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                                  
                                                  if(updatecell)
                                                  {
                                                     ImageView.image = Img;
                                                  }
                                                });
                           }
                       });
        ImageView.layer.cornerRadius = ImageView.frame.size.width / 2;
        ImageView.layer.masksToBounds = YES;*/
        
        UIWebView *WebView = (UIWebView *)[contentView viewWithTag:6];
//        NSString *htmlString = @"<html><body><img src='%@' width='200'></body></html>";
//        NSString *imageHTML  = [[NSString alloc] initWithFormat:htmlString, [CommentsArr [indexPath.row] valueForKey:@"imageIcon"]];
//        
//        
//        
//        
//        [WebView loadHTMLString:imageHTML baseURL:nil];
        
       // [WebView loadHTMLString:@"" baseURL:[NSURL URLWithString:[CommentsArr [indexPath.row] valueForKey:@"imageIcon"]]];
        
        [WebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[CommentsArr [indexPath.row] valueForKey:@"imageIcon"]]]];
        
        
        //[WebView setScalesPageToFit:YES];
       WebView.layer.cornerRadius = WebView.frame.size.width / 2;
        WebView.layer.masksToBounds = YES;

        
    }
    customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
    return customcell;
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    float endscrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if(endscrolling >= scrollView.contentSize.height)
    {
        if(LikesCommentOutlet.selectedSegmentIndex == 0)
        {
           [self performSelector:@selector(RefreshLikes) withObject:nil afterDelay:0];
        }
        if (LikesCommentOutlet.selectedSegmentIndex == 1)
        {
            [self performSelector:@selector(RefreshComment) withObject:nil afterDelay:0];
        }
        
    }
    
}
-(void)RefreshComment
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                      CommentsArr = [Helper query_alldata:@"EventGalleryComments"];
                       //NSLog(@"Commnet arr count %ld",[CommentsArr count]);
                    [CommentsTableView reloadData];
                     
                   });
}
-(void)RefreshLikes
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                      LikesArr = [Helper query_alldata:@"EventGalleryLikes"];
                       NSLog(@"Likes Arr Count %ld",[LikesArr count]);
                    [LikesTableView reloadData];
                      
                   });
}
@end
