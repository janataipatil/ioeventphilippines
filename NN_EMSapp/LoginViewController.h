//
//  LoginViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/7/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "HelperClass.h"
#import <QuartzCore/QuartzCore.h> 

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (strong,nonatomic) NSString *customerid;
@property (strong,nonatomic) NSString *applicationid;
@property (strong,nonatomic) NSString *enviroment;
@property (strong,nonatomic) NSString *azureappurl;
@property (strong,nonatomic) NSString *azureappkey;

@property (nonatomic,strong) HelperClass *helper;

@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *SignInButtonOutlet;

- (IBAction)login_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *versionLBL;

@end
