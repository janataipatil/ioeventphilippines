//
//  LoginViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/7/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
{
    
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    SLKeyChainStore *keychain;
    BOOL MSCALLOK,AESPUBKEYOK,AESPRIKEYOK;
    MSClient *client;
    
    NSString  *AESPrivateKey;
    NSString *AESPublicKey;
    NSString *requesturl;
    NSString *userpassword;
    dispatch_semaphore_t sem_pass;
}

@synthesize customerid,applicationid,enviroment,azureappurl,azureappkey,helper,SignInButtonOutlet;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];

    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    appdelegate.fdrstr = @"N";
    
   
  
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
   
    appdelegate.backupTranslations = [helper query_alldata:@"BackupLabel"];
    if(appdelegate.backupTranslations.count==0){
             [helper InsertBackupLabels];
    }
   // requesturl = @"https://nneventdev.azurewebsites.net/api/SL_MobileService"; ///App id DEV server
   // requesturl = @"https://nneventqa.azurewebsites.net/api/SL_MobileService"; ///App id QA server//
    //requesturl=@"https://ioeventqa.azurewebsites.net/api/SL_MobileService";
    
   //  requesturl = @"https://nneventstg.azurewebsites.net/api/SL_MobileService"; ///App id Stage server
    
     //requesturl = @"https://ioeventuat.azurewebsites.net/api/SL_MobileService"; ///App id UAT server
    
//    requesturl = [NSString stringWithFormat:@"%@%@",[helper geturl:@"QA"],@"/api/SL_MobileService"];
    
   //  requesturl = [NSString stringWithFormat:@"%@%@",[helper geturl:@"PREPROD"],@"/api/SL_MobileService"];
    
  //  requesturl = [NSString stringWithFormat:@"%@%@",[helper geturl:@"DEV"],@"/api/SL_MobileService"];

    requesturl = [NSString stringWithFormat:@"%@%@",[helper geturl:@"PROD"],@"/api/SL_MobileService"];
    
    // requesturl = @"https://ioeventqa.azurewebsites.net/api/SL_MobileService"; ///App id QA server
    

    applicationid = @"EMS_M_100";
    enviroment =  [self getenvironmentname:bundleIdentifier];
    
   //calling textfield delegate method
    _username.delegate = self;
    _password.delegate = self;
    
    [SignInButtonOutlet setTitle:[helper getLogintranslation:@"LBL_723"] forState:UIControlStateNormal];
    _username.placeholder = [helper getLogintranslation:@"LBL_272"];
    _password.placeholder = [helper getLogintranslation:@"LBL_724"];
    
    
    _username.layer.borderWidth=0.5f;
    _password.layer.borderWidth=0.5f;
    SignInButtonOutlet.layer.borderWidth=0.5f;
    
    _username.layer.cornerRadius=5.0f;
    _password.layer.cornerRadius=5.0f;
    
  _username.layer.borderColor=[[UIColor colorWithRed:73.0f/255.0f green:73.0f/255.0f blue:73.0f/255.0f alpha:1.0] CGColor];
    _password.layer.borderColor=[[UIColor colorWithRed:73.0f/255.0f green:73.0f/255.0f blue:73.0f/255.0f alpha:1.0] CGColor];
    SignInButtonOutlet.layer.borderColor=[[UIColor colorWithRed:73.0f/255.0f green:73.0f/255.0f blue:73.0f/255.0f alpha:1.0] CGColor];
    
    
    
    _username.layer.masksToBounds=YES;
    _password.layer.masksToBounds=YES;
    
    SignInButtonOutlet.layer.masksToBounds=YES;
    
   // [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"SignalR_reconnect"];
   // [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *flag = [helper query_data:@"name" inputdata:@"GoToDashboard" entityname:@"S_Config_Table"];
    NSLog(@"go to dashboard flag %@",flag);
    
    
    if ([[helper query_data:@"name" inputdata:@"GoToDashboard" entityname:@"S_Config_Table"] isEqualToString:@"YES"])
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"N" forKey:@"Pushregister"];
        [[NSUserDefaults standardUserDefaults]synchronize];

        [self performSegueWithIdentifier:@"LogintoDashboard" sender:self];
    }

    if ([appdelegate.navpurpose isEqualToString:@"LoginFail"])
    {
       [helper showalert:[helper getLogintranslation:@"LBL_419"] message:[helper getLogintranslation:@"LBL_471"] action:[helper getLogintranslation:@"LBL_462"]];
    }
    
    _versionLBL.text=[NSString stringWithFormat:@"v%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"AppVersion"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login_btn:(id)sender
{
    NSString *networkstatus = [helper checkinternetconnection];    
    customerid = @"7777";
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];

    if (_username.text.length >0 && _password.text.length>0)
    {
        [helper showWaitCursor:[helper getLogintranslation:@"LBL_520"]];

        if ([_username.text isEqualToString:[keychain stringForKey:@"username"]]&& [_password.text isEqualToString:[keychain stringForKey:@"password"]])
        {

            NSArray *array = [helper query_alldata:@"User"];
            if(array.count>0)
            {
               // It is necessarry to check database contain ,because when we delete application all data will be lost so necessary to do FDR once again for the same reason
                [helper removeWaitCursor];
                
                [[NSUserDefaults standardUserDefaults]setObject:@"N" forKey:@"Pushregister"];
                NSString *logFileDate = [helper query_data:@"" inputdata:@"logDate" entityname:@"S_Config_Table"];
                NSDateFormatter *logFormatter = [[NSDateFormatter alloc]init];
                [logFormatter setDateFormat:@"yyyyMMddHHmmss"];
                NSDate *logDate = [logFormatter dateFromString:logFileDate];
                
                NSString *strCurrentDate = [logFormatter stringFromDate:[NSDate date]];
                NSDate *currentDate = [logFormatter dateFromString:strCurrentDate];
                
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                           fromDate:logDate
                                                             toDate:currentDate
                                                            options:0];
                if(components.day>15)
                    [self CreateLogFile];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
                [helper update_data:@"GoToDashboard" inputdata:@"YES" entityname:@"S_Config_Table"];
                [self performSegueWithIdentifier:@"LogintoDashboard" sender:self];

            }
            else
            {
                NSString *networkstatus = [helper checkinternetconnection];

                if ([networkstatus isEqualToString:@"Y"])
                {
                    [helper delete_alldata:@"S_Config_Table"];
                    [helper insert_data:@"applicationid" valuedata:applicationid entityname:@"S_Config_Table"];
                    [helper insert_data:@"customerid" valuedata:customerid entityname:@"S_Config_Table"];
                    [self invoke_SL_MobileService:@"DEV" customerid:customerid applicationid:applicationid];
                    //Create Log File on Login
                    [self CreateLogFile];
                   
                }
                else
                {
                    [helper removeWaitCursor];
                    NSString *MessageStr = [NSString stringWithFormat:@"%@ \n%@",[helper gettranslation:@"LBL_725"],[helper gettranslation:@"LBL_726"]];
                    [helper showalert:[helper getLogintranslation:@"LBL_419"] message:[NSString stringWithFormat:@"%@ \n%@",[helper getLogintranslation:@"LBL_725"],[helper getLogintranslation:@"LBL_726"]] action:[helper getLogintranslation:@"LBL_462"]];
                }
            }
        }
        else
        {

            NSString *networkstatus = [helper checkinternetconnection];
//
            if ([networkstatus isEqualToString:@"Y"])
            {
                //do fdr
                [helper delete_alldata:@"S_Config_Table"];
                [helper insert_data:@"applicationid" valuedata:applicationid entityname:@"S_Config_Table"];
                [helper insert_data:@"customerid" valuedata:customerid entityname:@"S_Config_Table"];
                [self invoke_SL_MobileService:@"DEV" customerid:customerid applicationid:applicationid];

                
                //Create Log File on FDR
                [self CreateLogFile];
            }
            else
            {
                [helper removeWaitCursor];
                [helper showalert:[helper getLogintranslation:@"LBL_419"] message:[NSString stringWithFormat:@"%@ \n%@",[helper getLogintranslation:@"LBL_725"],[helper getLogintranslation:@"LBL_726"]] action:[helper getLogintranslation:@"LBL_462"]];
            }
        }

    }
    else
    {
        [helper showalert:[helper getLogintranslation:@"LBL_419"] message:[helper getLogintranslation:@"LBL_727"] action:[helper getLogintranslation:@"LBL_462"]];
    }
}

-(NSString *)getenvironmentname:(NSString *)title
{
    NSRange lastDotRange = [title rangeOfString:@"." options:NSBackwardsSearch];
    if (lastDotRange.location != NSNotFound)
    {
        NSString *text = [title substringFromIndex:lastDotRange.location+1];
        return text;
    }
    else
    {
        return @"INVALID";
    }
}

-(NSString *)invoke_SL_MobileService: (NSString *)env customerid:(NSString *)custid applicationid:(NSString *)appid
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:custid forKey:@"customerID"];
    [dict setValue:appid forKey:@"appID"];
    [dict setValue:env forKey:@"env"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    NSData *postData = [jsonStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requesturl]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
    
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn)
    {
        NSLog(@"Connection Successful");
    } else
    {
         NSLog(@"Connection could not be made");
    }
    

    return jsonStr;
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    NSError *error = nil;
//    NSString *jsonresult = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (jsonArray.count>=2)
    {
        NSString *resulturi = [helper insert_data:@"azureappurl" valuedata:[jsonArray valueForKey:@"uri"] entityname:@"S_Config_Table"];
        NSString *resultkey = [helper insert_data:@"azureappkey" valuedata:[jsonArray valueForKey:@"appKey"] entityname:@"S_Config_Table"];
        
        if ([resulturi  isEqual: @"Success"]&&[resultkey  isEqual: @"Success"])
        {
           MSCALLOK = YES;
           
            azureappurl = [helper query_data:@"name" inputdata:@"azureappurl" entityname:@"S_Config_Table"];
            azureappkey =[helper query_data:@"name" inputdata:@"azureappkey" entityname:@"S_Config_Table"];
            
        }else
        {
            MSCALLOK = NO;
        }
    }
    else
    {
        MSCALLOK = NO;
    }
    
    
    
}

// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

    [helper removeWaitCursor];
    NSString *errormsg = [NSString stringWithFormat:@"%@",error];
    [helper showalert:[helper getLogintranslation:@"LBL_590"] message:errormsg action:[helper getLogintranslation:@"LBL_462"]];
    
}

// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (MSCALLOK)
    {
        // NSLog(@"connectionDidFinishLoading OK ");
      //  NSURL *url = [NSURL URLWithString:azureappurl];
//        client = [MSClient clientWithApplicationURL:url applicationKey:azureappkey];
        client = [MSClient clientWithApplicationURLString:azureappurl];
        
        // Call Webservice to get AES Public Key
        
        NSDictionary *msgbodypub = [self create_aeskey_msgbody:@"AES_PUBLIC_KEY"];
        NSDictionary *msgbodypri = [self create_aeskey_msgbody:@"AES_PRIVATE_KEY"];
        
        
        
        NSMutableDictionary *AESPUBKEY_response = [helper invokewebservice:msgbodypub invokeAPI:@"SL_AESKey" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        
        if (![[AESPUBKEY_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            //  NSLog(@"Error occured while inoking webservice:AES_PUBLIC_KEY \nError details are: \n%@",[AESPUBKEY_response valueForKey:@"response_msg"]);
        }
        else
        {
            AESPublicKey = [[AESPUBKEY_response valueForKey:@"response_msg"]valueForKey:@"key"];
            
            
            NSMutableDictionary *AESPRIKEY_response = [helper invokewebservice:msgbodypri invokeAPI:@"SL_AESKey" HTTPMethod:@"POST" parameters:nil headers:nil];
            
            if (![[AESPRIKEY_response valueForKey:@"response_code"]  isEqual: @"000"])
            {
                //  NSLog(@"Error occured while inoking webservice:AES_PRIVATE_KEY \nError details are: \n%@",[AESPRIKEY_response valueForKey:@"response_msg"]);
            }
            else
            {
                
                // NSLog(@"printkey: %@",[AESPRIKEY_response valueForKey:@"key"]);
                AESPrivateKey =  [helper decryptData:[[AESPRIKEY_response valueForKey:@"response_msg"]valueForKey:@"key"] withKey:AESPublicKey];
                
                NSString *pubkeyres = [helper insert_data:@"aespublickey" valuedata:AESPublicKey entityname:@"S_Config_Table"];
                NSString *prikeyres = [helper insert_data:@"aesprivatekey" valuedata:AESPrivateKey entityname:@"S_Config_Table"];
                
                
                //insrt push register tag
                [[NSUserDefaults standardUserDefaults]setObject:@"N" forKey:@"Pushregister"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                
                if ([pubkeyres  isEqual: @"Success"]&&[prikeyres  isEqual: @"Success"])
                {
                    [helper removeWaitCursor];
                    [self Logintosystem];
                }
                else
                {
                    NSLog(@"Error occurred while inserting AES Keys to database");
                }
            }
        }
    }
    else
    {
        //  NSLog(@" NOT OK :connectionDidFinishLoading");
    }
    
}


-(NSDictionary *) create_aeskey_msgbody:(NSString *)keyType
{
    NSString *TxType = @"SLAES";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *gmtZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    [dateFormatter setTimeZone:gmtZone];
    [dateFormatter setDateFormat:@"yyyyMMddhhmmssSSS"];
    
    NSMutableDictionary *messagebody = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *msgbodypart1= [[NSMutableDictionary alloc] init];
    
    [msgbodypart1 setObject:customerid forKey:@"customerID"];
    [msgbodypart1 setObject:applicationid forKey:@"appID"];
    [msgbodypart1 setObject:[[NSString stringWithFormat:@"AES#%@#%@#%@",applicationid,TxType,[dateFormatter stringFromDate:[NSDate date]]]uppercaseString] forKey:@"messageId"];
    [msgbodypart1 setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timestamp"];
    [msgbodypart1 setObject:TxType forKey:@"TxType"];
    
    NSMutableDictionary *msgbodypart2= [[NSMutableDictionary alloc] init];
    [msgbodypart2 setObject:keyType forKey:@"keyType"];
    
    [messagebody setObject:msgbodypart1 forKey:@"reqHeader"];
    [messagebody setObject:msgbodypart2 forKey:@"reqBody"];
    
    // NSLog(@"\nmessagebody: %@\n",messagebody);
    
    return messagebody;
}



-(void)Logintosystem
{
  
    [helper showWaitCursor:[helper getLogintranslation:@"LBL_331"]];
    
    sem_pass = dispatch_semaphore_create(0);
    NSString *username = _username.text;
    NSString *password = _password.text;
    NSString *pushtag = [keychain stringForKey:@"app_uniquetag"];
    NSString *appID = [helper query_data:@"name" inputdata:@"applicationid" entityname:@"S_Config_Table"];
    NSString *custID = [helper query_data:@"name" inputdata:@"customerid" entityname:@"S_Config_Table"];
    
    
    NSString *payload = [helper create_auth_payload:password pushtag:pushtag];
    NSDictionary *msgbody = [helper create_auth_msgbody:payload username:username custID:custID appID:appID];
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Auth" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        [helper showalert:[helper getLogintranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[helper getLogintranslation:@"LBL_462"]];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivateKey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];

       // appdelegate.navpurpose = @"LoginFail";
        keychain[@"SLToken"] = jsonData[@"SLToken"];
        keychain[@"LoginID"] = jsonData[@"LoginID"];
        keychain[@"username"] = username;
        keychain[@"password"] = password;
      
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        keychain[@"SyncDate"] = [dateFormatter stringFromDate:[NSDate date]];
        
        [helper update_data:@"EMSUserRoleActions" inputdata:jsonData[@"EMSUserRoleActions"] entityname:@"S_Config_Table"];
        
        [helper update_data:@"StorageContainer" inputdata:jsonData[@"StorageContainer"] entityname:@"S_Config_Table"];
        [helper update_data:@"StorageAccount" inputdata:jsonData[@"StorageAccount"] entityname:@"S_Config_Table"];
        [helper update_data:@"StorageKey" inputdata:jsonData[@"StorageKey"] entityname:@"S_Config_Table"];
        [helper update_data:@"StorageURI" inputdata:jsonData[@"StorageURI"] entityname:@"S_Config_Table"];
        
        NSDateFormatter *logDateFormatter = [[NSDateFormatter alloc]init];
        [logDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        [helper update_data:@"logDate" inputdata:[dateFormatter stringFromDate:[NSDate date]] entityname:@"S_Config_Table"];
        [helper update_data:@"CurrencyCodeLIC" inputdata:jsonData[@"CurrencyCodeLIC"] entityname:@"S_Config_Table"];
        if([jsonData[@"SupportMail"] length]>0)
             [helper update_data:@"SupportMail" inputdata:jsonData[@"SupportMail"] entityname:@"S_Config_Table"];
        else
             [helper update_data:@"SupportMail" inputdata:@"info@iwizardsolutions.com" entityname:@"S_Config_Table"];
        
        [helper update_data:@"EMSAffiliateID" inputdata:jsonData[@"EMSAffiliateID"] entityname:@"S_Config_Table"];
        [helper update_data:@"TimeZoneLIC" inputdata:jsonData[@"TimeZoneLIC"] entityname:@"S_Config_Table"];
        
        [helper update_data:@"DefaultEventView" inputdata:@"ListView" entityname:@"S_Config_Table"];
        [helper update_data:@"DefaultCalendarView" inputdata:@"DayPlannerView" entityname:@"S_Config_Table"];
        [helper update_data:@"GalleryFDRdownload" inputdata:@"YES" entityname:@"S_Config_Table"];
        appdelegate.PerformDeltaSync = @"NO";
        [helper delete_alldata:@"Push_Notification_Table"];
        
        [helper downloadDocument:jsonData[@"HelpDocURL"]];
        [helper removeWaitCursor];
        
        NSString *passreset = jsonData[@"ResetPassword"];
        NSString *tncstatus = jsonData[@"TnCMStatus"];
        NSString *tncconcenton = jsonData[@"TnCMConsentOn"];
        NSString *tncconcenturl = jsonData[@"TnCMURL"];
        
       // NSString *ItinearyMetaData=jsonData[@"ItineraryFields"];
        
       // [helper ProcessItinearyMetaData:ItinearyMetaData];
        
        [helper update_data:@"TnC_Status" inputdata:[helper booltostring:[tncstatus boolValue]] entityname:@"S_Config_Table"];
        [helper update_data:@"TnC_ConsentOn" inputdata:[helper booltostring:[tncconcenton boolValue]] entityname:@"S_Config_Table"];
        [helper update_data:@"TnC_URL" inputdata:tncconcenturl entityname:@"S_Config_Table"];
        
        [helper update_data:@"ResetPassword" inputdata:[helper booltostring:[passreset boolValue]] entityname:@"S_Config_Table"];
    
        [[NSUserDefaults standardUserDefaults]setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] forKey:@"version"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [helper removeWaitCursor];
       
        NSLog(@"value is %@",[helper booltostring:[tncstatus boolValue]]);
        if ([[helper booltostring:[tncstatus boolValue]] isEqualToString:@"Y"])
        {
            
            if ([[helper booltostring:[passreset boolValue]] isEqualToString:@"Y"])
            {
                
                sem_pass = dispatch_semaphore_create(0);
                
               // UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Password Reset" message:@"Please Enter New Password" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[helper getLogintranslation:@"LBL_469"] message:[helper getLogintranslation:@"LBL_490"] preferredStyle:UIAlertControllerStyleAlert];
                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
                 {
                    // textField.placeholder = NSLocalizedString(@"New Password", @"New Password");
                     textField.placeholder = NSLocalizedString([helper getLogintranslation:@"LBL_427"],[helper getLogintranslation:@"LBL_427"]);
                     textField.secureTextEntry = YES;
                     [textField addTarget:self action:@selector(alertFieldBeginEditing:) forControlEvents:UIControlEventEditingDidEnd];
                     [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                 }];
                
                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
                 {
                    // textField.placeholder = NSLocalizedString(@"Confirm Password", @"Confirm Password");
                     textField.placeholder = NSLocalizedString([helper getLogintranslation:@"LBL_345"],[helper getLogintranslation:@"LBL_345"]);
                     textField.secureTextEntry = YES;
                     [textField addTarget:self action:@selector(alertFieldBeginEditing:) forControlEvents:UIControlEventEditingDidEnd];
                     [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                     
                     textField.layer.borderColor=[[UIColor redColor]CGColor];
                     textField.layer.borderWidth= 0.0f;
                 }];
                
                //UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Save", @"OK action") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                           UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString([helper getLogintranslation:@"LBL_219"], [helper getLogintranslation:@"LBL_462"]) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                           {
                                               
                                               dispatch_semaphore_signal(sem_pass);
                                               
                                           }];
                okAction.enabled = NO;
                [alertController addAction:okAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
                while (dispatch_semaphore_wait(sem_pass, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
                [self ResetPassword];
            }
            else
            {
               [helper showWaitCursor:[helper getLogintranslation:@"LBL_628"]];
                // [helper showWaitCursor:@"Downloading Data...."];
                [helper get_EMS_data];
                [helper removeWaitCursor];
                NSLog(@"data download flag is %@",appdelegate.DataDownloaded);
                
                if([appdelegate.DataDownloaded isEqualToString:@"YES"])
                {
                    [helper update_data:@"GoToDashboard" inputdata:@"YES" entityname:@"S_Config_Table"];
                    [self performSegueWithIdentifier:@"LogintoDashboard" sender:self];
                }
                else if ([appdelegate.PerformDeltaSync isEqualToString:@"NO"] && [appdelegate.DataDownloaded isEqualToString:@"NO"])
                {
                   // [ToastView showToastInParentView:self.view withText:appdelegate.ProcessStatus withDuaration:2.0];
                    
                     [helper showalert:[helper getLogintranslation:@"LBL_590"] message:appdelegate.ProcessStatus action:[helper getLogintranslation:@"LBL_462"]];
                }
//                else
//                {
//                    [helper showalert:[helper gettranslation:@"LBL_590"] message:appdelegate.ProcessStatus action:[helper gettranslation:@"LBL_462"]];
                //}
            }
            
            
            
        }
        else
        {
            [self performSegueWithIdentifier:@"LogintoTermnconditions" sender:self];
        }

    }
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UIAlertAction *okAction = alertController.actions.lastObject;
        UITextField *password = alertController.textFields.firstObject;
        UITextField *confirmpassword = alertController.textFields.lastObject;
        
        if (password.text.length >0 &&  confirmpassword.text.length >0)
        {
            
            if ([password.text isEqualToString: confirmpassword.text])
            {
                okAction.enabled = YES;
               // alertController.message = @"Click save to Reset Password.";
                alertController.message = [helper getLogintranslation:@"LBL_611"];
                userpassword = password.text;
                
            }
            else
            {
                alertController.message = @"";
                okAction.enabled = NO;
            }
        }
        else
        {
            okAction.enabled = NO;
        }
    }
}

-(void)alertFieldBeginEditing:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        
        UIAlertAction *okAction = alertController.actions.lastObject;
        UITextField *password = alertController.textFields.firstObject;
        UITextField *confirmpassword = alertController.textFields.lastObject;
        
        
        if(![helper ValidPassword:password.text])
        {
            UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
            NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@ 8.",[helper getLogintranslation:@"LBL_472"],[helper getLogintranslation:@"LBL_424"]]];
            [attributedStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,5)];
            lbl.attributedText = attributedStr;
            lbl.textAlignment = NSTextAlignmentCenter;
          //  [alertController setValue:lbl forKey:@"Message"];
            
            alertController.message=[NSString stringWithFormat:@"%@\n%@ 8.",[helper getLogintranslation:@"LBL_472"],[helper getLogintranslation:@"LBL_424"]];
           // alertController.view.tintColor=[UIColor redColor];
            password.text= @"";;
            confirmpassword.text=@"";
        }
        else
        {
           alertController.message=[helper getLogintranslation:@"LBL_472"];
        }
        
    }
}

-(void) ResetPassword
{
    
    
   // [helper showWaitCursor:@"Resetting Password"];
    [helper showWaitCursor:[helper getLogintranslation:@"LBL_615"]];
    NSError *error;
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:[keychain stringForKey:@"username"] forKey:@"UserName"];
    [JsonDict setValue:userpassword forKey:@"Password"];
    
//    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
//    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetUser" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        //[alert show];
        
        
        [helper showalert:[helper getLogintranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[helper getLogintranslation:@"LBL_462"]];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
       // [keychain removeItemForKey:@"username"];
       // [keychain removeItemForKey:@"password"];
        
        [helper update_data:@"GoToDashboard" inputdata:@"NO" entityname:@"S_Config_Table"];
        
        _username.text = @"";
        _password.text = @"";
    }
    
    [helper removeWaitCursor];
    
}

-(IBAction)UnwindToLogin:(UIStoryboardSegue *)unwndsegue
{
    [SignInButtonOutlet setTitle:[helper getLogintranslation:@"LBL_723"] forState:UIControlStateNormal];
    _username.placeholder = [helper getLogintranslation:@"LBL_272"];
    _password.placeholder = [helper getLogintranslation:@"LBL_724"];

    
    _username.text = @"";
    _password.text = @"";
}

#pragma mark textfeild delegate method
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
    
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
    
    return YES;
}
#pragma mark segue  method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LogintoDashboard"])
    {
       /* NSArray *userarr = [helper query_alldata:@"User"];
        if ([userarr count]== 0)
        {
            [self invoke_SL_MobileService:@"DEV" customerid:customerid applicationid:applicationid];
        
        }*/
    }
}

-(void)CreateLogFile
{
    NSArray *logPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [logPath objectAtIndex:0];
    NSString *fileName =[NSString stringWithFormat:@"%@.log",@"IOEvents"];
    appdelegate.logFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    freopen([appdelegate.logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

@end
