//
//  MailingListViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 28/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"

@interface MailingListViewController : UIViewController
- (IBAction)AddAttendeeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UISearchBar *AttendeeSearchBar;
@property (strong, nonatomic) IBOutlet UIButton *SalutationHeader;
@property (strong, nonatomic) IBOutlet UILabel *salutationLBL;
@property (strong, nonatomic) IBOutlet UILabel *firstNameLBL;
@property (strong, nonatomic) IBOutlet UILabel *lastNameLBL;
@property (strong, nonatomic) IBOutlet UILabel *statusLBL;
@property (strong, nonatomic) IBOutlet UILabel *specialityLBL;
@property (strong, nonatomic) IBOutlet UILabel *TargetClassLBL;
@property (strong, nonatomic) IBOutlet UITableView *AttendeeTableView;
@property (strong, nonatomic) IBOutlet UIButton *SendTestEmail;
- (IBAction)SendTestEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *SendEmailBtn;
- (IBAction)SendEmail:(id)sender;

@end
