//
//  MailingListViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 28/10/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "MailingListViewController.h"

@interface MailingListViewController ()
{
    NSArray *MailingList;
    NSArray *AttendeeList;
    NSArray *MailingAttendeeList;
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSString *test_email;
    SLKeyChainStore *keychain;
    int count;
}
@end

@implementation MailingListViewController

- (void)viewDidLoad {
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    
    [self RefreshData];
    
    _SendTestEmail.layer.borderColor = [[UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:101.0/255.0 alpha:1.0]CGColor];
    _SendTestEmail.layer.borderWidth = 1.0;
    _SendTestEmail.layer.cornerRadius= 5.0;
    [_SendTestEmail setTitle:[helper gettranslation:@"LBL_756"] forState:UIControlStateNormal];
    _SendEmailBtn.layer.borderColor = [[UIColor colorWithRed:0.0/255.0 green:25.0/255.0 blue:101.0/255.0 alpha:1.0]CGColor];
        _SendEmailBtn.layer.borderWidth=1.0;
    _SendEmailBtn.layer.cornerRadius=5.0;
    [_SendEmailBtn setTitle:[helper gettranslation:@"LBL_759"] forState:UIControlStateNormal];
    
    [_AttendeeSearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchfield = [_AttendeeSearchBar valueForKey:@"_searchField"];
    searchfield.placeholder = [helper gettranslation:@"LBL_224"];
    searchfield.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:242.0/255.0f green:246.0/255.0f blue:250.0f/255.0f alpha:1.0f];
    
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
   
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self RefreshData];
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
    NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];
    NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
    [eventattendee_dict setValue:[MailingList[indexPath.row] valueForKey:@"attendeeid"]?: @"" forKey:@"AttendeeID"];
    [eventattendee_dict setValue:[MailingList[indexPath.row] valueForKey:@"eventattendeeid"]?: @"" forKey:@"EventAttendeeID"];
    [eventattendee_dict setValue:[MailingList[indexPath.row] valueForKey:@"eventid"] forKey:@"EventID"];
    [eventattendee_dict setValue:[MailingList[indexPath.row] valueForKey:@"id"] forKey:@"ID"];
    [eventattendee_dict setValue:[MailingList[indexPath.row] valueForKey:@"eventmailid"] forKey:@"EventMailID"];
    [eventattendee_dict setValue:[helper stringtobool:@"Y"] forKey:@"Deleted"];
    
    
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar options:0 error:&error];
    if (jsonData)
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventMailingList" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSMutableArray *JsonArray = [[NSMutableArray alloc] init];
        NSPredicate *ExpenseIDPredicate = [NSPredicate predicateWithFormat:@"id like %@",[MailingList[indexPath.row] valueForKey:@"id"]];
        [helper delete_predicate:@"Event_MailingList" predicate:ExpenseIDPredicate];
        [helper removeWaitCursor];
        [self RefreshData];
    }
    
    
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [MailingAttendeeList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    customcell = [_AttendeeTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    

    
    UIView *contentView = customcell.contentView;
    UILabel *salutation = (UILabel *) [contentView viewWithTag:20];
    UILabel *firstname = (UILabel *) [contentView viewWithTag:30];
    UILabel *lastname = (UILabel *) [contentView viewWithTag:4];
    UILabel *status = (UILabel *) [contentView viewWithTag:5];
    UILabel *ownedby = (UILabel *) [contentView viewWithTag:6];
    UILabel *speciality = (UILabel *) [contentView viewWithTag:7];
    
    
    salutation.text = [MailingAttendeeList[indexPath.row] valueForKey:@"salutation"];
    firstname.text = [MailingAttendeeList[indexPath.row] valueForKey:@"firstname"];
    lastname.text = [MailingAttendeeList[indexPath.row] valueForKey:@"lastname"];
    status.text = [MailingAttendeeList[indexPath.row] valueForKey:@"status"];
    ownedby.text = [MailingAttendeeList[indexPath.row] valueForKey:@"targetclass"];
    speciality.text = [MailingAttendeeList[indexPath.row] valueForKey:@"speciality"];
    
    return customcell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)AddAttendeeBtn:(id)sender
{
    appdelegate.AttendeeAddType=@"MailingList";
    [self.parentViewController performSegueWithIdentifier:@"AddAttendeestoMailingList" sender:self];
}
- (IBAction)SendTestEmail:(id)sender
{
   
    NSString *templateid = appdelegate.templatename;
   
    NSPredicate *UserNamepredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:UserNamepredicate];
    if ([userarr count]>0)
    {
        test_email = [userarr[0] valueForKey:@"emailaddress"];
    }
    
    
    NSArray *EventAttendee = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if ([EventAttendee count]>0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_759"] message:[helper gettranslation:@"LBL_760"] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.text = test_email;
             textField.placeholder = [helper gettranslation:@"LBL_756"];
             textField.secureTextEntry = NO;
             [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
         }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_759"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       
                                       [helper sendemail:templateid emailaddr:test_email attendeeid:@""];
                                   }];
        okAction.enabled = YES;
        [alertController addAction:okAction];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil];
        cancelAction.enabled = YES;
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_762"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okaction];
        [self presentViewController:alert animated:NO completion:nil];
        
    }
}
- (IBAction)SendEmail:(id)sender
{
    if(MailingAttendeeList.count>0)
    {
        [helper showWaitCursor:[helper gettranslation:@"LBL_808"]];
    
        NSMutableArray *PayloadArr = [[NSMutableArray alloc]init];
        NSArray *TemplateDetailArr=[[helper query_alldata:@"Event_Mail"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"iD like %@",appdelegate.templatename]];
            NSMutableDictionary *ServerDict = [[NSMutableDictionary alloc]init];
            if ([TemplateDetailArr count]>0)
            {
                //NSString *generatedRowId = [helper generate_rowId];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"iD"] forKey:@"ID"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateID"] forKey:@"TemplateID"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateName"] forKey:@"TemplateName"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"comment"] forKey:@"Comment"];
                //[ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateDesc"] forKey:@"Description"];
                [ServerDict setValue:appdelegate.eventid forKey:@"EventID"];
                [ServerDict setValue:[helper stringtobool:@"Y"] forKey:@"SendMail"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"templateType"] forKey:@"TemplateType"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"category"] forKey:@"Category"];
                [ServerDict setValue:[helper getLic:@"TEMP_CAT" value:[TemplateDetailArr[0]     valueForKey:@"category"]]   forKey:@"CategoryLIC"];
                [ServerDict setValue:[TemplateDetailArr[0] valueForKey:@"subCategory"] forKey:@"SubCategory"];
            
            }
            [PayloadArr addObject:ServerDict];
    
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArr options:0 error:&error];
        if (jsonData)
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AesPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventMail" pageno:@"" pagecount:@"" lastpage:@""];
    
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
     
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            [helper removeWaitCursor];
            [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_761"] withDuaration:1.0];
         
        }
    
    }
}

-(void)RefreshData
{
    MailingList = [[helper query_alldata:@"Event_MailingList"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND (eventmailid like %@)",appdelegate.eventid,appdelegate.templatename]];
    
    AttendeeList = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    NSMutableArray *NewAttendeeList = [[NSMutableArray alloc]init];
    for (int i=0; i<MailingList.count; i++)
    {
        NSArray *tempAttendee = [AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[MailingList[i] valueForKey:@"attendeeid"]]];
        
        if([tempAttendee count]>0)
            [NewAttendeeList addObject:tempAttendee[0]];
    }
    
    
    MailingAttendeeList =[NewAttendeeList mutableCopy];
    [self.AttendeeTableView reloadData];
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UIAlertAction *okAction = alertController.actions.firstObject;
        UITextField *emailaddr = alertController.textFields.firstObject;
        
        if (emailaddr.text.length >0)
        {
            okAction.enabled = YES;
            test_email = emailaddr.text;
            
        }
        else
        {
            okAction.enabled = NO;
        }
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(count > searchText.length)
    {
        MailingList = [[helper query_alldata:@"Event_MailingList"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND (eventmailid like %@)",appdelegate.eventid,appdelegate.templatename]];
        
        AttendeeList = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
        NSMutableArray *NewAttendeeList = [[NSMutableArray alloc]init];
        for (int i=0; i<MailingList.count; i++)
        {
            NSArray *tempAttendee = [AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[MailingList[i] valueForKey:@"attendeeid"]]];
            
            if([tempAttendee count]>0)
                [NewAttendeeList addObject:tempAttendee[0]];
        }
        
        
        MailingAttendeeList =[NewAttendeeList mutableCopy];
    }
    if(searchText.length == 0)
    {
        MailingList = [[helper query_alldata:@"Event_MailingList"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND (eventmailid like %@)",appdelegate.eventid,appdelegate.templatename]];
        
        AttendeeList = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
        NSMutableArray *NewAttendeeList = [[NSMutableArray alloc]init];
        for (int i=0; i<MailingList.count; i++)
        {
            NSArray *tempAttendee = [AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[MailingList[i] valueForKey:@"attendeeid"]]];
            
            if([tempAttendee count]>0)
                [NewAttendeeList addObject:tempAttendee[0]];
        }
        
        
        MailingAttendeeList =[NewAttendeeList mutableCopy];
        [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
    }
    else
    {
        MailingList = [[helper query_alldata:@"Event_MailingList"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND (eventmailid like %@)",appdelegate.eventid,appdelegate.templatename]];
        
        AttendeeList = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
        NSMutableArray *NewAttendeeList = [[NSMutableArray alloc]init];
        for (int i=0; i<MailingList.count; i++)
        {
            NSArray *tempAttendee = [AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[MailingList[i] valueForKey:@"attendeeid"]]];
            
            if([tempAttendee count]>0)
                [NewAttendeeList addObject:tempAttendee[0]];
        }
        
        
        MailingAttendeeList =[NewAttendeeList mutableCopy];
        count = searchText.length;
        NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.speciality contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.targetclass contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
        filtereventarr = [NSMutableArray arrayWithArray:[MailingAttendeeList filteredArrayUsingPredicate:predicate]];
        MailingAttendeeList = [filtereventarr copy];
    }
    [self.AttendeeTableView reloadData];
}


@end
