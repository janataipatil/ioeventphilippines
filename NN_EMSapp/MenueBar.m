//
//  MenueBar.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "MenueBar.h"

@implementation MenueBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"MenueBar" owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        [self addSubview:mainView];
        self.HomeButton = (UIButton *)[mainView viewWithTag:1];
        self.CalendarButton = (UIButton *)[mainView viewWithTag:2];
        self.ActivitiesButton = (UIButton *)[mainView viewWithTag:3];
        self.SpeakerButton = (UIButton *)[mainView viewWithTag:4];
        self.AttendeeButton = (UIButton *)[mainView viewWithTag:5];
        self.ActivityPlanButton = (UIButton *)[mainView viewWithTag:6];
        self.VersionNumber = (UILabel *)[mainView viewWithTag:7];
        self.ActivityPlanButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.ActivityPlanButton.titleLabel.numberOfLines = 2;
        
    }
    return self;
}
@end
