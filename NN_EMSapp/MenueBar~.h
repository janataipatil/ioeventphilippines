//
//  MenueBar.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenueBar : UIView
@property(strong,nonatomic)IBOutlet UIButton *HomeButton;
@property(strong,nonatomic)IBOutlet UIButton *CalendarButton;
@property(strong,nonatomic)IBOutlet UIButton *ActivitiesButton;
@property(strong,nonatomic)IBOutlet UIButton *SpeakerButton;
@property(strong,nonatomic)IBOutlet UIButton *AttendeeButton;
@property(strong,nonatomic)IBOutlet UIButton *ActivityPlanButton;


@end
