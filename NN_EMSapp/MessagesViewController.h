//
//  MessagesViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 31/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"

@interface MessagesViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *LOVTableView;
@property (strong, nonatomic) IBOutlet UITableView *MessagesTableView;

@property (strong, nonatomic) IBOutlet UITextField *TemplateNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *AttendeeNameTextField;
@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UIView *MessageListHeaderView;

@property (strong, nonatomic) IBOutlet UILabel *attendeename_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *templatename_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *Statuslbl;
@property (strong, nonatomic) IBOutlet UITextField *StatusTextField;

- (IBAction)SearchButtonAction:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *templatename_lbl;
@property (strong, nonatomic) IBOutlet UILabel *templatedesc_lbl;

- (IBAction)close_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *close_btn;

@property (strong, nonatomic) IBOutlet UILabel *attendee_tlbl;
@property (strong, nonatomic) IBOutlet UILabel *type_tlbl;
@property (strong, nonatomic) IBOutlet UILabel *timestamp_tlbl;
@property (strong, nonatomic) IBOutlet UILabel *status_tlbl;

@property(strong,nonatomic)NSString *seltemplate;

@property (strong, nonatomic) IBOutlet UISegmentedControl *MessageSegmentController;
- (IBAction)MessageSegmentController:(id)sender;


@end
