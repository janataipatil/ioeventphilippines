//
//  MessagesViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 31/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "MessagesViewController.h"
#import "MailingListViewController.h"

@interface MessagesViewController ()
{
    NSString *TextfieldFlag;
    NSArray *AttendeeArr;
    NSArray *TemplateArr;
    NSArray *StatusArr;
    AppDelegate *appdelegate;
    NSPredicate *eventidPredicate;
    NSString *TemplateID;
    NSString *AttendeeID;
    NSArray *MessagesArr;
    NSString *statusLIC;
    
    NSMutableArray *messages;
    int recordcount;
    NSString *lstpage;
    NSString *pagecount;
    NSString *pageno;
    int page;
    BOOL loadData;
    NSArray *temparray;
    NSPredicate *tempredicate;
    MailingListViewController *MailVC;
   
}

@end

@implementation MessagesViewController
@synthesize LOVTableView,MessagesTableView,TemplateNameTextField,AttendeeNameTextField,helper,MessageListHeaderView,StatusTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    LOVTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableView.layer.borderWidth = 1;
    MessageListHeaderView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    MessageListHeaderView.layer.borderWidth = 1;
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    [self.MessageSegmentController setTitle:[helper gettranslation:@"LBL_888"] forSegmentAtIndex:0];
    [self.MessageSegmentController setTitle:[helper gettranslation:@"LBL_887"] forSegmentAtIndex:1];
    
//    [TemplateNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [AttendeeNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [StatusTextField addTarget:self  action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    eventidPredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    
    pagecount = @"1";
    pageno = @"1";
    page=1;
    
    
    _seltemplate = appdelegate.templatename;
    
    tempredicate = [NSPredicate predicateWithFormat:@"iD like %@",_seltemplate];
    temparray = [[helper query_alldata:@"Event_Mail"] filteredArrayUsingPredicate:tempredicate];
    if(temparray.count>0)
    {
        _templatename_lbl.text = [temparray[0] valueForKey:@"templateName"];
        TemplateNameTextField.text = [temparray[0] valueForKey:@"templateName"];
    
        NSString *templateID = [temparray[0] valueForKey:@"templateID"];
        NSArray *maintemparray = [[helper query_alldata:@"Template"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"templateID like %@",templateID]];
    
        if(maintemparray.count>0)
        {
        _templatedesc_lbl.text = [maintemparray[0] valueForKey:@"templateDesc"];
        }
    }
        _attendee_tlbl.text = [helper gettranslation:@"LBL_054"];
        _type_tlbl.text = [helper gettranslation:@"LBL_266"];
        _timestamp_tlbl.text = [helper gettranslation:@"LBL_570"];
        _status_tlbl.text = [helper gettranslation:@"LBL_242"];
    
    
        messages=[[NSMutableArray alloc]init];
        _attendeename_ulbl.text = [helper gettranslation:@"LBL_054"];
        _Statuslbl.text = [helper gettranslation:@"LBL_242"];
    
    TemplateNameTextField.placeholder = [helper gettranslation:@"LBL_757"];
    AttendeeNameTextField.placeholder = [helper gettranslation:@"LBL_757"];
    
    StatusTextField.placeholder = [helper gettranslation:@"LBL_757"];    
    [_close_btn setTitle:[helper gettranslation:@"LBL_342"] forState:UIControlStateNormal];
    
    AttendeeNameTextField.text=@"ALL";
   // AttendeeID = @"ALL";
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == LOVTableView)
    {
        if ([TextfieldFlag isEqualToString:@"TemplateTextField"])
        {
           return [TemplateArr count];
        }
        else if ([TextfieldFlag isEqualToString:@"AttendeeTextField"])
        {
           return [AttendeeArr count];
        }
        else if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            return ([StatusArr count]);
        }
        else
            return 0;
    }
    else if (tableView == MessagesTableView)
    {
        return [MessagesArr count];
    }
    else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    if (tableView == LOVTableView)
    {
        UILabel *Namelbl = (UILabel *)[contentView viewWithTag:1];
        if ([TextfieldFlag isEqualToString:@"TemplateTextField"])
        {
            Namelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateName"];
        }
        else if ([TextfieldFlag isEqualToString:@"AttendeeTextField"])
        {
            Namelbl.text = [NSString stringWithFormat:@"%@ %@",[AttendeeArr[indexPath.row] valueForKey:@"firstname"],[AttendeeArr[indexPath.row] valueForKey:@"lastname"]];
        }
        else if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            Namelbl.text=[StatusArr[indexPath.row]valueForKey:@"value"];
        }
        
        
    //    TemplateNamelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateName"];
//    TemplateDescriptionlbl.text = [TemplateArr[indexPath.row] valueForKey:@"comment"];
//    LastSentlbl.text = [TemplateArr[indexPath.row] valueForKey:@"lastSentDate"];
//    Typelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateType"];
//
        
        
    }
    if (tableView == MessagesTableView)
    {
        UILabel *Attendeelbl = (UILabel *)[contentView viewWithTag:1];
        UILabel *Typelbl = (UILabel *)[contentView viewWithTag:2];
        UILabel *Timestamplbl = (UILabel *)[contentView viewWithTag:3];
        UILabel *Statuslbl = (UILabel *)[contentView viewWithTag:4];
        
        UIButton *retrybtn = (UIButton *)[contentView viewWithTag:15];
        
        Attendeelbl.text = [MessagesArr[indexPath.row] valueForKey:@"AttendeeName"];
        Typelbl.text =[MessagesArr[indexPath.row] valueForKey:@"CommType"];
        Timestamplbl.text =[helper formatingdate:[MessagesArr[indexPath.row] valueForKey:@"SentDate"] datetime:@"FormatDate"] ;
        Statuslbl.text = [MessagesArr[indexPath.row] valueForKey:@"Status"];
        
        [retrybtn addTarget:self action:@selector(senduseremail:) forControlEvents:UIControlEventTouchUpInside];
        
        if(indexPath.row % 2 == 0)
        {
            contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
        }
        else
        {
            contentView.backgroundColor = [UIColor clearColor];
        }
        recordcount = indexPath.row + 1;
        if(recordcount%15==0)
        {
            
            if(![messages containsObject:[NSString stringWithFormat:@"%lu",(long)indexPath.row]])
            {
                [messages addObject:[NSString stringWithFormat:@"%lu",(long)indexPath.row]];
                if(loadData)
                {
                    dispatch_queue_t queue=dispatch_queue_create(0,0);
                    dispatch_async(queue,^{
                        
                        [self getMessages];
                        
                    });
                }
            }
        }

    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (tableView == LOVTableView)
    {
//        if ([TextfieldFlag isEqualToString:@"TemplateTextField"])
//        {
//            TemplateNameTextField.text = [TemplateArr[indexPath.row] valueForKey:@"templateName"];
//            TemplateID = [TemplateArr[indexPath.row] valueForKey:@"iD"];
//            [TemplateNameTextField resignFirstResponder];
//        }
//        else
        if ([TextfieldFlag isEqualToString:@"AttendeeTextField"])
        {
            AttendeeNameTextField.text = [NSString stringWithFormat:@"%@ %@",[AttendeeArr[indexPath.row] valueForKey:@"firstname"],[AttendeeArr[indexPath.row] valueForKey:@"lastname"]];
            if ([AttendeeNameTextField.text isEqualToString:@"ALL"])
            {
                AttendeeID = @"ALL";
            }
            else
            {
                AttendeeID = [AttendeeArr[indexPath.row] valueForKey:@"attendeeid"];
            }
            [AttendeeNameTextField resignFirstResponder];
            //[self getMessages];
        }
        else if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            StatusTextField.text=[StatusArr[indexPath.row]valueForKey:@"value"];
            statusLIC=[StatusArr[indexPath.row]valueForKey:@"lic"];
        }
        
        [AttendeeNameTextField resignFirstResponder];
        [StatusTextField resignFirstResponder];
        LOVTableView.hidden = YES;
    }
   
    
}
-(void)getMessages
{
    
    loadData=NO;
    NSMutableDictionary *PayloadDict = [[NSMutableDictionary alloc]init];
    [PayloadDict setValue:appdelegate.eventid forKey:@"EventID"];
    [PayloadDict setValue:AttendeeID forKey:@"AttendeeID"];
    [PayloadDict setValue:appdelegate.templatename forKey:@"EventMailID"];
    [PayloadDict setValue:statusLIC forKey:@"StatusLIC"];
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSGetEventComm" pageno:[NSString stringWithFormat:@"%@",pageno] pagecount:pagecount lastpage:lstpage];
    
    //online only functionality
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_419"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        MessagesArr = [jsonDict valueForKey:@"EventComm"];
        
        page = [[AUTHWEBAPI_response valueForKey:@"Page"] intValue];
        pageno = [NSString stringWithFormat:@"%d",page+1];
        pagecount = [AUTHWEBAPI_response valueForKey:@"PageCount"];
        lstpage = [helper booltostring:[[AUTHWEBAPI_response valueForKey:@"LastPage"] boolValue]];
        

        if ([MessagesArr count]>0)
        {
            [MessagesTableView reloadData];
        }
        else
        {
            [MessagesTableView reloadData];
 
        }
    }
    
    loadData=YES;
  
}
-(void)refreshdata
{
    NSArray *EventAttendeeArr = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:eventidPredicate];
    if ([EventAttendeeArr count]>0)
    {
        NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
        [Dict setValue:@"ALL" forKey:@"firstname"];
        [Dict setValue:@" " forKey:@"lastname"];
        AttendeeArr = [NSArray arrayWithObjects:Dict,nil];
        AttendeeArr = [AttendeeArr arrayByAddingObjectsFromArray:EventAttendeeArr];
    }
}
#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (textField == TemplateNameTextField)
//    {
//        TextfieldFlag = @"TemplateTextField";
//        TemplateArr = [[helper query_alldata:@"Event_Mail"] filteredArrayUsingPredicate:eventidPredicate];
//    }
//    else
//        
    if (textField == AttendeeNameTextField)
    {
        TextfieldFlag = @"AttendeeTextField";
        [self RefreshData];
        
    }
    if(textField==StatusTextField)
    {
            TextfieldFlag=@"StatusTextField";
        StatusArr=[[helper query_alldata:@"Event_LOV"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COMM_STAT"]];
    }
    [LOVTableView reloadData];
    [self adjustHeightOfTableview:LOVTableView];
//    [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.parentViewController.view];
    LOVTableView.hidden = NO;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.parentViewController.view];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
//        if (theTextField == TemplateNameTextField)
//        {
//            TemplateArr = [[helper query_alldata:@"Event_Mail"] filteredArrayUsingPredicate:eventidPredicate];
//        }
//        else
//            
        if (theTextField == AttendeeNameTextField)
        {
           [self refreshdata];
        }
        else if (theTextField==StatusTextField)
        {
            
            StatusArr=[[helper query_alldata:@"Event_LOV"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COMM_STAT"]];
        }
    }
    else
    {

        
        if (theTextField == AttendeeNameTextField)
        {
            [self refreshdata];
            NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@)",searchText,searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[AttendeeArr filteredArrayUsingPredicate:filterpredicate]];
            AttendeeArr = [filteredpartArray copy];
        }
        else if (theTextField==StatusTextField)
        {
            
            StatusArr=[[helper query_alldata:@"Event_LOV"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"COMM_STAT"]];
            NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"SELF.value contains[c] %@",searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[StatusArr filteredArrayUsingPredicate:filterpredicate]];
            StatusArr = [filteredpartArray copy];
        }
        
    }
    [LOVTableView reloadData];
}

#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    CGFloat height = LOVTableView.contentSize.height;
    CGFloat maxHeight = LOVTableView.superview.frame.size.height - LOVTableView.frame.origin.y;
    if (height > maxHeight)
        height = maxHeight;
    [UIView animateWithDuration:0.0 animations:^{
        CGRect frame = LOVTableView.frame;
        
        
//        if([TextfieldFlag isEqualToString:@"TemplateTextField"])
//        {
//            frame.origin.x = 30;
//            frame.origin.y = 68;
//            frame.size.width=270;
//        }
        
        
        if([TextfieldFlag isEqualToString:@"AttendeeTextField"])
        {
            frame.origin.x = AttendeeNameTextField.frame.origin.x + 70;
            frame.origin.y = AttendeeNameTextField.frame.origin.y + 185;
            frame.size.width=270;
        }
        if([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            frame.origin.x = StatusTextField.frame.origin.x + 70;
            frame.origin.y = StatusTextField.frame.origin.y + 185;
            frame.size.width = StatusTextField.frame.size.width;
            
        }
        LOVTableView.frame = frame;
    }];
    
}
#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *Touch;
    Touch = [touches anyObject];
    if (Touch.view != LOVTableView)
    {
        if (LOVTableView.hidden == NO)
        {
            LOVTableView.hidden = YES;
//            [TemplateNameTextField resignFirstResponder];
            [AttendeeNameTextField resignFirstResponder];
            [StatusTextField resignFirstResponder];
        }
    }
}

-(void)setupMailingListViewController
{
    MailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MailingListVC"];
    [MailVC.view setFrame:CGRectMake(70,157,913,550)];
    [self addChildViewController:MailVC];
    [self.view addSubview:MailVC.view];
    [MailVC didMoveToParentViewController:self];

}

- (IBAction)SearchButtonAction:(id)sender
{
    if([AttendeeNameTextField.text length]>0 || [StatusTextField.text length]>0)
    {
        LOVTableView.hidden = YES;
        [StatusTextField resignFirstResponder];
        [helper showWaitCursor:@"Downloading Data"];
        [self getMessages];
        [helper removeWaitCursor];
    }
}
- (IBAction)close_btn:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromAddToEventTemplate" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

-(IBAction)senduseremail:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:MessagesTableView];
    NSIndexPath *indexPath = [MessagesTableView indexPathForRowAtPoint:buttonPosition];
//    if (indexPath != nil)
//    {
//        NSString *templatename = [TempListArr[0] valueForKey:@"iD"];
//    }
    
//    NSArray *TempListArr = [[helper query_alldata:@"Event_Mail"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"templateID like %@",appdelegate.templatename]];;
   
//    NSString *templatename = [TempListArr[0] valueForKey:@"iD"];
    
    NSString *attendeeid = [MessagesArr[indexPath.row] valueForKey:@"AttendeeID"];
    NSArray *attendee = [[helper query_alldata:@"Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.id contains[c] %@",attendeeid]];
    
    [helper sendemail:appdelegate.templatename emailaddr:[attendee[0] valueForKey:@"emailaddress"] attendeeid:attendeeid];
    
    
   
}

-(IBAction)unwindtoMessageViewController:(UIStoryboardSegue *)segue
{
    switch (_MessageSegmentController.selectedSegmentIndex) {
        case 0:
            [MailVC removeFromParentViewController];
            [MailVC.view removeFromSuperview];
            [self refreshdata];
            break;
        case 1:
            [MailVC removeFromParentViewController];
            [MailVC.view removeFromSuperview];
            [self setupMailingListViewController];
            break;
            
        default:
            break;
    }
}

- (IBAction)MessageSegmentController:(id)sender
{
    switch (_MessageSegmentController.selectedSegmentIndex) {
        case 0:
            [MailVC.view removeFromSuperview];
            [MailVC removeFromParentViewController];
            [self refreshdata];
            break;
        case 1:
            
            [self setupMailingListViewController];
            break;
            
        default:
            break;
    }
}

-(void)RefreshData
{
   NSArray *MailingList = [[helper query_alldata:@"Event_MailingList"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(eventid like %@) AND (eventmailid like %@)",appdelegate.eventid,appdelegate.templatename]];
    
  NSArray  *AttendeeList = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    NSMutableArray *NewAttendeeList = [[NSMutableArray alloc]init];
    for (int i=0; i<MailingList.count; i++)
    {
        NSArray *tempAttendee = [AttendeeList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"attendeeid like %@",[MailingList[i] valueForKey:@"attendeeid"]]];
        
        if([tempAttendee count]>0)
            [NewAttendeeList addObject:tempAttendee[0]];
    }
    NSArray *MailingAttendeeList =[NewAttendeeList mutableCopy];
    
    NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
    [Dict setValue:@"ALL" forKey:@"firstname"];
    [Dict setValue:@" " forKey:@"lastname"];
    AttendeeArr = [NSArray arrayWithObjects:Dict,nil];
    AttendeeArr = [AttendeeArr arrayByAddingObjectsFromArray:MailingAttendeeList];
    
   
   
}

@end
