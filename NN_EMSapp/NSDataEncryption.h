//
//  NSDataEncryption.h
//  AES_encryption_Dencryption
//
//  Created by iWizardsIV on 7/22/15.
//  Copyright (c) 2015 iWizards. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface NSDataEncryption : NSData
@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;


@end
