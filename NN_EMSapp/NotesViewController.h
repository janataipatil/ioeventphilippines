//
//  NotesViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 03/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"

@interface NotesViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *NotesTextView;
@property(strong,nonatomic)HelperClass *helper;

@property (strong,nonatomic)NSString *cmnt;
@property (strong,nonatomic)NSString *Type;

@property (strong, nonatomic) IBOutlet UIButton *DoneBtn;
- (IBAction)DoneBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *MainView;


@end
