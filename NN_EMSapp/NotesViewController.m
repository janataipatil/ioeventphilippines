//
//  NotesViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 03/02/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "NotesViewController.h"

@interface NotesViewController ()
{
    NSArray *ExpenseArr;
    AppDelegate *appDelegate;
}

@end

@implementation NotesViewController
@synthesize helper,NotesTextView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NotesTextView.layoutManager.delegate = self;
    
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.MainView.bounds];
    self.MainView.layer.masksToBounds = NO;
    self.MainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.MainView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.MainView.layer.shadowOpacity = 0.3f;
    self.MainView.layer.shadowPath = shadowPath.CGPath;
    
    
    
    
    appDelegate.translations = [helper query_alldata:@"Lang_translations"];
    if([_Type isEqualToString:@"ExpenseApprovals"])
    {
        self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
        NotesTextView.text =_cmnt;
        
    }
    else
    {
        ExpenseArr = [[helper query_alldata:@"Event_Expense"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appDelegate.ExpenseID]];
        if ([ExpenseArr count]>0)
        {
            NotesTextView.text = [ExpenseArr[0] valueForKey:@"purpose"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    return 6; // Line spacing of 19 is roughly equivalent to 5 here.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)CancelSegue
{
    
    if([_Type isEqualToString:@"ExpenseApprovals"])
        [self performSegueWithIdentifier:@"BackToAddExpense" sender:self];
    else
        [self performSegueWithIdentifier:@"unwindFromNotesToExpense" sender:self];

        
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != NotesTextView)
    {
        [self CancelSegue];
    }
}
- (IBAction)DoneBtn:(id)sender {
    if([_Type isEqualToString:@"ExpenseApprovals"])
        [self performSegueWithIdentifier:@"BackToAddExpense" sender:self];
    else
        [self performSegueWithIdentifier:@"unwindFromNotesToExpense" sender:self];
    
    
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}
@end
