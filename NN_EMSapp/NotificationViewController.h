//
//  NotificationViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/14/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "GIBadgeView.h"

@interface NotificationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    
}
@property(nonatomic,strong)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UITableView *notifytable_tv;
@property (strong, nonatomic) IBOutlet UIView *MainView;

@property(nonatomic,strong)GIBadgeView *notificationview;
@property (strong, nonatomic) IBOutlet UILabel *notificarion_ulbl;
- (IBAction)ClearButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ClearBtn;

@end
