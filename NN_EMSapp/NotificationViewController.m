//
//  NotificationViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/14/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "NotificationViewController.h"
#import "AppDelegate.h"
#import "SOCustomCell.h"


@interface NotificationViewController ()
{
    NSArray *notificationdata;
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSString *pushmessageid;
}

@end

@implementation NotificationViewController
@synthesize helper,notificationview,MainView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    helper = [[HelperClass alloc]init];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    
    /*add swipe recogniser*/
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    [self updatemessagereadFlaginLocalDB];
    
    notificationdata = [helper query_alldata:@"Push_Notification_Table"];
    
    if(notificationdata.count ==0)
    {
        [_ClearBtn setHidden:YES];
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"message_date" ascending:NO];
    notificationdata=[notificationdata sortedArrayUsingDescriptors:@[sort]];
    
    
    [self langsetuptranslations];
    //[self setbadgevalue:appdelegate.badgevalue];
    
}
-(void)langsetuptranslations
{
    
    [_notificarion_ulbl setText:[helper gettranslation:@"LBL_184"]];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self dismisspopup];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [notificationdata count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    customcell = [self.notifytable_tv dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
//     customcell.backgroundColor = [UIColor colorWithRed:58/255.f green:87/255.f blue:96/255.f alpha: 1];
    
    UILabel *Notification_msg = (UILabel *) [contentView viewWithTag:1];
    UILabel *Notification_tym = (UILabel *) [contentView viewWithTag:2];
    
    
    [Notification_msg setText:[notificationdata[indexPath.row] valueForKey:@"message_detail"]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *cDate = [dateFormatter dateFromString:[notificationdata[indexPath.row] valueForKey:@"message_date"]];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
    
    [Notification_tym setText:[dateFormatter stringFromDate:cDate]];
    customcell.backgroundColor = [UIColor clearColor];
    
    return customcell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    
}


- (IBAction)notificationbutton:(id)sender
{
    [self performSegueWithIdentifier:@"NotificationviewToHome" sender:self];
    
    
//    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
//        [self.view removeFromSuperview];
//        [self removeFromParentViewController];
//    }];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view setFrame:CGRectMake(1200, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    }
                     completion:^(BOOL finished){[self.view removeFromSuperview];
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];}];
    
}

-(void)updatemessagereadFlaginLocalDB
{

    context = [appdelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Push_Notification_Table" inManagedObjectContext:context];
    
    NSError *error;
    
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    if([fetchedObjects count]>0)
    {
        for (NSManagedObject *object in fetchedObjects)
        {
            [object setValue:@"Y" forKey:@"meassage_read"];
        }
        [context save:&error];
    }

    
    
    NSArray *pushnotificationdatacountarray = [helper query_alldata:@"Push_Notification_Table"];
    int notifycount = 0;
    for (int i =0; i<[pushnotificationdatacountarray count]; i++)
    {
        if([[pushnotificationdatacountarray[i] valueForKey:@"meassage_read"] isEqualToString:@"N"])
        {
            notifycount++;
        }
    }
    appdelegate.badgevalue = notifycount;
    
    
}


//-(void)setbadgevalue:(NSUInteger)badgevalue
//{
//    notificationview= [GIBadgeView new];
//    notificationview.backgroundColor = [UIColor colorWithRed:251/255.0 green:45/255.0 blue:62/255.0 alpha:1.0];
//    notificationview.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
//    notificationview.topOffset = 3.0f;
//    notificationview.rightOffset = 50.0f;
//    [notificationview setBadgeValue:appdelegate.badgevalue];
//    [_notificationbutton addSubview:self.notificationview];
//    
//    
//}
#pragma mark dismiss notification view method
-(void)dismisspopup
{
    [self performSegueWithIdentifier:@"NotificationviewToHome" sender:self];
    [UIView animateWithDuration:0.5 animations:^{
        [self.view setFrame:CGRectMake(1200, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    }
                     completion:^(BOOL finished){[self.view removeFromSuperview];
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];}];
}
#pragma mark view  touche method
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *TouchedView = [touches anyObject];
    if([TouchedView view] != MainView)
    {
        [self dismisspopup];
    }
}
- (IBAction)ClearButtonAction:(id)sender
{
    [helper delete_alldata:@"Push_Notification_Table"];
    [_notifytable_tv reloadData];
}
@end
