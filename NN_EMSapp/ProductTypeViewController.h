//
//  ProductTypeViewController.h
//  NN_EMSapp
//
//  Created by iWizards XI on 07/11/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"


@interface ProductTypeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titileLBL;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)saveBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *productNmlbl;
@property (strong, nonatomic) IBOutlet UILabel *contributionLBL;
@property (strong, nonatomic) IBOutlet UITableView *ProductTypeTable;
@property (strong, nonatomic) IBOutlet UILabel *totalContributionLBL;

@property (strong, nonatomic) NSString *senderView;

@end
