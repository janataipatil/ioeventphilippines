//
//  ProductTypeViewController.m
//  NN_EMSapp
//
//  Created by iWizards XI on 07/11/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ProductTypeViewController.h"

@interface ProductTypeViewController ()
{
    AppDelegate *appdelegate;
    HelperClass *helper;
    NSArray *ProductTypes;
    NSArray *ProductLOV;
    float totalValue;
    float remaingValue;
    
    NSMutableArray *SelectedProductTypes;
}

@end

@implementation ProductTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    
    ProductLOV = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRODUCT"]];
    
    
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ProductLOV count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.ProductTypeTable dequeueReusableCellWithIdentifier:@"ProductType"];
    
    
    UILabel *ProductName = [cell.contentView viewWithTag:1];
    
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
}



-(void)RemovePopup
{
    [self performSegueWithIdentifier:@"BackToUpsertEvent" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)saveBtn:(id)sender {
    [self RemovePopup];
}
- (IBAction)cancelBtn:(id)sender {
}
@end
