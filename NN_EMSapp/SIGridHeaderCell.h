//
//  SIGridHeaderCell.h
//  SPM
//
//  Created by iWizardsVI on 17/02/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import <ShinobiGrids/ShinobiGrids.h>
#import <ShinobiGrids/SDataGridHeaderCell.h>
#import <ShinobiGrids/SDataGridColumnSortOrder.h>
@interface SIGridHeaderCell : SDataGridHeaderCell

@property(nonatomic,assign)SDataGridColumnSortOrder sortOrder;


@end
