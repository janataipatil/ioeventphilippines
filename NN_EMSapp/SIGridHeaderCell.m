//
//  SIGridHeaderCell.m
//  SPM
//
//  Created by iWizardsVI on 17/02/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import "SIGridHeaderCell.h"

@implementation SIGridHeaderCell
{
    UILabel *_multiLineLabel;
  UIImageView *sortImage;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}
-(void)showArrowForSortOrder:(SDataGridColumnSortOrder)sortOrder sortMode:(SDataGridColumnSortMode)mode
{
    // Remove old arrow.
   [sortImage removeFromSuperview];
    
    // Load in custom arrow image.
    UIImage *arrow = [UIImage imageNamed:@"spm_ipad_calculator_circle2.png"];
    sortImage = [[UIImageView alloc] initWithImage:arrow];
    
//    // Position and add arrow.
    [self addSubview:sortImage];
    sortImage.frame = CGRectMake(0, 12, 20, 20);
}

- (void)drawRect:(CGRect)rect
{
 
       for (UIView* view in self.subviews) {
        
        if([NSStringFromClass ([view class]) isEqualToString:@"SGridSortOrderArrow"])
            sortImage = (UIImageView *)view;
           
       
    }
    
    if(self.sortOrder==SDataGridColumnSortOrderNone)
    {
     
     
         sortImage.image = [UIImage imageNamed:@"nosort.png"];
    }
    else if(self.sortOrder==SDataGridColumnSortOrderAscending)
    {
        //set arrow down icon
        sortImage.image = [UIImage imageNamed:@"ascending.png"];
    }
    else if(self.sortOrder==SDataGridColumnSortOrderDescending)
    {
        //set arrow up icon
        sortImage.image = [UIImage imageNamed:@"descending.png"];
    }
    
   
   
}
- (id)initWithReuseIdentifier:(NSString *)identifier{
    self = [super initWithReuseIdentifier:identifier];
    if (self) {
        
        // Remove the default UITextField from our cell.
        [self.textField removeFromSuperview];
     
        _multiLineLabel = [[UILabel alloc] init];
        
        [_multiLineLabel setNumberOfLines:0];
        [_multiLineLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:_multiLineLabel];
        
        // Observe changes in default text field's 'text' property so we can update the multi-line label.
        [self.textField addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    // Update our multi-line label with the column header text that was just set on the default text field.
    _multiLineLabel.text = ((UITextField *)object).text;

    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    // Update our label's frame whenever the cell is resized or moved.
  
    CGRect frames = [self bounds];
    _multiLineLabel.frame = CGRectMake(25, 0, frames.size.width, frames.size.height);
    
    
    
    // Update the label's styling as we know by this point the defualt label will have had its styling applied.
    _multiLineLabel.backgroundColor = self.textField.backgroundColor;
    _multiLineLabel.font = self.textField.font;
    _multiLineLabel.textColor = self.textField.textColor;
}
- (void)dealloc
{
    [self.textField removeObserver:self forKeyPath:@"text"];
}
@end
