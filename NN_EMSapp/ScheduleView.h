//
//  ScheduleView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/31/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"


@interface ScheduleView : UIViewController<UITableViewDelegate,UITextFieldDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UILabel *eventname_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventstdate_lbl;
@property (strong, nonatomic) IBOutlet UILabel *eventenddate_lbl;

@property (strong, nonatomic) IBOutlet UILabel *Titledate;
@property (strong, nonatomic) IBOutlet UIButton *predate_btn;
- (IBAction)predate_btn:(id)sender;

- (IBAction)addnewsession_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *addnewsession_btn;

@property (strong, nonatomic) IBOutlet UIButton *nextdate_btn;
- (IBAction)nextdate_btn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *group_lbl;
@property (weak, nonatomic) IBOutlet UITextField *group_tv;
@property (weak, nonatomic) IBOutlet UITableView *groupTableView;


@property (strong, nonatomic) IBOutlet UITableView *schedulelistview;
@property(strong,nonatomic) HelperClass *helper;
@property (strong, nonatomic) IBOutlet UISearchBar *schedule_searchbar;

@property (strong, nonatomic) IBOutlet UILabel *eventname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventsdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventedate_ulbl;


@end
