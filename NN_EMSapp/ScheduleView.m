//
//  ScheduleView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/31/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "ScheduleView.h"

@interface ScheduleView ()
{
    NSArray *sessionlist;
    NSInteger count;
    AppDelegate *appdelegate;
    NSPredicate *eventidpredicate;
    
    NSString *sessionID;
    NSManagedObjectContext *context;
    NSArray *eventData;
    NSDate *currentDate;
    NSMutableArray *predicatedSessionArray;
    NSPredicate *delpredicate;
    NSDate *formateventstdate;
    NSDate *formateventenddate;
    NSDate *newsetdate;
    NSString *deletionid;
    NSDateFormatter * dateFormatter;
    NSMutableArray *sessionspeaker;
    
    UICollectionView *sessioncollectionview;
    NSMutableArray *sessionSpeakerArr;
    
    NSString *datestr;
    NSDate *newdisplaydate;
    NSString *canedit;
    
    NSString *semp;
    dispatch_semaphore_t sem_speaker;
    int RefreshTableview;
    NSMutableArray *groupArray;
    dispatch_semaphore_t sem_event;
    
}
@end

@implementation ScheduleView
@synthesize helper,schedulelistview,schedule_searchbar,addnewsession_btn;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
       
    RefreshTableview = 0;
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context=[appdelegate managedObjectContext];
    _group_tv.delegate =self;
    eventidpredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    [schedule_searchbar setBackgroundImage:[UIImage new]];
    UITextField *searchfield = [schedule_searchbar valueForKey:@"_searchField"];
    searchfield.placeholder = [helper gettranslation:@"LBL_224"];
    searchfield.textColor = [UIColor colorWithRed:51.0/255.0f green:76.0/255.0f blue:104.0/255.0f alpha:1.0f];
    searchfield.backgroundColor = [UIColor colorWithRed:242.0/255.0f green:246.0/255.0f blue:250.0f/255.0f alpha:1.0f];
    schedule_searchbar.delegate = self;
    
    _groupTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    _groupTableView.layer.borderWidth = 1;
    
    if (![appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
    {
        addnewsession_btn.hidden = YES;
    }
    [self refreshdata];
    [self SetupEventSchedule];
    groupArray = [[NSMutableArray alloc]init];
    _group_tv.placeholder=[helper gettranslation:@"LBL_932"];
    NSArray *temp = [[helper query_alldata:@"Event_Group"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    [groupArray addObjectsFromArray:temp];
    
    NSMutableDictionary *grpDict = [[NSMutableDictionary alloc]init];
    [grpDict setValue:@"" forKey:@"eventid"];
    [grpDict setValue:@"" forKey:@"groupid"];
    [grpDict setValue:@"ALL" forKey:@"groupname"];
    [groupArray addObject: grpDict];
    
    [_groupTableView setHidden:YES];
    
    
    _eventname_ulbl.text = [helper gettranslation:@"LBL_131"] ;
    _eventsdate_ulbl.text = [helper gettranslation:@"LBL_239"] ;
    _eventedate_ulbl.text = [helper gettranslation:@"LBL_122"] ;
    _group_lbl.text = [helper gettranslation:@"LBL_923"];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
   if( RefreshTableview == 1)
   {
       schedulelistview.delegate = self;
       schedulelistview.dataSource = self;
       [schedulelistview reloadData];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _groupTableView)
    {
        _groupTableView.hidden = YES;
        [_group_tv resignFirstResponder];
    }
}

-(void)refreshdata
{
  
    @autoreleasepool {
        eventData=[[helper query_alldata:@"Event"]filteredArrayUsingPredicate:eventidpredicate];
        sessionlist = [[helper query_alldata:@"Event_Session"] filteredArrayUsingPredicate:eventidpredicate];

    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) SetupEventSchedule
{
    @autoreleasepool {
        
    
    if([eventData count]>0)
    {
        _eventname_lbl.text = [eventData[0] valueForKey:@"name"];
        
        _eventstdate_lbl.text = [helper formatingdate:[eventData[0] valueForKey:@"startdate"] datetime:@"FormatDate"];
        _eventenddate_lbl.text = [helper formatingdate:[eventData[0] valueForKey:@"enddate"] datetime:@"FormatDate"];
        
       
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        
        currentDate = [NSDate date];
        
        NSDate *eventstdate = [dateFormatter dateFromString:[eventData[0] valueForKey:@"startdate"]];
        NSDate *eventenddate = [dateFormatter dateFromString:[eventData[0] valueForKey:@"enddate"]];
        
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        
        NSString *strCurrentDate=[dateFormatter stringFromDate:[NSDate date]];
        NSString *strEventStartDate=[dateFormatter stringFromDate:eventstdate];
        NSString *strEventEndDate=[dateFormatter stringFromDate:eventenddate];
        
        
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        
        currentDate=[dateFormatter dateFromString:strCurrentDate];
       
        
        formateventstdate=[dateFormatter dateFromString:[dateFormatter stringFromDate:eventstdate]];
        formateventenddate=[dateFormatter dateFromString:[dateFormatter stringFromDate:eventenddate]];
        
        
        
        /* if(currentDate>formateventstdate && currentDate <formateventenddate)
         {
         //current date lies between start date and end date
         if(currentDate==formateventstdate)
         {
         _Titledate.text=[NSString stringWithFormat:@"%@",strEventStartDate];
         newsetdate=formateventstdate;
         [_predate_btn setHidden:YES];
         
         }
         else if (currentDate==formateventenddate)
         {
         _Titledate.text=[NSString stringWithFormat:@"%@",strEventEndDate];
         //dt=_datelbl.text;
         newsetdate=formateventenddate;
         
         [_nextdate_btn setHidden:YES];
         }
         else
         {
         _Titledate.text=[NSString stringWithFormat:@"%@",strCurrentDate];
         // dt=_datelbl.text;
         newsetdate=currentDate;
         }
         }
         else if(currentDate<formateventstdate ||currentDate >formateventenddate)
         {
         _Titledate.text=[NSString stringWithFormat:@"%@",strEventStartDate];
         newsetdate = formateventstdate;
         [_predate_btn setHidden:YES];
         
         }
         else
         {
         _Titledate.text = [NSString stringWithFormat:@"%@",strCurrentDate];
         }*/
        if ([currentDate compare:formateventstdate] == NSOrderedDescending && [currentDate compare:formateventenddate] == NSOrderedAscending)
        {
            _Titledate.text= strCurrentDate;
            newsetdate=currentDate;
            
            _predate_btn.hidden = NO;
            _nextdate_btn.hidden=NO;

            
        }
        else if ([currentDate compare:formateventstdate] == NSOrderedSame)
        {
            _Titledate.text= strEventStartDate;
            newsetdate=formateventstdate;
            
            unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
            NSCalendar* calendar = [NSCalendar currentCalendar];
            
            NSDateComponents* components = [calendar components:flags fromDate:formateventstdate];
            NSDateComponents* components1 = [calendar components:flags fromDate:formateventenddate];
            NSDate* stDt = [calendar dateFromComponents:components];
            NSDate* endDt=[calendar dateFromComponents:components1];
            
            if([stDt compare:endDt]==NSOrderedSame)
            {
                _predate_btn.hidden = YES;
                _nextdate_btn.hidden=YES;
            }
            else
            _predate_btn.hidden = YES;
            
            
        }
        else if ([currentDate compare:formateventenddate] == NSOrderedSame)
        {
            _Titledate.text = strEventEndDate;
            newsetdate = formateventenddate;
            _nextdate_btn.hidden = YES;
            _predate_btn.hidden=NO;
        }
        else
        {
            _Titledate.text = strEventStartDate;
            newsetdate = formateventstdate;
            
            unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
            NSCalendar* calendar = [NSCalendar currentCalendar];
            
            NSDateComponents* startDatecomponents = [calendar components:flags fromDate:formateventstdate];
            NSDateComponents* endDateComponents = [calendar components:flags fromDate:formateventenddate];
            NSDate* stDt = [calendar dateFromComponents:startDatecomponents];
            NSDate* endDt=[calendar dateFromComponents:endDateComponents];
            
            if([stDt compare:endDt]==NSOrderedSame)
            {
                _predate_btn.hidden = YES;
                _nextdate_btn.hidden=YES;
            }
            else
            {
                _predate_btn.hidden = YES;
                _nextdate_btn.hidden = NO;
            }
        }
        [self filterSessions];
    }
    }
    
}

-(void) filterSessions
{
    
        
  
     predicatedSessionArray = [[NSMutableArray alloc]init];
    for(int i=0;i<sessionlist.count;i++)
    {
        NSDateFormatter *sessionDateFormatter = [[NSDateFormatter alloc] init];
        [sessionDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [sessionDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        NSDate *sessiondate = [sessionDateFormatter dateFromString:[sessionlist[i] valueForKey:@"startdate"]];
        
        
        [sessionDateFormatter setDateFormat:@"yyyyMMdd"];
        
        NSString *sessiondate_str = [sessionDateFormatter stringFromDate:sessiondate];
        NSString *currentdate_str = [sessionDateFormatter stringFromDate:newsetdate];
        
        
        if ([sessiondate_str isEqualToString:currentdate_str])
        {
            [predicatedSessionArray addObject:sessionlist[i]];
        }
        
    }
    sessionSpeakerArr = [[NSMutableArray alloc]init];
    for (int i =0; i<[predicatedSessionArray count]; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:predicatedSessionArray[i] forKey:@"schedule"];
        //sessionspeaker = [[helper query_alldata:@"Event_Speaker"] mutableCopy];
        NSPredicate *sessionidpred = [NSPredicate predicateWithFormat:@"sessionid like %@",[predicatedSessionArray[i] valueForKey:@"id"]];
        sessionspeaker = [[[helper query_alldata:@"Event_Speaker"] filteredArrayUsingPredicate:sessionidpred] mutableCopy];
        
        
        if ([appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
        {
            NSMutableDictionary *addspeaker = [[NSMutableDictionary alloc] init];
            [addspeaker setValue:@"Add" forKey:@"firstname"];
            [addspeaker setValue:@"Speaker" forKey:@"lastname"];
            [addspeaker setValue:@"addnewspeaker" forKey:@"specialitylic"];
            [addspeaker setValue:@"addnewspeaker" forKey:@"speciality"];
            [addspeaker setValue:@"EVENT_SPK_ADD" forKey:@"id"];
            [addspeaker setValue:[predicatedSessionArray[i] valueForKey:@"id"] forKey:@"sessionid"];
            [addspeaker setValue:@"" forKey:@"imageicon"];
            [sessionspeaker addObject:addspeaker];
        }
        
        [dict setObject:sessionspeaker forKey:@"speaker"];
        [sessionSpeakerArr addObject:dict];
        
    }
   // }
   
    schedulelistview.delegate = self;
    schedulelistview.dataSource = self;
    [schedulelistview reloadData];
    
 
}

#pragma mark tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == schedulelistview)
        return [sessionSpeakerArr count];
    else
        return [groupArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == schedulelistview){
    
        NSArray *predicatearr = [sessionSpeakerArr[indexPath.row] valueForKey:@"schedule"];
        NSString *typelic = [predicatearr valueForKey:@"typelic"];
        NSArray *SpeakerArr = [sessionSpeakerArr[indexPath.row] valueForKey:@"speaker"];
        if ([SpeakerArr count] == 0 )
        {
            return 88;
        }
    
        if ([typelic isEqualToString:@"Break"])
        {
            return 88;
        }
        else
        {
            return 190;
        }
    }
    else
        return 43;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    if(tableView == schedulelistview){
    customcell = [self.schedulelistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
    
    
    UIButton *sessionimg = (UIButton *) [contentView viewWithTag:1];
    UILabel *session_name = (UILabel *) [contentView viewWithTag:2];
    UILabel *session_sttime = (UILabel *) [contentView viewWithTag:3];
    UILabel *session_endtime = (UILabel *) [contentView viewWithTag:4];
    UILabel *session_loc = (UILabel *) [contentView viewWithTag:5];
    UILabel *session_desc = (UILabel *) [contentView viewWithTag:9];
    UIButton *session_delete= (UIButton *) [contentView viewWithTag:555];
    UILabel *session_group_lbl = (UILabel*)[contentView viewWithTag:5000];
    
    
    UIButton *session_editbtn = (UIButton *) [contentView viewWithTag:6];
    UIButton *session_qnabtn = (UIButton *) [contentView viewWithTag:7];
    UILabel *Ratinglbl = (UILabel *)[contentView viewWithTag:10];
    UIImageView *RatingImageView = (UIImageView *)[contentView viewWithTag:11];
    
    sessioncollectionview  = (UICollectionView *) [contentView viewWithTag:8];
    sessioncollectionview.allowsMultipleSelection = NO;
    
    
    UILabel *sessionname_ulbl = (UILabel *) [contentView viewWithTag:1901];
    UILabel *sessionstime_ulbl = (UILabel *) [contentView viewWithTag:1902];
    UILabel *sessionetime_ulbl = (UILabel *) [contentView viewWithTag:1903];
    UILabel *sessionloc_ulbl = (UILabel *) [contentView viewWithTag:1904];
    UILabel *session_group = (UILabel *)[contentView viewWithTag:5001];
   
 
    sessionname_ulbl.text = [helper gettranslation:@"LBL_231"];
    sessionstime_ulbl.text = [helper gettranslation:@"LBL_240"];
    sessionetime_ulbl.text = [helper gettranslation:@"LBL_123"];
    sessionloc_ulbl.text = [helper gettranslation:@"LBL_161"];
    session_group_lbl.text = [helper gettranslation:@"LBL_923"];
    
    sessioncollectionview.restorationIdentifier = [NSString stringWithFormat:@"%li", indexPath.row];
    
         [sessioncollectionview reloadData];
   
   
    
    [session_editbtn addTarget:self action:@selector(sessioneditbtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [session_delete addTarget:self action:@selector(delete_Session:) forControlEvents:UIControlEventTouchUpInside];
    
    [session_qnabtn addTarget:self action:@selector(sessionqnabtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmss"];
    
    NSArray *arr = [sessionSpeakerArr[indexPath.row] valueForKey:@"schedule"];
    
    NSDate *sessionstdate= [dateFormatter1 dateFromString:[arr valueForKey:@"startdate"]];
    NSDate *sessionenddate = [dateFormatter1 dateFromString:[arr valueForKey:@"enddate"]];
    
    
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter1 setDateFormat:@"HH:mm"];
    NSString *upcomingStartDate = [dateFormatter1 stringFromDate:sessionstdate];
    NSString *upcomingEndDate = [dateFormatter1 stringFromDate:sessionenddate];
    
    
    session_name.text = [arr valueForKey:@"title"];
    session_desc.text = [arr valueForKey:@"session_desc"];
    
        NSArray *tempArray = [groupArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupid like %@",[arr valueForKey:@"groupID"]]];
        
        if(tempArray.count>0)
            session_group.text = [tempArray[0] valueForKey:@"groupname"];
        else
            session_group.text = @"";
    
    session_sttime.text = upcomingStartDate;
    session_endtime.text = upcomingEndDate;
    session_loc.text=[predicatedSessionArray[indexPath.row] valueForKey:@"location"]; //location
    
    NSString *typelic = [arr valueForKey:@"typelic"];
    if ([typelic isEqualToString:@"Break"])
    {
        UIImage *btnImage = [UIImage imageNamed:@"break.png"];
        [sessionimg setImage:btnImage forState:UIControlStateNormal];
        session_qnabtn.hidden = YES;
    }
    else
    {
        UIImage *btnImage = [UIImage imageNamed:@"sessions.png"];
        [sessionimg setImage:btnImage forState:UIControlStateNormal];
        session_qnabtn.hidden = NO;
    }
    if (![appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
    {
        addnewsession_btn.hidden = YES;
        session_editbtn.hidden = YES;
        session_delete.hidden=YES;
    }
    
    
    Ratinglbl.text = [predicatedSessionArray[indexPath.row] valueForKey:@"rating"];
    if([predicatedSessionArray[indexPath.row] valueForKey:@"rating"] == nil )
    {
        [RatingImageView setHidden:YES];
    }
    else
    {
        RatingImageView.image = [UIImage imageNamed:@"Star.png"];
        [RatingImageView setHidden:NO];
    }
}
    else if (tableView == _groupTableView){
        customcell = [self.groupTableView dequeueReusableCellWithIdentifier:@"socustomcell" forIndexPath:indexPath];
        UILabel *groupName = (UILabel *)[customcell.contentView viewWithTag:1];
        
        if([[[groupArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"]){
            groupName.text = [helper gettranslation:@"LBL_037"];
        }
        else
        groupName.text = [[groupArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
    }
    return customcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Do not select");
    if(tableView == _groupTableView){
        if([[[groupArray objectAtIndex:indexPath.row] valueForKey:@"groupname"] isEqualToString:@"ALL"])
        {
            [self refreshdata];
            [self SetupEventSchedule];
        }
        else{
            
        sessionlist = [[helper query_alldata:@"Event_Session"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"groupID like %@",[[groupArray objectAtIndex:indexPath.row] valueForKey:@"groupid"]]];
//            _group_tv.text= [[groupArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
            [self SetupEventSchedule];
        }
        _group_tv.text= [[groupArray objectAtIndex:indexPath.row] valueForKey:@"groupname"];
        [_group_tv resignFirstResponder];
        [_groupTableView setHidden:YES];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //     Get the new view controller using [segue destinationViewController].
    //     Pass the selected object to the new view controller.
}
#pragma mark unwind method
-(IBAction)unwindtoEventschedule:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
    [self refreshdata];
    [self SetupEventSchedule];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    int row=[collectionView.restorationIdentifier intValue];
    NSArray *SpeakerArr = [[sessionSpeakerArr objectAtIndex:row] valueForKey:@"speaker"];
    return [SpeakerArr count];
    //return [[[sessionSpeakerArr objectAtIndex:row] valueForKey:@"speaker"] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int row=[collectionView.restorationIdentifier intValue];
    NSArray *SpeakerArr = [[sessionSpeakerArr objectAtIndex:row] valueForKey:@"speaker"];
    
    static NSString *identifier = @"speakercollectioncard";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIView *contentView = cell.contentView;
    
    UIWebView *speakerwebimage = (UIWebView *) [contentView viewWithTag:84];
    UIImageView *speaker_img = (UIImageView *) [contentView viewWithTag:81];
    UILabel *speakername_lv = (UILabel *) [contentView viewWithTag:82];
    UILabel *speakerspeciality_lv = (UILabel *) [contentView viewWithTag:83];
    UIButton *removespeaker_lv = (UIButton *) [contentView viewWithTag:85];
    
    
    
    if ([appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
    {
        removespeaker_lv.accessibilityValue = [NSString stringWithFormat:@"%ld",indexPath.row];
        [removespeaker_lv addTarget:self action:@selector(activateDeletionMode:) forControlEvents:UIControlEventTouchUpInside];
         removespeaker_lv.hidden = NO;
    }
    else
    {
        removespeaker_lv.hidden = YES;
    }
    NSString *speciality = [SpeakerArr[indexPath.row] valueForKey:@"speciality"];
    
    speaker_img.layer.cornerRadius = speaker_img.frame.size.width / 2;
    if ([speciality isEqualToString:@"addnewspeaker"])
    {
        //        if ([appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
        //        {
        speaker_img.image = [UIImage imageNamed:@"addnewspeaker.png"];
        [speakername_lv setText:[NSString stringWithFormat:@"%@ %@",[SpeakerArr[indexPath.row] valueForKey:@"firstname"],[SpeakerArr[indexPath.row] valueForKey:@"lastname"]]];
        
        [speakerspeciality_lv setText:@""];
        speakerwebimage.hidden  = YES;
        removespeaker_lv.hidden = YES;
          speaker_img.layer.masksToBounds=NO;
        //}
        
    }
    else
    {
        
        [speakername_lv setText:[NSString stringWithFormat:@"%@ %@",[SpeakerArr[indexPath.row] valueForKey:@"firstname"],[SpeakerArr[indexPath.row] valueForKey:@"lastname"]]];
        [speakerspeciality_lv setText:[SpeakerArr[indexPath.row] valueForKey:@"speciality"]];
        
        NSPredicate *spepredicate = [NSPredicate predicateWithFormat:@"id like %@",[SpeakerArr[indexPath.row] valueForKey:@"speakerid"]];
        NSArray *masterspkr = [[helper query_alldata:@"Speaker"] filteredArrayUsingPredicate:spepredicate];
       // NSString *spkrimg = [masterspkr[0] valueForKey:@"imageicon"];
        
        speakerwebimage.hidden  = YES;
        //speaker_img.layer.cornerRadius = speaker_img.frame.size.width / 2;
        speaker_img.image = [UIImage imageNamed:@"User.png"];
        
        dispatch_queue_t queue = dispatch_queue_create(0, 0);
        dispatch_async(queue,
                       ^{
                           NSString *spkrimg = [masterspkr[0] valueForKey:@"imageicon"];
                           NSData *imgdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:spkrimg]];
                           UIImage *Img = [UIImage imageWithData:imgdata];
                           
                           if(Img)
                           {
                               dispatch_async(dispatch_get_main_queue(),
                                              ^{
                                                  
                                                  
                                                  speaker_img.image=Img;
                                                  
                                                  
                                              });
                           }
//                           else
//                            {
//                                   dispatch_async(dispatch_get_main_queue(),
//                                                  ^{
//                                                      
//                                                      
//                                                      speaker_img.image=[UIImage imageNamed:@"User.png"];
//                                                      
//                                                      
//                                                  });
//                               }

                       });
        
        
        speaker_img.layer.masksToBounds=YES;
        
        //        if (spkrimg.length>0)
        //        {
        //            NSString *htmlString = @"<html><body><img src='%@' width='200'></body></html>";
        //            NSString *imageHTML  = [[NSString alloc] initWithFormat:htmlString, spkrimg];
        //
        //            // Load image in UIWebView
        //            speakerwebimage.backgroundColor=[UIColor whiteColor];
        //            speakerwebimage.scalesPageToFit = YES;
        //            [speakerwebimage loadHTMLString:imageHTML baseURL:nil];
        //
        ////            NSString *strTemplateHTML = [NSString stringWithFormat:@"<html><head><style>img{max-width:100%%;height:auto !important;width:auto !important;};</style></head><body style='margin:0; padding:0;'>%@</body></html>", spkrimg];
        ////
        ////            [speakerwebimage loadHTMLString:strTemplateHTML baseURL:nil];
        //
        //
        //            speakerwebimage.layer.cornerRadius = speakerwebimage.frame.size.width / 2;
        //
        //            speaker_img.hidden = YES;
        //        }
        //        else
        //        {
        //            speakerwebimage.hidden  = YES;
        //            speaker_img.layer.cornerRadius = speaker_img.frame.size.width / 2;
        //            speaker_img.image = [UIImage imageNamed:@"User.png"];
        //        }
        
        speakerwebimage.hidden  = YES;
        
        // speaker_img.image = [UIImage imageNamed:@"User.png"];
        
    }
    return cell;
}
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    return !collectionView.dragging && !collectionView.tracking;
//}
//-(BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    return NO;
//}
#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appdelegate.EventUserRoleArray containsObject:@"E_SES_MNG"])
    {
        //addnewsession_btn.hidden = YES;
        int viewcount = [collectionView numberOfItemsInSection:0];
        if (indexPath.row == viewcount-1)
        {
            CGPoint buttonPosition = [collectionView convertPoint:CGPointZero toView:schedulelistview];
            NSIndexPath *selindexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
            NSArray *SessionArr = [sessionSpeakerArr[selindexPath.row] valueForKey:@"schedule"];
            appdelegate.sessionid = [SessionArr valueForKey:@"id"];
            [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddspeaker" sender:self];
        }
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

//- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    
//}


#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 2, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 8.0;
}

- (IBAction)addnewsession_btn:(id)sender
{
    appdelegate.sessionid = @"";
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddsession" sender:self];
    
}

-(IBAction)sessioneditbtnClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    NSIndexPath *indexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
    appdelegate.sessionid = [predicatedSessionArray[indexPath.row] valueForKey:@"id"];
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddsession" sender:self];
}

-(IBAction)sessionqnabtnClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    NSIndexPath *indexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
    appdelegate.sessionid = [predicatedSessionArray[indexPath.row] valueForKey:@"id"];
    
    [self.parentViewController performSegueWithIdentifier:@"EventDetailTosessionQnA" sender:self];
}


- (IBAction)predate_btn:(id)sender
{
    
    @autoreleasepool {
        
    
    
    NSDateFormatter *dateNFormatter = [[NSDateFormatter alloc] init];
    [dateNFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateNFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDate *chckdt=[dateNFormatter dateFromString:_Titledate.text];
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.day = -1;
    newdisplaydate = [[NSCalendar currentCalendar]dateByAddingComponents:dateComponents toDate:chckdt options:0];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    
    currentDate=(NSDate *)[dateFormatter stringFromDate:newdisplaydate];
    _Titledate.text=[NSString stringWithFormat:@"%@",currentDate];
    newsetdate = newdisplaydate;
    

        
    
  //  [self refreshdata];
    
    [self filterSessions];
    
    
    
    if ([newsetdate compare:formateventstdate] == NSOrderedSame)
    {
        [_predate_btn setHidden:YES];
    }
    
    if([newsetdate compare:formateventenddate] == NSOrderedAscending)
    {
        [_nextdate_btn setHidden:NO];
    }
    
   
    }
}


- (IBAction)nextdate_btn:(id)sender
{
    
        
    @autoreleasepool {
        
    
    NSDateFormatter *dateNFormatter = [[NSDateFormatter alloc] init];
    [dateNFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateNFormatter setDateFormat:@"dd MMM yyyy"];
    NSDate *chckdt=[dateNFormatter dateFromString:_Titledate.text];
    
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.day = 1;
    newdisplaydate = [[NSCalendar currentCalendar]dateByAddingComponents:dateComponents toDate:chckdt options:0];
    
    currentDate = (NSDate *)[dateNFormatter stringFromDate:newdisplaydate];
    _Titledate.text = [NSString stringWithFormat:@"%@",currentDate];
    
    newsetdate = newdisplaydate;
   
    
    //[self refreshdata];
    [self filterSessions];
    
   
    if ([newsetdate compare:formateventenddate] == NSOrderedSame)
    {
        [_nextdate_btn setHidden:YES];
    }
    if([newsetdate compare:formateventstdate] == NSOrderedDescending)
    {
        [_predate_btn setHidden:NO];
    }
    
    }
    
}


- (void)activateDeletionMode:(UIButton *)sender
{
    
//    sem_speaker = dispatch_semaphore_create(0);
   
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    NSIndexPath *tbllindexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
    
    NSArray *SpeakerArr = [[sessionSpeakerArr objectAtIndex:tbllindexPath.row] valueForKey:@"speaker"];
    
   // UIButton *button = (UIButton *) sender;
    
   //NSString *row = button.accessibilityValue;
    
   UICollectionViewCell *cell = (UICollectionViewCell *)[[sender superview]superview];
    
  // NSIndexPath *collectionviewindex = [sessioncollectionview indexPathForCell:cell];
    
    NSIndexPath *collectionviewindex = [NSIndexPath indexPathForRow:[[sender accessibilityValue]intValue] inSection:0];
    
  //  [self startjingle:collectionviewindex];
    
    
    deletionid  = [SpeakerArr[collectionviewindex.row] valueForKey:@"id"];
    delpredicate = [NSPredicate predicateWithFormat:@"id like %@",deletionid];
    
//    sem_speaker = dispatch_semaphore_create(0);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_584"] message:[helper gettranslation:@"LBL_489"] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        semp = @"No";
        [cell.layer removeAllAnimations];

//        dispatch_semaphore_signal(sem_speaker);
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        semp = @"Yes";
        [self deleteSpeaker];
//        dispatch_semaphore_signal(sem_speaker);
    }]];

    [self presentViewController:alert animated:true completion:nil];
    

        
    
}

-(void)deleteSpeaker{
    [helper showWaitCursor:[helper gettranslation:@"LBL_365"]];
    dispatch_queue_t queue=dispatch_queue_create(0,0);
    dispatch_async(queue, ^{
    [helper serverdelete:@"E_SPEAKER" entityid:deletionid];
    [helper delete_predicate:@"Event_Speaker" predicate:delpredicate];
    
         dispatch_async(dispatch_get_main_queue(), ^{
             [helper removeWaitCursor];
    [self refreshdata];
    [self SetupEventSchedule];
       
    });
});
    
}
-(void) startjingle:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell = [sessioncollectionview cellForItemAtIndexPath:indexPath];
    
    CAKeyframeAnimation *position = [CAKeyframeAnimation animation];
    position.keyPath = @"position";
    position.values = @[
                        [NSValue valueWithCGPoint:CGPointZero],
                        [NSValue valueWithCGPoint:CGPointMake(-1, 0)],
                        [NSValue valueWithCGPoint:CGPointMake(1, 0)],
                        [NSValue valueWithCGPoint:CGPointMake(-1, 1)],
                        [NSValue valueWithCGPoint:CGPointMake(1, -1)],
                        [NSValue valueWithCGPoint:CGPointZero]
                        ];
    position.timingFunctions = @[
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]
                                 ];
    position.additive = YES;
    
    CAKeyframeAnimation *rotation = [CAKeyframeAnimation animation];
    rotation.keyPath = @"transform.rotation";
    rotation.values = @[
                        @0,
                        @0.03,
                        @0,
                        [NSNumber numberWithFloat:-0.02]
                        ];
    rotation.timingFunctions = @[
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]
                                 ];
    
    CAAnimationGroup *group = [[CAAnimationGroup alloc] init];
    group.animations = @[position, rotation];
    group.duration = 0.4;
    group.repeatCount = HUGE_VALF;
    
    
    
    [cell.layer addAnimation:group forKey:@"wiggle"];
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

-(void)delete_Session:(id)sender
{
    
//   sem_event = dispatch_semaphore_create(0);
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:schedulelistview];
    NSIndexPath *indexPath = [schedulelistview indexPathForRowAtPoint:buttonPosition];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_357"] message:[helper gettranslation:@"LBL_486"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   semp = @"Yes";
                                   [self DeleteFunction:indexPath];

                                   
                               }];
    [alert addAction:okaction];
    
    
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       semp = @"No";
                                   }];
    
    [alert addAction:cancelaction];
    [self presentViewController:alert animated:NO completion:nil];
    }

-(void)Delete_Session_Local:(NSString *)sessionid
{
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",sessionid];
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Event_Session"];
    
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    int cnt = 0;
    for (NSManagedObject *obj in matchingData)
    {
        [context deleteObject:obj];
        cnt++;
    }
    [context save:&error];
    
    if(matchingData)
    {
        NSLog(@"Removed");
    }
    else
    {
        NSLog(@"Error:%@",error);
    }

}

-(void)Delete_SessionSpeaker_Local:(NSString *)speakrID
{
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",speakrID];
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Event_Speaker"];
    
    fetchRequest.predicate=predicate;
    
    NSArray *matchingData = [context executeFetchRequest:fetchRequest error:&error];
    int cnt = 0;
    for (NSManagedObject *obj in matchingData)
    {
        [context deleteObject:obj];
        cnt++;
    }
    [context save:&error];
    
    if(matchingData)
    {
        NSLog(@"Removed");
    }
    else
    {
        NSLog(@"Error:%@",error);
    }
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == schedule_searchbar)
    {
        if(count > searchText.length)
        {
            [self refreshdata];
        }
        if(searchText.length == 0)
        {
            [self refreshdata];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            [self refreshdata];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.title contains[c] %@) OR (SELF.type contains[c] %@) OR (SELF.location contains[c] %@) OR (SELF.session_desc contains[c] %@)",searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[sessionlist filteredArrayUsingPredicate:predicate]];
            sessionlist = [filtereventarr copy];
        }
    }
        [self filterSessions];
        [schedulelistview reloadData];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _group_tv){
        [_group_tv resignFirstResponder];
        [_groupTableView setHidden:NO];
        [_groupTableView reloadData];
    }
}


-(void)DeleteFunction:(NSIndexPath*)indexPath{
    
    [helper showWaitCursor:[helper gettranslation:@"LBL_734"]];
        dispatch_queue_t queue=dispatch_queue_create(0,0);
        dispatch_async(queue, ^{
    NSString *sessID = [predicatedSessionArray[indexPath.row] valueForKey:@"id"];
    
    NSPredicate *sessionidpred = [NSPredicate predicateWithFormat:@"sessionid like %@",sessID];
    NSArray *sessionspkr = [[helper query_alldata:@"Event_Speaker"] filteredArrayUsingPredicate:sessionidpred];
    
    for(int i=0;i<sessionspkr.count;i++)
    {
        [helper serverdelete:@"E_SPEAKER" entityid:[sessionspkr[i] valueForKey:@"id"]];
        [self Delete_SessionSpeaker_Local:[sessionspkr[i] valueForKey:@"id"]];
    }
    [helper serverdelete:@"E_SESSION" entityid:sessID];
    [self Delete_Session_Local:sessID];
            
          dispatch_async(dispatch_get_main_queue(), ^{
    [helper removeWaitCursor];
    [self refreshdata];
              [self filterSessions];
//    [self SetupEventSchedule];
    [self.schedulelistview reloadData];
          });
        });
}
@end
