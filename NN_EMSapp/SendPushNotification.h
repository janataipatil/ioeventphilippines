//
//  SendPushNotification.h
//  NN_EMSapp
//
//  Created by Gaurav Kumar on 13/03/18.
//  Copyright © 2018 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendPushNotification : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *send_btn;
- (IBAction)send_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *titleLBL;

@property (weak, nonatomic) IBOutlet UILabel *enterYourMessage_lbl;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@end
