//
//  SendPushNotification.m
//  NN_EMSapp
//
//  Created by Gaurav Kumar on 13/03/18.
//  Copyright © 2018 iWizards. All rights reserved.
//

#import "SendPushNotification.h"
#import "AppDelegate.h"
#import "HelperClass.h"

@interface SendPushNotification ()
{
    NSString *PushMessage;
    AppDelegate *appdelegate;
    HelperClass *helper;
    
}

@end

@implementation SendPushNotification

- (void)viewDidLoad {
    [super viewDidLoad];
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    _messageTextView.text = @"";
    _messageTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    _messageTextView.layer.borderWidth = 0.5;
    
    [_send_btn setTitle:[helper gettranslation:@"LBL_916"] forState:UIControlStateNormal];
    [_cancelBtn setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    _enterYourMessage_lbl.text=[helper gettranslation:@"LBL_933"];
    _titleLBL.text = [helper gettranslation:@"LBL_921"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)send_btn:(id)sender {
    if([_messageTextView.text length]>0)
    {   [_messageTextView resignFirstResponder];
        [helper showWaitCursor:@"LBL_930"];
        NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
        [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
        [JsonDict setValue:_messageTextView.text forKey:@"Message"];
        
        NSString *jsonString;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSPUSHEventBroadcast" pageno:@"" pagecount:@"" lastpage:@""];
        
        NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
        
        if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
        {
            [helper removeWaitCursor];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            [helper removeWaitCursor];
            NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
            NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];

        }
    }
}
- (IBAction)cancelBtn:(id)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];

}
@end
