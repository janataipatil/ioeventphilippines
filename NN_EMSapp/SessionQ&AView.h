//
//  SessionQ&AView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/7/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "AnswerVC.h"

@interface SessionQ_AView : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextViewDelegate>

@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic) Reachability* reachability;

//@property (strong, nonatomic) IBOutlet UIView *Activitylistview;
@property (strong, nonatomic) IBOutlet UITableView *Questionlistview;

- (IBAction)Answer_segmentcontrol:(id)sender;

@property (strong, nonatomic) IBOutlet UISegmentedControl *Answer_segmentcontrol;

@property (strong, nonatomic) IBOutlet UISearchBar *AnswerSearchBar;

@property (strong, nonatomic) IBOutlet UIView *MainView;

-(IBAction)editanswerClicked:(id)sender;

- (IBAction)close_btn:(id)sender;
-(void) Refreshview;


@property (strong, nonatomic) IBOutlet UILabel *title_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *closebtn_ulbl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentcontroller_ulbl;

@end
