//
//  SessionQ&AView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/7/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "SessionQ&AView.h"
#import "AppDelegate.h"
#import "ToastView.h"
#import "SOCustomCell.h"

@interface SessionQ_AView ()
{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSPredicate *eventpredicate,*answeredpredicate,*pendingPredicate,*sessionpredicate;
    NSInteger selectedsegment;
}

@end

@implementation SessionQ_AView
{
    SLKeyChainStore *keychain;
    MSClient *client;
    NSArray *QsnAnswerlist;
    AnswerVC *answervc;
    NSString *appID;
    NSString *custID;
    NSString *AESPrivateKey;
    NSInteger questioncount;
    NSInteger answercount;
    NSInteger ln_expandedRowIndex;
    NSString *editflg;
    NSString *sel_activityid;
    int acordian;
    int deleteinsert;
}

@synthesize helper,Questionlistview,AnswerSearchBar,Answer_segmentcontrol,MainView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
   
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    AESPrivateKey =[helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    ln_expandedRowIndex = -1;
    
    [AnswerSearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.AnswerSearchBar valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    sessionpredicate = [NSPredicate predicateWithFormat:@"SELF.sessionid like %@",appdelegate.sessionid];
    answeredpredicate = [NSPredicate predicateWithFormat:@"SELF.statuslic like %@",@"Answered"];
    pendingPredicate = [NSPredicate predicateWithFormat:@"SELF.statuslic like %@",@"Unanswered"];
    

    [Answer_segmentcontrol setSelectedSegmentIndex:0];
    [Answer_segmentcontrol sendActionsForControlEvents:UIControlEventValueChanged];
    
    
    NSArray *sessionarray = [[helper query_alldata:@"Event_Session"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.id like %@",appdelegate.sessionid]] ;
    editflg = [sessionarray[0] valueForKey:@"canEdit"];
    
    AnswerSearchBar.delegate = self;
    
    Questionlistview.delaysContentTouches = false;
    
    
    [self langsetuptranslations];
    
    
}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_closebtn_ulbl setTitle:[helper gettranslation:@"LBL_342"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    
    _title_ulbl.text = [helper gettranslation:@"LBL_229"];
    
    [_segmentcontroller_ulbl setTitle:[helper gettranslation:@"LBL_037"] forSegmentAtIndex:0];
    [_segmentcontroller_ulbl setTitle:[helper gettranslation:@"LBL_042"] forSegmentAtIndex:1];
    [_segmentcontroller_ulbl setTitle:[helper gettranslation:@"LBL_574"] forSegmentAtIndex:2];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Answer_segmentcontrol:(id)sender
{
  
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    // NSPredicate *activityPredicate,*mypredicate,*teampredicate;
    // NSPredicate *activityPredicate;
    ln_expandedRowIndex = -1;
    selectedsegment = self.Answer_segmentcontrol.selectedSegmentIndex;
    
    switch (self.Answer_segmentcontrol.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"segment index is %ld",(long)self.Answer_segmentcontrol.selectedSegmentIndex);
            
            QsnAnswerlist = [[helper query_alldata:@"Event_SessionQnA"] filteredArrayUsingPredicate:sessionpredicate];
            [Questionlistview reloadData];
            
            break;
            
        case 1:
            NSLog(@"segment index is %ld",(long)self.Answer_segmentcontrol.selectedSegmentIndex);
            
            QsnAnswerlist = [[helper query_alldata:@"Event_SessionQnA"] filteredArrayUsingPredicate:sessionpredicate];
            QsnAnswerlist = [QsnAnswerlist filteredArrayUsingPredicate:answeredpredicate];
            [Questionlistview reloadData];
            break;
        case 2:
            NSLog(@"segment index is %ld",(long)self.Answer_segmentcontrol.selectedSegmentIndex);
            
            QsnAnswerlist = [[helper query_alldata:@"Event_SessionQnA"] filteredArrayUsingPredicate:sessionpredicate];
            QsnAnswerlist = [QsnAnswerlist filteredArrayUsingPredicate:pendingPredicate];
            [Questionlistview reloadData];
            break;
        default:
            break;
    }
    
}


//------------------Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (ln_expandedRowIndex != -1 && row == ln_expandedRowIndex + 1)
    {
        return 150;
    }
    
    return 60;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [QsnAnswerlist count] + (ln_expandedRowIndex != -1 ? 1 : 0);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    BOOL ln_expandedCell = ln_expandedRowIndex != -1 && ln_expandedRowIndex + 1 == row;
    
    
    if (!ln_expandedCell)
    {
        SOCustomCell *customcell;
       // UIImage *image;
        NSString *cellIdentifier = @"customcell";
        
        NSMutableArray *SelectedArray = [[NSMutableArray alloc] init];
        if (indexPath.row >= QsnAnswerlist.count)
        {
            [SelectedArray insertObject:QsnAnswerlist[indexPath.row - 1] atIndex:0];
        }
        else
        {
            [SelectedArray insertObject:QsnAnswerlist[indexPath.row] atIndex:0]; ;
        }
        
        customcell = [self.Questionlistview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
        customcell.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha: 1];
        
        
        
        UILabel *question = (UILabel *) [contentView viewWithTag:2];
        UILabel *qsn_status = (UILabel *) [contentView viewWithTag:3]
        ;
        
        UILabel *question_ulbl = (UILabel *) [contentView viewWithTag:1901];
        UILabel *qsn_status_ulbl = (UILabel *) [contentView viewWithTag:1902];
        
        question_ulbl.text = [helper gettranslation:@"LBL_515"];
        qsn_status_ulbl.text = [helper gettranslation:@"LBL_313"];
        
        
      //  UIImage *qsn_icon = (UIImage *) [contentView viewWithTag:4];
        
        
        [question setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"question_text"]]];
        [qsn_status setText:[NSString stringWithFormat:@"%@",[SelectedArray[0] valueForKey:@"status"]]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];

        
        return customcell;
    }
    else
    {
        [answervc.view removeFromSuperview];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];
        appdelegate.questionid = [QsnAnswerlist[indexPath.row-1] valueForKey:@"id"];
        
        answervc = [self.storyboard instantiateViewControllerWithIdentifier:@"answervc"];
        [answervc.view setFrame:CGRectMake(0, 0, 710, 125)];
        [self addChildViewController:answervc];
        
        [cell.contentView addSubview:answervc.view];
        [answervc didMoveToParentViewController:self];
       
        [Questionlistview setUserInteractionEnabled:YES];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
}
//---------------------Selection Functions start here

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    while (dispatch_semaphore_wait(appdelegate.sem, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSInteger row = [indexPath row];
    BOOL preventReopen = NO;
    
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
    
    
    if (row == ln_expandedRowIndex + 1 && ln_expandedRowIndex != -1)
    {
        return nil;
    }
    [tableView beginUpdates];
    
    
    if (ln_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = ln_expandedRowIndex + 1;
        preventReopen = row == ln_expandedRowIndex;
        if (row > ln_expandedRowIndex)
            row--;
        ln_expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        acordian++;
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        
        ln_expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        cell.contentView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha: 1] ;
    }
    [tableView endUpdates];
    if(indexPath.row !=0)
    {
        
        if (acordian !=  1)
        {
            //delete
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options: UIViewAnimationOptionTransitionNone
                             animations:^{
                                            CGRect frame = Questionlistview.frame;
                                            frame.origin.y = 62;
                                            frame.origin.x = 153;
                                            MainView.frame = frame;
                                 
                                        }
                             completion:nil];
            acordian = -1;
            
        }
       if (!preventReopen)
        {
            //insert
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options: UIViewAnimationOptionTransitionNone
                             animations:^{
                                 CGRect frame = Questionlistview.frame;
                                 frame.origin.y = -62.0*indexPath.row;
                                 frame.origin.x = 153;
                                 MainView.frame = frame;
                             }
                             completion:nil];
            acordian = indexPath.row;
        }
    }
  
    return nil;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row % 2)
    {
        cell.contentView.backgroundColor = [[UIColor alloc]initWithRed:242.0/255.0 green:246.0/255.0 blue:250.0/255.0 alpha:1];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor] ;
    }
}


-(IBAction)editanswerClicked:(id)sender
{
    
}


//-----------------------------------------------------------------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
- (IBAction)unwindToSessionQnAView:(UIStoryboardSegue *)segue;
{
    if(selectedsegment == 0){
        QsnAnswerlist = [[helper query_alldata:@"Event_SessionQnA"] filteredArrayUsingPredicate:sessionpredicate];
        [Questionlistview reloadData];
    }
    else if (selectedsegment == 1){
        QsnAnswerlist = [[helper query_alldata:@"Event_SessionQnA"] filteredArrayUsingPredicate:sessionpredicate];
        QsnAnswerlist = [QsnAnswerlist filteredArrayUsingPredicate:answeredpredicate];
        [Questionlistview reloadData];
    }
}
-(void) Refreshview
{
    
    [Answer_segmentcontrol setSelectedSegmentIndex:selectedsegment];
    [Answer_segmentcontrol sendActionsForControlEvents:UIControlEventValueChanged];
    
    [Questionlistview reloadData];
    
}


- (IBAction)close_btn:(id)sender
{
    [self removepopup];
}

-(void)removepopup
{
    
    [self performSegueWithIdentifier:@"unwindtoeventschedule" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

@end
