//
//  SettingMenu.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/16/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingMenu : UIView

@property (strong,nonatomic) IBOutlet UIView *mainview;
@property (strong, nonatomic) IBOutlet UILabel *fdr_lastsync;
@property (strong, nonatomic) IBOutlet UILabel *msync_lastsync;
@property (strong, nonatomic) IBOutlet UILabel *uploadlogs_lastsync;


@property (strong, nonatomic) IBOutlet UILabel *admin_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *username_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *fdrimage_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *dcal_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *syncDB_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *mlastsync_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *errormsg_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *fdrlastsync_ulbl;


@end
