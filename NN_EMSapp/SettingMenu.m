//
//  SettingMenu.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/16/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "SettingMenu.h"

@implementation SettingMenu

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
    
    self = [super initWithFrame:frame];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SettingMenu" owner:self options:nil];
    UIView *mainView = [subviewArray objectAtIndex:0];
    [self addSubview:mainView];
    
    self.mainview = (UIView *)[mainView viewWithTag:1];
    self.fdr_lastsync = (UILabel *)[mainView viewWithTag:6];
    self.msync_lastsync = (UILabel *)[mainView viewWithTag:8];
    self.uploadlogs_lastsync = (UILabel *)[mainView viewWithTag:12];
        
    
        self.admin_ulbl = (UILabel *)[mainView viewWithTag:1901];
        self.username_ulbl = (UILabel *)[mainView viewWithTag:1902];
        self.fdrimage_ulbl = (UILabel *)[mainView viewWithTag:1903];
        self.dcal_ulbl = (UILabel *)[mainView viewWithTag:1904];
        self.syncDB_ulbl = (UILabel *)[mainView viewWithTag:1905];
        self.fdrlastsync_ulbl = (UILabel *)[mainView viewWithTag:1906];
        self.mlastsync_ulbl = (UILabel *)[mainView viewWithTag:1907];
        self.errormsg_ulbl = (UILabel *)[mainView viewWithTag:1908];
        
        
        
        
        
    }
    
       return self;
}

@end
