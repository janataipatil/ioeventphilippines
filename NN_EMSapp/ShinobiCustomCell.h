//
//  ShinobiCustomCell.h
//  SPM
//
//  Created by iWizardsVI on 06/01/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import <ShinobiGrids/ShinobiGrids.h>

@class ShinobiCustomCell;
@protocol ShinobiCelldelegate <NSObject>

@end
@interface ShinobiCustomCell : SDataGridCell

@property (nonatomic, assign) int checked;


@property (nonatomic, assign) int checkedstr;
@property (nonatomic, weak) id <ShinobiCelldelegate> myCheckCellDelegate;


@end
