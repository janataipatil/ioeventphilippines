//
//  ShinobiCustomCell.m
//  SPM
//
//  Created by iWizardsVI on 06/01/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import "ShinobiCustomCell.h"
#import "AppDelegate.h"
#define CHECKBOX_IMAGE_SIZE CGSizeMake(15,15)
@implementation ShinobiCustomCell

{
    UIImageView *_checkBox;
    UIImage *_onImage;
}
@synthesize checkedstr;
-(id)initWithReuseIdentifier:(NSString *)identifier
{
    if (self = [super initWithReuseIdentifier:identifier])
    {
        [self setChecked:_checked];
        
        _checkBox = [[UIImageView alloc] initWithImage:_onImage];
     
        _checkBox.frame = CGRectMake(30, 15, CHECKBOX_IMAGE_SIZE.width, CHECKBOX_IMAGE_SIZE.height);
        _checkBox.userInteractionEnabled = YES;
        [self addSubview:_checkBox];
    }
    return self;
}
-(void)setChecked:(int)checked
{
    _checked = checked;
    
    if (_checked ==1)
    {
        _onImage = [UIImage imageNamed:@"ActivityDaysToGo.png"];
        _checkBox.image = _onImage;
    }
    else if(checked == 0)
    {
        _onImage = [UIImage imageNamed:@"ActivityDaysOverdue.png"];
        
        _checkBox.image = _onImage;
    }
    else if (checked ==3)
    {
        _onImage = [UIImage imageNamed:@"ActivityStatusComplete.png"];
        
        _checkBox.image = _onImage;
    }
    else
    {
        _onImage = [UIImage imageNamed:@"ActivityDaysRemaining"];
        
        _checkBox.image = _onImage;
    }
}

@end
