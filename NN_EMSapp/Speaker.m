//
//  Speaker.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 16/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Speaker.h"

@interface Speaker ()<SDataGridDataSourceHelperDelegate>
{
    ShinobiDataGrid *shinobigrid;
    NSArray *speakerlist;
    AppDelegate *appdelegate;
    NSInteger count;
    NSArray *predicatedSessionArray;
    NSMutableArray *DistinctSpeakerList;
    
}
@property (nonatomic, strong) SDataGridDataSourceHelper *datasourceHelper;
@end

@implementation Speaker
@synthesize speakerview,helpervar,speaker_searchbar;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helpervar = [[HelperClass alloc]init];
    
    predicatedSessionArray = [helpervar query_alldata:@"Event_Speaker"];
    NSLog(@"%@",appdelegate.eventid);
    predicatedSessionArray = [predicatedSessionArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    
    DistinctSpeakerList=[[NSMutableArray alloc]init];
  
    if(predicatedSessionArray.count>0)
    {
        
        DistinctSpeakerList=[[NSMutableArray alloc]init];
        NSArray *speaker=[predicatedSessionArray valueForKeyPath:@"@distinctUnionOfObjects.speakerid"];
        for(int i=0;i<speaker.count;i++)
        {
            NSArray *predicatedList=[predicatedSessionArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"speakerid like %@",speaker[i]]];
            [DistinctSpeakerList addObject:predicatedList[0]];
        }
        
        
        speakerlist= [DistinctSpeakerList mutableCopy];
        
        speakerlist  = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        
    }

    [speaker_searchbar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [speaker_searchbar valueForKey:@"_searchField"];
    searchField.placeholder = [helpervar gettranslation:@"LBL_224"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
   searchField.textColor = [helpervar DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    
    self.speakerview.layer.borderColor=[[UIColor whiteColor]CGColor];
    [self setupshinobiview];
    
    [self langsetuptranslations];
}

-(void)langsetuptranslations
{
}
#pragma mark shinobiview
-(void)setupshinobiview
{
    [shinobigrid removeFromSuperview];
    NSArray *propertynames = [[NSMutableArray alloc] initWithObjects:@"salutation",@"firstname",@"lastname",@"speciality",@"designation",@"company",@"department",nil];
    
    NSArray *displaynames = [[NSMutableArray alloc] initWithObjects:[helpervar gettranslation:@"LBL_218"], [helpervar gettranslation:@"LBL_141"],[helpervar gettranslation:@"LBL_153"],[helpervar gettranslation:@"LBL_238"],[helpervar gettranslation:@"LBL_095"],[helpervar gettranslation:@"LBL_147"],[helpervar gettranslation:@"LBL_092"],nil];
   
    NSArray *columnlength = [[NSMutableArray alloc] initWithObjects:@"100",@"150",@"128",@"128",@"128",@"128",@"140",nil];
    
    shinobigrid = [[ShinobiDataGrid alloc] initWithFrame:CGRectInset(speakerview.bounds, 0, 0)];
    shinobigrid.layer.borderWidth=0.0;
    

   
    // shinobigrid.defaultCellStyleForHeaderRow.backgroundColor = [UIColor whiteColor];
        shinobigrid.defaultCellStyleForHeaderRow.backgroundColor = [UIColor colorWithRed:220.0/255.0f green:228.0/255.0f blue:240.0/255.0 alpha:1.0f];
    shinobigrid.defaultCellStyleForHeaderRow.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.f];
   // shinobigrid.defaultCellStyleForHeaderRow.textColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    shinobigrid.defaultCellStyleForHeaderRow.textColor = [helpervar DarkBlueColour];
    shinobigrid.defaultHeaderRowHeight = @44;
    shinobigrid.defaultRowHeight = @44;
    //shinobigrid.selectionMode = SDataGridSelectionModeRowSingle;
    shinobigrid.defaultGridLineStyle.width = 0;
    
    
    
    SDataGridCellStyle* rowStyle = [SDataGridCellStyle new];
    rowStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    rowStyle.textAlignment = NSTextAlignmentLeft;
    rowStyle.textColor = [helpervar DarkBlueColour];
    
    SDataGridCellStyle  *selectionrowStyle = [SDataGridCellStyle new];
    selectionrowStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    selectionrowStyle.textAlignment = NSTextAlignmentLeft;
    selectionrowStyle.textColor = [helpervar DarkBlueColour];
    
    shinobigrid.defaultCellStyleForSelectedRows = selectionrowStyle;
    
    for (int i=0; i<displaynames.count; i++)
    {
        NSString *name = [propertynames objectAtIndex:i];
        NSString *dname = [displaynames objectAtIndex:i];
        
        SDataGridColumn *newcolumn = [[SDataGridColumn alloc] initWithTitle:dname forProperty:name];
        newcolumn.width = [columnlength objectAtIndex:i];
        newcolumn.cellStyle = rowStyle;
        [shinobigrid addColumn:newcolumn];
        // newcolumn.canReorderViaLongPress = YES;
        //newcolumn.canResizeViaPinch = YES;
    }
    
    // Create the helper
    self.datasourceHelper = [[SDataGridDataSourceHelper alloc] initWithDataGrid:shinobigrid];
    self.datasourceHelper.delegate = self;
    self.datasourceHelper.data = speakerlist;
    
    
    [speakerview addSubview:shinobigrid];
    
    
    
}
-(void)shinobiDataGrid:(ShinobiDataGrid *)grid alterStyle:(SDataGridCellStyle *)styleToApply beforeApplyingToCellAtCoordinate:(SDataGridCoord *)coordinate
{
    
    if(coordinate.row.rowIndex % 2 == 0)
    {
        styleToApply.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
       // styleToApply.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    }
    else
    {
        styleToApply.backgroundColor = [UIColor clearColor];
        //styleToApply.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    }
    
}

-(id)dataGridDataSourceHelper:(SDataGridDataSourceHelper *)helper displayValueForProperty:(NSString *)propertyKey withSourceObject:(id)object
{
    NSString *displayvalue;
    
    if([propertyKey isEqualToString:@"speciality"])
    {
        
        if([[object valueForKey:[helpervar gettranslation:@"LBL_238"]]length]>0)
        {
            displayvalue = [object valueForKey:[helpervar gettranslation:@"LBL_238"]];
        }
        else
        {
            displayvalue = @" ";
        }
        
        return displayvalue;
        
    }
    else if ([propertyKey isEqualToString:@"designation"])
    {
        if([[object valueForKey:[helpervar gettranslation:@"LBL_095"]]length]>0)
        {
            displayvalue = [object valueForKey:[helpervar gettranslation:@"LBL_095"]];
        }
        else
        {
            displayvalue = @" ";
        }
        
        return displayvalue;
    }
    else
        return nil;
    
}
//-(id)dataGridDataSourceHelper:(SDataGridDataSourceHelper *)helper displayValueForProperty:(NSString *)propertyKey withSourceObject:(id)object
//{
//    
//    if ([propertyKey isEqualToString:@""])
//    {
//        NSString *formateddate = [helpervar formatingdate:[object valueForKey:@""]];
//        return formateddate;
//    }
//    return nil;
//}
-(void)shinobiDataGrid:(ShinobiDataGrid *)grid didSelectRow:(SDataGridRow *)row
{
    appdelegate.speakerid = [speakerlist[row.rowIndex] valueForKey:@"speakerid"];
    NSLog(@"speakerid is %@",appdelegate.speakerid);
    [self.parentViewController performSegueWithIdentifier:@"EventSpeakerToSpeaker" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark touch view method
/*-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self removepopup];
}
- (IBAction)closebtnaction:(id)sender
{
    [self removepopup];
    
}
-(void)removepopup
{
    [self performSegueWithIdentifier:@"" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}*/


- (IBAction)addspeakerClicked:(id)sender
{
    
    appdelegate.swipevar = @"Sign";
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToaddspeaker" sender:self];
}


#pragma mark unwind method
-(IBAction)unwindToAttachment:(UIStoryboardSegue *)segue
{
   
}

#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == speaker_searchbar)
    {
        [helpervar MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == speaker_searchbar)
    {
        [helpervar MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == speaker_searchbar)
    {
        if(count > searchText.length)
        {
            speakerlist = [helpervar query_alldata:@"Event_Speaker"];
            speakerlist  = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        }
        if(searchText.length == 0)
        {
            speakerlist = [helpervar query_alldata:@"Event_Speaker"];
            speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            speakerlist = [helpervar query_alldata:@"Event_Speaker"];
            speakerlist = [speakerlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.speciality contains[c] %@) OR (SELF.designation contains[c] %@) OR (SELF.company contains[c] %@) OR (SELF.department contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[speakerlist filteredArrayUsingPredicate:predicate]];
            speakerlist = [filtereventarr copy];
        }
        [self setupshinobiview];
    }
}



@end
