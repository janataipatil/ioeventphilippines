//
//  SurveyQnResultView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/8/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import <ShinobiCharts/ShinobiCharts.h>
#import <ShinobiGrids/ShinobiDataGrid.h>
#import "ToastView.h"



@interface SurveyQnResultView : UIViewController

@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UISearchBar *SurveyresultSearchBar;

@property (strong, nonatomic) IBOutlet UITableView *SurveyresultTableview;

@property (strong, nonatomic) IBOutlet UILabel *SurveyresultTitle;
@property (strong, nonatomic) IBOutlet UIView *Surveyresultchartview;

@property (strong, nonatomic) IBOutlet UIView *Surveyresulttableview;

- (IBAction)Back_btn:(id)sender;


@end
