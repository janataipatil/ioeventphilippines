//
//  SurveyQnResultView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/8/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "SurveyQnResultView.h"
#import "SurveyQuestions.h"

@interface SurveyQnResultView ()<SDataGridDataSourceHelperDelegate>
{
    ShinobiDataGrid *shinobigrid;
    NSArray *surveyqnlist;
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
    NSInteger count;
    NSArray *Userarray;
    SLKeyChainStore *keychain;
    NSPredicate *EventidPredicate;
    NSPredicate *SurveyqnidPredicate;
    
    NSString *surveyqntype;
    
    NSMutableArray *pieanswers;
    NSMutableDictionary *pieqansdata_percent;
    NSMutableDictionary *pieqansdata_values;
    
    ShinobiChart* piechart_qanswers;
    UILabel *selectedPieLabel;
}
@property (nonatomic, strong) SDataGridDataSourceHelper *datasourceHelper;

@end

@implementation SurveyQnResultView

@synthesize helper,SurveyresultSearchBar,Surveyresulttableview,Surveyresultchartview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    surveyqnlist = [helper query_alldata:@"Event_SurveyQuestion"];
 
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];// for preprod
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    EventidPredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    SurveyqnidPredicate = [NSPredicate predicateWithFormat:@"eventsurveyid like %@",appdelegate.surveyqnid];
    pieanswers = [[NSMutableArray alloc] init];
    
    [SurveyresultSearchBar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [SurveyresultSearchBar valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    

    pieqansdata_percent = [[NSMutableDictionary alloc] init];
    pieqansdata_values = [[NSMutableDictionary alloc] init];
    
    
    
    surveyqnlist = [surveyqnlist filteredArrayUsingPredicate:SurveyqnidPredicate];
    NSArray *sqnarray = [[helper query_alldata:@"Event_SurveyQuestion"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.surveyqnid]];
    
    
    surveyqntype = [sqnarray[0] valueForKey:@"surveyqn_typelic"];
    _SurveyresultTitle.text = [sqnarray[0] valueForKey:@"surveyqn_text"];
    
    [self SetupResultsView:surveyqntype ];
    [self setPieChart];
    
//    _SurveyTitle.text = [sarray[0] valueForKey:@"title"];
//    [self refreshdata];
    
        //    [self setupshinobiview];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
//    Userarray  = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)SetupResultsView:(NSString *) questiontype
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_409"]];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
     [JsonDict setValue:appdelegate.surveyqnid forKey:@"SurveyQnID"];
    
    if ([questiontype isEqualToString:@"TEXT"])
    {
        [JsonDict setValue:@"false" forKey:@"Graph"];
    }
    else
    {
        [JsonDict setValue:@"true" forKey:@"Graph"];
    }
    
    NSError *error;

    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSGetSurveyResult" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        [helper removeWaitCursor];
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        
        if ([questiontype isEqualToString:@"TEXT"])
        {
            Surveyresultchartview.hidden = YES;
            
            
            [pieanswers removeAllObjects];
            for(NSDictionary *item in jsonData)
            {
                NSMutableDictionary *answerdict = [[NSMutableDictionary alloc] init];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
                [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
                
                NSDate *sdate = [dateFormat dateFromString:[item valueForKey:@"AnsweredOn"]];
                [dateFormat setDateFormat:@"dd MMM yyyy HH:mm"];

                
                
                [answerdict setValue:[item valueForKey:@"Answer"] forKey:@"Answer"];
                [answerdict setValue:[dateFormat stringFromDate:sdate] forKey:@"AnsweredOn"];
                [answerdict setValue:[item valueForKey:@"AttendeeName"] forKey:@"AttendeeName"];
                [answerdict setValue:[item valueForKey:@"SurveyQnID"] forKey:@"SurveyQnID"];

                [pieanswers addObject:answerdict];
            }

        }
        else
        {
            [pieqansdata_percent removeAllObjects];
            [pieqansdata_values removeAllObjects];
            
//            NSDictionary *Answerlistdata = [jsonData valueForKey:@"ListofAnswers"];
            for(NSDictionary *item in jsonData)
            {
                NSString *Label = [item valueForKey:@"OptionText"];
                int value = [[item valueForKey:@"Count"] intValue];
                int percentage = [[item valueForKey:@"Percentage"] intValue];
                
                [pieqansdata_percent setValue:[NSNumber numberWithFloat:percentage] forKey:Label];
                [pieqansdata_values setValue:[NSNumber numberWithFloat:value]  forKey:Label];
                
                [pieanswers addObject:item];
            }
            
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"Sequence" ascending:YES];
            [pieanswers sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
        }
        
    }
    [self setupshinobiview];
    [helper removeWaitCursor];

}

- (IBAction)Back_btn:(id)sender
{
    SurveyQuestions *vc = [[SurveyQuestions alloc]initWithNibName:@"SurveyQsnlist" bundle:nil];
    
    
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self willMoveToParentViewController:vc];
    
    
    [UIView animateWithDuration:0.5 animations:^{
    [self.view setFrame:CGRectMake(1200, 0, self.view.frame.size.width, 700)];
    }
    completion:^(BOOL finished){[self.view removeFromSuperview];
    [self removeFromParentViewController];}];
}

-(void) setPieChart
{
    [piechart_qanswers removeFromSuperview];
    
    CGFloat margin_pie = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? 0.0 : 5.0;
    piechart_qanswers = [[ShinobiChart alloc] initWithFrame:CGRectInset(self.Surveyresultchartview.bounds, margin_pie, margin_pie)];
    piechart_qanswers.autoresizingMask =  ~UIViewAutoresizingNone;
    
    piechart_qanswers.datasource = self;
    piechart_qanswers.delegate = self;
    
    piechart_qanswers.legend.hidden = NO;
    piechart_qanswers.legend.backgroundColor = [UIColor whiteColor];
    piechart_qanswers.legend.placement = SChartLegendPlacementInsidePlotArea;
    piechart_qanswers.legend.position = SChartLegendPositionBottomMiddle;
    piechart_qanswers.legend.style.font = [UIFont fontWithName:@"HelveticaNeue" size:14.f];
    piechart_qanswers.legend.style.fontColor = [UIColor blackColor];
    
    selectedPieLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 240, 120, 50)];
    selectedPieLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.f];
    selectedPieLabel.textColor = [UIColor colorWithRed:44/255.0 green:83/255.0 blue:153/255.0 alpha:1];
    selectedPieLabel.textAlignment =  NSTextAlignmentCenter;
    selectedPieLabel.backgroundColor = [UIColor clearColor];
    selectedPieLabel.text = @"";
    selectedPieLabel.numberOfLines = @3;
    selectedPieLabel.adjustsFontSizeToFitWidth = NO;
    
    [self.Surveyresultchartview  addSubview:piechart_qanswers];
    [piechart_qanswers addSubview:selectedPieLabel];
    
}



-(NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart
{
    return 1;
}
-(NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    return pieqansdata_percent.allKeys.count;
    
}
-(id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex
{
    SChartRadialDataPoint *datapoint = [[SChartRadialDataPoint alloc] init];
    NSString* key = pieqansdata_percent.allKeys[dataIndex];
    datapoint.name = key;
    datapoint.value =  [pieqansdata_percent valueForKey:key];  //piedata_percent[key];
    
    return datapoint;

}

-(SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index
{
    SChartDonutSeries *pieSeries = [[SChartDonutSeries alloc] init];
    
    //pieSeries.style.chartEffect = SChartRadialChartEffectRoundedLight;
    pieSeries.style.dataPointLabelStyle.textColor = [UIColor blackColor];
    pieSeries.style.dataPointLabelStyle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];
    
    //pieSeries.selectedStyle.chartEffect = SChartRadialChartEffectRounded;
    
    pieSeries.selectedStyle.protrusion = 15.0f;
    pieSeries.selectionAnimation.duration = @1.5;
    pieSeries.selectedPosition = @0.0;
    
    pieSeries.style.labelFontColor = [UIColor blackColor];
    pieSeries.selectedStyle.labelFontColor = [UIColor blackColor];
    
    pieSeries.animationEnabled = YES;
    UIColor *col1 = [UIColor colorWithRed:60/255.0 green:186/255.0 blue:84/255.0 alpha:1];
    UIColor *col2 = [UIColor colorWithRed:72/255.0 green:133/255.0 blue:237/255.0 alpha:1];
    UIColor *col3 = [UIColor colorWithRed:249/255.0 green:167/255.0 blue:70/255.0 alpha:1];
    UIColor *col4 = [UIColor colorWithRed:219/255.0 green:50/255.0 blue:54/255.0 alpha:1];
    UIColor *col5 = [UIColor colorWithRed:244/255.0 green:194/255.0 blue:13/255.0 alpha:1];
    UIColor *col6 = [UIColor colorWithRed:44/255.0 green:114/255.0 blue:22/255.0 alpha:1];
    UIColor *col7 = [UIColor colorWithRed:209/255.0 green:56/255.0 blue:50/255.0 alpha:1];
    
    UIColor *scol1 = [UIColor colorWithRed:60/255.0 green:186/255.0 blue:84/255.0 alpha:0.8];
    UIColor *scol2 = [UIColor colorWithRed:72/255.0 green:133/255.0 blue:237/255.0 alpha:0.8];
    UIColor *scol3 = [UIColor colorWithRed:249/255.0 green:167/255.0 blue:70/255.0 alpha:0.8];
    UIColor *scol4 = [UIColor colorWithRed:219/255.0 green:50/255.0 blue:54/255.0 alpha:0.8];
    UIColor *scol5 = [UIColor colorWithRed:244/255.0 green:194/255.0 blue:13/255.0 alpha:0.8];
    UIColor *scol6 = [UIColor colorWithRed:44/255.0 green:114/255.0 blue:22/255.0 alpha:0.8];
    UIColor *scol7 = [UIColor colorWithRed:209/255.0 green:56/255.0 blue:50/255.0 alpha:0.8];
    
    
    [[pieSeries style] setFlavourColors:[NSMutableArray arrayWithObjects: col1,col2,col3,col4,col5,col6,col7, nil]];
    [[pieSeries selectedStyle] setFlavourColors:[NSMutableArray arrayWithObjects: scol1,scol2,scol3,scol4,scol5,scol6,scol7, nil]];
    
    
    
    
    SChartAnimation *animation = [SChartAnimation growAnimation];
    animation.duration = @3;
    pieSeries.entryAnimation = animation;
    
    
    return pieSeries;
}

- (void)sChart:(ShinobiChart *)chart toggledSelectionForRadialPoint:(SChartRadialDataPoint *)dataPoint inSeries:(SChartRadialSeries *)series atPixelCoordinate:(CGPoint)pixelPoint
{
    
    NSLog(@"Time period Clicked: %@", dataPoint.name);
    NSString *dvalue = [pieqansdata_values valueForKey:dataPoint.name];
    NSLog(@"Time period Clicked: %@", dvalue);
    selectedPieLabel.text = [NSString stringWithFormat:@"%@\n%@",dataPoint.name,dvalue];
}

#pragma mark shinobiview
-(void)setupshinobiview
{
    NSArray *propertynames = [[NSArray alloc] init];
    NSArray *displaynames = [[NSArray alloc] init];
    NSArray *columnlength = [[NSArray alloc] init];

    if ([surveyqntype isEqualToString:@"TEXT"])
    {
        [Surveyresulttableview setFrame:CGRectMake(8, 93, 864, 526)];
        propertynames = [[NSMutableArray alloc] initWithObjects:@"Answer",@"AttendeeName",@"AnsweredOn",nil];
        displaynames = [[NSMutableArray alloc] initWithObjects: @"Response",@"Response by",@"Response On",nil];
        columnlength = [[NSMutableArray alloc] initWithObjects:@"480",@"200",@"200",nil];
   
    }
    else
    {
        // NSArray *propertynames;
        propertynames = [[NSMutableArray alloc] initWithObjects:@"OptionText",@"Count",@"Percentage",nil];
        displaynames = [[NSMutableArray alloc] initWithObjects: @"Available Answers",@"Count",@"Percentage",nil];
        columnlength = [[NSMutableArray alloc] initWithObjects:@"240",@"80",@"120",nil];
      
    }
    
    
    shinobigrid = [[ShinobiDataGrid alloc] initWithFrame:CGRectInset(Surveyresulttableview.bounds, 0, 0)];
    // shinobigrid.defaultCellStyleForHeaderRow.backgroundColor = [UIColor whiteColor];
    shinobigrid.defaultCellStyleForHeaderRow.backgroundColor = [UIColor colorWithRed:220.0/255.0f green:228.0/255.0f blue:240.0/255.0 alpha:1.0f];
    shinobigrid.defaultCellStyleForHeaderRow.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.f];
   // shinobigrid.defaultCellStyleForHeaderRow.textColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    shinobigrid.defaultCellStyleForHeaderRow.textColor = [helper DarkBlueColour];
    
    shinobigrid.defaultCellStyleForHeaderRow.textAlignment = NSTextAlignmentLeft;
    shinobigrid.defaultHeaderRowHeight = @44;
    shinobigrid.defaultRowHeight = @44;
    //shinobigrid.selectionMode = SDataGridSelectionModeRowSingle;
    shinobigrid.defaultGridLineStyle.width = 0;
    
    SDataGridCellStyle* rowStyle = [SDataGridCellStyle new];
    rowStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    rowStyle.textAlignment = NSTextAlignmentNatural;
    rowStyle.textColor = [helper DarkBlueColour];
    
    SDataGridCellStyle* selectionStyle = [SDataGridCellStyle new];
    selectionStyle.font = [UIFont fontWithName:@"Helvetica"size:12.0f];
    selectionStyle.textAlignment = NSTextAlignmentNatural;
    selectionStyle.backgroundColor = [UIColor colorWithRed:51/255.0f green:76/255.0f blue:104/255.0f alpha:1.0f];
    selectionStyle.textColor = [helper DarkBlueColour];
    shinobigrid.defaultCellStyleForSelectedRows = selectionStyle;

    
    for (int i=0; i<displaynames.count; i++)
    {
        NSString *name = [propertynames objectAtIndex:i];
        NSString *dname = [displaynames objectAtIndex:i];
        
        SDataGridColumn *newcolumn = [[SDataGridColumn alloc] initWithTitle:dname forProperty:name];
        newcolumn.width = [columnlength objectAtIndex:i];
        newcolumn.cellStyle = rowStyle;
        [shinobigrid addColumn:newcolumn];
        // newcolumn.canReorderViaLongPress = YES;
        //newcolumn.canResizeViaPinch = YES;
    }
    
    // Create the helper
    self.datasourceHelper = [[SDataGridDataSourceHelper alloc] initWithDataGrid:shinobigrid];
    self.datasourceHelper.delegate = self;
    self.datasourceHelper.data = pieanswers;
    
    [Surveyresulttableview addSubview:shinobigrid];
    
    
    
}

-(void)shinobiDataGrid:(ShinobiDataGrid *)grid alterStyle:(SDataGridCellStyle *)styleToApply beforeApplyingToCellAtCoordinate:(SDataGridCoord *)coordinate
{
    
    if(coordinate.row.rowIndex % 2 == 0)
    {
        styleToApply.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
        //styleToApply.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    }
    else
    {
        styleToApply.backgroundColor = [UIColor clearColor];
       // styleToApply.textColor = [UIColor colorWithRed:51.0/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    }
    
}
@end
