//
//  SurveyQuestions.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/8/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"


@interface SurveyQuestions : UIViewController

@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UISearchBar *SurveyQnlistSearchBarOutlet;

@property (strong, nonatomic) IBOutlet UITableView *SurveyQnlistTableview;

@property (strong, nonatomic) IBOutlet UILabel *SurveyTitle;
- (IBAction)Back_btn:(id)sender;


@end
