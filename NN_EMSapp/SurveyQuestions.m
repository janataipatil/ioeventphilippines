//
//  SurveyQuestions.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/8/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "SurveyQuestions.h"
#import "SurveyView.h"
#import "SurveyQnResultView.h"


@interface SurveyQuestions ()
{
    NSArray *surveyqnlist;
    AppDelegate *appdelegate;
    NSInteger count;
    NSArray *Userarray;
    SLKeyChainStore *keychain;
    NSPredicate *EventidPredicate;
    NSPredicate *SurveyidPredicate;
    SurveyQnResultView *SurveyQsnresult;
    
    NSSortDescriptor *sortdescriptor;
    
}

@end

@implementation SurveyQuestions

@synthesize helper,SurveyQnlistSearchBarOutlet,SurveyQnlistTableview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    surveyqnlist = [helper query_alldata:@"Event_SurveyQuestion"];
  
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];// for preprod
    
    EventidPredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    SurveyidPredicate = [NSPredicate predicateWithFormat:@"eventsurveyid like %@",appdelegate.surveyid];
    
    surveyqnlist = [surveyqnlist filteredArrayUsingPredicate:SurveyidPredicate];
     NSArray *sarray = [[helper query_alldata:@"Event_Survey"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.surveyid]];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    _SurveyTitle.text = [sarray[0] valueForKey:@"title"];
    [self refreshdata];
    
    [SurveyQnlistSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [SurveyQnlistSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    
    //    [self setupshinobiview];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    Userarray  = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    

}

-(void)refreshdata
{
    surveyqnlist = [helper query_alldata:@"Event_SurveyQuestion"];
    surveyqnlist = [surveyqnlist filteredArrayUsingPredicate:SurveyidPredicate];
    
    sortdescriptor = [[NSSortDescriptor alloc]initWithKey:@"surveyqn_sequence" ascending:YES];
    surveyqnlist = [surveyqnlist sortedArrayUsingDescriptors:@[sortdescriptor]];
    [SurveyQnlistTableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (IBAction)AddTeamButtonAction:(id)sender
//{
//    appdelegate.approvalid = @"";
//    [self.parentViewController performSegueWithIdentifier:@"EventDetailToupsertapprovals" sender:self];
//}
#pragma mark search bar delegate method
//-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
//{
//    //NSLog(@"yes called");
//    if(searchBar == SurveylistSearchBarOutlet)
//    {
//        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
//    }
//    return YES;
//}
//-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
//{
//    if(searchBar == SurveylistSearchBarOutlet)
//    {
//        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
//    }
//    return YES;
//}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == SurveyQnlistSearchBarOutlet)
    {
        if(count > searchText.length)
        {
            surveyqnlist = [helper query_alldata:@"Event_SurveyQuestion"];
            surveyqnlist = [surveyqnlist filteredArrayUsingPredicate:SurveyidPredicate];
        }
        if(searchText.length == 0)
        {
            surveyqnlist = [helper query_alldata:@"Event_SurveyQuestion"];
            surveyqnlist = [surveyqnlist filteredArrayUsingPredicate:SurveyidPredicate];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            surveyqnlist = [helper query_alldata:@"Event_SurveyQuestion"];
            surveyqnlist = [surveyqnlist filteredArrayUsingPredicate:SurveyidPredicate];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.surveyqn_text contains[c] %@)",searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[surveyqnlist filteredArrayUsingPredicate:predicate]];
            surveyqnlist = [filtereventarr copy];
        }
        [SurveyQnlistTableview reloadData];
    }
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [surveyqnlist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    
    //    NSPredicate *userpred = [NSPredicate predicateWithFormat:@"username like %@",[approvallist[indexPath.row] valueForKey:@"approverid"]];
    //    NSString *username = [[Userarray filteredArrayUsingPredicate:userpred] valueForKey:@"username"];
    //    NSString *department = [[Userarray filteredArrayUsingPredicate:userpred] valueForKey:@"username"];
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    UILabel *surveyqn_text = (UILabel *)[contentView viewWithTag:1];
    
    NSString *question = [NSString stringWithFormat:@" Question %@ : %@ ",[surveyqnlist[indexPath.row] valueForKey:@"surveyqn_sequence"],[surveyqnlist[indexPath.row] valueForKey:@"surveyqn_text"]];
    surveyqn_text.text = question;

    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//        NSPredicate *userpred = [NSPredicate predicateWithFormat:@"id like %@",[surveyqnlist[indexPath.row] valueForKey:@"id"]];
//        
//        //        [helper serverdelete:@"E_APPROVALS" entityid:[approvallist[indexPath.row] valueForKey:@"id"]];
//        //        NSLog(@"APPROVALS id is %@",[approvallist[indexPath.row] valueForKey:@"id"]);
//        //        [helper delete_predicate:@"Event_Approvals" predicate:userpred];
//        //        [self refreshdata];
//}
//-(IBAction)EditButtonAction:(id)sender
//{
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:SurveylistTableview];
//    NSIndexPath *indexPath = [SurveylistTableview indexPathForRowAtPoint:buttonPosition];
//    appdelegate.surveyid = [surveylist[indexPath.row] valueForKey:@"id"];
//
//
//    [self performSegueWithIdentifier:@"SurveylisttoSurveyQn" sender:self];
//}
#pragma mark unwind method

-(IBAction)unwindtoApprovallist:(UIStoryboardSegue *)segue
{
    //    approvallist = [helper query_alldata:@"Event_Approvals"];
    //    approvallist = [approvallist filteredArrayUsingPredicate:EventidPredicate];
    //    [ApprovalTableview reloadData];
    //
//    [self refreshdata];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    NSString *networkstatus = [helper checkinternetconnection];
    
    if ([networkstatus isEqualToString:@"Y"])
    {
        appdelegate.surveyqnid = [surveyqnlist[indexPath.row] valueForKey:@"id"];
        
        SurveyQsnresult = [self.storyboard instantiateViewControllerWithIdentifier:@"SurveyQnresults"];
        [SurveyQsnresult.view setFrame:CGRectMake(1200, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
        [self addChildViewController:SurveyQsnresult];
        [self.view addSubview:SurveyQsnresult.view];
        [SurveyQsnresult didMoveToParentViewController:self];
        
        [UIView animateWithDuration:0.8 animations:^{
            [SurveyQsnresult.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        }];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_441"] message:@"Active internet connection is needed to view the latest survey results. \nPlease try again later " action:[helper gettranslation:@"LBL_462"]];
    }  
}

- (IBAction)Back_btn:(id)sender
{
    SurveyView *vc = [[SurveyView alloc]initWithNibName:@"SurveyView" bundle:nil];
    
    
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self willMoveToParentViewController:vc];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        //[self.view setFrame:CGRectMake(1200, 70, self.view.frame.size.width, self.view.frame.size.height)];
        
        [self.view setFrame:CGRectMake(1200, 0, self.view.frame.size.width, 700)];
    }
                     completion:^(BOOL finished){[self.view removeFromSuperview];
                         [self removeFromParentViewController];}];
}
@end
