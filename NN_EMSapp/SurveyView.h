//
//  SurveyView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/7/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"

@interface SurveyView : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)HelperClass *helper;

@property (strong, nonatomic) IBOutlet UISearchBar *SurveylistSearchBarOutlet;

@property (strong, nonatomic) IBOutlet UITableView *SurveylistTableview;
@property (strong, nonatomic) IBOutlet UIButton *AddSurveyButtonOutlet;
- (IBAction)addsurvey:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *templatename_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *description_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *noofquestion_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *surveystdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *surveyedate_ulbl;





@end
