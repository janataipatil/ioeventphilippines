//
//  SurveyView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/7/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "SurveyView.h"
#import "SurveyQuestions.h"

@interface SurveyView ()
{
    NSArray *surveylist;
    AppDelegate *appdelegate;
    NSInteger count;
    NSArray *Userarray;
    SLKeyChainStore *keychain;
    NSPredicate *EventidPredicate;
    SurveyQuestions *SurveyQsnlist;
    int RefreshTableview;
}

@end

@implementation SurveyView
@synthesize helper,SurveylistSearchBarOutlet,SurveylistTableview,AddSurveyButtonOutlet;


- (void)viewDidLoad
{
    [super viewDidLoad];
    RefreshTableview = 0;
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    surveylist = [helper query_alldata:@"Event_Survey"];
   
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    EventidPredicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
    surveylist = [surveylist filteredArrayUsingPredicate:EventidPredicate];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [self refreshdata];
    
    [SurveylistSearchBarOutlet setBackgroundImage:[UIImage new]];
    UITextField *searchField = [SurveylistSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    
    //    [self setupshinobiview];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    Userarray  = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    
    if (![appdelegate.EventUserRoleArray containsObject:@"E_SRV_MNG"])
    {
        AddSurveyButtonOutlet.hidden = YES;
    }
    
    [self langsetuptranslations];
}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshTableview == 1)
    {
        SurveylistTableview.delegate = self;
        SurveylistTableview.dataSource = self;
        [SurveylistTableview reloadData];
    }
}
-(void)langsetuptranslations
{
    _templatename_ulbl.text = [helper gettranslation:@"LBL_258"];
    _description_ulbl.text = [helper gettranslation:@"LBL_094"];
    _noofquestion_ulbl.text = [helper gettranslation:@"LBL_181"];
    _surveystdate_ulbl.text = [helper gettranslation:@"LBL_239"];
    _surveyedate_ulbl.text = [helper gettranslation:@"LBL_122"];
    
}


-(void)refreshdata
{
    surveylist = [helper query_alldata:@"Event_Survey"];
    surveylist = [surveylist filteredArrayUsingPredicate:EventidPredicate];
    SurveylistTableview.delegate = self;
    SurveylistTableview.dataSource = self;
    [SurveylistTableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (IBAction)AddTeamButtonAction:(id)sender
//{
//    appdelegate.approvalid = @"";
//    [self.parentViewController performSegueWithIdentifier:@"EventDetailToupsertapprovals" sender:self];
//}
#pragma mark search bar delegate method
//-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
//{
//    //NSLog(@"yes called");
//    if(searchBar == SurveylistSearchBarOutlet)
//    {
//        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
//    }
//    return YES;
//}
//-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
//{
//    if(searchBar == SurveylistSearchBarOutlet)
//    {
//        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
//    }
//    return YES;
//}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == SurveylistSearchBarOutlet)
    {
        if(count > searchText.length)
        {
            surveylist = [helper query_alldata:@"Event_Survey"];
            surveylist = [surveylist filteredArrayUsingPredicate:EventidPredicate];
        }
        if(searchText.length == 0)
        {
            surveylist = [helper query_alldata:@"Event_Survey"];
            surveylist = [surveylist filteredArrayUsingPredicate:EventidPredicate];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            surveylist = [helper query_alldata:@"Event_Survey"];
            surveylist = [surveylist filteredArrayUsingPredicate:EventidPredicate];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.survey_desc contains[c] %@) OR (SELF.survey_enddate contains[c] %@) OR (SELF.survey_startdate contains[c] %@) OR (SELF.title contains[c] %@)",searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[surveylist filteredArrayUsingPredicate:predicate]];
            surveylist = [filtereventarr copy];
        }
        [SurveylistTableview reloadData];
       }
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [surveylist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    UILabel *title = (UILabel *)[contentView viewWithTag:1];
    UILabel *survey_desc = (UILabel *)[contentView viewWithTag:2];
    UILabel *survey_startdate = (UILabel *)[contentView viewWithTag:3];
    UILabel *survey_enddate = (UILabel *)[contentView viewWithTag:4];
    UILabel *survey_nofqn = (UILabel *)[contentView viewWithTag:5];
    UIButton *EditButtonOutlet = (UIButton *)[contentView viewWithTag:8];
    
    
    /*NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSDate *sdate = [dateFormatter dateFromString:[surveylist[indexPath.row] valueForKey:@"survey_startdate"]];
    NSDate *edate = [dateFormatter dateFromString:[surveylist[indexPath.row] valueForKey:@"survey_enddate"]];
    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];*/
    
    
    
    [EditButtonOutlet addTarget:self action:@selector(EditButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    title.text = [surveylist[indexPath.row] valueForKey:@"title"];
    survey_desc.text = [surveylist[indexPath.row] valueForKey:@"survey_desc"];
    //survey_startdate.text = [dateFormatter stringFromDate:sdate];
    //survey_enddate.text = [dateFormatter stringFromDate:edate];
    
    survey_startdate.text = [helper formatingdate:[surveylist[indexPath.row] valueForKey:@"survey_startdate"] datetime:@"FormatDate"];
    survey_enddate.text = [helper formatingdate:[surveylist[indexPath.row] valueForKey:@"survey_enddate"] datetime:@"FormatDate"];
    
    survey_nofqn.text = [surveylist[indexPath.row] valueForKey:@"question_count"];
    if ([appdelegate.EventUserRoleArray containsObject:@"E_SRV_MNG"])
    {
        [EditButtonOutlet setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    }
    else
    {
         [EditButtonOutlet setImage:[UIImage imageNamed:@"Visible.png"] forState:UIControlStateNormal];
    }
    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appdelegate.EventUserRoleArray containsObject:@"E_SRV_MNG"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSPredicate *surveyqnpred = [NSPredicate predicateWithFormat:@"eventsurveyid like %@",[surveylist[indexPath.row] valueForKey:@"id"]];
    NSPredicate *surveypred = [NSPredicate predicateWithFormat:@"id like %@",[surveylist[indexPath.row] valueForKey:@"id"]];
    
    [helper serverdelete:@"E_SRV_TEMPLATE" entityid:[surveylist[indexPath.row] valueForKey:@"id"]];
    
    [helper delete_predicate:@"Event_Survey" predicate:surveypred];
    [helper delete_predicate:@"Event_SurveyQuestion" predicate:surveyqnpred];
    
    [self refreshdata];
}
-(IBAction)EditButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:SurveylistTableview];
    NSIndexPath *indexPath = [SurveylistTableview indexPathForRowAtPoint:buttonPosition];
    appdelegate.surveyid = [surveylist[indexPath.row] valueForKey:@"id"];
    
    [self.parentViewController performSegueWithIdentifier:@"EventDetailstoAddSurvey" sender:self];
}
#pragma mark unwind method

-(IBAction)unwindtoSurveylist:(UIStoryboardSegue *)segue
{
    //    approvallist = [helper query_alldata:@"Event_Approvals"];
    //    approvallist = [approvallist filteredArrayUsingPredicate:EventidPredicate];
    //    [ApprovalTableview reloadData];
    //
    RefreshTableview = 1;
    [self refreshdata];
}



- (IBAction)addsurvey:(id)sender
{
    
    NSString *networkstatus = [helper checkinternetconnection];
    
    if ([networkstatus isEqualToString:@"Y"])
    {
        appdelegate.surveyid = @"";
        [self.parentViewController performSegueWithIdentifier:@"EventDetailstoAddSurvey" sender:self];
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_441"] message:[helper gettranslation:@"LBL_630"] action:[helper gettranslation:@"LBL_462"]];
    }
    
    
 
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    appdelegate.surveyid = [surveylist[indexPath.row] valueForKey:@"id"];
    
    SurveyQsnlist = [self.storyboard instantiateViewControllerWithIdentifier:@"SurveyQsnlist"];
    [SurveyQsnlist.view setFrame:CGRectMake(1200, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self addChildViewController:SurveyQsnlist];
    [self.view addSubview:SurveyQsnlist.view];
    [SurveyQsnlist didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.8 animations:^{
        [SurveyQsnlist.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
    
//    [self performSegueWithIdentifier:@"SurveylisttoSurveyQn" sender:self];
    
    
    
}

@end
