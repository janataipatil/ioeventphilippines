//
//  Team.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 15/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ShinobiGrids/ShinobiDataGrid.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "EventDetails_ViewController.h"

@interface Team : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UISearchBar *TeamSearchBarOutlet;
@property (strong, nonatomic) IBOutlet UITableView *TeamTableview;

@property (strong, nonatomic) IBOutlet UIButton *AddUserButtonOutlet;

- (IBAction)AddTeamButtonAction:(id)sender;

@property(strong,nonatomic) IBOutlet UILabel *admin_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *userid_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *fiestname_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *lastname_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *role_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *email_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *status_ulbl;


@end
