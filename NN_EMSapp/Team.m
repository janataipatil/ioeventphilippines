//
//  Team.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 15/09/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "Team.h"

@interface Team ()<SDataGridDataSourceHelperDelegate>
{
    ShinobiDataGrid *shinobigrid;
    NSArray *Teamlist;
    AppDelegate *appdelegate;
    NSInteger count;
    int RefreshTableview;
    NSArray *EventData;
    dispatch_semaphore_t sem_event;
    
    
}
@property (nonatomic, strong) SDataGridDataSourceHelper *datasourceHelper;
@end

@implementation Team
@synthesize helper,TeamSearchBarOutlet,TeamTableview,AddUserButtonOutlet;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    @autoreleasepool {
        
    
                        appdelegate.translations = [helper query_alldata:@"Lang_translations"];
                        [self refreshdata];
                        [TeamSearchBarOutlet setBackgroundImage:[UIImage new]];
    
                        EventData=[[helper query_alldata:@"Event"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
                        UITextField *searchField = [TeamSearchBarOutlet valueForKey:@"_searchField"];
                        searchField.placeholder = [helper gettranslation:@"LBL_224"];
                        // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
                        searchField.textColor = [helper DarkBlueColour];
                        searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    
                            if ([[helper EventCanEditFlag:@"canEdit"] isEqualToString:@"N"])
                                {
                                        AddUserButtonOutlet.hidden = YES;
                                }
    
                                    [self langsetuptranslations];
    }
  
    
}
-(void)viewDidAppear:(BOOL)animated
{
    @autoreleasepool {
                    if (RefreshTableview == 1)
                    {
                        TeamTableview.delegate = self;
                        TeamTableview.dataSource = self;
                        [TeamTableview reloadData];
                    }
    }
}
-(void)langsetuptranslations
{
    _admin_ulbl.text = [helper gettranslation:@"LBL_308"];
    _userid_ulbl.text = [helper gettranslation:@"LBL_175"];
    _fiestname_ulbl.text = [helper gettranslation:@"LBL_268"];
    _lastname_ulbl.text = [helper gettranslation:@"LBL_266"];
    _role_ulbl.text = [helper gettranslation:@"LBL_214"];
    _email_ulbl.text = [helper gettranslation:@"LBL_119"];
    _status_ulbl.text = [helper gettranslation:@"LBL_242"];

}

-(void)refreshdata
{
    @autoreleasepool {
                        Teamlist = [helper query_alldata:@"Event_Team"];
                        Teamlist = [Teamlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
                        TeamTableview.delegate = self;
                        TeamTableview.dataSource = self;
                        [TeamTableview reloadData];
                    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
 
 
}
*/
- (IBAction)AddTeamButtonAction:(id)sender
{
    [self.parentViewController performSegueWithIdentifier:@"EventDetailToAddMemberInTeam" sender:self];
}
#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == TeamSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == TeamSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == TeamSearchBarOutlet)
    {
        if(count > searchText.length)
        {
            Teamlist = [helper query_alldata:@"Event_Team"];
            Teamlist = [Teamlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        }
        if(searchText.length == 0)
        {
            Teamlist = [helper query_alldata:@"Event_Team"];
            Teamlist = [Teamlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            Teamlist = [helper query_alldata:@"Event_Team"];
            Teamlist = [Teamlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.teamName contains[c] %@) OR (SELF.username contains[c] %@) OR (SELF.type contains[c] %@) OR (SELF.role contains[c] %@) OR (SELF.email contains[c] %@) OR (SELF.status contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[Teamlist filteredArrayUsingPredicate:predicate]];
            Teamlist = [filtereventarr copy];
        }
        
        [TeamTableview reloadData];
    }
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [Teamlist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *Namelbl = (UILabel *)[contentView viewWithTag:2];
    UILabel *UserIdlbl = (UILabel *)[contentView viewWithTag:1];
    UILabel *Type = (UILabel *)[contentView viewWithTag:3];
    UILabel *Rolelbl = (UILabel *)[contentView viewWithTag:4];
    UILabel *Emaillbl = (UILabel *)[contentView viewWithTag:5];
    UILabel *Statuslbl = (UILabel *)[contentView viewWithTag:6];
    UIButton *EditButtonOutlet = (UIButton *)[contentView viewWithTag:7];
    UISwitch *adminSwitch = (UISwitch *)[contentView viewWithTag:8];
    adminSwitch.transform = CGAffineTransformMakeScale(0.60, 0.60);
   // adminSwitch.onTintColor = [UIColor colorWithRed:52/255.0F green:73/255.0F blue:94/255.0F alpha:1.0F];
    
    adminSwitch.onTintColor = [helper DarkBlueColour];
    
    [EditButtonOutlet addTarget:self action:@selector(EditButtonAction:) forControlEvents:UIControlEventTouchUpInside];
   
    UserIdlbl.text = [Teamlist[indexPath.row] valueForKey:@"teamName"];
    
    Namelbl.text= [Teamlist[indexPath.row] valueForKey:@"username"];
    Type.text = [Teamlist[indexPath.row] valueForKey:@"type"];
    Rolelbl.text = [Teamlist[indexPath.row] valueForKey:@"role"];
    Emaillbl.text = [Teamlist[indexPath.row] valueForKey:@"email"];
    Statuslbl.text = [Teamlist[indexPath.row] valueForKey:@"status"];
    
    NSString *status = [Teamlist[indexPath.row] valueForKey:@"statuslic"];
    if ([status isEqualToString:@"Active"])
    {
        [adminSwitch setOn:YES];
    }
    else
    {
        [adminSwitch setOn:NO];
    }
    
    [adminSwitch addTarget:self action:@selector(switchIsChanged:)forControlEvents:UIControlEventValueChanged];
    
    NSString *admindelegateflg = [Teamlist[indexPath.row] valueForKey:@"admindelegate"];
    if ([admindelegateflg isEqualToString:@"Y"])
    {
        [EditButtonOutlet setImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
    }
    else
    {
        [EditButtonOutlet setImage:[UIImage imageNamed:@"unCheckbox.png"] forState:UIControlStateNormal];
    }
    NSString *canedit = [EventData[0] valueForKey:@"canEdit"];
    
    if ([canedit isEqualToString:@"N"])
    {
        EditButtonOutlet.userInteractionEnabled = NO;
        adminSwitch.userInteractionEnabled = NO;
    }
    
    
    
    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[helper EventCanEditFlag:@"canDelete"] isEqualToString:@"Y"])
    {
        return YES;
    }
    else if ([[helper EventCanEditFlag:@"canDelete"] isEqualToString:@"Y"])
    {
        return NO;
    }
    else
        return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TeamTableview)
    {
        [helper serverdelete:@"E_TEAM" entityid:[Teamlist[indexPath.row] valueForKey:@"id"]];
        NSLog(@"team id is %@",[Teamlist[indexPath.row] valueForKey:@"id"]);
        [helper delete_TeamMember:@"Event_Team" teamid:[Teamlist[indexPath.row] valueForKey:@"id"] eventid:[Teamlist[indexPath.row] valueForKey:@"eventid"]];
        [self refreshdata];
    }
}
-(IBAction)EditButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TeamTableview];
    NSIndexPath *indexPath = [TeamTableview indexPathForRowAtPoint:buttonPosition];
    appdelegate.userid = [Teamlist[indexPath.row] valueForKey:@"teamid"];

    
    [self syncresponsewithserver:indexPath callfor:@"admindelegate"];
    
    
    
    
//    [self.parentViewController performSegueWithIdentifier:@"EventDetailToUserRole" sender:self];
}
#pragma mark unwind method
-(IBAction)unwindToEventTeam:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
   // NSLog(@"Event team function is called");
    [self refreshdata];
}

-(void) syncresponsewithserver:(NSIndexPath *) indexPath callfor:(NSString *)callfor
{
    
    NSString *admindelflg = [Teamlist[indexPath.row] valueForKey:@"admindelegate"];
     NSString *statuslic = [Teamlist[indexPath.row] valueForKey:@"status"];
    

    if ([callfor isEqualToString:@"admindelegate"])
    {
        if ([admindelflg isEqualToString:@"N"])
        {
            admindelflg = @"Y";
        }
        else
        {
            admindelflg = @"N";
        }
    }
    else if ([callfor isEqualToString:@"status"])
    {
        if ([statuslic isEqualToString:@"Active"])
        {
            statuslic = @" Inactive ";
        }
        else
        {
            statuslic = @"Active";
        }
    }
        

    NSString *status = [Teamlist[indexPath.row] valueForKey:@"status"];
    NSString *userid = [Teamlist[indexPath.row] valueForKey:@"teamid"];
    NSString *recordid = [Teamlist[indexPath.row] valueForKey:@"id"];
    
    
    NSError *error;
//    [helper showWaitCursor:@"Updating Team..."];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
    [JsonDict setValue:userid forKey:@"TeamID"];
    [JsonDict setValue:statuslic forKey:@"StatusLIC"];
    [JsonDict setValue:status forKey:@"Status"];
    [JsonDict setValue:[helper stringtobool:admindelflg ] forKey:@"AdminDelegate"];
    
    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArray options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
        
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventTeam" pageno:@"" pagecount:@"" lastpage:@""];
    
    appdelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"EventTeam" entityname:@"Event Team"  Status:@"Open"];
    
//    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
//    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
//    {
//        [helper removeWaitCursor];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
//        
//        [alert show];
//    }
//    else
//    {
//        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
//        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
    
        
        NSManagedObjectContext  *context = [appdelegate managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_Team" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.id like %@",recordid];
        
//        NSError *error;
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:predicate];
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if([fetchedObjects count]>0)
        {
            for (NSManagedObject *object in fetchedObjects)
            {
                [object setValue:[helper getvaluefromlic:@"REC_STAT" lic:statuslic] forKey:@"status"];
                [object setValue:statuslic forKey:@"statuslic"];
                [object setValue:admindelflg forKey:@"admindelegate"];
            }
            
            [context save:&error];
        }
//        [helper removeWaitCursor];
//    }
    
    Teamlist = [helper query_alldata:@"Event_Team"];
    Teamlist = [Teamlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    [TeamTableview reloadData];
}


- (void) switchIsChanged:(UISwitch *)paramSender
{
    CGPoint buttonPosition = [paramSender convertPoint:CGPointZero toView:TeamTableview];
    NSIndexPath *indexPath = [TeamTableview indexPathForRowAtPoint:buttonPosition];
    appdelegate.userid = [Teamlist[indexPath.row] valueForKey:@"teamid"];
    
    
    NSLog(@"indexPath : %ld",(long)indexPath.row);
    
    [self syncresponsewithserver:indexPath callfor:@"status"];
    
//    if ([paramSender isOn])
//    {
//        [helper update_data:@"GalleryFDRdownload" inputdata:@"YES" entityname:@"S_Config_Table"];
//        [self syncresponsewithserver:indexPath callfor:@"status"];
//    }
//    else
//    {
//        [helper update_data:@"GalleryFDRdownload" inputdata:@"NO" entityname:@"S_Config_Table"];
//    }
    
}

@end
