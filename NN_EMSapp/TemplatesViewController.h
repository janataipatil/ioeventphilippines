//
//  TemplatesViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 31/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "MessagesViewController.h"
#import "SLKeyChainStore.h"
#import "ToastView.h"

@interface TemplatesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UISearchBar *TemplateSearchBarOutlet;
- (IBAction)CommunicationSegmentControllerValueChangedAction:(id)sender;
- (IBAction)AddTemplateButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *TemplateTableView;

@property (strong, nonatomic) IBOutlet UIView *SearchBarHorizontalView;
@property (strong, nonatomic) IBOutlet UIButton *AddTemplateButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *pushBtn;
- (IBAction)pushBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UISegmentedControl *CommunicationSegmentControllerOutlet;
@property(strong,nonatomic) IBOutlet UILabel *tempname_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *description_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *type_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *category_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *subcategory_ulbl;
@property(strong,nonatomic) IBOutlet UILabel *lastsent_ulbl;

@end
