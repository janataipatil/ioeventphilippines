//
//  TemplatesViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 31/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "TemplatesViewController.h"
#import "MessagesViewController.h"

@interface TemplatesViewController ()
{
    NSArray *TemplateArr;
    AppDelegate *appdelegate;
    NSUInteger count;
    MessagesViewController *MessageView;
    int RefreshTableview;
    NSString *selected_record;
    NSString *test_email;
    SLKeyChainStore *keychain;
}

@end

@implementation TemplatesViewController
@synthesize TemplateSearchBarOutlet,TemplateTableView,helper,CommunicationSegmentControllerOutlet,AddTemplateButtonOutlet,SearchBarHorizontalView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
 
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
    [TemplateSearchBarOutlet setBackgroundImage:[UIImage new]];
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    UITextField *searchField = [TemplateSearchBarOutlet valueForKey:@"_searchField"];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
   // searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.backgroundColor = [UIColor colorWithRed:242/255.0 green:246/255.0 blue:250/255.0 alpha:1.0f];
    if (![appdelegate.EventUserRoleArray containsObject:@"E_COM_MNG"])
    {
        AddTemplateButtonOutlet.hidden = YES;
    }
    RefreshTableview = 0;
    [self refreshdata];
    CommunicationSegmentControllerOutlet.hidden = YES;
    
    
    [self langsetuptranslations];
    

}
-(void)viewDidAppear:(BOOL)animated
{
    if (RefreshTableview == 1)
    {
        TemplateTableView.delegate = self;
        TemplateTableView.dataSource = self;
        [TemplateTableView reloadData];
       
        
    }
}
-(void)langsetuptranslations
{

    [CommunicationSegmentControllerOutlet setTitle:[helper gettranslation:@"LBL_258"] forSegmentAtIndex:0];
    [CommunicationSegmentControllerOutlet setTitle:[helper gettranslation:@"LBL_168"] forSegmentAtIndex:1];
    
    _tempname_ulbl.text = [helper gettranslation:@"LBL_258"];
    _description_ulbl.text = [helper gettranslation:@"LBL_094"];
    _type_ulbl.text = [helper gettranslation:@"LBL_266"];
    _category_ulbl.text = [helper gettranslation:@"LBL_062"];
    _subcategory_ulbl.text = [helper gettranslation:@"LBL_557"];
    _lastsent_ulbl.text = [helper gettranslation:@"LBL_154"];
    
}


#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == TemplateSearchBarOutlet)
    {
        [helper MoveViewUp:YES Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == TemplateSearchBarOutlet)
    {
        [helper MoveViewUp:NO Height:350 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == TemplateSearchBarOutlet)
    {
        TemplateArr = [helper query_alldata:@"Event_Mail"];
        TemplateArr = [TemplateArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        if(searchText.length == 0)
        {
           [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
           
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.templateName contains[c] %@) OR (SELF.comment contains[c] %@) OR (SELF.lastSentDate contains[c] %@) OR (SELF.templateType contains[c] %@)",searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[TemplateArr filteredArrayUsingPredicate:predicate]];
            TemplateArr = [filtereventarr copy];
        }
        [TemplateTableView reloadData];
       }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)refreshdata
{
    TemplateArr = [helper query_alldata:@"Event_Mail"];
    TemplateArr = [TemplateArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    TemplateTableView.delegate = self;
    TemplateTableView.dataSource = self;
    [TemplateTableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [TemplateArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"customcell";
    
    customcell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *TemplateNamelbl = (UILabel *)[contentView viewWithTag:1];
    UILabel *TemplateDescriptionlbl = (UILabel *)[contentView viewWithTag:2];
    UILabel *Typelbl = (UILabel *)[contentView viewWithTag:3];
    UILabel *Categorylbl = (UILabel *)[contentView viewWithTag:4];
    UILabel *SubCategorylbl = (UILabel *)[contentView viewWithTag:5];
    UILabel *LastSentlbl = (UILabel *)[contentView viewWithTag:6];
    
    UIButton *testmail_btn = (UIButton *)[contentView viewWithTag:7];
    UIButton *viewdetails_btn = (UIButton *)[contentView viewWithTag:8];
    [testmail_btn setHidden:YES];
    
//    [testmail_btn setTitle:[helper gettranslation:@"LBL_756"] forState:UIControlStateNormal];
//    [viewdetails_btn setTitle:@"" forState:UIControlStateNormal];
    
    //[testmail_btn addTarget:self action:@selector(sendtestemail:) forControlEvents:UIControlEventTouchUpInside];
    [viewdetails_btn addTarget:self action:@selector(showdetailsview:) forControlEvents:UIControlEventTouchUpInside];
    
    
    TemplateNamelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateName"];
    TemplateDescriptionlbl.text = [TemplateArr[indexPath.row] valueForKey:@"comment"];
    LastSentlbl.text = [helper formatingdate:[TemplateArr[indexPath.row] valueForKey:@"lastSentDate"] datetime:@"FormatDate"];
    Typelbl.text = [TemplateArr[indexPath.row] valueForKey:@"templateType"];
    Categorylbl.text = [TemplateArr[indexPath.row] valueForKey:@"category"];
    SubCategorylbl.text = [TemplateArr[indexPath.row] valueForKey:@"subCategory"];
    
    if(indexPath.row % 2 == 0)
    {
        contentView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:246/255.0 blue:250/255.0 alpha:1];
    }
    else
    {
        contentView.backgroundColor = [UIColor clearColor];
    }
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appdelegate.EventUserRoleArray containsObject:@"E_COM_MNG"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == TemplateTableView)
    {
        [helper serverdelete:@"E_TEMPLATE" entityid:[TemplateArr[indexPath.row] valueForKey:@"iD"]];
        NSPredicate *TemplateIDPredicate = [NSPredicate predicateWithFormat:@"iD like %@",[TemplateArr[indexPath.row] valueForKey:@"iD"]];
        [helper delete_predicate:@"Event_Mail" predicate:TemplateIDPredicate];
        [self refreshdata];
       }
}
- (IBAction)CommunicationSegmentControllerValueChangedAction:(id)sender
{
    switch (CommunicationSegmentControllerOutlet.selectedSegmentIndex)
    {
        case 0:
            [MessageView.view removeFromSuperview];
           // TemplateSearchBarOutlet.hidden = NO;
            if (![appdelegate.EventUserRoleArray containsObject:@"E_COM_MNG"])
            {
                AddTemplateButtonOutlet.hidden = YES;
            }
            else
            {
                AddTemplateButtonOutlet.hidden = NO;
            }
            //SearchBarHorizontalView.hidden = NO;
            break;
        case 1:
           // TemplateSearchBarOutlet.hidden = YES;
            AddTemplateButtonOutlet.hidden = YES;
           // SearchBarHorizontalView.hidden = YES;
         [self setupMessageview];
            break;
        default:
            break;
    }

}


-(void)setupMessageview
{
    MessageView = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagesView"];
    [MessageView.view setFrame:CGRectMake(0, 58, self.view.frame.size.width, self.view.frame.size.height)];
    [self addChildViewController:MessageView];
    [self.view addSubview:MessageView.view];
    [MessageView didMoveToParentViewController:self];
}
- (IBAction)AddTemplateButtonAction:(id)sender
{
    [self.parentViewController performSegueWithIdentifier:@"TemplateToAddTemplate" sender:self];
}
#pragma mark unwind method
-(IBAction)unwindToEventTemplate:(UIStoryboardSegue *)segue
{
    RefreshTableview = 1;
    [self refreshdata];
}

-(IBAction)sendtestemail:(id)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TemplateTableView];
    NSIndexPath *indexPath = [TemplateTableView indexPathForRowAtPoint:buttonPosition];
    
    NSString *templateid = [TemplateArr[indexPath.row] valueForKey:@"iD"];
//    [helper sendemail:templateid emailaddr:@"yogesh@iwizardsolutions.com" attendeeid:@""];
    
    
    NSPredicate *UserNamepredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    NSArray *userarr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:UserNamepredicate];
    if ([userarr count]>0)
    {
        test_email = [userarr[0] valueForKey:@"emailaddress"];
    }
    

    NSArray *EventAttendee = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    if ([EventAttendee count]>0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_759"] message:[helper gettranslation:@"LBL_760"] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.text = test_email;
             textField.placeholder = [helper gettranslation:@"LBL_756"];
             textField.secureTextEntry = NO;
             [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
         }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_759"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            [helper sendemail:templateid emailaddr:test_email attendeeid:@""];
        }];
        okAction.enabled = YES;
        [alertController addAction:okAction];
    
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_061"] style:UIAlertActionStyleDefault handler:nil];
        cancelAction.enabled = YES;
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_762"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okaction = [UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okaction];
        [self presentViewController:alert animated:NO completion:nil];

    }
   
}

-(IBAction)showdetailsview:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TemplateTableView];
    NSIndexPath *indexPath = [TemplateTableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        appdelegate.templatename = [TemplateArr[indexPath.row] valueForKey:@"iD"];
    }
    [self.parentViewController performSegueWithIdentifier:@"EventdetailstoMessageView" sender:self];
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UIAlertAction *okAction = alertController.actions.firstObject;
        UITextField *emailaddr = alertController.textFields.firstObject;
        
        if (emailaddr.text.length >0)
        {
            okAction.enabled = YES;
            test_email = emailaddr.text;
            
        }
        else
        {
            okAction.enabled = NO;
        }
    }
}
- (IBAction)pushBtn:(id)sender {
    [self.parentViewController performSegueWithIdentifier:@"showPushNotificationPopup" sender:self];
}
@end
