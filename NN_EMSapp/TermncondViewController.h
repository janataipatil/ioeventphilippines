//
//  TermncondViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/22/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HelperClass.h"
#import "ToastView.h"


@interface TermncondViewController : UIViewController

- (IBAction)disagree_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *masteview;

@property (nonatomic,strong) HelperClass *helper;

@property (strong, nonatomic) IBOutlet UIButton *disagree_btn;

@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;

@property (strong, nonatomic) IBOutlet UIButton *agree_btn;

- (IBAction)agree_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIWebView *webview;


@end
