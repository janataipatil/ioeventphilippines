//
//  TermncondViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/22/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "TermncondViewController.h"

@interface TermncondViewController ()
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSString *webviewurl;
    SLKeyChainStore *keychain;
    NSString *checkreset;
    NSString *userpassword;
    
    dispatch_semaphore_t sem_pass;
}

@end

@implementation TermncondViewController
@synthesize TitleLabel,webview,helper,masteview,agree_btn,disagree_btn;

- (void)viewDidLoad
{
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];

    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
    
   // NSArray *config = [helper query_alldata:@"S_Config_Table"];

    checkreset = [helper query_data:@"name" inputdata:@"ResetPassword" entityname:@"S_Config_Table"];
    webviewurl = [helper query_data:@"name" inputdata:@"TnC_URL" entityname:@"S_Config_Table"];
    
    NSLog(@"Web view url is %@",webviewurl);
//    webviewurl = @"https://nnemsstorage.blob.core.windows.net/nneventdev/IOEvents_TermsOfUsage.pdf";
    
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webviewurl]]];
    TitleLabel.text = [helper getLogintranslation:@"LBL_608"];
    [agree_btn setTitle:[helper getLogintranslation:@"LBL_312"] forState:UIControlStateNormal];
    [disagree_btn setTitle:[helper getLogintranslation:@"LBL_367"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)disagree_btn:(id)sender
{
    [self removeview];
}
- (IBAction)agree_btn:(id)sender
{
    [self sendconcent];
    masteview.hidden = YES;
    
    sem_pass = dispatch_semaphore_create(0);
    
    if ([[helper booltostring:[checkreset boolValue]] isEqualToString:@"Y"])
    {
       // UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Password Reset" message:@"Please Enter New Password" preferredStyle:UIAlertControllerStyleAlert];
          UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_469"] message:[helper gettranslation:@"LBL_490"] preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             //textField.placeholder = NSLocalizedString(@"New Password", @"New Password");
            textField.placeholder = NSLocalizedString([helper gettranslation:@"LBL_427"],[helper gettranslation:@"LBL_427"]);
             textField.secureTextEntry = YES;
             [textField addTarget:self action:@selector(alertFieldBeginEditing:) forControlEvents:UIControlEventEditingDidEnd];
             [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
         }];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
            // textField.placeholder = NSLocalizedString(@"Confirm Password", @"Confirm Password");
            textField.placeholder = NSLocalizedString([helper gettranslation:@"LBL_345"],[helper gettranslation:@"LBL_345"]);
             textField.secureTextEntry = YES;
             [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
             [textField addTarget:self action:@selector(alertFieldBeginEditing:) forControlEvents:UIControlEventEditingDidEnd];
             textField.layer.borderColor=[[UIColor redColor]CGColor];
             textField.layer.borderWidth= 0.0f;
         }];
        
       // UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Save", @"OK action") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString([helper gettranslation:@"LBL_219"], [helper gettranslation:@"LBL_462"]) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            dispatch_semaphore_signal(sem_pass);
        }];
        okAction.enabled = NO;
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        while (dispatch_semaphore_wait(sem_pass, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
        [self ResetPassword];
        
    }
    else
    {
       
        //[helper showWaitCursor:@"Downloading Data...."];
        [helper showWaitCursor:[helper gettranslation:@"LBL_628"]];
        [helper get_EMS_data];
        [helper removeWaitCursor];
        [helper update_data:@"GoToDashboard" inputdata:@"YES" entityname:@"S_Config_Table"];
        [self performSegueWithIdentifier:@"termscreentoHome" sender:self];
        
    }
   
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}


- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UIAlertAction *okAction = alertController.actions.lastObject;
        UITextField *password = alertController.textFields.firstObject;
        UITextField *confirmpassword = alertController.textFields.lastObject;
        
        if (password.text.length >0 &&  confirmpassword.text.length >0)
        {
            if ([password.text isEqualToString: confirmpassword.text])
            {
                okAction.enabled = YES;
               // alertController.message = @"Click save to Reset Password.";
                 alertController.message = [helper gettranslation:@"LBL_611"];
                userpassword = password.text;
            }
            else
            {
                alertController.message = @"";
            }
        }
        else
        {
            okAction.enabled = NO;
        }
    }
}
-(void)alertFieldBeginEditing:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        
        UIAlertAction *okAction = alertController.actions.lastObject;
        UITextField *password = alertController.textFields.firstObject;
        UITextField *confirmpassword = alertController.textFields.lastObject;
        
        
        if(![helper ValidPassword:password.text])
        {
            
            //alertController.view.Col=[UIColor redColor];
            UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
            
            
           // NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:@"Please Enter 1 Special Char,1 Capital Letter and 1 Digit"];
            NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@ 8.",[helper getLogintranslation:@"LBL_472"],[helper getLogintranslation:@"LBL_424"]]];
            [attributedStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,5)];
            lbl.attributedText = attributedStr;
            lbl.textAlignment = NSTextAlignmentCenter;
            //  [alertController setValue:lbl forKey:@"Message"];
            
            alertController.message=[NSString stringWithFormat:@"%@\n%@ 8.",[helper getLogintranslation:@"LBL_472"],[helper getLogintranslation:@"LBL_424"]];
           // alertController.view.tintColor=[UIColor redColor];
            password.text=@"";
            confirmpassword.text=@"";
        }
        
    }
}


-(void) ResetPassword
{
   // [helper showWaitCursor:@"Resetting Password"];
    [helper showWaitCursor:[helper gettranslation:@"LBL_615"]];
    NSError *error;
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:[keychain stringForKey:@"username"] forKey:@"UserName"];
    [JsonDict setValue:userpassword forKey:@"Password"];
    
//        NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
//        [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetUser" pageno:@"" pagecount:@"" lastpage:@""];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
       // [alert show];
        
        [helper showalert:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[helper gettranslation:@"LBL_462"]];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [helper update_data:@"GoToDashboard" inputdata:@"NO" entityname:@"S_Config_Table"];
        
        [self removeview];
        
    }
    
    [helper removeWaitCursor];
    
}


- (void)sendconcent
{
    NSError *error;
   // [helper showWaitCursor:@"Sending Concent"];
    [helper showWaitCursor:[helper gettranslation:@"LBL_544"]];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    [JsonDict setValue:@"true" forKey:@"TnCMStatus"];
    [JsonDict setValue:[dateFormatter stringFromDate:[NSDate date]] forKey:@"TnCMConsentOn"];
    [JsonDict setValue:webviewurl forKey:@"TnCMURL"];
    [JsonDict setValue:[keychain stringForKey:@"username"] forKey:@"UserName"];
    
//    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
//    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetUser" pageno:@"" pagecount:@"" lastpage:@""];
    
//    appdelegate.senttransaction = @"Y";
//    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Set Term & Condition Concent" entityname:@"Term & Condition"  Status:@"Open"];


    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
       
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:@"Ok" otherButtonTitles:nil];
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
       // [alert show];
        
        [helper showalert:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[helper gettranslation:@"LBL_462"]];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [helper insert_transaction_local:msgbody entity_id:@"D-TnCConcent" type:@"Set Term & Condition Concent" entityname:@"Term & Condition" Status:@"Completed"];
        
    }
    [helper removeWaitCursor];
}


-(void) removeview
{
   // [keychain removeItemForKey:@"username"];
  //  [keychain removeItemForKey:@"password"];
    [helper update_data:@"GoToDashboard" inputdata:@"NO" entityname:@"S_Config_Table"];
    
    [self performSegueWithIdentifier:@"UnwindToLogin" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

@end
