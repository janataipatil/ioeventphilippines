//
//  TitleBar.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleBar : UIView

@property(strong,nonatomic)IBOutlet UILabel *UsernameLbl;
@property(strong,nonatomic)IBOutlet UIButton *SettingButton;
@property(strong,nonatomic)IBOutlet UIButton *HelpButton;
@property(strong,nonatomic)IBOutlet UIButton *NotificationButton;
@property(strong,nonatomic)IBOutlet UIImageView *networkImg;
@property(strong,nonatomic)IBOutlet UILabel *networkLbl;
@property(strong,nonatomic)IBOutlet UIButton *HomeMenuButton;


@end
