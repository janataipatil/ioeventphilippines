//
//  TitleBar.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 20/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "TitleBar.h"

@implementation TitleBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    if (self)
    {
        
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TitleBar" owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        [self addSubview:mainView];
        self.UsernameLbl = (UILabel *)[mainView viewWithTag:1];
        self.SettingButton = (UIButton *)[mainView viewWithTag:2];
        self.NotificationButton=(UIButton *)[mainView viewWithTag:5];
        self.networkImg = (UIImageView *)[mainView viewWithTag:8];
        self.networkLbl = (UILabel *)[mainView viewWithTag:9];
        self.HelpButton = (UIButton *)[mainView viewWithTag:10];
        self.HomeMenuButton=(UIButton *)[mainView viewWithTag:20];
        
    }
    return self;
}


@end
