//
//  ToastView.h
//  IFSProjectApprovals
//
//  Created by iWizardsIV on 7/27/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToastView : UIView

@property (strong, nonatomic) NSString *text;

+ (void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration;

@end