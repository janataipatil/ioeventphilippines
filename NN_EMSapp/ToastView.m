//
//  ToastView.m
//  IFSProjectApprovals
//
//  Created by iWizardsIV on 7/27/16.
//  Copyright © 2016 iWizardsVI. All rights reserved.
//

#import "ToastView.h"

@interface ToastView ()
@property (strong, nonatomic, readonly) UILabel *textLabel;
@end
@implementation ToastView
@synthesize textLabel = _textLabel;


#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define IPHONE UIUserInterfaceIdiomPhone

float ToastHeight = 50.0f;
float Toastwidth = 500.0f;
float const ToastGap = 10.0f;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(UILabel *)textLabel
{
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, self.frame.size.width - 10.0, self.frame.size.height - 10.0)];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.numberOfLines = 2;
        _textLabel.font = [UIFont fontWithName:@"Helvetica-Bold"size:14.0f];
        _textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:_textLabel];
        
    }
    return _textLabel;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textLabel.text = text;
}

+ (void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration;
{
    
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[ToastView class]])
        {
            toastsAlreadyInParent++;
        }
    }
    
    CGRect parentFrame = parentView.frame;
    float xOrigin;
    float yOrigin;
    
    if(IDIOM == IPAD)
    {
        xOrigin = parentFrame.size.width/2 - 350;
        yOrigin = parentFrame.size.height - 70.0;
        ToastHeight = 50.0f;
        Toastwidth = 700.0f;
    }
    else
    {
        xOrigin = parentFrame.size.width/2 - 150;
        yOrigin = parentFrame.size.height - 70.0;
        
        ToastHeight = 40.0f;
        Toastwidth = 300.0f;
    }

    
    
    
    
//    float yOrigin = parentFrame.size.height - 70.0;
//    float yOrigin = parentFrame.size.height - (70.0 + ToastHeight * toastsAlreadyInParent + ToastGap * toastsAlreadyInParent);
//    CGRect selfFrame = CGRectMake(parentFrame.origin.x + 20.0, yOrigin, parentFrame.size.width - 40.0, ToastHeight);

    CGRect selfFrame = CGRectMake(xOrigin, yOrigin, Toastwidth, ToastHeight);
    ToastView *toast = [[ToastView alloc] initWithFrame:selfFrame];
    
    toast.backgroundColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    toast.alpha = 0.4f;
    toast.layer.cornerRadius = 4.0;
    toast.text = text;
    
    [parentView addSubview:toast];
    
    [UIView animateWithDuration:0.4 animations:^{
        toast.alpha = 0.5f;
        toast.textLabel.alpha = 0.9f;
    }completion:^(BOOL finished) {
        if(finished){
            
        }
    }];
    
    
    [toast performSelector:@selector(hideSelf) withObject:nil afterDelay:duration];
    
}

- (void)hideSelf
{
    
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 0.5;
        self.textLabel.alpha = 0.0;
    }completion:^(BOOL finished) {
        if(finished){
            [self removeFromSuperview];
        }
    }];
}

@end
