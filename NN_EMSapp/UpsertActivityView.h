//
//  UpsertActivityView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 12/27/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "HSDatePickerViewController.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"


@interface UpsertActivityView : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UITextField *act_type_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_status_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_description_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_priority_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_assignee_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_stdate_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_enddate_tv;
@property (strong, nonatomic) IBOutlet UITextField *act_notes_tv;
@property (strong, nonatomic) IBOutlet UIView *Parentview;
@property (strong, nonatomic) IBOutlet UITableView *dynamicfields_tbl;
@property (strong, nonatomic) IBOutlet UIView *containerview;
@property (strong, nonatomic) IBOutlet UIView *addactfields_view;
@property (strong, nonatomic) IBOutlet UIButton *save_btn;
@property (strong, nonatomic) IBOutlet UIButton *cancel_btn;
@property (strong, nonatomic) IBOutlet UILabel *headertitle_lbl;
@property (strong, nonatomic) IBOutlet UIView *headertitle_view;
@property(strong,nonatomic) NSMutableArray *filteredpartArray;
@property (strong, nonatomic) IBOutlet UITableView *pickup_tbl;
@property(strong,nonatomic)NSString *invokingview;
@property(strong,nonatomic) HelperClass *helper;
@property (strong, nonatomic) IBOutlet UITableView *LovTableview;
@property (strong, nonatomic) IBOutlet UITextField *StatusTextField;
@property (strong, nonatomic) IBOutlet UITextField *TypeTextField;
@property (strong, nonatomic) IBOutlet UITextField *PriorityTextField;
@property (strong, nonatomic) IBOutlet UITextField *ActplanTextField;
@property (strong, nonatomic) IBOutlet UITextField *AssigneeTextField;
@property (strong, nonatomic) IBOutlet UIButton *StartDateOutlet;
@property (strong, nonatomic) IBOutlet UIButton *EndDateOutlet;
@property (strong, nonatomic) IBOutlet UITextField *DescriptionTextfield;
@property(strong,nonatomic)NSString *TypeOfActionStr;
@property (strong, nonatomic) IBOutlet UITextField *NotesTextfield;

@property (strong, nonatomic) IBOutlet UIImageView *StatusDropdownImageView;

@property(strong,nonatomic)NSString *CancelSegue;

- (IBAction)StartDateButtonAction:(id)sender;
- (IBAction)EndDateButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *priorityDropDownImage;


@property (strong, nonatomic) IBOutlet UIView *dropdown_view;
@property (strong, nonatomic) IBOutlet UILabel *dropdown_lbl;


@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *type_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actdesc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *priority_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *sdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *edate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *notes_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *actplan_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *assignee_ulbl;




@end
