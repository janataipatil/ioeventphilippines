//
//  UpsertActivityView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 12/27/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "UpsertActivityView.h"

@interface UpsertActivityView ()<HSDatePickerViewControllerDelegate>
{
    NSArray *AssignedActivitylist;
    SLKeyChainStore *keychain;
    CGRect origcontainerviewframe;
    CGRect origheadertitleframe;
    AppDelegate *appdelegate;
    CGRect origcancel_btnframe;
    CGRect headertitle_lblframe;
    
    NSArray* Fieldlist;
    NSArray* Piclistdata;
    NSUInteger count;
    NSPredicate *ActivityPredicate;
    NSPredicate *event_pred;
    NSString *validStatus;
    NSString *pickentity;
    NSString *selectedfield;
    NSString *Activitytype;
    NSString *TextfieldFlag;
    NSArray *LovArr;
    NSIndexPath *selindexpath;
    NSArray *EditActivityRecordArr;
    UIToolbar *toolBar;
    NSString *selecteddate;
    NSDateFormatter *formatter;
    NSString *startEndDate;
    NSString *ActivityID;
    NSManagedObjectContext *context;
    NSString *rowid,*transaction_type;
    NSMutableDictionary *DynamicFieldDict;
    //NSMutableArray *RequiredFieldArray;
    NSString *PlanName;
    NSMutableArray *PlanNames;
    NSMutableDictionary *EditcodefieldDict;
    NSString *CanEdit;
    NSString *CanDelete;
    NSString *Visibility;
    
    NSString *Actid;
    NSUInteger stringlength;
    NSString *StatusLic;
}
@property (nonatomic, strong) NSDate *selectedDate;
@end

@implementation UpsertActivityView



@synthesize dynamicfields_tbl, containerview,addactfields_view, save_btn, cancel_btn, headertitle_lbl, headertitle_view,Parentview,helper,invokingview,pickup_tbl, filteredpartArray,TypeTextField,StatusTextField,PriorityTextField,AssigneeTextField,ActplanTextField,LovTableview,StartDateOutlet,EndDateOutlet,TypeOfActionStr,DescriptionTextfield,NotesTextfield,StatusDropdownImageView,CancelSegue;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    origcontainerviewframe = containerview.frame ;
    origheadertitleframe  = headertitle_view.frame;
    origcancel_btnframe = cancel_btn.frame ;
    headertitle_lblframe = headertitle_lbl.frame ;
    DynamicFieldDict = [[NSMutableDictionary alloc]init];
   // RequiredFieldArray = [[NSMutableArray alloc]init];
    EditcodefieldDict = [[NSMutableDictionary alloc]init];
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
 
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    ActivityPredicate = [NSPredicate predicateWithFormat:@"activityid like %@",appdelegate.activityid];
    _dropdown_view.hidden = YES;
    
    [self langsetuptranslations];
    
    
    Fieldlist = [[helper query_alldata:@"Act_dynamicdetails"] filteredArrayUsingPredicate:ActivityPredicate];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"sequence" ascending:YES];
    Fieldlist=[Fieldlist sortedArrayUsingDescriptors:@[sort]];
    pickup_tbl.hidden = YES;
    if (Fieldlist.count <=0)
    {
        dynamicfields_tbl.hidden = YES;
        containerview.frame = CGRectMake( Parentview.frame.size.width/2 -250, containerview.frame.origin.y, addactfields_view.frame.size.width+15, containerview.frame.size.height);
        headertitle_view.frame = CGRectMake( Parentview.frame.size.width/2 -250, headertitle_view.frame.origin.y, addactfields_view.frame.size.width+15, headertitle_view.frame.size.height);
        
        cancel_btn.frame = CGRectMake( headertitle_view.frame.size.width-118, cancel_btn.frame.origin.y, cancel_btn.frame.size.width+20, cancel_btn.frame.size.height);
        headertitle_lbl.frame = CGRectMake( headertitle_view.frame.size.width/2 -83, headertitle_lbl.frame.origin.y, headertitle_lbl.frame.size.width, headertitle_lbl.frame.size.height);
    }
   
    
    
    //array contains codefield value
    for (int i =0; i<[Fieldlist count]; i++)
    {
        [EditcodefieldDict setObject:[Fieldlist[i] valueForKey:@"fieldValue"]?:@"" forKey:[Fieldlist[i] valueForKey:@"id"]];
    }
    
    AssignedActivitylist = [helper query_alldata:@"Activities"];
    NSArray *distPlanNames = [AssignedActivitylist valueForKeyPath:@"@distinctUnionOfObjects.plan_name"];
    PlanNames = [[NSMutableArray alloc] init];
    
    for (NSString *groupId in distPlanNames)
    {
        NSMutableDictionary *entry = [NSMutableDictionary new];
        
        if (![groupId isEqualToString:@"Ad-Hoc Activities"] )
        {
           /* NSPredicate *planpredicate = [NSPredicate predicateWithFormat:@"name like %@",groupId];
            NSArray *Actplans = [[helper query_alldata:@"ActivityPlans"] filteredArrayUsingPredicate:planpredicate];
            NSString *planid;
            NSString *actplanid;
            
            if (Actplans.count > 0)
            {
                
                planid = [[[Actplans valueForKey:@"planID"] allObjects] objectAtIndex:0];
            }
            
            NSPredicate *actypredicate = [NSPredicate predicateWithFormat:@"planID like %@",planid];
            NSArray *PlanActs = [[helper query_alldata:@"PlanActivities"] filteredArrayUsingPredicate:actypredicate];
            if (PlanActs.count > 0)
            {
                actplanid = [PlanActs[0] valueForKey:@"iD"];
            }*/
            
            NSArray *Actplans = [[helper query_alldata:@"Activities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"plan_name like %@",groupId]];
            NSString *planid;
           
            
            if (Actplans.count > 0)
            {
                
                planid = [[[Actplans valueForKey:@"planid"] allObjects] objectAtIndex:0];
                [entry setObject:groupId forKey:@"plan_name"];
                [entry setObject:planid forKey:@"plan_id"];
            }

            
            
            //[entry setObject:@"" forKey:@"plan_actid"];
            
        }
        else
        {
            [entry setObject:groupId forKey:@"plan_name"];
            [entry setObject:@"" forKey:@"plan_id"];
            //[entry setObject:@"" forKey:@"plan_actid"];
        }
        
        [PlanNames addObject:entry];
    }
    
    if (![distPlanNames containsObject: @"Ad-Hoc Activities"] )
    {
        NSMutableDictionary *adhoc_entry = [NSMutableDictionary new];
        [adhoc_entry setObject:@"Ad-Hoc Activities" forKey:@"plan_name"];
        [adhoc_entry setObject:@"" forKey:@"plan_id"];
      //  [adhoc_entry setObject:@"" forKey:@"plan_actid"];
        
        [PlanNames addObject:adhoc_entry];
    }
    
    
    
    LovTableview.layer.borderColor = [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LovTableview.layer.borderWidth = 1;
    [TypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [StatusTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [PriorityTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [AssigneeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [ActplanTextField  addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    validStatus = @"YES";
    
    //edit
    if ([TypeOfActionStr isEqualToString:@"Add"])
    {
        headertitle_lbl.text = [helper gettranslation:@"LBL_177"];
        ActivityID = [helper generate_rowId];
        transaction_type = @"Add Activity";
        StatusTextField.text = [helper getvaluefromlic:@"ACT_STATUS" lic:@"Planned"];
        StatusDropdownImageView.hidden = YES;
        StatusTextField.userInteractionEnabled = NO;
        CanEdit = @"Y";
        CanDelete = @"Y";
        Visibility = @"MY";
       
        
    }
    else if ([TypeOfActionStr isEqualToString:@"Edit"])
    {
        headertitle_lbl.text = [helper gettranslation:@"LBL_104"];
        transaction_type = @"Edit Activity";
        ActivityID = appdelegate.activityid;
        EditActivityRecordArr = [AssignedActivitylist filteredArrayUsingPredicate:ActivityPredicate];
        [self setupActivityView];
        
    }
     event_pred =  [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
 
    if([appdelegate.canEdit isEqualToString:@"N"]&&[TypeOfActionStr isEqualToString:@"Edit"])
    {
        [self setNotEditable];
    }
   if([[EditActivityRecordArr[0] valueForKey:@"status"] isEqualToString:@"Completed"])
   {
//       [StatusTextField setUserInteractionEnabled:NO];
     //  validStatus = @"YES";
   }
    
}



-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _type_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_266"]];
    [self markasrequired:_type_ulbl];
    
    _status_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_242"]];
    [self markasrequired:_status_ulbl];
    
    _actdesc_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_005"]];
    [self markasrequired:_actdesc_ulbl];
    
    _priority_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_203"]];
    [self markasrequired:_priority_ulbl];
    
    _sdate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_239"]];
    [self markasrequired:_sdate_ulbl];
    
    _edate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_122"]];
    [self markasrequired:_edate_ulbl];
    
    _notes_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_183"]];
    
    _actplan_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_007"]];
    [self markasrequired:_actplan_ulbl];
    
    _assignee_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_053"]];
  
}

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}

#pragma mark Setup Activity View
-(void)setupActivityView
{
    if ([EditActivityRecordArr count]>0)
    {
        appdelegate.eventid = [EditActivityRecordArr[0] valueForKey:@"eventid"];
        TypeTextField.text = [EditActivityRecordArr[0] valueForKey:@"type"];
        TypeTextField.userInteractionEnabled = NO;
        StatusTextField.text = [EditActivityRecordArr[0] valueForKey:@"status"];
        DescriptionTextfield.text = [EditActivityRecordArr[0] valueForKey:@"desc"];
        PriorityTextField.text = [EditActivityRecordArr[0] valueForKey:@"priority"];
        AssigneeTextField.text = [EditActivityRecordArr[0] valueForKey:@"ownedby"];
        ActplanTextField.text = [EditActivityRecordArr[0] valueForKey:@"plan_name"];
        [StartDateOutlet setTitle:[helper formatingdate:[EditActivityRecordArr[0] valueForKey:@"startdate"] datetime:@"FormatDate"] forState:UIControlStateNormal];
        [EndDateOutlet setTitle:[helper formatingdate:[EditActivityRecordArr[0] valueForKey:@"enddate"] datetime:@"FormatDate"] forState:UIControlStateNormal];
        NotesTextfield.text = [EditActivityRecordArr[0] valueForKey:@"comment"];
        PlanName = [EditActivityRecordArr[0] valueForKey:@"plan_name"];
        Visibility = [EditActivityRecordArr[0] valueForKey:@"visibility"];
        StatusDropdownImageView.hidden = NO;
        StatusTextField.userInteractionEnabled = YES;
        
        CanEdit = [EditActivityRecordArr[0] valueForKey:@"canEdit"];
        CanDelete = [EditActivityRecordArr[0] valueForKey:@"canDelete"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark checking value changed
/*-(NSMutableDictionary *)checktheupdatedvalues
{
    NSMutableDictionary *updatedict = [[NSMutableDictionary alloc]init];
    if ([EditActivityRecordArr count]>0)
    {
       // server will have "" value and textfield will have '' so check the text length
        if([TypeTextField.text length]>0 || [[EditActivityRecordArr[0] valueForKey:@"typelic"] length]>0 )
        {
            if (![TypeTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"typelic"]])
            {
                [updatedict setValue:TypeTextField.text forKey:@"Typelic"];
            }
        }
        if ([StatusTextField.text length]>0 || [[EditActivityRecordArr[0] valueForKey:@"statuslic"] length]>0)
        {
            if (![StatusTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"statuslic"]])
            {
                [updatedict setValue:StatusTextField.text forKey:@"Statuslic"];
            }
        }
        if ([DescriptionTextfield.text length]>0 || [[EditActivityRecordArr[0] valueForKey:@"desc"] length]>0 )
        {
            if (![DescriptionTextfield.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"desc"]])
            {
                [updatedict setValue:DescriptionTextfield.text forKey:@"Desc"];
            }
        }
        if ([PriorityTextField.text length]>0 || [[EditActivityRecordArr[0] valueForKey:@"prioritylic"] length]>0)
        {
            if (![PriorityTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"prioritylic"]])
            {
                [updatedict setValue:PriorityTextField.text forKey:@"Prioritylic"];
            }
        }
        if ([AssigneeTextField.text length]>0 || [[EditActivityRecordArr[0] valueForKey:@"ownedby"] length]>0 )
        {
            if (![AssigneeTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"ownedby"]])
            {
                [updatedict setValue:AssigneeTextField.text forKey:@"Ownedby"];
            }
        }
        if ([ActplanTextField.text length]>0 || [[EditActivityRecordArr[0] valueForKey:@"plan_name"] length]>0 )
        {
            if (![ActplanTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"plan_name"]])
            {
                //------------- [updatedict setValue:AssigneeTextField.text forKey:@"plan_name"];
            }
        }

        if ([StartDateOutlet.titleLabel.text length]>0 || [EditActivityRecordArr[0] valueForKey:@"startdate"] )
        {
            if(![[helper converingformateddatetooriginaldate:StartDateOutlet.titleLabel.text datetype:@"CreateEditActivity"] isEqualToString:[EditActivityRecordArr[0] valueForKey:@"startdate"]])
            {
                [updatedict setValue:StartDateOutlet.titleLabel.text forKey:@"Startdate"];
            }
        }
        if ([EndDateOutlet.titleLabel.text length]>0 || [EditActivityRecordArr[0] valueForKey:@"enddate"] )
        {
            if(![[helper converingformateddatetooriginaldate:EndDateOutlet.titleLabel.text datetype:@"CreateEditActivity"] isEqualToString:[EditActivityRecordArr[0] valueForKey:@"enddate"]])
            {
                [updatedict setValue:EndDateOutlet.titleLabel.text forKey:@"Enddate"];
            }
        }
        if ([NotesTextfield.text length]>0 || [EditActivityRecordArr[0] valueForKey:@"comment"])
        {
            if (![NotesTextfield.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"comment"]])
            {
                [updatedict setValue:NotesTextfield.text forKey:@"Comment"];
            }
        }
        if ([DynamicFieldDict count]>0)
        {
            [updatedict setValue:@"" forKey:@"Codefields"];
        }
        
    }
    return updatedict;
}*/
-(NSMutableDictionary *)createDictionary
{
  
    context = [appdelegate managedObjectContext];
    NSError *error;
    if ([TypeOfActionStr isEqualToString:@"Add"])
    {
//        NSArray *MasterCodefieldsArr = [[helper query_alldata:@"Act_Config_Table"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activity_typelic like %@",[helper getLic:@"ACT_STATUS" value:TypeTextField.text]]];
        
        
        NSArray *MasterCodefieldsArr = [[helper query_alldata:@"Act_Config_Table"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"activity_typelic like %@",[helper getLic:@"ACT_TYPE" value:TypeTextField.text]]];
        for (NSDictionary *record in  MasterCodefieldsArr)
        {
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Act_dynamicdetails" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            [newrecord setValue:ActivityID forKey:@"activityid"];
            [newrecord setValue:appdelegate.eventid forKey:@"eventid"];
            [newrecord setValue:[record valueForKey:@"fieldname"] forKey:@"fieldName"];
            [newrecord setValue:[record valueForKey:@"fieldnamelic"] forKey:@"fieldNameLIC"];
            [newrecord setValue:[record valueForKey:@"fieldtype"] forKey:@"fieldType"];
            [newrecord setValue:[record valueForKey:@"fieldtypelic"] forKey:@"fieldTypeLic"];
            [newrecord setValue:@"" forKey:@"fieldValue"];
            [newrecord setValue:[helper generate_rowId] forKey:@"id"];
            [newrecord setValue:[record valueForKey:@"picklist_type"] forKey:@"picklistType"];
            [newrecord setValue:[record valueForKey:@"required"] forKey:@"required"];
            [newrecord setValue:[record valueForKey:@"sequence"] forKey:@"sequence"];
            [context save:&error];
        }
    }
    else if ([TypeOfActionStr isEqualToString:@"Edit"])
    {
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Act_dynamicdetails" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        [request setEntity:entitydesc];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((SELF.eventid contains[c] %@) AND (SELF.activityid contains[c] %@))",appdelegate.eventid,ActivityID];
        [request setPredicate:predicate];
        NSArray *matchingData = [context executeFetchRequest:request error:&error];
       
        for (int i =0; i<[matchingData count]; i++)
        {
            if([DynamicFieldDict count] >0)
            {
                NSString *fieldvalue = [DynamicFieldDict valueForKey:[matchingData[i] valueForKey:@"id"]];
                if (fieldvalue)
                {
                    [matchingData[i] setValue:fieldvalue forKey:@"fieldValue"];
                    [context save:&error];
                }
                
            }

        }
    }
   
    
    NSMutableArray *ActivityCodeFieldArr = [[NSMutableArray alloc]init];
    
    NSArray *CreatedCodefieldsArr = [[helper query_alldata:@"Act_dynamicdetails"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"((SELF.eventid contains[c] %@) AND (SELF.activityid contains[c] %@))",appdelegate.eventid,ActivityID]];
    for (NSDictionary *record in CreatedCodefieldsArr)
    {
        NSMutableDictionary *CodeFieldDict = [[NSMutableDictionary alloc]init];
        [CodeFieldDict setObject:[record valueForKey:@"id"] forKey:@"ID"];
        [CodeFieldDict setObject:[record valueForKey:@"eventid"] forKey:@"EventID"];
        [CodeFieldDict setObject:[record valueForKey:@"activityid"] forKey:@"ActivityID"];
        [CodeFieldDict setObject:[record valueForKey:@"fieldNameLIC"] forKey:@"FieldNameLIC"];
        [CodeFieldDict setObject:[record valueForKey:@"fieldTypeLic"] forKey:@"FieldTypeLIC"];
        [CodeFieldDict setObject:[record valueForKey:@"fieldValue"]?:@"" forKey:@"FieldValue"];
        [CodeFieldDict setObject:[record valueForKey:@"picklistType"]?:@"" forKey:@"PickListType"];
        [CodeFieldDict setObject:[helper stringtobool:[record valueForKey:@"required"]] forKey:@"Required"];
        [CodeFieldDict setObject:[record valueForKey:@"sequence"] forKey:@"Sequence"];
        [ActivityCodeFieldArr addObject:CodeFieldDict];
    }
    NSMutableDictionary *ActivityDict = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *newActivity = [[NSMutableDictionary alloc] init];
    [newActivity setObject:ActivityID forKey:@"ActivityID"];
    [newActivity setObject:[helper query_data:@"name" inputdata:@"EMSAffiliateID" entityname:@"S_Config_Table"] forKey:@"AffiliateID"];
    [newActivity setObject:DescriptionTextfield.text forKey:@"Description"];
    
    if (EndDateOutlet.titleLabel.text.length > 0)
    {
        [newActivity setObject:[helper converingformateddatetooriginaldate:EndDateOutlet.titleLabel.text datetype:@"ServerDate"] forKey:@"EndDate"];
        
    }
    if (StartDateOutlet.titleLabel.text.length > 0)
    {
        [newActivity setObject:[helper converingformateddatetooriginaldate:StartDateOutlet.titleLabel.text datetype:@"ServerDate"] forKey:@"StartDate"];
    }
    [newActivity setObject:appdelegate.eventid forKey:@"EventID"];
    [newActivity setObject:AssigneeTextField.text forKey:@"OwnedBy"];
    [newActivity setObject:ActplanTextField.text forKey:@"PlanName"];
    
   
    if ([[helper getLic:@"ACT_STATUS" value:StatusTextField.text] isEqualToString:@"Completed"])
    {
        CanDelete = @"N";
        CanEdit = @"N";
    }
    
    [newActivity setObject:[helper stringtobool:CanDelete] forKey:@"CanDelete"];
    [newActivity setObject:[helper stringtobool:CanEdit] forKey:@"CanEdit"];
    
    
    
    if ([TypeOfActionStr isEqualToString:@"Add"])
    {
        if (![AssigneeTextField.text isEqualToString:[keychain stringForKey:@"username"]])
        {
            Visibility = @"TEAM";
        }
        else
        {
            Visibility = @"MY";
        }
    }
    
    
    if (Visibility.length == 0)
    {
        Visibility = @"MY";
    }
    
    
    [newActivity setObject:Visibility forKey:@"Visibility"];
    
    if (![ActplanTextField.text isEqualToString:@"Ad-Hoc Activities"] )
    {
        NSPredicate *planpredicate = [NSPredicate predicateWithFormat:@"name like %@",ActplanTextField.text];
        NSArray *Actplans = [[helper query_alldata:@"ActivityPlans"] filteredArrayUsingPredicate:planpredicate];
        
        if (Actplans.count > 0)
        {
            [newActivity setObject:[Actplans[0] valueForKey:@"planID"] forKey:@"PlanID"];
        }
    }
    else
    {
        [newActivity setObject:@"" forKey:@"PlanID"];
    }

    
    NSLog(@"TYPE: %@",TypeTextField.text);
    NSLog(@"LIC: %@",[helper getLic:@"ACT_TYPE" value:TypeTextField.text]);
    
    if([PriorityTextField.text length]>0)
        [newActivity setObject:[helper getLic:@"PRIORITY" value:PriorityTextField.text] forKey:@"PriorityLIC"];
    if([StatusTextField.text length]>0)
        [newActivity setObject:[helper getLic:@"ACT_STATUS" value:StatusTextField.text] forKey:@"StatusLIC"];
    if([TypeTextField.text length]>0)
        [newActivity setObject:[helper getLic:@"ACT_TYPE" value:TypeTextField.text] forKey:@"TypeLIC"];
   
    [newActivity setObject:PriorityTextField.text forKey:@"Priority"];
    [newActivity setObject:StatusTextField.text forKey:@"Status"];
    [newActivity setObject:TypeTextField.text forKey:@"Type"];
    [newActivity setObject:NotesTextfield.text forKey:@"Comment"];
    [ActivityDict setObject:newActivity forKey:@"Activity"];
    [ActivityDict setObject:ActivityCodeFieldArr forKey:@"ActCodeFields"];
    
    return ActivityDict;
    
}
/*-(BOOL)Codefieldsrequired
{
    int Flag = 0;
    for (int i =0;i<[ Fieldlist count];i++)
    {
        
        if ([[Fieldlist[i] valueForKey:@"required"] isEqualToString:@"Y"])
         {
             if(![[EditActivityRecordArr[0] valueForKey:@"statuslic"] isEqualToString:@"Planned"])
             {
                 if([DynamicFieldDict count]>0)
                 {
                     if([DynamicFieldDict valueForKey:[Fieldlist[i] valueForKey:@"id"]])
                     {
                         Flag = 0;
                     }
                 }
                 else
                 {
                     Flag = 1;
                 }
             }
        }
    }
        
    if (Flag == 1)
    {
        return NO;
    }
    else
    {
        return YES;
    }
    
}*/


-(BOOL)Codefieldsrequired
{
    int Flag = 0;
    if([[EditActivityRecordArr[0] valueForKey:@"statuslic"] isEqualToString:@"Planned"])
    {
        Flag=0;
    }
    else
    {
        for(int i=0;i<[Fieldlist count];i++)
        {
            if ([[Fieldlist[i] valueForKey:@"required"] isEqualToString:@"Y"])
            {
                selindexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                
                UITableViewCell *custcell = (UITableViewCell *)[dynamicfields_tbl cellForRowAtIndexPath:selindexpath];
                
                UIView *contentView = custcell.contentView;
                
                UITextField *fieldvalue = (UITextField *) [contentView viewWithTag:3];
                
                    
                if([fieldvalue.text length]>0||[DynamicFieldDict count]>0)
                {
                    Flag=0;
                }
                else
                {
                    Flag=1;
                    break;
                }
           }
        }
        
        
    }
    if (Flag == 1)
    {
        return NO;
    }
    else
    {
        return YES;
    }

}

- (IBAction)save_btn:(id)sender
{
    [StatusTextField resignFirstResponder];
    if ([TypeTextField.text length]>0 && [PriorityTextField.text length]>0 && [StartDateOutlet.titleLabel.text length]>0 && [EndDateOutlet.titleLabel.text length]>0 && [ActplanTextField.text length]>0 && [self Codefieldsrequired] && [validStatus isEqualToString:@"YES"])
    {
        if ([TypeOfActionStr isEqualToString:@"Edit"])
        {
            if ([EditActivityRecordArr count]>0)
            {
                if ([TypeTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"type"]? :@""] && [StatusTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"status"]?:@""] && [DescriptionTextfield.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"desc"]?:@""] && [PriorityTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"priority"]?:@""] && [AssigneeTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"ownedby"]?:@""] && [ActplanTextField.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"plan_name"]?:@""] && [[helper converingformateddatetooriginaldate:StartDateOutlet.titleLabel.text datetype:@"ServerDate"] isEqualToString:[EditActivityRecordArr[0] valueForKey:@"startdate"]?:@""] && [[helper converingformateddatetooriginaldate:EndDateOutlet.titleLabel.text datetype:@"ServerDate"] isEqualToString:[EditActivityRecordArr[0] valueForKey:@"enddate"]?:@""] && [NotesTextfield.text isEqualToString:[EditActivityRecordArr[0] valueForKey:@"comment"]?:@""] && [DynamicFieldDict isEqualToDictionary:EditcodefieldDict])
                    {
                        [self removepopup];
                        return;
                    }
                }
        }
        
            [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
            NSMutableDictionary *ActivityDict = [[NSMutableDictionary alloc]init];
            ActivityDict = [self createDictionary];
            
    
            NSError *error;
            NSString *jsonString;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ActivityDict options:0 error:&error];
            if (jsonData)
            {
               jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
            NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
            NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
            NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
            NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetActivityH" pageno:@"" pagecount:@"" lastpage:@""];
            
            appdelegate.senttransaction = @"Y";
            
            [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Add Activities"  Status:@"Open"];
            if ([TypeOfActionStr isEqualToString:@"Edit"])
            {
                NSArray *ActArray = [helper query_alldata:@"Activities"];
                ActArray = [ActArray filteredArrayUsingPredicate:ActivityPredicate];
                
                
                
                NSString *calidentify = [[[[[helper query_alldata:@"Activities"] filteredArrayUsingPredicate:ActivityPredicate] valueForKey:@"calevent_refid"] allObjects] objectAtIndex:0];
                
                [helper deletecal_event:calidentify];
                [helper delete_predicate:@"Activities" predicate:ActivityPredicate];
                
            }
            [helper update_activitydata:ActivityID inputdata:[ActivityDict valueForKey:@"Activity"] source:@"Child"];
        
        
        
           /* NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
            
            if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
            {
                [helper removeWaitCursor];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                 NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
                 NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
                 NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
                [helper update_activitydata:ActivityID inputdata:[ActivityDict valueForKey:@"Activity"] source:@"Child"];
                
            }*/
        
            [helper DeltaSyncCalander];
        
            [helper removeWaitCursor];
            [self removepopup];

    }
    else
    {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}
-(void)removepopup
{
    appdelegate.swipevar = @"activityview";
   // [self performSegueWithIdentifier:@"UnwindfromActivityUpsert" sender:self];
    
    [self performSegueWithIdentifier:CancelSegue sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == pickup_tbl)
    {
        return Piclistdata.count;
    }
    else if (tableView == dynamicfields_tbl)
    {
        return Fieldlist.count;
    }
    else if (tableView == LovTableview)
    {
        return [LovArr count];
    }
    else
    {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    if (tableView == pickup_tbl)
    {
        cellIdentifier = @"socustomcell";
        
        customcell = [self.pickup_tbl dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        
        UILabel *titlename = (UILabel *) [contentView viewWithTag:1];
        UILabel *titletype = (UILabel *) [contentView viewWithTag:2];
        UILabel *titlesize = (UILabel *) [contentView viewWithTag:3];
        
        if ([pickentity isEqualToString:@"Event_Attachments"])
        {
            [titlename setText:[Piclistdata[indexPath.row] valueForKey:@"filename"]];
            [titletype setText:[Piclistdata[indexPath.row] valueForKey:@"filesize"]];
            [titlesize setText:[Piclistdata[indexPath.row] valueForKey:@"filetype"]];
        }
        else if ([pickentity isEqualToString:@"CustomPickup"])
        {
            //[titlename setText:[Piclistdata[indexPath.row] valueForKey:@"lic"]];
             [titlename setText:[Piclistdata[indexPath.row] valueForKey:@"value"]];
            //[titletype setText:[Piclistdata[indexPath.row] valueForKey:@"name"]];
            [titlesize setText:@""];
            [titletype setText:@""];
        }
        else if ([pickentity isEqualToString:@"Event_Mail"])
        {
            [titlename setText:@""];
            [titletype setText:[Piclistdata[indexPath.row] valueForKey:@"templateName"]];
            [titlesize setText:@""];
           // templateID
        }
        
    }
    else if (tableView == dynamicfields_tbl)
    {
        
        cellIdentifier = @"repetedcustomcell";
        
        if (customcell == nil)
        {
            customcell = [self.dynamicfields_tbl dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        }

        UIView *contentView = customcell.contentView;
        UILabel *fieldname = (UILabel *) [contentView viewWithTag:2];
        UITextField *fieldvalue  = (UITextField *) [contentView viewWithTag:3];
        UIView *SepratorView = (UIView *) [contentView viewWithTag:4];
        UIButton *pickbutton = (UIButton *) [contentView viewWithTag:5];
       // UISwitch *switch_btn = (UISwitch *) [contentView viewWithTag:6];
        
        UISwitch *switch_btn;
        [pickbutton addTarget:self action:@selector(pickbutton_action:) forControlEvents:UIControlEventTouchUpInside];
        
       // NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldType"] ];
        //fieldname.text = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldLIC"] ];
        NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldTypeLic"] ];
        if ([[Fieldlist[indexPath.row] valueForKey:@"required"] isEqualToString:@"Y"])
        {
            if(![[EditActivityRecordArr[0] valueForKey:@"statuslic"] isEqualToString:@"Planned"])
            {
                fieldname.text = [NSString stringWithFormat:@"%@ *", [Fieldlist[indexPath.row] valueForKey:@"fieldName"] ];
                [self markasrequired:fieldname];
            }
            else
                fieldname.text = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldName"] ];
        }
        else
        {
             fieldname.text = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldName"] ];
        }
        
       

        [fieldvalue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        if ([field_val isEqualToString:@"PICKLIST"])
        {
            fieldvalue.hidden = NO;
            SepratorView.hidden = NO;
            pickbutton.hidden = NO;
            fieldvalue.delegate = self;
            if ([[Fieldlist[indexPath.row] valueForKey:@"fieldValue"] length]>0)
            {
                 fieldvalue.text = [Fieldlist[indexPath.row] valueForKey:@"fieldValue"];
            }
            else
            {
                fieldvalue.placeholder = [helper gettranslation:@"LBL_757"];
            }
            
            
            pickbutton.frame = CGRectMake(208.0, 0.0, 62.0f, 49.0f);
            pickbutton.backgroundColor = [UIColor clearColor];
            
            [pickbutton setImage:[UIImage imageNamed:@"dropdown.png"] forState:UIControlStateNormal];
        }
        else if ([field_val isEqualToString:@"SWITCH"])
        {
            //currently not using this case
            fieldvalue.hidden = YES;
            SepratorView.hidden = YES;
            pickbutton.hidden = YES;
            
            CGRect myFrame = CGRectMake(fieldvalue.frame.origin.x, fieldvalue.frame.origin.y+10, 51.0f, 31.0f);
            switch_btn  = [[UISwitch alloc] initWithFrame:myFrame];
            [switch_btn setOn:YES];
            switch_btn.transform = CGAffineTransformMakeScale(0.60, 0.60);
            switch_btn.tag = indexPath.row;
            //switch_btn.onTintColor = [UIColor colorWithRed:94/255.0F green:145/255.0F blue:121/255.0F alpha:1.0F];
            switch_btn.onTintColor = [helper DarkBlueColour];
        }
        else if ([field_val isEqualToString:@"ATTACHMENT"])
        {
            fieldvalue.hidden = NO;
            SepratorView.hidden = NO;
            pickbutton.hidden = NO;
            if ([[Fieldlist[indexPath.row] valueForKey:@"fieldValue"] length]>0)
            {
                fieldvalue.text = [Fieldlist[indexPath.row] valueForKey:@"fieldValue"];
                
            }
            else
            {
                fieldvalue.placeholder = [helper gettranslation:@"LBL_757"];
            }
           
            
            fieldvalue.delegate = self;
            
            pickbutton.frame = CGRectMake(208.0, 0.0, 62.0f, 49.0f);
            pickbutton.backgroundColor = [UIColor clearColor];
            
            [pickbutton setImage:[UIImage imageNamed:@"attach.png"] forState:UIControlStateNormal];
        }
        else if ([field_val isEqualToString:@"DATE"])
        {
          //  fieldvalue.hidden = NO;
            fieldvalue.hidden = NO;
            fieldvalue.userInteractionEnabled = NO;
            SepratorView.hidden = NO;
            pickbutton.hidden = NO;
            if ([[Fieldlist[indexPath.row] valueForKey:@"fieldValue"] length]>0)
            {
               // fieldvalue.text = [helper formatingdate:[Fieldlist[indexPath.row] valueForKey:@"fieldValue"] datetime:@"ActivityDate"];
                 fieldvalue.text = [helper formatingdate:[Fieldlist[indexPath.row] valueForKey:@"fieldValue"] datetime:@"datetime"];
                
            }
            else
            {
                [fieldvalue setText:@"Date Time"];
            }
            
            pickbutton.frame = CGRectMake(fieldvalue.frame.origin.x, fieldvalue.frame.origin.y, SepratorView.frame.size.width, fieldvalue.frame.size.height);
            [pickbutton setImage:[UIImage imageNamed:@"pickdt.png"] forState:UIControlStateNormal];
        }
        else if ([field_val isEqualToString:@"TEXT"])
        {
            fieldvalue.hidden = NO;
            fieldvalue.delegate = self;
            SepratorView.hidden = NO;
            pickbutton.hidden = YES;
            fieldvalue.text = [Fieldlist[indexPath.row] valueForKey:@"fieldValue"];

        }
        else if ([field_val isEqualToString:@"CTEMP"])
        {
            fieldvalue.hidden = NO;
            SepratorView.hidden = NO;
            pickbutton.hidden = NO;
            fieldvalue.delegate = self;
            if ([[Fieldlist[indexPath.row] valueForKey:@"fieldValue"] length]>0)
            {
                NSString *templateName = [self gettemplateName:[Fieldlist[indexPath.row] valueForKey:@"fieldValue"]];
                fieldvalue.text =templateName;
            }
            else
            {
                fieldvalue.placeholder = [helper gettranslation:@"LBL_757"];
            }
            
            
            pickbutton.frame = CGRectMake(208.0, 0.0, 62.0f, 49.0f);
            pickbutton.backgroundColor = [UIColor clearColor];
            
            [pickbutton setImage:[UIImage imageNamed:@"dropdown.png"] forState:UIControlStateNormal];
        }
        else
        {
            fieldvalue.hidden = NO;
            fieldvalue.delegate = self;
            SepratorView.hidden = NO;
            pickbutton.hidden = YES;
            fieldvalue.text = [Fieldlist[indexPath.row] valueForKey:@"fieldValue"];
            fieldvalue.keyboardType = UIKeyboardTypeNumberPad;
            

        }
        //customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
        customcell.tintColor = [helper DarkBlueColour];
    }
    else if (tableView == LovTableview)
    {
        cellIdentifier = @"socustomcell";
        
        customcell = [self.LovTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        UIView *contentView = customcell.contentView;
        UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
        if ([TextfieldFlag isEqualToString:@"AssigneeTextField"])
        {
            lovtypename.text = [NSString stringWithFormat:@"%@(%@)",[LovArr[indexPath.row] valueForKey:@"teamName"],[LovArr[indexPath.row] valueForKey:@"teamid"]];
        }
        if ([TextfieldFlag isEqualToString:@"ActplanTextField"])
        {
            lovtypename.text = [LovArr[indexPath.row] valueForKey:@"plan_name"];
        }
        else if([TextfieldFlag isEqualToString:@"TypeTextField"] || [TextfieldFlag isEqualToString:@"StatusTextField"] || [TextfieldFlag isEqualToString:@"PriorityTextField"])
        {
            lovtypename.text = [LovArr[indexPath.row] valueForKey:@"value"];
        }
    }
    return customcell;

}
-(NSString *)gettemplateName:(NSString *)templateId
{
    NSString *templatename;
    NSArray *TemplateArr = [[helper query_alldata:@"Event_Mail"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"iD like %@",templateId]];
    if ([TemplateArr count]>0)
    {
        templatename = [TemplateArr[0] valueForKey:@"templateName"];
    }
    return templatename;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == LovTableview)
    {
        if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
        {
            [tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
        {
            [tableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)])
        {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    if (tableView == pickup_tbl)
    {

    }
    else if (tableView == dynamicfields_tbl)
    {
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }
    
    
}

-(void)setNotEditable
{
    [TypeTextField setUserInteractionEnabled:NO];
    [PriorityTextField setUserInteractionEnabled:NO];
    [AssigneeTextField setUserInteractionEnabled:NO];
    [ActplanTextField setUserInteractionEnabled:NO];
    [NotesTextfield setUserInteractionEnabled:NO];
    [DescriptionTextfield setUserInteractionEnabled:NO];
    [StartDateOutlet setUserInteractionEnabled:NO];
    [EndDateOutlet setUserInteractionEnabled:NO];
    [save_btn setHidden:YES];
    [StatusTextField setUserInteractionEnabled:NO];
    [StatusTextField setEnabled:NO];
    headertitle_lbl.text=[helper gettranslation:@"LBL_816"];
    [dynamicfields_tbl setUserInteractionEnabled:NO];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    if (tableView == pickup_tbl)
    {
        UITableViewCell *custcell = (UITableViewCell *)[dynamicfields_tbl cellForRowAtIndexPath:selindexpath];
        
        UIView *contentView = custcell.contentView;
        
        UITextField *newfieldvalue = (UITextField *) [contentView viewWithTag:3];
        NSString *attachname;
        if ([pickentity isEqualToString:@"Event_Attachments"])
        {
            attachname = [Piclistdata[indexPath.row] valueForKey:@"filename"];
            newfieldvalue.text =  attachname;
            [newfieldvalue resignFirstResponder];
            [DynamicFieldDict setValue:attachname forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
            //[RequiredFieldArray setValue:[Fieldlist[selindexpath.row] valueForKey:@"required"] forKey:@"RequiredFields"];
        }
        else if ([pickentity isEqualToString:@"CustomPickup"])
        {
            newfieldvalue.text =  [Piclistdata[indexPath.row] valueForKey:@"lic"];
            [DynamicFieldDict setValue:[Piclistdata[indexPath.row] valueForKey:@"lic"] forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
           // [RequiredFieldArray setValue:[Fieldlist[selindexpath.row] valueForKey:@"required"] forKey:@"RequiredFields"];
            
            [newfieldvalue resignFirstResponder];
        }
        else if ([pickentity isEqualToString:@"Event_Mail"])
        {
            newfieldvalue.text = [Piclistdata[indexPath.row] valueForKey:@"templateName"];
            [DynamicFieldDict setValue:[Piclistdata[indexPath.row] valueForKey:@"iD"] forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
            // [RequiredFieldArray setValue:[Fieldlist[selindexpath.row] valueForKey:@"required"] forKey:@"RequiredFields"];
            [newfieldvalue resignFirstResponder];
        }
        pickup_tbl.hidden = YES;
    }
    else if (tableView == dynamicfields_tbl)
    {
        
        
    }
    else if (tableView == LovTableview)
    {
        if ([TextfieldFlag isEqualToString:@"TypeTextField"])
        {
            TypeTextField.text = [LovArr[indexPath.row] valueForKey:@"value"];
            [TypeTextField resignFirstResponder];
        }
        else if ([TextfieldFlag isEqualToString:@"StatusTextField"])
        {
            StatusTextField.text = [LovArr[indexPath.row] valueForKey:@"value"];
            [StatusTextField resignFirstResponder];
        }
        else if ([TextfieldFlag isEqualToString:@"PriorityTextField"])
        {
            PriorityTextField.text = [LovArr[indexPath.row] valueForKey:@"value"];
            [PriorityTextField resignFirstResponder];
        }
        else if ([TextfieldFlag isEqualToString:@"AssigneeTextField"])
        {
           
            AssigneeTextField.text = [NSString stringWithFormat:@"%@(%@)",[LovArr[indexPath.row] valueForKey:@"teamName"],[LovArr[indexPath.row] valueForKey:@"teamid"]];
            [AssigneeTextField resignFirstResponder];
//            if ([helper getLic:@"" value:StatusTextField.text]) {
//                <#statements#>
//            }
            StatusTextField.text = [helper getvaluefromlic:@"ACT_STATUS" lic:@"Assigned"];
        }
        else if ([TextfieldFlag isEqualToString:@"ActplanTextField"])
        {
      
            ActplanTextField.text = [LovArr[indexPath.row] valueForKey:@"plan_name"];
            [ActplanTextField resignFirstResponder];
        }
    
        _dropdown_view.hidden = YES;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)pickbutton_action:(UIButton *)sender
{
    SOCustomCell *cell = (SOCustomCell *)[[[sender superview]superview] superview];
    NSIndexPath *indexPath = [self.dynamicfields_tbl indexPathForCell:cell];
    selindexpath = indexPath;
    NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldTypeLic"] ];
   // NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldtype"] ];
    if ([field_val isEqualToString:@"DATE"])
    {
        pickentity = @"CheckinDate";
        startEndDate = @"CheckinDate";
        HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
        hsdpvc.delegate = self;
        if (self.selectedDate) {
            hsdpvc.date = self.selectedDate;
        }
        [self presentViewController:hsdpvc animated:YES completion:nil];

    }
}
#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == LovTableview)
    {
        CGFloat height = LovTableview.contentSize.height;
        CGFloat maxHeight = LovTableview.superview.frame.size.height - LovTableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LovTableview.frame;
            if([Fieldlist count]>0)
                {
                    //codefields are available
                    if([TextfieldFlag isEqualToString:@"TypeTextField"])
                    {
                        frame.origin.x = 86;
                        frame.origin.y = 40;
                      
                    }
                    if([TextfieldFlag isEqualToString:@"StatusTextField"])
                    {
                        frame.origin.x = 386;
                        frame.origin.y = 40;
                       
                    }
                    if([TextfieldFlag  isEqualToString:@"PriorityTextField"])
                    {
                        frame.origin.x = 86;
                        frame.origin.y = 162;
                        
                    }
                    if([TextfieldFlag isEqualToString:@"AssigneeTextField"])
                    {
                        frame.origin.x = 386;
                        frame.origin.y = 165;
                      
                    }
                    if ([TextfieldFlag isEqualToString:@"ActplanTextField"])
                    {
                        frame.origin.x = 386;
                        frame.origin.y = 285;
                        
                    }
                    
                }
                else
                {
                    //no codefields available
                    if([TextfieldFlag isEqualToString:@"TypeTextField"])
                    {
                        frame.origin.x = 276;
                        frame.origin.y = 40;
                      
                    }
                    if([TextfieldFlag isEqualToString:@"StatusTextField"])
                    {
                        frame.origin.x = 576;
                        frame.origin.y = 40;
                      
                    }
                    if([TextfieldFlag  isEqualToString:@"PriorityTextField"])
                    {
                        frame.origin.x = 267;
                        frame.origin.y = 162;
                       
                    }
                    if([TextfieldFlag isEqualToString:@"AssigneeTextField"])
                    {
                        frame.origin.x = 576;
                        frame.origin.y = 162;
                       
                    }
                    if ([TextfieldFlag isEqualToString:@"ActplanTextField"])
                    {
                        frame.origin.x = 576;
                        frame.origin.y = 285;
                       
                    }
                }
            _dropdown_view.frame = frame;
            }];
    }
   
    
}


#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint textfieldPosition = [textField convertPoint:CGPointZero toView:dynamicfields_tbl];
    NSIndexPath *indexPath = [dynamicfields_tbl indexPathForRowAtPoint:textfieldPosition];
    NSLog(@"Indexpath here is %ld",(long)indexPath.row);
    selindexpath =indexPath;
    
    if (indexPath == nil)
    {
        if (pickup_tbl.hidden == NO)
        {
            pickup_tbl.hidden = YES;
            
          
        }
        if (textField == TypeTextField)
        {
            TextfieldFlag = @"TypeTextField";
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
            [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_266"];
        }
        else if (textField == StatusTextField)
        {
            TextfieldFlag = @"StatusTextField";
            StatusLic = [helper getLic:@"ACT_STATUS" value:[EditActivityRecordArr[0] valueForKey:@"status"]];
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_STATUS"]];
            if ([StatusLic isEqualToString:@"Planned"])
            {
                LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Assigned",@"Cancelled"]];
            }
            else if ([StatusLic isEqualToString:@"Assigned"])
            {
                LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Inprogress",@"Cancelled"]];
            }
            else if ([StatusLic isEqualToString:@"Inprogress"])
            {
                LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Completed",@"Cancelled"]];
            }
            else if ([StatusLic isEqualToString:@"Cancelled"])
            {
                LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Assigned"]];
            }
            else if ([StatusLic isEqualToString:@"Completed"])
            {
                LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Inprogress"]];
            }
            
            [textField resignFirstResponder];
            
            [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_242"];
            
        }
        else if (textField == PriorityTextField)
        {
            TextfieldFlag = @"PriorityTextField";
            LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];
            [helper MoveViewUp:YES Height:0 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_203"];
            
        }
        else if (textField == AssigneeTextField)
        {
            TextfieldFlag = @"AssigneeTextField";
            LovArr = [[helper query_alldata:@"Event_Team"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            LovArr=[LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"typelic like %@",@"User"]];
            [helper MoveViewUp:YES Height:0 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_053"];
        }
        else if (textField == ActplanTextField)
        {
            TextfieldFlag = @"ActplanTextField";
            LovArr = PlanNames;
            [helper MoveViewUp:YES Height:150 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_007"];
        }
        else if (textField == NotesTextfield)
        {
            [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
            stringlength = 255;

        }
        if (textField == TypeTextField || textField == StatusTextField || textField == PriorityTextField || textField == AssigneeTextField || textField == ActplanTextField)
        {
            [LovTableview reloadData];
            [self adjustHeightOfTableview:LovTableview];
            LovTableview.hidden = NO;
//            if ([StatusLic isEqualToString:@"Completed"])
//            {
//                _dropdown_view.hidden = YES;
//            }
//            else
//            {
                _dropdown_view.hidden = NO;
//            }
        }
        if(textField == DescriptionTextfield)
        {
            stringlength = 100;
        }
    }
    else
    {
        if (_dropdown_view.hidden == NO)
        {
            _dropdown_view.hidden = YES;
        }
        CGRect rectOfCellInTableView = [dynamicfields_tbl rectForRowAtIndexPath: indexPath];
        CGRect rectOfCellInSuperview = [dynamicfields_tbl convertRect: rectOfCellInTableView toView: dynamicfields_tbl.superview];
        
        
        NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldTypeLic"] ];
       // NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldtype"] ];
        if ([field_val isEqualToString:@"PICKLIST"])
        {
            pickentity = @"CustomPickup";
            
            NSString *pickLOV = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"picklistType"] ];
            Piclistdata =  [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",pickLOV]];
            [pickup_tbl reloadData];
            [pickup_tbl reloadInputViews];
            
            
            
            pickup_tbl.hidden = NO;
            pickup_tbl.frame = CGRectMake(dynamicfields_tbl.frame.origin.x, rectOfCellInSuperview.origin.y+60, 260.0f, 270.0f);
            [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
            
        }
        else if ([field_val isEqualToString:@"SWITCH"])
        {
            
        }
        else if ([field_val isEqualToString:@"ATTACHMENT"])
        {
            pickentity = @"Event_Attachments";
            
            Piclistdata = [[helper query_alldata:pickentity] filteredArrayUsingPredicate:event_pred];
            [pickup_tbl reloadData];
           // [pickup_tbl reloadInputViews];
            pickup_tbl.hidden = NO;
            pickup_tbl.frame = CGRectMake(dynamicfields_tbl.frame.origin.x, rectOfCellInSuperview.origin.y+60, 260.0f, 270.0f);
            [helper MoveViewUp:YES Height:180 ViewToBeMoved:self.view];
            
        }
        else if ([field_val isEqualToString:@"DATE"])
        {

        }
        else if ([field_val isEqualToString:@"TEXT"])
        {
            [helper MoveViewUp:YES Height:150 ViewToBeMoved:self.view];
        }
        else if ([field_val isEqualToString:@"CTEMP"])
        {
            pickentity = @"Event_Mail";
            //event_pred =  [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];

            NSArray *ARR = [helper query_alldata:@"Event_Mail"];
            Piclistdata = [[helper query_alldata:pickentity] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@ AND categoryLIC like %@",appdelegate.eventid,@"Communication"]];
            [pickup_tbl reloadData];
            // [pickup_tbl reloadInputViews];
            pickup_tbl.hidden = NO;
            pickup_tbl.frame = CGRectMake(dynamicfields_tbl.frame.origin.x, rectOfCellInSuperview.origin.y+60, 260.0f, 270.0f);
           // [helper MoveViewUp:YES Height:180 ViewToBeMoved:self.view];

        }
    }
    
    
    
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGPoint textfieldPosition = [textField convertPoint:CGPointZero toView:dynamicfields_tbl];
    NSIndexPath *indexPath = [dynamicfields_tbl indexPathForRowAtPoint:textfieldPosition];
    NSLog(@"Indexpath here is %ld",(long)indexPath.row);
    
    if (indexPath == nil)
    {
        if (textField == TypeTextField)
        {
            [helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
            
        }
        else if (textField == StatusTextField)
        {
            [helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
            
           if(![helper isdropdowntextCorrect:StatusTextField.text lovtype:@"ACT_STATUS"])
           {
               StatusTextField.text = @"";
               [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_908"] action:[helper gettranslation:@"LBL_462"]];
               validStatus = @"NO";
           }
            else
                validStatus =@"YES";
            
            [textField resignFirstResponder];
        }
        else if (textField == PriorityTextField)
        {
            [helper MoveViewUp:NO Height:0 ViewToBeMoved:self.view];
            
        }
        else if (textField == AssigneeTextField)
        {
            [helper MoveViewUp:NO Height:0 ViewToBeMoved:self.view];
        }
        else if (textField == ActplanTextField)
        {
            [helper MoveViewUp:NO Height:150 ViewToBeMoved:self.view];
        }
        else if (textField == NotesTextfield)
        {
            [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
        }
    }
    else
    {
        NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldTypeLic"] ];
       // NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldtype"] ];
        if ([field_val isEqualToString:@"ATTACHMENT"])
        {
             [helper MoveViewUp:NO Height:180 ViewToBeMoved:self.view];
        }
        if ([field_val isEqualToString:@"PICKLIST"])
        {
            [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
        }
        if ([field_val isEqualToString:@"TEXT"])
        {
            UITableViewCell *custcell = (UITableViewCell *)[dynamicfields_tbl cellForRowAtIndexPath:selindexpath];
            
            UIView *contentView = custcell.contentView;
            
            UITextField *fieldvalue = (UITextField *) [contentView viewWithTag:3];
            
           if([textField.text length]>0)
               [DynamicFieldDict setValue:fieldvalue.text forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
            
           // [RequiredFieldArray setValue:[Fieldlist[selindexpath.row] valueForKey:@"required"] forKey:@"RequiredFields"];
           // NSLog(@"Filed name value is %@",fieldvalue.text);
             [helper MoveViewUp:NO Height:150 ViewToBeMoved:self.view];
           
        }
        if ([field_val isEqualToString:@"CTEMP"])
        {
            NSLog(@"Dynamic fields are already set in did select method");
        }
        else
        {
            //for numeric
            UITableViewCell *custcell = (UITableViewCell *)[dynamicfields_tbl cellForRowAtIndexPath:selindexpath];
            
            UIView *contentView = custcell.contentView;
            
            UITextField *fieldvalue = (UITextField *) [contentView viewWithTag:3];
            if([fieldvalue.text length]>0)
            [DynamicFieldDict setValue:fieldvalue.text forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
            
           // [RequiredFieldArray setValue:[Fieldlist[selindexpath.row] valueForKey:@"required"] forKey:@"RequiredFields"];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
       return NO;
}

-(void)textFieldDidChange :(UITextField *)theTextField
{
    CGPoint textfieldPosition = [theTextField convertPoint:CGPointZero toView:dynamicfields_tbl];
    NSIndexPath *indexPath = [dynamicfields_tbl indexPathForRowAtPoint:textfieldPosition];
    
    NSString *searchText = theTextField.text;

   
    if (indexPath == nil)
    {
        
       if (count > searchText.length)
        {
            if (theTextField == TypeTextField)
            {
                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
            }
//            else if (theTextField == StatusTextField)
//            {
//                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_STATUS"]];
//                if ([StatusLic isEqualToString:@"Planned"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Assigned",@"Cancelled"]];
//                }
//                else if ([StatusLic isEqualToString:@"Assigned"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Inprogress",@"Cancelled"]];
//                }
//                else if ([StatusLic isEqualToString:@"Inprogress"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Completed",@"Cancelled"]];
//                }
//                else if ([StatusLic isEqualToString:@"Cancelled"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Assigned"]];
//                }
////                else if ([StatusLic isEqualToString:@"Completed"])
////                {
////                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Inprogress"]];
////                }
//
//            }
            else if (theTextField == PriorityTextField)
            {
                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];

            }
            else if (theTextField == AssigneeTextField)
            {
               // LovArr = [helper query_alldata:@"User"];
                 LovArr = [[helper query_alldata:@"Event_Team"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
                
                if(AssigneeTextField.text.length==0)
                {
                    StatusTextField.text=[helper getvaluefromlic:@"ACT_STATUS" lic:@"Planned"];
                }
            }
            else if (theTextField == ActplanTextField)
            {
                LovArr = PlanNames;;
            }
        }
       
        if(searchText.length == 0)
        {
           
            if (theTextField == TypeTextField)
            {
                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
            }
//            else if (theTextField == StatusTextField)
//            {
//                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_STATUS"]];
//                if ([StatusLic isEqualToString:@"Planned"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Assigned",@"Cancelled"]];
//                }
//                else if ([StatusLic isEqualToString:@"Assigned"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Inprogress",@"Cancelled"]];
//                }
//                else if ([StatusLic isEqualToString:@"Inprogress"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Completed",@"Cancelled"]];
//                }
//                else if ([StatusLic isEqualToString:@"Cancelled"])
//                {
//                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Assigned"]];
//                }
////                else if ([StatusLic isEqualToString:@"Completed"])
////                {
////                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Inprogress"]];
////                }
//
//
//
//            }
            else if (theTextField == PriorityTextField)
            {
                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];
                
            }
            else if (theTextField == AssigneeTextField)
            {
               // LovArr = [helper query_alldata:@"User"];
                 LovArr = [[helper query_alldata:@"Event_Team"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
            }
            else if (theTextField == ActplanTextField)
            {
                LovArr = PlanNames;
            }

        }
        else
        {
          
           if (theTextField == TypeTextField)
           {
               LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_TYPE"]];
               count = searchText.length;
               [filteredpartArray removeAllObjects];
               NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
               filteredpartArray = [NSMutableArray arrayWithArray:[LovArr filteredArrayUsingPredicate:filterpredicate]];
               LovArr = [filteredpartArray copy];
            }
            else if (theTextField == StatusTextField)
            {
                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ACT_STATUS"]];
                if ([StatusLic isEqualToString:@"Planned"])
                {
                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Assigned",@"Cancelled"]];
                }
                else if ([StatusLic isEqualToString:@"Assigned"])
                {
                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Inprogress",@"Cancelled"]];
                }
                else if ([StatusLic isEqualToString:@"Inprogress"])
                {
                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Completed",@"Cancelled"]];
                }
                else if ([StatusLic isEqualToString:@"Cancelled"])
                {
                    LovArr = [LovArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Assigned"]];
                }

                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[LovArr filteredArrayUsingPredicate:filterpredicate]];
                LovArr = [filteredpartArray copy];
            }
            else if (theTextField == PriorityTextField)
            {
                LovArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRIORITY"]];
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[LovArr filteredArrayUsingPredicate:filterpredicate]];
                LovArr = [filteredpartArray copy];
            }
            else if (theTextField == AssigneeTextField)
            {
                //LovArr = [helper query_alldata:@"User"];
                 LovArr = [[helper query_alldata:@"Event_Team"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.teamName contains[c] %@)",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[LovArr filteredArrayUsingPredicate:filterpredicate]];
                LovArr = [filteredpartArray copy];
            }
            else if (ActplanTextField)
            {
               LovArr = PlanNames;
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.plan_name contains[c] %@)",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[LovArr filteredArrayUsingPredicate:filterpredicate]];
                LovArr = [filteredpartArray copy];
            }
            
        }
 
    if (theTextField == TypeTextField || theTextField == StatusTextField || theTextField == PriorityTextField || theTextField == AssigneeTextField || theTextField == ActplanTextField)
    {
        [LovTableview reloadData];
    }
    }
    else
    {
        //dynamic fields
        NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldTypeLic"] ];
       // NSString *field_val = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"fieldtype"] ];
        if (count > searchText.length)
        {
           if ([field_val isEqualToString:@"ATTACHMENT"])
            {
                Piclistdata = nil;
                if ([pickentity isEqualToString: @"Event_Attachments"])
                {
                    Piclistdata = [[helper query_alldata:pickentity] filteredArrayUsingPredicate:event_pred];
                }
            }
            else if ([field_val isEqualToString:@"PICKLIST"])
            {
                NSString *pickLOV = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"picklistType"] ];
                Piclistdata =  [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",pickLOV]];

            }
            else if ([field_val isEqualToString:@"CTEMP"])
            {
                Piclistdata = [[helper query_alldata:@"Event_Mail"]filteredArrayUsingPredicate:event_pred];
            }
        }
        if(searchText.length == 0)
        {
             if ([field_val isEqualToString:@"ATTACHMENT"])
             {
                 if ([pickentity isEqualToString: @"Event_Attachments"])
                 {
                     Piclistdata = [[helper query_alldata:pickentity] filteredArrayUsingPredicate:event_pred];
                 }
             }
             else if ([field_val isEqualToString:@"PICKLIST"])
             {
                 NSString *pickLOV = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"picklistType"] ];
                 Piclistdata =  [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",pickLOV]];

             }
             else if ([field_val isEqualToString:@"CTEMP"])
             {
                 Piclistdata = [[helper query_alldata:@"Event_Mail"]filteredArrayUsingPredicate:event_pred];
             }
        }
        else
        {
            if ([field_val isEqualToString:@"ATTACHMENT"])
             {
                 if ([pickentity isEqualToString: @"Event_Attachments"])
                 {
                     Piclistdata = [[helper query_alldata:pickentity] filteredArrayUsingPredicate:event_pred];
                     count = searchText.length;
                     [filteredpartArray removeAllObjects];
                     NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"((SELF.filename contains[c] %@) OR (SELF.filesize contains[c] %@) OR (SELF.filetype contains[c] %@)) ",searchText,searchText,searchText];
                     filteredpartArray = [NSMutableArray arrayWithArray:[Piclistdata filteredArrayUsingPredicate:filterpredicate]];
                     Piclistdata = [filteredpartArray copy];
                 }
             }
            else if ([field_val isEqualToString:@"PICKLIST"])
            {
                NSString *pickLOV = [NSString stringWithFormat:@"%@", [Fieldlist[indexPath.row] valueForKey:@"picklistType"] ];
                Piclistdata =  [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",pickLOV]];
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.value contains[c] %@) ",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[Piclistdata filteredArrayUsingPredicate:filterpredicate]];
                Piclistdata = [filteredpartArray copy];

            }
            else if ([field_val isEqualToString:@"CTEMP"])
            {
                Piclistdata = [[helper query_alldata:@"Event_Mail"]filteredArrayUsingPredicate:event_pred];
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.templateName contains[c] %@) ",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[Piclistdata filteredArrayUsingPredicate:filterpredicate]];
                Piclistdata = [filteredpartArray copy];

            }
        }
        if ([field_val isEqualToString:@"ATTACHMENT"] || [field_val isEqualToString:@"PICKLIST"] || [field_val isEqualToString:@"CTEMP"] )
        {
            [pickup_tbl reloadData];
            [pickup_tbl reloadInputViews];
        }


    }
   
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // NSLog(@"touches began");
    UITouch *touch = [touches anyObject];
    
    if([touch view] != pickup_tbl)
    {
        pickup_tbl.hidden = YES;

        [self.view endEditing:YES];
    }
    if (_dropdown_view.hidden == NO)
    {
       // LovTableview.hidden = YES;
         _dropdown_view.hidden = YES;
        [TypeTextField resignFirstResponder];
        [PriorityTextField resignFirstResponder];
        [StatusTextField resignFirstResponder];
        [AssigneeTextField resignFirstResponder];
        [ActplanTextField resignFirstResponder];
        [NotesTextfield resignFirstResponder];
    }
}
#pragma mark start and end date
-(IBAction)StartDateButtonAction:(id)sender
{
    if (pickup_tbl.hidden == NO)
    {
        pickup_tbl.hidden = YES;
        
        [self.view endEditing:YES];
    }
    if (_dropdown_view.hidden == NO)
    {
        _dropdown_view.hidden = YES;
        [self.view endEditing:YES];
    }
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    startEndDate = @"StartDate";

    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];

    
    hsdpvc=[[HSDatePickerViewController alloc]init];
    NSString *eventEndDate=[self.EndDateOutlet.titleLabel text];
    
    hsdpvc.delegate = self;
    hsdpvc.minDate=[NSDate date];
    hsdpvc.date=[dateFormatter1 dateFromString:[dateFormatter1 stringFromDate:[NSDate date]]];
    if(eventEndDate.length>0)
    {
        
        NSDate *evntEndDate=[dateFormatter1 dateFromString:eventEndDate];
        
        hsdpvc.maxDate=evntEndDate;
        
        
    }
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

-(IBAction)EndDateButtonAction:(id)sender
{
    if (pickup_tbl.hidden == NO)
    {
        pickup_tbl.hidden = YES;
        
        [self.view endEditing:YES];
    }
    if (_dropdown_view.hidden == NO)
    {
        _dropdown_view.hidden = YES;
        [self.view endEditing:YES];
    }
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    startEndDate = @"EndDate";
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];

    
    NSString *activityStartDate=[self.StartDateOutlet.titleLabel text];
    hsdpvc.delegate = self;
  
    
    if(activityStartDate.length>0)
    {
        
        NSDate *actStartDate=[dateFormatter1 dateFromString:activityStartDate];
        
        hsdpvc.date = actStartDate;
        
        hsdpvc.minDate = actStartDate;
    }
    else
        hsdpvc.minDate=[NSDate date];
    
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}
#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    
    
    
    if ([startEndDate isEqualToString:@"StartDate"])
    {
      [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
      [StartDateOutlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    }
    else if ([startEndDate isEqualToString:@"EndDate"])
    {
        [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
        [EndDateOutlet setTitle:[dateFormatter1 stringFromDate:date] forState:UIControlStateNormal];
    }
    else if ([startEndDate isEqualToString:@"CheckinDate"])
    {
        [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
        UITableViewCell *custcell = (UITableViewCell *)[dynamicfields_tbl cellForRowAtIndexPath:selindexpath];
        
        UIView *contentView = custcell.contentView;
        
        UITextField *newfieldvalue = (UITextField *) [contentView viewWithTag:3];
        newfieldvalue.hidden = NO;
      

        if([pickentity isEqualToString:@"CheckinDate"])
        {
          
            newfieldvalue.text = [dateFormatter1 stringFromDate:date];
            newfieldvalue.userInteractionEnabled = NO;
           // [DynamicFieldDict setValue:[helper converingformateddatetooriginaldate:newfieldvalue.text datetype:@"ServerDate"] forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
            [DynamicFieldDict setValue:[helper converingformateddatetooriginaldate:newfieldvalue.text datetype:@"ExpenseDate"] forKey:[Fieldlist[selindexpath.row] valueForKey:@"id"]];
            
            
            // [RequiredFieldArray addObject:[Fieldlist[selindexpath.row] valueForKey:@"required"]];
        }
    }
    
    self.selectedDate = date;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == NotesTextfield || textField == DescriptionTextfield)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;;
    }
}


@end
