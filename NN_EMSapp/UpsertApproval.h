//
//  UpsertApproval.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/30/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SOCustomCell.h"
#import "SLKeyChainStore.h"

@interface UpsertApproval : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITableView *LOVTableview;
}
@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic)NSString *attendeeid;


@property (strong, nonatomic) IBOutlet UITextField *approvalfor_tv;
@property (strong, nonatomic) IBOutlet UITextField *status_tv;
@property (strong, nonatomic) IBOutlet UITextField *approver_tv;
@property (strong, nonatomic) IBOutlet UITextField *PRapprover_tv;
@property(strong,nonatomic) NSMutableArray *filteredpartArray;
@property (strong, nonatomic) IBOutlet UITextField *comment_tv;
@property (strong, nonatomic) IBOutlet UIView *MainView;

@property(strong,nonatomic)NSString *senderview;
@property(strong,nonatomic)NSString *cancelsegue;
@property (strong, nonatomic) IBOutlet UILabel *viewtitle_lbl;

- (IBAction)save_btn:(id)sender;
- (IBAction)cancel_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *dropdown_view;
@property (strong, nonatomic) IBOutlet UILabel *dropdown_lbl;

@property (strong, nonatomic) IBOutlet UILabel *approvalfor_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *approver_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *prapprovalname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *comment_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *budgetappr;
@property (strong, nonatomic) IBOutlet UILabel *estcost_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *apprcost_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *approverName_ulbl;
@property (strong, nonatomic) IBOutlet UITextField *approverName_tf;



@property (strong, nonatomic) IBOutlet UITextField *EstimatedAmountTextField;

@property (strong, nonatomic) IBOutlet UITextField *ApprovedAmountTextField;

@property (strong, nonatomic) IBOutlet UILabel *EstimatedAmountDisplaylbl;
@property (strong, nonatomic) IBOutlet UILabel *ApprovedAmountDisplaylbl;
@property (strong, nonatomic) IBOutlet UIView *ApprovedAmountHorizontalView;
@property (strong, nonatomic) IBOutlet UIView *EstimatedAmountHorizontalView;
- (IBAction)BudgetAppoverSwitchValueChangedAction:(id)sender;

@property (strong, nonatomic) IBOutlet UISwitch *BudgetApproverOutlet;

@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;


@end
