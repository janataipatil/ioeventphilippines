//
//  UpsertApproval.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/30/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "UpsertApproval.h"

@interface UpsertApproval ()
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSArray *Eventapprovallist;
    NSArray *Eventlovlist;
    SLKeyChainStore *keychain;
    NSString *generatedrowid;
    NSArray *LOVArr;
    NSString *transaction_type;
    NSDateFormatter *dateFormatter;
    NSArray *ParApproverArr;
    NSString *TextfieldFlag;
    NSUInteger count;
    NSString *ParentApprovalID;
    NSString *canEdit;
    NSString *canDelete;
    NSString *ApprovalFlag;
    NSString *BudgetApproverSwitch;
    NSUInteger stringlength;
    NSArray *Eventlist;
    NSString *StatusLic;
    NSString *approvalStatusLic;
    NSString *currencyCode;
    NSString *approverID;
    NSString *approvedCost;
    NSString *estimatedCost;
    NSString *ApproverTypeLIC;
}
@end

@implementation UpsertApproval
@synthesize helper,senderview,cancelsegue,filteredpartArray,MainView,EstimatedAmountDisplaylbl,EstimatedAmountHorizontalView,ApprovedAmountDisplaylbl,ApprovedAmountHorizontalView,BudgetApproverOutlet,ApprovedAmountTextField,EstimatedAmountTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    Eventlovlist = [helper query_alldata:@"Event_LOV"];
 
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    BudgetApproverOutlet.transform = CGAffineTransformMakeScale(0.60, 0.60);
   // BudgetApproverOutlet.onTintColor = [UIColor colorWithRed:51/255.0F green:76/255.0F blue:104/255.0F alpha:1.0F];
    
    currencyCode=[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"];
    
    BudgetApproverOutlet.onTintColor = [helper DarkBlueColour];
    
   appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    if (appdelegate.approvalid.length >0)
    {
        senderview = @"Editapproval";
    }
    else
    {
        senderview = @"Addapproval";
    }
    
    
    if([senderview isEqualToString:@"Addapproval"])
    {
        generatedrowid = [helper generate_rowId];
        transaction_type = @"Add New Approval";
        _viewtitle_lbl.text = [helper gettranslation:@"LBL_013"];
        
        ParApproverArr =  [[helper query_alldata:@"Event_Approvals"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        canEdit = @"Y";
        canDelete = @"Y";
        ApprovalFlag = @"Y";
        [BudgetApproverOutlet setOn:NO];
        BudgetApproverSwitch = @"N";
        CGRect Frame;
        Frame = MainView.frame;
        Frame.size.height = MainView.frame.size.height - 65;
        MainView.frame = Frame;
        EstimatedAmountDisplaylbl.hidden = YES;
        ApprovedAmountDisplaylbl.hidden = YES;
        EstimatedAmountHorizontalView.hidden = YES;
        ApprovedAmountHorizontalView.hidden = YES;
        _status_tv.text = [helper getvaluefromlic:@"APR_STATUS" lic:@"Planned"];
//        approvedCost = @"0";
//        estimatedCost = @" "
        
    }
    else if([senderview isEqualToString:@"Editapproval"] )
    {
        [self setupaprovalview];
        if([approvalStatusLic isEqualToString:@"Approved"] || [canEdit isEqualToString:@"N"]) //ReadOnly if the status is approved
        {
            [_status_tv setUserInteractionEnabled:NO];
            [_approver_tv setUserInteractionEnabled:NO];
            [_PRapprover_tv setUserInteractionEnabled:NO];
            [_comment_tv setUserInteractionEnabled:NO];
            [EstimatedAmountTextField setUserInteractionEnabled:NO];
            [ApprovedAmountTextField setUserInteractionEnabled:NO];
            [BudgetApproverOutlet setUserInteractionEnabled:NO];
            [_approvalfor_tv setUserInteractionEnabled:NO];
            [_approverName_tf setUserInteractionEnabled:NO];
         //   _viewtitle_lbl.text=[helper gettranslation:@"LBL_814"];
            transaction_type = [helper gettranslation:@"LBL_814"];
            [_savebtn_ulbl setHidden:YES];
        }
        else
        {
            [_status_tv setUserInteractionEnabled:YES];
            [_approver_tv setUserInteractionEnabled:YES];
            [_PRapprover_tv setUserInteractionEnabled:YES];
            [_comment_tv setUserInteractionEnabled:YES];
            [_approverName_tf setUserInteractionEnabled:YES];
            [EstimatedAmountTextField setUserInteractionEnabled:YES];
            [ApprovedAmountTextField setUserInteractionEnabled:YES];
            [BudgetApproverOutlet setUserInteractionEnabled:YES];
            [_approvalfor_tv setUserInteractionEnabled:YES];
            [_savebtn_ulbl setHidden:NO];
            transaction_type = [helper gettranslation:@"LBL_107"];
        }
        
        generatedrowid = appdelegate.approvalid;
        _viewtitle_lbl.text =transaction_type;
    }
    
     StatusLic = [helper getLic:@"APR_STATUS" value:_status_tv.text];
    // Do any additional setup after loading the view.
    LOVTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableview.layer.borderWidth = 1;
    
    [_approvalfor_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_status_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_approver_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_PRapprover_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_comment_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_approverName_tf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    
    [self langsetuptranslations];
}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _approvalfor_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_245"]];
    [self markasrequired:_approvalfor_ulbl];
    
    _status_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_242"]];
    [self markasrequired:_status_ulbl];
    
    _approver_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_849"]];
    [self markasrequired:_approver_ulbl];
    
    _approverName_ulbl.text=[NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_050"]];
    [self markasrequired:_approverName_ulbl];

    
    _prapprovalname_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_189"]];
    _comment_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_066"]];
    
    
    _budgetappr.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_059"]];
    
    
    _estcost_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_128"]];
    _apprcost_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_049"]];
    
}

-(void) markasrequired:(UILabel *) label
{
    NSUInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupaprovalview
{
    Eventapprovallist = [helper query_alldata:@"Event_Approvals"];
    Eventapprovallist = [Eventapprovallist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.approvalid]];
    if ([Eventapprovallist count]>0)
    {
        _approvalfor_tv.text = [Eventapprovallist[0] valueForKey:@"approvalfor"];
        _status_tv.text = [Eventapprovallist[0] valueForKey:@"status"];
        
        _approver_tv.text = [helper getvaluefromlic:@"APPROVER_TYPE" lic:[Eventapprovallist[0] valueForKey:@"approverTypeLIC"]];
        _approverName_tf.text=[Eventapprovallist[0] valueForKey:@"approverName"];
        
       // _approverName_tf.text=[helper]
        _PRapprover_tv.text = [Eventapprovallist[0] valueForKey:@"parentapprovalname"];
        _PRapprover_tv.placeholder=[helper gettranslation:@"LBL_757"];
        _approver_tv.placeholder=[helper gettranslation:@"LBL_757"];
        _comment_tv.text = [Eventapprovallist[0] valueForKey:@"comment"];
        
        ParApproverArr =  [[helper query_alldata:@"Event_Approvals"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        ParApproverArr = [ParApproverArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (id CONTAINS %@)", appdelegate.approvalid]];
        ParApproverArr = [ParApproverArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (parentroutingid CONTAINS %@)", appdelegate.approvalid]];
    
        
        canEdit = [Eventapprovallist[0] valueForKey:@"canEdit"];
        canDelete = [Eventapprovallist[0] valueForKey:@"canDelete"];
        ApprovalFlag = [Eventapprovallist[0] valueForKey:@"approvalflag"];
        
        approvalStatusLic=[Eventapprovallist[0] valueForKey:@"statuslic"];
        
        if ([[Eventapprovallist[0] valueForKey:@"budgetapprover"] isEqualToString:@"Y"])
        {
            BudgetApproverSwitch = @"Y";
            [BudgetApproverOutlet setOn:YES];
        }
        else if([[Eventapprovallist[0] valueForKey:@"budgetapprover"] isEqualToString:@"N"])
        {
            BudgetApproverSwitch = @"N";
            [BudgetApproverOutlet setOn:NO];
            CGRect Frame;
            Frame = MainView.frame;
            Frame.size.height = MainView.frame.size.height - 65;
            MainView.frame = Frame;
            EstimatedAmountDisplaylbl.hidden = YES;
            ApprovedAmountDisplaylbl.hidden = YES;
            EstimatedAmountHorizontalView.hidden = YES;
            ApprovedAmountHorizontalView.hidden = YES;
            

        }
            
        if ([canEdit isEqualToString:@"N"])
        {
            _approvalfor_tv.userInteractionEnabled = NO;
            _status_tv.userInteractionEnabled = NO;
            _approver_tv.userInteractionEnabled = NO;
            _PRapprover_tv.userInteractionEnabled = NO;
            _comment_tv.userInteractionEnabled = NO;
        }

        if ([ApprovalFlag isEqualToString:@"Y"])
        {
            _status_tv.userInteractionEnabled = YES;
            _comment_tv.userInteractionEnabled = YES;
        }
        
        
        Eventlist = [helper query_alldata:@"Event"];
        Eventlist = [Eventlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
        if([Eventlist count]>0)
        {
//            EstimatedAmountTextField.text = [helper stringtocurrency:[Eventlist[0] valueForKey:@"estimatedcost"]];
            
            EstimatedAmountTextField.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"estimatedcost"]];
            
            ApprovedAmountTextField.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"approvedcost"]];
            
            approvedCost = [Eventlist[0] valueForKey:@"approvedcost"];
            estimatedCost = [Eventlist[0] valueForKey:@"estimatedcost"];
            
//            ApprovedAmountTextField.text =  [helper stringtocurrency:[Eventlist[0] valueForKey:@"approvedcost"]];
        }
        
    }
}
- (IBAction)save_btn:(id)sender
{
    [_approvalfor_tv resignFirstResponder];
    [_status_tv resignFirstResponder];
    [_approver_tv resignFirstResponder];
    [_approverName_tf resignFirstResponder];
    [_comment_tv resignFirstResponder];
    [EstimatedAmountTextField resignFirstResponder];
    [ApprovedAmountTextField resignFirstResponder];
    
    if ((_approvalfor_tv.text.length > 0)&&(_status_tv.text.length > 0)&&(_approver_tv.text.length > 0) && ([_approverName_tf.text length]>0))
    {
        if([senderview isEqualToString:@"Addapproval"])
        {
            [self savedata:Eventapprovallist];
        }
        else if([senderview isEqualToString:@"Editapproval"])
        {
            //edit records
            if ([Eventapprovallist count]>0)
            {
                if ([_approvalfor_tv.text isEqualToString:[Eventapprovallist[0] valueForKey:@"approvalfor"]?: @""]  && [_status_tv.text isEqualToString:[Eventapprovallist[0] valueForKey:@"status"]?: @""] && [_approver_tv.text isEqualToString:[Eventapprovallist[0] valueForKey:@"approverid"]?: @""] && [_PRapprover_tv.text isEqualToString:[Eventapprovallist[0] valueForKey:@"parentapprovalname"]?: @""]   && [_comment_tv.text isEqualToString:[Eventapprovallist[0] valueForKey:@"comment"]?: @""] && [BudgetApproverSwitch isEqualToString:[Eventapprovallist[0] valueForKey:@"budgetapprover"]?:@"" ] && [ApprovedAmountTextField.text isEqualToString:[Eventlist[0] valueForKey:@"approvedcost"]?:@""])
                {
                   
                                [self removepopup];
                   
                }
                else
                {
                    if ([_status_tv.text isEqualToString:@"Approved"])
                    {
                        if ([ApprovedAmountTextField.text length]>0)
                        {
                            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.eventid  contains[c] %@",appdelegate.eventid];
                            
                            
                            
                            NSError *error;
                            
                            [fetchRequest setEntity:entity];
                            
                            [fetchRequest setPredicate:predicate];
                            
                            NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
                            
                            
                            if([fetchedObjects count]>0)
                            {
                                for (NSManagedObject *object in fetchedObjects)
                                {
                                    [object setValue:[helper convertToStringFromCurrency:ApprovedAmountTextField.text] forKey:@"approvedcost"];
                                    
                                }
                                [context save:&error];
                                
                            }

                            [self savedata:Eventapprovallist];
                        }
                        
                        else
                        {
                            [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
                        }
                    }
                    else
                    {
                         [self savedata:Eventapprovallist];
                    }
                   
                }
            }
        }
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
    }
}

-(void)savedata:(NSArray *) Eventapprovaldata
{
    
    NSError *error;
    [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    
    [JsonDict setValue:generatedrowid forKey:@"ID"];
    [JsonDict setValue:appdelegate.eventid forKey:@"EventID"];
    [JsonDict setValue:_approvalfor_tv.text forKey:@"ApprovalFor"];
    [JsonDict setValue:approverID forKey:@"ApproverID"];
    
    [JsonDict setValue:ApproverTypeLIC forKey:@"ApproverTypeLIC"];
    
    [JsonDict setValue:ParentApprovalID forKey:@"ParentApprovalID"];
    [JsonDict setValue:[helper stringtobool:BudgetApproverSwitch] forKey:@"BudgetApprover"];
    [JsonDict setValue:_comment_tv.text forKey:@"Comment"];
    [JsonDict setValue:[helper getLic:@"APR_STATUS" value:_status_tv.text] forKey:@"StatusLIC"];
    [JsonDict setValue:_PRapprover_tv.text forKey:@"ParentApprovalName"];
    [JsonDict setValue:[helper stringtobool:canEdit] forKey:@"CanEdit"];
    [JsonDict setValue:[helper stringtobool:canDelete] forKey:@"CanDelete"];
    [JsonDict setValue:[helper stringtobool:ApprovalFlag] forKey:@"ApprovalFlag"];
    //[JsonDict setValue:ApprovedAmountTextField.text forKey:@"ApprovedCost"];
    [JsonDict setValue:approvedCost forKey:@"ApprovedCost"];

    NSMutableArray *JsonArray = [[NSMutableArray alloc]init];
    [JsonArray addObject:JsonDict];
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonArray options:0 error:&error];
    if (! JsonArray)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventApproval" pageno:@"" pagecount:@"" lastpage:@""];
    
    
    
    appdelegate.senttransaction = @"Y";
    
//    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Add Activities"  Status:@"Open"];
    
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
       // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        //[alert show];
        [helper showalert:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] action:[helper gettranslation:@"LBL_462"]];
        [self removepopup];
    }
    else
    {
       // NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
      //  NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
       // NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
        
        [helper insert_transaction_local:msgbody entity_id:@"D-TnCConcent" type:transaction_type entityname:@"Upsert Approval" Status:@"Completed"];
        
        
        if ([senderview isEqualToString:@"Editapproval"])
        {
            [helper delete_predicate:@"Event_Approvals" predicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.approvalid]];
            // [helper delete_predicate:@"Act_dynamicdetails" predicate:ActivityPredicate];
        }
        
        
        NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event_Approvals" inManagedObjectContext:context];
        NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
        
        [newrecord setValue:appdelegate.eventid forKey:@"eventid"];
        [newrecord setValue:[JsonDict valueForKey:@"ApproverID"] forKey:@"approverid"];
        [newrecord setValue:[JsonDict valueForKey:@"Comment"] forKey:@"comment"];
        [newrecord setValue:[JsonDict valueForKey:@"ID"] forKey:@"id"];
        [newrecord setValue:[JsonDict valueForKey:@"ParentApprovalID"] forKey:@"parentroutingid"];
        [newrecord setValue:_status_tv.text forKey:@"status"];
        [newrecord setValue:[JsonDict valueForKey:@"ApprovalFor"] forKey:@"approvalfor"];
        
        [newrecord setValue:[JsonDict valueForKey:@"ApproverTypeLIC"] forKey:@"approverTypeLIC"];
        [newrecord setValue:[JsonDict valueForKey:@"ParentApprovalName"] forKey:@"parentapprovalname"];
        [newrecord setValue:[JsonDict valueForKey:@"StatusLIC"] forKey:@"statuslic"];
        [newrecord setValue:[JsonDict valueForKey:@"BudgetApprover"] forKey:@"budgetapprover"];
        [newrecord setValue:[helper booltostring:[[JsonDict valueForKey:@"CanEdit"] boolValue]] forKey:@"canEdit"];
        [newrecord setValue:[helper booltostring:[[JsonDict valueForKey:@"CanDelete"] boolValue]] forKey:@"canDelete"];
        [context save:&error];
        
        
        
        NSString *status=[helper getLic:@"APR_STATUS" value:_status_tv.text];
        if([status isEqualToString:@"Accepted"] ||[status isEqualToString:@"Rejected"])
        {
        appdelegate.EventApprovals=@"YES";
            appdelegate.PerformDeltaSync = @"YES";
            [helper get_EMS_data];
        }
        else
        {
            appdelegate.EventApprovals=@"YES";
            appdelegate.PerformDeltaSync = @"YES";
            [helper GetEventApprovalData];
        }
        appdelegate.PerformDeltaSync = @"NO";
        appdelegate.EventApprovals=@"NO";
        
        
        [helper removeWaitCursor];
        [self removepopup];
       
    }  
}
- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}

-(void)removepopup
{
    appdelegate.swipevar = @"approvals";
    [self performSegueWithIdentifier:@"unwindtoEventDetail" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=====-=-=-=-=-=-=-=-=-=-=-=-=-==-==-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=÷÷


#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _status_tv)
    {
       // if([senderview isEqualToString:@"Editapproval"])
        //{
            TextfieldFlag = @"StatusTextField";
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"APR_STATUS"]];
        
        
        
            if ([StatusLic isEqualToString:@"Planned"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Pending"]];
            }
            else if ([StatusLic isEqualToString:@"Pending"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Approved",@"Rejected"]];
            }
            else if ([StatusLic isEqualToString:@"Approved"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Rejected"]];
            }
            else if ([StatusLic isEqualToString:@"Rejected"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Approved"]];
            }
            _dropdown_lbl.text = [helper gettranslation:@"LBL_242"];
        //}
    }
    else if (textField == _approver_tv)
    {
        TextfieldFlag = @"ApproverTypeTextField";
        LOVArr=[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"APPROVER_TYPE"]];
       // LOVArr = [helper query_alldata:@"User"];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_050"];
    }
    
    else if (textField ==_approverName_tf)
    {
            TextfieldFlag = @"ApproverTextField";
        
            if([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"User"])
            {
                LOVArr=[helper query_alldata:@"User"];
            }
            else if ([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"Position"])
            {
                LOVArr=[helper query_alldata:@"Position_List"];
            }
        else
        {
            LOVArr=nil;
        }
        
    }
    
    else if (textField == _PRapprover_tv)
    {
        TextfieldFlag = @"PRApproverTextField";
        LOVArr = ParApproverArr;
        _dropdown_lbl.text = [helper gettranslation:@"LBL_189"];
    }
    
    if (textField == _approver_tv || textField == _PRapprover_tv)
    {
        [helper MoveViewUp:YES Height:50 ViewToBeMoved:self.view];
    }
    if (textField == _status_tv || textField == _approver_tv || textField == _PRapprover_tv || textField==_approverName_tf)
    {
        [LOVTableview reloadData];
        [self adjustHeightOfTableview:LOVTableview];
        LOVTableview.hidden = NO;
        //_dropdown_view.hidden = NO;
    }
    if (textField == _approvalfor_tv)
    {
        stringlength = 50;
    }
    if (textField == _comment_tv)
    {
        stringlength = 255;
    }
    
    if(textField == ApprovedAmountTextField)
    {
            if([[Eventlist[0] valueForKey:@"approvedcost"] length]>0)
                ApprovedAmountTextField.text = [Eventlist[0] valueForKey:@"approvedcost"];
            else
                ApprovedAmountTextField.text = @"";
    }
    
    if(textField == EstimatedAmountTextField)
    {
        if([estimatedCost length]>0)
            EstimatedAmountTextField.text = estimatedCost;
        else
            EstimatedAmountTextField.text = @"";
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _approver_tv || textField == _PRapprover_tv)
    {
        [helper MoveViewUp:NO Height:50 ViewToBeMoved:self.view];
    }
    if (textField == _status_tv)
    {
        if (![helper isdropdowntextCorrect:_status_tv.text lovtype:@"APR_STATUS"])
        {
            _status_tv.text = @"";
        }
    }
    if (textField == _approver_tv)
    {
        NSMutableArray *ApproverNameArr = [[NSMutableArray alloc]init];
        for (int i =0; i < [LOVArr count]; i++)
        {
            [ApproverNameArr addObject:[LOVArr[i] valueForKey:@"value"]];
        }
        if (![ApproverNameArr containsObject:_approver_tv.text])
        {
            _approver_tv.text = @"";
        }
    }
    if (textField == _PRapprover_tv)
    {
        NSMutableArray *ParentApproverNameArr = [[NSMutableArray alloc]init];
        for (int i =0; i<[LOVArr count]; i++)
        {
            [ParentApproverNameArr addObject:[LOVArr[i] valueForKey:@"approvalfor"]];
        }
        if (![ParentApproverNameArr containsObject:_PRapprover_tv.text])
        {
            _PRapprover_tv.text = @"";
        }
    }
   if(textField==ApprovedAmountTextField)
   {
       BOOL check = [helper CostValidate:ApprovedAmountTextField.text];
       if(!(check)|| ApprovedAmountTextField.text.length>15)
       {
           [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_766"]action:[helper gettranslation:@"LBL_462"]];
           ApprovedAmountTextField.text=@"";
           approvedCost =@"0";
       }
       else
       {
           approvedCost = ApprovedAmountTextField.text;
           ApprovedAmountTextField.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],ApprovedAmountTextField.text];
       }
   }
  
    if(textField == EstimatedAmountTextField)
    {
        BOOL check = [helper CostValidate:EstimatedAmountTextField.text];
        

        
        if(!(check)|| EstimatedAmountTextField.text.length>15)
        {
            [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_766"]action:[helper gettranslation:@"LBL_462"]];
            EstimatedAmountTextField.text=@"";
            estimatedCost =@"0";
        }
        else
        {
            estimatedCost = EstimatedAmountTextField.text;
            EstimatedAmountTextField.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],EstimatedAmountTextField.text];
        }
    }
    

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == _status_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"APR_STATUS"]];
            if ([StatusLic isEqualToString:@"Planned"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Pending"]];
            }
            else if ([StatusLic isEqualToString:@"Pending"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Approved",@"Rejected"]];
            }
            else if ([StatusLic isEqualToString:@"Approved"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Rejected"]];
            }
            else if ([StatusLic isEqualToString:@"Rejected"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Approved"]];
            }

        }
        else if (theTextField == _approver_tv)
        {
            LOVArr=[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"APPROVER_TYPE"]];
        }
        else if (theTextField == _approverName_tf)
        {
            if([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"User"])
            {
                LOVArr=[helper query_alldata:@"User"];
            }
            else if ([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"Position"])
            {
                LOVArr=[helper query_alldata:@"Position_List"];
            }
        }
        
        else if (theTextField == _PRapprover_tv)
        {
            LOVArr = ParApproverArr;
        }
    }
    else
    {
        if (theTextField == _status_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"APR_STATUS"]];
            if ([StatusLic isEqualToString:@"Planned"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Pending"]];
            }
            else if ([StatusLic isEqualToString:@"Pending"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@ OR lic like %@",@"Approved",@"Rejected"]];
            }
            else if ([StatusLic isEqualToString:@"Approved"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Rejected"]];
            }
            else if ([StatusLic isEqualToString:@"Rejected"])
            {
                LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lic like %@",@"Approved"]];
            }

            count = searchText.length;
            [filteredpartArray removeAllObjects];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
            LOVArr = [filteredpartArray copy];
            
        }
        
        else if (theTextField == _approver_tv)
        {
            LOVArr=[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"APPROVER_TYPE"]];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.value contains[c] %@)",searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
            LOVArr = [filteredpartArray copy];

            
        }
        else if (theTextField == _approverName_tf)
        {
            if([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"User"])
            {
                LOVArr = [helper query_alldata:@"User"];
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.userid contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@)",searchText,searchText,searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
                LOVArr = [filteredpartArray copy];
   
            }
            else if ([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"Position"])
            {
                LOVArr=[helper query_alldata:@"Position_List"];
                count = searchText.length;
                [filteredpartArray removeAllObjects];
                NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(SELF.positionName contains[c] %@)",searchText];
                filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
                LOVArr = [filteredpartArray copy];

                
                
            }
        }

        else if (theTextField == _PRapprover_tv)
        {
            LOVArr = ParApproverArr;
            count = searchText.length;
            [filteredpartArray removeAllObjects];
            NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(approvalfor contains[c] %@)",searchText];
            filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
            LOVArr = [filteredpartArray copy];
        }
    }
    [LOVTableview reloadData];
}


#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == LOVTableview)
    {
        CGFloat height = LOVTableview.contentSize.height;
        CGFloat maxHeight = LOVTableview.superview.frame.size.height - LOVTableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableview.frame;
            
            if([TextfieldFlag isEqualToString:@"StatusTextField"])
            {
//                frame.origin.x = _status_tv.frame.origin.x;
//                frame.origin.y = _status_tv.frame.origin.y + 30;
                frame.origin.x = 522;
                frame.origin.y = 185;
//                
            }
            if([TextfieldFlag isEqualToString:@"ApproverTypeTextField"])
            {
                //frame.origin.x = _approver_tv.frame.origin.x;
                //frame.origin.y = _approver_tv.frame.origin.y + 30;
                frame.origin.x = 232;
                frame.origin.y = 245;
            
            }
        
            if([TextfieldFlag  isEqualToString:@"PRApproverTextField"])
            {
               // frame.origin.x = _PRapprover_tv.frame.origin.x;
                //frame.origin.y = _PRapprover_tv.frame.origin.y + 30;
                frame.origin.x = 232;
                frame.origin.y = 300;
                
                //x==522
            }
            if([TextfieldFlag isEqualToString:@"ApproverTextField"])
            {
                frame.origin.x = 522;
                frame.origin.y = 245;

            }
            
            LOVTableview.frame= frame;
            
            
        }];
        
    }
}
#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    customcell = [LOVTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    if([TextfieldFlag isEqualToString:@"StatusTextField"])
    {
        lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    }
    
    if([TextfieldFlag isEqualToString:@"ApproverTypeTextField"])
    {
        lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    }
    
    if([TextfieldFlag isEqualToString:@"ApproverTextField"])
    {
        if([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"User"])
        {
            lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"fullname"];
        }
        else if ([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"Position"])
        {
            lovtypename.text=[LOVArr[indexPath.row] valueForKey:@"positionName"];
        }
    }

    
    if([TextfieldFlag  isEqualToString:@"PRApproverTextField"])
    {
        lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"approvalfor"];
    }
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if([TextfieldFlag isEqualToString:@"StatusTextField"])
    {
        _status_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        if ([[LOVArr[indexPath.row] valueForKey:@"lic"] isEqualToString:@"Approved"])
        {
            ApprovedAmountDisplaylbl.text = [NSString stringWithFormat:@"%@ *",ApprovedAmountDisplaylbl.text];
           [self markasrequired:ApprovedAmountDisplaylbl];
        }
        else
        {
            ApprovedAmountDisplaylbl.text = @"Approved Cost";
        }
        [_status_tv resignFirstResponder];
        
    }
    if([TextfieldFlag isEqualToString:@"ApproverTypeTextField"])
    {
        _approver_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        
        ApproverTypeLIC=[LOVArr[indexPath.row] valueForKey:@"lic"];
        _approverName_tf.text=@"";
        [_approver_tv resignFirstResponder];
        
    }
    if([TextfieldFlag isEqualToString:@"ApproverTextField"])
    {
        if([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"User"])
        {
            _approverName_tf.text = [LOVArr[indexPath.row] valueForKey:@"fullname"];
            approverID=[LOVArr[indexPath.row] valueForKey:@"userid"];
            
        }
        else if ([[helper getLic:@"APPROVER_TYPE" value:_approver_tv.text] isEqualToString:@"Position"])
        {
            _approverName_tf.text=[LOVArr[indexPath.row] valueForKey:@"positionName"];
            approverID=[LOVArr[indexPath.row] valueForKey:@"positionID"];
        }
        
        [_approverName_tf resignFirstResponder];
        
    }
    
    if([TextfieldFlag  isEqualToString:@"PRApproverTextField"])
    {
        _PRapprover_tv.text = [LOVArr[indexPath.row] valueForKey:@"approvalfor"];
        ParentApprovalID = [LOVArr[indexPath.row] valueForKey:@"id"];
        [_PRapprover_tv resignFirstResponder];
        
    }
    LOVTableview.hidden = YES;
   // _dropdown_view.hidden = YES;
}
#pragma mark touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != _dropdown_view)
    {
        LOVTableview.hidden = YES;
        [_PRapprover_tv resignFirstResponder];
        [_approver_tv resignFirstResponder];
        [_status_tv resignFirstResponder];
        [_comment_tv resignFirstResponder];
        [_approvalfor_tv resignFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == _approvalfor_tv || textField == _comment_tv)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}




- (IBAction)BudgetAppoverSwitchValueChangedAction:(id)sender
{
    if([BudgetApproverSwitch isEqualToString:@"Y"])
    {
        BudgetApproverSwitch = @"N";
        [BudgetApproverOutlet setOn:NO];
    }
    else if ([BudgetApproverSwitch isEqualToString:@"N"])
    {
        BudgetApproverSwitch = @"Y";
        [BudgetApproverOutlet setOn:YES];
    }

}
@end
