//
//  UpsertEventVC.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/19/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SOCustomCell.h"
#import "SLKeyChainStore.h"
#import "HSDatePickerViewController.h"
#import "EventReschedule_ViewController.h"

@interface UpsertEventVC : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UITableView *LOVTableview;
    //IBOutlet UITableView *salutation_tableview;
}
@property(strong,nonatomic)HelperClass *helper;
@property(strong,nonatomic)NSString *attendeeid;


@property (strong, nonatomic) IBOutlet UITextField *eventname_tv;
@property (strong, nonatomic) IBOutlet UITextField *eventloc_tv;
@property (strong, nonatomic) IBOutlet UITextField *eventdesc_tv;
@property (strong, nonatomic) IBOutlet UITextField *eventstatus_tv;
@property (strong, nonatomic) IBOutlet UITextField *eventstdate_tv;
@property (strong, nonatomic) IBOutlet UITextField *eventenddate_tv;

@property (strong, nonatomic) IBOutlet UITextField *event_category_tv;
@property (strong, nonatomic) IBOutlet UITextField *event_type_tv;
@property (strong, nonatomic) IBOutlet UITextField *event_subtype_tv;

@property (strong, nonatomic) IBOutlet UITextField *event_estcost_tv;
@property (strong, nonatomic) IBOutlet UITextField *event_apprcost_tv;
@property (strong, nonatomic) IBOutlet UITextField *event_actcost_tv;
@property (strong, nonatomic) IBOutlet UITextField *event_product_tv;
@property (strong, nonatomic) IBOutlet UITextField *event_rembudget_tv;

@property(strong,nonatomic)NSString *senderview;
@property(strong,nonatomic)NSString *cancelsegue;
@property (strong, nonatomic) IBOutlet UILabel *viewtitle_lbl;

- (IBAction)save_btn:(id)sender;
-(void)savedata:(NSArray *) Eventlistdata;
- (IBAction)cancel_btn:(id)sender;
- (IBAction)startdate_btn:(id)sender;
- (IBAction)enddate_btn:(id)sender;
- (IBAction)location_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *dropdown_view;
@property (strong, nonatomic) IBOutlet UILabel *dropdown_lbl;


@property (strong, nonatomic) IBOutlet UIButton *save_btn;
@property (strong, nonatomic) IBOutlet UIButton *cancel_btn;


@property (strong, nonatomic) IBOutlet UILabel *eventname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *eventdesc_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *status_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *startdate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *enddate_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *category_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *type_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *subtype_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *product_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *estcost_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *departmentname_ulbl;
@property (strong, nonatomic) IBOutlet UITextField *eventdepartment_tv;











@end
