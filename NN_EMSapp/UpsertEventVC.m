//
//  UpsertEventVC.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 1/19/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "UpsertEventVC.h"


@interface UpsertEventVC ()<HSDatePickerViewControllerDelegate>
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSArray *Eventlist;
    NSArray *Eventlovlist;
    SLKeyChainStore *keychain;
    NSString *generatedrowid;
    NSString *affiliateid;
    NSString *owner;
    NSString *visibility;
    NSArray *UserArr;
    NSArray *LOVArr;
    NSString *TextfieldFlag;
    NSString *startEndDate;
    NSString *transaction_type;
    NSDateFormatter *dateFormatter;
    NSUInteger stringlength;
    HSDatePickerViewController *hsdpvc;
    NSString *estimatedCost;
    NSMutableArray *newEventData;
    NSMutableArray *eventData;
    NSString *currencyCode;
}

@property (nonatomic, strong) NSDate *selectedDate;
@end

@implementation UpsertEventVC
@synthesize helper,senderview,cancelsegue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    eventData=[[NSMutableArray alloc]init];
    
   
  
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid like %@",[keychain stringForKey:@"username"]];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userid CONTAINS[c] %@",[keychain stringForKey:@"username"]];
    
    currencyCode=[helper query_data:@"" inputdata:@"CurrencyCodeLIC" entityname:@"S_Config_Table"];

     Eventlovlist = [helper query_alldata:@"Event_LOV"];
    
    UserArr = [[helper query_alldata:@"User"] filteredArrayUsingPredicate:userpredicate];
    _dropdown_view.hidden = YES;
    dateFormatter = [[NSDateFormatter alloc] init];
    _eventstatus_tv.userInteractionEnabled = NO;
    
    hsdpvc=[[HSDatePickerViewController alloc]init];
    
    if([senderview isEqualToString:@"Addevent"])
    {
        generatedrowid = [helper generate_rowId];
        transaction_type = @"Add New Event";
        owner = [UserArr[0] valueForKey:@"userid"];
        visibility = @"MY";
        _eventstatus_tv.text = [helper getvaluefromlic:@"EVENT_STAT" lic:@"Planned"];
        _viewtitle_lbl.text=[helper gettranslation:@"LBL_021"];
        
}
    else if([senderview isEqualToString:@"Editevent"])
    {
        [self setupattendeeview];
        generatedrowid = appdelegate.eventid;
        transaction_type = @"Edit Event";
        _viewtitle_lbl.text=[helper gettranslation:@"LBL_380"];
    }
    
    _event_category_tv.placeholder=[helper gettranslation:@"LBL_757"];
    _event_type_tv.placeholder=[helper gettranslation:@"LBL_757"];
    _event_subtype_tv.placeholder=[helper gettranslation:@"LBL_757"];
    _event_product_tv.placeholder=[helper gettranslation:@"LBL_757"];
    
    
     affiliateid =[[[helper query_alldata:@"User"] objectAtIndex:0] valueForKey:@"affiliateid"];
    // Do any additional setup after loading the view.
    LOVTableview.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableview.layer.borderWidth = 1;
    
    [_eventstatus_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_eventstdate_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_eventenddate_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_event_category_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [_event_type_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_event_subtype_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_event_product_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_eventdepartment_tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    [self langsetuptranslations];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)langsetuptranslations
{
   // _viewtitle_lbl.text = [helper gettranslation:@"LBL_021"];
    [_save_btn setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    [_cancel_btn setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    
    
    _eventname_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_131"]] ;
    [self markasrequired:_eventname_ulbl];
                            
    _eventdesc_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_130"]] ;
//    [self markasrequired:_eventdesc_ulbl];
    
    _status_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_242"]] ;
    [self markasrequired:_status_ulbl];
    
    _startdate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_239"]] ;
    [self markasrequired:_startdate_ulbl];
    
    _enddate_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_122"]] ;
    [self markasrequired:_enddate_ulbl];
    
    _category_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_062"]] ;
    [self markasrequired:_category_ulbl];
    
    _type_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_266"]] ;
    [self markasrequired:_type_ulbl];
    
    _subtype_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_246"]] ;
    [self markasrequired:_subtype_ulbl];
    
    _product_ulbl.text = [helper gettranslation:@"LBL_204"] ;
    
    _estcost_ulbl.text = [NSString stringWithFormat:@"%@ *",[helper gettranslation:@"LBL_128"]] ;
    [self markasrequired:_estcost_ulbl];
    
    _departmentname_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_092"]] ;
    
}
                            

-(void) markasrequired:(UILabel *) label
{
    NSInteger labellength = label.text.length - 1;
    NSString *labelstr = label.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:labelstr];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(labellength,1)];
    label.attributedText = string;

}


-(void)setupattendeeview
{
    Eventlist = [helper query_alldata:@"Event"];
    Eventlist = [Eventlist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
    eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];
    
    eventData=[Eventlist mutableCopy];
    
    if (eventvenue.count >0)
    {
        _eventloc_tv.text = [eventvenue[0] valueForKey:@"location"];
    }
    
    if ([Eventlist count]>0)
    {
        _viewtitle_lbl.text = [Eventlist[0] valueForKey:@"name"];
        _eventname_tv.text = [Eventlist[0] valueForKey:@"name"];
        _eventdesc_tv.text = [Eventlist[0] valueForKey:@"desc"];
        _eventstatus_tv.text = [Eventlist[0] valueForKey:@"status"];
        _eventstdate_tv.text = [helper formatingdate:[Eventlist[0] valueForKey:@"startdate"] datetime:@"FormatDate"];
        _eventenddate_tv.text = [helper formatingdate:[Eventlist[0] valueForKey:@"enddate"] datetime:@"FormatDate"];
        _event_category_tv.text = [Eventlist[0] valueForKey:@"category"];
        _event_type_tv.text = [Eventlist[0] valueForKey:@"type"];
        _event_subtype_tv.text = [Eventlist[0] valueForKey:@"subtype"];
    //    _event_estcost_tv.text = [helper stringtocurrency:[Eventlist[0] valueForKey:@"estimatedcost"]];
      //  _event_apprcost_tv.text = [helper stringtocurrency:[Eventlist[0] valueForKey:@"approvedcost"]];
       // _event_actcost_tv.text = [helper stringtocurrency:[Eventlist[0] valueForKey:@"actualcost"]];
        
            _event_estcost_tv.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"estimatedcost"]];
//          _event_apprcost_tv.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"approvedcost"]];
        if(Eventlist.count>0 && currencyCode.length>0)
        _event_apprcost_tv.text =[[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode] stringByAppendingString:[Eventlist[0] valueForKey:@"approvedcost"]];
        
         _event_actcost_tv.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[Eventlist[0] valueForKey:@"actualcost"]];
        
        estimatedCost = [Eventlist[0] valueForKey:@"estimatedcost"];
        
        _event_product_tv.text = [Eventlist[0] valueForKey:@"product"];
  
        _eventdepartment_tv.text = [Eventlist[0] valueForKey:@"department"];
        owner = [Eventlist[0] valueForKey:@"ownedby"];
        visibility = [Eventlist[0] valueForKey:@"visibility"];
    
        float remaining_budget_float = [[Eventlist[0] valueForKey:@"approvedcost"] floatValue] - [[Eventlist[0] valueForKey:@"actualcost"] floatValue];
        
        _event_rembudget_tv.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],[NSString stringWithFormat:@"%f",remaining_budget_float]];
    
    }
    
}

- (IBAction)save_btn:(id)sender
{
    NSArray *eventvenue = [helper query_alldata:@"Event_Venue"];
    eventvenue = [eventvenue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid]];

    
    NSString *estcost = [NSString stringWithFormat:@"%.02f",[[Eventlist[0] valueForKey:@"estimatedcost"] floatValue]];
    NSString *apprcost = [NSString stringWithFormat:@"%.02f",[[Eventlist[0] valueForKey:@"approvedcost"] floatValue]];
    NSString *actcost = [NSString stringWithFormat:@"%.02f",[[Eventlist[0] valueForKey:@"actualcost"] floatValue]];
    
    NSDate *sdate = [dateFormatter dateFromString:_eventstdate_tv.text];
    NSDate *edate = [dateFormatter dateFromString:_eventenddate_tv.text];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
//    [_eventname_tv resignFirstResponder];
//    [_eventstdate_tv resignFirstResponder];
//    [_eventenddate_tv resignFirstResponder];
//    [_eventstatus_tv resignFirstResponder];
//    [_eventenddate_tv resignFirstResponder];
    [self.view endEditing:YES];
    
    if ((_eventname_tv.text.length > 0)&&(_eventstdate_tv.text.length > 0)&&(_eventenddate_tv.text.length > 0)&&(_eventstatus_tv.text.length > 0)&&(_event_category_tv.text.length > 0)&&(_event_type_tv.text.length > 0)&&(_event_subtype_tv.text.length > 0) &&(_event_estcost_tv.text.length > 0))
    {
        
        if([senderview isEqualToString:@"Addevent"])
        {
            [self savedata:Eventlist];
        }
        else if([senderview isEqualToString:@"Editevent"])
        {
            
            //edit records
            if ([_eventname_tv.text isEqualToString:[Eventlist[0] valueForKey:@"name"]?: @""]  && [_eventdesc_tv.text isEqualToString:[Eventlist[0] valueForKey:@"desc"]?: @""] && [_eventstatus_tv.text isEqualToString:[Eventlist[0] valueForKey:@"status"]?: @""] &&[[dateFormatter stringFromDate:sdate] isEqualToString:[Eventlist[0] valueForKey:@"startdate"]?: @""] && [[dateFormatter stringFromDate:edate] isEqualToString:[Eventlist[0] valueForKey:@"enddate"]?: @""] && [_event_category_tv.text isEqualToString:[Eventlist[0] valueForKey:@"category"]?: @""]  && [_event_type_tv.text isEqualToString:[Eventlist[0] valueForKey:@"type"]?: @""]   && [_event_subtype_tv.text isEqualToString:[Eventlist[0] valueForKey:@"subtype"]?: @""]  && [[helper convertToStringFromCurrency:_event_estcost_tv.text] isEqualToString:estcost?: @""] && [[helper convertToStringFromCurrency:_event_apprcost_tv.text] isEqualToString:apprcost?: @""] && [[helper convertToStringFromCurrency:_event_actcost_tv.text] isEqualToString:actcost?: @""]  && [_event_product_tv.text isEqualToString:[Eventlist[0] valueForKey:@"product"]?: @""] && [_eventdepartment_tv.text isEqualToString:[Eventlist[0] valueForKey:@"department"]?: @""])
            {
                
                [self removepopup];
            }
            else
            {
                appdelegate.eventTempStartDate=[helper converingformateddatetooriginaldate:_eventstdate_tv.text datetype:@"ServerDate"];
                appdelegate.eventTempEndDate=[helper converingformateddatetooriginaldate:_eventenddate_tv.text datetype:@"ServerDate"];
                NSUInteger value=[helper checkForRescheduling];
                if(value==0)
                {
                    [self savedata:Eventlist];
                }
                else
                {
                    [self prepArray];
                    [self performSegueWithIdentifier:@"showRescheduleScreen" sender:self];
                }
            }
        }
    }
    else
    {
        [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_492"] action:[helper gettranslation:@"LBL_462"]];
    }
}
-(void)prepArray
{
    newEventData=[[NSMutableArray alloc]init];
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    [JsonDict setValue:generatedrowid forKey:@"EventID"];
    [JsonDict setValue:[helper convertToStringFromCurrency:_event_actcost_tv.text] forKey:@"ActualCost"];
    [JsonDict setValue:affiliateid forKey:@"AffiliateID"];
    [JsonDict setValue:[helper convertToStringFromCurrency:_event_apprcost_tv.text] forKey:@"ApprovedCost"];
    [JsonDict setValue:[helper convertToStringFromCurrency:_event_rembudget_tv.text] forKey:@"BalanceBudget"];
    [JsonDict setValue:_eventdesc_tv.text forKey:@"Description"];
    
    
    //  [JsonDict setValue:[dateFormatter stringFromDate:edate] forKey:@"EndDate"];
    [JsonDict setValue:[helper formatingdate:[Eventlist[0] valueForKey:@"startdate"] datetime:@"FormatDate"] forKey:@"StartDate"];
    
    [JsonDict setValue:[helper convertToStringFromCurrency:_event_estcost_tv.text] forKey:@"EstimatedCost"];
    [JsonDict setValue:_eventname_tv.text forKey:@"Name"];
    [JsonDict setValue:owner forKey:@"OwnedBy"];
    
    //[JsonDict setValue:[dateFormatter stringFromDate:sdate] forKey:@"StartDate"];
    [JsonDict setValue:[helper formatingdate:[Eventlist[0] valueForKey:@"enddate"] datetime:@"FormatDate"] forKey:@"EndDate"];
    
    [JsonDict setValue:_eventstatus_tv.text forKey:@"Status"];
    [JsonDict setValue:[helper getLic:@"EVENT_CATEGORY" value:_event_category_tv.text] forKey:@"CategoryLIC"];
    [JsonDict setValue:[helper getLic:@"PRODUCT" value:_event_product_tv.text] forKey:@"ProductLIC"];
    [JsonDict setValue:[helper getLic:@"EVENT_STAT" value:_eventstatus_tv.text] forKey:@"StatusLIC"];
    [JsonDict setValue:[helper getLic:@"EVENT_SUBTYPE" value:_event_subtype_tv.text] forKey:@"SubTypeLIC"];
    [JsonDict setValue:[helper getLic:@"EVENT_TYPE" value:_event_type_tv.text] forKey:@"TypeLIC"];
    [JsonDict setValue:[helper getLic:@"EVENT_DEPT" value:_eventdepartment_tv.text] forKey:@"DepartmentLIC"];
    
    [JsonDict setValue:_event_category_tv.text forKey:@"Category"];
    [JsonDict setValue:_event_product_tv.text forKey:@"Product"];
    [JsonDict setValue:_eventstatus_tv.text forKey:@"Status"];
    [JsonDict setValue:_event_subtype_tv.text forKey:@"SubType"];
    [JsonDict setValue:_event_type_tv.text forKey:@"Type"];
    //Uncomment when dept change is done
    //[JsonDict setValue:_eventdepartment_tv.text forKey:@"Department"];
    
    
    [JsonDict setValue:[Eventlist[0] valueForKey:@"visibility"] forKey:@"Visibility"];

    [newEventData addObject:JsonDict];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showRescheduleScreen"])
    {
        EventReschedule_ViewController *controller1=(EventReschedule_ViewController *)segue.destinationViewController;
        controller1.eventDetails=newEventData;
        
    }
}

- (BOOL)isAcceptableTextLength:(NSUInteger)length {
    return length <= 255;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    return [self isAcceptableTextLength:textView.text.length + text.length - range.length];
    
    //    return YES;
}


-(void)savedata:(NSArray *) Eventlistdata
{
    NSError *error;
    
   /* [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:[[NSTimeZone localTimeZone] abbreviation]]];
    NSDate *sdate = [dateFormatter dateFromString:_eventstdate_tv.text];
    NSDate *edate = [dateFormatter dateFromString:_eventenddate_tv.text];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];*/
    NSString *categorylic = [helper getLic:@"EVENT_CATEGORY" value:_event_category_tv.text] ;
    NSString *EventStatus = [helper getLic:@"EVENT_STAT" value:_eventstatus_tv.text];
    NSString *Dept = [helper getLic:@"EVENT_DEPT" value:_eventdepartment_tv.text];
    NSString *Subtype = [helper getLic:@"EVENT_SUBTYPE" value:_event_subtype_tv.text];
    NSString *EventType = [helper getLic:@"EVENT_TYPE" value:_event_type_tv.text];
    
    
    [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    
    NSMutableDictionary *JsonDict = [[NSMutableDictionary alloc]init];
    [JsonDict setValue:generatedrowid forKey:@"EventID"];
//    [JsonDict setValue:[helper convertToStringFromCurrency:_event_actcost_tv.text] forKey:@"ActualCost"];
    [JsonDict setValue:[Eventlist[0] valueForKey:@"actualcost"]?:@""  forKey:@"ActualCost"];
    [JsonDict setValue:affiliateid forKey:@"AffiliateID"];
//    [JsonDict setValue:[helper convertToStringFromCurrency:_event_apprcost_tv.text] forKey:@"ApprovedCost"];
    [JsonDict setValue:[Eventlist[0] valueForKey:@"approvedcost"]?:@""  forKey:@"ApprovedCost"];
    [JsonDict setValue:[helper convertToStringFromCurrency:_event_rembudget_tv.text] forKey:@"BalanceBudget"];
    [JsonDict setValue:_eventdesc_tv.text forKey:@"Description"];
   
    
  //  [JsonDict setValue:[dateFormatter stringFromDate:edate] forKey:@"EndDate"];
    [JsonDict setValue:[helper converingformateddatetooriginaldate:_eventstdate_tv.text datetype:@"ServerDate"] forKey:@"StartDate"];
    
    [JsonDict setValue:estimatedCost forKey:@"EstimatedCost"];
    [JsonDict setValue:_eventname_tv.text forKey:@"Name"];
    [JsonDict setValue:owner forKey:@"OwnedBy"];
    
    //[JsonDict setValue:[dateFormatter stringFromDate:sdate] forKey:@"StartDate"];
    [JsonDict setValue:[helper converingformateddatetooriginaldate:_eventenddate_tv.text datetype:@"ServerDate"] forKey:@"EndDate"];
    
    [JsonDict setValue:_eventstatus_tv.text forKey:@"Status"];
    [JsonDict setValue:categorylic forKey:@"CategoryLIC"];
//    [JsonDict setValue:[helper getLic:@"PRODUCT" value:_event_product_tv.text] forKey:@"ProductLIC"];
    [JsonDict setValue:EventStatus forKey:@"StatusLIC"];
    [JsonDict setValue:Dept forKey:@"DepartmentLIC"];
    [JsonDict setValue:Subtype forKey:@"SubTypeLIC"];
    [JsonDict setValue:EventType forKey:@"TypeLIC"];
   
    
 
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JsonDict options:0 error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AESPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AESPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEvent" pageno:@"" pagecount:@"" lastpage:@""];
    
//    appdelegate.senttransaction = @"Y";
//    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:transaction_type entityname:@"Events"  Status:@"Open"];
//    [helper removeWaitCursor];
//    [self removepopup];
    NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
    
    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
    {
        [helper removeWaitCursor];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AESPrivatekey];
        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
    
        if([senderview isEqualToString:@"Editevent"])
        {
            appdelegate.swipevar = @"EditEvent";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventid like %@",appdelegate.eventid];
            NSString *calidentify = [[[[[helper query_alldata:@"Event"] filteredArrayUsingPredicate:predicate] valueForKey:@"calevent_refid"] allObjects] objectAtIndex:0];
            
            [helper delete_predicate:@"Event" predicate:predicate];
//            [helper deletecal_event:calidentify];
            
            NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
            NSManagedObject *newrecord = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
            
            [newrecord setValue:[JsonDict valueForKey:@"ActualCost"] forKey:@"actualcost"];
            [newrecord setValue:[JsonDict valueForKey:@"AffiliateID"] forKey:@"affiliateid"];
            [newrecord setValue:[JsonDict valueForKey:@"ApprovedCost"] forKey:@"approvedcost"];
            [newrecord setValue:[JsonDict valueForKey:@"BalanceBudget"] forKey:@"balancebudget"];
            [newrecord setValue:_event_category_tv.text forKey:@"category"];
            
      
            [newrecord setValue:[JsonDict valueForKey:@"DepartmentLIC"] forKey:@"departmentlic"];
            [newrecord setValue:[JsonDict valueForKey:@"Description"] forKey:@"desc"];
            [newrecord setValue:[JsonDict valueForKey:@"EndDate"] forKey:@"enddate"];
            [newrecord setValue:[JsonDict valueForKey:@"EstimatedCost"] forKey:@"estimatedcost"];
            [newrecord setValue:[JsonDict valueForKey:@"EventID"] forKey:@"eventid"];
            [newrecord setValue:[JsonDict valueForKey:@"EventID"] forKey:@"id"];
            [newrecord setValue:[JsonDict valueForKey:@"Name"] forKey:@"name"];
            [newrecord setValue:[JsonDict valueForKey:@"OwnedBy"] forKey:@"ownedby"];
            [newrecord setValue:_event_product_tv.text forKey:@"product"];
            [newrecord setValue:[JsonDict valueForKey:@"StartDate"] forKey:@"startdate"];
            [newrecord setValue:_eventstatus_tv.text forKey:@"status"];
            [newrecord setValue:_event_subtype_tv.text forKey:@"subtype"];
            [newrecord setValue:_event_type_tv.text forKey:@"type"];
            [newrecord setValue:_eventdepartment_tv.text forKey:@"department"];
            [newrecord setValue:[JsonDict valueForKey:@"Visibility"] forKey:@"visibility"];
            [newrecord setValue:[JsonDict valueForKey:@"ActCompleted"] forKey:@"actcompleted"];
            [newrecord setValue:[JsonDict valueForKey:@"ActTotal"] forKey:@"acttotal"];
            [newrecord setValue:[JsonDict valueForKey:@"ApprovalFlag"] forKey:@"approvalflag"];
            [newrecord setValue:[JsonDict valueForKey:@"AttApproved"] forKey:@"attapproved"];
            [newrecord setValue:[JsonDict valueForKey:@"AttTotal"] forKey:@"atttotal"];
            [newrecord setValue:[JsonDict valueForKey:@"CategoryLIC"] forKey:@"categorylic"];
            [newrecord setValue:[JsonDict valueForKey:@"ProductLIC"] forKey:@"productlic"];
            [newrecord setValue:[JsonDict valueForKey:@"StatusLIC"] forKey:@"statuslic"];
            [newrecord setValue:[JsonDict valueForKey:@"SubTypeLIC"] forKey:@"subtypelic"];
            [newrecord setValue:[JsonDict valueForKey:@"TypeLIC"] forKey:@"typelic"];
            [newrecord setValue:[JsonDict valueForKey:@"Venue"] forKey:@"venue"];
            [newrecord setValue:visibility forKey:@"visibility"];
            [newrecord setValue:nil forKey:@"calevent_refid"];
            
            
            NSError *error;
            [context save:&error];
            
           
            
            [helper insert_transaction_local:msgbody entity_id:appdelegate.eventid type:@"Event Update" entityname:[NSString stringWithFormat:@"Update event : %@ ",[JsonDict valueForKey:@"Name"]] Status:@"Completed"];
        }
        else if([senderview isEqualToString:@"Addevent"])
        {
            appdelegate.PerformDeltaSync =@"YES";
            
//            [helper ProcessEventDetails:generatedrowid];
            [helper ProcessEventList:generatedrowid];
            [helper insert_transaction_local:msgbody entity_id:appdelegate.eventid type:@"New Event Create" entityname:@"New Event Created " Status:@"Completed"];
        }
        else
        {
            
        }
        [helper FDRCalander];
        //[helper DeltaSyncCalander];
    }
    [helper removeWaitCursor];
    [self removepopup];
}

- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
}

- (IBAction)startdate_btn:(id)sender
{
    startEndDate = @"StartDate";
    
    hsdpvc=[[HSDatePickerViewController alloc]init];
    //NSString *eventEndDate=_eventenddate_tv.text;
  
    hsdpvc.delegate = self;
    /*hsdpvc.minDate=[NSDate date];
    if(eventEndDate.length>0)
    {
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter1 setDateFormat:@"dd MMM yyyy HH:mm"];
        
        NSDate *evntEndDate=[dateFormatter1 dateFromString:eventEndDate];
        
        hsdpvc.maxDate=evntEndDate;
        
        
    }*/
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (IBAction)enddate_btn:(id)sender
{
    startEndDate = @"EndDate";
    
    hsdpvc=[[HSDatePickerViewController alloc]init];
    NSString *activityStartDate=_eventstdate_tv.text;
    hsdpvc.delegate = self;
    
    if(activityStartDate.length>0)
    {
        NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
        [dateformat setTimeZone:[NSTimeZone systemTimeZone]];
        [dateformat setDateFormat:@"dd MMM yyyy HH:mm"];
        
        NSDate *actStartDate=[dateformat dateFromString:activityStartDate];
        hsdpvc.date = actStartDate;
        
        hsdpvc.minDate = actStartDate;
        
    }
    else
    {
        hsdpvc.minDate=[NSDate date];
    }
    
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (IBAction)location_btn:(id)sender
{
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date
{
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    [dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormater.dateFormat = @"dd MMM yyyy HH:mm";
    
    if ([startEndDate isEqualToString:@"StartDate"])
    {
        [_eventstdate_tv setText:[dateFormater stringFromDate:date]];
    }
    else if ([startEndDate isEqualToString:@"EndDate"])
    {
        [_eventenddate_tv setText:[dateFormater stringFromDate:date]];
    }
    
    self.selectedDate = date;
}


-(void)removepopup
{
    appdelegate.swipevar = @"EditEvent";
    [self performSegueWithIdentifier:cancelsegue sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
    
}



#pragma mark textfeild delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _eventname_tv)
    {
        stringlength = 50;
    }
    
    else if (textField == _eventloc_tv)
    {
    }
    
    else if (textField == _eventdesc_tv)
    {
        stringlength = 255;
    }
    
    else if (textField == _eventstatus_tv)
    {
        if([senderview isEqualToString:@"Editevent"])
        {
            NSPredicate *statuspredicate = [NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_STAT"];
            
            
            TextfieldFlag = @"StatusTextField";
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:statuspredicate];
            [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
            _dropdown_lbl.text = [helper gettranslation:@"LBL_242"];
        }   
    }
    
    else if (textField == _eventstdate_tv)
    {
        [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
    }
    
    else if (textField == _eventenddate_tv)
    {
        [helper MoveViewUp:YES Height:40 ViewToBeMoved:self.view];
    }
    
    else if (textField == _event_category_tv)
    {
        TextfieldFlag = @"CategoryTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_CATEGORY"]];
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_062"];
    }
    
    else if (textField == _event_type_tv)
    {
        TextfieldFlag = @"TypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_266"];
    }
    
    else if (textField == _event_subtype_tv)
    {
        NSString *typelicvalue =[helper getLic:@"EVENT_TYPE" value:_event_type_tv.text] ;
        
        TextfieldFlag = @"SubTypeTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@ && parlic like %@",@"EVENT_SUBTYPE",typelicvalue]];
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.view];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_246"];
    }
    
    else if (textField == _event_estcost_tv)
    {
        stringlength = 8;
        [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
        
        if (Eventlist.count>0)
        {
            _event_estcost_tv.text = estimatedCost;
        }
    }
    else if (textField == _event_apprcost_tv)
    {
        [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
    }
    else if (textField == _event_actcost_tv)
    {
        [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
    }
    else if (textField == _event_product_tv)
    {
        TextfieldFlag = @"ProductTextField";
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRODUCT"]];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_204"];
        
        [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
    }
    else if (textField == _event_rembudget_tv)
    {
        [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
    }
    else if (textField == _eventdepartment_tv)
    {
        TextfieldFlag = @"DepartmentTextFeild";
        
        //Change lovtype to Dept when Department change is done
        LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_DEPT"]];
        [helper MoveViewUp:YES Height:160 ViewToBeMoved:self.view];
        _dropdown_lbl.text = [helper gettranslation:@"LBL_092"];
    }
    
    
    
    if ( textField == _event_category_tv || textField == _event_type_tv || textField == _event_subtype_tv || textField == _event_product_tv || textField == _eventdepartment_tv)
    {
        [LOVTableview reloadData];
        [self adjustHeightOfTableview:LOVTableview];
        LOVTableview.hidden = NO;
        _dropdown_view.hidden = NO;
    }    if (textField == _eventstatus_tv)
    {
        if([senderview isEqualToString:@"Editevent"])
           {
               [LOVTableview reloadData];
               [self adjustHeightOfTableview:LOVTableview];
               LOVTableview.hidden = NO;
               _dropdown_view.hidden = NO;
           }
    }
}

    

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [_dropdown_view setHidden:YES];
    if (textField == _eventstdate_tv || textField == _eventenddate_tv)
    {
        [helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
    }
    else if (textField == _event_category_tv || textField == _event_type_tv || textField == _event_subtype_tv)
    {
        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.view];
    }
    else if (textField == _event_estcost_tv || textField == _event_apprcost_tv || textField == _event_actcost_tv)
    {
        [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
    }
    else if (textField == _event_product_tv || textField == _event_rembudget_tv || textField == _eventdepartment_tv)
    {
        [helper MoveViewUp:NO Height:160 ViewToBeMoved:self.view];
    }
    
   
    if (textField == _eventstatus_tv)
    {
        if([senderview isEqualToString:@"Editevent"])
        {
            [helper MoveViewUp:NO Height:40 ViewToBeMoved:self.view];
        }
    }
    if (textField == _event_category_tv)
    {
      
        if(![helper isdropdowntextCorrect:_event_category_tv.text lovtype:@"EVENT_CATEGORY"])
        {
            _event_category_tv.text = @"";
        }
    }
    if (textField == _eventdepartment_tv)
    {
        
        if (![helper isdropdowntextCorrect:_eventdepartment_tv.text lovtype:@"EVENT_DEPT"])
        {
            _eventdepartment_tv.text = @"";
        }
    }
    if (textField == _event_type_tv)
    {
        if (![helper isdropdowntextCorrect:_event_type_tv.text lovtype:@"EVENT_TYPE"])
        {
            _event_type_tv.text = @"";
        }
    }
    if (textField == _event_subtype_tv)
    {
        if (![helper isdropdowntextCorrect:_event_subtype_tv.text lovtype:@"EVENT_SUBTYPE"])
        {
            _event_subtype_tv.text = @"";
        }
    }
    if (textField == _event_product_tv)
    {
        if (![helper isdropdowntextCorrect:_event_product_tv.text lovtype:@"PRODUCT"])
        {
            _event_product_tv.text = @"";
        }
    }
    if (textField ==_event_estcost_tv)
    {
            if(!([helper CostValidate:[_event_estcost_tv.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]] &&  _event_estcost_tv.text.length<16))
            {
                [helper showalert:[helper gettranslation:@"LBL_590"] message:[helper gettranslation:@"LBL_765"]action:[helper gettranslation:@"LBL_462"]];
                    _event_estcost_tv.text=@"";
            }
            else
            {
                estimatedCost = _event_estcost_tv.text;
                _event_estcost_tv.text = [NSString stringWithFormat:@"%@ %@",[[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencySymbol value:currencyCode],_event_estcost_tv.text];
                
                //[helper stringtocurrency:_event_estcost_tv.text];
            }
    }

        
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        if (theTextField == _eventstatus_tv)
        {
            if([senderview isEqualToString:@"Editevent"])
            {
                LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_STAT"]];
            }
        }
        else if (theTextField == _event_category_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_CATEGORY"]];
        }
        else if (theTextField == _event_type_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];
        }
        else if (theTextField == _event_subtype_tv)
        {
            NSString *typelicvalue =[helper getLic:@"EVENT_TYPE" value:_event_type_tv.text] ;
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@ && parlic like %@",@"EVENT_SUBTYPE",typelicvalue]];
            
        }
        else if (theTextField == _event_product_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRODUCT"]];
        }
        //Change LOV type when Dept change is done
        else if (theTextField == _eventdepartment_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_DEPT"]];
        }
    }
    else
    {
        if (theTextField == _eventstatus_tv)
        {
            if([senderview isEqualToString:@"Editevent"])
            {
                LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_STAT"]];
            }
            
        }
        else if (theTextField == _event_category_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_CATEGORY"]];
        }
        else if (theTextField == _event_type_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_TYPE"]];
        }
        else if (theTextField == _event_subtype_tv)
        {
            NSString *typelicvalue =[helper getLic:@"EVENT_TYPE" value:_event_type_tv.text] ;
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@ && parlic like %@",@"EVENT_SUBTYPE",typelicvalue]];
//            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_SUBTYPE"]];
        }
        else if (theTextField == _event_product_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"PRODUCT"]];
        }
        else if (theTextField == _eventdepartment_tv)
        {
            LOVArr = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"EVENT_DEPT"]];
        }
        
        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
        LOVArr = [filteredpartArray copy];
        
    }
    [LOVTableview reloadData];
}


#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
    if (tableview == LOVTableview)
    {
        CGFloat height = LOVTableview.contentSize.height;
        CGFloat maxHeight = LOVTableview.superview.frame.size.height - LOVTableview.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableview.frame;
            
//            if([TextfieldFlag isEqualToString:@"StatusTextField"])
//            {
//                frame.origin.x = 8;
//                frame.origin.y = -7;//8;//182;
//                frame.size.width = 270;
//                
//            }
            if([TextfieldFlag isEqualToString:@"CategoryTextField"])
            {
                frame.origin.x = 8;
                frame.origin.y = 65;//92;//267;
                frame.size.width = 270;
                
            }
            if([TextfieldFlag  isEqualToString:@"TypeTextField"])
            {
                frame.origin.x = 298;
                frame.origin.y = 65;//92;////267;
                frame.size.width = 270;
                
            }
            if([TextfieldFlag isEqualToString:@"SubTypeTextField"])
            {
                frame.origin.x = 596;
                frame.origin.y = 65;//92;//267;
                frame.size.width = 270;
                
            }
            if([TextfieldFlag isEqualToString:@"ProductTextField"])
            {
                frame.origin.x = 298;
                frame.origin.y = 127;//153;//299;
                frame.size.width = 270;
            }
            if([TextfieldFlag isEqualToString:@"DepartmentTextFeild"])
            {
                frame.origin.x = 298;
                frame.origin.y = 127;//153;//299;
                frame.size.width = 270;
            }
            _dropdown_view.frame= frame;
//            LOVTableview.frame = frame;
            
        }];
        
    }
}
#pragma marka tableview delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [LOVTableview dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovtypename = (UILabel *)[contentView viewWithTag:1];
    UIView *sepratorview = (UIView *)[contentView viewWithTag:2];
//    CGRect lovtypenameframe = lovtypename.frame;
//    CGRect sepratorviewframe = sepratorview.frame;
//    
//    lovtypename.frame = lovtypenameframe;
//    sepratorview.frame = sepratorviewframe;
    
    lovtypename.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if([TextfieldFlag isEqualToString:@"StatusTextField"])
    {
        if([senderview isEqualToString:@"Editevent"])
        {
            _eventstatus_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
            [_eventstatus_tv resignFirstResponder];
        }
    }
    if([TextfieldFlag isEqualToString:@"CategoryTextField"])
    {
        _event_category_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_event_category_tv resignFirstResponder];
        
    }
    if([TextfieldFlag  isEqualToString:@"TypeTextField"])
    {
        _event_type_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_event_type_tv resignFirstResponder];
        
    }
    if([TextfieldFlag isEqualToString:@"SubTypeTextField"])
    {
        _event_subtype_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_event_subtype_tv resignFirstResponder];
        
    }
    if([TextfieldFlag isEqualToString:@"ProductTextField"])
    {
        _event_product_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_event_product_tv resignFirstResponder];
        
    }
    if([TextfieldFlag isEqualToString:@"DepartmentTextFeild"])
    {
        _eventdepartment_tv.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [_eventdepartment_tv resignFirstResponder];
        
    }
    
    LOVTableview.hidden = YES;
    _dropdown_view.hidden = YES;
}
#pragma mark touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view != LOVTableview)
    {
        LOVTableview.hidden = YES;
        _dropdown_view.hidden = YES;
        [_eventname_tv resignFirstResponder];
        [_eventloc_tv resignFirstResponder];
        [_eventdesc_tv resignFirstResponder];
        [_eventstatus_tv resignFirstResponder];
        [_eventstdate_tv resignFirstResponder];
        [_eventenddate_tv resignFirstResponder];
        [_event_category_tv resignFirstResponder];
        [_event_type_tv resignFirstResponder];
        [_event_subtype_tv resignFirstResponder];
        [_event_estcost_tv resignFirstResponder];
        [_event_apprcost_tv resignFirstResponder];
        [_event_actcost_tv resignFirstResponder];
        [_event_product_tv resignFirstResponder];
        [_event_rembudget_tv resignFirstResponder];
        [_eventdepartment_tv resignFirstResponder];
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ( textField == _eventname_tv || textField == _eventdesc_tv )
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= stringlength;
    }
    else
    {
        return YES;
    }
}

-(IBAction)unwindToAddEventDialog:(UIStoryboardSegue *)segue
{
    
   [self removepopup];
}


@end
