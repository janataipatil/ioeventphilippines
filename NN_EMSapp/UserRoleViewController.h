//
//  UserRoleViewController.h
//  NN_EMSapp
//
//  Created by iWizardsVI on 29/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
@interface UserRoleViewController : UIViewController


@property (strong, nonatomic) IBOutlet UITextField *UserTypeTextField;

@property (strong, nonatomic) IBOutlet UITextField *UserIdTextField;
@property (strong, nonatomic) IBOutlet UITextField *StatusTextField;
@property (strong, nonatomic) IBOutlet UITextField *EventRoleTextField;
@property (strong, nonatomic) IBOutlet UIView *DropdownView;
@property (strong, nonatomic) IBOutlet UITableView *LOVTableView;
@property (strong, nonatomic) IBOutlet UILabel *DropdownHeaderlbl;
@property(strong,nonatomic)HelperClass *helper;


- (IBAction)SaveButtonAction:(id)sender;
- (IBAction)CancelButtonAction:(id)sender;



@end
