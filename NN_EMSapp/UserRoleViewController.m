//
//  UserRoleViewController.m
//  NN_EMSapp
//
//  Created by iWizardsVI on 29/01/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "UserRoleViewController.h"

@interface UserRoleViewController ()
{
    NSArray *LOVArr;
    NSString *TextFieldFlag;
    AppDelegate *appdelegate;
    NSArray *EventTeamArr;
}

@end

@implementation UserRoleViewController
@synthesize UserTypeTextField,UserIdTextField,StatusTextField,EventRoleTextField,DropdownView,LOVTableView,DropdownHeaderlbl,helper;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [StatusTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [EventRoleTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
   // [UserTypeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    LOVTableView.layer.borderColor =  [[UIColor colorWithRed:220/255.0f green:228/255.0f blue:240/255.0f alpha:1.0f]CGColor];
    LOVTableView.layer.borderWidth = 1;
    [self setupUserRoleView];
}
#pragma mark setup userrole view
-(void)setupUserRoleView
{
    EventTeamArr = [[helper query_alldata:@"Event_Team"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF.eventid like %@) AND (SELF.userid like %@)",appdelegate.eventid,appdelegate.userid]];
    if([EventTeamArr count] >0)
    {
        UserIdTextField.text = [EventTeamArr[0] valueForKey:@"userid"];
        UserTypeTextField.text = [EventTeamArr[0] valueForKey:@"eMSPosition"];
        StatusTextField.text = [EventTeamArr[0] valueForKey:@"status"];
        EventRoleTextField.text = [EventTeamArr[0] valueForKey:@"role"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SaveButtonAction:(id)sender
{
    
    if ([UserIdTextField.text isEqualToString:[EventTeamArr[0] valueForKey:@"userid"]?: @""] && [UserTypeTextField.text isEqualToString:[EventTeamArr[0] valueForKey:@"eMSPosition"]?: @""] && [StatusTextField.text isEqualToString:[EventTeamArr[0] valueForKey:@"status"]?: @""] && [EventRoleTextField.text isEqualToString:[EventTeamArr[0] valueForKey:@"role"]?: @""])
    {
        [self CancelButtonAction:self];
        return;
    }

        NSMutableArray *PayloadArr = [[NSMutableArray alloc]init];
        NSMutableArray *DatabaseArr = [[NSMutableArray alloc]init];
    

        NSMutableDictionary *ServerDict = [[NSMutableDictionary alloc]init];
        NSMutableDictionary *DatabaseDict = [[NSMutableDictionary alloc]init];
        
        if ([EventTeamArr count]>0)
        {
//            [ServerDict setValue:EventRoleTextField.text forKey:@"RoleLIC"];
            [ServerDict setValue:[EventTeamArr[0] valueForKey:@"eventid"] forKey:@"EventID"];
            [ServerDict setValue:[helper getLic:@"REC_STAT" value:StatusTextField.text] forKey:@"Status"];
            [ServerDict setValue:[EventTeamArr[0] valueForKey:@"id"] forKey:@"ID"];
            [ServerDict setValue:[EventTeamArr[0] valueForKey:@"userid"] forKey:@"UserID"];
            
            
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"userid"] forKey:@"UserID"];
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"id"] forKey:@"ID"];
            [DatabaseDict setValue:StatusTextField.text forKey:@"Status"];
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"firstname"] forKey:@"FirstName"];
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"lastname"] forKey:@"LastName"];
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"eMSPosition"] forKey:@"EMSPosition"];
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"eMSPositionLIC"] forKey:@"EMSPositionLIC"];
            [DatabaseDict setValue:StatusTextField.text forKey:@"StatusLIC"];
            [DatabaseDict setValue:[EventTeamArr[0] valueForKey:@"email"] forKey:@"Email"];
            [DatabaseDict setValue:[helper getLic:@"ROLE" value:EventRoleTextField.text] forKey:@"RoleLIC"];
            [DatabaseDict setValue:EventRoleTextField.text forKey:@"Role"];
        }
        [DatabaseArr addObject:DatabaseDict];
        [PayloadArr addObject:ServerDict];
    
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:PayloadArr options:0 error:&error];
    if (jsonData)
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *AesPrivatekey = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:AesPrivatekey];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventTeam" pageno:@"" pagecount:@"" lastpage:@""];
    
    /* NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
     
     if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
     {
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
     
     [alert show];
     }
     else
     {
     NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:AesPrivatekey];
     NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
     NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
     if (jsonData)
     {
     [helper insertEventTeamMember:appdelegate.eventid TeamArr:DatabaseArr];
     [self CancelButtonAction:self];
     }
     
     
     
     
     }*/
    
    appdelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:[helper generate_rowId] type:@"Add Activity Plans to Event Type" entityname:@"EventTypeActivityPlans" Status:@"Open"];
    if ([EventTeamArr count]>0)
    {
        NSLog(@"Event team id is %@",[EventTeamArr[0] valueForKey:@"id"]);
        [helper delete_TeamMember:@"Event_Team" teamid:[EventTeamArr[0] valueForKey:@"id"] eventid:[EventTeamArr[0] valueForKey:@"eventid"]];
    }
    
    [helper insertEventTeamMember:appdelegate.eventid TeamArr:DatabaseArr];
    [self CancelButtonAction:self];
    
    
}

- (IBAction)CancelButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"unwindFromUserRoleToEventTeam" sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}
#pragma mark textfield delegate method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    LOVArr = [helper query_alldata:@"Event_LOV"];
    if (textField == StatusTextField)
    {
        TextFieldFlag = @"StatusTextField";
        LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        DropdownHeaderlbl.text = @"Status";
    }
    else if (textField == EventRoleTextField)
    {
        TextFieldFlag = @"EventRoleTextField";
        LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ROLE"]];
        DropdownHeaderlbl.text = @"Event Role";
    }
   /* else if (textField == UserTypeTextField)
    {
        TextFieldFlag = @"UserTypeTextField";
        LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"USER_TYPE"]];
        DropdownHeaderlbl.text = @"User Type";
    }*/
    if (textField == StatusTextField || textField == EventRoleTextField )
    {
        [LOVTableView reloadData];
        [self adjustHeightOfTableview:LOVTableView];
        DropdownView.hidden = NO;
        
    }
    [helper MoveViewUp:YES Height:120 ViewToBeMoved:self.view];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [helper MoveViewUp:NO Height:120 ViewToBeMoved:self.view];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
#pragma mark inductive search method
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText  = theTextField.text;
    if (searchText.length == 0)
    {
        LOVArr = [helper query_alldata:@"Event_LOV"];
        /*if ([TextFieldFlag isEqualToString:@"UserTypeTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"USER_TYPE"]];
        }
        else*/
        if ([TextFieldFlag isEqualToString:@"StatusTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if ([TextFieldFlag isEqualToString:@"EventRoleTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ROLE"]];
        }
        
    }
    else
    {
        LOVArr = [helper query_alldata:@"Event_LOV"] ;
       /* if ([TextFieldFlag isEqualToString:@"UserTypeTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"USER_TYPE"]];
        }
        else */
        if ([TextFieldFlag isEqualToString:@"StatusTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"REC_STAT"]];
        }
        else if ([TextFieldFlag isEqualToString:@"EventRoleTextField"])
        {
            LOVArr = [LOVArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lovtype like %@",@"ROLE"]];
        }
        NSMutableArray *filteredpartArray = [[NSMutableArray alloc]init];
        NSPredicate *filterpredicate = [NSPredicate predicateWithFormat:@"(value contains[c] %@)",searchText];
        filteredpartArray = [NSMutableArray arrayWithArray:[LOVArr filteredArrayUsingPredicate:filterpredicate]];
        LOVArr = [filteredpartArray copy];
        
    }
    [LOVTableView reloadData];
}
#pragma mark tableview delegate method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LOVArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier;
    cellIdentifier = @"socustomcell";
    
    customcell = [LOVTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    UILabel *lovlbl = (UILabel *)[contentView viewWithTag:1];
    lovlbl.text = [LOVArr[indexPath.row] valueForKey:@"value"];
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([TextFieldFlag isEqualToString:@"StatusTextField"])
    {
        StatusTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [StatusTextField resignFirstResponder];
    }
    else if ([TextFieldFlag isEqualToString:@"EventRoleTextField"])
    {
        EventRoleTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [EventRoleTextField resignFirstResponder];
    }
   /* else if ([TextFieldFlag isEqualToString:@"UserTypeTextField"])
    {
        UserTypeTextField.text = [LOVArr[indexPath.row] valueForKey:@"value"];
        [UserTypeTextField resignFirstResponder];
    }*/
    
    DropdownView.hidden = YES;
   
}

#pragma mark view touch method
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch;
    touch = [touches anyObject];
    if (touch.view != DropdownView)
    {
        if (DropdownView.hidden == NO)
        {
            DropdownView.hidden = YES;
            [StatusTextField resignFirstResponder];
           // [UserTypeTextField resignFirstResponder];
            [EventRoleTextField resignFirstResponder];
        }
    }
}
#pragma mark adjusting tableview height
- (void)adjustHeightOfTableview:(UITableView *)tableview
{
   
        CGFloat height = LOVTableView.contentSize.height;
        CGFloat maxHeight = LOVTableView.superview.frame.size.height - LOVTableView.frame.origin.y;
        if (height > maxHeight)
            height = maxHeight;
        [UIView animateWithDuration:0.0 animations:^{
            CGRect frame = LOVTableView.frame;
                            //codefields are available
                /*if([TextFieldFlag isEqualToString:@"UserTypeTextField"])
                {
                    frame.origin.x = 528;
                    frame.origin.y = 180;
                }*/
                if([TextFieldFlag isEqualToString:@"StatusTextField"])
                {
                    frame.origin.x = 227;
                    frame.origin.y = 250;
                }
                if([TextFieldFlag  isEqualToString:@"EventRoleTextField"])
                {
                    frame.origin.x = 528;
                    frame.origin.y = 250;
                }
            DropdownView.frame = frame;
        }];
    
}
@end
