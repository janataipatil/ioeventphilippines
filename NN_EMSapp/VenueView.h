//
//  VenueView.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>


@interface VenueView : UIView

@property(strong,nonatomic)IBOutlet UILabel *addresslineone;
@property(strong,nonatomic)IBOutlet UILabel *addresslinetwo;
@property(strong,nonatomic)IBOutlet UILabel *city;
@property(strong,nonatomic)IBOutlet UILabel *state;
@property(strong,nonatomic)IBOutlet UILabel *pincode;

@property(strong,nonatomic)IBOutlet UIView *mapView;
//@property (weak, nonatomic) IBOutlet GMSMapView *mymapView;


@end
