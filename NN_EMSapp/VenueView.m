//
//  VenueView.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "VenueView.h"

@implementation VenueView
@synthesize addresslineone,addresslinetwo,city,state,pincode,mapView;//,mymapView;

- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"VenueView" owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        [self addSubview:mainView];
        addresslineone = (UILabel *)[mainView viewWithTag:1];
        addresslinetwo = (UILabel *)[mainView viewWithTag:2];
        city = (UILabel *)[mainView viewWithTag:3];
        state = (UILabel *)[mainView viewWithTag:4];
        pincode = (UILabel *)[mainView viewWithTag:5];
        mapView = (UIView *)[mainView viewWithTag:6];
//        mymapView = (GMSMapView *)[mainView viewWithTag:6];
        
    }
    return self;
}

@end
