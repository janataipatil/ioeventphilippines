//
//  WeekViewController.h
//  CalendarDemo - Graphical Calendars Library for iOS
//
//  Copyright (c) 2014-2015 Julien Martin. All rights reserved.
//

#import "MGCDayPlannerEKViewController.h"
#import "EventCalendar_ViewController.h"
#import "HelperClass.h"

@protocol WeekViewControllerDelegate <MGCDayPlannerEKViewControllerDelegate, CalendarViewControllerDelegate, UIViewControllerTransitioningDelegate>

@end


@interface WeekViewController : MGCDayPlannerEKViewController <CalendarViewControllerNavigation>


@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic, weak) id<WeekViewControllerDelegate> delegate;

@end
