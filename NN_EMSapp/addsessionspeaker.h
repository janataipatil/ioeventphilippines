//
//  addsessionspeaker.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/3/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOCustomCell.h"
#import "HelperClass.h"
#import "AppDelegate.h"
#import "Reachability.h"

@interface addsessionspeaker : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UITableView *allSpeaker_tbl;

@property(strong,nonatomic) HelperClass *helper;
@property (nonatomic) Reachability* reachability;

@property (strong, nonatomic) IBOutlet UISearchBar *speaker_searchbar;

@property (nonatomic, readonly) NSMutableArray* AddedSpeaker;


@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *titlebar_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *salutation_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *fname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *lname_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *speciality_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *institution_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *department_ulbl;

@property (strong, nonatomic) IBOutlet UILabel *designation_ulbl;


@end
