//
//  addsessionspeaker.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 2/3/17.
//  Copyright © 2017 iWizards. All rights reserved.
//

#import "addsessionspeaker.h"

@interface addsessionspeaker ()
{
    NSMutableArray *allSpeakerslist;
    NSInteger count;
    AppDelegate *appdelegate;
    dispatch_queue_t squeue;
    NSMutableArray *ListofSpeaker;
    NSMutableArray *SelectedSpeaker;
    
    NSPredicate *sessionidpred;
}

@end

@implementation addsessionspeaker
@synthesize allSpeaker_tbl,helper,speaker_searchbar,AddedSpeaker,reachability;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    helper = [[HelperClass alloc]init];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    squeue = dispatch_queue_create("iwiz.serial.Queue", NULL);
    
    
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    [speaker_searchbar setBackgroundImage:[UIImage new]];
    UITextField *searchField = [self.speaker_searchbar valueForKey:@"_searchField"];
    //searchField.textColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    searchField.textColor = [helper DarkBlueColour];
    searchField.placeholder = [helper gettranslation:@"LBL_224"];
    
    [allSpeaker_tbl setEditing:YES animated:YES];

    allSpeakerslist = [NSMutableArray arrayWithArray:[[helper query_alldata:@"Speaker"]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"statuslic like %@",@"Active"] ]];
    
    SelectedSpeaker = [[NSMutableArray alloc] init];
    sessionidpred = [NSPredicate predicateWithFormat:@"sessionid like %@",appdelegate.sessionid];
    
    
    
    
    NSLog(@"allSpeakerslist count : %lu",(unsigned long)allSpeakerslist.count);
    NSArray *geteventspeaker = [[helper query_alldata:@"Event_Speaker"] filteredArrayUsingPredicate:sessionidpred];
    

    for (int i=0;i<[geteventspeaker count]; i++)
    {
        NSString *speakerid = [[geteventspeaker objectAtIndex:i] valueForKey:@"speakerid"];//id
        
        for (int j=0;j<[allSpeakerslist count]; j++)
        {
            NSString *masterspeakerid= [[allSpeakerslist objectAtIndex:j] valueForKey:@"id"];
            
            if ([masterspeakerid isEqualToString:speakerid])
            {
                [allSpeakerslist removeObjectAtIndex:j];
                j--;
            }
        }
    }
    ListofSpeaker = [allSpeakerslist mutableCopy];
    
    NSLog(@"allSpeakerslist count : %lu",(unsigned long)allSpeakerslist.count);
    
    [self langsetuptranslations];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    
    _titlebar_ulbl.text = [helper gettranslation:@"LBL_026"];
    _salutation_ulbl.text = [helper gettranslation:@"LBL_218"];
    _fname_ulbl.text = [helper gettranslation:@"LBL_141"];
    _lname_ulbl.text = [helper gettranslation:@"LBL_153"];
    _speciality_ulbl.text = [helper gettranslation:@"LBL_238"];
    _institution_ulbl.text = [helper gettranslation:@"LBL_147"];
    _department_ulbl.text = [helper gettranslation:@"LBL_092"];
    
    _designation_ulbl.text = [helper gettranslation:@"LBL_095"];
    
    
    
}



#pragma mark tableview method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allSpeakerslist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    customcell = [self.allSpeaker_tbl dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    UILabel *salutation = (UILabel *) [contentView viewWithTag:1];
    UILabel *firstname = (UILabel *) [contentView viewWithTag:2];
    UILabel *lastname = (UILabel *) [contentView viewWithTag:3];
    UILabel *status = (UILabel *) [contentView viewWithTag:4];
    //    UILabel *speciality = (UILabel *) [contentView viewWithTag:5];
    UILabel *company = (UILabel *) [contentView viewWithTag:5];
    UILabel *department = (UILabel *) [contentView viewWithTag:6];
    UILabel *designation = (UILabel *) [contentView viewWithTag:7];
    
    [salutation setText:[allSpeakerslist[indexPath.row] valueForKey:@"salutation"]];
    [firstname setText:[allSpeakerslist[indexPath.row] valueForKey:@"firstname"]];
    [lastname setText:[allSpeakerslist[indexPath.row] valueForKey:@"lastname"]];
    //    [speciality setText:[NSString stringWithFormat:@"%@",[allSpeakerslist[indexPath.row] valueForKey:@""]]];
    [status setText:[allSpeakerslist[indexPath.row] valueForKey:@"speciality"]];
    [company setText:[allSpeakerslist[indexPath.row] valueForKey:@"company"]];
    [department setText:[allSpeakerslist[indexPath.row] valueForKey:@"department"]];
    [designation setText:[allSpeakerslist[indexPath.row] valueForKey:@"designation"]];
    
   // customcell.tintColor = [UIColor colorWithRed:51/255.0 green:76/255.0 blue:104/255.0 alpha:1];
    customcell.tintColor = [helper DarkBlueColour];
    
    return customcell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 3;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SelectedSpeaker addObject:[allSpeakerslist[indexPath.row] valueForKey:@"id"]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[SelectedSpeaker count]; i++)
    {
        NSString *item = [SelectedSpeaker objectAtIndex:i];
        if ([item isEqualToString:[allSpeakerslist[indexPath.row] valueForKey:@"id"]])
        {
            [SelectedSpeaker removeObject:item];
            i--;
        }
    }
}



#pragma mark edit activity method
-(IBAction)savebtnClicked:(UIButton *)sender
{
    if (SelectedSpeaker.count<=0)
    {
        [ToastView showToastInParentView:self.view withText:[helper gettranslation:@"LBL_619"] withDuaration:1.0];
    }
    else
    {
        NSString *rowid;
        [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
        
        //        NSMutableDictionary *EventAttendee_dict = [[NSMutableDictionary alloc]init];
        NSMutableArray *EventSpeaker_ar = [[NSMutableArray alloc]init];
        
        for (int i=0;i<[SelectedSpeaker count]; i++)
        {
            NSLog(@"SelectedSpeaker %@",[SelectedSpeaker objectAtIndex:i]);
            
            NSArray*speakerdetail = [allSpeakerslist filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",[SelectedSpeaker objectAtIndex:i]]];
            
            
            
            rowid = [NSString stringWithFormat:@"%@",[helper generate_rowId]];
            
            
            NSMutableDictionary *eventspeaker_dict = [[NSMutableDictionary alloc]init];
            
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"id" ]?: @"" forKey:@"SpeakerID"];
            [eventspeaker_dict setValue:appdelegate.eventid forKey:@"EventID"];
            [eventspeaker_dict setValue:rowid forKey:@"ID"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"firstname"]?: @"" forKey:@"FirstName"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"lastname"]?: @"" forKey:@"LastName"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"salutation"]?: @"" forKey:@"Salutation"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"salutationlic"]?: @"" forKey:@"SalutationLIC"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"statuslic"]?: @"" forKey:@"StatusLIC"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"status"]?: @"" forKey:@"Status"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"speciality"]?: @"" forKey:@"Speciality"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"company"]?: @"" forKey:@"Company"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"designation"]?: @"" forKey:@"Designation"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"department"]?: @"" forKey:@"Department"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"emailaddress"]?: @"" forKey:@"EmailAddress"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"contactno"]?: @"" forKey:@"ContactNo"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"imageicon"]?: @"" forKey:@"ImageIcon"];
            [eventspeaker_dict setValue:[speakerdetail[0] valueForKey:@"cvid"]?: @"" forKey:@"CVID"];
            [eventspeaker_dict setValue:appdelegate.sessionid forKey:@"SessionID"];
            
            [EventSpeaker_ar addObject:eventspeaker_dict];
        }
        
        
        
        NSString *jsonString;
        NSError *error;
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventSpeaker_ar options:0 error:&error];
        if (! jsonData)
        {
            // NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        
        [helper addEventSpeaker:appdelegate.eventid EventSpeakerdata:EventSpeaker_ar];
        
        NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
        NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
        NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
        NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventSpeaker" pageno:@"" pagecount:@"" lastpage:@""];
        
        appdelegate.senttransaction = @"Y";
        [helper insert_transaction_local:msgbody entity_id:rowid type:@"Associate Speaker to Event" entityname:@"Event Speaker" Status:@"Open"];
        
        [helper removeWaitCursor];
        [self removepopup];
        
    }
}

-(IBAction)cancelbtnClicked:(UIButton *)sender
{
    [self removepopup];
}


#pragma mark search bar delegate method
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"yes called");
    if(searchBar == speaker_searchbar)
    {
        [helper MoveViewUp:YES Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    if(searchBar == speaker_searchbar)
    {
        [helper MoveViewUp:NO Height:100 ViewToBeMoved:self.parentViewController.view];
    }
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar == speaker_searchbar)
    {
        allSpeakerslist = [ListofSpeaker mutableCopy];
        if(searchText.length == 0)
        {
           [searchBar performSelector: @selector(resignFirstResponder)withObject: nil afterDelay: 0.1];
        }
        else
        {
            count = searchText.length;
            NSMutableArray *filtereventarr = [[NSMutableArray alloc]init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.salutation contains[c] %@) OR (SELF.firstname contains[c] %@) OR (SELF.lastname contains[c] %@) OR (SELF.status contains[c] %@) OR (SELF.company contains[c] %@) OR (SELF.department contains[c] %@)OR (SELF.designation contains[c] %@)",searchText,searchText,searchText,searchText,searchText,searchText,searchText,searchText];
            filtereventarr = [NSMutableArray arrayWithArray:[allSpeakerslist filteredArrayUsingPredicate:predicate]];
            
            allSpeakerslist = [filtereventarr copy];
        }
       [allSpeaker_tbl reloadData];
       NSMutableArray *SpeakeridArr = [[NSMutableArray alloc]init];
        for (int i =0; i<[allSpeakerslist count]; i++)
        {
            NSString *Speakerid = [allSpeakerslist[i] valueForKey:@"id"];
            [SpeakeridArr addObject:Speakerid];
        }
        
        for (int i =0; i<[SelectedSpeaker count]; i++)
        {
            NSString *SpeakerId = [SelectedSpeaker objectAtIndex:i];
            if ([SpeakeridArr containsObject:SpeakerId])
            {
                NSUInteger index = [SpeakeridArr indexOfObject:SpeakerId];
                NSInteger Speakercount = [SpeakeridArr count];
                if ((index > 0) || ((index == 0) && (index< Speakercount)) )
                {
                    [allSpeaker_tbl selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }//for loop end
        

    }
}


-(void)removepopup
{
    
    [self performSegueWithIdentifier:@"unwindtoEventschedule" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

@end
