//
//  capturesignature_ViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UviSignatureView.h"
#import "AppDelegate.h"
#import "HelperClass.h"
#import "SLKeyChainStore.h"
@interface capturesignature_ViewController : UIViewController
{
    
    IBOutlet UIView *displayimageview;
    NSMutableData *ResponseData;
}
- (IBAction)donebtnaction:(id)sender;

- (IBAction)closebtnaction:(id)sender;

- (IBAction)cancelbtnaction:(id)sender;
- (IBAction)erasebtnaction:(id)sender;

@property(strong,nonatomic)HelperClass *helper;
@property (strong, nonatomic) IBOutlet UviSignatureView *signatureView;
@property (strong, nonatomic) IBOutlet UIButton *EraseButtonOutlet;


@property (strong, nonatomic) IBOutlet UIImageView *displayimage;


@property (strong, nonatomic) IBOutlet UILabel *titleview_ulbl;
@property (strong, nonatomic) IBOutlet UILabel *signhere_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *savebtn_ulbl;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn_ulbl;


@end
