//
//  capturesignature_ViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "capturesignature_ViewController.h"
#define USER_SIGNATURE_PATH  @"user_signature_path"

@interface capturesignature_ViewController ()
{
    AppDelegate *appdelegate;
    NSManagedObjectContext *context;
    NSArray *imagearr;
    NSString *filename;
    NSString *attendeeid;
    dispatch_queue_t squeue;
    NSString *getstatus;
    SLKeyChainStore *keychain;
    dispatch_semaphore_t sem_att;
    NSString *SignatureStorageUrl;
    NSString *CurrentDateStr;
    NSString *StatusLic;
    
    
}

@end

@implementation capturesignature_ViewController
@synthesize signatureView,helper,displayimage,EraseButtonOutlet,savebtn_ulbl;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    helper = [[HelperClass alloc]init];
    imagearr = [helper query_alldata:@"Event_Attendee"];
    imagearr = [imagearr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.attendyid]];
   // attendeeid = [imagearr[0] valueForKey:@"attendeeid"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lovtype = %@ AND lic = %@", @"E_ATT_STAT", @"Signed"];
    getstatus = [[[[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:predicate] valueForKey:@"value"] objectAtIndex:0];
    squeue = dispatch_queue_create("iwiz.serial.Queue", NULL);
    keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
   
    CurrentDateStr = [helper getNSDateStr];
    if([imagearr count]>0)
    {
        NSString *path = [NSString stringWithFormat:@"%@",[imagearr[0] valueForKey:@"signatureid"]];
       // NSString *signflag = [NSString stringWithFormat:@"%@",[imagearr[0] valueForKey:@"signflag"]];
        StatusLic = [imagearr[0] valueForKey:@"statuslic"];
        //if([signflag isEqualToString:@"Y"])
        if ([[imagearr[0] valueForKey:@"signatureid"] length]>0 ){
            displayimageview.hidden = NO;
            signatureView.hidden = YES;
            displayimage.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:path]]];
            
        }
        else
        {
            signatureView.hidden = NO;
            
        }
    }
    [self langsetuptranslations];
    if (![StatusLic isEqualToString:@"Signed"]) {
        EraseButtonOutlet.userInteractionEnabled = YES;
        savebtn_ulbl.userInteractionEnabled = YES;
        [EraseButtonOutlet setImage:[UIImage imageNamed:@"erase.png"] forState:UIControlStateNormal];
        
    }else{
        EraseButtonOutlet.userInteractionEnabled = NO;
        savebtn_ulbl.userInteractionEnabled = NO;
        [EraseButtonOutlet setImage:[UIImage imageNamed:@"Erase"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)langsetuptranslations
{
    appdelegate.translations = [helper query_alldata:@"Lang_translations"];
    
    [_cancelbtn_ulbl setTitle:[helper gettranslation:@"LBL_061"] forState:UIControlStateNormal];
    [savebtn_ulbl setTitle:[helper gettranslation:@"LBL_219"] forState:UIControlStateNormal];
    
    _titleview_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_669"]];
    _signhere_ulbl.text = [NSString stringWithFormat:@"%@",[helper gettranslation:@"LBL_670"]];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //[self removepopup];
}
#pragma mark upload attachmment method
-(void) upload_attachmentdata:(NSString *)filenamestr base64:(NSString *)base64
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[NSString stringWithFormat:@"%@.%@",[NSString stringWithString:[helper generate_rowId]],@"JPG"] forKey:@"FileName"];
    [dict setValue:base64 forKey:@"Base64"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageContainer" entityname:@"S_Config_Table"] forKey:@"Container"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageAccount" entityname:@"S_Config_Table"] forKey:@"Account"];
    [dict setValue:[helper query_data:@"name" inputdata:@"StorageKey" entityname:@"S_Config_Table"] forKey:@"Key"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSData *postData = [jsonStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSLog(@"storage uri %@",[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]);
    [request setURL:[NSURL URLWithString:[helper query_data:@"name" inputdata:@"StorageURI" entityname:@"S_Config_Table"]]];
    [request setValue:@"2.0.0" forHTTPHeaderField:@"ZUMO-API-VERSION"];
    [request setValue:[keychain stringForKey:@"SLToken"] forHTTPHeaderField:@"X-ZUMO-AUTH"];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn)
    {
        ResponseData = [NSMutableData data];
    }
    else
    {
        
        // [helper printloginconsole:@"2" logtext:@"Connection could not be made in upload attachment request"];
        NSLog(@"Connection could not be made in upload attachment request");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [ResponseData setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [ResponseData appendData:data];
    
}
// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[helper gettranslation:@"LBL_510"] message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:[helper gettranslation:@"LBL_462"] style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
    dispatch_semaphore_signal(sem_att);
    
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    NSString *jsonArray = [NSJSONSerialization JSONObjectWithData:ResponseData options:NSJSONReadingAllowFragments error:&error];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[jsonArray dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    SignatureStorageUrl = [json valueForKey:@"FileURI"];
    dispatch_semaphore_signal(sem_att);
}

- (IBAction)donebtnaction:(id)sender
{
     if (![signatureView.signPath isEmpty]){
    context = [appdelegate managedObjectContext];
    [signatureView.pathArray addObject:signatureView.signPath];
  
    // NSData *saveData = [NSKeyedArchiver archivedDataWithRootObject:signatureView.pathArray];
    
   // [[NSUserDefaults standardUserDefaults] setObject:saveData forKey:USER_SIGNATURE_PATH];
   // [[NSUserDefaults standardUserDefaults] synchronize];
    
    signatureView.signPath = [signatureView.pathArray objectAtIndex:0];
    
    UIGraphicsBeginImageContextWithOptions(signatureView.bounds.size, NO, 0.0); //size of the image, opaque, and scale (set to screen default with 0)
    [signatureView.signPath stroke]; //or [path stroke]
    UIImage *myImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
   
    
    
  //  NSData *saveData = UIImageJPEGRepresentation(myImage, 0.8);
  
    NSData *saveData = UIImagePNGRepresentation(myImage);
    
    [helper showWaitCursor:[helper gettranslation:@"LBL_538"]];
    sem_att = dispatch_semaphore_create(0);
    NSString *SignatureName = [NSString stringWithFormat:@"AttendeeSignature1.png"];
    [self upload_attachmentdata:SignatureName base64:[saveData base64EncodedStringWithOptions:kNilOptions]];
    while (dispatch_semaphore_wait(sem_att, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]]; }
    [helper removeWaitCursor];
    
    
    NSString *stringpt = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"Attendee"];
    NSError *error = nil;
    if(![[NSFileManager defaultManager]fileExistsAtPath:stringpt])
        [[NSFileManager defaultManager]createDirectoryAtPath:stringpt withIntermediateDirectories:NO attributes:nil error:&error];
    //NSString *filename = [stringpt stringByAppendingFormat:@"/%@.png",appdelegate.attendyid];
   
   
    filename = [stringpt stringByAppendingFormat:@"/%@.png",appdelegate.attendyid];
    NSLog(@"Filename is %@",filename);
    [saveData writeToFile:filename atomically:YES];
    
    
    
    
    if(saveData)
    {
        [saveData writeToFile:filename atomically:YES];
        
       // NSLog(@"filedownload location %@",filename);
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event_Attendee" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",appdelegate.attendyid];
        
        
        NSError *error;
        
        [fetchRequest setEntity:entity];
        
        [fetchRequest setPredicate:predicate];
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        
        if([fetchedObjects count]>0)
        {
            for (NSManagedObject *object in fetchedObjects)
            {
                [object setValue:SignatureStorageUrl forKey:@"signatureid"];
                [object setValue:@"Y" forKey:@"signflag"];
                [object setValue:getstatus forKey:@"status"];
                [object setValue:@"Signed" forKey:@"statuslic"];
                [object setValue:CurrentDateStr forKey:@"signaturedate"];
            }
            [context save:&error];
            
        }
        [signatureView captureSignature];
    }
    
    [self updateAttendeeStatus];
    [self removepopup];
     }
}
- (IBAction)closebtnaction:(id)sender
{
    [self removepopup];
}

- (IBAction)cancelbtnaction:(id)sender
{
    [self removepopup];
}

- (IBAction)erasebtnaction:(id)sender
{
    
    displayimageview.hidden = YES;
    signatureView.hidden = NO;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_SIGNATURE_PATH];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [signatureView.pathArray removeAllObjects];
    signatureView.signPath = [UIBezierPath bezierPath];
    [signatureView.signPath setLineWidth:2.0];
    [signatureView.signPath setLineCapStyle:kCGLineCapRound];
    [signatureView.signPath setLineJoinStyle:kCGLineJoinRound];
    [signatureView setNeedsDisplay];
}
-(void)removepopup
{
    appdelegate.swipevar = @"SignClosed";
    [self performSegueWithIdentifier:@"SignatureToEventDetail" sender:self];
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}

-(void) updateAttendeeStatus
{
    [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    NSMutableArray *EventAttendee_ar = [[NSMutableArray alloc]init];
    NSArray*attendeedetail = [[helper query_alldata:@"Event_Attendee"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id like %@",appdelegate.attendyid]];
    
    //NSString *Cuurentdatestr =
    
    NSMutableDictionary *eventattendee_dict = [[NSMutableDictionary alloc]init];
        
    if ([attendeedetail count]>0)
    {
        
    
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"attendeeid"] ?: @"" forKey:@"AttendeeID"];
    [eventattendee_dict setValue:appdelegate.eventid forKey:@"EventID"];
    [eventattendee_dict setValue:appdelegate.attendyid forKey:@"ID"];
   // [eventattendee_dict setValue:[[attendeedetail valueForKey:@"owner"] objectAtIndex:0] forKey:@"Owner"];
    [eventattendee_dict setValue:getstatus forKey:@"Status"];
    [eventattendee_dict setValue:@"Signed" forKey:@"StatusLIC"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"firstname"] ?: @"" forKey:@"FirstName"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"lastname"] ?: @"" forKey:@"LastName"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"salutation"] ?: @"" forKey:@"Salutation"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"salutationlic"] ?: @"" forKey:@"SalutationLIC"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"speciality"] ?: @"" forKey:@"Speciality"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"specialitylic"] ?: @"" forKey:@"SpecialityLIC"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"targetclass"] ?: @"" forKey:@"TargetClass"];
    [eventattendee_dict setValue:[attendeedetail[0] valueForKey:@"targetclasslic"] ?: @"" forKey:@"TargetClassLIC"];
    [eventattendee_dict setValue:SignatureStorageUrl forKey:@"SignatureID"];
    [eventattendee_dict setValue:CurrentDateStr forKey:@"SignatureDate"];
    
    [eventattendee_dict setValue:[helper stringtobool:@"Y"] forKey:@"SignFlag"];
    
    [EventAttendee_ar addObject:eventattendee_dict];
    
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:EventAttendee_ar  options:0 error:&error];
            if (jsonData)
            {
          
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
    
            NSPredicate *att_pred = [NSPredicate predicateWithFormat:@"attendeeid like %@",appdelegate.attendyid];
            [helper delete_predicate:@"Event_Attendee" predicate:att_pred];
    
           // [helper addEventAttendee:appdelegate.eventid EventAttendeedata:EventAttendee_ar];
    
    
            NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
            NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
            NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
            NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetEventAttendee" pageno:@"" pagecount:@"" lastpage:@""];
    
                    appdelegate.senttransaction = @"Y";
//     NSMutableDictionary *AUTHWEBAPI_response = [helper invokewebservice:msgbody invokeAPI:@"SL_Tx" HTTPMethod:@"POST" parameters:nil headers:nil];
//    if (![[AUTHWEBAPI_response valueForKey:@"response_code"]  isEqual: @"000"])
//    {
//        [helper removeWaitCursor];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[helper gettranslation:@"LBL_545"] message:[AUTHWEBAPI_response valueForKey:@"response_msg"] delegate:nil  cancelButtonTitle:[helper gettranslation:@"LBL_462"] otherButtonTitles:nil];
//        
//        [alert show];
//        [self removepopup];
//    }
//    else
//    {
//        NSString *authresponse =  [helper decryptData:[[AUTHWEBAPI_response valueForKey:@"response_msg"] valueForKey:@"payload"] withKey:key];
//        NSData *resultData = [authresponse dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:nil];
//        
//    }
    
    
         [helper insert_transaction_local:msgbody entity_id:appdelegate.attendyid type:@"Update Event Attendee" entityname:@"Event Attendee" Status:@"Open"];
        
        
    }
    //[helper addEventAttendee:appdelegate.eventid EventAttendeedata:EventAttendee_ar];
    
    [helper removeWaitCursor];
}
@end
