//
//  updateactivity_ViewController.h
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"
#import "AppDelegate.h"
#import "SLKeyChainStore.h"
#import "EncryptedStore.h"
#import "Reachability.h"
#import "SOCustomCell.h"

@interface updateactivity_ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) HelperClass *helper;
@property (nonatomic) Reachability* reachability;

@property (strong, nonatomic) IBOutlet UILabel *message_header;
@property (strong, nonatomic) IBOutlet UILabel *message_detail;
@property (strong, nonatomic) IBOutlet UITextView *message_comment;


- (IBAction)reason_code:(id)sender;
- (IBAction)submit_btn:(id)sender;
- (IBAction)cancel_btn:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *status_tbl;

@property(nonatomic) NSString *senderView;

@property(nonatomic) NSString *activity_id;
@property(nonatomic) NSArray *activity_data;

@property(nonatomic) NSString *cancelSegue;
@property(nonatomic) NSString *lstunwindSegue;



@end

