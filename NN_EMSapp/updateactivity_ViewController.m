//
//  updateactivity_ViewController.m
//  NN_EMSapp
//
//  Created by iWizardsIV on 9/12/16.
//  Copyright © 2016 iWizards. All rights reserved.
//

#import "updateactivity_ViewController.h"

@interface updateactivity_ViewController ()
{
    NSManagedObjectContext *context;
    dispatch_queue_t squeue;
    AppDelegate *appdelegate;
}

@end

@implementation updateactivity_ViewController
{
    
    UIActivityIndicatorView * activityView;
    UIView *loadingView;
    NSTimer *timer;
    NSArray *status_data;

    NSUInteger count;
    UITextView *TextView;
    
    SLKeyChainStore *keychain;
    MSClient *client;
    NSString * AESPrivateKey;
    IBOutlet UIButton *reason_code_btn;
    NSString *rejectreason_code;
    NSString *selected_statuslic;
    NSString *selected_status;
    NSPredicate *lovPredicate;
    IBOutlet UIView *parentview;
}

@synthesize helper,message_header,message_detail,message_comment,activity_id,activity_data,senderView,status_tbl,cancelSegue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Declare Variables:
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    helper = [[HelperClass alloc] init];
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
  
   keychain = [SLKeyChainStore keyChainStoreWithService:appdelegate.keychainName];
    
    AESPrivateKey =[helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    
    squeue = dispatch_queue_create("iwiz.serial.Queue", NULL);
    status_data = [helper query_alldata:@"Event_LOV"];
    
    NSLog(@"activity_id: %@",activity_id);
    
    status_tbl.hidden = YES;
//    selectedreason = [reason_code_btn.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];

    lovPredicate = [NSPredicate predicateWithFormat:@"SELF.lovtype like %@",@"ACT_STATUS"];
    status_data = [[helper query_alldata:@"Event_LOV"] filteredArrayUsingPredicate:lovPredicate];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self adjustHeightOfTableview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return status_data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOCustomCell *customcell;
    NSString *cellIdentifier = @"customcell";
    
    customcell = [status_tbl dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *contentView = customcell.contentView;
    
    UILabel *status_lv = (UILabel *) [contentView viewWithTag:1];
    
    [status_lv setText:[status_data[indexPath.row] valueForKey:@"value"]];
    
    
    if (indexPath.row % 2)
    {
        customcell.contentView.backgroundColor = [[UIColor alloc]initWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1] ;
    }
    else
    {
        customcell.contentView.backgroundColor = [[UIColor alloc]initWithRed:239/255.0 green:248/255.0 blue:253/255.0 alpha:1];
    }
    
    return customcell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [reason_code_btn setTitle: [status_data[indexPath.row] valueForKey:@"value"] forState: UIControlStateNormal];
    selected_status = [status_data[indexPath.row] valueForKey:@"value"];
    selected_statuslic = [status_data[indexPath.row] valueForKey:@"lic"];
    status_tbl.hidden = YES;
    
}

- (void)adjustHeightOfTableview
{
    CGFloat height = status_tbl.contentSize.height;
    CGFloat maxHeight = status_tbl.superview.frame.size.height - status_tbl.frame.origin.y;
    if (height > maxHeight)
        height = maxHeight;
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = status_tbl.frame;
        frame.size.height = height;
        status_tbl.frame = frame;
    }];
}

- (IBAction)reason_code:(id)sender
{
    if (status_tbl.hidden)
    {
        status_tbl.hidden = NO;
    }
    else
    {
        status_tbl.hidden = YES;
    }
    
    [self adjustHeightOfTableview];
}

- (IBAction)submit_btn:(id)sender
{
    [message_comment resignFirstResponder];
    
    [helper showWaitCursor:[helper gettranslation:@"LBL_537"]];
    
    NSMutableArray *activityupdate_ar = [[NSMutableArray alloc]init];
    
    activity_data = [helper query_alldata:@"Activities"];
    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"activityid like %@",activity_id];
    activity_data = [activity_data filteredArrayUsingPredicate:aPredicate];
    
    NSString *rowid = [[activity_data valueForKey:@"activityid" ]objectAtIndex:0];
    NSString *eventid = [[activity_data valueForKey:@"eventid" ]objectAtIndex:0];
    
    NSMutableDictionary *activityupdate_dict = [[NSMutableDictionary alloc]init];
        
    [activityupdate_dict setValue:[[activity_data valueForKey:@"affiliateid" ]objectAtIndex:0] forKey:@"AffiliateID"];
    [activityupdate_dict setValue:eventid forKey:@"EventID"];
    [activityupdate_dict setValue:rowid forKey:@"ActivityID"];
    [activityupdate_dict setValue:message_comment.text forKey:@"Comment"];
    [activityupdate_dict setValue:selected_statuslic forKey:@"StatusLIC"];
    [activityupdate_dict setValue:selected_status forKey:@"Status"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"desc" ]objectAtIndex:0] forKey:@"Description"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"enddate" ]objectAtIndex:0] forKey:@"EndDate"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"ownedby" ]objectAtIndex:0] forKey:@"OwnedBy"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"priority" ]objectAtIndex:0] forKey:@"Priority"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"startdate" ]objectAtIndex:0] forKey:@"StartDate"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"type" ]objectAtIndex:0] forKey:@"Type"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"eventname" ]objectAtIndex:0] forKey:@"EventName"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"prioritylic" ]objectAtIndex:0] forKey:@"PriorityLIC"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"typelic" ]objectAtIndex:0] forKey:@"TypeLIC"];

    [activityupdate_dict setValue:[[activity_data valueForKey:@"plan_name" ]objectAtIndex:0] forKey:@"PlanName"];
    [activityupdate_dict setValue:[[activity_data valueForKey:@"visibility" ]objectAtIndex:0] forKey:@"Visibility"];
    
    [activityupdate_ar addObject:activityupdate_dict];
   
    
    NSString *jsonString;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:activityupdate_ar options:0 error:&error];
    
    if (! jsonData)
    {
        // NSLog(@"Got an error: %@", error);
    } else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }

    //insert data to database
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Activities" inManagedObjectContext:context];
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id like %@",appdelegate.attendyid];
    
    
   // NSError *error;
    
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPredicate:aPredicate];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    if([fetchedObjects count]>0)
    {
        for (NSManagedObject *object in fetchedObjects)
        {
            [object setValue:message_comment.text forKey:@"comment"];
            [object setValue:selected_statuslic forKey:@"statuslic"];
            [object setValue:selected_status forKey:@"status"];
           
        }
        [context save:&error];
        
    }

    
    
    NSString *key = [helper query_data:@"name" inputdata:@"aesprivatekey" entityname:@"S_Config_Table"];
    NSData *encryptedstring = [helper encryptString:jsonString withKey:key];
    NSString *base64String = [encryptedstring base64EncodedStringWithOptions:0];
    NSDictionary *msgbody = [helper create_trans_msgbody:base64String txType:@"EMSSetActivity" pageno:@"" pagecount:@"" lastpage:@""];
    
   /* Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        dispatch_sync(squeue,
        ^{
            [helper insert_transaction_local:msgbody entity_id:rowid type:@"Update Activity" entityname:@"Event Activity" Status:@"Open"];
            [helper removeWaitCursor];
            [ToastView showToastInParentView:self.view withText:@"No Internet connection! Request is stored and will be Processed when once the conectivity is restored." withDuaration:2.0];
        });
    }
    else
    {
        dispatch_sync(squeue, ^ { [helper insert_transaction_local:msgbody entity_id:rowid type:@"Update Activity" entityname:@"Event Activity" Status:@"Open"];});
        dispatch_sync(squeue, ^ { [helper send_transaction_server];});
    }*/
    
    
    
    appdelegate.senttransaction = @"Y";
    [helper insert_transaction_local:msgbody entity_id:rowid type:@"Update Activity" entityname:@"Event Activity" Status:@"Open"];
    [helper removeWaitCursor];
     [self removepopup];
    
}


- (IBAction)cancel_btn:(id)sender
{
    [self removepopup];
    
}

-(void)removepopup
{
    appdelegate.swipevar = @"activityview";
    
    [self performSegueWithIdentifier:cancelSegue sender:self];
    [UIView animateWithDuration:0.3f animations:^{ self.view.alpha = 0.0f; } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

//-----------------------------------------------------------------------------------------------------------------
- (void) reachabilityChanged:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        //        NSLog(@"Disconnected from network");
    }
    else
    {
    }
}


/*
-(void)textFieldDidChange :(UITextField *)theTextField
{
    NSString *searchText = message_comment.text;
    if (count > searchText.length)
    {
        if ([self.senderView isEqualToString:@"PRView"])
        {
            PRrejectcode_data = nil;
            PRrejectcode_data = [helper query_predicate_reqno:@"PurchaseReq_rejectcodes" name:@"requisition_no" value:purchase_req_no];
        }
        else
        {
            POrejectcode_data = nil;
            POrejectcode_data = [helper query_predicate_po:@"PurchaseOrder_rejectcode" name:@"purchaseorder_no" value:purchase_order_no];
        }
    }
    else
    {
        // NSLog(@"Text has added");
    }
    
    if(searchText.length == 0)
    {
        if ([self.senderView isEqualToString:@"PRView"])
        {
            PRrejectcode_data = [helper query_predicate_reqno:@"PurchaseReq_rejectcodes" name:@"requisition_no" value:purchase_req_no];
        }
        else
        {
            POrejectcode_data = [helper query_predicate_po:@"PurchaseOrder_rejectcode" name:@"purchaseorder_no" value:purchase_order_no];
        }
        
    }
    else
    {
        if ([self.senderView isEqualToString:@"PRView"])
        {
            PRrejectcode_data = [helper query_predicate_reqno:@"PurchaseReq_rejectcodes" name:@"requisition_no" value:purchase_req_no];
        }
        else
        {
            POrejectcode_data = [helper query_predicate_po:@"PurchaseOrder_rejectcode" name:@"purchaseorder_no" value:purchase_order_no];
        }
        
        
        count = searchText.length;
        [filteredrejectArray removeAllObjects];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((SELF.reject_reasoncode contains[c] %@) OR (SELF.reject_reasondesc contains[c] %@))",searchText,searchText];
        
        if ([self.senderView isEqualToString:@"PRView"])
        {
            filteredrejectArray = [NSMutableArray arrayWithArray:[PRrejectcode_data filteredArrayUsingPredicate:predicate]];
            PRrejectcode_data = [filteredrejectArray copy];
        }
        else
        {
            filteredrejectArray = [NSMutableArray arrayWithArray:[POrejectcode_data filteredArrayUsingPredicate:predicate]];
            POrejectcode_data = [filteredrejectArray copy];
        }
        
        
        
        
    }
    
    [rejectcodes_tbl reloadData];
    [rejectcodes_tbl reloadInputViews];
    
}
 */

@end
