//
//  SEssentialsAccordion.h
//  Accordion
//
//  Copyright (c) 2012 Scott Logic Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEssentialsAccordionSection.h"
#import "SEssentialsAccordionSectionHeader.h"
#import "SEssentialsAccordionDelegate.h"
#import "SEssentialsAccordionDataSource.h"
#import "SEssentialsCommon.h"

typedef NS_ENUM(NSInteger, SEssentialsAccordionType) {
    SEssentialsAccordionTypeFlexible,
    SEssentialsAccordionTypeFixed
};

/**
 An instance of `SEssentialsAccordion` (or simply, an accordion) is a means for
 displaying lists of information, where some of the information is shown, and
 some of the information is hidden.  An accordion is made up of zero or more
 sections.  You can open or close sections to display or hide the information
 they contain.
 
 Each section is implemented using the `SEssentialsAccordionSection` class. 
 A section has a header view which is an instance of `SEssentialsAccordionSectionHeader`, and a content view which is a standard `UIView`.
 You can either define a custom view for the header, or you can simply define a
 title for the section and use the default header view.  You supply the content
 to be displayed in each section.
 
 Each section header contains an instance of `SEssentialsAccordionSectionHeaderStyle`.  This style object manages the appearance of the section.
 
 An accordion must have an object that acts as a datasource. The datasource
 must adopt the `SEssentialsAccordionDataSource` protocol. 
 The datasource provides the content for each of the sections when they are
 displayed in "open" mode.
 
 The content supplied by the datasource is only retained by the Accordion while the content is visible on screen, and will be released when scrolled off screen or the section is closed. It is up to the class adopting the `SEssentialsAccordionDataSource` protocol to retain content so it can be requested at any time.
 
 An accordion can optionally have an object that acts as a delegate. 
 The delegate must adopt the `SEssentialsAccordionDelegate` protocol. The delegate
 is notified when sections are opened, closed, added, deleted or moved.
 
 There are currently two types of accordion:
 
 - A fixed accordion only shows the contents of one section at a time.
   When a section is opened, the previously open section is closed. The accordion
   has a fixed size, and does not scroll. 
 - A flexible accordion allows multiple sections to be open at the same time. 
   Its contents can be bigger than its frame. In this case, it will scroll up and down.
 
 @sample AccordionGettingStarted
 @sample AccordionHowToMoveSections
 
 */

@interface SEssentialsAccordion : UIView

/** The datasource provides the content for each of the accordion sections */
@property (assign, nonatomic)   id <SEssentialsAccordionDataSource> dataSource;

/** The delegate is notified when sections are opened, closed, deleted or removed. */
@property (assign, nonatomic)   id<SEssentialsAccordionDelegate>   delegate;

/**
 The type of Accordion. Defaults to `SEssentialsAccordionTypeFlexible`.
 
 There are currently two types of accordion.
 
 A fixed accordion only shows the contents of one section at a time.  When a section is opened, the previously open section is closed.  The accordion has a fixed size, and does not scroll.
 
 A flexible accordion allows multiple sections to be open at the same time.  Its contents can be bigger than its frame.  In this case, it will scroll up and down.
 
     typedef enum {
       SEssentialsAccordionTypeFlexible,
       SEssentialsAccordionTypeFixed
     } SEssentialsAccordionType;
 */
@property (assign, nonatomic)   SEssentialsAccordionType type;


/** @name Section Management
 
 These methods and properties are used to manage the sections present in the accordion.
 To populate an accordion with sections you need to create `SEssentialsAccordionSection`
 objects and then pass them to the accordion.
 */
#pragma mark - Section Management

/**
 A shallow copy of the sections which the accordion control contains.
 */
@property (retain, nonatomic, readonly)   NSArray  *sections;

/**
 Does the accordion populate section content on loading the section, or
 on opening the section? Defaults to `NO`.

 If `YES`, load content only when section is opened.
 If `NO`, eagerly load content when section is created. Cannot be set while the dataSource is set,
 and will throw an exception if this is attempted. Instead, the dataSource should
 be set to nil before changing this value.
 */
@property (assign, nonatomic)   BOOL lazyLoading;

#pragma mark - Section Management
/** @name Section Management */

/**
 Add a single section to the accordion. This will appear at the end (bottom) of the
 accordion
 @param section The `SEssentialsAccordionSection` to be added
 */
- (void)addSection:(SEssentialsAccordionSection*)section;

/**
 Add an array of sections to the accordion.
 @param sections The collection of `SEssentialsSection` objects to be added
 */
- (void)addSectionsFromArray:(NSArray*)sections;

/**
 Adds a single section to the accordion at the specified index. This will push the
 sections below (i.e. with this index or higher) down the accordion.
 @param section The `SEssentialsSection` to be added
 @param index The index at which to add the section
 */
- (void)insertSection:(SEssentialsAccordionSection*)section atIndex:(NSInteger)index;

/**
 Removes a section from the accordion. The removal will be animated.
 @param section The `SEssentialsSection` to be removed
 */
- (void)removeSection:(SEssentialsAccordionSection*)section;

/**
 Removes the section at the specified index from the accordion
 @param index The index of the section to be removed
 */
- (void)removeSectionAtIndex:(NSInteger)index;

/**
 Removes all sections from the accordion. This will reset the content and leave
 a completely empty accordion.
 */
- (void)removeAllSections;

/**
 Moves a section within the accordion
 @param fromIndex The index where the section can currently be found
 @param toIndex The index to which the section should be moved
 @exception NSInvalidArgumentException Thrown if the supplied indices are invalid
 */
- (void)moveSectionFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;

/**
 Reloads the content of a section, by fetching the new content from the dataSource.
 @param section The section to reload
 */
-(void)reloadSection:(SEssentialsAccordionSection*)section;

/**
 Reloads the contents of all sections, by fetching new content from the dataSource.
 */
-(void)reloadAllSections;

/** @name User interaction */
#pragma mark - User interaction

/** Specifies whether or not the 'move' and 'delete' accessories are visible, allowing movement and deletion.  This defaults to `NO`. */
@property (assign, nonatomic)   BOOL editing;

/** Whether or not you can swipe to delete a section within the accordion. This defaults to `YES`. */
@property (assign, nonatomic)   BOOL editable;

/** Specifies whether the accordion bounces when it reaches the limit of its scrolling.  This defaults to `YES`. */
@property (assign, nonatomic) BOOL scrollBounceEnabled;

/** Specifies whether the topmost section should slide under its header until pushed off screen. This defaults to `YES`. */
@property (assign, nonatomic) BOOL floatingHeader;


/** @name Styling */
#pragma mark - Styling

/** The default radius which is given to the corners of the accordion.  This defaults to `5` points. */
@property (assign, nonatomic) CGFloat cornerRadius;


/** @name Theming */
#pragma mark - Theming

/** Updates the style of the accordion using the specified theme.
 */
- (void)applyTheme:(SEssentialsTheme*)theme;

@end
