//
//  SEssentialsAccordionDelegate.h
//  Accordion
//
//  Copyright (c) 2012 Scott Logic Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SEssentialsAccordion.h"

/**
 The delegate of a `SEssentialsAccordion` object must adopt the `SEssentialsAccordionDelegate` protocol.  Optional methods of the protocol allow the delegate to keep track of when sections in the accordion are opened, closed, added, deleted or moved.
 */
@protocol SEssentialsAccordionDelegate <NSObject>

@required

@optional

/** @name Section Opening */
/** Called before a section is opened. */
- (BOOL)accordion:(SEssentialsAccordion *)accordion shouldOpenSection:(SEssentialsAccordionSection *)section;

/** Called before a section is opened. */
- (void)accordion:(SEssentialsAccordion *)accordion willOpenSection:(SEssentialsAccordionSection *)section;

/** Called when a section has been opened. */
- (void)accordion:(SEssentialsAccordion *)accordion didOpenSection:(SEssentialsAccordionSection *)section;


/** @name Section Closing */
/** Called before a section is closed. */
- (BOOL)accordion:(SEssentialsAccordion *)accordion shouldCloseSection:(SEssentialsAccordionSection *)section;

/** Called before a section is closed. */
- (void)accordion:(SEssentialsAccordion *)accordion willCloseSection:(SEssentialsAccordionSection *)section;

/** Called when a section has been closed. */
- (void)accordion:(SEssentialsAccordion *)accordion didCloseSection:(SEssentialsAccordionSection *)section;


/** @name Section Removal */
/** Called before a section is deleted. */
- (BOOL)accordion:(SEssentialsAccordion *)accordion shouldRemoveSection:(SEssentialsAccordionSection *)section;

/** Called before a section is deleted. */
- (void)accordion:(SEssentialsAccordion *)accordion willRemoveSection:(SEssentialsAccordionSection *)section;

/** Called when a section has been deleted. */
- (void)accordion:(SEssentialsAccordion *)accordion didRemoveSection:(SEssentialsAccordionSection *)section;


/** @name Section Insertion */
/** Called before a new section is inserted into the accordion.  */
- (void)accordion:(SEssentialsAccordion *)accordion willInsertSection:(SEssentialsAccordionSection *)section atIndex:(NSInteger)index;

/** Called when a new section has been inserted into the accordion. */
- (void)accordion:(SEssentialsAccordion *)accordion didInsertSection:(SEssentialsAccordionSection *)section atIndex:(NSInteger)index;

/** @name Section Moving */

/**
 Called before a section is moved. Returning `NO` will block the section from moving.
 
 @param accordion The `SEssentialsAccordion` sending the message
 @param fromIndex The start index of the moving section
 @param toIndex The final index of the moving section
 */
- (BOOL)accordion:(SEssentialsAccordion *)accordion shouldMoveSectionFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
/**
 Called when a section is about to move.
 
 @param accordion The `SEssentialsAccordion` sending the message
 @param fromIndex The start index of the moving section
 @param toIndex The final index of the moving section
 */
- (void)accordion:(SEssentialsAccordion *)accordion willMoveSectionFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;

/** 
 Called when a section has been moved.
 
 @param accordion The `SEssentialsAccordion` sending the message
 @param fromIndex The start index of the moving section
 @param toIndex The final index of the moving section
 */
- (void)accordion:(SEssentialsAccordion *)accordion didMoveSectionFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;

@end
