//
//  SEssentialsAccordionSection.h
//  Accordion
//
//  Copyright (c) 2012 Scott Logic Ltd. All rights reserved.
//

/* The SEssentialsAccordionSection is a UIView representing an individual section within a SEssentialsAccordion */

#import <UIKit/UIKit.h>

@class SEssentialsAccordion, SEssentialsAccordionSectionHeader, SEssentialsAccordionSectionHeaderStyle, SEssentialsView;

/**
 The `SEssentialsAccordionSection` class holds information about a single section
 in an `SEssentialsAccordion`. Each section has a header of type
 `SEssentialsAccordionSectionHeader` and a content view of type `UIView`.
 
 @warning Since `SEssentialsAccordionSection` is a subclass of `UIView` it cannot be used as
 a key in an `NSDictionary` (it does not adopt the `NSCopying` protocol). Therefore
 if you wish to have a dictionary of { sections => content } then you should use
 `NSValue` with a pointer to the section as the key:
     [NSValue valueWithNonretainedObject: section]
 
 **Note:** if the section is released, the NSValue in the dictionary will become a
 junk pointer.
 
 */
@interface SEssentialsAccordionSection : UIView

/**
 A reference to the accordion within which the section is contained. Readonly,
 and set by the accordion when the section is added to it.
 */
@property (readonly, assign, nonatomic) SEssentialsAccordion *accordion;

/**
 The header for the accordion section. This is readonly because the header
 view should be provided at construction time.
 */
@property (readonly, retain, nonatomic) SEssentialsAccordionSectionHeader   *header;

/**
 The content for the view. This property is readonly because the content is
 provided by the accordion's datasource.
 */
@property (readonly, retain, nonatomic) UIView  *content;

/**
 Boolean indicating whether or not the section is currently open.
 When set to `YES` it opens the section. This means the section will 
 display the content view provided by the accordion's datasource. 
 If in fixed mode the accordion will close the currently open view. 
 When set to `NO` closes the section. The section display will consist 
 solely of its header view.
 */
@property (nonatomic, assign) BOOL open;

/**
 An `SEssentialsAccordionSection` is typically created like this with default
 implementation for the header. Creates the section, with header and content.
 Applies the style and title to the header.
 @param frame The frame for the `SEssentialsAccordionSection`. The width of this
   will be changed to match that of the accordion when you add the section to an
   accordion.
 @param title The title to be displayed in the header
 */
- (id)initWithFrame:(CGRect)frame andTitle:(NSString *)title;

/**
 Use this initializer to create the section by supplying the header. Creates the
 section, with content and the given header. Applies the style to the header.
 @param frame The frame for the `SEssentialsAccordionSection`. The width of this
   will be changed to match that of the accordion when you add the section to an
   accordion.
 @param header An `SEssentialsAccordionSectionHeader` to use as the header for the section
 */
- (id)initWithFrame:(CGRect)frame andHeader:(SEssentialsAccordionSectionHeader *)header;



@end
