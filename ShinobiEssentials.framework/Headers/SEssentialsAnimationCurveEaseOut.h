//
//  SEssentialsCarouselCurveEaseOut.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsAnimationCurve.h"

@interface SEssentialsAnimationCurveEaseOut : SEssentialsAnimationCurve

@end
