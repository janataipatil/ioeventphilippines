//
//  SEssentialsCarouselLinear.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsCarousel.h"
/**
 An `SEssentialsCarouselLinear2D` is an `SEssentialsCarousel` which displays all items in a line, either with horizontal or vertical alignment, with a constant spacing between items.

 <img src="../docs/Docs/img/Linear2D.png" />
 
 The `SEssentialsCarouselLinear2D` allows for changes in layout specific to a linear display. To control other aspects of the carousel, including item management, wrapping, and gestures, use the methods and properties on the `SEssentialsCarousel` abstract base class.
 
 */
@interface SEssentialsCarouselLinear2D : SEssentialsCarousel

/** If `SEssentialsCarouselOrientationHorizontal`, items are laid out left-to-right. If `SEssentialsCarouselOrientationVertical`, items are laid out top-to-bottom. Defaults to `SEssentialsCarouselOrientationHorizontal`.
 
    typedef enum
    {
        SEssentialsCarouselOrientationHorizontal,
        SEssentialsCarouselOrientationVertical
    } SEssentialsCarouselOrientation;
 */
@property (nonatomic, assign) SEssentialsCarouselOrientation orientation;

/** The amount of spacing to use between the edges of adjacent items, measured in points. This assumes all items are equally sized. Defaults to `5` pts. */
@property (nonatomic, assign) CGFloat itemSpacing;

/** Whether the carousel will allow the first and last items to move to the focus point, or to have them constrained to the edges of the carousel. If `YES`, the carousel will allow the items to settle at the focus point. If `NO`, the items will be constrained so they cannot move more than `carouselInset` pixels away from the edges of the carousel. Defaults to `YES`. */
@property (nonatomic, assign) BOOL allItemsCanReachFocusPoint;

/** The maximum amount of whitespace to display on either side of the carousel, when the carousel is not wrapped. Defaults to `5` pts. */
@property (nonatomic, assign) CGFloat carouselInset;

@end
