//
//  SEssentialsDecoratedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>

/**
 The `SEssentialsDecoratedView` allows decoration to be added to the wrapped UIView. This allows for ShinobiControls effects, such as reflection and shadows, to be applied to the `view`, which update as the wrapped `view` is updated.
 
 For more information on the individual effects, see the relevant class;
 
 - Shadows (`SEssentialsDecoratedShadowedView`)
 
 <img src="../docs/Docs/img/DecoratedShadedThumb.png" />
 
 - Fading (`SEssentialsDecoratedFadedView`)
 
 <img src="../docs/Docs/img/DecoratedFadedThumb.png" />
 
 - Reflection (`SEssentialsDecoratedReflectedView`)

 <img src="../docs/Docs/img/DecoratedReflectiveThumb.png" />

 - Double sided (`SEssentialsDecoratedDoubleSidedView`)
 
 - Cached (`SEssentialsDecoratedCachedView`)
 
 Effects can be chained together to allow for multiple effects. For example, to add fading around the edges of a `view`, then reflect it;
 
    SEssentialsDecoratedFadedView *faded = [ [ SEssentialsDecoratedFadedView alloc ] initWithView:view ];
    SEssentialsDecoratedReflectedView *reflected = [ [ SEssentialsDecoratedReflectedView alloc] initWithView:faded ];
 
 */
@interface SEssentialsDecoratedView : UIView

/** @name Setup */

/** Create an `SEssentialsDecoratedView` with a target view to apply effects to. The `SEssentialsDecoratedView` resizes to fit the subview exactly.
 @param view The view to apply effects to.
 */
- (id)initWithView:(UIView*)view;

/** The view which the `SEssentialsDecoratedView` was initialized with. */
@property (nonatomic, readonly, retain) UIView *view;

/** This forces the decorated view to update its contents. */ 
-(void)applyDecoration;

@end
