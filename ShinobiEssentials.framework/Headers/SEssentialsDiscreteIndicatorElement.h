//
//  SEssentialsDiscreteIndicatorElement.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>

/**
 `SEssentialsDiscreteIndicatorElement` is a `UIView` subclass, a collection of which
 can be arranged to form a ShinobiEssentials discrete progress or activity indicator.
 
 An `SEssentialsDiscreteIndicatorElement` has 2 embedded `UIViews` - one for the `on` state
 and one for the `off` state. The `onView` is overlaid on top of the `offView` and
 has its transparency varied according to the current indicator state.
 
 */

@interface SEssentialsDiscreteIndicatorElement : UIView {
    UIView *_onView;
    UIView *_offView;
}

/**
 Sets the alpha level of the `onView`. Varies between `0` and `1`, with `0` representing
 the element in the fully off state and `1` in the fully on state. Override this setter
 to change the way the `2` views interact as the activeProportion changes.
 */
@property (nonatomic, assign) CGFloat activeProportion;


/**
 Create an indicator element with the specified off and on views:
 @param frame The frame of the created element
 @param onView The `UIView` associated with the fully-on state
 @param offView The `UIView` associated with the fully-off state
 */
- (id)initWithFrame:(CGRect)frame onView:(UIView*)onView offView:(UIView*)offView;

/**
 Create an indicator element with the specified off and on images. This is a helper
 method which will create the appropriate `UIImageViews` for you.
 @param frame The frame of the created element
 @param onImage The `UIImage` associated with the fully-on state
 @param offImage The `UIImage` associated with the fully-off state.
 */
- (id)initWithFrame:(CGRect)frame onImage:(UIImage*)onImage offImage:(UIImage*)offImage;

@end
