//
//  SEssentialsDiscreteIndicatorElementFactory.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

#import "SEssentialsDiscreteIndicatorElement.h"
@class SEssentialsProgressIndicator;

/**
 `SEssentialsDiscreteIndicatorElementFactory` defines a factory used by the discrete
 progress and activity indicators for generating the individual elements which
 make up the indicator.
 */

@protocol SEssentialsDiscreteIndicatorElementFactory <NSObject>

@required

/**
 Should return an `SEssentialsDiscreteIndicatorElement` object which will become
 part of the specified `SEssentialsProgressIndicator`. This method will be called
 multiple times per indicator - once for each of the elements required.
 @param index An `NSInteger` representing the index of the indicator element in the progress indicator
 @param indicator The `SEssentialsProgressIndicator` which has requested this indicator element
 */
- (SEssentialsDiscreteIndicatorElement*)progressIndicatorElementAtIndex:(NSInteger)index forIndicator:(SEssentialsProgressIndicator*)indicator;

@end
