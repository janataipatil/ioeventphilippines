//
//  SEssentialsFlowLayout.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SEssentialsFlowLayoutDelegate.h"
#import "SEssentialsFlowLayoutStyle.h"
#import "SEssentialsCommon.h"


typedef NS_ENUM(NSInteger, SEssentialsHorizontalAlignment)
{
    SEssentialsAlignLeft,
    SEssentialsAlignCenter,
    SEssentialsAlignRight
};

typedef NS_ENUM(NSInteger, SEssentialsVerticalAlignment)
{
    SEssentialsAlignTop,
    SEssentialsAlignMiddle,
    SEssentialsAlignBottom
};

typedef NS_ENUM(NSInteger, SEssentialsAnimationType)
{
    SEssentialsAnimationLinear,
    SEssentialsAnimationSweep,
    SEssentialsAnimationFromTop,
    SEssentialsAnimationUser
};

typedef NS_ENUM(NSInteger, SEssentialsFlowDeleteIdiom)
{
    SEssentialsFlowDeleteIdiomTrashCan,
    SEssentialsFlowDeleteIdiomIcon
};

typedef NS_ENUM(NSInteger, SEssentialsEdgeConstraints)
{
    SEssentialsEdgeConstrainNone = 0,
    SEssentialsEdgeConstrainVertical = 1 << 0,
    SEssentialsEdgeConstrainHorizontal = 1 << 1
};

typedef NS_ENUM(NSInteger, SEssentialsEditButtonLocation)
{
    SEssentialsLocationTopRight,
    SEssentialsLocationRight,
    SEssentialsLocationBottomRight,
    SEssentialsLocationBottom,
    SEssentialsLocationBottomLeft,
    SEssentialsLocationLeft,
    SEssentialsLocationTopLeft,
    SEssentialsLocationTop
};

/**
 The `SEssentialsFlowLayout` is a `UIView` that manages the layout of its subviews into rows
 attempting to flow the subviews as text would be laid out on a page. It supports an
 edit mode that allows users to long press a subview and then move/delete it.
 
 Once a flow layout has been created, subviews are added using the `addManagedSubview:` method.
 These subviews will be arranged in raster order within the containing flow layout view.
 The gaps between adjacent views within a row are determined with the `horizontalSpacing`
 property, whilst the spacing between rows is managed with the `verticalSpacing` property.
 
 The height of each row is determined by the subview within that row with the largest
 height. The vertical alignment of the subviews within a row is given by the `verticalAlignment`
 property and the horizontal alignment of each row within the flow layout by the
 `horizontalAlignment` property.
 
 Managed subviews can be removed or replaced using the `removeManagedSubview:animated:` and
 `moveManagedSubview:toIndex:` methods. An array of managed subviews in their current
 display order is provided by `managedViews` and passing an array of the current subviews
 to `reorderManagedSubviews:animated:` will change their ordering to that defined in the array.
 
 The flow layout has a `style` object, which is an instance of `SEssentialsFlowLayoutStyle`.
 This manages the look and feel of the control by setting things like the tint color and
 background texture of the flow layout. The `style` object should always be used to update the
 look of the control, rather than accessing the flow layout and setting its properties directly.
 
 The style has precedence over any visual changes which are made to the flow layout directly.
 For example, if you were to set a property such as the background color on the flow layout directly,
 this change will be overridden the next time the style is updated. That is why it is important to
 use the style to manage the look and feel of the control.
 
 @sample FlowLayoutGettingStarted
 @sample FlowLayoutHowToSortItems
 
 */

@interface SEssentialsFlowLayout : UIScrollView

#pragma mark - Subview alignment properties
/** @name Subview alignment properties */

/**
 Horizontal alignment of subviews within each row. Defaults to `SEssentialsAlignLeft`.
 
    typedef enum
    {
       SEssentialsAlignLeft,
       SEssentialsAlignCenter,
       SEssentialsAlignRight
    } SEssentialsHorizontalAlignment;

 */
@property(nonatomic, assign) SEssentialsHorizontalAlignment horizontalSubviewAlignment;

/**
 Vertical alignment of subviews within each row. Defaults to `SEssentialsAlignTop`.

    typedef enum
    {
       SEssentialsAlignTop,
       SEssentialsAlignMiddle,
       SEssentialsAlignBottom
    } SEssentialsVerticalAlignment;

 */
@property(nonatomic, assign) SEssentialsVerticalAlignment verticalSubviewAlignment;

/**
 *  The horizontal minimum spacing between elements in points. Defaults to `14`.
 */
@property(nonatomic, assign)  CGFloat horizontalSubviewSpacing;

/**
 *  The vertical minimum spacing between rows in points. Defaults to `14`.
 */
@property(nonatomic, assign)  CGFloat verticalSubviewSpacing;

/** @name Mainview Alignment properties */

/**
 *  The horizontal padding at edges of parent view in points. Defaults to `8`.
 */
@property(nonatomic, assign)  CGFloat horizontalPadding;

/**
 *  The vertical padding at edges of parent view in points. Defaults to `8`.
 */
@property(nonatomic, assign)  CGFloat verticalPadding;

/**
 When moving a subview in edit mode this prevents the subview from positioning outside the bounds of the main view
 (defaults to `SEssentialsEdgeConstraintNone`)
 
    typedef enum
    {
       SEssentialsEdgeConstrainNone = 0,
       SEssentialsEdgeConstrainVertical = 1 << 0,
       SEssentialsEdgeConstrainHorizontal = 1 << 1
    } SEssentialsEdgeConstraints;
 
 */
@property (nonatomic, assign) SEssentialsEdgeConstraints edgeConstraints;

#pragma mark - Animation properties
/** @name Animation properties */

/**
 Animation type when subviews are laid out, defaults to `SEssentialsAnimationSweep`
 
    typedef enum
    {
       SEssentialsAnimationLinear,
       SEssentialsAnimationSweep,
       SEssentialsAnimationFromTop,
       SEssentialsAnimationUser
    } SEssentialsAnimationType;

 */
@property(nonatomic, assign) SEssentialsAnimationType animationType;

/**
 * The duration of the animation when views animate to a new position, or when they grow as they are selected.  This defaults to a quarter of a second.
 */
@property (nonatomic, assign) CGFloat movementAnimationDuration;

/**
 The duration of the wobble effect animation on views when they go into edit mode.  This defaults to a quarter of a second.
 */
@property(nonatomic, assign) CGFloat wobbleAnimationDuration;

/**
 *  Updates the layout during drag operations when `YES`
 *  otherwise waits until drag has stopped moving. (Defaults to `YES`)
 */
@property(nonatomic, assign) BOOL instantUpdate;

/**
 * Enable / disable the wobble effect during edit mode. (Defaults to `YES`)
 */
@property(nonatomic, assign) BOOL showEditWobble;

/** @name Edit Mode Properties */

/**
 * Shows an indicator marking the target insertion point during edit. (Defaults to `YES`)
 */
@property(nonatomic, assign) BOOL showDestinationMarker;

/**
 *  The minimum duration for pressing a subview before edit mode begins
 */
@property(nonatomic, assign)  CGFloat longPressDuration;

/**
 *  Allows user to edit layout with long press. (Defaults to `YES`)
 */
@property(nonatomic, assign) BOOL editable;

/**
 *  When moving a subview in edit mode this centers the subview on the touch point
 *  (defaults to `NO`)
 */
@property(nonatomic, assign) BOOL snapToCenter;


/**
 *  When `YES` a 'done' button is generated when edit mode begins (for `SEssentialsFlowDeleteIdiomIcon`)
 *  When `NO` the implementer must offer a means to end editing (e.g. a button or gesture) that
 *  calls `endEditMode` to end editing (defaults to `YES`)
 */
@property(nonatomic, assign) BOOL showDoneButton;

/**
 The location of the editing button in the flow layout, defaults to `SEssentialsLocationTopRight`.
 The editing button is the trash can for `SEssentialsFlowDeleteIdiomTrashCan` and the done button
 for `SEssentialsFlowDeleteIdiomIcon`.
 The value is ignored if an implementation of `editButtonPositionInFlowLayout:`
 is supplied in the `delegate`.

    typedef enum
    {
       SEssentialsLocationTopRight,
       SEssentialsLocationRight,
       SEssentialsLocationBottomRight,
       SEssentialsLocationBottom,
       SEssentialsLocationBottomLeft,
       SEssentialsLocationLeft,
       SEssentialsLocationTopLeft,
       SEssentialsLocationTop,
    } SEssentialsEditButtonLocation;
 */
@property(nonatomic, assign) SEssentialsEditButtonLocation editButtonLocation;

/**
*  Option to allow a managed view to be dragged outside the flow layout area.
*  When `YES`, the view will follow the gesture anywhere
*  When `NO`, the view will stop at the edge of the flow, until the gesture returns inside the flow bounds.
*/
@property(nonatomic, assign) BOOL dragsOutsideBounds;

/**
 A read only property set at creation that determines the style of the edit mode.
 The Trashcan idiom requires users to drag unwanted subviews to a trashcan to remove them.
 The Icon idiom adds delete buttons to each subview like the Apple home screen (default).


    typedef enum
    {
       SEssentialsFlowDeleteIdiomTrashCan,
       SEssentialsFlowDeleteIdiomIcon
    } SEssentialsFlowDeleteIdiom;

 The `SEssentialsFlowDeleteIdomIcon` idiom causes the managed views in the flow layout control to wobble, just like in the Apple home screen.  By default, the edges of a view are not anti-aliased, so can appear jagged during this animation.  In order to enable anti-aliasing, you can set the `UIViewEdgeAntialiasing` flag in your app's info.plist.  Bear in mind that this will have an impact on performance, as it requires Core Animation to do extra work in order to calculate the blending. 
 
 */
@property(nonatomic, readonly) SEssentialsFlowDeleteIdiom deleteIdiom;

#pragma mark - Styling
/** @name Styling */

/**
 *  Background, buttons and icons used in edit mode can be controlled through the style.
 */
@property(nonatomic, readonly, retain) SEssentialsFlowLayoutStyle *style;

/** Applies the specified theme to the flow layout. */
- (void)applyTheme:(SEssentialsTheme*)theme;

/**
 A readonly property giving an `NSArray` of `UIViews` which have been added to the Flow Layout.
 This should be used in place of `[layout subviews]`, as subviews may include additional `UIView` classes
 which were not added by the implementer.
 */
@property (nonatomic, readonly) NSArray *managedViews;

#pragma mark - Initializing an SEssentialsFlowLayout
/** @name Initializing an SEssentialsFlowLayout */

/**
 * Initialize view with the specified frame rectangle with default `deleteIdiom`  
 *
 * @param frame rectangle defining views extent
 */
- (id)initWithFrame:(CGRect)frame;

/**
 * Initialize view with the specified frame rectangle and `deleteIdiom`
 *
 * @param frame rectangle defining views extent
 * @param idiom see SEssentialsFlowDeleteIdiom
 */
- (id)initWithFrame:(CGRect)frame withDeleteIdiom:(SEssentialsFlowDeleteIdiom) idiom;

/**
 * Initialize view with the specified frame rectangle and `deleteIdiom` and a custom style
 *
 * @param frame rectangle defining views extent
 * @param idiom see `SEssentialsFlowDeleteIdiom`
 * @param style that controls icons and graphics
 */
- (id)initWithFrame:(CGRect)frame withDeleteIdiom:(SEssentialsFlowDeleteIdiom) idiom style:(SEssentialsFlowLayoutStyle *)style;

#pragma mark - Managing layout subviews
/** @name Managing layout subviews */

/**
 * Add a subview that has its position managed
 *
 * @param subview the subview to be added
 */
- (void) addManagedSubview:(UIView *) subview;

/**
 * Add a subview that has its position managed, at the specified index
 *
 * @param subview the subview to be added
 * @param index the index to insert at
 */

- (void)addManagedSubview:(UIView *)subview atIndex:(NSInteger)index;

/**
 * Remove a subview
 *
 * @param subview the subview to be removed
 * @param animated when `YES` a small fading animation occurs before removing subview
 */
- (void) removeManagedSubview:(UIView *) subview animated:(BOOL)animated;

/**
 * Stops managing a view, without destroying the view.
 *
 * @param subview the subview to be unmanaged
 */
- (void) unmanageSubview:(UIView *) subview;

/**
 * Move a subview
 *
 * @param subview the subview that is to be moved
 * @param index target index for destination in `orderedViews`. The item is added before the target index shifting items following it down the list
 */
- (void) moveManagedSubview:(UIView *)subview toIndex:(NSInteger)index;

/**
 * Reorder subviews
 *
 * @param newOrdering an array holding the subviews in the desired ordering.
 * @param animated a boolean specifying whether the reordering should be animated or not
 * The array contents must match `managedViews`, otherwise an exception is thrown.
 */
- (void) reorderManagedSubviews:(NSArray *)newOrdering animated:(BOOL)animated;


/**
 * Puts the layout into edit mode (i.e. delete icons and wobbling, if necessary)
 *
 */
- (void) beginEditMode;

/**
 * End the edit mode if it is in progress (i.e. removes delete icons, stops wobbling)
 *
 */
- (void) endEditMode;

/**
 * Causes items to be laid out, this may be useful if a managed subview changes its size
 * and the flow layout needs to respond to this.
 *
 */
- (void) forceLayout;

/**
 * Force a layout with animation beginning at point.
 *
 * @param point the point from which the layout will begin
 */
- (void) forceLayoutFromPoint:(CGPoint) point;

#pragma mark - Delegation
/** @name Delegation */

/**
 * The delegate for the `SEssentialsFlowLayout` edit events.
 * See `SEssentialsFlowLayoutDelegate`
 */
@property(assign)  id<SEssentialsFlowLayoutDelegate> flowDelegate;

@end
