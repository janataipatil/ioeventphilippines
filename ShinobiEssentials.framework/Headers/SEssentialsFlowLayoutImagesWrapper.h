//
//  SEssentialsFlowLayoutImagesWrapper.h
//  ShinobiEssentials
//
//  Created by Thomas Kelly on 28/08/2013.
//
//

#import <Foundation/Foundation.h>

/** A collection of images, used for creating an `SEssentialsFlowLayoutStyle`. Any values set to nil will use the default images instead. */
@interface SEssentialsFlowLayoutImagesWrapper : NSObject

/**
 This is the image which appears for flow elements to be dragged to for deletion
 when the `SEssentialsFlowLayout` is set to use the `SEssentialsFlowDeleteIdiomTrashCan`
 delete idiom.
 */
@property (nonatomic, retain) UIImage *trashcanImage;

/**
 This is mask defines where the trashcan will be colored with `trashcanTintColor`.
 */
@property (nonatomic, retain) UIImage *trashcanMask;

/**
 This is the image which appears to exit edit mode on a `SEssentialsFlowLayout` when
 the `SEssentialsFlowDeleteIdiomIcon` delete idiom is used. This is the image for
 the default state.
 */
@property (nonatomic, retain) UIImage *doneButtonImage;

/**
 This is the image which appears to exit edit mode on a `SEssentialsFlowLayout` when
 the `SEssentialsFlowDeleteIdiomIcon` delete idiom is used. This is the image for
 the pressed state.
 */
@property (nonatomic, retain) UIImage *doneButtonPressedImage;

/**
 This is the image which appears on the corner of individual flow elements during
 edit mode when the `SEssentialsFlowLayout` is using `SEssentialsFlowDeleteIdiomIcon`
 delete idiom. This is the image for the default button state.
 */
@property (nonatomic, retain) UIImage *deleteImage;

/**
 This is the image which appears on the corner of individual flow elements during
 edit mode when the `SEssentialsFlowLayout` is using `SEssentialsFlowDeleteIdiomIcon`
 delete idiom. This is the image for the pressed button state.
 */

@property (nonatomic, retain) UIImage *deletePressedImage;

@end
