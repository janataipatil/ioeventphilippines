//
//  SEssentialsAppleTheme.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsTheme.h"

/**
 `SEssentialsIOS7Theme` is a concrete implementation of `SEssentialsTheme` which follows the Apple guidelines for styling controls in iOS7.
 
 This is set as the default global theme when your app is running in iOS7. To explicitly set it as the default global theme in other versions of iOS, you can do the following:
 
 [ShinobiEssentials setTheme:[[[ SEssentialsIOS7Theme alloc] init] autorelease]];
 */
@interface SEssentialsIOS7Theme : SEssentialsTheme

@end
