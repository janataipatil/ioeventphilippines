//
//  SEssentialsProgressIndicatorStyle.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsStyle.h"

/**
 The `SEssentialsProgressIndicatorStyle` defines the look and feel for an instance of the
 `SEssentialsProgressIndicator` class.  If you wish to update how a progress or activity indicator looks, you should modify the properties of its style object.
 
 `SEssentialsProgressIndicatorStyle` derives from `SEssentialsStyle`, and so it is created from a
 `SEssentialsTheme` object. The theme with which it is created provides the default settings of the style.
 
 The default settings of the following properties on the control are set from the theme:
 
 - The `tertiaryTintColor` on the theme is used for the color of the completed portion of the indicator (`tintColor`).
 - The `inactiveTintColor` on the theme is used for the color of the incomplete portion of the indicator (`trackColor`).
 - The `primaryFont` on the theme defines the font of the progress label (`progressLabelFont`).
 - The `elementStyle` on the theme defines whether shading is applied to the completed portion of the indicator (`shadingEffect`), and whether a border is drawn around it (`trackBorderWidth`).
 
 @sample IndicatorsHowToCustomStyling
 */
@interface SEssentialsProgressIndicatorStyle : SEssentialsStyle

/** @name Common Properties */

/**
The color of the incomplete portion of the indicator. This defaults to the `inactiveTintColor` provided by the theme.
*/
@property (nonatomic,retain) UIColor *trackColor;

/**
The color of the completed portion of the indicator.  This defaults to the `tertiaryTintColor` provided by the theme.
*/
@property (nonatomic,retain) UIColor *tintColor;

/**
Boolean value which determines whether or not the indicator should
render shading effects. This defaults to the `elementStyle` property provided by the theme.
*/
@property (nonatomic,assign) BOOL shadingEffect;

/**
 In discrete indicators, this represents the diameter of default circle elements.  Note: this has no effect with user-defined elements.
 
 In continuous indicators, this represents the width of the main indicator element.
 
 Defaults to `10.0`.
 */
@property (nonatomic,assign) CGFloat elementSize;

/**
@name Continuous indicator properties
*/

/**
Represents the width of the border around the
foreground element in the continuous indicators. If `elementStyle` on the theme is set to `DECORATED`, this defaults to `2.0`.  Otherwise, this defaults to `0.0`.
*/
@property (nonatomic,assign) CGFloat trackBorderWidth;

/**
@name Discrete indicator properties
*/

/**
Determines whether the tail of a discrete activity
indicator should fade or not. If not, all the elements will either be in an `on`
state, or an `off` state. Defaults to `YES`.
*/
@property (nonatomic,assign) BOOL fadeTail;

/**
The number of elements present in a discrete
indicator. Defaults to `16`.
*/
@property (nonatomic,assign) NSUInteger numberOfElements;

/** Determines whether the progress label will be shown or not.  The label is only displayed for radial indicators, and is displayed in the center of the indicator.  Defaults to `NO`.  */
@property (nonatomic, assign) BOOL showProgressLabel;

/** The color of the progress label. Defaults to the `primaryTextColor` on the theme. */
@property (nonatomic,retain) UIColor *progressLabelColor;

/** The font used for the progress label. Defaults to the `primaryFont` on the theme. */
@property (nonatomic,retain) UIFont *progressLabelFont;

/** Determines whether the progress indicator has a background. Defaults to `NO`. */
@property (nonatomic, assign) BOOL showBackground;

/** The color of the progress indicators background. */
@property (nonatomic, retain) UIColor *indicatorBackgroundColor;

/** The padding in pixels around the indicator for the background. Defaults to `5` pixels. */
@property (nonatomic, assign) CGFloat backgroundPadding;

@end
