//
//  SEssentialsPullToActionSlideInVisualizer.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 25/02/2014.
//
//

#import "SEssentialsPullToActionBaseVisualizer.h"

/** The `SEssentialsPullToActionAlignBottomVisualizer` is an implementation of the `SEssentialsPullToActionVisualizer` that gives the visual
 appearance of the status view sliding inwards, aligned to the bottom of the Pull to Action control.
 
 This visualizer can be chosen by initializing the Pull to Action control providing the value `SEssentialsPullToActionStatusViewAlignmentBottom`
 to one of either `initWithScrollView:statusViewAlignment:` or `initWithFrame:statusViewAlignment:`.
 
 This is the default choice if a constructor without the parameter `statusViewAlignment:` is used.
 */
@interface SEssentialsPullToActionAlignBottomVisualizer : SEssentialsPullToActionBaseVisualizer

@end
