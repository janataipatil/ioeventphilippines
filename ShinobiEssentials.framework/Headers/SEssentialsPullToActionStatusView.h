//
//  SEssentialsPullToActionStatusView.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 25/02/2014.
//
//

#import <Foundation/Foundation.h>

#import "SEssentialsPullToActionState.h"

@class SEssentialsPullToActionStyle;

/** The `SEssentialsPullToActionStatusView` protocol defines the messages sent to a status view from it's parent `SEssentialsPullToAction` component, allowing the status view to update itself visually.
 */
@protocol SEssentialsPullToActionStatusView <NSObject>

@optional

/** Called by the Pull to Action's visualizer when a state change has occured.
 
 Use this opportunity to update the status view visually for any state changes.
 */
- (void)updateForState:(SEssentialsPullToActionState)state;

/** Called by the Pull to Action's visualizer whenever the pulled amount is changed.
 
 Use this opportunity to update the status view visually for the new pulled amount.
 */
-(void)updateForPulledAmountChanged:(CGFloat)pulledAmount pullThreshold:(CGFloat)pullThreshold;

/** Called by the Pull to Action control whenever the style object is updated.
 
 Use this opportunity to update the status view to conform to the new style.
 */
-(void)applyStyle:(SEssentialsPullToActionStyle *)style;

@end
