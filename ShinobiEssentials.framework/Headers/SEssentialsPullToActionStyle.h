//
//  SEssentialsPullToActionStyle.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 25/02/2014.
//
//

#import <UIKit/UIKit.h>

#import "SEssentialsStyle.h"

/**
 The `SEssentialsPullToActionStyle` defines the look and feel for an instance of the `SEssentialsPullToAction` class.  If you wish to configure how an Pull to Action control looks, you should set the relevant properties on this class.
 
 `SEssentialsPullToActionStyle` derives from `SEssentialsStyle`, and so it is initialized using a `SEssentialsTheme` object. The theme sets the default properties of the style.
 */
@interface SEssentialsPullToActionStyle : SEssentialsStyle

#pragma mark - Pull to Action
/** @name Pull to Action */

/** The color of the Pull to Action view. This defaults to the `activeTintColor` on the theme. */
@property (nonatomic, retain) UIColor *backgroundColor;

#pragma mark - Status View
/** @name Status View */

/** The color of the status view. This defaults to the `secondaryTintColor` on the theme. */
@property (nonatomic, retain) UIColor *statusViewBackgroundColor;

/** The font of the status view's status label. This defaults to the `primaryFont` on the theme. */
@property (nonatomic, retain) UIFont *statusViewFont;

/** The color of the status view's status label text. This defaults to the `primaryTextColor` on the theme. */
@property (nonatomic, retain) UIColor *statusViewTextColor;

/** The fill color of the status view's arrow. This defaults to the `inactiveTintColor` on the theme.
 */
@property (nonatomic, retain) UIColor *arrowColor;

/** The border color of the status view's arrow. This defaults to the `inactiveTintColor` on the theme.
*/
@property (nonatomic, retain) UIColor *arrowBorderColor;

/** The border width of the status view's arrow.  This defaults to `0`.
 */
@property (nonatomic, retain) NSNumber *arrowBorderWidth;

@end
