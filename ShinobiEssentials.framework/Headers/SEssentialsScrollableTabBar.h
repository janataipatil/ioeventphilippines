//
//  SEssentialsScrollableTabBar.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SEssentialsFlowLayout.h"
#import "SEssentialsTabBarProtocol.h"
#import "SEssentialsTab.h" 

/**
 * An implementation of `SEssentialsTabBarProtocol` that scrolls left and right to hold more tabs that can be fit on screen at once.
 */
@interface SEssentialsScrollableTabBar : UIScrollView<SEssentialsTabBarProtocol,UIScrollViewDelegate,SEssentialsFlowLayoutDelegate>

/**
 * This implementation of `SEssentialsTabBarProtocol` uses the flow layout control to render the tabs and manage the positioning of them.
 * The layout control is embedded in a scroll view to allow it to pan through the tabs and the ordering and reordering
 * of the tabs is managed by the flow control.
 */
@property (retain,nonatomic) SEssentialsFlowLayout *tabLayout;

/**
 * Reference to the containing `SEssentialsTabbedView`. This is set automatically when the `SEssentialsScrollableTabBar` is added to the
 * `SEssentialsTabbedView`.
 */
@property (assign,nonatomic) SEssentialsTabbedView *parentTabbedView;

@end
