//
//  SEssentialsTab.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsTabHeaderView.h"
#import "SEssentialsTabbedView.h"
#import "SEssentialsTabbedViewStyle.h"

/**
 `SEssentialsTab` holds information about a single tab within `SEssentialsTabbedView`.
 
 It contains a header view, which is an instance of the `SEssentialsTabHeaderViewProtocol`.  The header view controls the appearance of the tab in the tabbed view.  The default implementation of the header view is `SEssentialsTabHeaderView`.  If you create a tab and just specify its name and icon in the init method, then the default implementation of the header view is used.  You can optionally specify your own header view if you require custom appearance or behavior for your tab.
 
 @warning Since `SEssentialsTab` does not adopt the `NSCopying` protocol it cannot be used as
 a key in an `NSDictionary`. Therefore if you wish to have a dictionary of { tabs => content } then you should use `NSValue` with a pointer to the tab as the key: [NSValue valueWithNonretainedObject: tab]
 
 **Note:** if the tab is released, the `NSValue` in the dictionary will become a
 junk pointer.
 */
@interface SEssentialsTab : NSObject

/**
 * The name is displayed in the tab, its does not need to be unique
 */
@property (retain, nonatomic) NSString *name;

/**
 * A `UIImage` displayed in the tab. The icon is scaled to fit and is displayed in the left side of the tab.
 */
@property (retain, nonatomic) UIImage *icon;

/**
 * A reference to the containing `SEssentialsTabbedView`, this is required to access style objects
 * this is set automatically when the `SEssentialsTab` is added to the `SEssentialsTabbedView`.
 */
@property (assign, nonatomic) SEssentialsTabbedView *parentTabbedView;

/**
 * The view that represents this `SEssentialsTab` in the tab bar.
 */
@property (retain,   nonatomic) UIView<SEssentialsTabHeaderViewProtocol> *tabHeaderView;

/**
 * Setting this property indicates if the tab can be removed
 */
@property (assign,   nonatomic) BOOL removable;

/**
 * The tag property is provided as a convenience when using `SEssentialsTab` references is not appropriate to the application.
 * e.g. in the dataSource implementation.
 */
@property (assign,   nonatomic) NSInteger tag;

/** @name Initializing an SEssentialsTab */

/**
  A `SEssentialsTab` is typically created like this with the default `SEssentialsTabHeaderView` being used for display.
 
  @param name the name displayed in the tab header
  @param icon the icon displayed in the tab header
 */
- (id) initWithName:(NSString *)name icon:(UIImage *)icon;

/**
  Use this initialization method to assign a custom `SEssentialsTabHeaderView` class that implements `SEssentialsTabbedHeaderViewProtocol`.
 
  @param name the name displayed in the tab header
  @param icon the icon displayed in the tab header
  @param customTabView the class that implements `SEssenialsTabHeaderViewProtocol`
 */
- (id) initWithName:(NSString *)name icon:(UIImage *)icon customTabView:(UIView<SEssentialsTabHeaderViewProtocol>*) customTabView;

@end
