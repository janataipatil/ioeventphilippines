//
//  SEssentialsTabHeaderView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SEssentialsTabHeaderViewProtocol.h"

@class SEssentialsTab;
@class SEssentialsTabbedView;
@class SEssentialsTabbedViewStyle;

/**
 `SEssentialsTabHeaderView` is the default implementation of header views for tabs in the `SEssentialsTabbedView` control.  Each instance of a tab header view is associated with a tab.
 
 When a tab header view is tapped, it tells the `SEssentialsTabbedView` which contains it to activate the tab with which it is associated.
 
 The `SEssentialsTabHeaderView` takes its tint color from the `SEssentialsTabbedViewStyle` associated with the tabbed view.  When the style updates, the tab header view updates to match.
 */
@interface SEssentialsTabHeaderView : UIView<UIGestureRecognizerDelegate,SEssentialsTabHeaderViewProtocol>

/**
 * The label renders the tab name, use this to customize the label. The font and font color used for the label are obtained
 * from `SEssentialsTabbedViewStyle`.
 */
@property (assign, nonatomic) UILabel *label;

/**
 * The `iconImageView` contains the icon, use this to customize the presentation of the icon
 */
@property (retain, nonatomic) UIImageView *iconImageView;

/**
 Initializes and returns a newly allocated tab header view object.
 
 @param tabbedViewStyle The style of the tabbed view control which will contain this view
 @param resizeToText    Whether the header view will resize to show the text it contains.  This defaults to `YES`.
 */
-(id) initWithStyle: (SEssentialsTabbedViewStyle*)tabbedViewStyle resizeToText: (BOOL)resizeToText;

@end
