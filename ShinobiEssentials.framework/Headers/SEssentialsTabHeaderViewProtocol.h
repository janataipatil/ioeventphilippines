//
//  SEssentialsTabHeaderViewProtocol.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsTab;

/**
 The header view for a `SEssentialsTab` must adopt this protocol.
 
 In addition to this protocol the implementation should also handle 'touch' actions that should call `activateTab` on the `SEssentialsTabbedView` and also handle touches on the 'delete' section of the tab that should call `removeTab`.
 */
@protocol SEssentialsTabHeaderViewProtocol <NSObject>

/**
 * Reference to the `SEssentialsTab` that holds the reference to this view
 */
@property (assign, nonatomic) SEssentialsTab *tab;

/**
 * The header view must render in two states active/inactive. When set this should
 * update the rendered view to reflect the state in the tab area.
 */
@property (nonatomic,assign) BOOL active;

/**
 Defines whether the tab header view will display some kind of control to allow the user to remove the tab.  The default implementation, `SEssentialsTabHeaderView`, has a 'remove' button that can be shown or hidden by setting this property.
 */
@property (nonatomic,assign) BOOL removable;

/**
 * When a property has changed on the `SEssentialsTab`, the header view is updated via this method.
 */
-(void) updateView;

/**
 * Get an image which represents a placeholder for this tab when it is being dragged.  This will commonly be a translucent snapshot of the tab header view.
 */
-(UIImage*) destinationMarker;

@end
