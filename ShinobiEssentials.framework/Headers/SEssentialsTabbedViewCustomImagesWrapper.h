//
//  SEssentialsTabbedViewImagesWrapper.h
//  ShinobiEssentials
//
//  Created by Thomas Kelly on 22/08/2013.
//
//

#import <Foundation/Foundation.h>

/** 
 You should use the `SEssentialsTabbedViewCustomImagesWrapper` to customize the images used in a `SEssentialsTabbedViewStyle`. Set the images on the wrapper which you wish to customize. When you create a style object with this wrapper, it will use these custom images. For any images which aren't set, the style will just use its default images.
*/
@interface SEssentialsTabbedViewCustomImagesWrapper : NSObject

#pragma mark - Tab images
/** @name Tab images */

/**
 * Graphics for the active tab item button
 */
@property (nonatomic, retain) UIImage *activeImage;

/**
 * Graphics for the inactive tab item button
 */
@property (nonatomic, retain) UIImage *inactiveImage;

/**
 * The mask image to use for active tabs
 */
@property (nonatomic, retain) UIImage *activeTabMask;

/**
 The mask image to use for inactive tabs
 */
@property (nonatomic, retain) UIImage *inactiveTabMask;

/**
 * Graphics for the close tab icon
 */
@property (nonatomic, retain) UIImage *closeTabImage;

/**
 * Graphics for the close tab icon pressed
 */
@property (nonatomic, retain) UIImage *closeTabPressedImage;

#pragma mark - Button images
/** @name Button images */

/**
 * Graphics for the 'new tab' button
 */
@property (nonatomic, retain) UIImage *addTabImage;

/**
 * Graphics for the 'new tab' button when pressed
 */
@property (nonatomic, retain) UIImage *addTabPressedImage;

/**
 * Graphics for the 'new tab' button mask
 */
@property (nonatomic, retain) UIImage *addTabMask;

/**
 * Graphics for the 'overflow dropdown' button
 */
@property (nonatomic, retain) UIImage *overflowTabsImage;

/**
 * Graphics for the 'overflow dropdown' button when pressed
 */
@property (nonatomic, retain) UIImage *overflowTabsPressedImage;

/**
 * Graphics for the 'overflow dropdown' button mask
 */
@property (nonatomic, retain) UIImage *overflowTabsMask;

/**
 * Graphics for the background mask of the 'new tab' and 'overflow dropdown' buttons
 */
@property (nonatomic, retain) UIImage *buttonsMask;

#pragma mark - Background images
/** @name Background images */

/**
 * Graphics for the background of the tab bar
 */
@property (nonatomic, retain) UIImage *tabBackgroundImage;

/**
 * Graphics for the mask of the background of the tab bar
 */
@property (nonatomic, retain) UIImage *tabBackgroundMask;

/**
 * Graphics for the edge of the tab bar under the buttons
 */
@property (nonatomic, retain) UIImage *buttonsEndImage;

/**
 * Graphics for the mask of the edge of the tab bar under the buttons
 */
@property (nonatomic, retain) UIImage *buttonsEndImageMask;

@end
