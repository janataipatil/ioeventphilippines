//
//  SEssentialsTabbedViewDelegate.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsTabbedView;
@class SEssentialsTab;

/**
 * These callbacks allow for notification and control of events occurring in the tabbed view. When the user adds, removes, activates or moves a tab
 * a callback is generated.
 */
@protocol SEssentialsTabbedViewDelegate <NSObject>

@optional

/**
 * This is called before a tab becomes active to determine whether or not the tab should be made active
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that should become active
 * @return a boolean value indicating if the tab should become active
 */
-(BOOL) tabbedView:(SEssentialsTabbedView *)tabbedView tabShouldBecomeActive:(SEssentialsTab *)tab;

/**
 * This is called before a tab becomes active
 * 
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that will become active
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView tabWillBecomeActive:(SEssentialsTab *)tab;

/**
 * This is called after a tab becomes active
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that did become active
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView tabDidBecomeActive:(SEssentialsTab *)tab;

/**
 * This is called before a tab becomes inactive to determine whether or not the tab should become inactive
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that should become inactive
 * @return a boolean value indicating if the tab should become inactive
 */
-(BOOL) tabbedView:(SEssentialsTabbedView *)tabbedView tabShouldBecomeInactive:(SEssentialsTab *)tab;

/**
 * This is called before a tab becomes inactive
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that will become inactive
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView tabWillBecomeInactive:(SEssentialsTab *)tab;

/**
 * This is called after a tab becomes inactive
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that did become inactive
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView tabDidBecomeInactive:(SEssentialsTab *)tab;

/**
 * This is called before a tab is added to determine whether or not the tab should be added
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that should be added
 * @return a boolean value indicating if the tab should be added
 */
-(BOOL) tabbedView:(SEssentialsTabbedView *)tabbedView shouldAddTab:(SEssentialsTab *)tab;

/**
 * This is called before a tab is added
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that will be added
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView willAddTab:(SEssentialsTab *)tab;

/**
 * This is called after a tab is added
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that was added
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView didAddTab:(SEssentialsTab *)tab;

/**
 * This is called before a tab is removed to determine whether or not the tab should be removed
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that should be removed
 * @return a boolean value indicating if the tab should be removed
 */
-(BOOL) tabbedView:(SEssentialsTabbedView *)tabbedView shouldRemoveTab:(SEssentialsTab *)tab;

/**
 * This is called before a tab is removed
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that will be removed
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView willRemoveTab:(SEssentialsTab *)tab;

/**
 * This is called after a tab is removed
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that was removed
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView didRemoveTab:(SEssentialsTab *)tab;

/**
 * This is called before a tab is moved to determine whether or not the tab should be moved
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that should be moved
 * @return a boolean value indicating if the tab should be moved
 */
-(BOOL) tabbedView:(SEssentialsTabbedView *)tabbedView shouldMoveTab:(SEssentialsTab *)tab;

/**
 * This is called before a tab is moved to a new position
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that will be moved
 * @param fromIndex the index from which the tab will be moved
 * @param toIndex the index to which the tab will be moved
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView willMoveTab:(SEssentialsTab *)tab fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;

/**
 * This is called after a tab is moved
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that was moved
 * @param fromIndex the index from which the tab was moved
 * @param toIndex the index to which the tab was moved
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView didMoveTab:(SEssentialsTab *)tab fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;

@end
