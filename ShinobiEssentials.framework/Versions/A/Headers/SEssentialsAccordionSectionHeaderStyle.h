//
//  SEssentialsAccordionSectionHeaderStyle.h
//  Accordion
//
//  Copyright (c) 2012 Scott Logic Ltd. All rights reserved.
//

#import "SEssentialsStyle.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 The `SEssentialsAccordionSectionHeaderStyle` defines the look and feel for an instance of the `SEssentialsAccordionSectionHeader` class.  If you wish to configure how an accordion section header looks, you should set the relevant properties on this class.
 
 `SEssentialsAccordionSectionHeaderStyle` derives from `SEssentialsStyle`, and so it is initialized using a `SEssentialsTheme` object. The theme sets the default properties of the style.
 
 The following settings are set by default from the theme:
 
 - The `primaryTintColor` on the theme is used for the background color of the header (`backgroundColor`).
 - The `secondaryTintColor` on the theme is used for the section background color (`sectionBackgroundColor`) and the selected header color (`selectedBackgroundColor`).
 
 - The `primaryTextColor` on the theme is used for the font color in the header (`fontColor`).
 
 - The `primaryDecorationTintColor` on the theme is used for the color of the chisel at the top of the header (`chiselPrimaryColor`).
 - The `secondaryDecorationTintColor` on the theme is used for the color of the chisel at the bottom of the header (`chiselSecondaryColor`).
 - The `shineColor` on the theme is used for the shine color (`shineColor`).
 - The `shadowColor` and `shadowDepth` properties on the theme set the matching properties on the style (`shadowColor` and `shadowDepth`).
 */
@interface SEssentialsAccordionSectionHeaderStyle : SEssentialsStyle

#pragma mark - Background style
/** @name Background style */

/** The rounding at the corners on each section. The default is 0. */
@property (assign, nonatomic) CGFloat sectionCornerRadius;

/**
 The background color of the section.  This defaults to the `secondaryTintColor` on the theme.
 */
@property (retain, nonatomic)   UIColor     *sectionBackgroundColor;

/**
 The background color of the section header.  This defaults to the `primaryTintColor` on the theme.
 */
@property (retain, nonatomic)   UIColor     *backgroundColor;

/**
 The background color of a selected section header.  This defaults to the `secondaryTintColor` on the theme.
 */
@property (retain, nonatomic)   UIColor     *selectedBackgroundColor;

/**
 The background color of an pressed section header. This defaults to the `activeTintColor` on the theme.
 */
@property (retain, nonatomic)   UIColor     *pressedBackgroundColor;

/**
 The texture which is applied to the section header.  This defaults to a clear color.
 */
@property (retain, nonatomic)   UIColor     *texture;

#pragma mark - Text style
/** @name Text style */

/** The font of the `UILabel` which displays the title in the section header.  This defaults to the `primaryFont` on the theme. */
@property (retain, nonatomic)   UIFont      *font;

/**
 The color of title in the section header.  This defaults to the `primaryTextColor` on the theme.
 */
@property (retain, nonatomic)   UIColor     *fontColor;

#pragma mark - Decoration
/** @name Decoration */

/** DEPRECATED: This property is no longer used.  If you would like to configure the decoration on the section header, you should use the properties on the style which configure the particular setting you need, such as `shineColor`, `chiselPrimaryColor`, `chiselSecondaryColor` etc. */
@property (assign, nonatomic) BOOL showDecoration DEPRECATED_ATTRIBUTE;

/** The color of the shine layer over the header.  Defaults to the `shineColor` of the theme. */
@property (nonatomic, retain) UIColor *shineColor;

/** The top color of the chisel separating the header from the content.  Defaults to the `primaryDecorationTintColor` of the theme. */
@property (nonatomic, retain) UIColor *chiselPrimaryColor;

/** The bottom color of the chisel separating the header from the content. Defaults to the `secondaryDecorationTintColor` of the theme. */
@property (nonatomic, retain) UIColor *chiselSecondaryColor;

/** The color of the drop shadow under the section.  Defaults to the `shadowColor` of the theme. */
@property (nonatomic, retain) UIColor *shadowColor;

/** The depth of the drop shadow under the section.  Defaults to the `shadowDepth` of the theme. */
@property (nonatomic, assign) CGFloat shadowDepth;

@end
