//
//  ShinobiCurve.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SEssentialsAnimationCurveType) {
    SEssentialsAnimationCurveTypeLinear,
    SEssentialsAnimationCurveTypeEaseIn,
    SEssentialsAnimationCurveTypeEaseOut,
    SEssentialsAnimationCurveTypeEaseInOut,
    SEssentialsAnimationCurveTypeBounce,
    SEssentialsAnimationCurveTypeWobbleLight,
    SEssentialsAnimationCurveTypeWobbleHeavy
};

/**
 * A class that provides a variety of curves for use with `SEssentialCarousel`.
 */
@interface SEssentialsAnimationCurve : NSObject

/**
 * Creates an animation curve from the enum
 
    typedef enum {
       SEssentialsAnimationCurveTypeLinear,
       SEssentialsAnimationCurveTypeEaseIn,
       SEssentialsAnimationCurveTypeEaseOut,
       SEssentialsAnimationCurveTypeEaseInOut,
       SEssentialsAnimationCurveTypeBounce,
       SEssentialsAnimationCurveTypeWobbleLight,
       SEssentialsAnimationCurveTypeWobbleHeavy
    } SEssentialsAnimationCurveType;
 
*/
+(id)curveForCurveType:(SEssentialsAnimationCurveType)curveType;

/**
 All curves implement this method
 
 @param percentTime The proportion of the duration of the curve completed.
 @return Value of the curve at specified time. Must be `0` at time `0` and `1` at
 time `1`.
 */
-(CGFloat)valueAtPercentTime:(CGFloat)percentTime;

@end
