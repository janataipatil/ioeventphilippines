//
//  SEssentialsCarouselCylindrical.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsCarousel.h"

/**
 An `SEssentialsCarouselCylindrical` is an `SEssentialsCarousel` which displays all items on the surface of a cylinder.
 
 <img src="../docs/Docs/img/Cylindrical.png" />
 
 The carousel items are evenly spaced on the surface of the cylinder along an elliptical path. The width and depth of the elliptical path can be configured along with the spacing between items and tilt of the cylinder. Rotation, fading and clipping can also be applied to items.
 
 The width and depth of the cylinder are controlled by the `widthRatio` and `depthRatio` properties. The diagram below shows a top down view of the carousel and how these properties relate to the carousels size.
 
 <img src="../docs/Docs/img/CylindricalSizing.png" />
 
 The `SEssentialsCarouselCylindrical` inherits from the `SEssentialsCarousel` base class. The methods and properties on the base class can be used to control all other aspects of the carousels behavior.
 
  @sample CarouselHowToDoubleSidedViews
 */
@interface SEssentialsCarouselCylindrical : SEssentialsCarousel

/** If `SEssentialsCarouselOrientationHorizontal`, items are laid out left-to-right. If `SEssentialsCarouselOrientationVertical`, items are laid out top-to-bottom. Defaults to `SEssentialsCarouselOrientationHorizontal`.
 
    typedef enum
    {
        SEssentialsCarouselOrientationHorizontal = 0,
        SEssentialsCarouselOrientationVertical
    } SEssentialsCarouselOrientation;
 
 */
@property (nonatomic, assign) SEssentialsCarouselOrientation orientation;

/** This controls the width of the cylinder on which the items are placed and is a positive number. This value is the ratio of carousel width to frame width (or carousel height to frame height if `orientation` is `SEssentialsCarouselOrientationVertical`). Defaults to `1.0`
 
 The `SEssentialsCarouselCylindrical` displays items on an elliptical path. The `widthRatio` defines the distance from the center of the carousel to the left and right (or top and bottom if `orientation` is set to `SEssentialsCarouselOrientationVertical`) edges of the carousel. To control the depth of the carousel use `depthRatio`.
 
 * A value of `1.0` draws the cylinder such that the edges of the cylinder touch the carousels frame
 * A value of less that `1.0` would make the edges of the cylinder lie inside the carousels frame
 * A value greater than `1.0` would make the edges of the cylinder lie outside the carousels frame */
@property (nonatomic, assign) CGFloat widthRatio;

/** This controls the depth of the cylinder on which the items are placed and is a positive number. This allows control over the scaling of items as they move towards the back of the carousel. This value is the ratio of depth to the frame width (or height if `orientation` is `SEssentialsCarouselOrientationVertical`). Defaults to `1.0`
 
 The `SEssentialsCarouselCylindrical` displays items on an elliptical path. The `depthRatio` defines the distance from the center to the front and back of the carousel. To control the width of the carousel use `widthRatio`.
 
 * A value of `1.0` draws the cylinder such that the total depth is equal to the carousels frame size
 * A value of less that `1.0` draws the cylinder such that the total depth is less than the carousels frame size
 * A value greater than `1.0` draws the cylinder such that the total depth is greater than the carousels frame size */
@property (nonatomic, assign) CGFloat depthRatio;

/** This controls the spacing between the items in the carousel and is a positive number. Defaults to `1.1`
 
 * A value of `1.0` would result in items being arranged so that they are touching side by side
 * A value of less that `1.0` would cause the items to overlap
 * A value greater than `1.0` would spread the items out with spaces between them */
@property (nonatomic, assign) CGFloat itemSpacingFactor;

/** This controls the point where items in the carousel reach an alpha value equal to `fadeAlpha`. It is a positive number between `0.0` and `1.0`. Defaults to `0.9` 
 
 * A value of `1.0` would result in items at the back of the carousel having an alpha value of `fadeAlpha`
 * A value of `0.5` would result in all items from the back of the carousel to half through the carousel having an alpha value of `fadeAlpha`
 * A value of `0.0` would result in all items having an alpha value of `fadeAlpha`
 */
@property (nonatomic, assign) CGFloat fadeDepth;

/** Controls the minimum alpha value to apply to items in the carousel at the point specified by `fadeDepth`. It is a positive number between `0.0` and `1.0`. Defaults to `0.0` */
@property (nonatomic, assign) CGFloat fadeAlpha;

/** Controls whether the items remain forwards facing as they travel around the cylinder. Defaults to `NO`.
 
 * If `YES`, the items will not rotate as they move away from the focus point
 * If `NO`, the items will rotate to lie tangential to the surface of the cylinder as they move away from the focus point
 */
@property (nonatomic, assign) BOOL frontFacing;

/** This can be used to give the impression the camera is above or below the level of the items in the carousel. The value represents the gradient of the items as they move further from the focus point. Defaults to `0.5`
 
 * A value of `0.0` will result in the items being arranged in a straight line
 * A value of `1.0` will result in the items moving at 45 degrees upwards as they move further from the focus point
 * A value of `-1.0` will result in the items moving at 45 degrees downwards as they move further from the focus point  */
@property (nonatomic, assign) CGFloat tiltFactor;

/** Adjust the item spacing to fit exactly the number of visible views around the circle.
 
 This means that as one item disappears from view the next item appears in the same place on the circle and gives the
 illusion of items rotating seamlessly around.
 */
-(void)adjustSpacingToFitVisibleItemsExactly;

@end
