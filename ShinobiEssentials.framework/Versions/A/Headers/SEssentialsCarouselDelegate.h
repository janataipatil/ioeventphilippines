//
//  SEssentialsCarouselDelegate.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsCarousel;

/**
 The `SEssentialsCarouselDelegate` defines methods which allow you to manage the appearance and modification of items, and the UI interactions with the `SEssentialsCarousel`. It offers control over and notifications regarding;
 
 - Items appearing or disappearing
 - Modifications to items
 - Scrolling of the Carousel
 
 The `offset` argument is the distance from the fixed focus point of the carousel. As the carousel pans through the items, the offset changes while the fixed point remains constant. For example, when the carousel first displays, the fixed point is at index 0, so the offset of each item is the same as their index;
 
 <img src="../docs/Docs/img/Offset0.png" />
 
 As the carousel begins panning through the items, the items move past the fixed point, and their offsets change. For example, in the example below, the fixed point maps to part way between index 0 and index 1, and the offset of all the items shifts accordingly;
 
 <img src="../docs/Docs/img/Offset06.png" />
 
 The `offset` argument is used primarily to calculate a given position and transform for an item, based on it's distance from the focus point of the carousel, as well as informing the `delegate` of a layout of an item at a given offset, to allow for additional effects to be applied, such as fading the alpha based on distance from the focus of the carousel.
 */
@protocol SEssentialsCarouselDelegate <NSObject>

@optional

/** @name Item layout */

/** Informs the delegate that an item will be moved, and allows for any additional modifications (E.g. alpha level, background color or zPosition)
 @param carousel The parent carousel
 @param item The target item to be modified
 @param offset The distance of the target item from the center of the carousel
 */
-(void)carousel:(SEssentialsCarousel*)carousel willLayoutItem:(UIView*)item atOffset:(CGFloat)offset;
/** Informs the delegate that an item has been moved, and allows for any additional modifications (E.g. alpha level, background color or zPosition)
 @param carousel The parent carousel
 @param item The target item to be modified
 @param offset The distance of the target item from the center of the carousel
 */
-(void)carousel:(SEssentialsCarousel*)carousel didLayoutItem:(UIView*)item atOffset:(CGFloat)offset;

/** @name Carousel scrolling */

/** Informs the delegate that the carousel is about to begin scrolling.
 
 This callback is only made on receiving a gesture. This will not be called if scrolling programmatically.
 @param carousel The parent carousel
 */
-(void)carouselWillScroll:(SEssentialsCarousel*)carousel;
/** Informs the delegate that the carousel is part way through scrolling.
 
 The `contentOffset` of the carousel will be updated before this callback is made, and can be queried for the current offset.
 This callback will not trigger when programmatically moving without animation.

 @param carousel The parent carousel
 */
-(void)carouselIsScrolling:(SEssentialsCarousel*)carousel;
/** Informs the delegate that the carousel has begun decelerating under friction.
 
 This callback is only after a gesture completes. This will not be called if scrolling programmatically.
 @param carousel The parent carousel
 */
-(void)carouselWillDecelerate:(SEssentialsCarousel*)carousel;
/** Informs the delegate that the carousel has scrolled to a new offset.
 
 This callback will be called both for gestures and programmatic scrolling, with or without animation.
 @param carousel The parent carousel
 @param offset The new offset the carousel has scrolled to
 */
-(void)carousel:(SEssentialsCarousel*)carousel didFinishScrollingAtOffset:(CGFloat)offset;

/** @name Virtualized items */

/** Informs the delegate that an item will appear on screen
 @param carousel The parent carousel
 @param item The target item to be shown
 @param offset The distance of the target item from the center of the carousel
 */
-(void)carousel:(SEssentialsCarousel*)carousel willDisplayItem:(UIView*)item atOffset:(CGFloat)offset;
/** Informs the delegate that an item has appeared on screen
 @param carousel The parent carousel
 @param item The target item to be shown
 @param offset The distance of the target item from the center of the carousel
 */
-(void)carousel:(SEssentialsCarousel*)carousel didDisplayItem:(UIView*)item atOffset:(CGFloat)offset;

/** Informs the delegate that an item will disappear, usually because it has scrolled off screen
 @param carousel The parent carousel
 @param item The target item to be hidden
 @param offset The distance of the target item from the center of the carousel
 */
-(void)carousel:(SEssentialsCarousel*)carousel willEndDisplayingItem:(UIView*)item atOffset:(CGFloat)offset;
/** Informs the delegate that an item disappeared, usually because it has scrolled off screen
 @param carousel The parent carousel
 @param item The target item to be hidden
 @param offset The distance of the target item from the center of the carousel
 */
-(void)carousel:(SEssentialsCarousel*)carousel didEndDisplayingItem:(UIView*)item atOffset:(CGFloat)offset;

/** @name Tap Gesture callbacks */
/** Informs the delegate that an item has been tapped
 @param carousel The parent carousel
 @param item The item that has been tapped
 @param offset The offset of the tapped item
 */
-(void)carousel:(SEssentialsCarousel*)carousel didTapItem:(UIView*)item atOffset:(CGFloat)offset;

@end
