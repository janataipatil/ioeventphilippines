//
//  SEssentialsCarouselInverseCylindrical.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2013. All rights reserved.
//
//

#import "SEssentialsCarousel.h"
/**
 An `SEssentialsCarouselInverseCylindrical` is an `SEssentialsCarousel` which displays all items on the surface of a cylinder. The items are viewed from the center of the carousel.
 
 <img src="../docs/Docs/img/InverseCylindrical.png" />
 
 The carousel items are evenly spaced on the circumference of the cylinder around the viewpoint. The cylinder radius and spacing of items can be configured. Once an item is rotated more than 90 degrees it is not displayed in the carousel.
 
 The majority of interactions with the `SEssentialsCarouselInverseCylindrical` will be done via the methods and properties on the `SEssentialsCarousel` abstract base class, with the properties of the `SEssentialsCarouselInverseCylindrical` class making small changes to the positioning of items.
 */
@interface SEssentialsCarouselInverseCylindrical : SEssentialsCarousel

/** If `SEssentialsCarouselOrientationHorizontal`, items are laid out left-to-right. If `SEssentialsCarouselOrientationVertical`, items are laid out top-to-bottom. Defaults to `SEssentialsCarouselOrientationHorizontal`.
 
    typedef enum
    {
        SEssentialsCarouselOrientationHorizontal = 0,
        SEssentialsCarouselOrientationVertical
    } SEssentialsCarouselOrientation;
 
 */
@property (nonatomic, assign) SEssentialsCarouselOrientation orientation;

/** This controls the spacing between the items in the carousel and is a positive number. Defaults to `1.0`
 
 * A value of `1.0` would result in items being arranged so that they are touching side by side
 * A value of less that `1.0` would cause the items to overlap
 * A value greater than `1.0` would spread the items out with spaces between them */
@property (nonatomic, assign) CGFloat itemSpacingFactor;

/** This controls the radius of the cylinder on which the items are placed and is a positive number. This value is multiplied by half the frame width or height to give the radius of the cylinder. Defaults to `1.0`
 
 * A value of `1.0` draws the cylinder such that the edges of the cylinder touch the carousels frame
 * A value of less that `1.0` would make the edges of the cylinder lie inside the carousels frame
 * A value greater than `1.0` would make the edges of the cylinder lie outside the carousels frame */
@property (nonatomic, assign) CGFloat radius;

/** This controls the point where items in the carousel reach an alpha value equal to `fadeAlpha`. It is a positive number between `0.0` and `1.0`. Defaults to `0.75`
 
 * A value of `1.0` would result in items at the front of the carousel having an alpha value of `fadeAlpha`
 * A value of `0.5` would result in all items from the front of the carousel to half through the carousel having an alpha value of `fadeAlpha`
 * A value of `0.0` would result in all items having an alpha value of `fadeAlpha`
 
 Note that the front of the carousel is defined as the side closest to the screen.
 */
@property (nonatomic, assign) CGFloat fadeDepth;

/** Controls the minimum alpha value to apply to items in the carousel at the point specified by `fadeDepth`. It is a positive number between `0.0` and `1.0`. Defaults to `0.0` */
@property (nonatomic, assign) CGFloat fadeAlpha;

/** This can be used to give the impression the camera is above or below the level of the items in the carousel. The value represents the gradient of the items as they move further from the focus point. Defaults to `0.0`
 
 * A value of `0.0` will result in the items being arranged in a straight line
 * A value of `1.0` will result in the items moving at 45 degrees downwards as they move further from the focus point
 * A value of `-1.0` will result in the items moving at 45 degrees upwards as they move further from the focus point */
@property (nonatomic, assign) CGFloat tiltFactor;

@end
