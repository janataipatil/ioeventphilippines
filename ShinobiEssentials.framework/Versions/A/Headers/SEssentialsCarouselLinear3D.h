//
//  SEssentialsCarouselLinear3D.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsCarousel.h"
/**
 An `SEssentialsCarouselLinear3D` is an `SEssentialsCarousel` which displays all items in a line with perspective scaling as the items move further from the focus point. The alignment and spacing of items can be configured.
 
 <img src="../docs/Docs/img/Linear3D.png" />
 
 The majority of interactions with the `SEssentialsCarouselLinear3D` will be done via the methods and properties on the `SEssentialsCarousel` abstract base class, with the properties of the `SEssentialsCarouselLinear3D` making small changes to the positioning of items.
 */
@interface SEssentialsCarouselLinear3D : SEssentialsCarousel

/** If `SEssentialsCarouselOrientationHorizontal`, items are laid out left-to-right. If `SEssentialsCarouselOrientationVertical`, items are laid out top-to-bottom. Defaults to `SEssentialsCarouselOrientationHorizontal`. 
 
    typedef enum
    {
      SEssentialsCarouselOrientationHorizontal = 0,
      SEssentialsCarouselOrientationVertical
    } SEssentialsCarouselOrientation;
 
 */
@property (nonatomic, assign) SEssentialsCarouselOrientation orientation;

/** This controls how much the items shrink or grow as they move away from the focus point and can be used to control the illusion of perspective. It is a positive number representing the scale relative to the original item size that the item will be as it moves one index further away from the focus point. Defaults to `0.8` */
@property (nonatomic, assign) CGFloat scaleFactor;

/** This can be used to give the impression the camera is above or below the level of the items in the carousel. The value represents the gradient of the items as they move further from the focus point. Defaults to `0.05`
 
 * A value of `0` will result in the items arranged in a straight line
 * A value of `1.0` will result in the items moving at 45 degrees upwards as they move further from the focus
 * A value of `-1.0` will result in the items moving at 45 degrees downwards.  */
@property (nonatomic, assign) CGFloat tiltFactor;

/** This controls the spacing between the items in the carousel and is a positive number. Defaults to `0.9`
 
 * A value of `1.0` would result in items being arranged so that they are touching side by side
 * A value of less that `1` would cause the items to overlap 
 * A value greater than `1` would spread the items out with spaces between them. */
@property (nonatomic, assign) CGFloat itemSpacingFactor;



@end
