//
//  SEssentialsCarouselRadial.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2013. All rights reserved.
//
//

#import "SEssentialsCarousel.h"
/**
 An `SEssentialsCarouselRadial` is an `SEssentialsCarousel` which displays all items in a circular layout.
 
 <img src="../docs/Docs/img/Radial.png" />

 The carousel items are evenly spaced on the circumference of the circle, arranged like a dial. The radius and spacing of items can be configured as well as the orientation of the items as they move around the circle.
 
 There are many more methods and properties on the `SEssentialsCarousel` base class to control all other aspects of the carousels behavior.
 */
@interface SEssentialsCarouselRadial : SEssentialsCarousel


/** This controls the spacing between the items in the carousel and is a positive number. Defaults to `1.2`
 
 * A value of `1.0` would result in items being arranged so that they are touching side by side around the circle.
 * A value of less that `1.0` would cause the items to overlap.
 * A value greater than `1.0` would spread the items out with spaces between them. */
@property (nonatomic, assign) CGFloat itemSpacingFactor;

/** This controls the radius of the circle on which the items are placed and is a ratio relative to the width of the frame. Defaults to `2`. */
@property (nonatomic, assign) CGFloat radius;

/** Specified in radians this controls at which angle the focus point lies. Defaults to `0`. Since the focus point (see `focusPointNormalized`) defines where the item in focus will be on screen the focus point angle will control how the circle is rendered relative to that point.

 * A value of `0` would result in the focus point being at the top of the circle
 * A value of `1.0 * M_PI_2` would move the focus point to the right of the circle
 * A value of `2.0 * M_PI_2` would move the focus point to the bottom of the circle
 * A value of `3.0 * M_PI_2` would move the focus point to the left of the circle
*/
@property (nonatomic, assign) CGFloat focusPointAngle;

/**
 This controls whether items rotate as they move around the circle. Defaults to `NO`.

 * A value of `YES` would result in each item rotating as it moves around the circle.
 * A value of `NO` means that all items remain upright as they move around the circle.
 */
@property (nonatomic, assign) BOOL itemsRotate;

/** This controls how much the items shrink or grow as they move away from the focus point. Defaults to `0.8`.
 
 It is a positive number representing the scale relative to the original item size that the item will be as it moves one index further away from the focus point. To remove all scaling effects set this to `1.0`. If you only wish the item in focus to appear larger than the other items see the `scaleAllItems` property. */
@property (nonatomic, assign) CGFloat scaleFactor;

/** This controls how scaling (see scaleFactor) is applied. Defaults to `YES`.
 
 * A value of `YES` means that all items not in focus are scaled equally relative to the item in focus.
 
 <img src="../docs/Docs/img/RadialNonScalingThumb.png" />

 * A value of `NO` would result in each successive item being scaled as they move further from the focus point.
 
 <img src="../docs/Docs/img/RadialScalingThumb.png" />

*/
@property (nonatomic, assign) BOOL onlyScaleItemInFocus;

/** Adjust the parameters of the carousel to center the circle at given position.
 
 A helper method that adjusts the focusPointAngle and radius to fit a circle to the given constraint. The position is supplied as a normalized coordinate where the x and y values are ratios of the width and height.
 
 @param normalizedCirclePosition The normalized position of the center of the circle path along which items move.
 @param normalizedFocusPosition The normalized position of the item in focus when the carousel is at rest.
 */
-(void)centerCircleAt:(CGPoint)normalizedCirclePosition withFocusPointAt:(CGPoint)normalizedFocusPosition;

/** Adjust the parameters of the carousel to center the circle at given position.
 
 A helper method that adjusts the focusPointNormalised position to fit a circle to the given constraint. The position is supplied as a normalized coordinate where the x and y values are ratios of the width and height.
 
 @param normalizedCirclePosition The normalized position of the center of the circle path along which items move.
 @param angle Specified in radians this controls at which angle the focus point lies.
 @param normalizedRadius The radius relative to the width of the carousel.
 */
-(void)centerCircleAt:(CGPoint)normalizedCirclePosition withFocusPointAngle:(CGFloat)angle andRadius:(CGFloat)normalizedRadius;

/** Adjust the item spacing to fit exactly the number of visible views around the circle.
 
 This means that as one item disappears from view the next item appears in the same place on the circle and gives the
 illusion of items rotating seamlessly around.
 */
-(void)adjustSpacingToFitVisibleItemsExactly;

@end
