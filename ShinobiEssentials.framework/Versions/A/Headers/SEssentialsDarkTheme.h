//
//  SEssentialsDarkTheme.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsTheme.h"

/**
 `SEssentialsDarkTheme` is a concrete implementation of an `SEssentialsTheme` with
 dark tones as defaults. This is the default global theme used in an app which
 is running in versions of iOS earlier than 7.0.
 */

@interface SEssentialsDarkTheme : SEssentialsTheme

@end
