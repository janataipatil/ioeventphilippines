//
//  SEssentialsDecoratedDoubleSidedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsDecoratedView.h"

/**
 The `SEssentialsDecoratedDoubleSidedView` creates a view that is double sided, i.e. it will display a different view when rotated away from the camera.
 
 
 If you imagine a playing card with a 'pattern' on the rear and 'suit' on the front the following code would be used.
 
    SEssentialsDecoratedDoubleSidedView *doubleView = [ [ SEssentialsDecoratedDoubleSidedView alloc ] initWithView:suit andBackView:pattern ];

 This is useful when the view is placed as an item in a 3d carousel and you do not want the contents to show through as they rotate away from the camera.
 
  @sample CarouselHowToDoubleSidedViews
 */
@interface SEssentialsDecoratedDoubleSidedView : SEssentialsDecoratedView

/** The view which is shown when the main view faces away from the camera. */
@property (nonatomic, readonly, retain) UIView *backView;

/** Shows either front or back view. Defaults to YES */
@property (nonatomic, assign) BOOL showFront;

/** Create an `SEssentialsDecoratedView` with a view and backView.
 @param view The forward facing view
 @param backView The back facing view
 */
- (id)initWithView:(UIView*)view andBackView:(UIView*)backView;


@end
