//
//  SEssentialsFlowLayoutDelegate.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@class SEssentialsFlowLayout;

/**
 * These callbacks allow for notification of events on the layout by the user and
 * also allows for a confirmation that edit events like move and delete should proceed.
 */
 
@protocol SEssentialsFlowLayoutDelegate <NSObject>

@optional

/**
 * Called to confirm if a users attempt to remove a subview should proceed. 
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that is requesting deletion
 * @return `YES` indicates deletion should proceed
 */
-(BOOL) flowLayout:(SEssentialsFlowLayout *)flow shouldRemoveView:(UIView *)view;

/**
 * Indicates a subview will be removed
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that will be removed
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow willRemoveView:(UIView *)view;

/**
 * Indicates a subview has been removed.
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that was removed
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow didRemoveView:(UIView *)view;

/**
 * Called to confirm if a users attempt to directly move a subview should proceed. 
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that is about to be moved
 * @return `YES` indicates move should proceed
 */
-(BOOL) flowLayout:(SEssentialsFlowLayout *)flow shouldMoveView:(UIView *)view;

/**
 * Indicates a subview will be moved
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that will be moved
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow willMoveView:(UIView *)view;

/**
 * Indicates a subview has been moved. To find out the new index look up the subview in `flow.orderedSubviews`
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that was moved
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow didMoveView:(UIView *)view;

/**
 * Indicates a subview is moving and makes repeated calls as the subview is dragged.
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that was moved
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow didDragView:(UIView *)view;

/**
 * Indicates that a subview has not been moved after it was dragged.
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that was not moved
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow didNotMoveView:(UIView *)view;

/**
 * If implemented allows position other than default top left for 'delete' icon on each item during edit mode
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that was moved
 * @return position relative to view origin for delete icon
 */
-(CGPoint) flowLayout:(SEssentialsFlowLayout *)flow deleteIconPositionInView:(UIView *)view;

/**
 * If implemented allows position other than that supplied by `editButtonLocation` for the editing button in the
 * flow layout
 * @param flow the `SEssentialsFlowLayout`
 * @return position relative to flow origin for done button
 */
-(CGPoint) editButtonPositionInFlowLayout:(SEssentialsFlowLayout *)flow;

/**
 * If implemented provides the offset in the subview that will be positioned under the drag point during editing.
 * @param flow the `SEssentialsFlowLayout`
 * @param point initial touch point offset
 * @param view the subview that was moved
 * @return position relative to view center that will move under the drag point
 */
-(CGPoint) flowLayout:(SEssentialsFlowLayout *)flow dragPointForTouchAtPoint:(CGPoint)point view:(UIView *)view;

/**
 * If implemented allows the destination marker image to be provided.
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that is moving
 * @return image that will be drawn at the the insertion point while editing 
 */
-(UIImage *) flowLayout:(SEssentialsFlowLayout *)flow imageForDestinationMovingView:(UIView *)view;

/**
 * When implemented must animate the view to the target position. This is called when the
 * `animationType` is set to `SEssentialsAnimationUser`
 * @param flow the `SEssentialsFlowLayout`
 * @param view the subview that is moving
 * @param target the intended center destination at the end of the animation
 */
-(void) flowLayout:(SEssentialsFlowLayout *)flow animateView:(UIView *)view toTarget:(CGPoint) target;

/**
 * Indicates layout will be put into edit mode
 * @param layout `The SEssentialsFlowLayout`
 */
-(void) willBeginEditInFlowLayout:(SEssentialsFlowLayout *)flow;

/**
 * Indicates layout is now in edit mode
 * @param flow the `SEssentialsFlowLayout`
 */
-(void) didBeginEditInFlowLayout:(SEssentialsFlowLayout *)flow;

/**
 * Indicates layout will finish editing
 * @param flow the `SEssentialsFlowLayout`
 */
-(void) willEndEditInFlowLayout:(SEssentialsFlowLayout *)flow;

/**
 * Indicates layout has finished editing
 * @param flow the `SEssentialsFlowLayout`
 */
-(void) didEndEditInFlowLayout:(SEssentialsFlowLayout *)flow;

@end
