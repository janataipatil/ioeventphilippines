//
//  SEssentialsFlowLayoutStyle.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsStyle.h"
#import "SEssentialsFlowLayoutImagesWrapper.h"

/**
 The `SEssentialsFlowLayoutStyle` defines the look and feel for an instance of the `SEssentialsFlowLayout` class.  If you wish to configure how your flow layout looks, you should modify the properties on this class.
 
 `SEssentialsFlowLayoutStyle` derives from `SEssentialsStyle`, and so it is initialized from a `SEssentialsTheme` object. The theme
 provides the default settings for the style.
 
 The following default settings are taken from the theme:
 
 - The `primaryTintColor` on the theme is used for the background color of the flow layout (`mainViewTintColor`).
 - The `primaryTexture` on the theme is used for the background texture of the flow layout (`mainViewTexture`).
*/

@interface SEssentialsFlowLayoutStyle : SEssentialsStyle

#pragma mark - Colors and textures
/** @name Colors and textures */

/**
 The texture of the view background. This defaults to the `primaryTexture` on the theme.
 */
@property (nonatomic,retain) UIColor *mainViewTexture;

/**
 The tint color of the view background. This defaults to the `primaryTintColor` on the theme.
 */
@property (nonatomic,retain) UIColor *mainViewTintColor;

/**
 The tint color of the FlowLayout trashcan icon. This defaults to the 'tertiaryTintColor' on the theme.
 */
@property (nonatomic,retain) UIColor *trashcanTintColor;

#pragma mark - Selection
/** @name Selection */

/**
 The amount to scale a view by when it is selected.
 
 A value of `1.0` will not scale the subview. The default value for this 
 property is `1.1`.
 */
@property (nonatomic,assign) CGFloat selectedScaleFactor;

#pragma mark - Initialization
/** @name Initialization */

/** DEPRECATED in ShinobiEssentials v2.1, use `initWithTheme:customImages:` instead */
- (id)    initWithTheme:(SEssentialsTheme*)theme
          trashcanImage:(UIImage*)trashcanImage
           trashcanMask:(UIImage*)trashcanMask
        doneButtonImage:(UIImage*)doneButtonImage
 doneButtonPressedImage:(UIImage*)doneButtonPressedImage
            deleteImage:(UIImage*)deleteImage
     deletePressedImage:(UIImage*)deletePressedImage DEPRECATED_ATTRIBUTE;

/**
 Initializes the style object with a set of custom icons for the flow layout. See `SEssentialsFlowLayoutImagesWrapper` for more details.
 */
-(id)initWithTheme:(SEssentialsTheme *)theme customImages:(SEssentialsFlowLayoutImagesWrapper*)customImages;

#pragma mark - Images
/** @name Images */

/**
 Trash can image
 
 This is the image which appears for flow elements to be dragged to for deletion
 when the `SEssentialsFlowLayout` is set to use the `SEssentialsFlowDeleteIdiomTrashCan`
 delete idiom.
 
 Has a default image provided by the ShinobiEssentials framework. To override,
 use the custom initialization method above.
 */
@property (nonatomic, retain, readonly) UIImage *trashcanImage;

/**
 Trash can mask image
 
 This mask defines where the trashcan will be colored with `trashcanTintColor`.
 
 Has a default image provided by the ShinobiEssentials framework. To override,
 use the custom initialization method above.
 */
@property (nonatomic, retain, readonly) UIImage *trashcanMask;

/**
 Done button image
 
 This is the image which appears to exit edit mode on a `SEssentialsFlowLayout` when
 the `SEssentialsFlowDeleteIdiomIcon` delete idiom is used. This is the image for
 the default state.
 
 Has a default image provided by the ShinobiEssentials framework. To override,
 use the custom initialization method above.
 */
@property (nonatomic, retain, readonly) UIImage *doneButtonImage;

/**
 Done button pressed image
 
 This is the image which appears to exit edit mode on a `SEssentialsFlowLayout` when
 the `SEssentialsFlowDeleteIdiomIcon` delete idiom is used. This is the image for
 the pressed state.
 
 Has a default image provided by the ShinobiEssentials framework. To override,
 use the custom initialization method above.
 */
@property (nonatomic, retain, readonly) UIImage *doneButtonPressedImage;

/**
 Delete image
 
 This is the image which appears on the corner of individual flow elements during
 edit mode when the `SEssentialsFlowLayout` is using `SEssentialsFlowDeleteIdiomIcon`
 delete idiom. This is the image for the default button state.
 
 Has a default image provided by the ShinobiEssentials framework. To override,
 use the custom initialization method above.
 */
@property (nonatomic, retain, readonly) UIImage *deleteImage;

/**
 Delete pressed image
 
 This is the image which appears on the corner of individual flow elements during
 edit mode when the `SEssentialsFlowLayout` is using `SEssentialsFlowDeleteIdiomIcon`
 delete idiom. This is the image for the pressed button state.
 
 Has a default image provided by the ShinobiEssentials framework. To override,
 use the custom initialization method above.
 */

@property (nonatomic, retain, readonly) UIImage *deletePressedImage;

@end
