//
//  SEssentialsLightTheme.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsTheme.h"

/**
 `SEssentialsLightTheme` is a concrete implementation of an `SEssentialsTheme` with
 light tones as defaults.
 
 To use `SEssentialsLightTheme` as the default global theme:
 
    [ShinobiEssentials setTheme:[[[ SEssentialsLightTheme alloc] init] autorelease]];
 */
@interface SEssentialsLightTheme : SEssentialsTheme

@end
