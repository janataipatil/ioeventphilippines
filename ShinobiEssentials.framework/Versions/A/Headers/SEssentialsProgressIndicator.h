//
//  SEssentialsProgressIndicator.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsProgressIndicatorStyle.h"
#import "SEssentialsDiscreteIndicatorElementFactory.h"
#import "SEssentialsCommon.h"

typedef NS_ENUM(NSInteger, SEssentialsIndicatorType)    {
    SEssentialsIndicatorTypeLinearContinuous,
    SEssentialsIndicatorTypeLinearDiscrete,
    SEssentialsIndicatorTypeRadialContinuous,
    SEssentialsIndicatorTypeRadialDiscrete
};

/**
 `SEssentialsProgressIndicator` is used to display the progress of a long running task.
 
 The `SEssentialsProgressIndicator` holds the current progress of the indicator.  Progress can range between `0`, indicating no progress, and `1`, indicating that a task is complete.
 
 To create a progress indicator, you use one of the factory methods provided.  For example:
 
    SEssentialsProgressIndicator *progressIndicator = [SEssentialsProgressIndicator progressIndicatorOfType:SEssentialsIndicatorTypeLinearContinuous
        withFrame:CGRectMake(0, 0, 200, 200)];
 
 This returns a progress indicator which you can use in your code.  When creating an indicator, you specify the type of indicator you would like.  The following types are available:
 
 - SEssentialsIndicatorTypeLinearContinuous: A continuous horizontal progress bar
 - SEssentialsIndicatorTypeLinearDiscrete: A horizontal progress bar, made up of a set of discrete elements
 - SEssentialsIndicatorTypeRadialContinuous: A continuous radial progress bar
 - SEssentialsIndicatorTypeRadialDiscrete: A radial progress bar, made up of a set of discrete elements
 
 When you set the progress of a progress indicator, it will redraw to show the new status.
 
 For discrete progress indicators, you can set the number of elements in the indicator using the `numberOfElements` property.  If you wish to use a custom factory to provide the progress indicator with elements, you can override the default by setting the `elementFactory` property.
 
 The `SEssentialsProgressIndicator` has a style object, which is an instance of `SEssentialsProgressIndicatorStyle`.
 This manages the look and feel of the control by setting things like the color of the progress indicator.
 The style object should always be used to update the look and feel of the control, rather than
 accessing the progress indicator and setting its properties directly.
 
 The style has precedence over any visual changes which are made to the progress indicator directly. For example,
 if you were to set a property such as the tint color on the progress indicator itself this change would be
 overridden the next time the style is updated. That is why it is important to use the style to manage the look
 and feel of the control.
 
 @sample IndicatorsGettingStarted
 @sample IndicatorsHowToCustomElements
 @sample IndicatorsHowToCustomStyling
 
 */
@interface SEssentialsProgressIndicator : UIView

/**
 The current progress of the indicator.  Progress can range between `0` and `1`. Because this updates the UI it should always be set on the main thread.
 */
@property (nonatomic, assign) CGFloat progress;

/**
 The styling which will be applied to the progress bar.
 */
@property(nonatomic, readonly, retain) SEssentialsProgressIndicatorStyle *style;

#pragma mark - Discrete Indicator Properties
/** @name Discrete Indicator Properties */

/**
 A factory which provides individual elements to be drawn in the progress indicator.  This is only relevant to discrete progress bars.
 
 **Note:** When you set the element factory on a progress indicator, this causes the indicator to redraw.  For this reason, it is important to set the number of elements on your indicator before setting the factory, if your factory will only display a certain number of elements correctly.
 */
@property (nonatomic, assign) id<SEssentialsDiscreteIndicatorElementFactory> elementFactory;

/** Applies the specified theme to the progress indicator. */
- (void)applyTheme:(SEssentialsTheme*)theme;

#pragma mark - Factory Methods
/** @name Factory Methods */

/**
 Creates a progress indicator.  
 
 @param indicatorType The type of indicator you would like.  The following types are available:
 
 - SEssentialsIndicatorTypeLinearContinuous: A continuous horizontal progress bar
 - SEssentialsIndicatorTypeLinearDiscrete: A horizontal progress bar, made up of a set of discrete elements
 - SEssentialsIndicatorTypeRadialContinuous: A continuous radial progress bar
 - SEssentialsIndicatorTypeRadialDiscrete: A radial progress bar, made up of a set of discrete elements
 
 @param frame The frame rectangle for the indicator, measured in points.
 @return An initialized progress indicator.
 */
+ (SEssentialsProgressIndicator*)progressIndicatorOfType: (SEssentialsIndicatorType)indicatorType withFrame: (CGRect)frame;

/**
 Creates a progress indicator.
 
 @param indicatorType The type of indicator you would like.  The following types are available:
 
 - SEssentialsIndicatorTypeLinearContinuous: A continuous horizontal progress bar
 - SEssentialsIndicatorTypeLinearDiscrete: A horizontal progress bar, made up of a set of discrete elements
 - SEssentialsIndicatorTypeRadialContinuous: A continuous radial progress bar
 - SEssentialsIndicatorTypeRadialDiscrete: A radial progress bar, made up of a set of discrete elements
 
 @param frame The frame rectangle for the indicator, measured in points.
 @param style The style to use for this progress indicator.
 @return An initialized progress indicator.
 */
+ (SEssentialsProgressIndicator*)progressIndicatorOfType: (SEssentialsIndicatorType)indicatorType withFrame: (CGRect)frame style: (SEssentialsProgressIndicatorStyle*)style;

@end
