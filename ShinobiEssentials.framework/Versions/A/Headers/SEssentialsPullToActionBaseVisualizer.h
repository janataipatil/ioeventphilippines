//
//  SEssentialsPullToActionBaseVisualizer.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 25/02/2014.
//
//

#import <Foundation/Foundation.h>

#import "SEssentialsPullToActionVisualizer.h"

/** An abstract implementation of `SEssentialsPullToActionVisualizer` that handles sizing and animation of the Pull to Action
 control.
 */
@interface SEssentialsPullToActionBaseVisualizer : NSObject <SEssentialsPullToActionVisualizer>

@end
