//
//  SEssentialsPullToActionDelegate.h
//  ShinobiEssentials
//
//  Created by Thomas Kelly on 20/02/2014.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsPullToActionState.h"

@class SEssentialsPullToAction;

/**
 The delegate of a `SEssentialsPullToAction` must adopt the `SEssentialsPullToActionDelegate` protocol.
 
 These callbacks allow for notifications about certain Pull to Action control events, such as when an action is triggered, and
 when the control 'will' and 'did' change state.
 
 The `pullToActionTriggeredAction:` method gives you an opportunity to perform your action.
 */
@protocol SEssentialsPullToActionDelegate <NSObject>

@required

/** An action has been triggered on the Pull to Action control.
 
 This will be called when a gesture pulls the control down past its `pullThreshold`, and then the gesture finishes.
 
 When the triggered action has completed, you will need to call `[pullToAction actionCompleted]` to signal that the
 Pull to Action control should return to an idle position.
 */
-(void)pullToActionTriggeredAction:(SEssentialsPullToAction*)pullToAction;

@optional

/** The state of the Pull to Action control is about to change.
 
 @param oldState The old state of the control
 @param newState The new state of the control
 */
-(void)pullToAction:(SEssentialsPullToAction*)pullToAction willChangeFromState:(SEssentialsPullToActionState)oldState toState:(SEssentialsPullToActionState)newState;

/** The state of the Pull to Action control has changed.
 
 @param oldState The old state of the control
 @param newState The new state of the control
 */
-(void)pullToAction:(SEssentialsPullToAction*)pullToAction didChangeFromState:(SEssentialsPullToActionState)oldState toState:(SEssentialsPullToActionState)newState;

@end
