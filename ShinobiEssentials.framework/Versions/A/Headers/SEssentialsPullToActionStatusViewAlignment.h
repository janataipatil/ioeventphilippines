//
//  SEssentialsPullToActionStatusViewAlignment.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 26/02/2014.
//
//

/**
 Specify how the Pull to Action control is visualized.

The possible values are:

- SEssentialsPullToActionStatusViewAlignmentStretch: The status view of the Pull to Action control stretches to fill the available space outside of the scroll view, keeping the contents of the status view centered.
- SEssentialsPullToActionStatusViewAlignmentBottom: The status view of the Pull to Action control remains a constant size. It is anchored to the bottom edge of the control.
 */
typedef NS_ENUM(NSInteger, SEssentialsPullToActionStatusViewAlignment) {
    SEssentialsPullToActionStatusViewAlignmentStretch,
    SEssentialsPullToActionStatusViewAlignmentBottom
};
