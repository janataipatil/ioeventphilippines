//
//  SEssentialsPullToActionDefaultVisualizer.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 24/02/2014.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsPullToActionBaseVisualizer.h"

/** The `SEssentialsPullToActionStretchVisualizer` is an implementation of the `SEssentialsPullToActionVisualizer` that gives the visual
 appearance of the Pull to Action control stretching to reveal the status view.
 
 This visualizer can be chosen by initializing the Pull to Action control with value `SEssentialsPullToActionStatusViewAlignmentStretch`
 to one of either `initWithScrollView:statusViewAlignment:` or `initWithFrame:statusViewAlignment:`.
 */
@interface SEssentialsPullToActionStretchVisualizer : SEssentialsPullToActionBaseVisualizer

@end
