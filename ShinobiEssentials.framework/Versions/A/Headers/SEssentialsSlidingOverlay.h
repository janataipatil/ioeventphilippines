//
//  SEssentialsSlidingOverlay.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsView.h"
#import "SEssentialsSlidingOverlayToolbar.h"
#import "SEssentialsSlidingOverlayStyle.h"
#import "SEssentialsSlidingOverlayDelegate.h"
#import "SEssentialsCommon.h"

typedef NS_ENUM(NSInteger, SEssentialsSlidingOverlayLocation)
{
    SEssentialsSlidingOverlayLocationLeft,
    SEssentialsSlidingOverlayLocationTop,
    SEssentialsSlidingOverlayLocationRight,
    SEssentialsSlidingOverlayLocationBottom
};

typedef NS_ENUM(NSInteger, SEssentialsUnderlaySizeType)
{
    SEssentialsUnderlayPixelSize,
    SEssentialsUnderlayRatioSize,
};



/**
 The `SEssentialsSlidingOverlay` class represents two `UIViews`, one of which
 slides over the other, to reveal it. This saves screen real-estate by hiding
 content behind the main view and animating it in when required.
 
 The `underlay` may be exposed from left, right, top or bottom of the main
 content view, which will feel like it is sliding (not shrinking) to expose
 the `underlay`. The `underlay` is the full height (left or right
 side) or full width (top or bottom) and its other dimension is a variable
 percentage of the `overlay`.
 
 The sliding overlay has a `style` object, which is an instance of `SEssentialsSlidingOverlayStyle`.  This manages the look and feel of the control by setting things like the tint color and background texture of the `underlay` and the `overlay`.  The `style` object should always be used to update the look of the control, rather than accessing the `underlay` or `overlay` and setting their properties directly.
 
 The `style` has precedence over any visual changes which are made to the sliding overlay directly.  For example, if you were to set a property such as the `tintColor` on the `underlay` directly, this change will be overridden next time the `style` is updated.  That is why it is important to use the `style` to manage the look and feel of the control.
 
 @sample SlidingOverlayGettingStarted
 @sample SlidingOverlayHowToDoubleOverlay
 */

@interface SEssentialsSlidingOverlay : UIView


#pragma mark Initalisation
/** @name Initializing an SEssentialsSlidingOverlay */

/** Initialize the `overlay` with a frame and specify whether a toolbar should be
 * displayed and supply a style */
- (id) initWithFrame:(CGRect)frame andToolbar:(BOOL)showToolbar style:(SEssentialsSlidingOverlayStyle *)style;

/** Initialize the `overlay` with a frame and specify whether a toolbar should be
 * displayed */
- (id) initWithFrame:(CGRect)frame andToolbar:(BOOL)showToolbar;

/** Initialize the `overlay` with a frame. Toolbar is on by default */
- (id) initWithFrame:(CGRect)frame;

/** Initialize the `overlay`. Toolbar is on by default */
- (id) init;

#pragma mark Delegate
/** @name Delegation */

/**
 * The delegate for the `SEssentialsSlidingOverlay`
 * See `SEssentialsSlidingOverlayDelegate`
 */
@property(nonatomic, assign)  id<SEssentialsSlidingOverlayDelegate> delegate;

#pragma mark View Properties
/** @name View Properties */

/** The hidden view, which the `overlay` slides over to reveal - add subviews
 * to this as necessary */
@property(nonatomic, retain, readonly) SEssentialsView                             *underlay;

/** The `overlay`, which slides over to reveal the `underlay` - add subviews to
 * this as necessary */
@property(nonatomic, retain, readonly) SEssentialsView                             *overlay;

/**
  The location of the `underlay` - the `overlay` slides out to reveal the
  `underlay` anchored in this direction
 
    typedef enum
    {
      SEssentialsSlidingOverlayLocationTop,
      SEssentialsSlidingOverlayLocationBottom,
      SEssentialsSlidingOverlayLocationLeft,
      SEssentialsSlidingOverlayLocationRight
    } SEssentialsSlidingOverlayLocation;
 
 Setting this property will result in the change being animated. Use
 `setUnderlayLocation:animated:` to control this.
 
 */
@property(nonatomic, assign)           SEssentialsSlidingOverlayLocation   underlayLocation;


/**
 The location of the `underlay` - the `overlay` slides out to reveal the
 `underlay` anchored in this direction
 
   typedef enum
   {
     SEssentialsSlidingOverlayLocationTop,
     SEssentialsSlidingOverlayLocationBottom,
     SEssentialsSlidingOverlayLocationLeft,
     SEssentialsSlidingOverlayLocationRight
   } SEssentialsSlidingOverlayLocation;
 
 This method allows the location to be set with or without animating from the
 current state to the new state.
 
 @param newLocation The location to which the underlay should be moved.
 @param animated Specify `YES` for the location change to be animated.
 */
- (void)setUnderlayLocation:(SEssentialsSlidingOverlayLocation)newLocation animated:(BOOL)animated;

/**
  The method used to define the size of the `underlay`. Defaults to `SEssentialsUnderlayRatioSize`.
 
  `SEssentialsUnderlayPixelSize` allows the size of the `underlay` to be given as a value of pixels
  in `underlayRevealAmount`.
  `SEssentialsUnderlayRatioSize` allows the size of the `underlay` to be defined as a proportion
  of the view in `underlayRevealAmount`.
 
    typedef enum
    {
      SEssentialsUnderlayPixelSize,
      SEssentialsUnderlayRatioSize,
    } SEssentialsUnderlaySizeType;
 */
@property(nonatomic, assign) SEssentialsUnderlaySizeType underlaySizeType;

/** 
  A `CGFloat` value which determines the size of the `underlay`. Defaults to `0.25`.
 
  If `underlaySizeType` is set to
  `SEssentialsUnderlayPixelSize` then `underlayRevealAmount` defines the size of the `underlay` in
  pixels. This is a value between `0.0` and the width/height of the sliding overlay.
 
  Otherwise, if `underlaySizeType` is set to `SEssentialsUnderlayRatioSize` then `underlayRevealAmount`
  defines the size of the `underlay` as a proportion of the view. In this case, the value should be
  between `0.0` and `1.0`.
 */
@property(nonatomic, assign)           CGFloat                             underlayRevealAmount;

/** A value describing the area where the user can perform a gesture to reveal the `underlay`.
 This is a value between `0.0` and `1.0`.
 This represents a fraction of the control's width or height (Defaults to `0.5`) */
@property(nonatomic, assign)           CGFloat                             gestureAreaRatio;

/** @return A boolean indicating whether the `underlay` is currently hidden */
- (BOOL) underlayHidden;

/**
 * Reference to the `style` object used to style the `overlay` and `underlay`.
 */
@property(nonatomic, readonly, retain) SEssentialsSlidingOverlayStyle *style;

/** Applies the specified theme to the sliding overlay. */
- (void)applyTheme:(SEssentialsTheme*)theme;

#pragma mark Underlay Control
/** @name `underlay` Show and Hide */

/** Toggle the `underlay`'s visibility, causing the `overlay` to slide over,
 * revealing or concealing it. */
- (void) toggleUnderlayAnimated:(BOOL)animate;

/** Show the `underlay`, specifying animation */
- (void) showUnderlayAnimated:(BOOL)animate;

/** Hide the `underlay`, specifying animation */
- (void) hideUnderlayAnimated:(BOOL)animate;

#pragma mark Toolbar
/** @name Toolbar */

/** Toolbar property - using this the toolbar can be changed or removed */
@property(nonatomic, readonly, retain) SEssentialsSlidingOverlayToolbar *toolbar;

#pragma mark Gesturing
/** @name Gesturing */

/** Gesture recognizer property - using this the gesture recognizer can be
 * disabled or changed completely. This is a swipe gesture by default and,
 * unless overridden, the swipe direction is updated to keep in sync with the
 * `underlay` location
 */
@property(nonatomic, retain)           UIGestureRecognizer      *gestureRecognizer;

#pragma mark Animation Properties
/**
 * @name Animation
 *
 * This section exposes animation properties.
 * These are used when the `overlay` slides over the `underlay`.
 */

/** The duration, in seconds, over which the animation should take place. Defaults to `0.25`. */
@property(nonatomic, assign)           CGFloat                   animationDuration;

/** The animation's curve, see `UIViewAnimationCurve`. This animation curve defines the way the `underlay` slides in and out of view. */
@property(nonatomic, assign)           UIViewAnimationCurve      animationCurve;

@end
