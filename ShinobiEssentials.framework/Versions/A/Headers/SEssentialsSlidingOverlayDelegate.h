//
//  SEssentialsSlidingOverlayDelegate.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

@class SEssentialsSlidingOverlay;

/**
 These callback methods allow tracking of the sliding overlay's movement.
 The callbacks inform you of when the movement begins and ends, allowing you to perform appropriate actions
 */

@protocol SEssentialsSlidingOverlayDelegate <NSObject>

@optional

/** Called before the underlay is revealed to determine whether or not it should be revealed */
- (BOOL) slidingOverlayUnderlayShouldAppear:(SEssentialsSlidingOverlay *)slidingOverlay;

/** Called when the underlay is about to be revealed */
- (void) slidingOverlayUnderlayWillAppear:(SEssentialsSlidingOverlay *)slidingOverlay;

/** Called when the underlay has been revealed */
- (void)  slidingOverlayUnderlayDidAppear:(SEssentialsSlidingOverlay *)slidingOverlay;

/** Called before the underlay is hidden to determine whether or not it should be hidden */
- (BOOL) slidingOverlayUnderlayShouldDisappear:(SEssentialsSlidingOverlay *)slidingOverlay;

/** Called when the underlay will hide */
- (void) slidingOverlayUnderlayWillDisappear:(SEssentialsSlidingOverlay *)slidingOverlay;

/** Called when the underlay has hidden */
- (void)  slidingOverlayUnderlayDidDisappear:(SEssentialsSlidingOverlay *)slidingOverlay;

@end
