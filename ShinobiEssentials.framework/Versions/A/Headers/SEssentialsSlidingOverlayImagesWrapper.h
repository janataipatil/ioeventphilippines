//
//  SEssentialsSlidingOverlayImagesWrapper.h
//  ShinobiEssentials
//
//  Created by Thomas Kelly on 28/08/2013.
//
//

#import <Foundation/Foundation.h>

/** You should use the `SEssentialsSlidingOverlayImagesWrapper` to customize the images used in a `SEssentialsSlidingOverlayStyle`. Set the images on the wrapper which you wish to customize. When you create a style object with this wrapper, it will use these custom images. For any images which aren't set, the style will just use its default images.
*/
@interface SEssentialsSlidingOverlayImagesWrapper : NSObject

/**
 * The image which is displayed for the main button on the toolbar.
 */
@property (nonatomic, retain) UIImage *buttonImage;

/**
 * The image which is displayed for the main button on the toolbar when it is pressed.
 */
@property (nonatomic, retain) UIImage *buttonPressedImage;

/**
 * The image mask which is used for the main button on the toolbar.
 */
@property (nonatomic, retain) UIImage *buttonMask;

@end
