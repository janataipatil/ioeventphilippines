//
//  SEssentialsSlidingOverlayToolbar.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsView.h"

typedef NS_ENUM(NSInteger, SEssentialsSlidingOverlayButtonPosition)
{
    SEssentialsSlidingOverlayButtonPositionLeft,
    SEssentialsSlidingOverlayButtonPositionRight
};

/**
 * The `SEssentialsSlidingOverlayToolbar` class provides access to the button
 * and toolbar area for custom styling.
 */
@interface SEssentialsSlidingOverlayToolbar : SEssentialsView

/**
 Defines the location of the button in the toolbar with an enum

     typedef enum {
         SEssentialsSlidingOverlayButtonPositionLeft,
         SEssentialsSlidingOverlayButtonPositionRight
     } SEssentialsSlidingOverlayButtonPosition;


 */
@property(nonatomic, assign) SEssentialsSlidingOverlayButtonPosition buttonPosition;

/**
 * Reference to the button that opens and closes the overlay.
 */
@property(nonatomic, retain) UIButton *button;

@end
