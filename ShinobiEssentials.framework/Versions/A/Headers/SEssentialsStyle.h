//
//  SEssentialsStyle.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsTheme;

/**
 SEssentialsStyle is the base class for all styles in ShinobiEssentials. Each control has its own particular style object, which it uses to determine its appearance.

 SEssentialsStyle objects are initialized from an instance of SEssentialsTheme.  The theme is the base on which any concrete style object is built.  This allows us to ensure that controls across an application have a common look and feel.  
 
 Style objects can be initialized with or without a particular theme.  If a style object is not initialized with a concrete instance of a theme, it uses the global theme provided by SEssentialsTheme.

 All concrete style classes inherit from this base class.
 
 */
@interface SEssentialsStyle : NSObject

/** DEPRECATED: We create a style object with a theme, but we no longer hold a persistent reference to it.  If you wish to update your style object with a theme, you should call `applyTheme:` instead.
 *
 * This defaults to use the global theme but when set it can listen to a specific theme.
 */
@property (nonatomic,retain) SEssentialsTheme *theme DEPRECATED_ATTRIBUTE;

/**
 * Initialize the style with a particular theme.
 */
- (id)initWithTheme:(SEssentialsTheme*)theme;

/** Applies the properties of the specified theme to this style object.  Each concrete style implementation will use the properties of the theme in its own way.
 
 For example, the progress indicator style will use the secondary tint color on the theme to set its tint color.
 */
- (void)applyTheme:(SEssentialsTheme*)theme;

@end
