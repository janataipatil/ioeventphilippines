//
//  SEssentialsTabOverflowProtocol.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsTabbedView;

@protocol SEssentialsTabOverflowProtocol <NSObject>

/**
 * This is the protocol that is adopted by `SEssentialsOverflowDropdownView` when the overflow dropdown button is touched.
 * The array contains `SEssentialsTab` for each tab. The implementation should call `SEssentialsTabbedView` activateTab on the selected item.
 */
-(void) tabbedView:(SEssentialsTabbedView *)tabbedView showTabs:(NSArray *)tabs;


@end
