//
//  SEssentialsLicenseVerifier.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

//Essentials Components
#import "SEssentialsAccordion.h"
#import "SEssentialsActivityIndicator.h"
#import "SEssentialsFlowLayout.h"
#import "SEssentialsProgressIndicator.h"
#import "SEssentialsSlidingOverlay.h"
#import "SEssentialsTabbedView.h"
#import "SEssentialsPullToAction.h"

//Essentials Components - Carousel
#import "SEssentialsCarouselLinear2D.h"
#import "SEssentialsCarouselLinear3D.h"
#import "SEssentialsCoverFlow.h"
#import "SEssentialsCarouselRadial.h"
#import "SEssentialsCarouselCylindrical.h"
#import "SEssentialsCarouselInverseCylindrical.h"

//View Decorators
#import "SEssentialsDecoratedFadedView.h"
#import "SEssentialsDecoratedReflectedView.h"
#import "SEssentialsDecoratedShadowedView.h"
#import "SEssentialsDecoratedDoubleSidedView.h"
#import "SEssentialsDecoratedCachedView.h"

//Themes
#import "SEssentialsDarkTheme.h"
#import "SEssentialsTransparentTheme.h"
#import "SEssentialsLightTheme.h"
#import "SEssentialsIOS7Theme.h"

//Animation
#import "SEssentialsAnimationCurveBounce.h"
#import "SEssentialsAnimationCurveEaseIn.h"
#import "SEssentialsAnimationCurveEaseOut.h"
#import "SEssentialsAnimationCurveEaseInOut.h"
#import "SEssentialsAnimationCurveLinear.h"
#import "SEssentialsAnimationCurveWobbleHeavy.h"
#import "SEssentialsAnimationCurveWobbleLight.h"

// NS_ENUM is part of Foundation from iOS6 onwards. If using a previous version of iOS, we add it back in.
#ifndef NS_ENUM
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#endif

/**
 ShinobiEssentials is a global utility class for the ShinobiEssentials framework.
 
 It allows you to do the following:
 
 - Get information on the version of the framework
 
 - For trial versions of ShinobiEssentials, it allows you to set the license key.  This enables you to use the framework for the duration of the trial period.
 
 - It defines a global theme.  If a SEssentialsStyle object is not created with a specific instance of a theme, it uses the global theme.  This allows us to define a common appearance for all controls within an application.
 
 All controls by default use the global theme. To access this theme and set one of its values you would use the following code
 
    SEssentialsTheme *globalTheme = [ ShinobiEssentials theme];
 
 If you wish to change the current global theme then use
 
    [ ShinobiEssentials setTheme:[ SEssentialsDarkTheme new]];
 
 To change the primary tint color on all controls using the theme you would use the following code
 
    globalTheme.primaryTintColor = [ UIColor redColor];
 
 Once you've updated the theme, you should go through all your controls and call `applyTheme` on them, to configure them from the theme.
 */
@interface ShinobiEssentials : NSObject

/**
 Sets the license key for the trial version of the ShinobiEssentials framework.  You will be emailed the license key when you download the framework.
 */
+ (void)setLicenseKey: (NSString*)licenseKey;

/**
 Verifies the license key.  Each control in the framework does this when they are initialized.
 */
+ (void)verifyLicenseKey;

/**
 Returns the version number of the framework, and the date on which it was built.
 */
+ (NSString *)getInfo;

/** Sets the global theme to the specified theme.
 */
+ (void)setTheme:(SEssentialsTheme*)theme;

/** The global theme, which is used by default when creating any new ShinobiEssentials controls.  This defaults to `SEssentialsIOS7Theme` when the framework is run in iOS7, and `SEssentialsDarkTheme` for previous versions of iOS.
 */
+ (SEssentialsTheme*)theme;

@end
