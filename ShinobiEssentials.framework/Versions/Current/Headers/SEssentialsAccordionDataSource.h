//
//  SEssentialsAccordionDataSource.h
//  Accordion
//
//  Copyright (c) 2012 Scott Logic Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SEssentialsAccordion.h"

/**
 The `SEssentialsAccordionDataSource` protocol is adopted by an object to provide
 the content for the sections within the accordion. It has one required method.
 
 The content supplied by the datasource is only retained by the Accordion while the content is visible on screen, and will be released when scrolled off screen or the section is closed. It is up to the class adopting the `SEssentialsAccordionDataSource` protocol to retain content so it can be requested at any time.
 */
@protocol SEssentialsAccordionDataSource <NSObject>

@required
/**
 Provides the content `UIView` for a given section. This method is called at some
 point between the section being added to an accordion, and the section being
 opened. If the datasource on an accordion is replaced, the content for each
 section will be re-requested.
 @param accordion The `SEssentialsAccordion` requesting the content
 @param section The `SEssentialsAccordionSection` for which the content is being requested
 */
- (UIView *)accordion:(SEssentialsAccordion *)accordion contentForSection:(SEssentialsAccordionSection*)section;

@end
