//
//  SEssentialsAccordionSectionHeader.h
//  Accordion
//
//  Copyright (c) 2012 Scott Logic Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEssentialsView.h"
#import "SEssentialsAccordionSectionHeaderStyle.h"

/**
 The `SEssentialsAccordionSectionHeader` contains information relevant to an
 `SEssentialsAccordionSection` header. It is typically instantiated with `initWithFrame:andTitle:`
 
 The `SEssentialsAccordionSectionHeader` has a style object, which is an instance of `SEssentialsAccordionSectionHeaderStyle`.
 This manages the look and feel of the control by setting things like the tint color and background texture of the header.
 The style object should always be used to update the look of the control, rather than accessing the accordion section header
 and setting its properties directly.
 
 The style has precedence over any visual changes which are made to the accordion section header directly. For example,
 if you were to set a property such as the background color on the accordion section header directly, this change will be
 overridden the next time the style is updated. That is why it is important to use the style to manage the look and feel of
 the control.
 */
@interface SEssentialsAccordionSectionHeader : SEssentialsView

/**
 A reference to the style controlling the style of the section header and
 content. The style must be provided at construction time.
 */
@property (readonly, nonatomic, retain) SEssentialsAccordionSectionHeaderStyle *style;

/**
 A label displaying the title of the section. The font and font color are
 controlled in the style.
 */
@property (retain, nonatomic) UILabel *sectionTitleLabel;

/**
 Initializes the section header with a given title.
 
 The `SEssentialsAccordionSectionHeader` is given a default style. This is the
 typical method used for instantiating an `SEssentialsAccordionSectionHeader`.
 
 @param title The title displayed in the section header
 */
- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title;

/**
 Initializes the section header with a given title and style.
 
 @param title The title displayed in the section header
 @param style The section header style used to configure the appearance of the header
 */
- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title andStyle:(SEssentialsAccordionSectionHeaderStyle *)style;

@end
