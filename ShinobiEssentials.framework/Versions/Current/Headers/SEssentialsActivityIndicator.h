//
//  SEssentialsActivityIndicator.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsProgressIndicator.h"

/**
 The `SEssentialsActivityIndicator` is used to show that a task is in progress.  An activity indicator starts animating when it is instantiated.
 
 You can control the proportion of the indicator which is filled at any time using the `fillProportion` property.  You can control how long the indicator takes to make a full revolution using the `cycleTime` property.
 
 To create an activity indicator, you use one of the factory methods provided.  For example:
 
    SEssentialsActivityIndicator *activityIndicator = [SEssentialsActivityIndicator activityIndicatorOfType:SEssentialsIndicatorTypeRadialContinuous
        withFrame:CGRectMake(0, 0, 200, 200)];
 
 This returns a activity indicator which you can use in your code.  When creating an indicator, you specify the type of indicator you would like.  The following types are available:
 
 - SEssentialsIndicatorTypeLinearContinuous: A continuous horizontal activity indicator
 - SEssentialsIndicatorTypeLinearDiscrete: A horizontal activity indicator, made up of a set of discrete elements
 - SEssentialsIndicatorTypeRadialContinuous: A continuous radial activity indicator
 - SEssentialsIndicatorTypeRadialDiscrete: A radial activity indicator, made up of a set of discrete elements
 
 `SEssentialsActivityIndicator` inherits properties from `SEssentialsProgressIndicator`, so for discrete indicators you can set the number of elements in the indicator or the factory which provides elements to the indicator, in the same way as you would do for a progress indicator.
 
 @sample IndicatorsGettingStarted
 @sample IndicatorsHowToCustomElements
 @sample IndicatorsHowToCustomStyling
 
 */
@interface SEssentialsActivityIndicator : SEssentialsProgressIndicator

/**
 The proportion of the indicator which is filled.  The value of this property can range between `0` and `1`.  The default value of this property is `0.35`.
 */
@property (nonatomic, assign) CGFloat fillProportion;

/**
 The time which the indicator takes to make a full revolution, in seconds.  The default value of this property for continuous activity indicators is `5` seconds.  The default value of this property for discrete activity indicators is `1` second.
 */
@property (nonatomic, assign) CGFloat cycleTime;


#pragma mark - Factory Methods
/** @name Factory Methods */

/**
 Creates an activity indicator.
 
 @param indicatorType The type of indicator you would like.  The following types are available:
 
 - SEssentialsIndicatorTypeLinearContinuous: A continuous horizontal activity bar
 - SEssentialsIndicatorTypeLinearDiscrete: A horizontal activity bar, made up of a set of discrete elements
 - SEssentialsIndicatorTypeRadialContinuous: A continuous radial activity bar
 - SEssentialsIndicatorTypeRadialDiscrete: A radial activity bar, made up of a set of discrete elements
 
 @param frame The frame rectangle for the indicator, measured in points.
 @return An initialized activity indicator.
 */
+ (SEssentialsActivityIndicator*)activityIndicatorOfType: (SEssentialsIndicatorType)indicatorType withFrame:(CGRect)frame;

/**
 Creates an activity indicator.
 
 @param indicatorType The type of indicator you would like.  The following types are available:
 
 - SEssentialsIndicatorTypeLinearContinuous: A continuous horizontal activity bar
 - SEssentialsIndicatorTypeLinearDiscrete: A horizontal activity bar, made up of a set of discrete elements
 - SEssentialsIndicatorTypeRadialContinuous: A continuous radial activity bar
 - SEssentialsIndicatorTypeRadialDiscrete: A radial activity bar, made up of a set of discrete elements
 
 @param frame The frame rectangle for the indicator, measured in points.
 @param style The style to use for this activity indicator.
 @return An initialized activity indicator.
 */
+ (SEssentialsActivityIndicator*)activityIndicatorOfType: (SEssentialsIndicatorType)indicatorType withFrame:(CGRect)frame style: (SEssentialsProgressIndicatorStyle*)style;

@end
