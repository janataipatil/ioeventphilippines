//
//  SEssentialsCarousel.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@class SEssentialsAnimationCurve;
#import "SEssentialsCarouselDataSource.h"
#import "SEssentialsCarouselDelegate.h"

typedef NS_ENUM(NSInteger, SEssentialsCarouselOrientation)
{
    SEssentialsCarouselOrientationHorizontal = 0,
    SEssentialsCarouselOrientationVertical
};

/** 
 An SEssentialsCarousel is a layout component for displaying a collection of UIViews, in any 2D or 3D configuration. Users wanting a preset layout type should use or subclass the relevant subclass of the carousel. This allows for various layouts, including:
 
 - Horizontal & Vertical 2D linear (SEssentialsCarouselLinear2D)
 
 <img src="../docs/Docs/img/Linear2DThumb.png" />
 
 - Horizontal & Vertical 3D linear (SEssentialsCarouselLinear3D)
 
 <img src="../docs/Docs/img/Linear3DThumb.png" />
 
 - Horizontal & Vertical Cover Flow (SEssentialsCoverFlow)
 
 <img src="../docs/Docs/img/CoverFlowThumb.png" />
 
 - Cylinder (SEssentialsCarouselCylindrical)
 
 <img src="../docs/Docs/img/CylindricalThumb.png" />
 
 - Inverse Cylinder (SEssentialsCarouselInverseCylindrical)
 
 <img src="../docs/Docs/img/InverseCylindricalThumb.png" />
 
 - Radial (SEssentialsCarouselRadial)
 
 <img src="../docs/Docs/img/RadialScalingThumb.png" />

 To create a carousel with a given type, such as Horizontal Cover Flow, initialize it as follows;
 
    SEssentialsCoverFlow *carousel = [[ SEssentialsCoverFlow alloc] initWithFrame:view.bounds];
    carousel.orientation = SEssentialsCarouselOrientationHorizontal;
 
 The SEssentialsCarousel class is an abstract implementation, and should only be directly subclassed by advanced users, to implement their own layouts.
 
 If the carousel has an SEssentialsCarouselDelegate, the carousel will send updates about the carousel scrolling, items being displayed and or removed from display, and items being given a new layout. All of these are optional, but allow responses to changes in the state of the carousel.
 
 A carousel can either wrap items (`wrapItems`), such that items at each end loop and display the items from the other end, or stop panning upon reaching each end.
 
 The focus point (`focusPointNormalized`) is the point on the carousel where the item in focus is placed. An item close to this point will move to settle exactly on this point. By default, the focus point is the center of the carousel. The carousel can move an item to the focus point by panning through the carousel items, or programmatically (`focusOnItemAtIndex:animated:`).
 
 The carousel contains a SEssentialsCarouselDataSource object, which provides it with its items. Items are requested from the `dataSource` as they become visible, and are removed from the carousel when they disappear off screen. The carousel can also be informed of a need to refresh the items (`reloadData`) by requesting new items from the `dataSource`.
 
 <h2>Advanced Carousel Usage</h2>
 
 If you create a user defined carousel, the carousel will ask for the `positionOfItemAtOffset:` and `transformOfItemAtOffset:` of items in the carousel. These allow arbitrary positioning and transformations for advanced carousel users, by providing their own functions to calculate the position from the focus, rotation, and scaling of each item. The `offset` argument is the distance from the fixed focus point of the carousel. As the carousel pans through the items, the offset changes while the fixed point remains constant. For example, when the carousel first displays, the fixed point is at index 0, so the offset of each item is the same as their index;
 
 <img src="../docs/Docs/img/Offset0.png" />
 
 As the carousel begins panning through the items, the items move past the fixed point, and their offsets change. For example, in the example below, the fixed point maps to part way between index 0 and index 1, and the offset of all the items shifts accordingly;
 
 <img src="../docs/Docs/img/Offset06.png" />
 
 The `offset` argument is used primarily to calculate a given position and transform for an item, based on it's distance from the focus point of the carousel, as well as informing the `delegate` of a layout of an item at a given offset, to allow for additional effects to be applied, such as fading the alpha based on distance from the focus of the carousel.
 
 @sample CarouselGettingStarted
 @sample CarouselHowToCustomPath
 @sample CarouselHowToDoubleSidedViews
 @sample CarouselDesigner
 */
@interface SEssentialsCarousel : UIView

#pragma mark - Basic layout properties
/** @name Basic Layout properties */

/** The current offset through the carousel, measured in indices. Defaults to `0`.
 
 <img src="../docs/Docs/img/ContentOffset.png" />
 
 If the carousel is focused on the view with index = 2, then the contentOffset is 2. May also be a non-integer value to display between items.
 */
@property (nonatomic, assign) CGFloat contentOffset;

/** Focuses the carousel on a new offset, with optional animation.
 @param offset The offset to focus the carousel on.
 @param animated Whether to animate to the new position, or snap straight there
 @param duration The total duration of the animation
 
 When using `setContentOffset:` without specifying `animated`, the carousel will appear at the new `offset` without animating.
 */
-(void)setContentOffset:(CGFloat)offset animated:(BOOL)animated withDuration:(CGFloat)duration;

/** Focuses the carousel on the item at the specified index, with optional animation.
 @param index The index of the item to focus on
 @param animated Whether to animate to the new position, or snap straight there
 @param duration The total duration of the animation
 */
-(void)panToItemAtIndex:(NSInteger)index animated:(BOOL)animated withDuration:(CGFloat)duration;

/** Converts an index to the nearest corresponding offset in the carousel.
 @param index The index to convert
 
 In a wrapped carousel there are many offsets which correspond to a single index. This method returns the nearest offset which corresponds to the index specified.
 */
-(CGFloat)convertToOffsetFromIndex:(NSInteger)index;

/** Converts an offset to the corresponding index.
 @param offset The offset to convert
 */
-(NSInteger)convertToIndexFromOffset:(CGFloat)offset;

/** The point in the carousel bounds to center items around, taking values between `0.0` and `1.0` for the x and y axes. Defaults to being in the center of the carousel `(0.5, 0.5)`. */
@property (nonatomic, assign) CGPoint focusPointNormalized;

/** The item closest to the focus point of the carousel */
@property (nonatomic, readonly) UIView *currentItemInFocus;

/** If `YES`, the carousel loops by scrolling off one end, and onto the other. If `NO`, the carousel stops upon reaching the end.
 
 If the total number of items in the carousel is less than `maxNumberOfItemsToDisplay`, then the carousel will not wrap, regardless of whether wrapItems is set or not.
 */
@property (nonatomic, assign) BOOL wrapItems;

/** If `YES`, the carousel will always finish a pan gesture with an item centered on the focus point. If `NO`, the carousel will not automatically center the items on the focus point. Defaults to `YES` */
@property (nonatomic, assign) BOOL snapToFocusPoint;

/** Whether the carousel will bounce on reaching the end, or simply stop. Defaults to `YES` */
@property (nonatomic, assign) BOOL bounces;

#pragma mark - Basic Item management
/** @name Basic Item management */

/** The datasource to call for fetching item content. See the SEssentialsCarouselDataSource documentation for more information. */
@property (nonatomic, assign) id<SEssentialsCarouselDataSource> dataSource;

/** Reloads all items by re-requesting them from the datasource. */
-(void)reloadData;

/** The set of all currently visible items on the carousel. Be aware that the indices of items may not match up to the indices of the items as requested through the datasource. */
@property (nonatomic, readonly) NSArray *visibleItems;

/** @name Delegation */

/** The delegate to call back to on specific events. See the SEssentialsCarouselDelegate documentation for more information. */
@property (nonatomic, assign) id<SEssentialsCarouselDelegate> delegate;

#pragma mark - Advanced interactions
/** @name Advanced interactions */

/** How much friction to apply to the carousel. A higher value will bring the carousel to a halt faster. Defaults to 1.0 */
@property (nonatomic, assign) CGFloat frictionCoefficient;

/** If `YES`, the carousel will respond to the pan gesture, if enough items have been added. If `NO`, the carousel will not respond to panning. Defaults to `YES`. */
@property (nonatomic, assign) BOOL panEnabled;

/** The maximum number of on-screen items to display. Defaults to `15`.
 
 May be more than the number of items, if the maximum value has not yet been reached. If less than the number of items, then only a subset of items around the focus point will be displayed at a time.
 */
@property (nonatomic, assign) NSInteger maxNumberOfItemsToDisplay;

/** Force a layout of all items in the carousel. Similar to `reloadData`, but will not request new views from the datasource. */
-(void)redrawCarousel;

/** The type of animation curve to use when panning the carousel. Defaults to  `SEssentialsAnimationCurveEaseOut`.
 
 This curve is used during momentum after panning the carousel.
 
 @see SEssentialsAnimationCurve*/
@property (nonatomic, retain) SEssentialsAnimationCurve *momentumAnimationCurve;

/** The type of animation curve to use when programmatically changing the carousel offset. Defaults to `SEssentialsAnimationCurveEaseOut`.
 
 This curve is used when programmatically changing with `setContentOffset:animated:withDuration:`

 @see SEssentialsAnimationCurve*/
@property (nonatomic, retain) SEssentialsAnimationCurve *programmaticAnimationCurve;

/** Specifies a sub-area of the carousel bounds to allow panning.
 
 If set to `CGRectZero`, then the entire frame is used for gestures. Defaults to `CGRectZero`. To turn off panning completely, refer to `panEnabled`. */
@property (nonatomic, assign) CGRect panGestureFrame;

/** Specifies the direction of forward panning, as a vector. Need not be a unit vector.
 
 To increase the `contentOffset` by panning right-to-left, the `panVector` would be `(1,0)`. For left-to-right, it would be `(-1,0)`.
 Likewise, for panning bottom-to-top, `panVector` would be `(0,1)`, and for top-to-bottom, `(0,-1)`.
 */
@property (nonatomic, assign) CGPoint panVector;

/** The position of an item at a given offset from the focus point of the carousel.
 
 For the base class, this is CGPointZero, while the subclasses implement their own positioning.
 
 For advanced users wanting custom layouts, this can be implemented to position the views as desired. When used in combination with `transformOfItemAtOffset:` the position is applied before the transform.
 @param offset The distance of the target item from the focus of the carousel
 */
-(CGPoint)positionOfItemAtOffset:(CGFloat)offset;

/** The transform of a item (rotation, scaling and translation) at a given offset from the focus point of the carousel.
 
 For the base class, this is CATransform3DIdentity, while the subclasses implement their own transforms.
 
 For advanced users wanting custom layouts, this can be implemented to transform the views as desired. When used in combination with `positionOfItemAtOffset:` the position is applied before the transform.
 @param offset The distance of the target item from the focus of the carousel
 */
-(struct CATransform3D)transformOfItemAtOffset:(CGFloat)offset;

@end
