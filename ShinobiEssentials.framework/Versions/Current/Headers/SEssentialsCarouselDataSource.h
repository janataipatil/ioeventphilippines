//
//  SEssentialsCarouselDataSource.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsCarousel;

/**
 The `SEssentialsCarouselDataSource` provides an `SEssentialsCarousel` with it's content, and information about the content. The Carousel fetches items from the datasource as they become visible. This may be a long time after they were originally added to the Carousel, if they would have been added off screen. As a general rule all views supplied by the datasource should be equally sized, this will result in a nicely spaced carousel. If views of varying sizes are supplied then the carousel will still function but views may overlap and have uneven spacing.
 */
@protocol SEssentialsCarouselDataSource <NSObject>

@required
/** Return an item at a given absolute index. Note that the size of the item when displayed in the carousel is determined by the size of the view returned here. The item at the focus point of the carousel will be guaranteed to match exactly the size of the view supplied, views for items off focus may be transformed and appear larger or smaller depending on the carousel type.
 @param carousel The parent carousel
 @param index The index of the requested item in the carousel
 */
-(UIView*)carousel:(SEssentialsCarousel*)carousel itemAtIndex:(NSInteger)index;

/** The total number of items in the carousel (visible and off-screen included).
 @param carousel The parent carousel
 */
-(NSUInteger)numberOfItemsInCarousel:(SEssentialsCarousel*)carousel;

@end
