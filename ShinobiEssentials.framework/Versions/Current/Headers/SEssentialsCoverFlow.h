//
//  SEssentialsCarouselCoverFlow.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsCarousel.h"

/**
 An `SEssentialsCoverFlow` is an `SEssentialsCarousel` which displays all items in a line, either with horizontal or vertical alignment. Off-center items have a constant spacing between centers (`itemSpacing`), and the item in focus has padding around it (`centerItemPadding`). The items to the left of the center are rotated left, and the items to the right rotated right, with the center item facing forwards.

 <img src="../docs/Docs/img/CoverFlow.png" />
 
 
 The majority of interactions with the `SEssentialsCoverFlow` will be done via the methods and properties on the `SEssentialsCarousel` abstract base class, with the properties of the `SEssentialsCoverFlow` making small changes to the positioning of items.
  */
@interface SEssentialsCoverFlow : SEssentialsCarousel

/** If `SEssentialsCarouselOrientationHorizontal`, items are laid out left-to-right. If `SEssentialsCarouselOrientationVertical`, items are laid out top-to-bottom. Defaults to `SEssentialsCarouselOrientationHorizontal`.
 
    typedef enum
    {
        SEssentialsCarouselOrientationHorizontal,
        SEssentialsCarouselOrientationVertical
    } SEssentialsCarouselOrientation;
 */
@property (nonatomic, assign) SEssentialsCarouselOrientation orientation;

/** The amount of spacing to use between the centers of all off-center items, measured in points. Defaults to `65` pts. */
@property (nonatomic, assign) CGFloat itemSpacing;

/** The distance between the edge of the focused item, and the center of the nearest off-center item, measured in points. Defaults to `70` pts */
@property (nonatomic, assign) CGFloat centerItemPadding;

/** The scale of off-center items, in comparison to the center item, as a positive value. All off-center items will be the same scale, until moved to the center when they will scale to 1. Defaults to `0.9`. */
@property (nonatomic, assign) CGFloat scaleFactor;

/** The percentage time the focused item remains front-facing while the carousel is panning, before the item rotates away and moves out of focus.
 
 At `0`, the item will always rotate as the carousel pans, and at `1`, the item will not rotate until a new item is centered. Defaults to `0.2`. */
@property (nonatomic, assign) CGFloat frontFacingFactor;

/** The rotation applied to the off-center items.
 
 At `0`, all items face forwards. At `1`, all items are turned perpendicular to the viewer, facing inwards, and at `-1` all items are perpendicular facing outwards. Defaults to `0.3` */
@property (nonatomic, assign) CGFloat rotateFactor;

@end
