//
//  SEssentialsDecoratedCachedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsDecoratedView.h"

/**
 The `SEssentialsDecoratedCachedView` creates a view that is a cached image of the view supplied. This can often be used to improve the speed of a carousel since it only has to transform an image and not a complex view.

 To use a drop shadow effect without slowing down the framerate of a carousel use the following code in your datasource:
 
    UIView *original = [ [ UIView alloc] initWithFrame:frame ];
    original.layer.shadowRadius = 5;
    SEssentialsDecoratedCachedView *cachedView = [ [ SEssentialsDecoratedCachedView alloc ] initWithView:original andMargin:UIEdgeInsetsMake(5,5,5,5) ];
 
 __Note:__ complex effects that use layer masks or OpenGL will not be cached by this class.
 
 */
@interface SEssentialsDecoratedCachedView : SEssentialsDecoratedView

/** The image view which is formed from view supplied in the constructor. */
@property (nonatomic, readonly, retain) UIImageView *cachedView;

/** Create an `SEssentialsDecoratedView` from a view by capturing the image
 @param view The view to be cached
 @param margin How much to extend the view frame, typically to capture drop shadow.
 */
- (id)initWithView:(UIView*)view andMargin:(UIEdgeInsets)margin;

/** Refresh the cached image from the view
 */
-(void)refreshCachedView;

@end
