//
//  SEssentialsDecoratedFadedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsDecoratedView.h"

/**
 The `SEssentialsDecoratedShadowedView` applies a gradual fade towards the edges of the view.
 
 <img src="../docs/Docs/img/DecoratedFaded.png" />

 A faded view with default settings can be created, then further customized as;
 
    SEssentialsDecoratedFadedView *fadedView = [ [ SEssentialsDecoratedFadedView alloc ] initWithView:view ];
    fadedView.fadeLength = 20.f;
    fadedView.fadeAlpha = 0.2f;
 
 */
@interface SEssentialsDecoratedFadedView : SEssentialsDecoratedView

/** The length of the faded section, in points. Defaults to `50` pts. */
@property (nonatomic, assign) CGFloat fadeLength;

/** The alpha value of the fade at the edges, between `0` and `1`. Defaults to `0`. */
@property (nonatomic, assign) CGFloat fadeAlpha;

@end
