//
//  SEssentialsDecoratedReflectedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsDecoratedView.h"

/**
 The `SEssentialsDecoratedReflectedView` adds a reflection underneath the wrapped UIView.
 
 <img src="../docs/Docs/img/DecoratedReflective.png" />
 
 A reflected view with default settings can be created, then further customized as;
 
    SEssentialsDecoratedReflectedView *reflectedView = [ [ SEssentialsDecoratedReflectedView alloc ] initWithView:view ];
    reflectedView.offset = 10.f;
    reflectedView.scale = 0.8f;

 */
@interface SEssentialsDecoratedReflectedView : SEssentialsDecoratedView

/** The height of the view above its reflection, in points. Defaults to `0` pts. */
@property (nonatomic, assign) CGFloat offset;

/** How stretched or squashed the reflection is, in comparison to the original item. At `0.5`, the reflection would be half the height of the original view. Defaults to `1`. */
@property (nonatomic, assign) CGFloat scale;

/** How much of the reflection to display before fading to transparent, capped between `0` and `1`. At `0`, none of the reflection is shown, and at `1`, the fade gradient reaches the end of the view. Defaults to `0.2`. */
@property (nonatomic, assign) CGFloat fadeHeight;

@end
