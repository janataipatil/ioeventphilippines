//
//  SEssentialsDecoratedShadowedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsDecoratedView.h"

/**
 The `SEssentialsDecoratedShadowedView` adds an inner shadow effect on either side of the wrapped view.
 
 <img src="../docs/Docs/img/DecoratedShaded.png" />
 
 A shadowed view with default settings can be created, then further customized as;

    SEssentialsDecoratedShadowedView *shadowedView = [ [ SEssentialsDecoratedShadowedView alloc ] initWithView:view ];
    shadowedView.shadowAlpha = 0.9f;
    shadowedView.shadowLength = 4.f;

 */
@interface SEssentialsDecoratedShadowedView : SEssentialsDecoratedView

/** The length of the shadow, in points. Defaults to `8` pts. */
@property (nonatomic, assign) CGFloat shadowLength;

/** The alpha value of the shadow at the edges of the view, between `0` and `1`. Defaults to `0.5`. */
@property (nonatomic, assign) CGFloat shadowAlpha;

@end
