//  SEssentialsOverflowDropdownView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsTabOverflowProtocol.h"
#import "SEssentialsView.h"

@class SEssentialsTab;
@class SEssentialsTabbedView;

/**
 * When there are more tabs than can be rendered on screen this class
 * is called when the overflow dropdown button is pressed and can be overridden to control the style
 * of the drop down selector.
 */
@interface SEssentialsOverflowDropdownView : SEssentialsView<SEssentialsTabOverflowProtocol,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

/**
 * This property is an array of the tabs in the tabbed view. It is used to display the rows in the drop down table.
 */
@property (retain, nonatomic) NSArray *items;

/**
 * The source `SEssentialsTabbedView` is stored in this property.
 */
@property (assign, nonatomic) SEssentialsTabbedView *parentTabbedView;

/**
 * The list of tabs is displayed in this table.
 */
@property (retain, nonatomic) UITableView *tableView;


@end
