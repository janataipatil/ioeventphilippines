//
//  SEssentialsPullToAction.h
//  ShinobiEssentials
//
//  Created by Thomas Kelly on 19/02/2014.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsPullToActionState.h"
#import "SEssentialsPullToActionDelegate.h"
#import "SEssentialsPullToActionVisualizer.h"
#import "SEssentialsPullToActionStyle.h"
#import "SEssentialsPullToActionStatusView.h"
#import "SEssentialsPullToActionDefaultStatusView.h"
#import "SEssentialsPullToActionStatusViewAlignment.h"

/** The `SEssentialsPullToAction` control attaches to a scrollview, or any class which is a sub-class of `UIScrollView`. When the user pulls down the scroll view, the Pull to Action control is displayed. If the user pulls down the scroll view past a certain threshold, an action is triggered by the Pull to Action control.
 
 You can configure the look and feel of the control in a few ways. The control is provided with default behavior, where the control is displayed at the top of the scroll view. By default, the control status view will slide out aligned to the bottom of the control (`SEssentialsPullToActionStatusViewAlignmentBottom`), unless a different `statusViewAlignment` is specified. If you wish to tweak this default view, you can do this using the `style` property on the control. If you wish to display your own custom view when the Pull to Action control is displayed, you can set the `statusView` property on the control. If you do this, then modifying the `style` property will no longer have any effect.
 
 If the default visualization behavior of the control doesn't meet your needs, you can also set the `visualizer` property on the control. This allows you to configure exactly what is displayed in the control when it changes state, from being idle, to being pulled, to an action being triggered, and so on. This is advanced behavior, and will require more work on your part.
 */
@interface SEssentialsPullToAction : UIView

#pragma mark - Setup
/** @name Setup */

/** Create a Pull to Action control, and attach it to the given `UIScrollView`. Specify how the Pull to Action control is visualized.
 
 The possible values of `statusViewAlignment` are:
 
 - SEssentialsPullToActionStatusViewAlignmentStretch: The status view of the Pull to Action control stretches to fill the available space outside of the scroll view, keeping the contents of the status view centered.
 - SEssentialsPullToActionStatusViewAlignmentBottom: The status view of the Pull to Action control remains a constant size. It is anchored to the bottom edge of the control.
 */
-(instancetype)initWithScrollView:(UIScrollView *)scrollView statusViewAlignment:(SEssentialsPullToActionStatusViewAlignment)statusViewAlignment;

/** Create a Pull to Action control, and attach it to the given `UIScrollView`. */
-(instancetype)initWithScrollView:(UIScrollView *)scrollView;

/** Create a Pull to Action control with the given frame. Specify how the Pull to Action control is visualized.
 
 The possible values of `statusViewAlignment` are:
 
 - SEssentialsPullToActionStatusViewAlignmentStretch: The status view of the Pull to Action control stretches to fill the available space outside of the scroll view, keeping the contents of the status view centered.
 - SEssentialsPullToActionStatusViewAlignmentBottom: The status view of the Pull to Action control remains a constant size. It is anchored to the bottom edge of the control.
 */
-(instancetype)initWithFrame:(CGRect)frame statusViewAlignment:(SEssentialsPullToActionStatusViewAlignment)statusViewAlignment;

/** The `UIScrollView` that the Pull to Action is attached to.
 
 We only hold a weak reference, as we are retained by the scrollView as one of its subviews. */
@property (nonatomic, assign) UIScrollView *scrollView;

#pragma mark - Pull to Action State
/** @name Pull to Action State */

/** The current state of the Pull to Action. Defaults to `SEssentialsPullToActionStateIdle`.
 
 Possible states are:
 
 * SEssentialsPullToActionStateIdle: The Pull to Action is not being animated, or interacted with.
 * SEssentialsPullToActionStatePulling: The Pull to Action is being pulled down, but has not yet triggered an action.
 * SEssentialsPullToActionStateTriggered: The Pull to Action has been pulled down past the `pullThreshold`, and an action has been triggered.
 * SEssentialsPullToActionStateExecuting: The pull gesture on the control has finished, and the action is now executing.
 * SEssentialsPullToActionStateRetracting: The action has now completed, and the control is now returning to the idle state.
 */
@property (nonatomic, readonly) SEssentialsPullToActionState state;

/** The current distance that the Pull to Action has been pulled down by, in points. */
@property (nonatomic, readonly) CGFloat pulledAmount;

/** The distance at which an action is triggered, once the `pulledAmount` has exceeded this value.
 
 Defaults to `100` points.
 While the state of the control will change to `SEssentialsPullToActionStateTriggered` when this point is passed, the `pullToActionTriggeredAction:` method will only be called once the drag motion is released.
 If you wish to trigger as soon as the threshold is reached, you should instead use the `pullToAction:didChangeStateTo:` method, and fire on the state changing to `SEssentialsPullToActionStateTriggered`.
 */
@property (nonatomic, assign) CGFloat pullThreshold;

/** The height of the Pull to Action control during its executing state.
 
 Defaults to the height of the `statusView`.
 When the pull gesture ends after having passed the `pullThreshold`, the Pull to Action control will return to this height, until the `actionCompleted` method is called.
 */
@property (nonatomic, assign) CGFloat executingHeight;

#pragma mark - Delegation
/** @name Delegation */

/** The `delegate` for the Pull to Action control.
 
 Will be sent notifications about changes in state, and the triggering of actions. */
@property (nonatomic, assign) id<SEssentialsPullToActionDelegate> delegate;

/** The delegate should call this once it has completed its action. This tells the Pull to Action control to retract back to its idle state. */
- (void)actionCompleted;

#pragma mark - Basic Visual Properties
/** @name Basic Visual Properties */

/** Updates the style of the component using the specified theme.
 */
-(void)applyTheme:(SEssentialsTheme *)theme;

/** By default, the control displays a default status view. This property allows you to configure how that default view looks.
 
 @warning Setting the style will cause the component to apply the style when the component is next rendered.
 */
@property (nonatomic, retain) SEssentialsPullToActionStyle *style;


/** The status view which is displayed when you interact with the Pull to Action control.
 
 This defaults to a view with an arrow and text showing the status of the control. The default view can be configured with the `style` property. You can set this view if you want to display your own custom view in the control.
 */
@property (nonatomic, retain) UIView<SEssentialsPullToActionStatusView> *statusView;


#pragma mark - Advanced Visual Properties
/** @name Advanced Visual Properties */

/** Handles the visualization of the Pull to Action control.
 
 This defaults to a visualizer which anchors the status view to the bottom of the Pull to Action control.
 
 You can provide your own visualizer to handle the look and feel of the Pull to Action control. The visualizer is notified of any changes in state, along with changes to the `pulledAmount` property on the control.
 */
@property (nonatomic, retain) id<SEssentialsPullToActionVisualizer> visualizer;

/** This is an optional method you can use to tell the control to execute its action if a custom `visualizer` was provided.
 
 By default, this is called internally when the control moves into its executing state. If the control state is not `SEssentialsPullToActionStateExecuting`, calling this method will have no effect.
 
 If the `visualizer` is set to a custom `SEssentialsPullToActionVisualizer`, then it will need to call `executeAction` to make the Pull to Action control begin the action and change state. Ideally, this would be in the visualizer's implementation of `pullToActionTriggeredAction:`.
 */
- (void)executeAction;

/** This is an optional method you can use to tell the control that the animation back to the idle state has completed, if a custom `visualizer` was provided.
 
 By default, this will call down to the `statusView` to inform it that it is safe to return to its idle state.
 
 If the `visualizer` is set to a custom `SEssentialsPullToActionVisualizer`, then it will need to call `resetToIdle` in `pullToActionActionCompleted:`.
 */
- (void)resetToIdle;


@end
