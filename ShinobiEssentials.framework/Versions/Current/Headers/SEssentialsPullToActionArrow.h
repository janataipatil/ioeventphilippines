//
//  SEssentialsPullToActionArrow.h
//  ShinobiEssentials
//
//  Created by Jan Akerman on 12/03/2014.
//
//

#import <UIKit/UIKit.h>

@class SEssentialsPullToActionStyle;

/** A `UIView` which draws an arrow spanning its entire bounds.
 */
@interface SEssentialsPullToActionArrow : UIView

#pragma mark - Setup
/** @name Setup */

/** Initializes a view which renders an arrow object between the two points provided.
 
 @param frame The frame of the view.
 @param tailPoint The point that the tail of the arrow is rendered to.
 @param headPoint The point that the head of the arrow is rendered to.
 @param headSize The width and height of the arrows head.
 @param tailWidth The width of the tail.
 @param borderWidth The width of the arrows border.
 */
-(instancetype)initWithFrame:(CGRect)frame tailPoint:(CGPoint)tailPoint headPoint:(CGPoint)headPoint headSize:(CGSize)headSize tailWidth:(CGFloat)tailWidth borderWidth:(CGFloat)borderWidth;

#pragma mark - Size and Position
/** @name Size and Position */

/** The point that the tail of the arrow is rendered to. */
@property (nonatomic, assign) CGPoint tailPoint;

/** The point that the head of the arrow is rendered to. */
@property (nonatomic, assign) CGPoint headPoint;

/** The width and height of the arrows head. */
@property (nonatomic, assign) CGSize headSize;

/** The width of the tail. */
@property (nonatomic, assign) CGFloat tailWidth;

#pragma mark - Style
/** @name Style */

/** The color of the arrow. */
@property (nonatomic, retain) UIColor *color;

/** The border color of the arrow. */
@property (nonatomic, retain) UIColor *borderColor;

/** The width of the arrow's border. */
@property (nonatomic, assign) CGFloat borderWidth;

/** Apply the given style to the arrow.
 */
-(void)applyStyle:(SEssentialsPullToActionStyle *)style;

@end
