//
//  SEssentialsPullToActionStatusView.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 25/02/2014.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsPullToActionStatusView.h"

/** A default implementation of the `SEssentialsPullToAction`'s status view.
 */
@interface SEssentialsPullToActionDefaultStatusView : UIView <SEssentialsPullToActionStatusView>

/** A label showing the current status of the Pull to Action control.
 */
@property (nonatomic, retain) UILabel *statusLabel;

/** An icon to be displayed when the status view is being pulled.
 
 By default, this is an `SEssentialsPullToActionArrow` which rotates as the Pull to Action control nears its `pullThreshold`.
 
 @warning Override the method `updateForState:` to customize when the pulling icon shows/hides.
 */
@property (nonatomic, retain) UIView *pullingIcon;

/** The status view associates each state of the Pull to Action control with some text. 
 
 This text is displayed in the status label when the Pull to Action control is in that state.
 The default mappings are:
 
    SEssentialsPullToActionStateIdle => @"Idle"
    SEssentialsPullToActionStatePulling => @"Pulling"
    SEssentialsPullToActionStateRetracting => @"Retracting"
    SEssentialsPullToActionStateTriggered => @"Triggered"
    SEssentialsPullToActionStateExecuting => @"Executing"
 */
@property (nonatomic, retain) NSDictionary *textForStates;

/** The activity indicator which is displayed when the action is executing.
 
 By default, this is an instance of `UIActivityIndicator`.
  
 @warning Override the method `updateForState:` to customize when the activity indicator shows/hides.
 */
@property (nonatomic, retain) UIView *activityIndicator;

@end
