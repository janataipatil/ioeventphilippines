//
//  SEssentialsPullToActionState.h
//  ShinobiEssentials
//
//  Created by Thomas Kelly on 20/02/2014.
//
//

#import <Foundation/Foundation.h>


/** The possible states of the Pull to Action control.
 
 * SEssentialsPullToActionStateIdle: The Pull to Action control is not being animated, or interacted with.
 * SEssentialsPullToActionStatePulling: The Pull to Action control is being pulled down, but has not yet triggered an action.
 * SEssentialsPullToActionStateTriggered: The Pull to Action control has been pulled down past the `pullThreshold`, and an action has been triggered.
 * SEssentialsPullToActionStateExecuting: The pull gesture on the control has finished, and the action is now executing.
 * SEssentialsPullToActionStateRetracting: The action has now completed, and is now returning to the idle state.
 */
typedef NS_ENUM(NSUInteger, SEssentialsPullToActionState)
{
    SEssentialsPullToActionStateIdle,
    SEssentialsPullToActionStatePulling,
    SEssentialsPullToActionStateTriggered,
    SEssentialsPullToActionStateExecuting,
    SEssentialsPullToActionStateRetracting
};
