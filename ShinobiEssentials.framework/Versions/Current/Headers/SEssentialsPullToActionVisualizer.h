//
//  SEssentialsPullToActionVisualizer.h
//  ShinobiEssentials
//
//  Created by Daniel Gorst on 24/02/2014.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "SEssentialsPullToActionDelegate.h"
#import "SEssentialsPullToActionStatusView.h"

/** The `SEssentialsPullToActionVisualizer` protocol defines the messages sent to a status view from it's parent `SEssentialsPullToAction` control.
 
 The visualizer is responsible for forwarding messages to the status view regarding state changes, handing positioning / size changes,
 and taking care of animation.
 */
@protocol SEssentialsPullToActionVisualizer <NSObject, SEssentialsPullToActionDelegate>

@required

/** Called by the Pull to Action control when the pulled amount is changed.
 
 Use this opportunity to reposition / resize the Pull to Action control and notify the status view of the size change.
 */
- (void)pullToAction:(SEssentialsPullToAction*)pullToAction pulledAmountChanged:(CGFloat)pulledAmount;

/** Called by the Pull to Action control when a new status view is set.
 
 Use this opportunity to position, set autoresizing masks, or any other initial setup that the status view may need.
 */
- (void)pullToAction:(SEssentialsPullToAction*)pullToAction layoutStatusView:(UIView<SEssentialsPullToActionStatusView> *)statusView;

/** Called by the Pull to Action control when its action is completed.
 
 Use this opportunity to trigger an animation after the pull action is completed.
 */
- (void)pullToActionActionCompleted:(SEssentialsPullToAction*)pullToAction;

@end
