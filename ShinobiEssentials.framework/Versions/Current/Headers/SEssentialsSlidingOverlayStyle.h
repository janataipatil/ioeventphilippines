//
//  SEssentialsSlidingOverlayStyle.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsStyle.h"
#import "SEssentialsSlidingOverlayImagesWrapper.h"

/**
 The `SEssentialsSlidingOverlayStyle` defines the look and feel for an instance of the `SEssentialsSlidingOverlay` class.  To modify the look of the sliding overlay from its default settings, update properties on this style object.
 
 In the style, you can customize the following aspects of a sliding overlay:
 
 - The overlay. This is the main UIView in the sliding overlay. You can slide it to the side to expose the underlay beneath it.
 - The underlay. This is the view which is usually hidden under the overlay. You can slide the overlay to the side to reveal it.
 - The toolbar.
 - The decoration which is applied to the sliding overlay.  This is described in more detail below.
 
 `SEssentialsSlidingOverlayStyle` derives from `SEssentialsStyle`, and so it is initialized with a `SEssentialsTheme` object.  The theme provides the default settings for the style.
 
 The following default settings are taken from the theme:
 
 - The `primaryTintColor` on the theme is used for the overlay (`overlayTintColor`) and the toolbar (`toolbarTintColor`).
 - The `primaryTexture` on the theme is used for the background texture of the overlay (`overlayTexture`) and the toolbar (`toolbarTexture`).
 
 - The `secondaryTintColor` on the theme is used for the underlay (`underlayTintColor`).
 - The `secondaryTexture` on the theme is used for the background texture of the underlay (`underlayTexture`).
 
 - The `tertiaryTintColor` on the theme is used for the toolbar button (`buttonTintColor`).
 
 Decoration is applied to the sliding overlay.  This refers to the shine which is applied to the toolbar, the chisel which is drawn between the toolbar and the overlay, and the drop shadow which is drawn under the overlay onto the underlay.  The theme provides the following default settings for decoration:
 
 - The `shadowColor` and `shadowDepth` properties on the theme map to the properties with the same name on the style.
 - The `shineColor` on the theme sets the color of the shine on the toolbar (`shineColor`).
 - The `primaryDecorationTintColor` and the `secondaryDecorationTintColor` set the primary and secondary colors for the chisel between the toolbar and the overlay (`chiselPrimaryColor` and `chiselSecondaryColor`).
 */
@interface SEssentialsSlidingOverlayStyle : SEssentialsStyle

#pragma mark - Initialization
/** @name Initialization */

/**
 * Constructor with default button styles and a default theme.
 */
- (id)init;

/**
 * Constructor with default button styles.
 */
- (id)initWithTheme:(SEssentialsTheme *)theme;

/**
 * Constructor that overrides the default button images and the default image mask.
 
 DEPRECATED: Use `initWithTheme:customImages:` instead.
 */
- (id)initWithButtonImage:(UIImage *)buttonImage
 buttonPressedImage:(UIImage *)buttonPressedImage
         buttonMask:(UIImage *)buttonMask DEPRECATED_ATTRIBUTE;

/** Constructor that overrides the default button images and masks. See the `SEssentialsSlidingOverlayImagesWrapper` for more details. */
-(id)initWithTheme:(SEssentialsTheme *)theme customImages:(SEssentialsSlidingOverlayImagesWrapper*)customImages;

#pragma mark Overlay
/** @name Overlay */

/**
 * The texture used for the overlay.  This defaults to the `primaryTexture` from the theme.
 */
@property (nonatomic,retain) UIColor *overlayTexture;

/**
 * The color of the overlay.  This defaults to the `primaryTintColor` from the theme.
 */
@property (nonatomic,retain) UIColor *overlayTintColor;

#pragma mark Underlay
/** @name Underlay */

/**
 * The texture used for the underlay.  This defaults to the `secondaryTexture` from the theme.
 */
@property (nonatomic,retain) UIColor *underlayTexture;

/**
 * The color of the underlay.  This defaults to the `secondaryTintColor` from the theme.
 */
@property (nonatomic,retain) UIColor *underlayTintColor;

/**
 * The depth of the shadow above the underlay.  Defaults to the `shadowDepth` of the theme.
 */
@property (nonatomic,assign) CGFloat shadowDepth;

/**
 * The color of the shadow above the underlay.  Defaults to the `shadowColor` of the theme.
 */
@property (nonatomic, retain) UIColor *shadowColor;

#pragma mark Toolbar
/** @name Toolbar */

/**
 * The texture used for the toolbar.  This defaults to the `primaryTexture` from the theme.
 */
@property (nonatomic,retain) UIColor *toolbarTexture;

/**
 * The color of the toolbar.  This defaults to the `primaryTintColor` from the theme.
 */
@property (nonatomic,retain) UIColor *toolbarTintColor;

/**
 * The color of the main button on the toolbar.  This defaults to the `tertiaryTintColor` from the theme.
 */
@property (nonatomic,retain) UIColor *buttonTintColor;

/** DEPRECATED: This property is no longer used.  If you would like to configure the shine, chisel or shadow on the control, you can use the specific properties which relate to those elements of the control, such as `shineColor`, `shadowColor`, `chiselPrimaryColor`, etc.
 */
@property (nonatomic,assign) BOOL decoration DEPRECATED_ATTRIBUTE;

/** The color of the shine layer above the toolbar. Defaults to the `shineColor` of the theme. */
@property (nonatomic, retain) UIColor *shineColor;

/** The top color of the chisel.  Defaults to the `primaryDecorationTintColor` of the theme. */
@property (nonatomic, retain) UIColor *chiselPrimaryColor;

/** The bottom color of the chisel.  Defaults to the `secondaryDecorationTintColor` of the theme. */
@property (nonatomic, retain) UIColor *chiselSecondaryColor;

/**
 The padding around the main button in the toolbar.  This property defaults to a value of `6` points.
 */
@property (nonatomic,assign) CGFloat toolbarButtonPadding;

/**
 * The image which is displayed for the main button on the toolbar.  To customize this image, you can specify the image when you create the style.
 */
@property (nonatomic, retain, readonly) UIImage *buttonImage;

/**
 * The image which is displayed for the main button on the toolbar when it is pressed.  To customize this image, you can specify the image when you create the style.
 */
@property (nonatomic, retain, readonly) UIImage *buttonPressedImage;

/**
 * The image mask which is used for the main button on the toolbar.  To customize this mask, you can specify the mask to use when you create the style.
 */
@property (nonatomic, retain, readonly) UIImage *buttonMask;


@end
