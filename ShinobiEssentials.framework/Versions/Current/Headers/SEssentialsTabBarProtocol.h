//
//  SEssentialsTabBarProtocol.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class SEssentialsTabbedView;
@class SEssentialsTab;

/**
 * `SEssentialsTabBarProtocol` is a protocol defining the `SEssentialsTabbedView` interaction with the tabs in the tab area. Implementations of this can provide
 * a variety of layout formats e.g. scrollable tabs, dropdown tabs. A concrete implementation of this protocol is supplied in
 * `SEssentialsScrollableTabBar`.
 */
@protocol SEssentialsTabBarProtocol <NSObject>

/**
 * Add a tab that will be rendered on screen. The rendered view will be an `SEssentialsTabHeaderView` found on the property of the tab at `tab.tabHeaderView`.
 * The added tab should start in the inactive state. If the `SEssentialsTabBarProtocol` implementation cannot fit this new tab on screen it can do
 * nothing as the `SEssentialsTab` will be managed by the `SEssentialsTabbedView` until it becomes active.
 */
- (void) addTab:(SEssentialsTab *)tab;

/**
 * Add a tab that will be rendered on screen at a given index. The rendered view will be an `SEssentialsTabHeaderView` found on the property of the tab at `tab.tabHeaderView`.
 * The added tab should start in the inactive state. If the `SEssentialsTabBarProtocol` implementation cannot fit this new tab on screen it can do
 * nothing as the `SEssentialsTab` will be managed by the `SEssentialsTabbedView` until it becomes active.
 */
- (void) addTab:(SEssentialsTab *)tab atIndex:(NSInteger)index;

/**
 * Remove a tab. If the removed tab was off screen then no action is required. If the tab was on screen then ensure that 'allDisplayedTabs' 
 * is maintained correctly. In some implementations this may result in a previously off screen tab becoming on screen to fill the gap left 
 * by the removed tab.
 */
- (void) removeTab:(SEssentialsTab *)tab;

/**
 * Activate a tab (typically this will highlight the tab by calling the `SEssentialsTabbedView` activateTab method). Depending on the implementation of the `SEssentialsTabBarProtocol` this
 * should also move a previously off screen tab to be on screen.
 */
- (void) activateTab:(SEssentialsTab *)tab;

/**
 * Move a tab to a destination index. For some implementations this may mean that the tab will no longer be on screen
 * in which case the tab should be removed and the next available tab moved in.
 */
- (void) moveTab:(SEssentialsTab *)tab toIndex:(NSInteger)destinationIndex;

/**
 * This should return an array of all the tabs displayed in the tab bar. This is used to compute which tabs to show in the
 * overflow dropdown `SEssentialsOverflowDropdownView`.
 */
- (NSArray *) allDisplayedTabs;

/**
 * This is called when the user sets the editable flag on `SEssentialsTabbedView`. The implementation should not allow tabs to be removed
 * or reordered when `NO`. When it is `YES` a typical implementation of `SEssentialsTabHeaderView` will show a delete button (icon) that should
 * be configured to handle presses and call back to the `SEssentialsTabbedView` with removeTab. 
 */
- (void) setEditable:(BOOL)editable;

/**
 * This is called when the tab bar is added to the `SEssentialsTabbedView`. Any initialization of graphics should occur in this call.
 */
- (void) setTabbedView:(SEssentialsTabbedView *)tabbedView;

@optional

/**
 * Called to indicate the tab bar has changed in some way. Each tab should then be updated to reflect this.
 */
-(void)reloadTabs;

@end
