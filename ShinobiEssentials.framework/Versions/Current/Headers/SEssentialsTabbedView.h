//  SEssentialsTabbedView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "SEssentialsTabbedViewDelegate.h"
#import "SEssentialsTabBarProtocol.h"
#import "SEssentialsTab.h"
#import "SEssentialsTabbedViewDatasource.h"
#import "SEssentialsTabbedViewStyle.h"
#import "SEssentialsTabOverflowProtocol.h"
#import "SEssentialsOverflowDropdownView.h"
#import "SEssentialsScrollableTabBar.h"
#import "SEssentialsCommon.h"

typedef NS_ENUM(NSInteger, SEssentialsTabBarPosition)
{
    SEssentialsTabBarPositionTop,
    SEssentialsTabBarPositionBottom
};

/**
 The `SEssentialsTabbedView` is a `UIView` which comprises a set of subviews, only one
 of which is visible at any given time. Navigating between these subviews is achieved
 via a horizontal tab bar.
 
 The `SEssentialsTabbedView` requires a `datasource` - an object which implements the
 `SEssentialsTabbedViewDataSource` protocol. This will provide the content view
 associated with each tab as the user activates them in turn.
 
 ### Code Sample
 
 The code below shows how to create a simple tabbed view, with 2 tabs:
 
    SEssentialsTabbedView *tabbedView = [[[ SEssentialsTabbedView alloc] initWithFrame:frame] autorelease];
    tabbedView.dataSource = self;
    [tabbedView addTab:[[[ SEssentialsTab alloc] initWithName:@"tab1" icon:nil] autorelease]];
    [tabbedView addTab:[[[ SEssentialsTab alloc] initWithName:@"tab2" icon:nil] autorelease]];
 
 You also need to implement the following datasource method:
 
    - (UIView *)tabbedView:(SEssentialsTabbedView *)tabbedView contentForTab:(SEssentialsTab *)tab {
        // Create and return a UIView appropriate for the given tab here...
    }
 
 
 ### Further Details

 The `SEssentialsTabbedView` can be used to hold several `UIViews` that the user can
 switch between by touching the tabs. The tabbed view can hold many tabs
 and these appear in the tab bar. The tab bar is an implementation of the protocol
 `SEssentialsTabBarProtocol` and manages the display of multiple tabs where only one tab
 can be active at an one time.  Each tab is an instance of `SEssentialsTab`.
 
 - __SEssentialsTab__ A `SEssentialsTab` object holds information about the tab
 including its name and a representative icon.  Importantly, it does not hold any
 reference to the view associated with the tab.  This allows the tab to be a
 relatively lightweight object.  This also means that should you require access to the content
 of the tabs at any point you should keep a reference to them.  The associated view is supplied by the tabbed view's
 datasource. Only one `SEssentialsTab` can be active at one time and the tabs can be added, reordered and removed
 when in edit mode.
 
 - __Tab header views__ Each tab contains a header view, which adopts the `SEssentialsTabHeaderViewProtocol`.  The header view controls the appearance of the tab.  By default, the tab header view will be an instance of `SEssentialsTabHeaderView`.  You can create your own implementation of the header view protocol if you require different appearance or behavior from what this provides.
 
 Tabs appear in the order in which they were added but this ordering can be
 changed when the tabbed view is in edit mode. Tabs can be moved by dragging
 them in the tab area and can also be removed by touching the cross symbol to
 close a tab. 
 
 Callbacks are generated when the tabs are edited and these can be
 handled by implementing the `SEssentialsTabbedViewDelegate`. The implementer may
 provide various ways to add new tabs (e.g. browser long tap on links) but the
 tabbed view can also show a 'new tab' button. When this button is enabled the
 delegate will receive a callback when it is pressed and the implementer can
 provide a default tab and view.
 
 ### Styling
 
 The `SEssentialsTabbedView` has a style object, which is an instance of `SEssentialsTabbedViewStyle`.
 This manages the look and feel of the control by setting things like the background color and background
 texture of the tab bar. The style object should always be used to update the look of the control,
 rather than accessing the tabbed view and setting its properties directly.
 
 When using the default implementation supplied, the style has precedence over any visual changes which
 are made to the tabbed view directly. For example, if you were to set a property such as the background
 color on a `SEssentialsTabHeaderView` object itself, this change will be overridden the next time the style is updated.
 That is why it is important to use the style to manage the look and feel of the control.
 
 If a style change will result in the tabs changing size, such as updating the font or title of a tab, the tab bar must be updated by calling `reloadTabs`.
 
 @sample TabbedViewGettingStarted
 @sample TabbedViewAddRemoveTabs
 
 */

@interface SEssentialsTabbedView : UIView

/** @name Initializing an SEssentialsTabbedView */

/**
 * Initialize view with the specified frame rectangle. The `tabBarPosition` defaults to `SEssentialsTabBarPositionTop` and the 
 * `tabBarView` is initialized with a default `SEssentialsScrollableTabBar` that allows for multiple tabs that can be scrolled.
 *
 * @param frame rectangle defining views extent
 */
- (id)initWithFrame:(CGRect)frame;

/**
 Initialize view with the specified frame rectangle and tab bar position.
 
 @param frame rectangle defining views extent
 @param tabBarPosition the position of the tab bar within the frame
 
    typedef enum
    {
      SEssentialsTabBarPositionTop,
      SEssentialsTabBarPositionBottom
    } SEssentialsTabBarPosition;
 
 */
- (id)initWithFrame:(CGRect)frame tabBarPosition:(SEssentialsTabBarPosition) tabBarPosition;

/**
 Initialize view with the specified frame rectangle, tab bar position and style.
 
 @param frame rectangle defining views extent
 @param tabBarPositionValue the position of the tab bar within the frame
 @param style `SEssentialsTabbedViewStyle` derived object

     typedef enum
     {
       SEssentialsTabBarPositionTop,
       SEssentialsTabBarPositionBottom
     } SEssentialsTabBarPosition;
 
 */
- (id)initWithFrame:(CGRect)frame tabBarPosition:(SEssentialsTabBarPosition)tabBarPositionValue style:(SEssentialsTabbedViewStyle *)style;

/** @name Managing tabs */

/**
 * Adds a new tab to the tab bar, initially in an inactive state.
 * An `SEssentialsTab` should be allocated and initialized with a name and icon for use in this call. The content associated with the `SEssentialsTab`
 * is provided by the datasource delegate (typically this association is made prior to the addTab call). The added tab will appear at the end
 * (to the right) of all other tabs. If the added tab is to become immediately active then you should call `activateTab` after its addition.
 *
 * @param tab the tab that is to be added
 */
-(void) addTab:(SEssentialsTab *)tab;

/**
 * Adds a new tab to the tab bar, initially in an inactive state.
 * An `SEssentialsTab` should be allocated and initialized with a name and icon for use in this call. The content associated with the `SEssentialsTab`
 * is provided by the datasource delegate (typically this association is made prior to the `addTab` call). The added tab will appear at the index provided.
 * If the added tab is to become immediately active then you should call `activateTab` after its addition.
 *
 * @param tab the tab that is to be added
 * @param index the index at which to add the tab
 */
-(void) addTab:(SEssentialsTab *)tab atIndex:(NSInteger)index;

/**
 * Remove a tab. This removes the tab from the tab bar and if its content view is active that is removed from the `contentViewArea`.
 * The next tab is then made active causing its content view to be displayed.
 *
 * @param tab the tab to be removed
 */
-(void) removeTab:(SEssentialsTab *)tab;

/**
 * Remove a tab at a given display index. This removes the tab from the tab bar and if its content view is active that is removed from the `contentViewArea`.
 * The next tab is then made active causing its content view to be displayed.
 * 
 * @param index the index of the tab to remove
 */
-(void) removeTabDisplayedAtIndex:(NSInteger)index;

/**
 * Activate a tab at a given display index. This results in the tab at the given index becoming rendered in the active state in the tab bar and its associated content view
 * being loaded into the `contentViewArea`. If a different tab was previously active it will become rendered inactive in the tab bar
 * and its associated content will be removed from the `contentViewArea`.
 * 
 * @param index the index of the tab to be activated
 */
-(void) activateTabDisplayedAtIndex:(NSInteger)index;

/**
 * Activate a tab. This results in the tab becoming rendered in the active state in the tab bar and its associated content view
 * being loaded into the `contentViewArea`. If a different tab was previously active it will become rendered inactive in the tab bar
 * and its associated content will be removed from the `contentViewArea`.
 * 
 * @param tab the tab to be activated
 */
-(void) activateTab:(SEssentialsTab *)tab;

/**
 * Moves a tab programmatically. If it is necessary to change the order of the tabs as displayed on screen then this interface allows
 * tabs to be moved.
 *
 * @param tab the tab to be moved
 * @param destinationIndex the index to which the tab is to me be moved
 */
-(void) moveTab:(SEssentialsTab *)tab toIndex:(NSInteger)destinationIndex;

/** @name Retrieve information */

/**
 * When a view is created to be associated with an `SEssentialsTab` this method provides the bounds of the view where it will be displayed. 
 * If a view is created with different bounds then it will be resized when shown.
 */
@property (readonly, nonatomic) CGRect contentViewBounds;

/**
 * All tabs (including off screen ones) in display order. This returns an array of `SEssentialsTab` where the order reflects the order the tabs 
 * are displayed on screen in the tab bar.
 */
@property (readonly, nonatomic) NSArray *allTabs;

/** @name Properties */

/**
 Tabs are displayed at the top of the view by default but can be changed in the constructor. This readonly property reflects that position
 
    typedef enum
    {
      SEssentialsTabBarPositionTop,
      SEssentialsTabBarPositionBottom
    } SEssentialsTabBarPosition;
 
 */

@property (readonly, nonatomic) SEssentialsTabBarPosition tabBarPosition;

/**
 * The currently active (displayed) tab. The active tab changes when the user selects a tab by tapping the `SEssentialsTabbedView` and
 * the active tab may also change by being set programmatically or change after a tab is removed. This readonly property 
 * returns the currently active tab or nil if no tabs have yet been added to the control.
 */
@property (readonly, nonatomic) SEssentialsTab *activeTab;

/**
 * The tabbed area can be reimplemented by using the `SEssentialsTabBarProtocol` protocol. If a customized version of the tab area is required,
 * derive a class from `UIView` that implements `SEssentialsTabBarProtocol` and assign it to this property. A default implementation
 * is supplied in `SEssentialsScrollableTabBar`.
 */
@property (assign, nonatomic) UIView <SEssentialsTabBarProtocol> *tabBarView;

/**
 The area in which the content of a tab will be displayed.  You can customize this if you want to style all your tabs to have a particular appearance. 
 */
@property (nonatomic, retain, readonly) UIView *contentViewArea;

/**
 * Delegate to control appearance and interaction. See `SEssentialsTabbedViewDelegate` for full details of all delegate calls that
 * are generated by this view.
 */
@property (assign, nonatomic) id<SEssentialsTabbedViewDelegate> delegate;

/**
 * Datasource to provide views for tabs. See `SEssentialsTabbedViewDataSource` for full details on the required delegate
 * that must be supplied for the `SEssentialsTabbedView` to function.
 */
@property (assign, nonatomic) id<SEssentialsTabbedViewDataSource> dataSource;

/**
 * Assign to this delegate to change the behavior of the drop down list that
 * is used to select tabs when there are more than can be displayed on screen.
 */
@property (retain,nonatomic) UIView <SEssentialsTabOverflowProtocol> *tabOverflowDropdown;

/**
 * A subclass of `SEssentialsTabbedViewStyle` can be assigned to the constructor to provide a customized tab style
 */
@property (readonly, retain, nonatomic) SEssentialsTabbedViewStyle *style;

/**
 * Controls the availability of the 'new tab' button on the tab bar. The callback associated by the 'new tab' button 
 * is contained within the `SEssentialsTabbedViewDataSource` and must be implemented to create new tabs when the button is clicked.
 * Defaults to `NO`.
 */
@property (assign, nonatomic) BOOL hasNewTabButton;


/**
 * Controls the availability of the 'overflow dropdown' button. The 'overflow dropdown' button is displayed when all tabs do not fit
 * on the tab bar and when pressed displays a drop down list of all tabs. Tabs can be selected from this drop down list.
 * Defaults to `YES`.
 */
@property (assign,nonatomic) BOOL hasOverflowDropdown;

/**
 * When `YES` the tab width is sized to fit the text it contains. Defaults to `YES`.
 */
@property (assign, nonatomic) BOOL resizeToText;

/**
 * Determines if tabs can be edited after creation. When `YES` tabs can be reordered and removed.
 * When this property has value `NO` the control contains a static list of tabs that cannot be reordered but can
 * be activated by touching them. Defaults to `YES`. 
 */
@property (assign, nonatomic) BOOL editable;

/** Applies the specified theme to the tabbed view. */
- (void)applyTheme:(SEssentialsTheme*)theme;

@end
