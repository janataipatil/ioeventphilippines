//
//  SEssentialsTabbedViewDatasource.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SEssentialsTabbedView;
@class SEssentialsTab;

/**
 The `SEssentialsTabbedViewDataSource` protocol is adopted by an object which provides content for a `SEssentialsTabbedView` control.
 
 When a tab is activated the datasource is called to retrieve the associated view. The view is only retained by `SEssentialsTabbedView` for the duration that the tab is active. The responsibility for the memory management of the view associated with a tab is with the datasource and is typically implemented by storing the view in a `NSDictionary` that maps `SEssentialsTab` -> `UIView`.
 */
@protocol SEssentialsTabbedViewDataSource <NSObject>

/**
 * This is called before the view associated with the tab becomes active and this
 * must return the view associated with the tab supplied. The view is only retained while
 * the tab is active and the client application must retain it if it is to be shown the next time
 * the tab becomes active. 
 *
 * @param tabbedView the `SEssentialsTabbedView` that contains the tab
 * @param tab the `SEssentialsTab` that will become active
 * @return the `UIView` that will be shown when the tab becomes active
 */
-(UIView *) tabbedView:(SEssentialsTabbedView *)tabbedView contentForTab:(SEssentialsTab *)tab;

@optional

/**
 * This is called when a user clicks the 'add tab' button on a tabbed view.
 * The implementation should create and return a new `SEssentialsTab` with suitable
 * default fields and add an associated content view to the datasource.
 * If this is not implemented the 'add tab' button will have no action.
 *
 * @param tabbedView the `SEssentialsTabbedView` that is requesting the tab
 * @return the `SEssentialsTab` that will be added to the tabbed view.
 */
-(SEssentialsTab *) tabForTabbedView:(SEssentialsTabbedView *)tabbedView;

@end
