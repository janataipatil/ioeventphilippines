//
//  SEssentialsTabbedViewStyle.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import "SEssentialsStyle.h"
#import "SEssentialsTabbedViewCustomImagesWrapper.h"

/**
 The `SEssentialsTabbedViewStyle` defines the look and feel for an instance of the
 `SEssentialsTabbedView` class.  If you wish to update how a tabbed view looks, you should update its style object.
 
 `SEssentialsTabbedViewStyle` derives from `SEssentialsStyle`, and so it is initialized with an
 `SEssentialsTheme` object. The theme provides the default settings for the style.
 
 The following default settings are provided by the theme:
 
 - The `primaryTintColor` on the theme is used for the background color of the tab header views (`tintColor`).
 - The `secondaryTintColor` on the theme is used for the background color of the tab bar (`tabBarBackgroundColor`).
 - The `secondaryTexture` on the theme is used for the background texture of the tab bar (`tabBarBackgroundTexture`).
 - The `primaryTextColor` on the theme is used for text in active tab header views (`defaultFontColor`).
 - The `secondaryTextColor` on the theme is used as the color for text in inactive tab header views (`alternativeFontColor`).
 
 Decoration can be applied to the tabbed view.  When decoration is applied, the following effects are added to the control:
 
 - The tab header views are moved so they overlap slightly with each other.  When no decoration is applied, the header views will line up without any overlap.
 - The look of the overflow dropdown menu is changed.  A translucent border is drawn around the menu, and a drop shadow is drawn beneath the menu.
 
 The `decoration` property controls whether decoration is applied to the control.
 */
@interface SEssentialsTabbedViewStyle : SEssentialsStyle

#pragma mark - Tab Header View Properties
/**
 @name Tab Header View Properties
 */

/**
 The tint color of the tab header views. This defaults to the `primaryTintColor` on the theme.
 */
@property (nonatomic, retain) UIColor *tintColor;

/**
 * The default width of a tab. This will be used when `resizeToText` is set to `NO`.
 */
@property (assign, nonatomic) CGFloat tabWidth;

/**
 * When `resizeToText` is set to `YES` this acts as an upper limit on the width of the tab.
 */
@property (assign, nonatomic) CGFloat maximumTabWidth;

/**
 * The default font used on the tab. This defaults to the `secondaryFont` on the theme.
 */
@property (retain, nonatomic) UIFont *defaultFont;

/**
 * The default font used on the overflow picker. This defaults to the `primaryFont` on the theme.
 */
@property (retain, nonatomic) UIFont *defaultOverflowFont;

/**
 * The default font color used on the active tab. This defaults to the `primaryTextColor` on the theme.
 */
@property (retain, nonatomic) UIColor *defaultFontColor;

/**
 * The default font color used on the inactive tabs. This defaults to the `secondaryTextColor` on the theme.
 */
@property (retain, nonatomic) UIColor *alternativeFontColor;

/** Whether to apply decoration to the control.  
 
 When decoration is applied, the following effects are added to the control:
 
 - The tab header views are moved so they overlap slightly with each other.  When no decoration is applied, the header views will line up without any overlap.
 - The look of the overflow dropdown menu is changed.  A translucent border is drawn around the menu, and a drop shadow is drawn beneath the menu.  
 
 If `elementStyle` on the theme is set to `DECORATED`, this defaults to `YES`. Otherwise, this defaults to `NO`. */
@property (nonatomic, assign) BOOL decoration;

#pragma mark - Tab Bar Properties
/**
 @name Tab Bar Properties
 */

/**
 The color of the background to the tab bar. This defaults to the `secondaryTintColor` on the theme.
 */
@property (nonatomic, retain) UIColor *tabBarBackgroundColor;

/**
 The texture of the background to the tab bar. This defaults to the `secondaryTexture` on the theme.
 */
@property (nonatomic, retain) UIColor *tabBarBackgroundTexture;

/**
 * The height of the area reserved for the tabs
 */
@property (assign, nonatomic) CGFloat tabHeight;

#pragma mark - Tab Bar Button Properties

/**
 * The default width of the button that reveals the drop down of off-screen tabs
 */
@property (assign, nonatomic) CGFloat overflowButtonWidth;

/**
 * The default width of the 'new tab' button.
 */
@property (assign, nonatomic) CGFloat newTabButtonWidth;

/** The tint color of the 'new tab' and 'overflow dropdown' icons. This defaults to the `tertiaryTintColor` on the theme. */
@property (nonatomic, retain) UIColor *tabButtonsTintColor;

#pragma mark - Button view properties

/** The size of the shadow underneath the button area (containing the overflow and new tab buttons). This defaults to the `shadowDepth` on the theme. */
@property (nonatomic, assign) CGFloat shadowDepth;

#pragma mark - Initialization
/**
 @name Initialization
 */

/**
 Initializes the style object with a set of custom images for the tabbed view. If an image is passed in as `nil`, the style will use a default image for that property.
 
 Deprecated in 2.1, use initWithTheme:customImages: instead.
 */
- (id)     initWithTheme:(SEssentialsTheme *)theme
             activeImage:(UIImage*)activeImage
           inactiveImage:(UIImage*)inactiveImage
           activeTabMask:(UIImage*)activeTabMask
         inactiveTabMask:(UIImage*)inactiveTabMask
           closeTabImage:(UIImage*)closeTabImage
    closeTabPressedImage:(UIImage*)closeTabPressedImage
             addTabImage:(UIImage*)addTabImage
      addTabPressedImage:(UIImage*)addTabPressedImage
              addTabMask:(UIImage*)addTabMask
       overflowTabsImage:(UIImage*)overflowTabsImage
overflowTabsPressedImage:(UIImage*)overflowTabsPressedImage
        overflowTabsMask:(UIImage*)overflowTabsMask
         buttonsEndImage:(UIImage*)buttonsEndImage
     buttonsEndImageMask:(UIImage*)buttonsEndImageMask
      tabBackgroundImage:(UIImage*)tabBackgroundImage
       tabBackgroundMask:(UIImage*)tabBackgroundMask __deprecated;

/** Initializes the style object with a set of custom images for the tabbed view, using the `SEssentialsTabbedViewCustomImagesWrapper`. If an image is passed in as `nil`, the style will use a default image for that property. */
-(id)initWithTheme:(SEssentialsTheme *)theme customImages:(SEssentialsTabbedViewCustomImagesWrapper*)customImages;

#pragma mark - Images
/**
 @name Images
 */

/**
 * Graphics for the active tab item button
 */
@property (nonatomic, retain, readonly) UIImage *activeImage;

/**
 * Graphics for the inactive tab item button
 */
@property (nonatomic, retain, readonly) UIImage *inactiveImage;

/**
 * The mask image to use for active tabs
 */
@property (nonatomic, retain, readonly) UIImage *activeTabMask;

/**
 The mask image to use for inactive tabs
 */
@property (nonatomic, retain, readonly) UIImage *inactiveTabMask;

/**
 The fixed inset on the tab active and inactive images. The image provided
 needs to be of the 9-part stretchable form used by
 `UIImage:resizableImageWithCapInsets:` - with a 1x1 central stretchable area.
 `tabImageTopInset` is the size of the top inset.
 */
- (NSInteger)tabImageTopInset;

/**
 The fixed inset on the tab active and inactive images. The image provided
 needs to be of the 9-part stretchable form used by
 `UIImage:resizableImageWithCapInsets:` - with a 1x1 central stretchable area.
 `tabImageLeftInset` is the size of the left inset.
 */
- (NSInteger)tabImageLeftInset;

/**
 * Graphics for the close tab icon
 */
@property (nonatomic, retain, readonly) UIImage *closeTabImage;

/**
 * Graphics for the close tab icon pressed
 */
@property (nonatomic, retain, readonly) UIImage *closeTabPressedImage;


/**
 * Graphics for the 'new tab' button
 */
@property (nonatomic, retain, readonly) UIImage *addTabImage;

/**
 * Graphics for the 'new tab' button when pressed
 */
@property (nonatomic, retain, readonly) UIImage *addTabPressedImage;

/**
 * Graphics for the 'new tab' button mask
 */
@property (nonatomic, retain, readonly) UIImage *addTabMask;

/**
 * Graphics for the 'overflow dropdown' button
 */
@property (nonatomic, retain, readonly) UIImage *overflowTabsImage;

/**
 * Graphics for the 'overflow dropdown' button when pressed
 */
@property (nonatomic, retain, readonly) UIImage *overflowTabsPressedImage;

/**
 * Graphics for the 'overflow dropdown' button mask
 */
@property (nonatomic, retain, readonly) UIImage *overflowTabsMask;

/**
 * Graphics for the background mask of the 'new tab' and 'overflow dropdown' buttons
 */
@property (nonatomic, retain, readonly) UIImage *buttonsMask;

/**
 * Graphics for the background of the tab bar
 */
@property (nonatomic, retain, readonly) UIImage *tabBackgroundImage;

/**
 * Graphics for the mask of the background of the tab bar
 */
@property (nonatomic, retain, readonly) UIImage *tabBackgroundMask;

/**
 * Graphics for the edge of the tab bar under the buttons
 */
@property (nonatomic, retain, readonly) UIImage *buttonsEndImage;

/**
 * Graphics for the mask of the edge of the tab bar under the buttons
 */
@property (nonatomic, retain, readonly) UIImage *buttonsEndImageMask;

@end
