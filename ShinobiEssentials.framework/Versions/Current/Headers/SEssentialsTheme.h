//
//  SEssentialsTheme.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SEssentialsThemeElementStyle)    {
    FLAT,
    DECORATED
};

/**
 The appearance of a ShinobiEssentials control is defined by its style object. The default settings of the style object are determined by the theme it is created with. A style object is always created with a theme, either one you set yourself, or the default global theme. The theme defines a set of generic properties, which individual style objects can then utilize for their specific needs.  The properties defined are:
 
 - A palette of colors to use within a control.
 - A set of textures which can be used for sub-components of a control.
 - A set of fonts and text colors which can be used within a control.
 
 We also use the concept of decoration when configuring the look and feel of ShinobiEssentials controls.  This refers to things such as shine, shadowing or chisel effects which are applied to the controls. In iOS7, we tend to turn off shine or shadowing effects, or at least make them a lot more subtle.  In earlier versions of iOS, we use shine and shadowing effects to make items look more realistic.  Chisel effects are applied in all versions of iOS, although again, they are more subtle in iOS7.
 
 For more information on how the properties of a theme map to the properties of a specific style object, you should look at the API documentation for that style object. The available style objects are listed below:
 
 - Progress and activity indicators: `SEssentialsProgressIndicatorStyle`
 - Sliding overlay: `SEssentialsSlidingOverlayStyle`
 - Flow layout: `SEssentialsFlowLayoutStyle`
 - Tabbed view: `SEssentialsTabbedViewStyle`
 - Accordion: `SEssentialsAccordionSectionHeaderStyle`
 - Pull to Action: `SEssentialsPullToActionStyle`
 */
@interface SEssentialsTheme : NSObject

/** DEPRECATED: You should use [ShinobiEssentials theme] instead
 
 * The default theme used by all controls is a singleton accessed through this method.
 * When the properties on this are updated all dependent controls will update unless
 * their styles have explicitly set values. By default this is initialized to SEssentialsDarkTheme.
 */
+ (SEssentialsTheme *)sharedTheme DEPRECATED_ATTRIBUTE;

/** DEPRECATED: You should call [ShinobiEssentials setTheme:] instead

 * The default theme can be changed completely here.  Any new controls you create will use this theme by default.
 */
+ (void)setSharedTheme:(SEssentialsTheme*)theme DEPRECATED_ATTRIBUTE;

#pragma mark - Color scheme
/** @name Color scheme */

/**
  This sets the color used behind the texture of controls.
 */
@property (nonatomic,retain) UIColor *primaryTintColor;

/**
  This sets the color used behind the texture on subviews of controls.
 */
@property (nonatomic,retain) UIColor *secondaryTintColor;

/**
  This sets the color used on controls that require a third themed color (e.g. the button in the `SEssentialsSlidingOverlay`)
 */
@property (nonatomic, retain) UIColor *tertiaryTintColor;

/** The color used for controls which have elements with an inactive state.  
 
 An example of one of these controls would be the progress indicators.  The background track of the indicator will use this color. */
@property (nonatomic, retain) UIColor *inactiveTintColor;

/** The color used for controls which have elements with an active state.

 An example of one of these controls would be the `SEssentialsAccordion` header. The pressed state of the header will use this color. */
@property (nonatomic, retain) UIColor *activeTintColor;

#pragma mark - Textures
/** @name Textures */

/**
 This sets the texture used on the background of controls and is supplied as a UIColor
 */
@property (nonatomic,retain) UIColor *primaryTexture;

/**
 This sets the texture on subviews of controls and is supplied as a UIColor
 */
@property (nonatomic,retain) UIColor *secondaryTexture;

#pragma mark - Text
/** @name Text */

/**
  This sets the font used on controls which display text (e.g. the `SEssentialsAccordion` section title).
  Individual controls will have styling options to control the font size.
 */
@property (nonatomic, retain) UIFont *primaryFont;

/**
 The alternative font to use for text display (e.g. tab header titles). If none is provided, the `primaryFont` will be used instead. */
@property (nonatomic, retain) UIFont *secondaryFont;

/**
 This sets the font color of text used on controls (e.g. `SEssentialsAccordion` section header font color, `SEssentialTabbedView` header font color)
 */
@property (nonatomic, retain) UIColor *primaryTextColor;

/** This sets the secondary text color to use within controls. */
@property (nonatomic, retain) UIColor *secondaryTextColor;

#pragma mark - Decoration
/** @name Decoration */

/** This sets the primary color to use for decorations (e.g. the chisel on the sliding overlay) */
@property (nonatomic, retain) UIColor *primaryDecorationTintColor;

/** This sets the secondary color to use for decorations (e.g. the chisel on the sliding overlay) */
@property (nonatomic, retain) UIColor *secondaryDecorationTintColor;

/** This sets the shine color used on controls (e.g. toolbar shine on the sliding overlay, shine on progress indicators and accordion section headers) */
@property (nonatomic, retain) UIColor *shineColor;

/** This sets the drop shadow color used on controls (e.g. underneath the sliding overlay's overlay, and the accordion drop shadow) */
@property (nonatomic, retain) UIColor *shadowColor;

/** The depth of the drop shadow used on controls (e.g. underneath the sliding overlay's overlay, and the accordion drop shadow) */
@property (nonatomic, assign) CGFloat shadowDepth;

/** The look and feel of sub-elements in each control will be affected by whether we are applying decoration to our control.
 
 For example, in iOS7, we tend to not apply shading or shadowing, or at least use more subtle effects. In earlier versions of iOS, it is common to use shading and shadowing to give a more skeumorphic look. Sub-elements should reflect this. We currently provide two styles for sub-elements:
 
 - Flat. This is intended for use with iOS7 themes. Borders aren't applied around the edges of sub-elements, and shading effects are turned off.
 - Decorated. This is intended for use with earlier versions of iOS. Sub-elements are given shading and shadowing effects, and some elements will be given extra borders or chrome.
 */
@property (nonatomic, assign) SEssentialsThemeElementStyle elementStyle;

@end

