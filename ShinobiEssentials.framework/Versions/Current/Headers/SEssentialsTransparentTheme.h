//
//  SEssentialsTransparentTheme.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "SEssentialsTheme.h"

/**
 `SEssentialsTransparentTheme` is a concrete implementation of an `SEssentialsTheme` with
 transparent tones as default.
 
 To use `SEssentialsTransparentTheme` as the default global theme:
 
    [ShinobiEssentials setTheme:[[[ SEssentialsTransparentTheme alloc] init] autorelease]];
*/

@interface SEssentialsTransparentTheme : SEssentialsTheme

@end
