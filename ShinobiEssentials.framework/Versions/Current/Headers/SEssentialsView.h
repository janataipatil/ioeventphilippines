//
//  SEssentialsView.h
//  ShinobiEssentials
//
//  (c) Scott Logic Ltd 2012. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

/**
 SEssentialsView is a subclass of UIView which provides shadowing, shine and a textured background.
 
 By default, all of these properties are disabled.  Users can use the setter methods to turn on shine, shadow or texturing, or to change the background color of the view.
 */
@interface SEssentialsView : UIView

#pragma mark - Background
/** @name Background */

/**
 The texture layer within the view.  By default, no texture is applied.
 */
@property (nonatomic, retain) CALayer *textureLayer;

/** The background color of the view. */
@property (nonatomic, retain) UIColor *tintColor;

/** The background texture of the view. */
@property (nonatomic, retain) UIColor *texture;

/**
 Applies a shine to the top of the view and a chiseled line to the bottom of the view.
 */
-(void) addDecoration;

#pragma mark - Shine
/** @name Shine */

/**
 Applies a shine to the top of the view.
 */
-(void) addShine;

/**
 Configures the shine layer on the view.  You pass it a set of locations in the view at which colors should be applied, and a set of colors to be applied in those locations.  The shine layer renders gradients between these locations.
 
 @param colors  The colors to be applied must be specified as an array of UIColors.
 @param locations   The locations where the colors will be applied.  Each object in the array should be a NSNumber, with a value between 0 and 1.
 */
- (void)configureShineLayerWithColors: (NSArray*)colors locations:(NSArray*)locations;

/**
 This layer adds a shine to the top of the view.  By default, this effect is disabled.
 */
@property (nonatomic, retain) CAGradientLayer *shineLayer;

/** The tint color of the shine layer. Defaults to White.
 
 The alpha value will be overridden by `shineAlpha`.
 */
@property (nonatomic, retain) UIColor *shineTintColor;

/**
 Sets how strongly the shine layer on the view is shown over the view.  Its maximum value is 1, and its minimum value is 0.  It defaults to 0.4.
 */
@property (nonatomic, assign) CGFloat shineAlpha;

#pragma mark - Chisel
/** @name Chisel */

/**
 Applies a chiseled line to the bottom of the view.
 */
-(void) addChisel;

/** The main color of the chisel. Defaults to Black. */
@property (nonatomic, retain) UIColor *chiselPrimaryTintColor;

/** The complementary color of the chisel. Defaults to white. */
@property (nonatomic, retain) UIColor *chiselSecondaryTintColor;

/**
 This layer adds a chisel to the bottom of the view.  By default, this effect is disabled.
 */
@property (nonatomic, retain) CAGradientLayer *chiselLayer;

#pragma mark - Shadows
/** @name Shadows */

/**
 Applies a shadow to the right hand side of the view.
 */
-(void) addShadowRight;

/**
 Applies a shadow to the left hand side of the view.
 */
-(void) addShadowLeft;

/**
 Applies a shadow to the top of the view.
 */
-(void) addShadowTop;

/**
 Applies a shadow to the bottom of the view.
 */
-(void) addShadowBottom;

/** The color of the shadow layers. Defaults to Black. */
@property (nonatomic, retain) UIColor *shadowTintColor;

/** The depth of the drop shadow. Defaults to `8`. */
@property (nonatomic, assign) CGFloat shadowDepth;

/**
 Applies a shadow to the left hand side of the view.  By default, this shadow is disabled.
 */
@property (nonatomic, retain) CAGradientLayer *leftShadowLayer;

/**
 Applies a shadow to the right hand side of the view.  By default, this shadow is disabled.
 */
@property (nonatomic, retain) CAGradientLayer *rightShadowLayer;

/**
 Applies a shadow to the bottom of the view.  By default, this shadow is disabled.
 */
@property (nonatomic, retain) CAGradientLayer *topShadowLayer;

/**
 Applies a shadow to the top of the view.  By default, this shadow is disabled.
 */
@property (nonatomic, retain) CAGradientLayer *bottomShadowLayer;

@end
